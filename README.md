DEKAF flow solver
=================

The DEKAF flow solver is distributed under a GNU Lesser General
Public License 3.0

DEKAF is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU Lesser General Public License for more details
http://www.gnu.org/licenses.

In order to **install DEKAF** please follow the instructions found
in the [INSTALL.md](INSTALL.md) file.

Please refer to the documentation of functions
`SourceCode/DEKAF.m` and `SourceCode/Defaults/setDefaults.m`
for more information on how to call the solver and the main
options available. Examples can be found in the `Examples/`
folder.

If you are to be involved in the development of DEKAF, please
refer to the [developers manual](DevMANUAL.md).

Included features (version 1.0.0) are:

A Solvers
---------

A.1) Solvers for the (quasi-)self-similar and (full) boundary-layer equations.
*  A.1.1) Constant spanwise velocity (quasi-3D flow)
*  A.1.2) Multi-species assumptions
*  A.1.3) 1-T and 2-T to describe the thermal state of the gas (numberOfTemp)
*  A.1.4) Spec-spec and spec-mix diffusion theories

A.2) Geometries
*  A.2.1) Wedge
*  A.2.2) Cone
*  A.2.3) User-specified body of revolution (internal or external side)


B Thermophysical modeling
-------------------------

B.1) Flow assumptions:
*  B.1.1) `CPG` (Calorically Perfect Gas)
*  B.1.2) `TPG` (Thermally Perfect Gas)
*  B.1.3) `LTE` (Local Thermodynamic Equilibrium with constant elemental composition)
*  B.1.4) `CNE` (Chemical Non-Equilibrium)

B.2) Mixtures:
*  B.2.1)  `Air` (Air)
*  B.2.2)  `Air-He` (Air & He)
*  B.2.3)  `Air-Ne` (Air & Ne)
*  B.2.4)  `Air-Ar` (Air & Ar)
*  B.2.5)  `Air-CO2` (Air & CO2)
*  B.2.6)  `air2` (O2 & N2)
*  B.2.7)  `air2-He` (O2, N2 & He)
*  B.2.8)  `air2-Ne` (O2, N2 & Ne)
*  B.2.9)  `air2-Ar` (O2, N2 & Ar)
*  B.2.10) `air2-CO2` (O2, N2 & CO2)
*  B.2.11) `oxygen2mutation` (O & O2)
*  B.2.12) `oxygen2Bortner` (O & O2)
*  B.2.13) `air5Park85` (N, O, NO, N2 & O2)
*  B.2.14) `air5Park90` (N, O, NO, N2 & O2)
*  B.2.15) `air5Stuckert91` (N, O, NO, N2 & O2)
*  B.2.16) `air5Bortner` (N, O, NO, N2 & O2)
*  B.2.17) `air5mutation` (N, O, NO, N2 & O2)
*  B.2.18) `air11Park91` (N, O, N2, O2, NO, N+, O+, N2+, O2+, NO+ & e-)
*  B.2.19) `air11Park91noIoniz` (N, O, N2, O2, NO, N+, O+, N2+, O2+, NO+ & e-)
*  B.2.20) `air11mutation` (e-, N, N+, O, O+, NO, N2, N2+, O2, O2+ & NO+)
*  B.2.21) `mars5Park94` (O, C, CO, O2 & CO2)
*  B.2.22) `airC6Mortensen` (CO2, N, O, NO, N2, & O2)
*  B.2.23) `airC11Mortensen` (C3, CO2, C2, CO, CN, C, N, O, NO, N2, & O2)

B.3) Transport models:
*  B.3.1) Power law (only CPG & TPG)
*  B.3.2) Sutherland (only CPG & TPG)
*  B.3.3) Blottner-Eucken-Wilke
*  B.3.4) Sutherland-Wilke
*  B.3.5) Sutherland-Eucken-Wilke
*  B.3.6) Gupta-Wilke
*  B.3.7) Chapman-Enskog (3rd approx. in CNE giving problems for the electron thermal conductivity)
*  B.3.8) Yos67 (approximations to Chapman-Enskog)
*  B.3.9) Brokaw58 (further approximations from Yos64)

B.4) Diffusion models:
*  B.4.1) `cstSc` (constant Schmidt number)
*  B.4.2) `cstLe` (constant Lewis number)
*  B.4.3) `FOCE_Effective` (1st-order Chapman-Enskog plus EBD)
*  B.4.4) `FOCE_EffectiveAmbipolar` (1st-order Chapman-Enskog plus EBAD)
*  B.4.5) `FOCE_Ramshaw` (1st-order Chapman-Enskog plus Ramshaw SCEBD)
*  B.4.6) `FOCE_RamshawAmbipolar` (1st-order Chapman-Enskog plus Ramshaw SCEBAD)
*  B.4.7) `FOCE_StefanMaxwell` (1st-order Chapman-Enskog plus Stefan-Maxwell equation)
*  B.4.8) `FOCE_StefanMaxwellAmbipolar` (1st-order Chapman-Enskog plus Stefan-Maxwell equation assuming ambipolar diffusion)

B.5) Collision models for neutral species (needed for B.3.7-9 & B.4.3-9)
*  B.5.1) `Wright2005`
*  B.5.2) `Wright2005Bellemans2015`
*  B.5.3) `Stuckert91`
*  B.5.4) `GuptaYos90`
*  B.5.5) `mars5dplr`
*  B.5.6) `Yos63 `
*  B.5.7) `Capitelli98`
*  B.5.8) `Mason67Devoto73`
Not all of them are supported for all collisions (see `getPair_Collision_constants`)

B.6) Thermal models:
*  B.6.1) RRHO (Rigid-Rotor and Harmonic-Oscillator)


C Inviscid flow region
----------------------

C.1) Shock relations:
*  C.1.1) vibrationally and chemically frozen (same as CPG)
*  C.1.2) chemically frozen yet in vibrational equilibrium (same as TPG with 1T)
*  C.1.3) chemical and vibrational equilibrium (LTE)
Both oblique and conical shocks can be solved

C.2) Boundary-layer edge properties:
*  C.2.1 User-specified (external solvers)
*  C.2.2 Inviscid chemically-reacting and vibrationally-relaxing 1D solver
*  C.2.3 User-specified profiles obtained from an airfoil (see `SourceCode/Utilities/Coordsys/airfoil/convert_airfoil.m`)


D Boundary conditions
---------------------

D.1) Thermal:
*  D.1.1) Adiabatic
*  D.1.2) Consant heat flux
*  D.1.3) Isothermal
*  D.1.4) Fixed wall temperature profile
*  D.1.5) Fixed non-dimensional wall enthalpy
*  D.1.6) Vibrationally frozen
*  D.1.7) Thermal equilibrium
*  D.1.8) Ablative boundary condition (surface energy balance)

D.2) Blowing:
*  D.2.1) Impenetrable wall
*  D.2.2) Self-similar blowing (constant fw)
*  D.2.3) Constant wall-normal velocity
*  D.2.4) Fixed wall-normal velocity profile
*  D.2.5) Constant wall-normal mass flux
*  D.2.6) Fixed wall-normal mass flux profile
*  D.2.7) Ablative boundary condition (surface mass balance)

D.3) Mass fractions:
*  D.3.1) Non-catalytic
*  D.3.2) Catalytic (gamma model) *NV
*  D.3.3) Fixed mass fraction
*  D.3.4) Fixed mass fraction profile
*  D.3.5) Ablative boundary condition (species surface mass balance) *NV


E Numerics
---------------------

E.1) Wall-normal mappings:
*  E.1.1) Malik

E.2) Streamwise mappings:
*  E.2.1) Linear
*  E.2.2) Logarithmic
*  E.2.3) Variable tanh
*  E.2.4) Customized (user-fixed) x range
*  E.2.4) Customized (user-fixed) xi range

E.3) Wall-normal numerical scheme:
*  E.3.1) Chebyshev pseudo-spectral method

E.4) Streamwise numerical scheme:
*  E.4.1) 1st-6th order forward difference

Footnotes
-----------
*NV Not verified

March 18th 2020
