% compare_LTE_DEKAFvsVESTA_ChapmanEnskog is a script to run the DEKAF solver in LTE conditions and using transport properties obtained from Chapman-Enskog and compare it to VESTA's
%
% Date: March 2018

clear;

%%
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode/']));
clear options intel

% Flow parameters
M_e = 10;
T_e = 600;
p_e = 4000;

% Optional inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.thermal_BC              = 'adiab';              % Wall bc
%options.G_bc                    = 0.1;                  % Value for g boundary condition
options.flow                    = 'LTE';                % flow assumption
options.mixture                 = 'air5mutation';       % mixture
options.modelTransport          = 'CE_12';              % transport model
options.modelDiffusion          = 'FOCE_Ramshaw';       % diffusion model
options.molarDiffusion          = true;                 % we want our diffusion coefficients to be for mole fraction gradients

options.tol                     = 1e-14;                % Convergence tolerance
options.it_max                  = 50;
options.T_tol                   = 1e-13;
options.T_itMax                 = 50;

options.plot_transient          = false;                % to plot the inter-iteration profiles
options.plotRes                 = false;                % to plot the residual convergence

options.dimoutput               = true;                 % to output fields in dimensional form
intel.xi                        = 1e-4;                 % xi location where we want the dimensional profiles

intel.M_e                       = M_e;                   % Mach number at boundary-layer edge                        [-]
intel.T_e                       = T_e;                   % Static temperature at boundary-layer edge                 [K]
intel.p_e                       = p_e;                   % Absolute static pressure at boundary-layer edge           [Pa]

[intel,options] = DEKAF(intel,options);


%% Running VESTA N=5001
clear options
addpath(genpath('/home/miromiro/workspace/VESTA/'));
options.tol = 1e-15;
options.L = 40;
options.N = 5001;
options.s_new = 1.0;
options.H_F= true;
options.G_bc = 0; % adiabatic flow
options.beta = 0;
options.import_sol = false;% false
%options.FileName = 'profiles/.mat';
options.fluid = 'mutation_air';% 'air';% 'nitrogen';%
options.mutationfilename = 'mutationTables_T200-10-15000_P400-100-20000_air5.dat';
options.M_e = M_e;
options.T_e = T_e;
options.p_e = p_e;
options.save = false;

disp(['Running for N=',num2str(options.N)]);
compr_profile{1} = CBL_profile (options);
compr_profile{1}.eta = linspace(0,options.L,options.N);
compr_profile{1}.N = options.N;

%% Running VESTA N=10001
options.N = 10001;
disp(['Running for N=',num2str(options.N)]);
compr_profile{2} = CBL_profile (options);
compr_profile{2}.eta = linspace(0,options.L,options.N);
compr_profile{2}.N = options.N;

%% Running VESTA N=100001
options.N = 100001;
disp(['Running for N=',num2str(options.N)]);
compr_profile{3} = CBL_profile (options);
compr_profile{3}.eta = linspace(0,options.L,options.N);
compr_profile{3}.N = options.N;

%% Running VESTA N=1000001
options.N = 1000001;
disp(['Running for N=',num2str(options.N)]);
compr_profile{4} = CBL_profile (options);
compr_profile{4}.eta = linspace(0,options.L,options.N);
compr_profile{4}.N = options.N;

%% Saving
save(['VESTAvsDEKAF_LTE_M',num2str(M_e),'_T',num2str(T_e),'_p',num2str(p_e),'.mat'],'compr_profile','intel','options');

%% Plotting
figure
clear plots legends
N_VESTAs = length(compr_profile);
for ii=1:N_VESTAs
    plots(ii) = plot(compr_profile{ii}.g,compr_profile{ii}.eta,'Color',two_colors(ii,N_VESTAs,'GB')*0.8);
    legends{ii} = ['VESTA N=',num2str(compr_profile{ii}.N)];
    hold on;
end
plots(N_VESTAs+1) = plot(intel.g,intel.eta,'k--');
legends{N_VESTAs+1} = 'DEKAF N=100';
ylim([0,6]); legend(plots,legends);
xlabel('H/H_e [-]'); ylabel('\eta [-]');
