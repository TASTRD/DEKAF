% compare_LTE_DEKAFvsVESTA is a script to run the DEKAF solver in LTE
% conditions and compare it to VESTA's
%
% Date: October 2017
%{
clear;
close all;
%clc
%}

addpath(genpath('/home/miromiro/workspace/DEKAF/SourceCode/'));

clear options intel

% Optional inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.thermal_BC              = 'adiab';              % Wall bc
%options.G_bc                    = 0.1;                  % Value for g boundary condition
options.flow                    = 'LTE';                % flow assumption
options.mixture                 = 'air5mutation';       % mixture
options.modelTransport          = 'BlottnerEucken';     % transport model
options.cstPr                   = false;                % constant PRandtl number
options.specify_cp              = false;                % user-specified cp

options.tol                     = 1e-14;                % Convergence tolerance
options.it_max                  = 50;
options.perturbRelaxation       = 1;
options.highOrderTerms          = false;                % include higher order fluctuation terms in the equations
options.expandBaseFlow          = false;                % expand g about base flow variable T and dfdeta
options.T_tol                   = 1e-13;
options.T_itMax                 = 50;

options.plot_transient          = false;                % to plot the inter-iteration profiles
options.plotRes                 = true;                 % to plot the residual convergence

% Convergence gif options
% options.gifConv                 = true;
% options.fields4gif              = {'fh','dfh_deta','dfh_deta2','dfh_deta3','gh','dgh_deta','dgh_deta2'};
% options.gifLogPlot              = true;
% options.gifxlim                 = [1e-16,1e0];

options.dimoutput               = true;                 % to output fields in dimensional form
intel.xi                        = 1e-4;                 % xi location where we want the dimensional profiles

% Flow parameters
intel.M_e                       = 10;                    % Mach number at boundary-layer edge                        [-]
intel.T_e                       = 600;                   % Static temperature at boundary-layer edge                 [K]
intel.p_e                       = 4000;                 % Absolute static pressure at boundary-layer edge           [Pa]
% intel.cp                        = 1004.5;               % Constant specific heat                                    [J/(kg K)]
% intel.gam                       = 1.4;                  % Constant specific heat ratio                              [-]
% options.Pr                      = 0.7;                  % Constant Prandtl                              [-]
% intel.ys_e                      = {0.22,0.78};          % mass fractions of O2 and N2
% intel.ys_e                      = {1e-10,1e-10,1e-10,0.78-3e-10,0.22}; % mass fractions of N, O, NO, N2 and O2
intel.Sc_e                      = 0.5;                  % Schmidt number

intel = DEKAF(intel,options);


%% Running VESTA
clear options
addpath(genpath('/home/miromiro/workspace/VESTA/'));
options.tol = 1e-15;
options.L = 40;
options.N = 10001;
options.s_new = 1.0;
options.H_F= true;
options.G_bc = 0; % adiabatic flow
options.beta = 0;
options.import_sol = false;% false
%options.FileName = 'profiles/.mat';
options.fluid = 'mutation_air';% 'air';% 'nitrogen';%
options.mutationfilename = 'mutationTables_T200-10-15000_P400-100-20000_air5.dat';
options.M_e = 10;
options.T_e = 600;
options.p_e = 4000;
options.save = true;
compr_profile = CBL_profile (options);

eta_VESTA = linspace(0,options.L,options.N);

%% Plotting
figure
plot(intel.g,intel.eta,compr_profile.g,eta_VESTA,'--');
ylim([0,6]); legend('DEKAF','VESTA');
xlabel('g [-]'); ylabel('\eta [-]');