function [] = computeEqResiduals(xAxis,rc,s_e,gam,R,cp,M_e,T_e,p_e)
% computeEqResiduals plots the residuals of the streamwise momentum and
% total-energy equations.
%
% Usage:
%   (1)
%       computeEqResiduals(xAxis,rc,s_e,gam,R,cp,M_e,T_e,p_e)
%
% Inputs:
%   xAxis       [m]
%       coordinate along the axis of revolution
%   rc          [m]
%       radius of spanwise curvature
%   s_e         [m]
%       surface coordinate
%   gam         [-]
%       heat capacity ratio
%   cp          [J/kg-K]
%       heat capacity
%   R           [J/kg-K]
%       gas constant
%   M_e         [-]
%       mach number
%   T_e         [K]
%       temperature
%   p_e         [Pa]
%       static pressure
%
% Author: Fernando Miro Miro
% Date: July 2019
% GNU Lesser General Public License 3.0


% Computing streamline equation residuals
Dse = FDdif_nonEquis(s_e,'centered6');
U_e = M_e.*sqrt(gam*R*T_e);
H_e = cp*T_e + 0.5*U_e.^2;
rho_e = p_e./(R*T_e);

momRef = abs(Dse*p_e);
enrRef = max([cp.*T_e , 0.5*U_e.^2],[],2);

mom_eq = abs(Dse*p_e + rho_e.*U_e.*(Dse*U_e))./momRef;
enr_eq = abs(Dse*H_e)./enrRef;

dpds = Dse*p_e;
rhodu2ds = -rho_e.*U_e.*(Dse*U_e);

figure; Ni=3;Nj=1;ic=0;
ic=ic+1; subplot(Ni,Nj,ic); plot(xAxis,rc);         xlabel('x [m]'); ylabel('r [m]');                   title('Nozzle geometry'); grid on;daspect([1,1,1]);ylim([0,Inf]);
% ic=ic+1; subplot(Ni,Nj,ic); semilogy(xAxis,dpds,'-',xAxis,rhodu2ds,'--'); xlabel('x [m]'); ylabel('pressure gradient [Pa/m]'); legend('\partialp/\partials','-\rho u \partialu/\partials'); title('Bernouilli equation');
% ic=ic+1; subplot(Ni,Nj,ic); semilogy(xAxis,H_e);                          xlabel('x [m]'); ylabel('total enthalpy [J/kg]'); title('Energy equation');
ic=ic+1; subplot(Ni,Nj,ic); semilogy(xAxis,mom_eq); xlabel('x [m]'); ylabel('mom residual [Pa/m]');     title('1D-momentum-equation residual'); grid on;
ic=ic+1; subplot(Ni,Nj,ic); semilogy(xAxis,enr_eq); xlabel('x [m]'); ylabel('enr residual [J/kg-m]');   title('1D-energy-equation residual');   grid on;

end % computeEqResiduals