% Test_argonNozzleBL is a script to compare the nozzle boundary layer
% obtained with DEKAF + an inviscid solver to that of a full NS solution
% from CFD++.
%
% Author: Fernando Miro Miro
% Date: February 2020

clear; close all;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

%% Thermophysical properties
Tw = 296; % [K]
Pr  = 0.70;     % Prandtl number [-]
mixture = 'Ar'; % pure argon mixture
gam = 5/3;      % atomic species (cp/R=5/2 and cv/R=3/2)
mm2m = 1e-3;    % milimeters to meters
R = 208.1738; % Argon
cp = 5/2*R; % mono-atomic species

%% Nozzle
% load('MoC_inviscidProfiles.mat'); skip=1;  % MoC
% s_e     = s_e(1:skip:end)   * mm2m; % converting to meters
% rc      = rc(1:skip:end)    * mm2m;
% xAxis   = xAxis(1:skip:end) * mm2m;
load('CFDpp_viscousProfiles.mat'); skip=1; % CFD++
s_e     = s_e(1:skip:end); % converting to meters
rc      = rc(1:skip:end);
xAxis   = xAxis(1:skip:end);
M_e     = M_e(1:skip:end);
T_e     = T_e(1:skip:end);
p_e     = p_e(1:skip:end);

computeEqResiduals(xAxis,rc,s_e,gam,R,cp,M_e,T_e,p_e);

%% intermediate plotting
figure; Ni=4;Nj=1;ic=0;
ic=ic+1; subplot(Ni,Nj,ic); plot(xAxis,rc);      xlabel('x [m]'); ylabel('r [m]');       title('Nozzle geometry'); grid on;daspect([1,1,1]);ylim([0,Inf]);
ic=ic+1; subplot(Ni,Nj,ic); plot(xAxis,M_e);     xlabel('x [m]'); ylabel('M_e [-]');     title('Mach number'); grid on;
ic=ic+1; subplot(Ni,Nj,ic); plot(xAxis,T_e);     xlabel('x [m]'); ylabel('T_e [K]');     title('Static temperature'); grid on;
ic=ic+1; subplot(Ni,Nj,ic); semilogy(xAxis,p_e); xlabel('x [m]'); ylabel('p_e [Pa]');    title('Static pressure'); grid on;

%% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 6;                   % Domain size (in non-dimensional eta length)
options.eta_i                   = 2;                    % Mapping parameter, eta_crit
options.tol                     = 1e-12;                % Convergence tolerance
options.it_max                  = 40;                   % maximum number of iterations

% flow options
options.thermal_BC              = 'Twall';              % Wall bc (isothermal)
options.G_bc                    = Tw;                   % value of the wall temperature
options.flow                    = 'CPG';                % flow assumption
options.mixture                 = mixture;              % mixture
options.modelTransport          = 'Sutherland';         % transport model

options.specify_cp              = false;
options.specify_gam             = false;
options.cstPr                   = true;
options.Pr                      = Pr;

options.BLproperties = true;
% options.dimoutput = true;

% display options
options.plotRes = false;                        % we don't want the residual plot
options.MarchingGlobalConv = false;             % we don't want the global residual plot
options.quiet   = true;                         % do not display the full convergence info at each streamwise station

% flow conditions
% intel.M_e0              = M_e(1);               % Mach number at the first boundary-layer edge pos.     [-]
% intel.p_e               = p_e;                  % Pressure at the boundary-layer edge                   [Pa]
intel.M_e               = M_e;                  % Mach number at the first boundary-layer edge pos.     [-]
intel.p_e0              = p_e(1);               % Pressure at the boundary-layer edge                   [Pa]
intel.T_e               = T_e;                  % Temperature at boundary-layer edge                    [K]
intel.x_e               = s_e;                  % streamwise surface coordinate                         [m]
intel.rc                = rc;                   % nozzle radius                                         [m]
intel.xcAxis            = xAxis;                % nozzle axis location                                  [m]

% intel.cp                = cp;                   % heat capacity at constant pressure    [J/kg-K]
% intel.gam               = gam;                  % heat capacity ratio                   [-]

% marching mapping options
N_x = 200;
% options.xSpacing            = 'tanh';       % mapping in the marching direction
% options.mapOpts.N_x         = 200;          % number of streamwise points
% options.mapOpts.x_start     = 1e-8;         % intial surface location [m]
% options.mapOpts.x_end       = s_e(end);     % final surface location [m]
options.xSpacing            = 'custom_x';       % mapping in the marching direction
options.mapOpts.x_custom    = linspace(1e-8,s_e(end),N_x);

%% Executing DEKAF (without curvature)
options.coordsys = 'cylindricalInternal';
options.transverseCurvature = true;

[intelB,optionsB] = DEKAF(intel,options,'marching');

%% Executing DEKAF (without curvature)
options.coordsys = 'cylindricalInternal';
options.transverseCurvature = false;

[intelA,optionsA] = DEKAF(intel,options,'marching');

%% Saving
saveDEKAF('DKF_argonNozzle.mat','intelA','optionsA','intelB','optionsB');

%% plotting
Nxplt = 12;pltFlags={'mgm','vrdsInv','KR'}; lineS = {'-','-.','s'};
idx2plt = round(linspace(1,optionsB.mapOpts.separation_i_xi-1,Nxplt+1)); idx2plt(1) = [];
x2plt = intelB.x(1,idx2plt);
figure;
Ni=1; Nj=2;
for ix=1:Nxplt
        [~,idxA] = min(abs(intelA.x(1,:)-x2plt(ix)));
        [~,idxB] = min(abs(intelB.x(1,:)-x2plt(ix)));
        [~,idxCFD] = min(abs(s_e-x2plt(ix)));
        nameE = ['s=',num2str(intelA.x(1,idxA)*1e3,'%0.2f'),'mm (DEKAF witout curv.)'];
        nameI = ['s=',num2str(intelB.x(1,idxB)*1e3,'%0.2f'),'mm (DEKAF with curv.)'];
        nameCFD = ['s=',num2str(s_e(idxCFD)*1e3,'%0.2f'),'mm (CFD)'];
%         yMax = 1.2*intelB.delta99(idxB);
        yMax = 5e-3;

        ip=0;ic=1;
ip=ip+1;subplot(Ni,Nj,ip);
                            plot(intelA.u(:,idxA),intelA.y(:,idxA),lineS{ic},'Color',two_colors(ix,Nxplt+5,pltFlags{ic}),'displayname',nameE); hold on;ic=ic+1;
                            plot(intelB.u(:,idxB),intelB.y(:,idxB),lineS{ic},'Color',two_colors(ix,Nxplt+5,pltFlags{ic}),'displayname',nameI);  hold on;ic=ic+1;
                            plot(U_rot(:,idxCFD),Y_rot(:,idxCFD),  lineS{ic},'Color',[0,0,0],                          'displayname',nameCFD);hold on;ic=ic+1;
                xlabel('u [m/s]');  ylabel('r [m]'); grid minor; ylim([0,yMax]);ic=1;
ip=ip+1;subplot(Ni,Nj,ip);
                            plot(intelA.T(:,idxA),intelA.y(:,idxA),lineS{ic},'Color',two_colors(ix,Nxplt+5,pltFlags{ic}),'displayname',nameE); hold on;ic=ic+1;
                            plot(intelB.T(:,idxB),intelB.y(:,idxB),lineS{ic},'Color',two_colors(ix,Nxplt+5,pltFlags{ic}),'displayname',nameI);  hold on;ic=ic+1;
                            plot(T(:,idxCFD),Y_rot(:,idxCFD),      lineS{ic},'Color',[0,0,0],                          'displayname',nameCFD);hold on;ic=ic+1;
                xlabel('T [K]');    ylabel('r [m]'); grid minor; ylim([0,yMax]);
end
legend();