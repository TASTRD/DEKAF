% Test_fakeNozzleBL is a script to compare the a fake nozzle's boundary
% layer obtained with DEKAF + an isentropic expansion, and with various
% coordinate systems
%
% Author: Fernando Miro Miro
% Date: January 2020

clear; close all;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

%% Nozzle
x_e       = linspace(0,6,1001).';                       % nozzle axis coordinates
rStar   = 0.02;                                         % throat radius
R_e       = rStar + 0.1*(tanh(2)+tanh(x_e-2)+0.2*x_e);  % nozzle radius

%% Flow conditions

pe1 = 1000;     % static pressure desired at the end    [Pa]
Te1 = 70;       % static Temperature desired at the end [K]
Tw  = 293;      % Nozzle wall temperature               [K]

gam = 1.4;      % heat-capacity ratio                   [-]
cp  = 1004.5;   % heat capacity at constant pressure    [J/kg-K]
Pr  = 0.72;     % Prandtl number                        [-]
R   = cp*2/7;

%% Computing auxiliary variables
A_rat = R_e.^2./rStar.^2;
M_e = getIsentropic_M(A_rat,gam);

T_rat =  1+(gam-1)/2*M_e.^2;
p_rat = (1+(gam-1)/2*M_e.^2).^(gam/(gam-1));

T0 = Te1*T_rat(end); % stagnation temperature such that the static temperature at the end is what we want
p0 = pe1*p_rat(end); % stagnation pressure such that the static pressure at the end is what we want

T_e = T0./T_rat; % static temperature
p_e = p0./p_rat; % static pressure

D1x = FDdif_nonEquis(x_e,'centered4');      % streamwise differentiation matrix
theta = atand(D1x*R_e);                     % nozzle streamwise inclination
s_e = get_airfoil_arc_length(x_e,R_e);

computeEqResiduals(x_e,R_e,s_e,gam,R,cp,M_e,T_e,p_e);

%% intermediate plotting
figure; Ni=5;Nj=1;ic=0;
ic=ic+1; subplot(Ni,Nj,ic); plot(x_e,R_e);    xlabel('x [m]'); ylabel('r [m]');       title('Nozzle geometry'); grid on;daspect([1,1,1]);ylim([0,Inf]);
ic=ic+1; subplot(Ni,Nj,ic); plot(x_e,M_e);    xlabel('x [m]'); ylabel('M_e [-]');     title('Isentropic Mach number'); grid on;
ic=ic+1; subplot(Ni,Nj,ic); plot(x_e,T_e);    xlabel('x [m]'); ylabel('T_e [K]');     title('Isentropic static temperature'); grid on;
ic=ic+1; subplot(Ni,Nj,ic); plot(x_e,p_e);    xlabel('x [m]'); ylabel('p_e [Pa]');    title('Isentropic static pressure'); grid on;
ic=ic+1; subplot(Ni,Nj,ic); plot(x_e,theta);  xlabel('x [m]'); ylabel('\theta [deg]');title('Surface inclination'); grid on;

%% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 20;                   % Domain size (in non-dimensional eta length)
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 1e-12;                % Convergence tolerance
options.it_max                  = 40;                   % maximum number of iterations

% flow options
options.thermal_BC              = 'Twall';              % Wall bc (isothermal)
options.G_bc                    = Tw;                   % value of the wall temperature
options.flow                    = 'CPG';                % flow assumption
options.mixture                 = 'air2';               % mixture
options.modelTransport          = 'Sutherland';         % transport model

options.specify_cp              = true;
options.specify_gam             = true;
options.cstPr                   = true;
options.Pr                      = Pr;

options.BLproperties = true;
% options.dimoutput = true;

% display options
options.plotRes = false;                        % we don't want the residual plot
options.quiet   = true;                         % do not display the full convergence info at each streamwise station

% flow conditions
intel.M_e               = M_e;                  % Mach number at boundary-layer edge                    [-]
intel.T_e               = T_e;                  % Temperature at boundary-layer edge                    [K]
intel.p_e0              = p_e(1);               % Pressure at the first boundary-layer edge position    [Pa]
intel.x_e               = s_e;                  % streamwise surface coordinate                         [m]

intel.cp                = cp;                   % heat capacity at constant pressure    [J/kg-K]
intel.gam               = gam;                  % heat capacity ratio                   [-]

% marching mapping options
N_x = 200;
% options.xSpacing            = 'tanh';       % mapping in the marching direction
% options.mapOpts.N_x         = 200;          % number of streamwise points
% options.mapOpts.x_start     = 1e-8;         % intial surface location [m]
% options.mapOpts.x_end       = s_e(end);     % final surface location [m]
options.xSpacing            = 'custom_x';       % mapping in the marching direction
options.mapOpts.x_custom    = linspace(1e-8,s_e(end),N_x);

%% Executing DEKAF
ic=0;
ic=ic+1; [all_intels{ic},all_options{ic}] = DEKAF(intel,options,'marching');    % cartesian
caseNames{ic} = 'Cartesian';

%%
options.coordsys = 'cylindricalExternal';
intel.rc        = R_e;
intel.xcAxis    = x_e;
ic=ic+1; [all_intels{ic},all_options{ic}] = DEKAF(intel,options,'marching');    % cylindricalExternal
caseNames{ic} = 'Cylindrical external';

%%
options.coordsys = 'cylindricalInternal';
options.transverseCurvature = false;
ic=ic+1; [all_intels{ic},all_options{ic}] = DEKAF(intel,options,'marching');    % cylindricalInternal (without curvature)
caseNames{ic} = 'Cylindrical internal (without curvature)';

%%
options.coordsys = 'cylindricalInternal';
options.transverseCurvature = true;
ic=ic+1; [all_intels{ic},all_options{ic}] = DEKAF(intel,options,'marching');    % cylindricalInternal (with curvature)
caseNames{ic} = 'Cylindrical internal (with curvature)';

%% Saving
% saveLight('DKF_issentropicNozzle.mat',intel,options);
saveDEKAF('DKF_issentropicNozzle.mat','all_intels','all_options');

%% plotting
Nxplt = 12;pltFlag='Rbw2';
styleS = {'-','-.','--',':'};
lnwdths = [1,1,1,1.5];
idx2plt = round(linspace(1,N_x,Nxplt+1)); idx2plt(1) = [];
x2plt = all_intels{end}.x(1,idx2plt);
etaMax = 1.2*max(all_intels{ic}.eta99);
Ncases = length(caseNames);
figure; Ni=2; Nj=Nxplt;
for ix=1:Nxplt
    for ic=1:Ncases
        [~,idx] = min(abs(all_intels{ic}.x(1,:) - x2plt(ix)));
        yMax = 1.2*all_intels{1}.delta99(idx);
%         yMax = Inf;
        ip=0;
ip=ip+1;subplot(Ni,Nj,(ip-1)*Nj+ix); plot(all_intels{ic}.u(:,idx),all_intels{ic}.y(:,idx),styleS{ic},'linewidth',lnwdths(ic),'Color',two_colors(ic,Ncases,pltFlag),'displayname',caseNames{ic}); hold on;
                xlabel('u [m/s]');  ylabel('r [m]'); grid minor; ylim([0,yMax]);
                title(['s=',num2str(x2plt(ix),'%0.2f'),'m']);
% ip=ip+1;subplot(Ni,Nj,(ip-1)*Nj+ix); plot(all_intels{ic}.v(:,idx),all_intels{ic}.y(:,idx),styleS{ic},'linewidth',lnwdths(ic),'Color',two_colors(ic,Ncases,pltFlag),'displayname',caseNames{ic}); hold on;
%                 xlabel('v [m/s]');  ylabel('r [m]'); grid minor; ylim([0,yMax]);
ip=ip+1;subplot(Ni,Nj,(ip-1)*Nj+ix); plot(all_intels{ic}.T(:,idx),all_intels{ic}.y(:,idx),styleS{ic},'linewidth',lnwdths(ic),'Color',two_colors(ic,Ncases,pltFlag),'displayname',caseNames{ic}); hold on;
                xlabel('T [K]');    ylabel('r [m]'); grid minor; ylim([0,yMax]);
    end
end
legend();