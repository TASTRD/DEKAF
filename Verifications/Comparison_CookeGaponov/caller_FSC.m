% caller_FSC is a script to run the DEKAF solver to verify the 3D
% implementation against Gaponov and Smorodskii 2008, "LINEAR STABILITY OF
% THREE-DIMENSIONAL BOUNDARY LAYERS"
%
% Date: May 2018

clear intel options
addpath(genpath([getenv('DEKAF_DIRECTORY')]))

mPowers = [0,1/4,1/3,1/2,1,2,3,4];
Nm = length(mPowers);
for ii=1:Nm
    clear intel options;
% Optional inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.thermal_BC              = 'adiab';              % Wall bc
options.flow                    = 'CPG';                % flow assumption
options.modelTransport          = 'powerLaw';           % transport model
options.cstPr                   = true;                 % constant PRandtl number
options.specify_cp              = true;                 % user-specified cp
options.specify_gam             = true;                 % user-specified gamma
options.Cooke                   = true;                 % 3D boundary layer

options.tol                     = 1e-14;                % Convergence tolerance
options.it_max                  = 80;
options.T_tol                   = 1e-13;
options.T_itMax                 = 50;

options.plotRes                 = false;                % to plot the residual convergence


% Flow parameters
intel.M_e                       = 2;                    % Mach number at boundary-layer edge                        [-]
intel.T_e                       = 300;                  % Static temperature at boundary-layer edge                 [K]
intel.Re1_e                     = 10e6;
intel.cp                        = 1004.5;               % Constant specific heat                                    [J/(kg K)]
intel.gam                       = 1.4;                  % Constant specific heat ratio                              [-]
intel.Lambda_sweep              = 45;                   % sweep angle [deg]
intel.beta                      = 0.29;                 % pressure gradient [-]
intel.mPowerLaw                 = mPowers(ii);          % exponent in the viscosity power law
options.Pr                      = 0.72;                 % Constant specific heat ratio                              [-]

options.dimoutput               = true;                 % to output fields in dimensional form
options.dimXQSS                 = true;                 % we want to input the dimensional x location (not xi)
intel.x                         = 5000^2/intel.Re1_e;   % x location where we want the dimensional profiles (sqrt(Rex)=5000)

[intelOut,optionsOut] = DEKAF(intel,options);
allIntels{ii} = intelOut;
allOptions{ii} = optionsOut;
end

options.modelTransport = 'Sutherland';
% options.gifConv = true;
% options.fields4gif = {'df_deta','df_deta2','k','dk_deta','g','dg_deta'};
[intelOut,optionsOut] = DEKAF(intel,options);
allIntels{Nm+1} = intelOut;
allOptions{Nm+1} = optionsOut;
mPowers(Nm+1) = 0;

%% non-dimensionalizing like Gaponov

gap_u = load('gaponov2008_u.csv');
gap_10w = load('gaponov2008_-10w.csv');
gap_rho = load('gaponov2008_rho.csv');

clear plots legends;
figure;
for ii=1:Nm+1
U_e = allIntels{ii}.u(1);
u = allIntels{ii}.u/U_e;
w = allIntels{ii}.w/U_e;
[~,idx] = min(abs(u-0.95)); % closest location to delta95
delta95 = interp1(u(idx-1:idx+1),allIntels{ii}.y(idx-1:idx+1),0.95,'spline');
y = allIntels{ii}.y/delta95;
rho = allIntels{ii}.T_e./allIntels{ii}.T;

plots(ii) = plot(y,u,'-','Color',color_rainbow(ii,Nm+1)); hold on;
plot(y,-10*w,'--','Color',color_rainbow(ii,Nm+1));
plot(y,rho,':','linewidth',1.5,'Color',color_rainbow(ii,Nm+1));
legends{ii} = ['DEKAF m=',num2str(mPowers(ii))];
end
plots(Nm+2) = plot(gap_u(:,1),gap_u(:,2),'ks');
plots(Nm+3) = plot(gap_10w(:,1),gap_10w(:,2),'kx');
plots(Nm+4) = plot(gap_rho(:,1),gap_rho(:,2),'ko');
legends(Nm+1:Nm+4) = {'DEKAF Sutherland','Gaponov U','Gaponov -10 W','Gaponov \rho'};
legend(plots,legends); grid minor;
xlim([0,2]);
xlabel('y/\delta_{95} [-]'); ylabel('u/U_e, -10w/U_e, \rho/\rho_e [-]')