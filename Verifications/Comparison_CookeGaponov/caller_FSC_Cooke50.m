% caller_FSC_Cooke50 is a script to run the DEKAF solver to verify the 3D
% implementation against Cooke's 1950 paper "THE BOUNDARY LAYER OF A CLASS
% OF INFINITE YAWED CYLINDERS"
%
% Date: May 2018

clear intel options
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

beta_vec = [-0.1988,-0.1,0,0.2,0.6,1,1.6,2.0];
Nbeta = length(beta_vec);
for ii=1:Nbeta
    clear options intel;
% Optional inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.flow                    = 'CPG';                % flow assumption
options.Cooke                   = true;                 % 3D boundary layer

options.tol                     = 1e-14;                % Convergence tolerance
options.it_max                  = 80;
options.T_tol                   = 1e-13;
options.T_itMax                 = 50;

options.plotRes                 = false;                % to plot the residual convergence

% Flow parameters
intel.M_e                       = 1e-5;                 % Mach number at boundary-layer edge                        [-]
intel.beta                      = beta_vec(ii);                    % pressure gradient [-]
% None of these inputs really matter, since we are solving incompressible
intel.T_e                       = 300;                  % Static temperature at boundary-layer edge                 [K]
intel.Re1_e                     = 10e6;
intel.Lambda_sweep              = 45;                   % sweep angle [deg]

[intelOut,optionsOut] = DEKAF(intel,options);
allIntels{ii} = intelOut;
end

%% plotting
cooke = load('Cooke1950.m.csv');
clear plots legends;
figure
for ii=1:Nbeta
    plots(ii) = plot(allIntels{ii}.k,allIntels{ii}.eta,'Color',color_rainbow(ii,Nbeta)*0.8); hold on;
    legends{ii} = ['\beta_H = ',num2str(beta_vec(ii))];
    plots(Nbeta+ii) = plot(cooke(:,ii+1),cooke(:,1),'s','Color',color_rainbow(ii,Nbeta)*0.8);
end
legends{Nbeta+1} = 'Cooke 1950';
legend(plots(1:Nbeta+1),legends,'location','northwest'); grid minor;
xlabel('w/W0 [-]');
ylabel('\eta [-]');
ylim([0,6]); xlim([0,1.05]);

%% Saving for comparison
for ii=1:Nbeta
data2save(ii).kDKF = allIntels{ii}.k;
data2save(ii).dfdetaDKF = allIntels{ii}.df_deta;
data2save(ii).etaDKF = allIntels{ii}.eta;

data2save(ii).kCooke = cooke(:,ii+1);
data2save(ii).etaCooke = cooke(:,1);
end

% Saving
saveDEKAF('DKF_CPGCooke.mat','data2save','beta_vec');
