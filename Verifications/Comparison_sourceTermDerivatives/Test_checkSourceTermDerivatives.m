% Test_checkSourceTermDerivatives is a script to check the implementation
% of the derivatives of the various source terms.
%
% Author: Fernando Miro Miro
% Date: January 2020

clear;
addpath(genpath(getenv('DEKAF_DIRECTORY')));

% Setting options
mixture = 'air11Park91';
options.flow            = 'CNE';
options.numberOfTemp    = '2T';
options.mixture         = mixture;
[mixCnst,options] = getAll_constants(mixture,options);
flds = fieldnames(mixCnst);     for ii=1:length(flds)    eval([flds{ii},' = mixCnst.',flds{ii},';']);   end

% Test matrix
N = 2000;
Te      = 3000;         % [K]
Tve     = 2000;         % [K]
pe      = 1000;         % [Pa]
y_se    = [0.4,0.1,0.1,0.05,0.01,0.23,0.1-3e-10,1e-10,1e-10,0.01,1e-10];
y_se    = enforce_chargeNeutralityCond(enforce_concCond(y_se,1),bElectron,bPos,Mm_s,R0,'massFrac');

T_range = [300,10000];
Tv_range = T_range;
y_s_range = [0.01,0.8];
%{%
%% T derivatives
T   = linspace(T_range(1),T_range(2),N).';
Tv  = Tve*ones(N,1);
p   = pe*ones(N,1);
y_s = repmat(y_se,[N,1]);
DT = FDdif_nonEquis(T,'centered4');

% Auxiliary quantities
TTrans = T;     TRot = T;
TVib = Tv;      TElec = Tv;     Tel = Tv;
rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);

% Source terms
[source_s,~,~,source_sr] = getSpecies_source(TTrans,TRot,TVib,TElec,Tel,y_s,rho,Mm_s,nu_reac,nu_prod,A_r,nT_r,theta_r,qf2T_r,qb2T_r,Keq_struc,options);
[dsources_dT,dsourcesr_dT] =  getSpeciesDer_dsource_dT(TTrans,TRot,TVib,TElec,Tel,y_s,p,A_r,nT_r,theta_r,nu_prod,nu_reac,Mm_s,bElectron,R0,qf2T_r,qb2T_r,Keq_struc,options);

QTransRot2Vib     = getMixture_QTransRot2Vib(TTrans,TVib,Tel,y_s,p,rho,Mm_s,thetaVib_s,R_s,aVib_sl,bVib_sl,sigmaVib_s,bMol,bElectron,R0,nAvogadro,pAtm,options);
dQTransRot2Vib_dT = getMixtureDer_dQTransRot2Vib_dT(TTrans,TVib,Tel,y_s,p,rho,pAtm,R0,nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,R_s,bElectron,thetaVib_s,options.numberOfTemp,options);

Qel     = getMixture_Qel(       y_s,TTrans,Tel,rho,  Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,             source_sr,thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc);
dQel_dT = getMixtureDer_dQel_dT(y_s,TTrans,Tel,rho,p,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dT,source_sr,thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc,options.numberOfTemp);

sourcev = getMixture_sourcev(TTrans,TVib,TElec,Tel,y_s,p,rho,source_s,Mm_s,thetaVib_s,thetaElec_s,gDegen_s,R_s,aVib_sl,bVib_sl,sigmaVib_s,bMol,bElectron,R0,nAvogadro,pAtm,source_sr,hIon_s,kBoltz,qEl,epsilon0,consts_Omega11,bPos,bIonelim,bIonHeavyim,bDissel,bIonAssoc,options,'noQFormv');
dsourcev_dT = getMixtureDer_dsourcev_dT(TTrans,TVib,TElec,Tel,y_s,p,rho,source_s,dsources_dT,pAtm,R0,nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,R_s,bElectron,thetaVib_s,thetaElec_s,gDegen_s,bPos,epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dT,source_sr,hIon_s,bIonelim,bIonHeavyim,bDissel,bIonAssoc,options.numberOfTemp,options,'noQFormv');

% Plotting
figure; [Ni,Nj] = optimalPltSize(N_spec+3); ic=0;
for s=1:N_spec
    ic=ic+1; subplot(Ni,Nj,ic); loglog(T,abs(dsources_dT(:,s,1)),'r-',T,abs(DT*source_s(:,s)),'k--'); xlabel('T [K]'); ylabel('\partial\omega_s/\partialT [kg/s-K]'); title(['s = ',mixCnst.spec_list{s}]);
end
ic=ic+1; subplot(Ni,Nj,ic); loglog(T,abs(dQTransRot2Vib_dT(:,1)),'r-',T,abs(DT*QTransRot2Vib),'k--'); xlabel('T [K]'); ylabel('\partialQ^{TR-V}/\partialT [J/s-K]'); title('Trans.-Rot. to Vib');
ic=ic+1; subplot(Ni,Nj,ic); loglog(T,abs(dQel_dT(:,1)),          'r-',T,abs(DT*Qel),          'k--'); xlabel('T [K]'); ylabel('\partialQ^{el}/\partialT [J/s-K]');   title('electrons');
ic=ic+1; subplot(Ni,Nj,ic); loglog(T,abs(dsourcev_dT(:,1)),      'r-',T,abs(DT*sourcev),      'k--'); xlabel('T [K]'); ylabel('\partialQ^{TR-VEe}/\partialT [J/s-K]');   title('TR-VEe');
legend('Analytical','FD-4'); suptitle('T derivatives');

%% Tv derivatives
T   = Te*ones(N,1);
Tv  = linspace(Tv_range(1),Tv_range(2),N).';
p   = pe*ones(N,1);
y_s = repmat(y_se,[N,1]);
DTv = FDdif_nonEquis(Tv,'centered4');

% Auxiliary quantities
TTrans = T;     TRot = T;
TVib = Tv;      TElec = Tv;     Tel = Tv;
rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);

% Source terms
[source_s,~,~,source_sr] = getSpecies_source(TTrans,TRot,TVib,TElec,Tel,y_s,rho,Mm_s,nu_reac,nu_prod,A_r,nT_r,theta_r,qf2T_r,qb2T_r,Keq_struc,options);
[dsources_dT,dsourcesr_dT] =  getSpeciesDer_dsource_dT(TTrans,TRot,TVib,TElec,Tel,y_s,p,A_r,nT_r,theta_r,nu_prod,nu_reac,Mm_s,bElectron,R0,qf2T_r,qb2T_r,Keq_struc,options);

QTransRot2Vib     = getMixture_QTransRot2Vib(TTrans,TVib,Tel,y_s,p,rho,Mm_s,thetaVib_s,R_s,aVib_sl,bVib_sl,sigmaVib_s,bMol,bElectron,R0,nAvogadro,pAtm,options);
dQTransRot2Vib_dT = getMixtureDer_dQTransRot2Vib_dT(TTrans,TVib,Tel,y_s,p,rho,pAtm,R0,nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,R_s,bElectron,thetaVib_s,options.numberOfTemp,options);

Qel     = getMixture_Qel(       y_s,TTrans,Tel,rho,  Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,             source_sr,thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc);
dQel_dT = getMixtureDer_dQel_dT(y_s,TTrans,Tel,rho,p,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dT,source_sr,thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc,options.numberOfTemp);

sourcev = getMixture_sourcev(TTrans,TVib,TElec,Tel,y_s,p,rho,source_s,Mm_s,thetaVib_s,thetaElec_s,gDegen_s,R_s,aVib_sl,bVib_sl,sigmaVib_s,bMol,bElectron,R0,nAvogadro,pAtm,source_sr,hIon_s,kBoltz,qEl,epsilon0,consts_Omega11,bPos,bIonelim,bIonHeavyim,bDissel,bIonAssoc,options,'noQFormv');
dsourcev_dT = getMixtureDer_dsourcev_dT(TTrans,TVib,TElec,Tel,y_s,p,rho,source_s,dsources_dT,pAtm,R0,nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,R_s,bElectron,thetaVib_s,thetaElec_s,gDegen_s,bPos,epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dT,source_sr,hIon_s,bIonelim,bIonHeavyim,bDissel,bIonAssoc,options.numberOfTemp,options,'noQFormv');

% Plotting
figure; [Ni,Nj] = optimalPltSize(N_spec+3); ic=0;
for s=1:N_spec
    ic=ic+1; subplot(Ni,Nj,ic); loglog(Tv,abs(dsources_dT(:,s,2)),'r-',Tv,abs(DTv*source_s(:,s)),'k--'); xlabel('T_v [K]'); ylabel('\partial\omega_s/\partialT_v [kg/s-K]'); title(['s = ',mixCnst.spec_list{s}]);
end
ic=ic+1; subplot(Ni,Nj,ic); loglog(Tv,abs(dQTransRot2Vib_dT(:,2)),'r-',Tv,abs(DTv*QTransRot2Vib),'k--'); xlabel('T_v [K]'); ylabel('\partialQ^{TR-V}/\partialT_v [J/s-K]'); title('Trans.-Rot. to Vib');
ic=ic+1; subplot(Ni,Nj,ic); loglog(Tv,abs(dQel_dT(:,2)),          'r-',Tv,abs(DTv*Qel),          'k--'); xlabel('T_v [K]'); ylabel('\partialQ^{el}/\partialT_v [J/s-K]');   title('electrons');
ic=ic+1; subplot(Ni,Nj,ic); loglog(Tv,abs(dsourcev_dT(:,2)),      'r-',Tv,abs(DTv*sourcev),      'k--'); xlabel('T_v [K]'); ylabel('\partialQ^{TR-VEe}/\partialT_v [J/s-K]');   title('TR-VEe');
legend('Analytical','FD-4'); suptitle('T_v derivatives');
%}
%% y_s derivatives
for sDer = 1:N_spec
    T   = Te*ones(N,1);
    Tv  = Tve*ones(N,1);
    p   = pe*ones(N,1);
    y_s = repmat(y_se,[N,1]);
    if bElectron(sDer);     y_s(:,sDer) = 2.3e-5*linspace(y_s_range(1),y_s_range(2),N).'; % electron molar mass is 2.3e-5 times smaller than the average
    else;                   y_s(:,sDer) =        linspace(y_s_range(1),y_s_range(2),N).';
    end
    Dym = FDdif_nonEquis(y_s(:,sDer),'centered4');

    % Auxiliary quantities
    TTrans = T;     TRot = T;
    TVib = Tv;      TElec = Tv;     Tel = Tv;
    rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);

    % Source terms
    [source_s,~,~,source_sr]     = getSpecies_source(        TTrans,TRot,TVib,TElec,Tel,y_s,rho,Mm_s,nu_reac,nu_prod,A_r,nT_r,theta_r,qf2T_r,qb2T_r,Keq_struc,options);
    [dsources_dym,dsourcesr_dym] = getSpeciesDer_dsource_dym(TTrans,TRot,TVib,TElec,Tel,y_s,p,A_r,nT_r,theta_r,nu_prod,nu_reac,Mm_s,bElectron,R0,qf2T_r,qb2T_r,Keq_struc,options);

    QTransRot2Vib      = getMixture_QTransRot2Vib(TTrans,TVib,Tel,y_s,p,rho,Mm_s,thetaVib_s,R_s,aVib_sl,bVib_sl,sigmaVib_s,bMol,bElectron,R0,nAvogadro,pAtm,options);
    dQTransRot2Vib_dym = getMixtureDer_dQTransRot2Vib_dym(TTrans,TVib,Tel,y_s,p,rho,pAtm,R0,nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,R_s,bElectron,thetaVib_s,options);

    Qel      = getMixture_Qel(        y_s,TTrans,Tel,rho,  Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,source_sr,    thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc);
    dQel_dym = getMixtureDer_dQel_dym(y_s,TTrans,Tel,rho,p,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dym,thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc);

    sourcev = getMixture_sourcev(TTrans,TVib,TElec,Tel,y_s,p,rho,source_s,Mm_s,thetaVib_s,thetaElec_s,gDegen_s,R_s,aVib_sl,bVib_sl,sigmaVib_s,bMol,bElectron,R0,nAvogadro,pAtm,source_sr,hIon_s,kBoltz,qEl,epsilon0,consts_Omega11,bPos,bIonelim,bIonHeavyim,bDissel,bIonAssoc,options,'noQFormv');
    dsourcev_dym = getMixtureDer_dsourcev_dym(TTrans,TVib,TElec,Tel,y_s,p,rho,dsources_dym,pAtm,R0,nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,R_s,bElectron,thetaVib_s,thetaElec_s,gDegen_s,bPos,epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dym,hIon_s,bIonelim,bIonHeavyim,bDissel,bIonAssoc,options,'noQFormv');

    %% Plotting
    figure; [Ni,Nj] = optimalPltSize(N_spec+3); ic=0;
    for s=1:N_spec
        ic=ic+1; subplot(Ni,Nj,ic); loglog(y_s(:,sDer),abs(dsources_dym(:,sDer,s)),'r-',y_s(:,sDer),abs(Dym*source_s(:,s)),'k--'); xlabel(['y_{',mixCnst.spec_list{sDer},'} [-]']); ylabel(['\partial\omega_s/\partialy_{',mixCnst.spec_list{sDer},'} [kg/s]']); title(['s = ',mixCnst.spec_list{s}]);
    end
    ic=ic+1; subplot(Ni,Nj,ic); loglog(y_s(:,sDer),abs(dQTransRot2Vib_dym(:,sDer)),'r-',y_s(:,sDer),abs(Dym*QTransRot2Vib),'k--'); xlabel(['y_{',mixCnst.spec_list{sDer},'} [-]']); ylabel(['\partialQ^{TR-V}/\partialy_{',mixCnst.spec_list{sDer},'} [J/s]']); title('Trans.-Rot. to Vib');
    ic=ic+1; subplot(Ni,Nj,ic); loglog(y_s(:,sDer),abs(dQel_dym(:,sDer)),          'r-',y_s(:,sDer),abs(Dym*Qel),          'k--'); xlabel(['y_{',mixCnst.spec_list{sDer},'} [-]']); ylabel(['\partialQ^{el}/\partialy_{',mixCnst.spec_list{sDer},'} [J/s]']);   title('electrons');
    ic=ic+1; subplot(Ni,Nj,ic); loglog(y_s(:,sDer),abs(dsourcev_dym(:,sDer)),      'r-',y_s(:,sDer),abs(Dym*sourcev),      'k--'); xlabel(['y_{',mixCnst.spec_list{sDer},'} [-]']); ylabel(['\partialQ^{TR-VEe}/\partialy_{',mixCnst.spec_list{sDer},'} [J/s]']);   title('TR->VEe');
    legend('Analytical','FD-4'); suptitle(['y_s(',mixCnst.spec_list{sDer},') derivatives']);
end
