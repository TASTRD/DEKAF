#include "mutation++.h"
#include <iostream>

int main(int argc, char* argv[])
{
// Load air5 mixture
Mutation::Mixture mix(“air5”);
// Print the species
std::cout << "Species: ";
for (auto& s : mix.species())
std::cout << s.name() << ' ';
std::cout << '\n';
return 0;
}
