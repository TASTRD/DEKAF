/* This script is meant to output T vs Tv tables with different
 * properties for a chosen composition
 *
 * Author: Fernando Miro Miro
 * Date: June 2018
 */

#include "mutation++.h"
#include <iostream>


void printProductionRates(Mutation::Mixture& mix)
{
    std::vector<double> wdot(mix.nSpecies());
    mix.netProductionRates(wdot.data());
    for (int i = 0; i < mix.nSpecies(); ++i) {
        std::cout << std::setw(15) << wdot[i];
    }
}

// This example starts setting equilibrium, it obtains the equilibrium composition, and then tests it in CNE (no production) and in CNE with slightly different temperature (away from equilibrium)

int main(int argc, char* argv[])
{
    // Create the mixture
    Mutation::MixtureOptions options("air5");
    options.setStateModel("ChemNonEqTTv");

    Mutation::Mixture mix(options);
    mix.addComposition("N2:0.79, O2:0.21", true);

    // Equilibrate the solution to T and P
    double P = Mutation::ONEATM; // used to get the equilibrium species densities at this pres. & temp.
    double T = 6000.0;
    mix.equilibrate(T, P);

    // allocating
    double Tmin = 500.0;
    double Tmax = 10000.0;
	double dT = 50;
	int NT = (Tmax-Tmin)/dT;
    std::vector<double> Tvec(2); // defining temperature vectors
    std::vector<double> rho_i(mix.nSpecies());
    mix.densities(rho_i.data());

	for (int iT = 0; iT <= NT; iT++){
		for (int jT = 0; jT < NT; jT++){
			Tvec[0] = Tmin+iT*dT;
			Tvec[1] = Tmin+iT*dT;
			mix.setState(rho_i.data(), Tvec.data(), 1);
			std::cout << Tvec[0] << Tvec[1];
			printProductionRates(mix);
			std::cout << "\n";
		}
	}

    return 0;
}
