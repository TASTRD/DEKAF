% caller_DEKAF runs the SS solver for the test on the ideal-gas equation of
% state.
%
% Author: Fernando Miro Miro
% April 2020


clear; clc; close all;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
% M_e = 13.5;         % [-]           % JFM-ioniz. case
% T_e = 3000;         % [K]
% Tw  = 10000;        % [K]
% p_e = 1e4;          % [Pa]
U_e = 10;           % [m/s]       % Lani's case
T_e = 5000;         % [K]
Tw  = 1000;         % [K]
p_e = 1e6;          % [Pa]
x   = 2;            % [m]
% y_Ow = 0.8;         % quantity of oxygen at the wall
flow_vec = {'LTE','LTEED'};
Nf = length(flow_vec);

% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size (in non-dimensional eta length)
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 5e-13;                % Convergence tolerance
options.it_max                  = 100;                   % maximum number of iterations

% flow options
% options.thermal_BC              = 'adiab';              % Wall bc
options.thermal_BC              = 'Twall';              % Wall bc (isothermal)
options.G_bc                    = Tw;                   % value of the wall temperature
% options.wallBlowing_BC          = 'SS';                 % self-similar blowing
% options.F_bc                    = -0.1;                 % self-similar blowing parameter
options.wallYE_BC               = 'zeroYEgradient';     % null elemental fraction gradient at the wall
% options.wallYE_BC               = 'yEWall';             % constant elemental fraction at the wall
% options.YE_bc                   = {1-y_Ow,y_Ow};

% options.passElementalFractions  = true;
% intel.yE_e = {0.7,0.3};

% options.mixture                 = 'air11mutation';              % mixture
options.mixture                 = 'air5mutation';               % mixture
options.modelTransport          = 'CE_122';                     % transport model
% options.modelDiffusion          = 'FOCE_RamshawAmbipolar';      % diffusion model
options.modelCollisionNeutral   = 'Capitelli98Stallcop96';      % collision model

options.BLproperties = true;

% display options
options.plotRes = false;                         % we don't want the residual plot

% flat plate after a wedge
options.dimoutput = true;
options.dimXQSS = true;

% stagnation point
% intel.beta = 1; % stagnation point
% options.dimoutput = false;

% flow conditions
intel.U_e = U_e;  % streamwise velocity at boundary-layer edge    [m/s]
% intel.M_e = M_e;  % Mach number at boundary-layer edge            [-]
intel.T_e = T_e;  % Temperature at boundary-layer edge            [K]
intel.p_e = p_e;  % Pressure at boundary-layer edge               [Pa]
intel.x   = x;    % dimensional streamwise location to evaluate   [m]

% Executing DEKAF
for ii=Nf:-1:1
    options.flow = flow_vec{ii};
    [intel_all{ii},options_all{ii}] = DEKAF(intel,options);
end

% Saving
saveDEKAF('DKF_equilCompare.mat','intel_all','options_all','flow_vec');

%% plotting
[Ni,Nj] = optimalPltSize(4+intel_all{end}.mixCnst.N_spec+intel_all{end}.mixCnst.N_elem);
% [Ni,Nj] = optimalPltSize(4+intel_all{end}.mixCnst.N_spec+1);
% elem2plt = 'O';
% Ni=2; Nj=5;
styleS = {'-','-.','--'};
for ii=1:Nf
    elem_list = intel_all{ii}.mixCnst.elem_list;
    spec_list = intel_all{ii}.mixCnst.spec_list;
    clr = two_colors(ii,Nf+1,'Rbw2');
    wall = intel_all{ii}.y;     wallName = 'y [m]';     wallMax = max(cellfun(@(cll)max(cll.y_i),intel_all));
    u = intel_all{ii}.u;                                T = intel_all{ii}.T;
    v = intel_all{ii}.v;                                rho = intel_all{ii}.rho;
%     wall = intel_all{ii}.eta;   wallName = '\eta [-]';  wallMax = 6;
%     u = intel_all{ii}.df_deta * intel_all{ii}.U_e;      T = intel_all{ii}.j * intel_all{ii}.T_e;
    ip=0;
ip=ip+1;subplot(Ni,Nj,ip); plot(u,  wall,styleS{ii},'Color',clr,'displayname',flow_vec{ii}); hold on;
                xlabel('u [m/s]');      ylabel(wallName); grid minor; ylim([0,wallMax]); title('Streamwise velocity');
ip=ip+1;subplot(Ni,Nj,ip); plot(v,  wall,styleS{ii},'Color',clr,'displayname',flow_vec{ii}); hold on;
                xlabel('v [m/s]');      ylabel(wallName); grid minor; ylim([0,wallMax]); title('Wall-normal velocity');
ip=ip+1;subplot(Ni,Nj,ip); plot(T,  wall,styleS{ii},'Color',clr,'displayname',flow_vec{ii}); hold on;
                xlabel('T [K]');        ylabel(wallName); grid minor; ylim([0,wallMax]); title('Temperature');
ip=ip+1;subplot(Ni,Nj,ip); plot(rho,wall,styleS{ii},'Color',clr,'displayname',flow_vec{ii}); hold on;
                xlabel('\rho [kg/m^3]');ylabel(wallName); grid minor; ylim([0,wallMax]); title('Density');
%     E = find(strcmp(elem_list,elem2plt));
    for E=1:length(elem_list)
ip=ip+1;subplot(Ni,Nj,ip); plot(intel_all{ii}.yE{E},wall,styleS{ii},'Color',clr,'displayname',flow_vec{ii}); hold on;
                xlabel(['Y_{',elem_list{E},'} [-]']); ylabel(wallName); grid minor; ylim([0,wallMax]); title([elem_list{E},' elemental mass fraction']);
    end
    for s=1:intel_all{ii}.mixCnst.N_spec
ip=ip+1;subplot(Ni,Nj,ip); plot(intel_all{ii}.ys{s},wall,styleS{ii},'Color',clr,'displayname',flow_vec{ii}); hold on;
                xlabel(['Y_{',spec_list{s},'} [-]']); ylabel(wallName); grid minor; ylim([0,wallMax]); title([spec_list{s},' species mass fraction']);
    end
end
legend();
