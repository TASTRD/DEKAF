% Test_LTEED2CNE is a script to initialize a CNE simulation with an LTEED
% one, and see how it develops. If it does not change, it means that the
% LTEED implementation is indeed the limit to which CNE tends.
%
% Author: Fernando Miro Miro
% Date: April 2020

clear; clc; %close all;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']));

% defining parameters
p_e     = 10000;        % [Pa]
T_e     = 700;          % [K]
% M_e     = 20;           % [-]
M_e     = 10;           % [-]
x0      = 1;            % [m]
% x1      = 2;    xSpacing = 'tanh';  % [m]
x1      = 100;  xSpacing = 'log';   % [m]
trc     = 1e-10;        % [-] species trace

N       = 100;          % [-]
L       = 20;           % [-]
eta_i   = 6;            % [-]
N_x     = 50;           % [-]

thermal_BC = 'adiab';       Tw = [];
% mixture = 'air5mutation';
% mixture = 'mars8Mortensen';
mixtures = {'air5mutation','mars8Mortensen'};

modelTransport = 'BlottnerEucken';
modelDiffusion = 'FOCE_Ramshaw';

%% Common options
options.N                       = N;                     % Number of points
options.L                       = L;                     % Domain size
options.eta_i                   = eta_i;                 % Mapping parameter, eta_crit
options.thermal_BC              = thermal_BC;            % Wall bc
options.G_bc                    = Tw;                    % wall temperature
options.modelTransport          = modelTransport;        % transport model
options.modelDiffusion          = modelDiffusion;        % diffusion model

options.tol                     = 1e-10;                 % Convergence tolerance
options.it_max                  = 100;
options.T_tol                   = 1e-10;                 % tolerance of the solver for the temperature
options.T_itMax                 = 50;
% options.LTE_NR_relax            = 1;
options.Fields4conv             = {'df_deta3','dg_deta2'};

options.plot_transient          = false;                 % to plot the inter-iteration profiles
options.plotRes                 = false;                 % to plot the residual convergence
options.max_residual            = 1e-2;
% options.enthalpyOffset          = false;

% options.gifMarch                = true;
options.quiet                   = true;

% Flow parameters
intel.M_e                       = M_e;                   % Mach number at boundary-layer edge                        [-]
intel.T_e                       = T_e;                   % Static temperature at boundary-layer edge                 [K]
intel.p_e                       = p_e;                   % Absolute static pressure at boundary-layer edge           [Pa]
% intel.M_e0                      = M_e;                   % Mach number at boundary-layer edge                        [-]
% intel.T_e0                      = T_e;                   % Static temperature at boundary-layer edge                 [K]
% intel.p_e0                      = p_e;                   % Absolute static pressure at boundary-layer edge           [Pa]
% options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';
% options.inviscidMethod          = 'FD';

% Marching options (CNE-exclusive)
options.xSpacing        = xSpacing;
options.mapOpts.x_start = x0;
options.mapOpts.x_end   = x1;
options.mapOpts.N_x     = N_x ;
options.ox              = 2;

%% Looping
for iM=1%:length(mixtures)
if isfield(intel,'ys_e')
    intel = rmfield(intel,'ys_e');
end
options.mixture                 = mixtures{iM};          % mixture
%% LTEED
options.restartFromMarching = false;
options.flow = 'LTEED'; % flow assumption
[intelLTEED,optionsLTEED] = DEKAF(intel,options);

%% CNE from LTEED
options.restartFromMarching = true;
options.intelQSS            = intelLTEED;
options.flow                = 'CNE';
options.enhanceChemistry    = 1e6;

% options.gifConv                 = true;
% options.gifLogPlot              = true;
% options.xlim                    = [1e-12,1];

intel.ys_e = intelLTEED.ys_e;
[intelCNE1,optionsCNE1] = DEKAF(intel,options,'marching');

%% Populating structure for plotting
DKFLTEED(iM).yE   = cellfun(@(cll)cll(:,1),intelCNE1.yE,'UniformOutput',false);
DKFCNE(iM).yE     = cellfun(@(cll)cll(:,5:5:N_x),intelCNE1.yE,'UniformOutput',false);
DKFLTEED(iM).eta  = intelCNE1.eta;
DKFCNE(iM).eta    = intelCNE1.eta;
DKFCNE(iM).x      = intelCNE1.x(1,[5:5:N_x]);
DKFCNE(iM).elem_list = intelCNE1.mixCnst.elem_list;

%% LTE
%{
options.restartFromMarching = false;
options.flow                = 'LTE'; % flow assumption

[intelLTE,optionsLTE] = DEKAF(intel,options);

%% CNE from LTE
options.restartFromMarching = true;
options.intelQSS            = intelLTE;
options.flow                = 'CNE';

[intelCNE2,optionsCNE2] = DEKAF(intel,options,'marching');
%}
%% Plotting comparison
N_spec = intelCNE1.mixCnst.N_spec;      spec_list = intelCNE1.mixCnst.spec_list;
N_elem = intelCNE1.mixCnst.N_elem;      elem_list = intelCNE1.mixCnst.elem_list;
Nx2plt = 20; Np=N_elem;  % Np=3+N_spec+N_elem; 
idx_x = round(linspace(1,optionsCNE1.mapOpts.separation_i_xi,Nx2plt));
xplot = intelCNE1.x(1,idx_x);
rndFact = 100;
yE_max = ceil( rndFact*cellfun(@(cll)max(cll(:)),intelCNE1.yE))/rndFact;
yE_min = floor(rndFact*cellfun(@(cll)min(cll(:)),intelCNE1.yE))/rndFact;
figure;
% [Ni,Nj] = optimalPltSize(Np);
Ni=1; Nj=Np;
styleS = repmat({{'-'},{'-.'},{'--'},{':','linewidth',1.5}},[1,Nx2plt]);
for ii = 1:length(xplot)
    [~,ixi1] = min(abs(intelCNE1.x(1,:)-xplot(ii)));
    name1 = ['x = ',num2str(intelCNE1.x(1,ixi1)),' m'];
    clr1 = two_colors(ii,Nx2plt,'Rbw2');
    stl1 = [styleS{ii},{'Color',clr1,'displayname',name1}];
    yVar1 = intelCNE1.eta;       yMax = optionsCNE1.eta_i;
    ip=0;
%     ip=ip+1;subplot(Ni,Nj,ip); plot(intelCNE1.u(:,ixi1),yVar1,stl1{:}); hold on;
%     ip=ip+1;subplot(Ni,Nj,ip); plot(intelCNE1.T(:,ixi1),yVar1,stl1{:}); hold on;
%     ip=ip+1;subplot(Ni,Nj,ip); plot(intelCNE1.h(:,ixi1),yVar1,stl1{:}); hold on;
%     for s=1:N_spec
%     ip=ip+1;subplot(Ni,Nj,ip); plot(intelCNE1.ys{s}(:,ixi1),yVar1,stl1{:}); hold on;
%     end
    for E=1:N_elem
    ip=ip+1;subplot(Ni,Nj,ip); plot(intelCNE1.yE{E}(:,ixi1),yVar1,stl1{:}); hold on;
    end
end
ip=0;
% ip=ip+1;subplot(Ni,Nj,ip); xlabel('u [m/s]');                       ylabel('\eta [-]'); title('velocity');      grid minor; ylim([0,yMax]);
% ip=ip+1;subplot(Ni,Nj,ip); xlabel('T [K]');                         ylabel('\eta [-]'); title('temperature');   grid minor; ylim([0,yMax]);
% ip=ip+1;subplot(Ni,Nj,ip); xlabel('h [J/kg]');                      ylabel('\eta [-]'); title('enthalpy');      grid minor; ylim([0,yMax]);
% for s=1:N_spec
% ip=ip+1;subplot(Ni,Nj,ip); xlabel(['Y_{',spec_list{s},'} [-]']);    ylabel('\eta [-]'); title([spec_list{s},' species fraction']);      grid minor; ylim([0,yMax]);
% end
for E=1:N_elem
ip=ip+1;subplot(Ni,Nj,ip); xlabel(['Y_{',elem_list{E},'} [-]']);    ylabel('\eta [-]'); title([elem_list{E},' elemental fraction']);    grid minor; ylim([0,yMax]); xlim([yE_min(E),yE_max(E)]);
end
legend(); suptitle('Initializing from LTEED');
end

saveDEKAF('data4verification_LTEED.mat','DKFLTEED','DKFCNE','mixtures');

%% Plotting comparison
%{
figure;
for ii = 1:length(xplot)
    [~,ixi2] = min(abs(intelCNE2.x(1,:)-xplot(ii)));
    name1 = ['x = ',num2str(intelCNE2.x(1,ixi2)),' m'];
    clr1 = two_colors(ii,Nx2plt,'Rbw2');
%     yVar = intelCNE2.y(:,ixi1);     yMax = intelCNE2.y_i(end);
    yVar = intelCNE2.eta;           yMax = optionsCNE2.eta_i;
    ip=0;
%     ip=ip+1;subplot(Ni,Nj,ip); plot(intelCNE2.u(:,ixi2),yVar,styleS{ii}{:},'Color',clr1,'displayname',name1); hold on;
%     ip=ip+1;subplot(Ni,Nj,ip); plot(intelCNE2.T(:,ixi2),yVar,styleS{ii}{:},'Color',clr1,'displayname',name1); hold on;
%     ip=ip+1;subplot(Ni,Nj,ip); plot(intelCNE2.h(:,ixi2),yVar,styleS{ii}{:},'Color',clr1,'displayname',name1); hold on;
%     for s=1:N_spec
%     ip=ip+1;subplot(Ni,Nj,ip); plot(intelCNE2.ys{s}(:,ixi2),yVar,styleS{ii}{:},'Color',clr1,'displayname',name1); hold on;
%     end
    for E=1:N_elem
    ip=ip+1;subplot(Ni,Nj,ip); plot(intelCNE2.yE{E}(:,ixi2),yVar,styleS{ii}{:},'Color',clr1,'displayname',name1); hold on;
    end
end
ip=0;
% ip=ip+1;subplot(Ni,Nj,ip); xlabel('u [m/s]');                       ylabel('\eta [-]'); title('velocity');      grid minor; ylim([0,yMax]);
% ip=ip+1;subplot(Ni,Nj,ip); xlabel('T [K]');                         ylabel('\eta [-]'); title('temperature');   grid minor; ylim([0,yMax]);
% ip=ip+1;subplot(Ni,Nj,ip); xlabel('h [J/kg]');                      ylabel('\eta [-]'); title('enthalpy');      grid minor; ylim([0,yMax]);
% for s=1:N_spec
% ip=ip+1;subplot(Ni,Nj,ip); xlabel(['Y_{',spec_list{s},'} [-]']);    ylabel('\eta [-]'); title([spec_list{s},' species fraction']);      grid minor; ylim([0,yMax]);
% end
for E=1:N_elem
ip=ip+1;subplot(Ni,Nj,ip); plot(intelLTEED.yE{E},   intelLTEED.eta,'k--','displayname','LTEED');
xlabel(['Y_{',elem_list{E},'} [-]']);    ylabel('\eta [-]'); title([elem_list{E},' elemental fraction']);    grid minor; ylim([0,yMax]); xlim([yE_min(E),yE_max(E)]);
end
legend(); suptitle('Initializing from LTE');
%}