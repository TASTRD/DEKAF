% Test_elementalDiffusionCoeffs is a script to compare the elemental
% diffusion coefficients obtained with various elemental compositions.
%
% Author: Fernando Miro Miro
% Date: April 2020

clear; close all
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']));

%%% INPUTS
T = (2000:10:15000).';                        % temperatures [K]
% T = (3000:1:10000).';                       % temperatures [K]
method           = 'mtimesx';               % resolution method for the equilibrium system
mixture          = 'air5mutation';          % mixture
yE_e             = {0.77, 0.23};            % edge concentrations
folder           = 'data_Rini2005';         % air-5 reference data
% mixture          = 'air11mutation';         % mixture
% yE_e             = {0,0.77, 0.23};          % edge concentrations
% folder           = 'data_Rini2007';         % air-11 reference data
modelEquilibrium = 'RRHO';                  % equilibrium model
modelDiffusion   = 'FOCE_StefanMaxwell';    % diffusion model
modelCollision_list = {'Wright2005','Stuckert91','GuptaYos90','Capitelli98','Capitelli98Stallcop96'};
% modelCollision_list = {'Capitelli98Stallcop96'};
flow             = 'LTEED';                 % flow assumption
trc     = 1e-10;                            % trace concentration
p       =        101325*ones(size(T));      % pressures [Pa]
%%% END OF INPUTS

% Reference data
Rini_A_O  = load([folder,'/Rini_A_O.csv']);
Rini_2C_O = load([folder,'/Rini_2C_O.csv']);

% Computing auxiliaries
NT = length(T);                             % number of temperatures
% Np = length(yN_vec);                        % number of atomic concentrations
Np = length(modelCollision_list);           % number of collisional models
mixtureMUT = regexprep(mixture,'^([a-z|A-Z]+)([0-9]+).*$','$1$2');

%% Running
for ip=Np:-1:1
    options.mixture             = mixture;                  % populating options
    options.modelEquilibrium    = modelEquilibrium;
    options.modelDiffusion      = modelDiffusion;
    options.modelCollisionNeutral = modelCollision_list{ip};
    options.flow                = flow;
    options.T_tol               = 1e-11;
    [mixCnst,options] = getAll_constants(mixture,options);  % populating mixture constants
    for s=mixCnst.N_spec:-1:1
        idxEl = find(strcmp(mixCnst.elem_list,mixCnst.spec_list{s}));
        if isempty(idxEl);      ys_e{s} = 0;
        else;                   ys_e{s} = yE_e{idxEl};
        end
    end
    [mixCnst.LTEtablePath,mixCnst.XEq] = checkCompositionLTE(ys_e,mixCnst.Mm_s,mixCnst.R0,options.mixture,options);
    mixCnst.LTEtableData = load(mixCnst.LTEtablePath);
    % Numerical derivatives
    X_s{ip} = getEquilibrium_X_complete(p,T,method,mixture,mixCnst,modelEquilibrium,0,options);
    % thermal diffusion coefficient
    X_E = repmat(mixCnst.XEq,[NT,1]);
    [outputs{1:38}] = getAll_properties(mixCnst,[],p,X_E,T,[],options,'guessXsLTE',X_s{ip});
    A_E{ip}  = outputs{34};
    C_EF{ip} = outputs{35};
end

%% Plotting elemental diffusion coefficients
styleS = repmat({'-','-.','--',':'},[1,Np]);
figure;
[Ni,Nj] = optimalPltSize(2);
TMax = max(Rini_A_O(:,1));
for ip=1:Np
    name = modelCollision_list{ip};
    idxO = find(strcmp(mixCnst.elem_list,'O'));
    idxN = find(strcmp(mixCnst.elem_list,'N'));
    Ctot = (C_EF{ip}(:,idxO,idxO) - C_EF{ip}(:,idxO,idxN));
    clr = two_colors(ip,Np,'Rbw2');
    ic=0;
    ic=ic+1; subplot(Ni,Nj,ic); plot(T,  A_E{ip}(:,idxO),styleS{ip},'Color',clr,'displayname',name); hold on;
    ic=ic+1; subplot(Ni,Nj,ic); plot(T,  Ctot,           styleS{ip},'Color',clr,'displayname',name); hold on;
end
ic=0; refStyle = 'ks'; name=['Rini et al. (',folder(end-3:end),')'];
ic=ic+1; subplot(Ni,Nj,ic); plot(Rini_A_O(:,1),   Rini_A_O(:,2), refStyle,'displayname',name); hold on;
ic=ic+1; subplot(Ni,Nj,ic); plot(Rini_2C_O(:,1),  Rini_2C_O(:,2),refStyle,'displayname',name); hold on;
ic=0;
ic=ic+1; subplot(Ni,Nj,ic); xlabel('T [K]'); ylabel('A_O [kg/m-s-K]');          title('Thermal diffusion coefficient');         xlim([0,TMax]);
ic=ic+1; subplot(Ni,Nj,ic); xlabel('T [K]'); ylabel('C_{OO}-C_{ON} [kg/m-s]');  title('Elemental mass diffusion coefficient');  xlim([0,TMax]); legend();

%% Plotting elemental diffusion coefficients against mutation
%{
N_elem = mixCnst.N_elem;
A_E_mut_cll  = mppequilWrap([min(T),mean(diff(T)),max(T)],p(1),[],[],mixtureMUT,'-o',6,N_elem);
C_EF_mut_cll = mppequilWrap([min(T),mean(diff(T)),max(T)],p(1),[],[],mixtureMUT,'-o',7,N_elem*N_elem);
A_E_mut = -[A_E_mut_cll{1},A_E_mut_cll{2}];
C_EF_mut = cat(3,[C_EF_mut_cll{1},C_EF_mut_cll{2}],[C_EF_mut_cll{3},C_EF_mut_cll{4}]);
styleS = repmat({'-','-.','--',':'},[1,Np]);
figure;
[Ni,Nj] = optimalPltSize(2);
TMax = max(Rini_A_O(:,1));
for ip=1:Np
    name = modelCollision_list{ip};
    idxO = find(strcmp(mixCnst.elem_list,'O'));
    idxN = find(strcmp(mixCnst.elem_list,'N'));
    Ctot = (CX_EF{ip}(:,idxO,idxO) - CX_EF{ip}(:,idxO,idxN));
    clr = two_colors(ip,Np,'Rbw2');
    ic=0;
    ic=ic+1; subplot(Ni,Nj,ic); plot(T,  A_E{ip}(:,idxO),styleS{ip},'Color',clr,'displayname',name); hold on;
    ic=ic+1; subplot(Ni,Nj,ic); plot(T,  Ctot,           styleS{ip},'Color',clr,'displayname',name); hold on;
end
ic=0; refStyle = 'kx'; name='mutation++';
stp=10;
Ctot_mut = (C_EF_mut(:,idxO,idxO) - C_EF_mut(:,idxO,idxN));
ic=ic+1; subplot(Ni,Nj,ic); plot(T(1:stp:end),  A_E_mut(1:stp:end,idxO), refStyle,'displayname',name); hold on;
ic=ic+1; subplot(Ni,Nj,ic); plot(T(1:stp:end),  Ctot_mut(1:stp:end),     refStyle,'displayname',name); hold on;
ic=0;
ic=ic+1; subplot(Ni,Nj,ic); xlabel('T [K]'); ylabel('A_O [kg/m-s-K]');          title('Thermal diffusion coefficient');         xlim([0,TMax]);
ic=ic+1; subplot(Ni,Nj,ic); xlabel('T [K]'); ylabel('C_{OO}-C_{ON} [kg/m-s]');  title('Elemental mass diffusion coefficient');  xlim([0,TMax]); legend();
%}
%%
%{
figure;
Ni=2;Nj=2;ic=0;
for E=1:mixCnst.N_elem
    for F=1:mixCnst.N_elem
        ic=ic+1; subplot(Ni,Nj,ic);
        for ip=1:Np
            name = modelCollision_list{ip};
            clr = two_colors(ip,Np,'Rbw2');
            plot(T,  C_EF{ip}(:,E,F),styleS{ip},'Color',clr,'displayname',name); hold on;
        end
        refStyle = 'kx'; name='mutation++';
        plot(T(1:stp:end),  C_EF_mut(1:stp:end,E,F),refStyle,'displayname',name); hold on;
        refStyle = 'ks'; name='Rini et al. (2005)';
        plot(Rini_2C_O(:,1),Rini_2C_O(:,2),refStyle,'displayname',name); hold on;
        xlabel('T [K]'); ylabel(['C_{',mixCnst.elem_list{E},',',mixCnst.elem_list{F},'} [kg/m-s]']); title('Elemental mass diffusion coefficient');  xlim([0,TMax]); title([mixCnst.elem_list{E},' - ',mixCnst.elem_list{F}]);
    end
end
legend(); 
%}