% caller_blowingCPG is a script to run the DEKAF solver to compare against
% CPG basic-state flowfields with wall blowing.
%
% Date: November 2018

clear;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
M_e     = 4.5;
T_e     = 61.61;    % K
rho_e   = 1.899e-2; % Pa
Tw      = 311;      % K
Re      = 1500;     % doesn't really change anything, because we are then plotting non-dimensional variables
fw_vec  = [0,-0.2,-0.4];

Nf = length(fw_vec);

for ii=1:Nf
% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 5e-13;                % Convergence tolerance
options.it_max                  = 40;                   % maximum number of iterations
options.T_tol                   = 5e-13;                % convergence tolerance for the NR to get T from the enthalpy
options.T_itMax                 = 200;                  % maximum number of iterations for the NR to get T from the enthalpy

% flow options
options.thermal_BC              = 'Twall';              % Wall bc (g or dg/deta)
options.G_bc                    = Tw;                   % wall temperature
options.flow                    = 'CPG';                % flow assumption
options.mixture                 = 'air2';               % mixture
options.modelTransport          = 'Sutherland';         % transport model
options.wallBlowing_BC          = 'SS';                 % self-similar blowing
options.F_bc                    = fw_vec(ii);           % blowing parameter
options.specify_cp              = true;                 % user-specified heat capacity
options.specify_gam             = true;                 % user-specified heat capacity ratio
options.cstPr                   = true;                 % constant Prandtl number
options.dimoutput               = true;                 % we want dimensional profiles
options.fixReQSS                = true;                 % fix a Reynolds number where to evaluate the dimensional profiles

intel.Re                        = Re;                   % [-]
intel.cp                        = 1004.5;               % [J/kg-K]
intel.gam                       = 1.4;                  % [-]
options.Pr                      = 0.72;                 % [-]

% display options
options.plotRes = false;                        % we don't want the residual plot

% flow conditions
intel.M_e               = M_e;                  % Mach number at boundary-layer edge        [-]
intel.T_e               = T_e;                  % temperature at boundary-layer edge        [K]
intel.rho_e             = rho_e;                % density at boundary-layer edge            [kg/m^3]

[intel_all{ii},options_all{ii}] = DEKAF(intel,options);
end

%% Reference files
ic=1;
filesGT{ic} = load('images_Ghaffari/Temperature_profile_Ghaffari_fw0.0.csv');
filesG{ic}  = load('images_Ghaffari/Velocity_profile_Ghaffari_fw0.0.csv'); ic=ic+1;

filesGT{ic} = load('images_Ghaffari/Temperature_profile_Ghaffari_fw-0.2.csv');
filesG{ic}  = load('images_Ghaffari/Velocity_profile_Ghaffari_fw-0.2.csv'); ic=ic+1;

filesGT{ic} = load('images_Ghaffari/Temperature_profile_Ghaffari_fw-0.4.csv');
filesG{ic}  = load('images_Ghaffari/Velocity_profile_Ghaffari_fw-0.4.csv'); ic=ic+1;

StyleS = {'-','-.','--'}; y_lim = intel_all{ii}.y_i/intel_all{ii}.lBlasius; clear legends plots;
figure
for ii=1:Nf
    legends{ii} = ['f_w = ',num2str(fw_vec(ii))];
    subplot(1,3,1);
    plots(ii) = plot(intel_all{ii}.u/intel_all{ii}.U_e,intel_all{ii}.y/intel_all{ii}.lBlasius,StyleS{ii},'Color',two_colors(ii,Nf,'mgm')); hold on;
    plots(Nf+1) = plot(filesG{ii}(:,2),filesG{ii}(:,1),'ko');
    xlabel('$$u/u_e$$ [-]','interpreter','latex'); ylabel('$$y /\ell$$ [-]','interpreter','latex'); box on; grid minor;
    ylim([0,y_lim]);

    subplot(1,3,2);
    plot(intel_all{ii}.T/intel_all{ii}.T_e,intel_all{ii}.y/intel_all{ii}.lBlasius,StyleS{ii},'Color',two_colors(ii,Nf,'mgm')); hold on;
    plot(filesGT{ii}(:,2),filesGT{ii}(:,1),'ko');
    xlabel('$$T/T_e$$ [-]','interpreter','latex'); ylabel('$$y /\ell$$ [-]','interpreter','latex'); box on; grid minor;
    ylim([0,y_lim]);

    subplot(1,3,3);
    plot(intel_all{ii}.v/intel_all{ii}.U_e,intel_all{ii}.y/intel_all{ii}.lBlasius,StyleS{ii},'Color',two_colors(ii,Nf,'mgm')); hold on;
    xlabel('$$v/u_e$$ [-]','interpreter','latex'); ylabel('$$y /\ell$$ [-]','interpreter','latex'); box on; grid minor;
    ylim([0,y_lim]);
end
legends{Nf+1} = 'Ghaffari';
legend(plots,legends);

%% Preparing to export
for ii=1:length(fw_vec)
    data2save(ii).yDKF = intel_all{ii}.y;
    data2save(ii).uDKF = intel_all{ii}.u;
    data2save(ii).vDKF = intel_all{ii}.v;
    data2save(ii).TDKF = intel_all{ii}.T;

    data2save(ii).yuGaf = filesG{ii}(:,1)  * intel_all{ii}.lBlasius;
    data2save(ii).uGaf  = filesG{ii}(:,2)  * intel_all{ii}.U_e;
    data2save(ii).yTGaf = filesGT{ii}(:,1) * intel_all{ii}.lBlasius;
    data2save(ii).TGaf  = filesGT{ii}(:,2) * intel_all{ii}.T_e;
end

saveDEKAF('DKF_blowingGhaffari.mat','data2save','fw_vec');