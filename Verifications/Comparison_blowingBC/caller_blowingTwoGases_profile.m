% caller_blowingTwoGases_profile is a script to run the DEKAF solver to perform
% a comparison of the results obtained by blowing different gases with the
% wall-profile blowing BC on a wedge.
%
% Author: Fernando Miro Miro
% Date: December 2018

clear;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
M_e     = 6;
T_e     = 300;      % [K]
p_e     = 4000;     % [Pa]
T_w     = 300;      % [K]
x_max   = 0.5;      % [m]
angle   = 7;        % [deg]
m_blow  = 0.01;     % [kg/s-m^2]
x_blow  = 0.1;      % [m]
Nx      = 50;
trc     = 1e-5;     % [-]

Nbc                 = 1001;
x_bc                = linspace(0,x_max,Nbc).';
[~,idx_blow]        = min(abs(x_bc - x_blow));
% mw_bc               = zeros(Nbc,1);
% mw_bc(idx_blow:end) = m_blow;
% mw_bc               = m_blow * ones(Nbc,1);
mw_bc               = linspace(0,m_blow,Nbc);
yAir_w              = trc*ones(Nbc,1);
% yAir_w              = linspace(1-trc,trc,Nbc).';

NT = 1000;
Tvec = linspace(200,2000,NT).';

mixtures = {'Air-He','Air-Ne','Air','Air-Ar','Air-CO2'};

Nm = length(mixtures);

for jj=1:Nm
    clear options intel;
    % Solver inputs
    options.N                       = 100;                  % Number of points
    options.L                       = 40;                   % Domain size
    options.eta_i                   = 6;                    % Mapping parameter, eta_crit
    options.tol                     = 5e-13;                % Convergence tolerance
    options.it_max                  = 40;                   % maximum number of iterations
    options.T_tol                   = 5e-13;                % convergence tolerance for the NR to get T from the enthalpy
    options.T_itMax                 = 200;                  % maximum number of iterations for the NR to get T from the enthalpy

    % flow options
    options.thermal_BC              = 'Twall';              % Wall bc (g or dg/deta)
    options.G_bc                    = T_w;                  % wall temperature
    options.flow                    = 'TPGD';               % flow assumption
    options.mixture                 = mixtures{jj};         % mixture
    options.modelTransport          = 'SutherlandWilke';    % transport model
    options.modelDiffusion          = 'cstSc';              % diffusion model
    options.wallBlowing_BC          = 'mwProfile';          % blowing boundary condition
    options.F_bc                    = mw_bc;                % blowing parameter at the wall
    options.wallYs_BC               = 'yswProfile';         % constant mass-fraction BC
    options.x_bc                    = x_bc;

    options.shockJump               = true;
%     options.coordsys                = 'cone';
%     intel.cone_angle                = angle;                % [deg]
    intel.wedge_angle               = angle;                % [deg]

    if strcmp(mixtures{jj},'Air')
        options.Ys_bc = {ones(Nbc,1)};
        ys_e = {1};
    else
        options.Ys_bc = {yAir_w,1-yAir_w};
        ys_e = {1-trc,trc};
    end

    % display options
    options.plotRes = false;                        % we don't want the residual plot
    options.MarchingGlobalConv = false;             % nor the marching convergence plot

% options.gifMarch = true;
options.fields4gifMarch = {'tau','df_deta','ys'};
options.xlim = [1e-12,3];
options.ylim = [0,10];

    % flow conditions
    options.inviscidFlowfieldTreatment = 'constant';
    intel.M_e0              = M_e;                  % Mach number at boundary-layer edge        [-]
    intel.T_e0              = T_e;                  % temperature at boundary-layer edge        [K]
    intel.p_e0              = p_e;                  % pressure at boundary-layer edge           [Pa]
    intel.ys_e0             = ys_e;
    options.xSpacing        = 'tanh';
    options.mapOpts.x_start = 1e-7;
    options.mapOpts.x_end   = x_max;
    [intel_all{jj},options_all{jj}] = DEKAF(intel,options,'marching');
end

%% Reference files
StyleS = {'-','-.','--',':'}; y_lim = intel_all{1,1}.y_i(end); clear plots all_plots legends;
pltFlag = 'Rbw2';
[Ni,Nj] = optimalPltSize(6);
idx_plt = Nx;
figure
for jj=1:Nm
    ic=1;
    clrVec = two_colors(jj,Nm,pltFlag);

    subplot(Ni,Nj,ic);ic=ic+1;
    plot(intel_all{jj}.u(:,idx_plt),intel_all{jj}.y(:,idx_plt),'Color',clrVec,'displayname',[mixtures{jj}]); hold on;
    xlabel('$$u$$ [m/s]','interpreter','latex'); ylabel('$$y$$ [m]','interpreter','latex'); box on; grid minor; legend();
    ylim([0,y_lim]);

    subplot(Ni,Nj,ic);ic=ic+1;
    plot(intel_all{jj}.v(:,idx_plt),intel_all{jj}.y(:,idx_plt),'Color',clrVec); hold on;
    xlabel('$$v$$ [m/s]','interpreter','latex'); ylabel('$$y$$ [m]','interpreter','latex'); box on; grid minor;
    ylim([0,y_lim]);

    subplot(Ni,Nj,ic);ic=ic+1;
    plot(intel_all{jj}.T(:,idx_plt),intel_all{jj}.y(:,idx_plt),'Color',clrVec); hold on;
    xlabel('$$T$$ [K]','interpreter','latex'); ylabel('$$y$$ [m]','interpreter','latex'); box on; grid minor;
    ylim([0,y_lim]);

    subplot(Ni,Nj,ic);ic=ic+1;
    plot(intel_all{jj}.ys{1}(:,idx_plt),intel_all{jj}.y(:,idx_plt),'Color',clrVec); hold on;
    xlabel('$$Y_{Air}$$ [-]','interpreter','latex'); ylabel('$$y$$ [m]','interpreter','latex'); box on; grid minor;
    ylim([0,y_lim]);

    subplot(Ni,Nj,ic);ic=ic+1;
    plot(intel_all{jj}.x(1,:),intel_all{jj}.v(end,:),'Color',clrVec); hold on;
    xlabel('$$x$$ [m]','interpreter','latex'); ylabel('$$v_w$$ [m/s]','interpreter','latex'); box on; grid minor;
%     ylim([0,1]);

    subplot(Ni,Nj,ic);ic=ic+1;
    plot(intel_all{jj}.x(1,:),intel_all{jj}.rho(end,:),'Color',clrVec); hold on;
    xlabel('$$x$$ [m]','interpreter','latex'); ylabel('$$\rho_w$$ [kg/m\textsuperscript{3}]','interpreter','latex'); box on; grid minor;
%     ylim([0,1]);

end