% caller_blowingTwoGases_collisions is a script to run the DEKAF solver to
% perform a comparison of the results obtained by blowing different gases
% with the SS blowing BC on a wedge. The transport and diffusion models use
% collisional data, rather than Sutherland's law with Wilke's mixing rule
% and cstSc.
%
% Author: Fernando Miro Miro
% Date: December 2018

clear;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
M_e     = 6;
T_e     = 300;      % [K]
p_e     = 4000;     % [Pa]
T_w     = 300;      % [K]
x       = 0.5;      % [m]
angle   = 7;        % [deg]
fw      = -0.2;

NT = 1000;
Tvec = linspace(200,2000,NT).';

mixtures = {'air2','air2-Ar','air2-CO2'};
modelsTransport = {'CE_122','SutherlandWilke'};
modelsDiffusion = {'FOCE_StefanMaxwell','cstSc'};
names = {'CE-SM','Suth-cst Sc'};
yAir_w = 1e-5;

Nth = length(modelsDiffusion);
Nm = length(mixtures);

for jj=Nm:-1:1
    for ii=1:Nth
        clear options intel;
        if ~(~strcmp(mixtures{jj},'air2') && fw==0)
            % Solver inputs
            options.N                       = 100;                  % Number of points
            options.L                       = 40;                   % Domain size
            options.eta_i                   = 6;                    % Mapping parameter, eta_crit
            options.tol                     = 5e-13;                % Convergence tolerance
            options.it_max                  = 40;                   % maximum number of iterations
            options.T_tol                   = 5e-13;                % convergence tolerance for the NR to get T from the enthalpy
            options.T_itMax                 = 200;                  % maximum number of iterations for the NR to get T from the enthalpy

            % flow options
            options.thermal_BC              = 'Twall';              % Wall bc (g or dg/deta)
            options.G_bc                    = T_w;                  % wall temperature
            options.flow                    = 'TPGD';               % flow assumption
            options.mixture                 = mixtures{jj};         % mixture
            options.modelTransport          = modelsTransport{ii};  % transport model
            options.modelDiffusion          = modelsDiffusion{ii};  % diffusion model
            options.wallBlowing_BC          = 'SS';                 % blowing boundary condition
            options.F_bc                    = fw;               % blowing parameter at the wall
            options.wallYs_BC               = 'ysWall';             % constant mass-fraction BC

            options.shockJump               = true;
%             options.coordsys                = 'cone';
%             intel.cone_angle                = angle;                % [deg]
            intel.wedge_angle               = angle;                % [deg]
            options.dimoutput               = true;                 % we want dimensional profiles
            options.dimXQSS                 = true;                 % and we are providing a dimensional x
            intel.x = x;

            if strcmp(mixtures{jj},'air2')
                options.Ys_bc = {0.2329,0.7671};
            else
                options.Ys_bc = {yAir_w*0.2329,yAir_w*0.7671,1-yAir_w};
            end

            % display options
            options.plotRes = false;                        % we don't want the residual plot
            options.MarchingGlobalConv = false;             % nor the marching convergence plot

            % flow conditions
            intel.M_e               = M_e;                  % Mach number at boundary-layer edge        [-]
            intel.T_e               = T_e;                  % temperature at boundary-layer edge        [K]
            intel.p_e               = p_e;                  % pressure at boundary-layer edge           [Pa]
            [intel_all{ii,jj},options_all{ii,jj}] = DEKAF(intel,options);

    % Computing properties
    mixCnst = intel_all{ii,jj}.mixCnst;
    y_s = cell1D2matrix2D(cellfun(@(cll)repmat(cll,[NT,1]),options.Ys_bc,'UniformOutput',false));
    p = ones(NT,1) * p_e;
    rho = getMixture_rho(y_s,p,Tvec,Tvec,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron);
    Mm  = getMixture_Mm_R(y_s, Tvec,Tvec,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron);
    [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
        bElectron_mat,hForm_s_mat,T_mat] = reshape_inputs4enthalpy(mixCnst.R_s,...
        mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,Tvec);
    [~,cp] = getMixture_h_cp(y_s,T_mat,T_mat,T_mat,T_mat,T_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options_all{ii,jj});
    [mu{ii,jj},kappa{ii,jj}] = getTransport_mu_kappa(y_s,Tvec,Tvec,rho,p,Mm,options_all{ii,jj},mixCnst,Tvec,Tvec,Tvec,cp);
    Pr{ii,jj} = mu{ii,jj}.*cp./kappa{ii,jj};
        end
    end
end

%% Reference files
StyleS = {'-','-.','--',':'}; y_lim = intel_all{1,1}.y_i; clear plots all_plots legends;
pltFlag = 'Rbw2';
[Ni,Nj] = optimalPltSize(5);
figure
for jj=1:Nm
    for ii=1:Nth
        ic=1;
        if ~(~strcmp(mixtures{jj},'Air') && fw==0)
            clrVec = two_colors(jj,Nm,pltFlag);

            subplot(Ni,Nj,ic);ic=ic+1;
            plot(intel_all{ii,jj}.u,intel_all{ii,jj}.y,StyleS{ii},'Color',clrVec,'displayname',[mixtures{jj},', ',names{ii}]); hold on;
            xlabel('$$u$$ [m/s]','interpreter','latex'); ylabel('$$y$$ [m]','interpreter','latex'); box on; grid minor; legend();
            ylim([0,y_lim]);

            subplot(Ni,Nj,ic);ic=ic+1;
            plot(intel_all{ii,jj}.v,intel_all{ii,jj}.y,StyleS{ii},'Color',clrVec); hold on;
            xlabel('$$v$$ [m/s]','interpreter','latex'); ylabel('$$y$$ [m]','interpreter','latex'); box on; grid minor;
            ylim([0,y_lim]);

            subplot(Ni,Nj,ic);ic=ic+1;
            plot(intel_all{ii,jj}.T,intel_all{ii,jj}.y,StyleS{ii},'Color',clrVec); hold on;
            xlabel('$$T$$ [K]','interpreter','latex'); ylabel('$$y$$ [m]','interpreter','latex'); box on; grid minor;
            ylim([0,y_lim]);

            subplot(Ni,Nj,ic);ic=ic+1;
            plot(intel_all{ii,jj}.ys{2},intel_all{ii,jj}.y,StyleS{ii},'Color',clrVec); hold on;
            xlabel('$$Y_{N_2}$$ [-]','interpreter','latex'); ylabel('$$y$$ [m]','interpreter','latex'); box on; grid minor;
            ylim([0,y_lim]);

%     subplot(Ni,Nj,ic);ic=ic+1;
%     plot(Tvec,mu{ii,jj},StyleS{ii},'Color',clrVec); hold on;
%     xlabel('$$T$$ [K]','interpreter','latex'); ylabel('$$\mu$$ [kg/m-s]','interpreter','latex'); box on; grid minor;
%
%     subplot(Ni,Nj,ic);ic=ic+1;
%     plot(Tvec,kappa{ii,jj},StyleS{ii},'Color',clrVec); hold on;
%     xlabel('$$T$$ [K]','interpreter','latex'); ylabel('$$\kappa$$ [W/m-K]','interpreter','latex'); box on; grid minor;

    subplot(Ni,Nj,ic);ic=ic+1;
    plot(Tvec,Pr{ii,jj},StyleS{ii},'Color',clrVec); hold on;
    xlabel('$$T$$ [K]','interpreter','latex'); ylabel('$$Pr$$ [-]','interpreter','latex'); box on; grid minor;


        end
    end
end