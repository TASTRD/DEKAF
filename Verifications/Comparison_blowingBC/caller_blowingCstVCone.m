% caller_blowingCstVCone is a script to run the DEKAF solver to perform
% a comparison of the results obtained with the constant-V blowing BC on a
% cone.
%
% Author: Fernando Miro Miro
% Date: December 2018

clear;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
M_e     = 6;
T_e     = 300;      % [K]
p_e     = 4000;     % [Pa]
cp      = 1004.5;   % [J/kg-K]
gam     = 1.4;      % [-]
Pr      = 0.72;     % [-]
T_w     = 400;      % [K]
V_vec   = 0.5:0.5:1.5;% [m/s]
x_min   = 1e-7;     % [m]
x_max   = 0.2;      % [m]
Nxe     = 100;      % number of streamwise pointsç
cone_angle = 7;     % [deg]

% computing common properties
x_e     = linspace(x_min*0.9,x_max*1.1,Nxe);    % [m]
R       = (1-1/gam)*cp;                         % [J/kg-K]
rho_e   = p_e/(R*T_e);                          % [kg/m^3]
rho_w   = p_e/(R*T_w);                          % [kg/m^3]
U_e     = M_e*sqrt(T_e*R*gam);                  % [m/s]

Nf = length(V_vec);
vec1 = ones(1,Nxe);

for ii=1:Nf
    clear options intel;
    % Solver inputs
    options.N                       = 100;                  % Number of points
    options.L                       = 40;                   % Domain size
    options.eta_i                   = 6;                    % Mapping parameter, eta_crit
    options.tol                     = 5e-13;                % Convergence tolerance
    options.it_max                  = 40;                   % maximum number of iterations
    options.T_tol                   = 5e-13;                % convergence tolerance for the NR to get T from the enthalpy
    options.T_itMax                 = 200;                  % maximum number of iterations for the NR to get T from the enthalpy
    options.coordsys = 'cone';

    % flow options
    options.thermal_BC              = 'Twall';              % Wall bc (g or dg/deta)
    options.G_bc                    = T_w;                  % wall temperature
    options.flow                    = 'CPG';                % flow assumption
    options.mixture                 = 'air2';               % mixture
    options.modelTransport          = 'Sutherland';         % transport model
    options.wallBlowing_BC          = 'VwProfile';          % blowing boundary condition
    options.specify_cp              = true;                 % user-specified heat capacity
    options.specify_gam             = true;                 % user-specified heat capacity ratio
    options.cstPr                   = true;                 % constant Prandtl number
    options.dimoutput               = true;                 % we want dimensional profiles

    intel.cp                        = cp;                   % [J/kg-K]
    intel.gam                       = gam;                  % [-]
    options.Pr                      = Pr;                   % [-]
    intel.cone_angle                = cone_angle;           % [deg]
    options.F_bc = V_vec(ii)*vec1;                                        % wall-normal velocity profile [m/s]
    options.x_bc = x_e;

%     [mixCnst,options4mu] = getAll_constants(options.mixture,options); % obtaining mixCnst to evaluate mu_e
%     mu_e = getTransport_mu_kappa([],T_e,T_e,[],[],[],options4mu,mixCnst); % empty inputs are not needed for Sutherland's law

    % display options
    options.plotRes = false;                        % we don't want the residual plot
    options.MarchingGlobalConv = false;             % nor the marching convergence plot

    % flow conditions
    intel.M_e               = M_e*vec1;             % Mach number at boundary-layer edge        [-]
    intel.T_e               = T_e*vec1;             % temperature at boundary-layer edge        [K]
    intel.p_e0              = p_e;                  % pressure at boundary-layer edge           [Pa]
    intel.x_e               = x_e;                  % streamwise coordinate corresponding to the previous profiles [m]
    options.inviscidFlowfieldTreatment = 'constant';

    options.mapOpts.N_x     = Nxe;                  % number of streamwise positions
    options.xSpacing        = 'tanh';               % linear xi spacing
    options.mapOpts.x_start = x_min;                % minimum x to be swept                     [m]
    options.mapOpts.x_end   = x_max;                % maximum x to be swept                     [m]
    [intel_all{ii},options_all{ii}] = DEKAF(intel,options,'marching');

end

%% Reference files
ic=1;
StyleS = {'-','-.','--'}; y_lim = options_all{end}.eta_i; clear plots all_plots legends;
idx_2plt = round(linspace(1,Nxe,11)); idx_2plt(1) = [];
x_2plt = intel_all{1}.x(1,idx_2plt);
Nx2plt = length(idx_2plt);
pltFlags = {'KR','GB','Rbw','mgm','vrds'};
figure
for ii=1:3
    for jj=1:Nx2plt

        [~,idx_jj] = min(abs(intel_all{ii}.x(1,:) - x_2plt(jj)));
        legends{jj} = ['x = ',num2str(x_2plt(jj)),'-',num2str(intel_all{ii}.x(1,idx_jj)),' m'];
        clrVec = two_colors(jj,Nx2plt,pltFlags{ii});

    subplot(1,3,1);
    all_plots(ii,jj) = plot(intel_all{ii}.u(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
    xlabel('$$u$$ [m/s]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
    ylim([0,y_lim]);

    subplot(1,3,2);
    plot(intel_all{ii}.T(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
    xlabel('$$T$$ [K]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
    ylim([0,y_lim]);

    subplot(1,3,3);
    plot(intel_all{ii}.v(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
    xlabel('$$v$$ [m/s]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
    ylim([0,y_lim]);
    end
    if ii~=1
    legends{Nx2plt+ii-1} = ['V_w = ',num2str(V_vec(ii)),' m/s'];
    end
end
plots = [all_plots(1,:),all_plots(2:end,1).'];
legend(plots,legends);

figure
for ii=1:3
plot(intel_all{ii}.x(end,:),intel_all{ii}.v(end,:),StyleS{ii},'Color',two_colors(ii,Nf,'mgm'),'displayname',['V_w = ',num2str(V_vec(ii)),' m/s']); hold on;
end
xlabel('$$x$$ [m]','interpreter','latex'); ylabel('$$v_w$$ [m/s]','interpreter','latex'); box on;
legend();