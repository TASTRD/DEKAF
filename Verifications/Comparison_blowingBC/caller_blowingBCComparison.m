% caller_blowingBCComparison is a script to run the DEKAF solver to perform
% a comparison of the results obtained with the several blowing BCs.
%
% Author: Fernando Miro Miro
% Date: November 2018

clear;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'_stable/SourceCode']))

% Flow conditions
M_e     = 6;
T_e     = 300;      % [K]
p_e     = 4000;     % [Pa]
cp      = 1004.5;   % [J/kg-K]
gam     = 1.4;      % [-]
Pr      = 0.72;     % [-]
T_w     = 400;      % [K]
f_w     = -0.1;
x_min   = 1e-3;     % [m]
x_max   = 0.25;     % [m]
Nxe     = 1000;     % number of streamwise points of the input profiles
Nx      = 50;       % number of streamwise points for the BL evaluation

BC_vec = {'SS','Vwall','mwall','VwProfile','mwProfile'};

% computing common properties
x_e     = linspace(x_min*0.9,x_max*1.1,Nxe);    % [m]
R       = (1-1/gam)*cp;                         % [J/kg-K]
rho_e   = p_e/(R*T_e);                          % [kg/m^3]
rho_w   = p_e/(R*T_w);                          % [kg/m^3]
U_e     = M_e*sqrt(T_e*R*gam);                  % [m/s]

Nf = length(BC_vec);

for ii=1:Nf
    clear options intel;
    % Solver inputs
    options.N                       = 100;                  % Number of points
    options.L                       = 40;                   % Domain size
    options.eta_i                   = 6;                    % Mapping parameter, eta_crit
    options.tol                     = 5e-13;                % Convergence tolerance
    options.it_max                  = 40;                   % maximum number of iterations
    options.T_tol                   = 5e-13;                % convergence tolerance for the NR to get T from the enthalpy
    options.T_itMax                 = 200;                  % maximum number of iterations for the NR to get T from the enthalpy

    % flow options
    options.thermal_BC              = 'Twall';              % Wall bc (g or dg/deta)
    options.G_bc                    = T_w;                  % wall temperature
    options.flow                    = 'CPG';                % flow assumption
    options.mixture                 = 'air2';               % mixture
    options.modelTransport          = 'Sutherland';         % transport model
    options.wallBlowing_BC          = BC_vec{ii};           % blowing boundary condition
    options.specify_cp              = true;                 % user-specified heat capacity
    options.specify_gam             = true;                 % user-specified heat capacity ratio
    options.cstPr                   = true;                 % constant Prandtl number
    options.dimoutput               = true;                 % we want dimensional profiles

    intel.cp                        = cp;                   % [J/kg-K]
    intel.gam                       = gam;                  % [-]
    options.Pr                      = Pr;                   % [-]

    [mixCnst,options4mu] = getAll_constants(options.mixture,options); % obtaining mixCnst to evaluate mu_e
    mu_e = getTransport_mu_kappa([],T_e,T_e,[],[],[],options4mu,mixCnst); % empty inputs are not needed for Sutherland's law
    switch BC_vec{ii}
        case 'SS'                                                       % for the self-similar BC
            options.F_bc = f_w;                                             % simply f_w
        case 'Vwall'                                                    % we must compute V at the wall
            options.F_bc = -sqrt(rho_e*mu_e*U_e/(2*x_max))*f_w/rho_w;       % wall-normal velocity [m/s]
        case 'VwProfile'                                                % we must compute V profile at the wall (SS)
            options.F_bc = -sqrt(rho_e*mu_e*U_e./(2*x_e))*f_w/rho_w;         % wall-normal velocity profile [m/s]
        case 'mwall'                                                    % we must compute the mass-flow at the wall
            options.F_bc = -sqrt(rho_e*mu_e*U_e/(2*x_max))*f_w;             % wall-normal massflow [kg/s-m^2]
        case 'mwProfile'                                                % we must compute the mass-flow profile at the wall (SS)
            options.F_bc = -sqrt(rho_e*mu_e*U_e./(2*x_e))*f_w;               % wall-normal massflow profile [kg/s-m^2]
        otherwise
            error(['Error: the chosen BC_vec ''',BC_vec{ii},''' is not supported']);
    end


    % display options
    options.plotRes = false;                        % we don't want the residual plot
    options.MarchingGlobalConv = false;             % nor the marching convergence plot

    % flow conditions
    switch BC_vec{ii}
        case {'SS','Vwall','mwall'} % SS solver
            intel.M_e               = M_e;                  % Mach number at boundary-layer edge        [-]
            intel.T_e               = T_e;                  % temperature at boundary-layer edge        [K]
            intel.p_e               = p_e;                  % pressure at boundary-layer edge           [Pa]

            options.dimXQSS         = true;                 % fix an x location where to evaluate the dimensional profile
            intel.x                 = x_max;                % [-]
            [intel_all{ii},options_all{ii}] = DEKAF(intel,options);
        case {'VwProfile','mwProfile'} % marching solver
            vec1 = ones(1,Nxe);
            intel.M_e               = M_e*vec1;             % Mach number at boundary-layer edge        [-]
            intel.T_e               = T_e*vec1;             % temperature at boundary-layer edge        [K]
            intel.p_e0              = p_e;                  % pressure at boundary-layer edge           [Pa]
            intel.x_e               = x_e;                  % streamwise coordinate corresponding to the previous profiles [m]
            options.inviscidFlowfieldTreatment = 'constant';

            options.mapOpts.N_x     = Nx;                   % number of streamwise positions
            options.xSpacing        = 'linear';             % linear xi spacing
            options.mapOpts.x_start = x_min;                % minimum x to be swept                     [m]
            options.mapOpts.x_end   = x_max;                % maximum x to be swept                     [m]
            [intel_all{ii},options_all{ii}] = DEKAF(intel,options,'marching');
        otherwise
            error(['Error: the chosen BC_vec ''',BC_vec{ii},''' is not supported']);
    end

end

%% Reference files
ic=1;
StyleS = {'-','-.','--','-.','--'}; y_lim = intel_all{ii}.y_i(end); clear plots;
pltFlag = 'Rbw';
figure
for ii=1:Nf
    subplot(1,3,1);
    plots(ii) = plot(intel_all{ii}.u(:,end),intel_all{ii}.y(:,end),StyleS{ii},'Color',0.8*two_colors(ii,Nf,pltFlag)); hold on;
    xlabel('$$u$$ [m/s]','interpreter','latex'); ylabel('$$y$$ [m]','interpreter','latex'); box on; grid minor;
    ylim([0,y_lim]);

    subplot(1,3,2);
    plot(intel_all{ii}.T(:,end),intel_all{ii}.y(:,end),StyleS{ii},'Color',0.8*two_colors(ii,Nf,pltFlag)); hold on;
    xlabel('$$T$$ [K]','interpreter','latex'); ylabel('$$y$$ [m]','interpreter','latex'); box on; grid minor;
    ylim([0,y_lim]);

    subplot(1,3,3);
    plot(intel_all{ii}.v(:,end),intel_all{ii}.y(:,end),StyleS{ii},'Color',0.8*two_colors(ii,Nf,pltFlag)); hold on;
    xlabel('$$v$$ [m/s]','interpreter','latex'); ylabel('$$y$$ [m]','interpreter','latex'); box on; grid minor;
    ylim([0,y_lim]);
end
legend(plots,BC_vec);