% Test_DEKAFvsCFDpp_CPGcone is a script to compare the basic state obtained
% from solving the boundary-layer equations, undoing the Probstein-Elliot
% and with edge values coming from solving the Tayor-Maccoll equation.
%
% Author: Fernando Miro Miro
% Date: March 2018

clear;
x_init = 0.05; % [m]
cone_angle = 7; % [deg]

%% Extracting tecplot data
addpath(genpath([getenv('VESTA_DIRECTORY'),'/VESTA_code']));
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']));

file_name_ff = '../../../6.Pieter_experiments/tecplot_files/QUATEC_Re15a_mesh8.dat'; % path of the file containing the flow field from Tecplot
file_name_w = '../../../6.Pieter_experiments/tecplot_files/SURFTEC_Re15a_mesh8.dat'; % path of the file containing the wall info from Tecplot
% Retrieving the information
opts2.display = false; % options structure just to silence the plotting
[Data_ff,Data_w] = save_CFD_Data(file_name_ff,file_name_w,opts2); % VESTA function
%% Extracting the orthogonal lines to the surface
[X,Y,P,PX,PY,T,TX,TY,U,V,UX,UY,VX,VY] = extract_ortholine_eN(Data_ff,Data_w,cone_angle,x_init,opts2); % VESTA function
rot_mat = [cosd(cone_angle) , sind(cone_angle);...
          -sind(cone_angle) , cosd(cone_angle)];

xy = (rot_mat*[X(:),Y(:)].').';
uv = (rot_mat*[U(:),V(:)].').';
Txy = (rot_mat*[TX(:),TY(:)].').';
xRot = reshape(xy(:,1),size(X));
yRot = reshape(xy(:,2),size(X));
uRot = reshape(uv(:,1),size(X));
vRot = reshape(uv(:,2),size(X));
TyRot = reshape(Txy(:,2),size(X));

uyRot = UY*cosd(cone_angle)^2 - UX*sind(cone_angle)*cosd(cone_angle) + VY*cosd(cone_angle)*sind(cone_angle) - VX*sind(cone_angle)^2 ;
vyRot = -UY*cosd(cone_angle)*sind(cone_angle) + UX*sind(cone_angle)^2 + VY*cosd(cone_angle)^2 - VX*sind(cone_angle)*cosd(cone_angle);

%% RUNNING DEKAF
clear intel options

x_plot = [0.05,0.1,0.15,0.2,0.25];
ylims = [0,0.001];
options.dimXQSS = true;
for ix=length(x_plot):-1:1
    [~,idx_tcplt(ix)] = min(abs(xRot(1,:)-x_plot(ix)));
end

% U_e = U(int16(end/2),end); % using tecplot post-shock conditions
% T_e = T(int16(end/2),end);
% p_e = P(1,idx_tcplt);
% options.shockJump = false;

U_e = U(end,end); % using pre-shock conditions in conjunction with Taylor-Maccoll
T_e = T(end,end);
p_e = P(end,end);
options.shockJump = true;

Tw = T(1,1);
cone_angle = 7; % [deg]

% Optional inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.thermal_BC              = 'Twall';              % Wall bc (g or dg/deta)
options.G_bc                    = Tw;               % tecplot wall temperature
options.flow                    = 'CPG';                % flow assumption
options.modelTransport          = 'Sutherland';
options.coordsys = 'cone';
intel.cone_angle = cone_angle;
options.plotRes = false;
intel.U_e               = U_e;
intel.T_e               = T_e;
intel.p_e               = p_e;

[intel_QSS,options] = DEKAF(intel,options);

intel_QSS.x = x_plot;
intel_dimVars = eval_dimVars(intel_QSS,options);

%% Ludo's method
%{
Nx = 51;
Ny = 10001;
x_vec = linspace(x_plot-0.01,x_plot+0.01,Nx);
dx = mean(diff(x_vec));
for ix = 1:Nx
intel_QSS.x = x_vec(ix);
intelprov = eval_dimVars(intel_QSS,options);
rhou(:,ix) = intelprov.u.*intelprov.rho;
rho(:,ix) = intelprov.rho;
y(:,ix) = intelprov.y;
end
y_interp = linspace(0,max(y(:)),Ny).'; % interpolated vector
dy = mean(diff(y_interp));
rhou_interp = zeros(Ny,Nx);
rho_interp = zeros(Ny,Nx);
for ix = 1:Nx
rhou_interp(:,ix) = interp1(y(:,ix),rhou(:,ix),y_interp,'spline');
rho_interp(:,ix) = interp1(y(:,ix),rho(:,ix),y_interp,'spline');
end
Dx = -FDdif(Nx,dx,'centered');
Dy = FDdif(Ny,dy,'forward6');
Iy = integral_mat(Dy,'ud');

r = repmat(x_vec,[Ny,1])*sind(cone_angle) + repmat(y_interp,[1,Nx])*cosd(cone_angle);
drhou_dx = (Dx*(rhou_interp).').';
v_cartes = -1./(rho_interp).*(Iy*drhou_dx);

drhou_dx = (Dx*(r.*rhou_interp).').';
v_cylind = -1./(r.*rho_interp).*(Iy*drhou_dx);
%}
% Plotting
figure
subplot(2,3,1);
plot(intel_dimVars.u,intel_dimVars.y,'k'); hold on; grid minor;
plot(uRot(:,idx_tcplt),yRot(:,idx_tcplt),'r--');
xlabel('u [m/s]'); ylabel('y [m]');
ylim(ylims);

subplot(2,3,2);
plot(intel_dimVars.v,intel_dimVars.y,'k'); hold on; grid minor;
plot(vRot(:,idx_tcplt),yRot(:,idx_tcplt),'r--');
% plot(v_cartes(:,ceil(Nx/2)),y_interp,'b-.');
% plot(v_cylind(:,ceil(Nx/2)),y_interp,'c-.');
legend('Probstein-Elliot transf.'...
    ,'CFD++ Navier-Stokes'...
    ...,'cartesian \psi cond.'...
    ...,'cylindrical \psi cond.'...
    );
xlabel('v [m/s]'); ylabel('y [m]');
ylim(ylims);

subplot(2,3,3);
plot(intel_dimVars.T,intel_dimVars.y,'k'); hold on; grid minor;
plot(T(:,idx_tcplt),yRot(:,idx_tcplt),'r--');
xlabel('T [K]'); ylabel('y [m]');
ylim(ylims);

subplot(2,3,4);
plot(intel_dimVars.du_dy,intel_dimVars.y,'k'); hold on; grid minor;
plot(uyRot(:,idx_tcplt),yRot(:,idx_tcplt),'r--');
xlabel('\partialu/\partialy [1/s]'); ylabel('y [m]');
ylim(ylims);

subplot(2,3,5);
plot(intel_dimVars.dv_dy,intel_dimVars.y,'k'); hold on; grid minor;
plot(vyRot(:,idx_tcplt),yRot(:,idx_tcplt),'r--');
xlabel('\partialv/\partialy [1/s]'); ylabel('y [m]');
ylim(ylims);

subplot(2,3,6);
plot(intel_dimVars.dT_dy,intel_dimVars.y,'k'); hold on; grid minor;
plot(TyRot(:,idx_tcplt),yRot(:,idx_tcplt),'r--');
xlabel('\partialT/\partialy [K/m]'); ylabel('y [m]');
ylim(ylims);
legend('DEKAF Taylor-Maccoll + BL','CFD++ Navier-Stokes');

%% Saving for comparison
Nx = length(x_plot);
for ii=1:Nx
data2save(ii).yDKF = intel_dimVars.y(:,ii);
data2save(ii).uDKF = intel_dimVars.u(:,ii);
data2save(ii).vDKF = intel_dimVars.v(:,ii);
data2save(ii).TDKF = intel_dimVars.T(:,ii);

data2save(ii).yCFD = yRot(:,idx_tcplt(ii));
data2save(ii).uCFD = uRot(:,idx_tcplt(ii));
data2save(ii).vCFD = vRot(:,idx_tcplt(ii));
data2save(ii).TCFD = T(:,idx_tcplt(ii));
end

% Saving
saveDEKAF('DKF_CPGCone.mat','data2save','x_plot');

%% Saving profiles for ludo
%{
DEKAF.u = intel_dimVars.u;
DEKAF.du_dy = intel_dimVars.du_dy;
DEKAF.v = intel_dimVars.v;
DEKAF.dv_dy = intel_dimVars.dv_dy;
DEKAF.T = intel_dimVars.T;
DEKAF.dT_dy = intel_dimVars.dT_dy;
DEKAF.y = intel_dimVars.y;

CFDpp.u = uRot(:,idx_tcplt);
CFDpp.du_dy = uyRot(:,idx_tcplt);
CFDpp.v = vRot(:,idx_tcplt);
CFDpp.dv_dy = vyRot(:,idx_tcplt);
CFDpp.T = T(:,idx_tcplt);
CFDpp.dT_dy = TyRot(:,idx_tcplt);
CFDpp.y = yRot(:,idx_tcplt);

save('profiles4ludo.mat','DEKAF','CFDpp','U_e','p_e','T_e','Tw','x_plot','cone_angle');
%}