% Test_FSCvsBL_CPGcone is a script to test the marching solver over a BL
%
% Author: Fernando Miro Miro
% Date: October 2018

clear;
close all;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'_stable/SourceCode']))

% Flow conditions
M_e = 10;
T_e = 300; % K
p_e = 4000; % Pa
cp = 1004.5; % [J/kg-K]
gam = 1.4; % [-]
cone_angle = 7; % deg
x1 = 0.5; % [m]
x0 = 1e-4; % [m]
Nx = 30;

% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 5e-13;                % Convergence tolerance
options.it_max                  = 40;                   % maximum number of iterations
options.coordsys                = 'cone';               % conical coordinate system
intel.cone_angle                = cone_angle;           % wedge angle in degrees

% flow options
options.thermal_BC              = 'adiab';              % Wall bc (g or dg/deta)
options.flow                    = 'CPG';                % flow assumption
options.modelTransport          = 'Sutherland';         % transport model
options.specify_cp = true;
options.specify_gam = true;
intel.cp = cp;
intel.gam = gam;

% display options
options.plotRes = false;                        % we don't want the residual plot
options.MarchingGlobalConv = false;             % neither the final global convergence plot

%% Self-similar block
% shock options
optionsSS = options;
optionsSS.shockJump         = true;                 % we do want to use shock relations

% dimensional evaluation options
optionsSS.dimoutput = true;
optionsSS.dimXQSS = true;

% flow conditions
intelSS = intel;
intelSS.M_e               = M_e;                  % Mach number at boundary-layer edge                          [-]
intelSS.T_e               = T_e;                  % Absolute temperature at boundary-layer edge                 [K]
intelSS.p_e               = p_e;                  % Absolute static pressure at boundary-layer edge             [Pa]
intelSS.x = x1;

% SS solver
[intelSS,optionsSS] = DEKAF(intelSS,optionsSS);

%% Marching block
optionsBL = options;
optionsBL.shockJump = false; % we introduce directly the post-shock conditions we got from the previous case

% flow conditions
Nxe = 50;
intelBL = intel;
intelBL.M_e0 = intelSS.M_e;
intelBL.x_e = linspace(0,1.1*x1,Nxe);            % edge conditions
intelBL.p_e = intelSS.p_e * ones(1,Nxe);
intelBL.T_e = intelSS.T_e * ones(1,Nxe);

% marching mapping options
optionsBL.xSpacing            = 'tanh';         % mapping in the marching direction
optionsBL.mapOpts.N_x         = Nx;             % number of streamwise points
optionsBL.mapOpts.x_start     = x0;             % intial x location [m]
optionsBL.mapOpts.x_end       = x1;             % final x location [m]
optionsBL.transverseCurvature = false;          % we want the transverse curvature term off

[intelBL0,optionsBL0] = DEKAF(intelBL,optionsBL,'marching');
%}

%% Marching block with curvature
%{%
optionsBL.transverseCurvature = true;           % we want the transverse curvature term on

[intelBL1,optionsBL1] = DEKAF(intelBL,optionsBL,'marching');
%}
%% plotting comparison
figure
Ni = 2; Nj = 3; ip=1;

subplot(Ni,Nj,ip); ip=ip+1;
plot(intelSS.u,intelSS.y,'k-'); hold on;
plot(intelBL0.u(:,end),intelBL0.y(:,end),'r-.');
% plot(intelBL1.u(:,end),intelBL1.y(:,end),'b--');
xlabel('u [m/s]'); ylabel('y [m]');

subplot(Ni,Nj,ip); ip=ip+1;
plot(intelSS.du_dy,intelSS.y,'k-'); hold on;
plot(intelBL0.du_dy(:,end),intelBL0.y(:,end),'r-.');
% plot(intelBL1.du_dy(:,end),intelBL1.y(:,end),'b--');
xlabel('\partial u / \partial y [1/s]'); ylabel('y [m]');
title([num2str(cone_angle),'-deg cone with M_\infty = ',num2str(M_e),', T_\infty = ',num2str(T_e),' [K], p_\infty = ',num2str(p_e),' [Pa], adiabatic ',options.flow,' at x = ',num2str(x1),' [m]']);

subplot(Ni,Nj,ip); ip=ip+1;
plot(intelSS.du_dy2,intelSS.y,'k-'); hold on;
plot(intelBL0.du_dy2(:,end),intelBL0.y(:,end),'r-.');
% plot(intelBL1.du_dy2(:,end),intelBL1.y(:,end),'b--');
xlabel('\partial^2 u / \partial y^2 [1/s-m]'); ylabel('y [m]');

subplot(Ni,Nj,ip); ip=ip+1;
plot(intelSS.T,intelSS.y,'k-'); hold on;
plot(intelBL0.T(:,end),intelBL0.y(:,end),'r-.');
% plot(intelBL1.T(:,end),intelBL1.y(:,end),'b--');
xlabel('T [K]'); ylabel('y [m]');

subplot(Ni,Nj,ip); ip=ip+1;
plot(intelSS.dT_dy,intelSS.y,'k-'); hold on;
plot(intelBL0.dT_dy(:,end),intelBL0.y(:,end),'r-.');
% plot(intelBL1.dT_dy(:,end),intelBL1.y(:,end),'b--');
xlabel('\partial T / \partial y [K/m]'); ylabel('y [m]');

subplot(Ni,Nj,ip); ip=ip+1;
plot(intelSS.dT_dy2,intelSS.y,'k-'); hold on;
plot(intelBL0.dT_dy2(:,end),intelBL0.y(:,end),'r-.');
% plot(intelBL1.dT_dy2(:,end),intelBL1.y(:,end),'b--');
xlabel('\partial^2 T / \partial y^2 [K/m^2]'); ylabel('y [m]');
legend('Self-similar','Marched (t=1)','Marched (t=r/r_0)');