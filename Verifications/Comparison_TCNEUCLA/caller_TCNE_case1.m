% caller_TCNE_case1 is a script to run the DEKAF solver to compare
% against Bitter & Shepherd's 2-T basic-state flowfields.
%
% Case corresponds to that in Knisely, C. P., & Zhong, X. (2019). Sound
% radiation by supersonic unstable modes in hypersonic blunt cone boundary
% layers. I. Linear stability theory Sound radiation by supersonic unstable
% modes in hypersonic blunt cone boundary layers. I. Linear stability
% theory. Physics of Fluids, 31(024103). https://doi.org/10.1063/1.5055761
%
% Date: July 2019
%{%s
clear;
close all;
addpath(genpath([getenv('VESTA_DIRECTORY'),'/VESTA_code']))
addpath(genpath(getenv('DEKAF_DIRECTORY')))

% Flow conditions
% M_inf   = 5;
U_inf   = 3882.42;      % m/s
T_inf   = 1500;         % K
% ys_inf  = {1e-10,1e-10,1e-10,0.78-3e-10,0.22};
ys_inf  = {0.22,0.78};
x_max   = 1;            % m
% rho_inf = 2.322e-2;     % kg/m^3
p_inf   = 10e3;         % Pa
T_w     = 300;          % K
cone_angle = 5;         % deg
Nx      = 50;

% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 1e-11;                % Convergence tolerance
options.it_max                  = 500;                  % maximum number of iterations
% options.it_res_sat              = options.it_max;       % residual saturation limit
options.T_tol                   = 1e-13;                % convergence tolerance for the NR to get T from the enthalpy
options.T_itMax                 = 200;                  % maximum number of iterations for the NR to get T from the enthalpy

% flow options
options.thermal_BC              = 'Twall';              % Wall bc (g or dg/deta)
options.G_bc                    = T_w;
% options.flow                    = 'CNE';                % flow assumption
% options.mixture                 = 'air5Park90';         % mixture
options.flow                    = 'TPG';                % flow assumption
options.mixture                 = 'air2';               % mixture
options.modelDiffusion          = 'cstSc';              % diffusion model
options.modelTransport          = 'BlottnerEucken';     % transport model
options.referenceVibRelax       = 'Mortensen2013';      % reference from which to obtain the vibrational relaxation constants
options.bParkCorrectionTauVib   = false;                % park's correction on the relaxation times
options.numberOfTemp            = '2T';                 % T-Tv model
options.dimoutput               = true;                 % we want dimensional profiles
options.shockJump               = true;                 % we give preshock conditions
% options.coordsys                = 'cone';               % we are solving a conical case
options.mapOpts.x_start         = 1e-5;                 % m
options.mapOpts.x_end           = x_max;                % m
options.mapOpts.N_x             = Nx;
options.xSpacing                = 'tanh';
options.perturbRelaxation       = 0.5;
options.Fields4conv = {'df_deta3','dg_deta2','momXeq','enereq'};
% options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';

% display options
options.plotRes = false;                        % we don't want the residual plot
options.MarchingGlobalConv = false;             % nor the marching global plot

% flow conditions
% intel.M_e0              = M_inf;        % preshock Mach
intel.U_e0              = U_inf;        % preshock velocity
intel.T_e0              = T_inf;        % preshock temperature
intel.Tv_e0             = T_inf;        % preshock vibrational temperature
intel.ys_e0             = ys_inf;       % preshock composition
% intel.rho_e0            = rho_inf;      % preshock density
intel.p_e0              = p_inf;        % preshock pressure
% intel.cone_angle        = cone_angle;   % cone half-angle
intel.wedge_angle       = cone_angle;   % cone half-angle

[intel_wdg,options_wdg] = DEKAF(intel,options,'marching');
%% cone case
options.coordsys            = 'cone';
intel.cone_angle            = cone_angle;   % cone half-angle
options.xSpacing            = 'custom_x';
options.mapOpts.x_custom    = intel_wdg.x(1,:);
[intel_con,options_con]     = DEKAF(intel,options,'marching');
%}

%% Plotting thermal convergence
%{%
idxX = round(linspace(1,Nx,10)); idxX(1) = [];
x_plot = intel_con.x(1,idxX); Nxplt = length(x_plot);
figure
for ix=1:Nxplt
    [~,idxW] = min(abs(intel_wdg.x(1,:) - x_plot(ix)));
    idxC = idxX(ix);
    subplot(1,2,1);
    plot(intel_wdg.T(:,idxW) ,intel_wdg.eta,'Color',two_colors(ix,Nxplt,'mgm')); hold on;
    plot(intel_wdg.Tv(:,idxW),intel_wdg.eta,'Color',two_colors(ix,Nxplt,'vrdsInv')); hold on;
    xlabel('T, T_v [K]'); ylabel('\eta [-]'); ylim([0,options.eta_i]); title('Wedge');
    subplot(1,2,2);
    plot(intel_con.T(:,idxC) ,intel_con.eta,'Color',two_colors(ix,Nxplt,'mgm')    ,'displayname',['T x = ',num2str(x_plot(ix)),'m']); hold on;
    plot(intel_con.Tv(:,idxC),intel_con.eta,'Color',two_colors(ix,Nxplt,'vrdsInv'),'displayname',['T_v x = ',num2str(x_plot(ix)),'m']); hold on;
    xlabel('T, T_v [K]'); ylabel('\eta [-]'); ylim([0,options.eta_i]); title('Cone');
%     suptitle(['x = ',num2str(x_plot(ix)),'m']);
end
legend();
%%
%}
XI = linspace(0,1,Nx);
for ix=Nx:-1:1
    [~,idxTmax_wdg] = max(intel_wdg.T(:,ix));
    [~,idxTmax_con] = max(intel_con.T(:,ix));
    Tdiff_wdg(ix) = intel_wdg.T(idxTmax_wdg,ix)-intel_wdg.Tv(idxTmax_con,ix);
    Tdiff_con(ix) = intel_con.T(idxTmax_con,ix)-intel_con.Tv(idxTmax_con,ix);
end
figure
subplot(2,1,1);
% plot(XI,intel_wdg.x,'-',XI,intel_con.x,'--'); ylabel('x [m]'); xlabel('\chi [-]');
plot(intel_wdg.x,Tdiff_wdg,'-',intel_con.x,Tdiff_con,'--'); xlabel('x [m]'); ylabel('\DeltaT_{max}');
subplot(2,1,2);
plot(intel_wdg.x(1,:),intel_wdg.xi,'-',intel_con.x(1,:),intel_con.xi,'--');xlabel('x [m]'); ylabel('\xi [kg^2/m^2-s^2]'); legend('wedge','cone'); grid minor;

%{%

%% Reference datax
LLNLpath = '/home/miromiro/workspace/33.SupersonicModesLLNL/1.BaseflowsLLNL/PoF31-024103_case1/';       % path to the folder containing the files
xmin = 0.05; [~,idxMin] = min(abs(intel_con.x(1,:)-xmin));
idxX = round(linspace(idxMin,Nx,10));
x_plot = intel_con.x(1,idxX); Nxplt = length(x_plot);
fields_LLNL = loadLLNL_flowfield(LLNLpath,x_plot);
% passing from LLNL species order (in rho_s#) to DEKAF's order (in y_s)
rhoLLNL = zeros(size(fields_LLNL.r1));
for s=1:N_spec
    rhoLLNL = rhoLLNL + fields_LLNL.(['r',num2str(s)]);         % computinig mixture density
end
LLNLspecList = {'N2','O2','NO','N','O'};                        % order of the species in the LLNL baseflow
for s=1:N_spec
    l = find(strcmp(intel_con.mixCnst.spec_list,LLNLspecList{s}));        % finding the position of each species s in the LLNL list, in our spec_list order
    fields_LLNL.ys{l} = fields_LLNL.(['r',num2str(s)]) ./ rhoLLNL;
end

%% plotting
vars2plt = {'u','v','T','Tv'}; Nvars = length(vars2plt);
units =    {'m/s','m/s','K','K'};
xlims =    {[0,4000],[-50,50],[0,3000],[0,3000]};
% [Ni,Nj] = optimalPltSize(Nvars+N_spec);
Ni = Nvars; Nj = Nxplt;
figure;
ymax = intel_con.y_i(end);
[ha, pos] = tightSubplot(Ni, Nj, [0.05,0.02], [0.05,0.05], [0.05,0.01]);
for ix=1:Nxplt
%     clf;
    for ic=1:Nvars
        axes(ha((ic-1)*Nxplt+ix));
%         clr1 = two_colors(ix,Nxplt,'mgm');
%         clr2 = two_colors(ix,Nxplt,'vrdsInv');
        clr1 = [0,0,0];
        clr2 = [1,0,0];
        clr3 = [0,0,1];
        plot(intel_con.(vars2plt{ic})(:,idxX(ix)) , intel_con.y(:,idxX(ix)) , '-','Color',clr1,    'displayname',['DEKAF x=',num2str(x_plot(ix)),'m']); hold on
        plot(fields_LLNL.(vars2plt{ic})(:,ix) , fields_LLNL.y(:,ix) , '-','Color',clr2,'displayname',['LLNL x=',num2str(fields_LLNL.s(1,ix)),'m']);
        if ic==2
        plot(-fields_LLNL.(vars2plt{ic})(:,ix) , fields_LLNL.y(:,ix) , '-','Color',clr3,'displayname',['LLNL x=',num2str(fields_LLNL.s(1,ix)),'m']);
        end
        ylim([0,ymax]); xlim(xlims{ic});
        xlabel([vars2plt{ic},' [',units{ic},']']);
        if ix==1;       ylabel('y [m]');
        else;           set(ha((ic-1)*Nxplt+ix),'YTickLabel',''); % no y axis
        end
        if ic==2 && ix==1;  legend('DEKAF','LLNL','-LLNL'); end
        if ic==1;           title(['x=',num2str(x_plot(ix),'%0.3f'),' m']); end
    end
%     for s=1:N_spec
%         ic=ic+1;
%         subplot(Ni,Nj,ic);
%         plot(intel_con.ys{s}(:,idxX(ix)) , intel_con.y(:,idxX(ix)) , '-','Color',two_colors(ix,Nxplt,'mgm'),    'displayname',['DEKAF x=',num2str(x_plot(ix)),'m']); hold on
%         plot(fields_LLNL.ys{s}(:,ix) , fields_LLNL.y(:,ix) , '-','Color',two_colors(ix,Nxplt,'vrdsInv'),'displayname',['LLNL x=',num2str(fields_LLNL.s(1,ix)),'m']);
%         ylim([0,ymax]);
%         xlabel(['Y_{',intel_con.mixCnst.spec_list{s},'}']); ylabel('y [m]');
%     end
%     pause(0.5);
end
%}
