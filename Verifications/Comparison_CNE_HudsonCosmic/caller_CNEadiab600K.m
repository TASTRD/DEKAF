% caller_CNEadiab600K compares against Cosmic
%
% Date: October 2017
%{
clear;
close all;
%clc
%}

clear;
addpath(genpath(getenv('DEKAF_DIRECTORY')));

% Optional inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.thermal_BC              = 'adiab';              % Wall bc (g or dg/deta)
% options.G_bc                    = 3000;                 % Value for g boundary condition
options.flow                    = 'CNE';                % flow assumption
options.mixture                 = 'air5Park85';         % mixture
options.modelTransport          = 'CE_122';             % transport model
options.modelDiffusion          = 'FOCE_Ramshaw';      % diffusion model

options.tol                     = 1e-14;                % Convergence tolerance
options.it_max                  = 100;
options.T_tol                   = 1e-13;
options.T_itMax                 = 200;

options.plot_transient          = false;                % to plot the inter-iteration profiles
options.plotRes                 = false;                % to plot the residual convergence

% Convergence gif options
% options.gifConv                 = true;
% options.fields4gif              = {'fh','dfh_deta','dfh_deta2','dfh_deta3','gh','dgh_deta','dgh_deta2'};
% options.gifLogPlot              = true;
% options.gifxlim                 = [1e-16,1e0];
%{%
% Flow parameters
M_e = 10;
T_e = 600;
ys_e = {1e-10,1e-10,1e-10,0.78-3e-10,0.22}; % mass fractions of N, O, NO, N2 and O2
Re1_e = 6.6e6;
x_max = 0.6;

intel.x_e               = [0,x_max*1.1];
intel.M_e               = M_e*[1,1];                  % Mach number at boundary-layer edge                        [-]
intel.T_e               = T_e*[1,1];                  % Static temperature at boundary-layer edge                 [K]
intel.Re1_e0            = Re1_e;                % Unit Reynolds number at boundary-layer edge               [-]
intel.ys_e              = cellfun(@(cll)cll*[1,1],ys_e,'UniformOutput',false);                 % mass fractions at the bl edge

options.mapOpts.x_start= 1e-7;
options.mapOpts.x_end   = x_max;
options.mapOpts.N_x     = 200;
options.xSpacing        = 'tanh';
options.ox              = 6;

intel = DEKAF(intel,options,'marching');

%% Loading cosmic data and plotting

T_cosmic = load('Cosmic_BL_adiab600K/T.csv');
U_cosmic = load('Cosmic_BL_adiab600K/U.csv');
Y_cosmic{1} = sortrows(load('Cosmic_BL_adiab600K/Y_N.csv'),2);
Y_cosmic{2} = sortrows(load('Cosmic_BL_adiab600K/Y_O.csv'),2);
Y_cosmic{3} = sortrows(load('Cosmic_BL_adiab600K/Y_NO.csv'),2);
Y_cosmic{4} = sortrows(load('Cosmic_BL_adiab600K/Y_N2.csv'),2);
Y_cosmic{5} = sortrows(load('Cosmic_BL_adiab600K/Y_O2.csv'),2);

figure;
subplot(1,3,1)
plot(intel.T(:,end),intel.y(:,end),'k',T_cosmic(:,1),T_cosmic(:,2),'r--');
ylabel('y [m]'); xlabel('T [K]'); ylim([0,0.008]);
subplot(1,3,2)
plot(intel.u(:,end),intel.y(:,end),'k',U_cosmic(:,1),U_cosmic(:,2),'r--');
ylabel('y [m]'); xlabel('u [m/s]'); ylim([0,0.008]);
legend('DEKAF','cosmic');
subplot(1,3,3)
for s=1:length(Y_cosmic)
plot(intel.ys{s}(:,end),intel.y(:,end),'k',Y_cosmic{s}(:,1),Y_cosmic{s}(:,2),'r--');
hold on
end
ylabel('y [m]'); xlabel('Y_s [-]'); ylim([0,0.008]);

