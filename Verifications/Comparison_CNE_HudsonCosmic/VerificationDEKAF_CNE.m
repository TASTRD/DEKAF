% VerificationDEKAF_CNE is a script to plot the Mach 10 adiabatic profile
% obtained with DEKAF and compare it against Hudson's and Cosmic's results.
%
% Author: Fernando Miro Miro
% Date: November 2017

clear;
addpath(genpath('/home/miromiro/Dropbox/Project_DEKAF/DEKAF'));

DEKAF = load('/home/miromiro/workspace/8.Verification_CNE/9.CNEmeanflow_stability/Mach10/6.DEKAF/DEKAF_CNE_M10_T278_adiab_Neta100_Nxi300_xi1e-07to0.0025.mat');
cosmic_path = 'Cosmic_BL/fp1.d0.#pos0';
hudson_path = 'Hudson_Figs/MeanFlow_Hudson_#var_x0p#pos.csv';
pos = {'05','2','4'};
x = [0.05,0.2,0.4];
figure
for i=1:length(pos)
    cosmic = load(strrep(cosmic_path,'#pos',pos{i}));
    hudson_U = load(strrep(strrep(hudson_path,'#pos',pos{i}),'#var','U'));
    hudson_U = sortrows(hudson_U);
    hudson_T = load(strrep(strrep(hudson_path,'#pos',pos{i}),'#var','T'));
    hudson_T = sortrows(hudson_T);
    hudson_O2 = load(strrep(strrep(hudson_path,'#pos',pos{i}),'#var','O2'));
    hudson_O2 = sortrows(hudson_O2);
    hudson_O = load(strrep(strrep(hudson_path,'#pos',pos{i}),'#var','O'));
    hudson_O = sortrows(hudson_O);
    [~,posX] = min(abs(DEKAF.x(1,:)-x(i)));

    %figure
    %subplot(2,2,1)
    subplot(3,4,(i-1)*4+1)
    plot(DEKAF.u(:,posX),DEKAF.y(:,posX),'k-',cosmic(:,3),cosmic(:,1),'b-.',hudson_U(:,2),hudson_U(:,1),'r--');
    xlabel('U [m/s]'); ylabel('y [m]');

    %subplot(2,2,2)
    subplot(3,4,(i-1)*4+2)
    plot(DEKAF.T(:,posX),DEKAF.y(:,posX),'k-',cosmic(:,4),cosmic(:,1),'b-.',hudson_T(:,2),hudson_T(:,1),'r--');
    xlabel('T [K]'); ylabel('y [m]');

    %subplot(2,2,3)
    subplot(3,4,(i-1)*4+3)
    plot(DEKAF.ys{5}(:,posX),DEKAF.y(:,posX),'k-',cosmic(:,5),cosmic(:,1),'b-.',hudson_O2(:,2),hudson_O2(:,1),'r--');
    xlabel('Y_{O2} [-]'); ylabel('y [m]');

    %subplot(2,2,4)
    subplot(3,4,(i-1)*4+4)
    plot(DEKAF.ys{2}(:,posX),DEKAF.y(:,posX),'k-',cosmic(:,8),cosmic(:,1),'b-.',hudson_O(:,2),hudson_O(:,1),'r--');
    xlabel('Y_O [-]'); ylabel('y [m]'); title(['x = ',num2str(x(i)),' m']);
end
legend('DEKAF','cosmic','Hudson');

%% plotting DEKAF profiles along x
x_plot = 0.01:0.025:0.45;
clear plots legends
figure
for i=1:length(x_plot)
    [~,posX] = min(abs(x_plot(i) - DEKAF.x(1,:)));
    rainbowVec = color_rainbow(i,length(x_plot));
    subplot(2,3,1)
    plot(DEKAF.ys{1}(:,posX),DEKAF.y(:,posX),'Color',rainbowVec);
    hold on;
    xlabel('Y_N [-]'); ylabel('y [m]');

    subplot(2,3,2)
    plot(DEKAF.ys{2}(:,posX),DEKAF.y(:,posX),'Color',rainbowVec);
    hold on;
    xlabel('Y_O [-]'); ylabel('y [m]');

    subplot(2,3,3)
    plot(DEKAF.ys{3}(:,posX),DEKAF.y(:,posX),'Color',rainbowVec);
    hold on;
    xlabel('Y_{NO} [-]'); ylabel('y [m]');

    subplot(2,3,4)
    plot(DEKAF.ys{4}(:,posX),DEKAF.y(:,posX),'Color',rainbowVec);
    hold on;
    xlabel('Y_{N2} [-]'); ylabel('y [m]');

    subplot(2,3,5)
    plot(DEKAF.ys{5}(:,posX),DEKAF.y(:,posX),'Color',rainbowVec);
    hold on;
    xlabel('Y_{O2} [-]'); ylabel('y [m]');
    %{
    subplot(2,3,6)
    plots(i) = plot(DEKAF.T(:,posX),DEKAF.y(:,posX),'Color',rainbowVec);
    hold on;
    xlabel('T [K]'); ylabel('y [m]');
    legends{i} = ['x = ',num2str(x_plot(i)),' m'];
    %}
    subplot(2,3,6)
    plots(i) = plot(DEKAF.h(:,posX),DEKAF.y(:,posX),'Color',rainbowVec);
    hold on;
    xlabel('T [K]'); ylabel('y [m]');
    legends{i} = ['x = ',num2str(x_plot(i)),' m'];
end
legend(plots,legends);
