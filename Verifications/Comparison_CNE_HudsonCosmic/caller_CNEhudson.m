% caller_CNEhudson is a script to run the DEKAF solver
%
% Date: October 2017
%{
clear;
close all;
%clc
%}
%{%
clear;
addpath(genpath(getenv('DEKAF_DIRECTORY')));

% Optional inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.thermal_BC              = 'adiab';              % Wall bc (g or dg/deta)
% options.G_bc                    = 3000;                 % Value for g boundary condition
options.flow                    = 'CNE';               % flow assumption
options.mixture                 = 'air5mutation';       % mixture
options.modelTransport          = 'BlottnerEucken';     % transport model
options.modelDiffusion          = 'cstSc';              % diffusion model
options.cstPr                   = false;                % constant PRandtl number
options.specify_cp              = false;                % user-specified cp

options.tol                     = 1e-10;                % Convergence tolerance
options.it_max                  = 100;
options.T_tol                   = 1e-13;
options.T_itMax                 = 200;

options.plot_transient          = false;                % to plot the inter-iteration profiles
options.plotRes                 = false;                % to plot the residual convergence
options.plot_marching           = false;

% Convergence gif options
% options.gifConv                 = true;
% options.fields4gif              = {'fh','dfh_deta','dfh_deta2','dfh_deta3','gh','dgh_deta','dgh_deta2'};
% options.gifLogPlot              = true;
% options.gifxlim                 = [1e-16,1e0];
%{%
% Flow parameters
M_e = 10;
T_e = 278;
% p_e = 10000;
ys_e = {1e-10,1e-10,1e-10,0.78-3e-10,0.22}; % mass fractions of N, O, NO, N2 and O2
Re1_e = 9.8425e6;
% Re1_e = 6.6e6;
Sc_e = 0.5;
x_max = 0.4;
intel.x_e               = [0,x_max*1.1];
intel.M_e               = M_e*[1,1];                  % Mach number at boundary-layer edge                        [-]
intel.T_e               = T_e*[1,1];                  % Static temperature at boundary-layer edge                 [K]
intel.Re1_e0            = Re1_e;                % Unit Reynolds number at boundary-layer edge               [-]
intel.ys_e              = ys_e;                 % mass fractions at the bl edge
intel.Sc_e              = Sc_e;                 % Schmidt number

options.mapOpts.x_start= 1e-7;
options.mapOpts.x_end   = x_max;
options.mapOpts.N_x     = 200;
options.ox              = 6;

intel = DEKAF(intel,options,'marching');

%% Loading cosmic data and plotting

ic=1;

U_hudson{ic}  = sortrows(load('Hudson_Figs/MeanFlow_Hudson_U_x0p05.csv'));
O_hudson{ic}  = sortrows(load('Hudson_Figs/MeanFlow_Hudson_O_x0p05.csv'));
O2_hudson{ic} = sortrows(load('Hudson_Figs/MeanFlow_Hudson_O2_x0p05.csv'));
T_hudson{ic}  = sortrows(load('Hudson_Figs/MeanFlow_Hudson_T_x0p05.csv')); ic=ic+1;

U_hudson{ic}  = sortrows(load('Hudson_Figs/MeanFlow_Hudson_U_x0p2.csv'));
O_hudson{ic}  = sortrows(load('Hudson_Figs/MeanFlow_Hudson_O_x0p2.csv'));
O2_hudson{ic} = sortrows(load('Hudson_Figs/MeanFlow_Hudson_O2_x0p2.csv'));
T_hudson{ic}  = sortrows(load('Hudson_Figs/MeanFlow_Hudson_T_x0p2.csv')); ic=ic+1;

U_hudson{ic}  = sortrows(load('Hudson_Figs/MeanFlow_Hudson_U_x0p4.csv'));
O_hudson{ic}  = sortrows(load('Hudson_Figs/MeanFlow_Hudson_O_x0p4.csv'));
O2_hudson{ic} = sortrows(load('Hudson_Figs/MeanFlow_Hudson_O2_x0p4.csv'));
T_hudson{ic}  = sortrows(load('Hudson_Figs/MeanFlow_Hudson_T_x0p4.csv')); ic=ic+1;

x_plot = [0.05,0.2,0.4];
for ix=length(x_plot):-1:1
[~,idxX(ix)] = min(abs(intel.x(1,:) - x_plot(ix)));
end

figure;
for ix=length(x_plot):-1:1
subplot(1,3,1)
plot(intel.T(:,idxX(ix)),intel.y(:,idxX(ix)),'k'); hold on
plot(T_hudson{ix}(:,2),T_hudson{ix}(:,1),'r--');
ylabel('y [m]'); xlabel('T [K]'); ylim([0,intel.y_i(end)]);
subplot(1,3,2)
plot(intel.u(:,idxX(ix)),intel.y(:,idxX(ix)),'k'); hold on;
plot(U_hudson{ix}(:,2),U_hudson{ix}(:,1),'r--');
ylabel('y [m]'); xlabel('u [m/s]'); ylim([0,intel.y_i(end)]);
legend('DEKAF','hudson');
subplot(1,3,3)
plot(intel.ys{2}(:,idxX(ix)),intel.y(:,idxX(ix)),'k'); hold on;
plot(O_hudson{ix}(:,2),O_hudson{ix}(:,1),'r--'); hold on;
end
% plot(intel.ys{5}(:,end),intel.y(:,end),'k'...
%     ...,Y_cosmic{s}(:,1),Y_cosmic{s}(:,2),'r-.'...
%     ,O2_hudson(:,2),O2_hudson(:,1),'r--');
hold on
ylabel('y [m]'); xlabel('Y_s [-]'); ylim([0,intel.y_i(end)]); %xlim([yslim,1]);

%}

%% Saving for comparison

for ii=length(x_plot):-1:1
data2save(ii).uDKF = intel.u(:,idxX(ii));
data2save(ii).TDKF = intel.T(:,idxX(ii));
data2save(ii).YODKF = intel.ys{2}(:,idxX(ii));
data2save(ii).yDKF = intel.y(:,idxX(ii));

data2save(ii).uHud      =  U_hudson{ii}(:,2);
data2save(ii).yuHud     =  U_hudson{ii}(:,1);
data2save(ii).THud      =  T_hudson{ii}(:,2);
data2save(ii).yTHud     =  T_hudson{ii}(:,1);
data2save(ii).YOHud     =  O_hudson{ii}(:,2);
data2save(ii).yYOHud    =  O_hudson{ii}(:,1);
end

% Saving
saveDEKAF('DKF_CNEhudson.mat','data2save','x_plot');