% Test_nozzleViscousCorrection is a script to test the function to make a
% correction of a nozzle geometry based on viscous effects.
%
% Author: Fernando Miro Miro
% Date: March 2020

clear; close all; clc;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

%% Thermophysical properties
T_w = 296; % [K]
mixture = 'Ar'; % pure argon mixture
modelTransport = 'Sutherland';
flow = 'CPG';
geomTol = 1e-6;
mm2m = 1e-3;    % milimeters to meters

%% Reference data
dataCFD = load('CFDpp_viscousProfiles.mat'); % CFD++
xCFD    = dataCFD.xAxis(:);
rCFD    = dataCFD.rc(:);

%% Nozzle
dataMoC = load('MoC_inviscidProfiles.mat'); skip=10; % MoC
idxEnd  = find(dataMoC.xAxis<max(xCFD/mm2m),1,'last');
s_e     = dataMoC.s_e(1:skip:idxEnd)   * mm2m; % converting to meters
rInv    = dataMoC.rc(1:skip:idxEnd)    * mm2m;
xInv    = dataMoC.xAxis(1:skip:idxEnd) * mm2m;
M_e     = dataMoC.M_e(1:skip:idxEnd);
T_e     = dataMoC.T_e(1:skip:idxEnd);
p_e     = dataMoC.p_e(1:skip:idxEnd);
p_e0    = p_e(1);

%% Running
tic;
[rVis,deltaStar,conv_vec,delta99] = DEKAF_nozzleViscousCorrection(xInv,rInv,[],p_e0,T_e,M_e,T_w,mixture,flow,modelTransport,geomTol,'guessDeltaStarFromExternalBL',2,'additionalOptions',{'L',6,'eta_i',2},'runOnOriginalMesh','additionalOutVars',{'delta99'});
toc;

%% Plotting
plot(xCFD,rCFD,'k:','linewidth',1.5,'displayname','CFD'); daspect([1,1,1]); lgnd=legend(); set(lgnd,'location','eastoutside');
figure;
semilogy(conv_vec,'kx-'); xlabel('Iteration'); ylabel('max(\Delta r) [m]');