% caller_FSC_incompressible is a script to run the incompressible case for
% the comparison against Tempelmann.
%
% Author(s): Ludovico Zanus and Fernando Miro Miro
% Date: June 2019

clear;

addpath(genpath(getenv('DEKAF_DIRECTORY')));

% gas properties and constants
Pr = 0.7;                              % constant prandtl number
R = 287.058;                           % gas constant
gamma = 1.4;                           % constant gamma
cp = R/(gamma-1) * gamma;

% flow variables
T_e = 293;                             % edge temperature, K
M_e = 1e-3;
Lambda_sweep = 45;%             % sweep angle of flat plate, deg
m = 0.2;
ReTemp = 220;
xTemp  = 167;
Re = sqrt(ReTemp*xTemp / cosd(Lambda_sweep));
Re1_e = Re;                            % edge unit reynolds

% numerics
tol = 1e-15;
N_eta = 100;
eta_max = 100;
eta_i = 6;

% DEKAF inputs
options.Cooke                   = true;
options.N                       = N_eta;                % Number of points
options.L                       = eta_max;              % Domain size
options.eta_i                   = eta_i;                % Mapping parameter, eta_crit
options.thermal_BC              = 'adiab';              % Wall bc
options.flow                    = 'CPG';                % flow assumption
options.mixture                 = 'air2';               % mixture
options.modelTransport          = 'Sutherland';         % transport model
options.cstPr                   = true;                 % constant Prandtl number
options.specify_cp              = true;                 % user-specified cp
options.specify_gam             = true;                 % user-specified gam

options.tol                     = tol;                  % Convergence tolerance
options.it_max                  = 100;

options.plot_transient          = false;                % to plot the inter-iteration profiles
options.plotRes                 = false;                 % to plot the residual convergence

options.dimoutput               = false;                % to output fields in dimensional form
options.dimXQSS                 = false;                % we want to input the dimensional x location (not xi)

% Flow parameters
intel.M_e                       = M_e;                  % velocity at boundary-layer edge                           [m/s]
intel.T_e                       = T_e;                  % Static temperature at boundary-layer edge                 [K]
intel.Re1_e                     = Re1_e;                % Edge unit Reynolds number
intel.Lambda_sweep              = Lambda_sweep;         % sweep angle                                               [deg]
intel.cp                        = cp;                   % Constant specific heat                                    [J/(kg K)]
intel.gam                       = gamma;                % Constant specific heat ratio                              [-]
intel.beta                      = 2*m/(m+1);            % beta hartree parameter                                    [-]
options.Pr                      = Pr;                   % Constant Prandtl number                                   [-]


% --- Run DEKAF --- %
[intel_QSS,options] = DEKAF(intel,options);


% Convert str location
options.rotateVelocityVector = 'streamline';
options.bVaryingEdgeQSS = true;
U_e     = intel_QSS.U_e;
rho_e   = intel_QSS.rho_e;
mu_e    = intel_QSS.mu_e;
Q_e     = sqrt(U_e^2 + intel_QSS.W_0^2);

options.fixReQSS = true;
intel_QSS.Re = Re;
% options.dimXQSS = true;
% intel_QSS.x = Re^2/Q_e/rho_e*mu_e;

delta = ReTemp/U_e/rho_e*mu_e;


% Compute dimensional boundary layer
intel_dimVars = eval_dimVars(intel_QSS,options);

l_sc = sqrt(mu_e*intel_dimVars.x/(rho_e*Q_e));


% Mean flow plot
Fig3_2_u = sortrows(load('Literature_data/Fig3.2_u.csv'),2);
Fig3_2_w = sortrows(load('Literature_data/Fig3.2_w.csv'),2);

figure;
hold on
ylabel('y/l [-]');

uRefTem = max(Fig3_2_u(:,1));

ylim([0 intel_dimVars.y_i])
plot(intel_dimVars.u_s/Q_e,intel_dimVars.y,'or')
plot(-10*intel_dimVars.w_s/Q_e,intel_dimVars.y,'sb')
plot(Fig3_2_u(:,1)/uRefTem,Fig3_2_u(:,2)*delta,'--k')
plot(Fig3_2_w(:,1)/uRefTem,Fig3_2_w(:,2)*delta,'--k')
legend('U_s','-10 x W_s','Tempelmann(2011)','location','northwest')

%% Saving for comparison
data2save.uDKF = intel_dimVars.u_s / Q_e;
data2save.wDKF = intel_dimVars.w_s / Q_e;
data2save.yDKF = intel_dimVars.y;

data2save.uTem =  Fig3_2_u(:,1)/uRefTem;
data2save.wTem = -Fig3_2_w(:,1)/uRefTem/10;
data2save.yuTem = Fig3_2_u(:,2)*delta;
data2save.ywTem = Fig3_2_w(:,2)*delta;

% Saving
saveDEKAF('DKF_3DTempelmann.mat','data2save');
