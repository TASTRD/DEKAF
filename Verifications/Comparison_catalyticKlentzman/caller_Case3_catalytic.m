% caller_Case3_catalytic is a script to run the Klentzman 2013's Case #3
% witht various catalytic recombination velocities
%
% Author: Fernando Miro Miro
% Date: January 2020

clear;
close all;
    addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

outfile = 'DEKAF_Case3_cat';

% Flow conditions
logKCat_vec = [0,2,3,4,5,6];        % [cm/s]
M_e         = 10.545;               % frozen edge Mach number, -
p_e         = 0.0433*101325;        % edge pressure, 0.0433 atm to Pa
ys_e        = {1e-10,1-1e-10};      % mass fractions of O, O2
T_e         = 834;                  % temperature [K]
xMax        = 0.4;                  % streamwise length [m]
Sc_e        = 0.7;                  % Schmidt number

H_ref  = 0.05596*1e-2;      % reference length scale [m]
T_ref  = T_e;               % reference temperature [K]

Ncases  = length(logKCat_vec);
pltFlag = 'Rbw2';

%%
for ii = Ncases:-1:1

    clear intel options

    % Optional inputs
    options.N                       = 100;                                      % Number of points
    options.L                       = 40;                                       % Domain size
    options.eta_i                   = 6;                                        % Mapping parameter, eta_crit
    options.thermal_BC              = 'adiab';                                  % Wall bc (g or dg/deta)
    options.flow                    = 'TPG';                                    % flow assumption
    options.mixture                 = 'oxygen2Bortner';                         % mixture
    options.modelTransport          = 'Brokaw58';                               % transport model
    options.modelDiffusion          = 'FOCE_Klentzman';                         % diffusion model
    options.molarDiffusion          = false;                                    % mass-fraction diffusion
    options.modelCollisionNeutral   = 'GuptaYos90';                             % collision integral data source
    options.modelEquilibrium        = 'Arrhenius';
    options.enhanceChemistry        = [25 9];                      % IMPORTANT!!! Klentzman's unexplained factors
    options.collisionRefExceptions.references = {'Yos63'};                      % the Yos exception
    options.collisionRefExceptions.pairs = {{'O - O','N - N'}};

    options.wallYs_BC               = 'catalyticK';
    options.Ys_bc                   = 10.^(logKCat_vec(ii)-2); % [m/s]

    options.BLproperties            = true;

    options.tol                     = 1e-14;                % Convergence tolerance
    options.it_max                  = 100;
    options.T_tol                   = 1e-13;
    options.T_itMax                 = 200;

    options.plot_transient          = false;                % to plot the inter-iteration profiles
    options.plotRes                 = false;                % to plot the residual convergence
    options.MarchingGlobalConv      = false;                % to plot the global convergence plot
%     options.quiet                   = true;                 % minimal workspace messages during marching

    % Flow parameters
    N_x                     = 20;
    intel.x_e               = linspace(0,1.1*xMax,N_x);
    intel.M_e               = M_e * ones(1,N_x);
    intel.T_e               = T_e * ones(1,N_x);
    intel.ys_e              = cellfun(@(cll)cll*ones(1,N_x),ys_e,'UniformOutput',false);
    options.flow            = 'CNE';
    intel.p_e0              = p_e;
    intel.Sc_e              = Sc_e * ones(1,N_x);

    options.mapOpts.x_start = 1e-5;
    options.tol             = 1e-10;
    options.it_max          = 400;
    options.mapOpts.x_end   = xMax;
    options.mapOpts.N_x     = 50;
    options.ox              = 2;

    [intel,options] = DEKAF(intel,options,'marching');

    all_intels(ii) = intel;
    all_options(ii) = options;

    %% plotting
    refData_YO2 = load(['Klentzman_data/YO2_Kcat1e',num2str(logKCat_vec(ii)),'cms.csv']);
    refData_T   = load(['Klentzman_data/T_Kcat1e',num2str(logKCat_vec(ii)),'cms.csv']);

    yMax = 1.5*intel.delta99(end);

    figure(111); Ni=1;Nj=2;ic=0;
    ic=ic+1; subplot(Ni,Nj,ic);
        plot(all_intels(ii).ys{2}(:,end),all_intels(ii).y(:,end),   'Color',two_colors(ii,Ncases,pltFlag)); hold on;
        plot(refData_YO2(:,2),           refData_YO2(:,1)*H_ref,'s','Color',two_colors(ii,Ncases,pltFlag)); hold on;
        xlabel('Y_{O_2} [-]'); ylabel('y [m]'); ylim([0,yMax]);
    ic=ic+1; subplot(Ni,Nj,ic);
        plots(ii+1)= plot(all_intels(ii).T(:,end),    all_intels(ii).y(:,end),  'Color',two_colors(ii,Ncases,pltFlag)); hold on;
        plots(1)   = plot(refData_T(:,2)*T_ref,       refData_T(:,1)*H_ref,'s', 'Color',two_colors(ii,Ncases,pltFlag)); hold on;
        xlabel('T [K]'); ylabel('y [m]'); ylim([0,yMax]);
        legends{ii+1} = ['K^{cat} = 10^',num2str(logKCat_vec(ii)),' cm/s'];     legends{1} = 'Klentzman & Tumin';
        idx4lgnd=[1,ii+1:Ncases+1]; legend(plots(idx4lgnd),legends(idx4lgnd));
%         idx4lgnd=1:ii+1; legend(plots(idx4lgnd),legends(idx4lgnd));
    drawnow;
end

save([outfile,'.mat'],'all_intels','all_options');
