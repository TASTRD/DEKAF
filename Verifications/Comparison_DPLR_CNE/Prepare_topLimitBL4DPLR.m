% Prepare_topLimitBL4DPLR is a script that grabs a BL simulation coming
% from DEKAF and saves the BL top limit, as well as the edge profiles.
%
% Author: Fernando Miro Miro
% Date: January 2018

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']));

path = 'DEKAF_CNE_air5m_M15_T700_adiab_Neta100_Nxi200_xi0.0001to0.0027634.mat';
load(path);
[data.x,data.delta,idx] = getBL_delta_fromPath(path,1.0001,1.1,'fitSqrtX','poly3');

for ii=1:length(data.x)
%     data.V_delta(ii) = v(idx(ii),ii); % edge wall-normal velocity as a function of x
    data.V_delta(ii) = v(1,ii); % edge wall-normal velocity as a function of x
end
data.U_delta = U_e(1)*ones(size(data.x)); % edge streamwise velocity as a function of x
data.T_delta = T_e(1)*ones(size(data.x)); % edge streamwise velocity as a function of x
data.rho_N_delta = ys_e{1}(1).*rho_e(1)*ones(size(data.x)); % edge N partial density as a function of x
data.rho_O_delta = ys_e{2}(1).*rho_e(1)*ones(size(data.x)); % edge O partial density as a function of x
data.rho_NO_delta = ys_e{3}(1).*rho_e(1)*ones(size(data.x)); % edge NO partial density as a function of x
data.rho_O2_delta = ys_e{4}(1).*rho_e(1)*ones(size(data.x)); % edge N2 partial density as a function of x
data.rho_N2_delta = ys_e{5}(1).*rho_e(1)*ones(size(data.x)); % edge O2 partial density as a function of x

save('profile_K_top.mat','-struct','data');

%% plotting
all_fields = fieldnames(data);
[N_i,N_j] = optimalPltSize(length(all_fields)-1);
figure;
itplt = 1;
for ii=1:length(all_fields)
    if ~sum(strcmp(all_fields{ii},{'x'}))
        subplot(N_i,N_j,itplt); itplt = itplt+1;
        plot(data.x,data.(all_fields{ii}),'b.');
        semilogx(data.x,data.(all_fields{ii}),'b.');
        ylabel(all_fields{ii}); xlabel('x [m]');
        xlim([data.x(2),Inf]);
    end
end


%% loading solution to visualize V(x) profile

test = load('DEKAF_CNE_air5m_M10_T600_adiab_Neta100_Nxi400_xi0.001to0.0046297.mat');

figure;
plot(test.x(1,:),test.v(1,:),'b.');
xlabel('x [m]');ylabel('v [m/s]');
ylim([0,Inf]);