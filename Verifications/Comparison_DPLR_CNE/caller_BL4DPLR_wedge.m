% caller_BL4DPLR_wedge is a script to run the DEKAF solver for the verification of
% DEKAF against DPLR for a wedge
%
% Date: March 2018
%{
clear;
close all;
%clc
%}

clear intel options
addpath(genpath([getenv('DEKAF_DIRECTORY'),'_stable']))
% Flow parameters
M_e = 35;
T_e = 220; % K
ys_e = {1e-10,1e-10,1e-10,0.767-3e-10,0.233}; % mass fractions of N, O, NO, N2 and O2
rho_e = 1e-3; % kg/m^3
Sc_e = 0.5;
Tw = 4000; % K
wedge_angle = 15; % degrees

% Optional inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.thermal_BC              = 'Twall';              % Wall bc (g or dg/deta)
options.G_bc                    = Tw;                   % Value for g boundary condition
options.flow                    = 'CNE';
options.mixture                 = 'air5mutation';       % mixture
options.modelTransport          = 'BlottnerEucken';     % transport model
options.modelDiffusion          = 'cstSc';              % diffusion model
options.shockJump               = true;
options.shockType               = 'wedge';

options.tol                     = 1e-14;                % Convergence tolerance
options.it_max                  = 100;
options.T_tol                   = 1e-13;
options.T_itMax                 = 200;
options.it_res_sat              = 100;

options.inviscid_ox             = 3;
options.inviscidMap.x_end       = 1;
options.inviscidMap.x_start     = 1e-7;
options.inviscidMap.N_x         = 5000;
options.xInviscidSpacing        = 'linear';
options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';

options.plot_transient          = false;                % to plot the inter-iteration profiles
options.plotRes                 = false;                % to plot the residual convergence

intel.Sc_e              = Sc_e;                 % Schmidt number
intel.wedge_angle       = wedge_angle;          % wedge angle
intel.M_e0 = M_e;
intel.T_e0 = T_e;
intel.rho_e0 = rho_e;
intel.ys_e0 = ys_e;

options.tol             = 1e-9; % solver and convergence options
options.it_max          = 1000;
options.ox              = 2;

options.xSpacing        = 'tanh'; % streamwise mapping parameters
options.mapOpts.x_start = 1e-7;
options.mapOpts.x_end   = options.inviscidMap.x_end;
options.mapOpts.N_x     = 1000;
options.mapOpts.XI_i    = 0.7;
options.mapOpts.frac_XIi= 0.8;
options.mapOpts.deltax_start = 0.01 * (options.mapOpts.x_end-options.mapOpts.x_start)/(options.mapOpts.N_x -1);

options.dimoutput       = true;
[intel1,options] = DEKAF(intel,options,'marching');

%% saving
% savepath = ['DEKAF_',options.flow,'_',options.mixture(1:min(5,length(options.mixture))),'_M',num2str(M_e),'_T',...
%     num2str(T_e),'_Tw',num2str(Tw),'_Neta',num2str(options.N),'_Nx',num2str(options.mapOpts.N_x ),'_x',...
%     num2str(options.mapOpts.x_start),'to',num2str(options.mapOpts.x_end),'.mat'];
savepath = ['DEKAF_wedge',num2str(intel.wedge_angle),'deg_',options.flow,'_',...
    options.mixture(1:min(5,length(options.mixture))),'_M',num2str(M_e),'_T',...
    num2str(T_e),'_Tw',num2str(Tw),'_Neta',num2str(options.N),'_Nx',num2str(options.mapOpts.N_x ),'_x',...
    num2str(options.mapOpts.x_start),'to',num2str(options.mapOpts.x_end),'.mat'];
saveLight(savepath,intel_dimVars,options,{'h'},'noDerivs')

%% plotting
clear plots legends
figure
idx = round(linspace(10,options.mapOpts.N_x ,10));
[N_i,N_j] = optimalPltSize(intel_dimVars.mixCnst.N_spec+4);
it = 0; ylims = [0,40];
for ii=idx
    it=it+1;
    legends{it} = ['x = ',num2str(intel_dimVars.x(1,ii)),' m'];
    ic = 1; % index counter
    subplot(N_i,N_j,ic); ic = ic+1;
    plots(it) = plot(intel_dimVars.u(:,ii),intel_dimVars.eta,'Color',color_rainbow(ii,options.mapOpts.N_x )); hold on;
    xlabel('u [m/s]'); ylabel('\eta [-]'); ylim(ylims); grid minor;
    subplot(N_i,N_j,ic); ic = ic+1;
    plot(intel_dimVars.T(:,ii),intel_dimVars.eta,'Color',color_rainbow(ii,options.mapOpts.N_x )); hold on;
    xlabel('T [K]'); ylabel('\eta [-]'); ylim(ylims); grid minor;
    subplot(N_i,N_j,ic); ic = ic+1;
    plot(intel_dimVars.g(:,ii),intel_dimVars.eta,'Color',color_rainbow(ii,options.mapOpts.N_x )); hold on;
    xlabel('g [-]'); ylabel('\eta [-]'); ylim(ylims); grid minor;
    subplot(N_i,N_j,ic); %ic = ic+1;
    plot(intel_dimVars.v(:,ii),intel_dimVars.eta,'Color',color_rainbow(ii,options.mapOpts.N_x )); hold on;
    xlabel('v [m/s]]'); ylabel('\eta [-]'); ylim(ylims); grid minor;
    for s=1:intel_dimVars.mixCnst.N_spec
    subplot(N_i,N_j,s+ic);
    plot(intel_dimVars.ys{s}(:,ii),intel_dimVars.eta,'Color',color_rainbow(ii,options.mapOpts.N_x )); hold on;
    xlabel(['y_{',intel_dimVars.mixCnst.spec_list{s},'} [-]']); ylabel('\eta [-]'); ylim(ylims); grid minor;
    end
end
legend(plots,legends);
%%
figure
semilogy(intel_dimVars.x(1,:),intel_dimVars.v(1,:),'k');
xlabel('x [m]'); ylabel('v [m/s]');
grid minor