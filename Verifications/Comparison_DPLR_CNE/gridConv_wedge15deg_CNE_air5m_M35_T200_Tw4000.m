% gridConv_wedge15deg_CNE_air5m_M35_T200_Tw4000 is a script to perform the
% mean-flow grid convergence.
%
% Author: Fernando Miro Miro
% Date: March 2018

clear;

% defining options
N_prof = 10; % number of profiles to compare
fld2plt = {'u','T','h','ys'};
units = {'[m/s]','[K]','[J/kg]','[-]'};
ylims = [0,6];

% defining paths
paths = {...
    'DEKAF_wedge15deg_CNE_air5m_M35_T220_Tw4000_Neta100_Nxi1000_xi0to0.0068286.mat',...
    'DEKAF_wedge15deg_CNE_air5m_M35_T220_Tw4000_Neta100_Nxi500_xi0to0.0068286.mat',...
    'DEKAF_wedge15deg_CNE_air5m_M35_T220_Tw4000_Neta100_Nxi200_xi0to0.0068286.mat',...
    };
pltStyle = {'rbw','k-.',':c'};

% loading files
for ii=1:length(paths)
    DKF{ii} = load(paths{ii});
end

%% Plotting
% sizes etc
N_paths = length(paths);
N_spec = DKF{1}.mixCnst.N_spec;
spec_list = DKF{1}.mixCnst.spec_list;
[N_i,N_j] = optimalPltSize(length(fld2plt)-1+N_spec); % subplot sizes
idx_x = round(linspace(1,size(DKF{1}.x,2),N_prof+1));  % indices of the profiles in the first intel
idx_x = idx_x(2:end); % removing first profile (BL assumptions don't work near the leading edge)
x_plt = DKF{1}.x(1,idx_x);

figure
for ii=1:N_paths
    for i_prf = 1:N_prof
        [~,idx] = min(abs(DKF{ii}.x(1,:)-x_plt(i_prf)));
        if strcmp(pltStyle{ii},'rbw')
            color_cell = {'Color',0.8*color_rainbow(i_prf,N_prof)};
        else
            color_cell = pltStyle(ii);
        end
        ic = 1; % index counter
        for i_fld = 1:length(fld2plt)
            if strcmp(fld2plt{i_fld},'ys') % we need to sweep species
                for s=1:N_spec
                    subplot(N_i,N_j,ic); ic = ic+1;
                    plot(DKF{ii}.(fld2plt{i_fld}){s}(:,idx),DKF{ii}.eta,color_cell{:}); hold on;
                    xlabel([fld2plt{i_fld},'_{',spec_list{s},'} ',units{i_fld}]); ylabel('\eta [-]');
                    grid minor; ylim(ylims);
                end
            else % there no need to sweep
                subplot(N_i,N_j,ic); ic = ic+1;
                plot(DKF{ii}.(fld2plt{i_fld})(:,idx),DKF{ii}.eta,color_cell{:}); hold on;
                xlabel([fld2plt{i_fld},' ',units{i_fld}]); ylabel('\eta [-]');
                grid minor; ylim(ylims);
            end
        end
    end
end