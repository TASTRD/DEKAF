% CompareDEKAFvsDPLR_M15_700K_adiab is a script to compare the flowfield
% obtained with DPLR and DEKAF for the Mach 15, 700K adiabatic case.
%
% Author: Fernando Miro Miro
% Date: March 2018

% loading files
clear;
DEKAF = load('DEKAF_CNE_mars5_M30_T200_Tw3000_Neta100_Nxi300_xi3.7818e-09to0.030357.mat');
DPLR_data = load('MarsV.dat');

varList_DPLR = {'x','y','p','T','M','C_co2','C_co','C_o2','C_c','C_o','u','v',... % lists of variables outputted by DPLR
    'rho','h'};

N_i = 802; % size of the grid in the i direction (horizontal)
N_j = 202; % size of the grid in the j direction (vertical)

for ii=1:length(varList_DPLR) % looping DPLR variables
    DPLR_fields.(varList_DPLR{ii}) = reshape(DPLR_data(:,ii),[N_i,N_j])'; % extracting and reshaping variable
end % closing variable loop

%% Plotting
DPLR_x = DPLR_fields.x(1,:);
DEKAF_x = DEKAF.x(1,:);
x_plot = 0.1:0.1:0.5;
%{%
for ix = 1:length(x_plot)
    [~,idx_DKF] = min(abs(DEKAF_x-x_plot(ix)));
    [~,idx_DPLR] = min(abs(DPLR_x-x_plot(ix)));
    y_max = real(DEKAF.y(round(end*2/5),idx_DKF));
    figure
    subplot(3,3,1)
    plot(DEKAF.u(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.u(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--'); grid minor;
    xlabel('u [m/s]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(3,3,2)
    plot(DEKAF.T(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.T(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--'); grid minor;
    xlabel('T [K]'); ylabel('y [m]'); ylim([0,y_max]);
%     title(['Re = ',num2str(Re_plot(ix))])
    title(['x = ',num2str(x_plot(ix)),' m'])

    subplot(3,3,3)
    plot(DEKAF.p_e(idx_DKF)*ones(DEKAF.options.N,1),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.p(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--'); grid minor;
    xlabel('p [Pa]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(3,3,4)
    plot(DEKAF.rho(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.rho(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--'); grid minor;
    xlabel('\rho [kg/m^3]'); ylabel('y [m]'); ylim([0,y_max]);

    % Note that DEKAF's order is  {'O','C','CO','O2','CO2'} for Mars5Park94

    subplot(3,3,5)
    plot(DEKAF.ys{1}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.C_o(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--'); grid minor;
    xlabel('y_{O} [-]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(3,3,6)
    plot(DEKAF.ys{2}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.C_c(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--'); grid minor;
    xlabel('y_{C} [-]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(3,3,7)
    plot(DEKAF.ys{3}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.C_co(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--'); grid minor;
    xlabel('y_{CO} [-]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(3,3,8)
    plot(DEKAF.ys{4}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.C_o2(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--'); grid minor;
    xlabel('y_{O2} [-]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(3,3,9)
    plot(DEKAF.ys{5}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.C_co2(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--'); grid minor;
    xlabel('y_{CO2} [-]'); ylabel('y [m]'); ylim([0,y_max]);
    legend('DEKAF','DPLR');
end
%}