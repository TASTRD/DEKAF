% CompareDEKAFvsDPLR_M15_700K_Tw1225K is a script to compare the flowfield
% obtained with DPLR and DEKAF for the Mach 15, 700K isothermal (1225K)
% case.
%
% Author: Fernando Miro Miro
% Date: January 2018

% loading files
clear;
DEKAF = load('DEKAF_CNE_air5m_M15_T700_Tw1225_Neta100_Nxi400_xi0.001to0.0018111.mat');
% DPLR_data = load('DPLR_case12.dat'); % neglecting vibr. energy
DPLR_data = load('DPLR_case13.dat'); % without neglecting vibr. energy

varList_DPLR = {'x','y','p','T','M','C_n2','C_o2','C_no','C_n','C_o','u','v',... % lists of variables outputted by DPLR
    'rho','G','mu_l','ReL'};
N_i = 802; % size of the grid in the i direction (horizontal)
N_j = 402; % size of the grid in the j direction (vertical)

for ii=1:length(varList_DPLR) % looping DPLR variables
    DPLR_fields.(varList_DPLR{ii}) = reshape(DPLR_data(:,ii),[N_i,N_j])'; % extracting and reshaping variable
end % closing variable loop

%% Obtaining x of the initial profile
x0 = DEKAF.xi(1)/(DEKAF.U_e(1)*DEKAF.rho_e(1)*DEKAF.mu_e(1));
DPLR_fields.x = DPLR_fields.x + x0; % updating x locations
DEKAF.x = DEKAF.x + x0;

%% Obtaining Re in DEKAF profiles
DEKAF_x = DEKAF.x(1,:);
DEKAF_Re = sqrt(DEKAF.rho_e.*DEKAF.U_e.*DEKAF_x./DEKAF.mu_e);

%% Obtaining Re number in DPLR profiles
DPLR_fields.Re = sqrt(DPLR_fields.x.*DPLR_fields.ReL);
% momentum bc
u_norm = DPLR_fields.u./repmat(DPLR_fields.u(end,:),[N_j,1]);
uSub99 = u_norm; uSub99(uSub99>0.99) = 0;
[~,idx_BL] = max(uSub99,[],1);

% thermal bc
% T_norm = DPLR_fields.T./repmat(DPLR_fields.T(end,:),[N_j,1]); % normalize T with the freestream
% TSub99 = T_norm; TSub99(TSub99<1.01) = 1e4; % make all those locations with more than 101% T_inf ridiculously high
% [~,idx_BL] = min(TSub99,[],1); % taking the indices of the minimum Tsub99 left

idx_SF = 10;                 % we define a safety factor for the BL edge index
idx_BL = idx_BL+idx_SF;     % adding this SF to make sure we're out of the boundary
                            % layer we now use these idx_BL to evaluate Re_L and get
                            % our Reynolds number

DPLR_Re = zeros(N_i,1);
DPLR_delta = zeros(N_i,1);
for ix = 1:N_i
    DPLR_Re(ix) = DPLR_fields.Re(idx_BL(ix),ix);
    DPLR_delta(ix) = DPLR_fields.y(idx_BL(ix),ix);
end
DPLR_x = DPLR_fields.x(1,:);

figure
subplot(1,2,1)
plot(DPLR_fields.x(1,:),DPLR_delta)
grid minor;
xlabel('x [m]'); ylabel('\delta_{U,99%} [m]'); title('DPLR BL height');
subplot(1,2,2)
plot(DPLR_fields.x(1,:),DPLR_Re)
hold on
plot(DEKAF.x(1,:),DEKAF_Re)
grid minor;
legend('DPLR','DEKAF');
xlabel('x [m]'); ylabel('Re [1/m]'); title(['idx = idx + ',num2str(idx_SF)]);

%% Plotting
Re_plot = [950:25:1000,1100:100:1300]; % profile locations to plot
% x_plot = 0.3:0.05:0.5;
%{%
for ix = 1:length(Re_plot)
%     [~,idx_DKF] = min(abs(DEKAF_x-x_plot(ix)));
%     [~,idx_DPLR] = min(abs(DPLR_x-x_plot(ix)));
    [~,idx_DKF] = min(abs(DEKAF_Re-Re_plot(ix)));
    [~,idx_DPLR] = min(abs(DPLR_Re-Re_plot(ix)));
    y_max = real(DEKAF.y(round(end/3),idx_DKF));
    figure
    subplot(3,3,1)
    plot(DEKAF.u(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.u(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r'); grid minor;
    xlabel('u [m/s]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(3,3,2)
    plot(DEKAF.T(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.T(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r'); grid minor;
    xlabel('T [K]'); ylabel('y [m]'); ylim([0,y_max]);
    title(['Re = ',num2str(Re_plot(ix))])
%     title(['x = ',num2str(x_plot(ix)),' m'])

    subplot(3,3,3)
    plot(DEKAF.p_e(idx_DKF)*ones(DEKAF.options.N,1),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.p(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r'); grid minor;
    xlabel('p [Pa]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(3,3,4)
    plot(DEKAF.rho(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.rho(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r'); grid minor;
    xlabel('\rho [kg/m^3]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(3,3,5)
    plot(DEKAF.ys{1}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.C_n(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r'); grid minor;
    xlabel('y_{N} [-]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(3,3,6)
    plot(DEKAF.ys{2}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.C_o(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r'); grid minor;
    xlabel('y_{O} [-]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(3,3,7)
    plot(DEKAF.ys{3}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.C_no(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r'); grid minor;
    xlabel('y_{NO} [-]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(3,3,8)
    plot(DEKAF.ys{4}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.C_n2(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r'); grid minor;
    xlabel('y_{N2} [-]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(3,3,9)
    plot(DEKAF.ys{5}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k'); hold on;
    plot(DPLR_fields.C_o2(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r'); grid minor;
    xlabel('y_{O2} [-]'); ylabel('y [m]'); ylim([0,y_max]);
    legend('DEKAF','DPLR');
end
%}