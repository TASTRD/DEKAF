% caller_FSC4DPLR is a script to run the DEKAF solver and prepare the
% generated profile to be used afterwards by DPLR as an inflow condition
%
% Date: January 2018
%{%
clear;
close all;
%clc
%}

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

clear options intel

% Optional inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.thermal_BC              = 'adiab';              % Wall bc
% options.G_bc                    = 1225;                  % Value for g boundary condition
options.flow                    = 'TPGD';               % flow assumption
options.mixture                 = 'air5mutation';       % mixture
options.modelTransport          = 'BlottnerEucken';     % transport model
options.modelDiffusion          = 'cstSc';              % transport model
options.cstPr                   = false;                % constant PRandtl number
options.specify_cp              = false;                % user-specified cp

options.tol                     = 1e-14;                % Convergence tolerance
options.it_max                  = 40;
options.perturbRelaxation       = 1;
options.highOrderTerms          = false;                % include higher order fluctuation terms in the equations
options.expandBaseFlow          = false;                % expand g about base flow variable T and dfdeta
options.T_tol                   = 1e-13;
options.T_itMax                 = 50;

options.plot_transient          = false;                % to plot the inter-iteration profiles
options.plotRes                 = true;                 % to plot the residual convergence

% Convergence gif options
options.gifConv                 = false;
% options.fields4gif              = {'fh','dfh_deta','dfh_deta2','dfh_deta3','gh','dgh_deta','dgh_deta2'};
% options.fields4gif              = {'g'};
% options.gifLogPlot              = false;
% options.gifxlim                 = [1e-16,1e0];
% options.gifxlim                 = [0.3,1.5];

options.dimoutput               = true;                % to output fields in dimensional form
intel.xi                        = 1e-4;                % xi location where we want the dimensional profiles
% intel.xi                        = 5.787124649327183e-06; % xi location that would give an x=0.001m for Re1=6.6e6 1/m, T_e=600K, M_e=10

% Flow parameters
intel.M_e                       = 15;                   % Mach number at boundary-layer edge                        [-]
intel.T_e                       = 700;                  % Static temperature at boundary-layer edge                 [K]
intel.Re1_e                     = 5e6;
% intel.p_e                       = 10000;               % Absolute static pressure at boundary-layer edge           [Pa]
% intel.cp                        = 1004.5;               % Constant specific heat                                    [J/(kg K)]
% intel.gam                       = 1.4;                  % Constant specific heat ratio                              [-]
% options.Pr                        = 0.7;                  % Constant specific heat ratio                              [-]
% intel.ys_e                      = {0.22,0.78};          % mass fractions of O2 and N2
intel.ys_e                      = {1e-10,1e-10,1e-10,0.78-3e-10,0.22}; % mass fractions of N, O, NO, N2 and O2
intel.Sc_e                      = 0.5;                  % Schmidt number

intel = DEKAF(intel,options);

Y = intel.y;
T = intel.T;
U = intel.u;
V = intel.v;
% V = zeros(size(intel.v));
Rhos = intel.rhos;
x = intel.xi / (intel.rho_e * intel.mu_e * intel.U_e);

% save('profile_F.mat','U','V','Y','T','Rhos');
% save('profile_G.mat','U','V','Y','T','Rhos');
% save('profile_H.mat','U','V','Y','T','Rhos');
% save('profile_I.mat','U','V','Y','T','Rhos');
%save('profile_J.mat','U','V','Y','T','Rhos');
save('profile_K.mat','U','V','Y','T','Rhos');

%% plotting profiles
figure
[N_i,N_j] = optimalPltSize(length(Rhos)+4);
subplot(N_i,N_j,1)
plot(U,Y,'.');
xlabel('u [m/s]'); ylabel('y [m]');

subplot(N_i,N_j,2)
plot(V,Y,'.');
xlabel('v [m/s]'); ylabel('y [m]');
title([options.mixture,' at x = ',num2str(x),' m in ',options.flow]);

subplot(N_i,N_j,3)
plot(T,Y,'.');
xlabel('T [K]'); ylabel('y [m]');

for s=1:intel.mixCnst.N_spec
    subplot(N_i,N_j,4+s)
    plot(Rhos{s},Y,'.');
    xlabel(['\rho_{',intel.mixCnst.spec_list{s},'} [kg/m^3]']); ylabel('y [m]');

    subplot(N_i,N_j,4)
    plot(intel.ys{s},Y,'Color',color_rainbow(s,intel.mixCnst.N_spec));
    hold on
end
legend(intel.mixCnst.spec_list);
xlabel('y_s [-]'); ylabel('y [m]');