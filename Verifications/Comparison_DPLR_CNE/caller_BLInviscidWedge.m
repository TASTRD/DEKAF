% caller_BLInviscidWedge is a script to run DEKAF's inviscid solver for a
% wedge, to compare the profiles to frozen and equilibrium conditions.
%
% Date: April 2018

% close all;
clear intel options
addpath(genpath([getenv('DEKAF_DIRECTORY'),'_stable/SourceCode']))

%%% INPUTS
wedge_angle = 15;
M_inf = 35;
T_inf = 220; % K
ys_inf = {1e-10,1e-10,1e-10,0.767-3e-10,0.233}; % mass fractions of N, O, NO, N2 and O2
rho_inf = 1e-3; % kg/m^3
%%% END OF INPUTS

% Optional inputs
% options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';
options.mixture                 = 'air5mutation';       % mixture
options.flow                    = 'CNE';                % flow assumption
options.inviscid_ox             = 1;                    % order of the inviscid solver
options.mapOpts.x_end           = 10;                  % final x location
options.mapOpts.x_start         = 1e-3;                 % initial x location
options.inviscidMap.N_x         = 10000;                % number of points
options.inviscidMap.deltax_start= 1e-7;                 % number of points
options.xInviscidSpacing        = 'log';               % spacing
options.shockJump               = true;                 % we don't want shock-jump relations
options.T_tol                   = 10*eps;               % tolerance for the NR on the temperature

intel.wedge_angle = wedge_angle;
intel.M_e0 = M_inf;
intel.T_e0 = T_inf;
intel.rho_e0 = rho_inf;
intel.ys_e0 = ys_inf;

% setting defaults and retrieving mixture constants
options = setDefaults(options);
[mixCnst,options] = getAll_constants(options.mixture,options);
intel.mixCnst = mixCnst;

% options for the inviscid solver
[intel,options] = DEKAF_inviscid(intel,options);

%% Second tram
%{
options.inviscidMap.x_end       = 1000;                % final x location
options.inviscidMap.x_start     = 10;                  % initial x location
options.xInviscidSpacing        = 'log';
intel2.wedge_angle = wedge_angle;
intel2.U_e0 = intel.U_e(end);
intel2.T_e0 = intel.TTrans_e(end);
intel2.p_e0 = intel.p_e(end);
intel2.ys_e0 = matrix2D2cell1D(intel.y_se(end,:));
intel2.mixCnst = mixCnst;
[intel2,options] = DEKAF_inviscid(intel2,options);
%}

% frozen and equilibrium shock relations
mixCnst = intel.mixCnst;
y_sinf = cell1D2matrix2D(ys_inf);
[mixCnst.LTEtablePath,mixCnst.XEq] = checkCompositionLTE(y_sinf,mixCnst.Mm_s,mixCnst.R0,options.mixture,options);
mixCnst.LTEtableData = load(mixCnst.LTEtablePath);

gam_inf = getMixture_gam(T_inf,T_inf,T_inf,T_inf,T_inf,y_sinf,mixCnst.R_s,mixCnst.nAtoms_s,... % computing gamma
                       mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,options);
[~,R] = getMixture_Mm_R(y_sinf,T_inf,T_inf,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron); % computing gas constant
U_inf = M_inf*sqrt(gam_inf*R*T_inf);                                        % computing velocity
p_inf = rho_inf*R*T_inf;
[T_froz,p_froz,u_froz,M_froz,shock_froz] = getShock_Props(T_inf,p_inf,M_inf,R,wedge_angle,gam_inf); % computing frozen edge conditions
[T_eq,p_eq,u_eq,y_seq,shock_eq] = getLTEShock_Props(U_inf,T_inf,p_inf,mixCnst,wedge_angle,options); % computing equilibrium edge conditions
froz.TTrans_e = T_froz*[1,1];
froz.p_e = p_froz*[1,1];
froz.U_e = u_froz*[1,1];
froz.y_se = repmat(y_sinf,[2,1]);
eq.TTrans_e = T_eq*[1,1];
eq.p_e = p_eq*[1,1];
eq.U_e = u_eq*[1,1];
eq.y_se = repmat(y_seq,[2,1]);

%% plotting
fields2plt = {'y_se','U_e','p_e','TTrans_e'};%,'h_e'};
units = {'[-]','[m/s]','[Pa]','[K]'};%,'[J/kg]'};
N_spec = intel.mixCnst.N_spec;
N_plts = length(fields2plt)-1+N_spec;
[N_i,N_j] = optimalPltSize(N_plts);
figure
it = 1;
for ii=1:length(fields2plt)
    switch size(intel.(fields2plt{ii}),2)
        case 1
            subplot(N_i,N_j,it); it = it+1;
            semilogx(intel.x_e,intel.(fields2plt{ii}),'k-'); hold on;
            semilogx(intel.x_e([1,end]),froz.(fields2plt{ii}),'b-');
            semilogx(intel.x_e([1,end]),eq.(fields2plt{ii}),'r-');
            xlabel('x [m]'); ylabel([fields2plt{ii},' ',units{ii}]);
        case N_spec
            for s=1:N_spec
            subplot(N_i,N_j,it); it = it+1;
            semilogx(intel.x_e,intel.(fields2plt{ii})(:,s),'k-'); hold on;
            semilogx(intel.x_e([1,end]),froz.(fields2plt{ii})(:,s),'b-');
            semilogx(intel.x_e([1,end]),eq.(fields2plt{ii})(:,s),'r-');
            xlabel('x [m]'); ylabel([fields2plt{ii},'_{',intel.mixCnst.spec_list{s},'} ',units{ii}]);
            end
    end
end
legend('CNE inviscid 1D','Frozen','Equilibrium');
