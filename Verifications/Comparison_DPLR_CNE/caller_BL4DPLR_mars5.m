% caller_BL4DPLR is a script to run the DEKAF solver for the verification of
% DEKAF against DPLR for a highly reacting case.
%
% Date: January 2018
%{
clear;
close all;
%clc
%}

clear intel options
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Optional inputs
Tw = 3000;                                            % wall temperature [K]
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.thermal_BC              = 'Twall';              % Wall bc (g or dg/deta)
% options.thermal_BC              = 'adiab';              % Wall bc (g or dg/deta)
options.G_bc                    = Tw;                 % Value for g boundary condition
options.flow            = 'CNE';
options.mixture                 = 'mars5Park94';          % mixture
options.modelCollisionNeutral   = 'mars5dplr';
options.modelTransport          = 'Brokaw58';           % transport model
options.modelDiffusion          = 'cstSc';              % transport model

% options.elecStates              = 'mutation';           % source of the quantum electronic states

options.tol                     = 1e-14;                % Convergence tolerance
options.it_max                  = 100;
options.perturbRelaxation       = 1;
options.T_tol                   = 1e-13;
options.T_itMax                 = 200;
options.plot_transient          = false;                % to plot the inter-iteration profiles
options.plotRes                 = false;                % to plot the residual convergence

%{%
% Flow parameters
M_e = 30;
T_e = 200;
Re1_e = 5e6;
Sc_e = 0.5;
N_x                     = 10;
intel.x_e               = linspace(0,1,N_x);
intel.M_e0              = M_e;
intel.T_e0              = T_e;
intel.Re1_e0            = Re1_e;
intel.Sc_e              = Sc_e;                 % Schmidt number

options.shockJump       = true;
intel.wedge_angle       = 20;
options.shockType       = 'wedge';
options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';
options.xSpacing        = 'tanh';
options.tol             = 1e-9;
options.it_max          = 50;
options.mapOpts.x_start = 1e-7;
options.mapOpts.x_end   = intel.x_e(end);
options.mapOpts.N_x     = 300;
options.ox              = 2;

options.plot_marching   = true;
options.dimoutput       = true;
[intel1,options] = DEKAF(intel,options,'marching');

%% saving
savepath = ['DEKAF_',options.flow,'_',options.mixture(1:min(5,length(options.mixture))),'_M',num2str(M_e),'_T',...
    num2str(T_e),'_Tw',num2str(Tw),'_Neta',num2str(options.N),'_Nx',num2str(options.mapOpts.N_x ),'_x',...
    num2str(options.mapOpts.x_start),'to',num2str(options.mapOpts.x_end),'.mat'];
% savepath = ['DEKAF_',options.flow,'_',options.mixture(1:min(5,length(options.mixture))),'_M',num2str(M_e),'_T',...
%     num2str(T_e),'_adiab_Neta',num2str(options.N),'_Nx',num2str(options.mapOpts.N_x ),'_x',...
%     num2str(options.mapOpts.x_start),'to',num2str(options.mapOpts.x_end),'.mat'];
saveLight(savepath,intel_dimVars,options,{'h'})

%% plotting
clear plots legends
figure
idx = 20:20:options.mapOpts.N_x ;
[N_i,N_j] = optimalPltSize(intel_dimVars.mixCnst.N_spec+3);
it = 0;
for ii=idx
    it=it+1;
    legends{it} = ['x = ',num2str(intel_dimVars.x(1,ii)),' m'];
    subplot(N_i,N_j,1);
    plots(it) = plot(intel_dimVars.u(:,ii),intel_dimVars.eta,'Color',color_rainbow(ii,options.mapOpts.N_x )); hold on;
    xlabel('u [m/s]]'); ylabel('\eta [-]'); ylim([0,5]);
    subplot(N_i,N_j,2);
    plot(intel_dimVars.T(:,ii),intel_dimVars.eta,'Color',color_rainbow(ii,options.mapOpts.N_x )); hold on;
    xlabel('T [K]]'); ylabel('\eta [-]'); ylim([0,5]);
    subplot(N_i,N_j,3);
    plot(intel_dimVars.v(:,ii),intel_dimVars.eta,'Color',color_rainbow(ii,options.mapOpts.N_x )); hold on;
    xlabel('v [m/s]]'); ylabel('\eta [-]'); ylim([0,5]);
    for s=1:intel_dimVars.mixCnst.N_spec
    subplot(N_i,N_j,s+3);
    plot(intel_dimVars.ys{s}(:,ii),intel_dimVars.eta,'Color',color_rainbow(ii,options.mapOpts.N_x )); hold on;
    xlabel(['y_{',intel_dimVars.mixCnst.spec_list{s},'} [-]']); ylabel('\eta [-]'); ylim([0,5]);
    end
end
ylim([0,5]);
grid minor
legend(plots,legends);

figure
semilogy(intel_dimVars.x(1,:),intel_dimVars.v(1,:),'k');
xlabel('x [m]'); ylabel('v [m/s]');
grid minor

%% Outputting profile for DPLR
x_target = input('At what x location should we take the profile? ');
idx_x = find(intel_dimVars.x(1,:)>x_target,1,'first');

[x,delta,idx] = getBL_delta_fromPath(savepath,0.005,'chooseXStart','fitSqrtX');

Y = intel_dimVars.y(:,idx_x);
T = intel_dimVars.T(:,idx_x);
U = intel_dimVars.u(:,idx_x);
V = intel_dimVars.v(:,idx_x);
for s=1:intel_dimVars.mixCnst.N_spec
Rhos{s} = intel_dimVars.rhos{s}(:,idx_x);
end

% Plotting y profiles
figure
[N_i,N_j] = optimalPltSize(length(Rhos)+4);
subplot(N_i,N_j,1)
plot(U,Y,'.');
xlabel('u [m/s]'); ylabel('y [m]');

subplot(N_i,N_j,2)
plot(V,Y,'.');
xlabel('v [m/s]'); ylabel('y [m]');
title([options.mixture,' at x = ',num2str(x(1)),' m in ',options.flow]);

subplot(N_i,N_j,3)
plot(T,Y,'.');
xlabel('T [K]'); ylabel('y [m]');

for s=1:intel_dimVars.mixCnst.N_spec
    subplot(N_i,N_j,4+s)
    plot(Rhos{s},Y,'.');
    xlabel(['\rho_{',intel_dimVars.mixCnst.spec_list{s},'} [kg/m^3]']); ylabel('y [m]');

    subplot(N_i,N_j,4)
    plot(intel_dimVars.ys{s}(:,idx_x),Y,'Color',color_rainbow(s,intel_dimVars.mixCnst.N_spec));
    hold on
end
legend(intel_dimVars.mixCnst.spec_list);
xlabel('y_s [-]'); ylabel('y [m]');

save('profile_M.mat','U','V','Y','T','Rhos');

clear T U V Rhos
it = 0;
for ii=1:length(idx)
    T(ii) = interp1(real(intel_dimVars.y(:,ii+idx_x-1)),real(intel_dimVars.T(:,ii+idx_x-1)),real(delta(ii)),'spline');
    U(ii) = interp1(real(intel_dimVars.y(:,ii+idx_x-1)),real(intel_dimVars.u(:,ii+idx_x-1)),real(delta(ii)),'spline');
    V(ii) = interp1(real(intel_dimVars.y(:,ii+idx_x-1)),real(intel_dimVars.v(:,ii+idx_x-1)),real(delta(ii)),'spline');
    for s=1:intel_dimVars.mixCnst.N_spec
    Rhos{s}(ii) = interp1(real(intel_dimVars.y(:,ii+idx_x-1)),real(intel_dimVars.rhos{s}(:,ii+idx_x-1)),real(delta(ii)),'spline');
    end
end

save('profile_M2.mat','U','V','x','T','Rhos');


% Plotting y profiles
figure
[N_i,N_j] = optimalPltSize(length(Rhos)+4);
subplot(N_i,N_j,1)
plot(x,U,'.');
ylabel('u [m/s]'); xlabel('x [m]');

subplot(N_i,N_j,2)
plot(x,V,'.');
ylabel('v [m/s]'); xlabel('x [m]');

subplot(N_i,N_j,3)
plot(x,T,'.');
ylabel('T [K]'); xlabel('x [m]');

for s=1:intel_dimVars.mixCnst.N_spec
    subplot(N_i,N_j,4+s)
    plot(x,Rhos{s},'.');
    ylabel(['\rho_{',intel_dimVars.mixCnst.spec_list{s},'} [kg/m^3]']); xlabel('x [m]');
end