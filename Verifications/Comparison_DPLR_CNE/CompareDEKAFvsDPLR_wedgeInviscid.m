% CompareDEKAFvsDPLR_wedgeInviscid is a script to plot the comparisson
% between the inviscid flowfield obtained with DPLR and DEKAF for the
% 15-deg wedge at Mach 35, 220K isothermal (4000K) case.
%
% Author: Fernando Miro Miro
% Date: April 2018

% loading files
clear;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'_stable']));
% DEKAF_path = 'DEKAF_wedge15deg_CNE_air5m_M35_T220_Tw4000_Neta100_Nxi500_x0to1_wrongShock.mat';
DEKAF_path = 'DEKAF_wedge15deg_CNE_air5m_M35_T220_Tw4000_Neta100_Nxi1000_x1e-07to1.mat';
DPLRViscous_path = 'DPLR_M35Tw4000Wedge.dat';
DPLRInviscid_path = 'DPLR_M35Tw4000WedgeInviscid.dat';
DEKAF = load(DEKAF_path);
DPLR_data = load(DPLRViscous_path); %
DPLRI_data = load(DPLRInviscid_path); %

varList_DPLR = {'x','y','p','T','M','u','v','C_n2','C_o2','C_no','C_n','C_o','h_s'}; % lists of variables outputted by DPLR
DPLR_ysList = {'C_n','C_o','C_no','C_n2','C_o2'};

N_i = 1902;         % size of the grid in the i direction (horizontal)
N_j = 352;          % size of the grid in the j direction (vertical)
x_0 = 0.05;         % margin left for the symmetry boundary in DPLR
wedge_angle = 15;   % degrees

% rotating x and y
XY_idx = cellfun(@(cll)find(ismember(varList_DPLR,cll)),{'x','y'}); % positions of x and y in the varList vector
uv_idx = cellfun(@(cll)find(ismember(varList_DPLR,cll)),{'u','v'}); % positions of u and v in the varList vector
XY = DPLR_data(:,XY_idx);
XY(:,1) = XY(:,1) - x_0;
uv = DPLR_data(:,uv_idx);
rot_mat = [cosd(wedge_angle) , sind(wedge_angle);...
          -sind(wedge_angle) , cosd(wedge_angle)];
DPLR_data(:,XY_idx) = (rot_mat * XY.').';
DPLR_data(:,uv_idx) = (rot_mat * uv.').';

XYI = DPLRI_data(:,XY_idx); % same story for the inviscid case
XYI(:,1) = XYI(:,1) - x_0;
uvI = DPLRI_data(:,uv_idx);
DPLRI_data(:,XY_idx) = (rot_mat * XYI.').';
DPLRI_data(:,uv_idx) = (rot_mat * uvI.').';

for ii=1:length(varList_DPLR) % looping DPLR variables
    DPLR_fields.(varList_DPLR{ii}) = reshape(DPLR_data(:,ii),[N_i,N_j])'; % extracting and reshaping variable
    DPLRI_fields.(varList_DPLR{ii}) = reshape(DPLRI_data(:,ii),[N_i,N_j])'; % extracting and reshaping variable
end % closing variable loop

%% Computing shock position in DPLR
U_inf = DPLR_fields.u(end,end)/cosd(wedge_angle);                           % freestream conditions in DPLR
T_inf = DPLR_fields.T(end,end);
y_sinf = [DPLR_fields.C_n(end,end) ,...
          DPLR_fields.C_o(end,end) ,...
          DPLR_fields.C_no(end,end) ,...
          DPLR_fields.C_n2(end,end) ,...
          DPLR_fields.C_o2(end,end) ];
p_inf   = DPLR_fields.p(end,end);
tol = 1e-5;                                                                 % shock position tolerance
[xBL,deltaBL] = getBL_delta_fromPath(DEKAF_path,0.005,1,'noSave','fitSqrtX','quiet'); % boundary-layer limit in DEKAF (asumed the same for DPLR)
deltaBL = real(deltaBL);            deltaBL(deltaBL<0) = 0;                 % making BL limit real and >0
for ix = 1:N_i                                                              % looping x to get DPLR shock location and edge conditions
    pos1 = find(abs(DPLR_fields.u(:,ix)-DPLR_fields.u(end,end))>tol,1,'last');  % position of the shock in the y vector
    shock_pos(ix) = DPLR_fields.y(pos1,ix);                                     % height of the shock
    pos1I = find(abs(DPLRI_fields.u(:,ix)-DPLRI_fields.u(end,end))>tol,1,'last'); % same story for the inviscid shock
    shockI_pos(ix) = DPLRI_fields.y(pos1I,ix);                                  % height of the shock
    deltaBLDPLR(ix) = interp1(xBL,deltaBL,DPLR_fields.x(1,ix),'spline','extrap'); % interpolating the BL height onto the DPLR x grid
    for ii=1:length(varList_DPLR)                                               % looping DPLR variables to obtain edge conditions
        DPLR_BLedge.(varList_DPLR{ii})(ix) = interp1(DPLR_fields.y(:,ix),DPLR_fields.(varList_DPLR{ii})(:,ix),real(deltaBLDPLR(ix)),'spline');
    end
end
pShock = polyfit(sqrt(DPLR_fields.x(1,:)),shock_pos,3);                     % fitting a polynomial onto the sqrt(x) - shock
shock_fit = polyval(pShock,sqrt(DPLR_fields.x(1,:))); shock_fit(shock_fit<0)=0; % evaluating fit
pShockI = polyfit(sqrt(DPLRI_fields.x(1,:)),shockI_pos,3);                  % same story for the inivsicid shock
shockI_fit = polyval(pShockI,sqrt(DPLRI_fields.x(1,:))); shockI_fit(shockI_fit<0)=0;
figure;                                                                     % plotting fit vs original shock
plot(DPLR_fields.x(1,:),shock_pos,DPLR_fields.x(1,:),shock_fit); hold on
plot(DPLRI_fields.x(1,:),shockI_pos,DPLRI_fields.x(1,:),shockI_fit);
legend('Viscous original','Viscous fitted','Inviscid original','Inviscid fitted'); ylabel('relative shock height [m]'); xlabel('x [m]');

% Computing frozen and equilibrium shocks
mixCnst = DEKAF.mixCnst;                                                    % extracting mixture constants from DEKAF
opts = DEKAF.options;                                                       % extracting options and setting defaults
opts = setDefaults(opts);
[mixCnst.LTEtablePath,mixCnst.XEq] = checkCompositionLTE(y_sinf,mixCnst.Mm_s,mixCnst.R0,opts.mixture,opts);
mixCnst.LTEtableData = load(mixCnst.LTEtablePath);

gam_inf = getMixture_gam(T_inf,T_inf,T_inf,T_inf,T_inf,y_sinf,mixCnst.R_s,mixCnst.nAtoms_s,... % computing gamma
                       mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,opts);
[~,R] = getMixture_Mm_R(y_sinf,T_inf,T_inf,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron); % computing gas constant
M_inf = U_inf/sqrt(gam_inf*R*T_inf);                                        % computing Mach number
[T_froz,p_froz,u_froz,M_froz,shock_froz] = getShock_Props(T_inf,p_inf,M_inf,R,wedge_angle,gam_inf); % computing double-frozen edge conditions
[T_froz1T,p_froz1T,u_froz1T,shock_froz1T,bConv] = getTPG1TShock_Props(U_inf,T_inf,p_inf,y_sinf,mixCnst,wedge_angle,opts); % computing chem-froz & vib-eq. edge conditions
[T_eq,p_eq,u_eq,y_seq,shock_eq] = getLTEShock_Props(U_inf,T_inf,p_inf,mixCnst,wedge_angle,opts); % computing equilibrium edge conditions

%% Extracting streamlines
N_str = 30;
x0_str = DPLR_fields.x(end,end);
y0_str = linspace(0,DPLR_fields.y(end,end),N_str+1); y0_str(1) = [];
for i_str = 1:N_str
    [x_str{i_str},y_str{i_str}] = get2DStreamline(DPLR_fields.x,DPLR_fields.y,DPLR_fields.u,DPLR_fields.v,x0_str,y0_str(i_str));
end

%% plotting
ii = 0;
Nlines = 6;
lineLabels = {'DEKAF edge'...
       ,'DPLR Viscous'...
       ,'DPLR Inviscid'...
       ,'vib & chem frozen'...
       ,'chem frozen'...
       ,'equilibrium'...
       };
colors = 1:6;
rbwfact = 0.8; % rainbow darkening factor
N_plts = 4+mixCnst.N_spec;
[Ni,Nj] = optimalPltSize(N_plts);
Ni = Ni + 1; % we add a row of plots for the shock location vs x
StyleS = {'-','-.','--',':'};
% StyleS = {'-','-','-','-'};
StyleS = repmat(StyleS,[1,10]);
linewidths = 2*[1,1,1,1.5]; linewidths = repmat(linewidths,[1,10]);

figure
ii=ii+1; subplot(Ni,Nj,[ii:ii+Nj-1]); ic=1;
plot(xBL,deltaBL,                                               StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1; hold on;
plot(DPLR_fields.x(1,:),shock_fit,                              StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DPLRI_fields.x(1,:),shockI_fit,                            StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,:),DEKAF.x(1,:)*tand(shock_froz-wedge_angle),    StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,:),DEKAF.x(1,:)*tand(shock_froz1T-wedge_angle),  StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,:),DEKAF.x(1,:)*tand(shock_eq-wedge_angle),      StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
xlabel('x [m]'); ylabel('y [m]'); xlim([0,Inf]); ylim([0,1.2*max(shock_fit)]); %daspect([1,0.5,1]); grid minor;
for i_str = 1:N_str
    plot(x_str{i_str},y_str{i_str},'Color',[0.5,0.5,0.5]);
end
ii=ii+Nj-1;

ii=ii+1; subplot(Ni,Nj,ii); ic=2;
plot(DPLR_fields.x(1,2:end),atand(diff(shock_fit)./diff(DPLR_fields.x(1,:))),   StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic)); ic=ic+1; hold on;
plot(DPLRI_fields.x(1,2:end),atand(diff(shockI_fit)./diff(DPLRI_fields.x(1,:))),StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic)); ic=ic+1;
plot(DEKAF.x(1,[1,end]),(shock_froz-wedge_angle)*[1,1],                         StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,[1,end]),(shock_froz1T-wedge_angle)*[1,1],                       StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,[1,end]),(shock_eq-wedge_angle)*[1,1],                           StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
xlabel('x [m]'); ylabel('rel. shock angle [deg]'); grid minor; xlim([0,Inf]); ylim([0,2*(shock_froz-wedge_angle)]);

ii=ii+1; subplot(Ni,Nj,ii); ic=1;
plot(DEKAF.x(1,:),DEKAF.T(1,:),                 StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic)); ic=ic+1; hold on;
plot(DPLR_BLedge.x,DPLR_BLedge.T,               StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DPLRI_fields.x(1,:),DPLRI_fields.T(1,:),   StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,[1,end]),T_froz*[1,1],           StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,[1,end]),T_froz1T*[1,1],         StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,[1,end]),T_eq*[1,1],             StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
xlabel('x [m]'); ylabel('T_e [K]'); grid minor; xlim([0,Inf]);

ii=ii+1; subplot(Ni,Nj,ii); ic=1;
plot(DEKAF.x(1,:),DEKAF.p_e,                    StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic)); ic=ic+1; hold on;
plot(DPLR_BLedge.x,DPLR_BLedge.p,               StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DPLRI_fields.x(1,:),DPLRI_fields.p(1,:),   StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,[1,end]),p_froz*[1,1],           StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,[1,end]),p_froz1T*[1,1],         StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,[1,end]),p_eq*[1,1],             StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
xlabel('x [m]'); ylabel('p_e [K]'); grid minor; xlim([0,Inf]); ylim([0.5*p_eq,1.5*p_froz]);

ii=ii+1; subplot(Ni,Nj,ii); ic=1;
plot(DEKAF.x(1,:),DEKAF.u(1,:),                 StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;  hold on;
plot(DPLR_BLedge.x,DPLR_BLedge.u,               StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DPLRI_fields.x(1,:),DPLRI_fields.u(1,:),   StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,[1,end]),u_froz*[1,1],           StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,[1,end]),u_froz1T*[1,1],         StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
plot(DEKAF.x(1,[1,end]),u_eq*[1,1],             StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
xlabel('x [m]'); ylabel('u_e [m/s]'); grid minor; xlim([0,Inf]); ylim([0.99*u_froz,Inf]);

for s=1:mixCnst.N_spec
    ii=ii+1; subplot(Ni,Nj,ii); ic=1;
    plot(DEKAF.x(1,:),DEKAF.ys{s}(1,:),                         StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic)); ic=ic+1; hold on;
    plot(DPLR_BLedge.x,DPLR_BLedge.(DPLR_ysList{s}),            StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
    plot(DPLRI_fields.x(1,:),DPLRI_fields.(DPLR_ysList{s})(1,:),StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
    plot(DEKAF.x(1,[1,end]),y_sinf(1,s)*[1,1],                  StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
    plot(DEKAF.x(1,[1,end]),y_sinf(1,s)*[1,1],                  StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
    plot(DEKAF.x(1,[1,end]),y_seq(1,s)*[1,1],                   StyleS{ic},'Color',rbwfact*color_rainbow(colors(ic),Nlines),'linewidth',linewidths(ic));ic=ic+1;
    xlabel('x [m]'); ylabel(['y_{',mixCnst.spec_list{s},'} [-]']); grid minor; xlim([0,Inf]);
end
legend(lineLabels{:});