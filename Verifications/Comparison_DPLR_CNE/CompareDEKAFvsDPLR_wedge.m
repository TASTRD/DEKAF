% CompareDEKAFvsDPLR_wedge is a script to plot the comparisson between the
% flowfield obtained with DPLR and DEKAF for the 15-deg wedge at Mach 35,
% 220K isothermal (4000K) case.
%
% Author: Fernando Miro Miro
% Date: March 2018

%{
% loading files
clear;
addpath(genpath(getenv('DEKAF_DIRECTORY')));
% DEKAF_path = 'DEKAF_wedge15deg_CNE_air5m_M35_T220_Tw4000_Neta100_Nxi500_x0to1_wrongShock.mat';
DEKAF_path = 'DEKAF_wedge15deg_CNE_air5m_M35_T220_Tw4000_Neta100_Nxi1000_x1e-07to1.mat';
DEKAF = load(DEKAF_path);
DPLR_data = load('DPLR_M35Tw4000Wedge.dat'); %

varList_DPLR = {'x','y','p','T','M','u','v','C_n2','C_o2','C_no','C_n','C_o','h_s'}; % lists of variables outputted by DPLR

N_i = 1902; % size of the grid in the i direction (horizontal)
N_j = 352; % size of the grid in the j direction (vertical)
x_0 = 0.05; % margin left for the symmetry boundary in DPLR
wedge_angle = 15; % degrees

% rotating x and y
XY_idx = cellfun(@(cll)find(ismember(varList_DPLR,cll)),{'x','y'}); % positions of x and y in the varList vector
uv_idx = cellfun(@(cll)find(ismember(varList_DPLR,cll)),{'u','v'}); % positions of u and v in the varList vector
XY = DPLR_data(:,XY_idx);
XY(:,1) = XY(:,1) - x_0;
uv = DPLR_data(:,uv_idx);
rot_mat = [cosd(wedge_angle) , sind(wedge_angle);...
          -sind(wedge_angle) , cosd(wedge_angle)];
DPLR_data(:,XY_idx) = (rot_mat * XY.').';
DPLR_data(:,uv_idx) = (rot_mat * uv.').';

for ii=1:length(varList_DPLR) % looping DPLR variables
    DPLR_fields.(varList_DPLR{ii}) = reshape(DPLR_data(:,ii),[N_i,N_j])'; % extracting and reshaping variable
end % closing variable loop
%}
%% Plotting
clear data2save
DPLR_x = DPLR_fields.x(1,:); % rotating and translating to DEKAF's x (surface)
DEKAF_x = DEKAF.x(1,:);
x_plot = 0.2:0.2:1;lnwdth = 2;
[Ni,Nj] = optimalPltSize(10);
%{%
for ix = 1:length(x_plot)
    [~,idx_DKF] = min(abs(DEKAF_x-x_plot(ix)));
    [~,idx_DPLR] = min(abs(DPLR_x-x_plot(ix)));
    y_max = real(DEKAF.y(round(end/3),idx_DKF));
    ic=1;
    figure
    subplot(Ni,Nj,ic); ic=ic+1;
    plot(DEKAF.u(:,idx_DKF),DEKAF.y(:,idx_DKF),'k-','linewidth',lnwdth); hold on;
    plot(DPLR_fields.u(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--','linewidth',lnwdth,'linewidth',lnwdth); grid minor;
    xlabel('u [m/s]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(Ni,Nj,ic); ic=ic+1;
    plot(DEKAF.v(:,idx_DKF),DEKAF.y(:,idx_DKF),'k-','linewidth',lnwdth); hold on;
    plot(DPLR_fields.v(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--','linewidth',lnwdth,'linewidth',lnwdth); grid minor;
    xlabel('v [m/s]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(Ni,Nj,ic); ic=ic+1;
    plot(DEKAF.T(:,idx_DKF),DEKAF.y(:,idx_DKF),'k-','linewidth',lnwdth); hold on;
    plot(DPLR_fields.T(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--','linewidth',lnwdth); grid minor;
    xlabel('T [K]'); ylabel('y [m]'); ylim([0,y_max]);
%     title(['Re = ',num2str(Re_plot(ix))])
    title(['x = ',num2str(x_plot(ix)),' m'])

    subplot(Ni,Nj,ic); ic=ic+1;
    plot(DEKAF.p_e(idx_DKF)*ones(DEKAF.options.N,1),DEKAF.y(:,idx_DKF),'k-','linewidth',lnwdth); hold on;
    plot(DPLR_fields.p(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--','linewidth',lnwdth); grid minor;
    xlabel('p [Pa]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(Ni,Nj,ic); ic=ic+1;
    plot(DEKAF.ys{1}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k-','linewidth',lnwdth); hold on;
    plot(DPLR_fields.C_n(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--','linewidth',lnwdth); grid minor;
    xlabel('y_{N} [-]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(Ni,Nj,ic); ic=ic+1;
    plot(DEKAF.ys{2}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k-','linewidth',lnwdth); hold on;
    plot(DPLR_fields.C_o(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--','linewidth',lnwdth); grid minor;
    xlabel('y_{O} [-]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(Ni,Nj,ic); ic=ic+1;
    plot(DEKAF.ys{3}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k-','linewidth',lnwdth); hold on;
    plot(DPLR_fields.C_no(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--','linewidth',lnwdth); grid minor;
    xlabel('y_{NO} [-]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(Ni,Nj,ic); ic=ic+1;
    plot(DEKAF.ys{4}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k-','linewidth',lnwdth); hold on;
    plot(DPLR_fields.C_n2(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--','linewidth',lnwdth); grid minor;
    xlabel('y_{N2} [-]'); ylabel('y [m]'); ylim([0,y_max]);

    subplot(Ni,Nj,ic); ic=ic+1;
    plot(DEKAF.ys{5}(:,idx_DKF),DEKAF.y(:,idx_DKF),'k-','linewidth',lnwdth); hold on;
    plot(DPLR_fields.C_o2(:,idx_DPLR),DPLR_fields.y(:,idx_DPLR),'r--','linewidth',lnwdth); grid minor;
    xlabel('y_{O2} [-]'); ylabel('y [m]'); ylim([0,y_max]);
    legend('DEKAF','DPLR');

    %% Saving for thesis
    data2save(ix).yDKF   = real(DEKAF.y(:,idx_DKF));
    data2save(ix).uDKF   = real(DEKAF.u(:,idx_DKF));
    data2save(ix).vDKF   = real(DEKAF.v(:,idx_DKF));
    data2save(ix).TDKF   = real(DEKAF.T(:,idx_DKF));
    data2save(ix).YNDKF  = real(DEKAF.ys{1}(:,idx_DKF));
    data2save(ix).YODKF  = real(DEKAF.ys{2}(:,idx_DKF));
    data2save(ix).YNODKF = real(DEKAF.ys{3}(:,idx_DKF));

    data2save(ix).yCFD   = DPLR_fields.y(:,idx_DPLR);
    data2save(ix).uCFD   = DPLR_fields.u(:,idx_DPLR);
    data2save(ix).vCFD   = DPLR_fields.v(:,idx_DPLR);
    data2save(ix).TCFD   = DPLR_fields.T(:,idx_DPLR);
    data2save(ix).YNCFD  = DPLR_fields.C_n(:,idx_DPLR);
    data2save(ix).YOCFD  = DPLR_fields.C_o(:,idx_DPLR);
    data2save(ix).YNOCFD = DPLR_fields.C_no(:,idx_DPLR);
end

saveDEKAF('DKF_CNEDPLR.mat','data2save','x_plot');
%}