% generateCommand_MutationaTables_Collision.m
%
% This script generates the unix command to call mutation, generating a
% table of values for collision integrals
%
% This is a wonky workaround until I figure out how to call mutation
% from MATLAB, fixing my environmental variable's paths to point
% to the MPP_DIRECTORY

%%%% START OF INPUTS
mixture = 'air11mutation';
mixtureMut = 'air11';
p_chosen = 101325;
T_min = 200;
T_max = 20000;
dT = 50;
%%%% END OF INPUTS

%% mutation
%{%
mutdatfile = ['mutationTables_T',num2str(T_min),'-',num2str(dT),'-',num2str(T_max),'_P',...
    num2str(p_chosen),'_',mixture,'_collision.dat'];
string4mut = ['mppequil --no-header -T ',num2str(T_min),':',num2str(dT),':',num2str(T_max),' -P ',...
    num2str(p_chosen),' -m 0,1,3 -s 2 -o 11-18 ',mixtureMut,' > ',...
    mutdatfile];
% 11-18 are Q11ij, Q22ij, Bstij, Cstij, Q11ei, Q22ei, Bstei & Cstei

% To run Mutation++
fprintf(string4mut)
fprintf('\n')

% To view the csv output in the terminal
fprintf(['column -s, -t < ',mutdatfile,' | less -#2 -N -S']);
fprintf('\n')
