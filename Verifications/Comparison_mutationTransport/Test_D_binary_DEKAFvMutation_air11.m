% Test_D_binary_DEKAFvMutation_air11 is a script to test the multicomponent
% diffusion coefficients of air11.
%
% Author: Fernando Miro Miro
% Date: February 2018

clear;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

%%%% START OF INPUTS
% loading mutation properties
mixture = 'air11mutation';
N_spec = 11;
mutdatfile = 'mutationTables_T700-10-20000_P3000-500-4000_air11_binary_D.dat';
p_chosen = 4000;
idx_start = 6; % T,P,mu,kappaInt,kappaHTrans then all the species values

%%%% END OF INPUTS

%% Load the daters
% "you gotta write comments. Else how will anyone understand the code???"
data = load(mutdatfile);

% Starting indices for tables
idx_ys_start    = idx_start;
idx_ys_end      = idx_ys_start + N_spec - 1;
idx_xs_start    = idx_ys_end + 1;
idx_xs_end      = idx_xs_start + N_spec - 1;
idx_Dms_start    = idx_xs_end + 1;
idx_Dms_end      = idx_Dms_start + N_spec - 1;
idx_Dsl_start   = idx_Dms_end + 1;
idx_Dsl_end     = idx_Dsl_start + N_spec^2 - 1;
idx_nbDsl_start   = idx_Dsl_end + 1;
idx_nbDsl_end     = idx_nbDsl_start + N_spec*(N_spec+1)/2 - 1;

% Index booleans
idx_ys  = idx_ys_start:idx_ys_end;
idx_xs  = idx_xs_start:idx_xs_end;
idx_Dms  = idx_Dms_start:idx_Dms_end;
idx_Dsl = idx_Dsl_start:idx_Dsl_end;
idx_nbDsl = idx_nbDsl_start:idx_nbDsl_end;
p   = data(:,2); ip = (p==p_chosen);
p   = p(ip);
p       = unique(p);    N_p = length(p);
T   = data(ip,1);    N_T = length(T);

prop2comp_mut.X_s       =     reshape(data(ip,idx_xs),                                  [N_T,N_p,N_spec]);      % -s 0 : X         [-]         mole fractions
prop2comp_mut.D_sm      =     reshape(data(ip,idx_Dms),                                 [N_T,N_p,N_spec]);      % -s 18 : Dm        [-]         effective diffusion coefficients
prop2comp_mut.D_sl      =     reshape(data(ip,idx_Dsl),                                 [N_T,N_p,N_spec,N_spec]);       % -o 0 : Dsl       [m^2/s]     multicomponent diffusion coefficients
prop2comp_mut.D_sl      = permute(prop2comp_mut.D_sl,[1,2,4,3]);                               % (eta,l,s) --> (eta,s,l)
prop2comp_mut.nbD_sl    =     reshape(data(ip,idx_nbDsl),                               [N_T,N_p,N_spec*(N_spec+1)/2]); % -o 10 : nbDsl    [m^2/s]     binary diffusion coefficients times number density


%% Obtaining tabulated values
% % For Wright2005 (mutation data)
[mixCnst,opts] = getAll_constants(mixture,struct);
fldnms_mixCnst = fieldnames(mixCnst);
for ii=1:length(fldnms_mixCnst)
    eval([fldnms_mixCnst{ii},' = mixCnst.',fldnms_mixCnst{ii},';']); % e.g. hForm_s = mixCnst.hForm_s;
end
[mixCnst.LTEtablePath,mixCnst.XEq] = checkCompositionLTE({0,0,0,0,0,0,0.767,0,0.232,0,0},Mm_s,R0,mixture,opts);
mixCnst.LTEtableData = load(mixCnst.LTEtablePath);

[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,T_mat] = ... % properties as needed for the enthalpy evaluation
                       reshape_inputs4enthalpy(R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,T);

%% Equilibrium conditions
X_s = getEquilibrium_X_complete(p_chosen,T,'mtimesx',mixture,mixCnst,opts.modelEquilibrium,0,opts);
y_s = getEquilibrium_ys(X_s,Mm_s);
%% Obtaining properties from DEKAF
% enthalpy
[~,~,~,~,~,~,cvMod_s]  = getMixture_h_cp(y_s,T_mat,T_mat,T_mat,T_mat,T_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,opts);
% density
rho                 = getMixture_rho(y_s,p,T,T,R0,Mm_s,bElectron);  % computing mixture density and other properties ...
% binary diffusion coefficients
[D_sl_b_mut,nDsl_b_mut] = getPair_nD_binary_FOCE(T,T,y_s,rho,consts_Omega11,Mm_s,kBoltz,...
                                                        nAvogadro,bElectron,qEl,epsilon0,bPos);
% multicomponent diffusion coefficients
[D_sl_R,D_sm]     = getPair_D_multicomp_Ramshaw(D_sl_b_mut,y_s,T,T,...
                                        Mm_s,bElectron,R0,bPos,false);
% D_sl_Exact  = getPair_D_multicomp_Exact(D_sl_b_mut,y_s,T_mut,T_mut,...
%     Mm_s,bElectron,R0);
% D_sl_SM     = getPair_D_multicomp_StefanMaxwell(D_sl_b_mut,y_s,T_mut,T_mut,...
%                                 Mm_s,bElectron,bPos,qEl,kBoltz);

%% Plotting
spec_list = spec_list;
%{%
% Multicomponent diffusion coefficients
figure
for s=1:N_spec
    for l=1:N_spec
        subplot(N_spec,N_spec,(s-1)*(N_spec)+l)
        if s==l
%         plots(1) = semilogy(T_mut,D_sl_SM(:,s,l),'r-'); hold on;
        plots(1) = semilogy(T,D_sl_R(:,s,l),'b-'); hold on;
%         plots(3) = semilogy(T_mut,D_sl_R_YG(:,s,l),'g-.'); hold on;
        else
%         plots(1) = plot(T_mut,D_sl_SM(:,s,l),'r-'); hold on;
        plots(1) = plot(T,D_sl_R(:,s,l),'b-'); hold on;
%         plots(3) = plot(T_mut,D_sl_R_YG(:,s,l),'g-.'); hold on;
        end
        if s==l
        plots(2) = semilogy(T,prop2comp_mut.D_sl(:,1,s,l),'k--');
        else
        plots(2) = plot(T,prop2comp_mut.D_sl(:,1,s,l),'k--');
        end
        Dmax = max(prop2comp_mut.D_sl(:,1,s,l));
        Dmin = min(prop2comp_mut.D_sl(:,1,s,l));
        Dmarge = (Dmax-Dmin)/10;
%         ylim([Dmin-Dmarge , Dmax+Dmarge]);
        title([spec_list{s},' - ',spec_list{l}]);
        xlabel('T [K]'); ylabel(['D_{',spec_list{s},',',spec_list{l},'}']);
    end
end
legend(...'DEKAF Wright2005 Stefan-Maxwell',...
       'DEKAF Wright2005 Ramshaw',...
       ...'DEKAF GuptaYos90 Ramshaw',...
       'Mutation++'...
       );
%% Effective diffusion coefficients
figure
[NE_i,NE_j] = optimalPltSize(N_spec);
for s=1:N_spec
    subplot(NE_i,NE_j,s);
    plot(T,D_sm(:,s),'b-');
    hold on;
%     plot(T_mut,D_sm_YG(:,s),'g-.');
    plot(T,prop2comp_mut.D_sm(:,s),'k--');
    title(spec_list{s});
    xlabel('T [K]'); ylabel(['D_{',spec_list{s},'}']);
end
% legend('DEKAF Wright2005 Ramshaw','DEKAF GuptaYos90 Ramshaw','Mutation++');
legend('DEKAF Wright2005 Ramshaw','Mutation++');
   %}
%% Binary diffusion coefficients
figure
it = 0;
[NB_i,NB_j] = optimalPltSize(size(prop2comp_mut.nbD_sl,3));
for s=2:N_spec
    for l=s:N_spec
        it = it+1;
        subplot(NB_i,NB_j,it);
        plot(T,nDsl_b_mut(:,s,l),'b-');
        hold on
%         plot(T_mut,nD_sl_b_YG(:,s,l),'g-.');
        plot(T,prop2comp_mut.nbD_sl(:,1,it),'k--');
        xlabel('T [K]'); ylabel(['binary n*D_{',spec_list{s},',',spec_list{l},'}']);
        title([spec_list{s},' <-> ',spec_list{l}]);
    end
end
% legend('DEKAF Wright2005 Ramshaw','DEKAF GuptaYos90 Ramshaw','Mutation++');
legend('DEKAF Wright2005 Ramshaw','Mutation++');

%% difference between binary diffusion coefficient and mutation
%{%
figure
it = 0;
for s=2:N_spec
    for l=s:N_spec
        it = it+1;
        subplot(NB_i,NB_j,it);
        plot(T,nDsl_b_mut(:,s,l)./prop2comp_mut.nbD_sl(:,1,it),'b-');
        ylim([0.9,1.5]);
        hold on
%         plot(T_mut,nD_sl_b_YG(:,s,l)./prop2comp_mut.nbD_sl(:,1,it),'g-.');
        xlabel('T [K]'); ylabel(['\epsilon(n*D_{',spec_list{s},',',spec_list{l},'})']);
        title([spec_list{s},' <-> ',spec_list{l}]);
    end
end
% legend('DEKAF Wright2005 Ramshaw','DEKAF GuptaYos90 Ramshaw');
legend('DEKAF Wright2005 Ramshaw');
%}