% Test_muKappa_DEKAFvsMutation_air5 is a script to test the
% thermodynamic transport properties as approximated by Gupta Yos 1990's
% paper by comparing them against mutation++'s results, as well as
% DEKAF's first approximation of Chapman Enskog equations.
%
% See generate_GuptaYos_MutationaTables.m
%
% Author: Ethan Beyak & Fernando Miro
% Date: January 2018

clear;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'_stable/SourceCode']))


error('Error: This function was not updated when merging the two-T branch with the master (on 12-5-2018 FMM could not be bothered). A much simpler transport-property-comparing script should be produced profiting of the newly available tools.');

%%%% START OF INPUTS
% loading mutation properties
mixture = 'air5mutation';
mutdatfile = 'mutationTables_T200-10-20000_P400-400-4000_air5_transport_Ethan.dat';
p_chosen = 4000;
idx_start = 9; % T,P,mu,kappaMix,kappaReac,kappaInt,kappaHeavyTrans,kappaElTrans then all the species values

Sc = 0.71; % only to calc the D_s spec-mixture coeff... not a big deal.

%%%% END OF INPUTS

% string4mut = ['mppequil -T ',num2str(T_min),':',num2str(dT),':',num2str(T_max),' -P ',...
%     num2str(p_min),':',num2str(dp),':',num2str(p_max),' -m 0,1,32,33,34 -s 2,0 -o 0 ',mixture,' > ',...
%     saveFileName]; % numbers correspond to T,P,mu, mixture thermal conductivity, reactive thermal conductivity, mass fractions, mole fractions, multicomponent diffusion coeff.

%% Load the daters
data = load(mutdatfile);

%% Extracting mutation data for chosen pressure
N_spec = str2double(regexp(mixture,'\d+','match','once')); % first digit in mixture string
%{
% Starting indices for tables
idx_ys_start    = idx_start;
idx_ys_end      = idx_ys_start + N_spec - 1;
idx_xs_start    = idx_ys_end + 1;
idx_xs_end      = idx_xs_start + N_spec - 1;
idx_dXsdT_start   = idx_xs_end + 1;
idx_dXsdT_end     = idx_dXsdT_start + N_spec-1;

% Index booleans
idx_ys  = idx_ys_start:idx_ys_end;
idx_xs  = idx_xs_start:idx_xs_end;
idx_dXsdT = idx_dXsdT_start:idx_dXsdT_end;
p_mut   = data(:,2); ip = (p_mut==p_chosen);
p_mut   = p_mut(ip);
p_mut       = unique(p_mut);    N_p = length(p_mut);
T_mut       = unique(data(ip,1));    N_T = length(T_mut);

mut.mu        =     reshape(data(ip,3),                                        [N_T,1]);            % 32: mu        [Pa-s]      dynamic viscosity
mut.kappaMix  =     reshape(data(ip,4),                                        [N_T,1]);            % 33: lambda    [W/m-K]     mixture equilibrium thermal conductivity
mut.kappaReac =     reshape(data(ip,5),                                        [N_T,1]);            % 34: lam_reac  [W/m-K]     reactive thermal conductivity
mut.kappaInt  =     reshape(data(ip,6),                                        [N_T,1]);            % 37: lam_int   [W/m-K]     internal energy thermal conductivity
mut.kappaHTrans  =  reshape(data(ip,7),                                        [N_T,1]);            % 38: lam_h     [W/m-K]     heavy particle translational thermal conductivity
mut.kappaElTrans =  reshape(data(ip,8),                                        [N_T,1]);            % 39: lam_e     [W/m-K]     electron translational thermal conductivity
mut.kappaFroz = mut.kappaInt + mut.kappaHTrans + mut.kappaElTrans;
y_s     = data(ip,idx_ys);
mut.X_s        =     reshape(data(ip,idx_xs),                                  [N_T,N_spec]);      % 0 : X         [-]         mole fractions
mut.dXs_dT     =     reshape(data(ip,idx_dXsdT),                               [N_T,N_spec]);      % 1 : dXs/dT    [1/K]       temperature gradients of the mole fractions
%}
%% Alternative - using mppequilWrap
[T_mut,p_mut,mu_mut,kappa_mut,kappaReac_mut,kappaInt_mut,kappaTransH_mut,kappaEl_mut,h_mut,s_mut,cp_mut,Xs_mut,dXsdT_mut,ys_mut] = mppequilWrap([200,10,20000],[p_chosen],[0,1,32,33,34,37,38,39,10,12,9],[0,1,2],'air5');
mut.mu = mu_mut;
mut.kappaMix = kappa_mut;
mut.kappaReac = kappaReac_mut;
mut.kappaInt = kappaInt_mut;
mut.kappaHTrans = kappaTransH_mut;
mut.kappaElTrans = kappaEl_mut;
mut.kappaFroz = mut.kappaInt + mut.kappaHTrans + mut.kappaElTrans;
for s=1:N_spec
mut.X_s(:,s) = Xs_mut{s};
mut.dXs_dT(:,s) = dXsdT_mut{s};
y_s(:,s) = ys_mut{s};
end

%% Obtaining tabulated values
opts.modelEnergyModes = 'mutation';

% For GuptaYos collisional data
optsGY2.modelCollisionNeutral = 'GuptaYos90';
optsGY2.collisionRefExceptions.references = {'Yos63'};
optsGY2.collisionRefExceptions.pairs = {{'O - O','N - N'}};
optsGY2.flow = 'LTE';
mixCnstGY2 = getAll_constants(mixture,optsGY2);

% For GuptaYos collisional data
optsGY3.modelCollisionNeutral = 'GuptaYos90';
optsGY3.collisionRefExceptions.references = {};
optsGY3.collisionRefExceptions.pairs = {};
optsGY3.flow = 'LTE';
mixCnstGY3 = getAll_constants(mixture,optsGY3);

% For Wright2005 (mutation data)
opts.modelCollisionNeutral = 'Wright2005';
opts.collisionRefExceptions.references = [];
opts.collisionRefExceptions.pairs = [];

opts.modelDiffusion = 'cstSc';
% opts.modelDiffusion = 'GuptaYos90';
opts.modelEquilibrium = 'RRHO';
opts.flow = 'LTE';
[mixCnst,opts] = getAll_constants(mixture,opts);
mixCnst.XEq = 0.21/0.79; % ratio of equilibrium compositions
% extracting everything from mixCnst
fldnms_mixCnst = fieldnames(mixCnst);
for ii=1:length(fldnms_mixCnst)
    eval([fldnms_mixCnst{ii},' = mixCnst.',fldnms_mixCnst{ii},';']); % e.g. hForm_s = mixCnst.hForm_s;
end

[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,T_mat] = ... % properties as needed for the enthalpy evaluation
                       reshape_inputs4enthalpy(R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,T_mut);

%% Obtaining properties from DEKAF
% enthalpy
[~,cp,~,h_s,~,cpMod_s,cvMod_s]  = getMixture_h_cp(y_s,T_mat,T_mat,T_mat,T_mat,T_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,opts);
% density
rho                 = getMixture_rho(y_s,p_mut,T_mut,T_mut,R0,Mm_s,bElectron);  % computing mixture density and other properties ...
% reaction rates
dlnKeqrdT           = getReactionDer_dlnKeqr_dT(y_s,T_mut,delta_nuEq,Keq_struc,opts);
dlnKpeqrdT          = getEquilibriumDer_dlnKpeq_dT(dlnKeqrdT,T_mut,sum(delta_nuEq,1)');
% Mole fraction gradient with temperature
Mm                  = getMixture_Mm_R(y_s,T_mut,T_mut,Mm_s,R0,bElectron);
X_s                  = getSpecies_X(y_s,Mm,Mm_s);
[dXsdT,~]           = getEquilibriumDer_X_air5(1,X_s,p_mut,XEq,spec_list,reac_list_eq,dlnKpeqrdT);
% Diffusion coefficients from constant Schmidt number
% D_s                  = getSpecies_D_cstSchmidt(Sc,mu,rho,N_spec);
% Diffusion coefficients from multi-component diffusion
[D_sl_b_mut,nDsl_b_mut] = ...
    getPair_nD_binary_FOCE(T_mut,T_mut,y_s,rho,...
        consts_Omega11,Mm_s,...
        kBoltz,nAvogadro,bElectron,qEl,...
        epsilon0,bPos);
% [D_sl,D_sm]     = getPair_D_multicomp_Ramshaw(D_sl_b_mut,y_s,T_mut,T_mut,...
%                                         Mm_s,bElectron,R0,bPos);
D_sl     = getPair_D_multicomp_StefanMaxwell(D_sl_b_mut,y_s,T_mut,T_mut,...
                                        Mm_s,bElectron,bPos,qEl,kBoltz);
rho_s = y_s.*repmat(rho,[1,N_spec]);


% Transport
mu_Yos = ...
    applyYos67(11,y_s,T_mut,T_mut,rho,p_mut,Mm_s,Mm,R0,consts_Omega11,...
                                    consts_Omega22,nAvogadro,kBoltz,bElectron,...
                                    qEl,epsilon0,bPos);
kappaHTrans = ...
    applyYos67(21,y_s,T_mut,T_mut,rho,p_mut,Mm_s,Mm,R0,consts_Omega11,...
                                    consts_Omega22,nAvogadro,kBoltz,bElectron,...
                                    qEl,epsilon0,bPos,consts_Bstar);
[kappaInt,kappaRot,kappaVib,kappaElec] = getMixture_kappaInt_EuckenCorrection(T_mut,T_mut,T_mut,T_mut,T_mut,...
                                    y_s,rho,p_mut,R0,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,...
                                    bPos,hForm_s,nAvogadro,consts_Omega11,Mm_s,...
                                    kBoltz,qEl,epsilon0,opts);
                                %{
mu = ...
    applyChapmanEnskog(11,y_s,T_mut,T_mut,rho,p_mut,Mm_s,Mm,consts_Omega11,...
                                    consts_Omega22,nAvogadro,R0,kBoltz,bElectron,...
                                    qEl,epsilon0,bPos);
kappaHTrans2 = ...
    applyChapmanEnskog(22,y_s,T_mut,T_mut,rho,p_mut,Mm_s,Mm,consts_Omega11,...
                                    consts_Omega22,nAvogadro,R0,kBoltz,bElectron,...
                                    qEl,epsilon0,bPos,consts_Bstar);
                                %}

[mu,kappaFroz] = getTransport_mu_kappa(y_s,T_mut,T_mut,rho,p_mut,Mm,opts,mixCnst,T_mut,T_mut,T_mut,cp);
kappaHTrans2 = kappaFroz - kappaInt;
% kappaReac_mutLong           = getEquilibrium_kappareac('spec-spec',hs_mutLong,rho_s_mutLong,Dsl_mutLong,dXsdT_mutLong);
% kappaReac           = getEquilibrium_kappareac('spec-mix',hs_mutLong,rho_mutLong,Ds_mutLong,dXsdT_mutLong); % spec-mix coefficients
kappaReac           = getEquilibrium_kappareac('spec-spec',h_s,rho_s,D_sl,dXsdT); % spec-spec coefficients
kappaEl             = zeros(size(T_mut));
kappaFroz           = kappaHTrans + kappaInt + kappaEl;
kappaFroz2          = kappaHTrans2 + kappaInt + kappaEl;
kappaMix            = kappaFroz + kappaReac; % neglect thermal (aka soret) diffusion for now
kappaMix2           = kappaFroz2 + kappaReac; % neglect thermal (aka soret) diffusion for now

% passing everything to a structure like in mutation
Yos67.mu          = mu_Yos;
Yos67.kappaMix    = kappaMix;
Yos67.kappaReac   = kappaReac;
Yos67.kappaFroz   = kappaFroz;
Yos67.kappaInt    = kappaInt;
Yos67.kappaRot    = kappaRot;
Yos67.kappaVib    = kappaVib;
Yos67.kappaElec   = kappaElec;
Yos67.kappaHTrans = kappaHTrans;
Yos67.kappaElTrans= kappaEl;
Yos67.X_s         = X_s;

CE_12 = Yos67;
CE_12.mu = mu;
CE_12.kappaHTrans = kappaHTrans2;
CE_12.kappaFroz = kappaFroz2;
CE_12.kappaMix = kappaMix2;
CE_12.kappaElTrans = kappaEl;
% prop2comp_DEKAF.Dsl_mutLong % skip for now

%% Compare entropy

% entropy
s = getMixture_s(T_mut,T_mut,T_mut,T_mut,T_mut,rho,y_s,R_s,nAtoms_s,...
                      thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,Mm_s,...
                      nAvogadro,hPlanck,kBoltz,L_s,sigma_s,thetaRot_s,opts);

figure
plot(T_mut,s,'r-',T_mut,s_mut,'k--');
% plot(T_mut,s_mut,'k-');
xlabel('T [K]'); ylabel('s [J/kg-K]'); legend('DEKAF','mutation++');

%% Compare mole fraction gradients
%{
clear plots
figure
for s=1:N_spec
%     plots(s) = plot(T_mut,dXsdT_mutLong(:,s),'Color',color_rainbow(s,N_spec)); hold on;
    plots(s) = plot(T_mut,dXsdT_mutLong(:,s)./mut.dXs_dT(:,s),'Color',color_rainbow(s,N_spec)); hold on;
%     plot(T_mut,mut.dXs_dT(:,s),'k--');
end
xlim([1500,10000]);
xlabel('T [K]');ylabel('dX_s/dT [K^{-1}]');
legend(plots,spec_list);
%}

%% Compare various transport models
% GuptaYos
GY.mu = ...
    getMixture_mu_Brokaw58(T_mut,T_mut,y_s,rho,p_mut,Mm_s,...
        R0,nAvogadro,consts_Omega22,...
        bElectron,kBoltz,qEl,epsilon0,...
        bPos);
[GY.kappaFroz,~,GY.kappaHTrans,GY.kappaInt,GY.kappaRot,GY.kappaVib,GY.kappaElec] = ...
    getMixture_kappa_GuptaYos(y_s,T_mut,T_mut,rho,p_mut,...
        consts_Omega11,consts_Omega22,...
        cvMod_s.cvRot_s,cvMod_s.cvVib_s,cvMod_s.cvElec_s,...
        Mm_s,R0,kBoltz,bElectron,...
        nAvogadro,qEl,epsilon0,bPos);
% GuptaYos with GuptaYos90 + Yos63 Data
GY2.mu = ...
    getMixture_mu_Brokaw58(T_mut,T_mut,y_s,rho,p_mut,mixCnstGY2.Mm_s,...
        mixCnstGY2.R0,mixCnstGY2.nAvogadro,mixCnstGY2.consts_Omega22,...
        mixCnstGY2.bElectron,mixCnstGY2.kBoltz,mixCnstGY2.qEl,mixCnstGY2.epsilon0,...
        mixCnstGY2.bPos);

[GY2.kappaFroz,~,GY2.kappaHTrans,GY2.kappaInt,GY2.kappaRot,GY2.kappaVib,GY2.kappaElec] = ...
    getMixture_kappa_GuptaYos(y_s,T_mut,T_mut,rho,p_mut,...
        mixCnstGY2.consts_Omega11,mixCnstGY2.consts_Omega22,...
        cvMod_s.cvRot_s,cvMod_s.cvVib_s,cvMod_s.cvElec_s,...
        mixCnstGY2.Mm_s,mixCnstGY2.R0,mixCnstGY2.kBoltz,mixCnstGY2.bElectron,...
        mixCnstGY2.nAvogadro,mixCnstGY2.qEl,mixCnstGY2.epsilon0,mixCnstGY2.bPos);
% GuptaYos with GuptaYos90
GY3.mu = ...
    getMixture_mu_Brokaw58(T_mut,T_mut,y_s,rho,p_mut,mixCnstGY3.Mm_s,...
        mixCnstGY3.R0,mixCnstGY3.nAvogadro,mixCnstGY3.consts_Omega22,...
        mixCnstGY3.bElectron,mixCnstGY3.kBoltz,mixCnstGY3.qEl,mixCnstGY3.epsilon0,...
        mixCnstGY3.bPos);

[GY3.kappaFroz,~,GY3.kappaHTrans,GY3.kappaInt,GY3.kappaRot,GY3.kappaVib,GY3.kappaElec] = ...
    getMixture_kappa_GuptaYos(y_s,T_mut,T_mut,rho,p_mut,...
        mixCnstGY3.consts_Omega11,mixCnstGY3.consts_Omega22,...
        cvMod_s.cvRot_s,cvMod_s.cvVib_s,cvMod_s.cvElec_s,...
        mixCnstGY3.Mm_s,mixCnstGY3.R0,mixCnstGY3.kBoltz,mixCnstGY3.bElectron,...
        mixCnstGY3.nAvogadro,mixCnstGY3.qEl,mixCnstGY3.epsilon0,mixCnstGY3.bPos);

% Wilke's
% Note that this model does not compute mixture internal kappa or mixture
% translational kappa -- straight for mixture frozen kappa from Wilke's
% mixing rule
[Wilke.mu,Wilke.mu_s] = ...
    getMixture_mu_BlottnerWilke(y_s,T_mut,T_mut,Mm_s,...
        Amu_s,Bmu_s,Cmu_s,...
        R0,bElectron);

Wilke.kappaFroz = ...
    getMixture_kappa_EuckenWilke(y_s,Wilke.mu_s,T_mut,T_mut,...
        cvMod_s.cvTrans_s,cvMod_s.cvRot_s,cvMod_s.cvVib_s,cvMod_s.cvElec_s,...
        Mm_s,R0,bElectron);

% Compute errors of models with respect to Mutation++'s 2nd C-E Approx.
% mu
transport.err.Yos67.mu  = (Yos67.mu - mut.mu)./mut.mu;
transport.err.CE_12.mu  = (CE_12.mu - mut.mu)./mut.mu;
transport.err.GY.mu     = (GY.mu - mut.mu)./mut.mu;
transport.err.GY2.mu    = (GY2.mu - mut.mu)./mut.mu;
transport.err.GY3.mu    = (GY3.mu - mut.mu)./mut.mu;
transport.err.Wilke.mu  = (Wilke.mu - mut.mu)./mut.mu;

% kappaFrozen
transport.err.Yos67.kappaFroz   = ...
    (Yos67.kappaFroz - mut.kappaFroz)./mut.kappaFroz;
transport.err.CE_12.kappaFroz   = ...
    (CE_12.kappaFroz - mut.kappaFroz)./mut.kappaFroz;
transport.err.GY.kappaFroz      = ...
    (GY.kappaFroz - mut.kappaFroz)./mut.kappaFroz;
transport.err.GY2.kappaFroz      = ...
    (GY2.kappaFroz - mut.kappaFroz)./mut.kappaFroz;
transport.err.GY3.kappaFroz      = ...
    (GY3.kappaFroz - mut.kappaFroz)./mut.kappaFroz;
transport.err.Wilke.kappaFroz   = ...
    (Wilke.kappaFroz - mut.kappaFroz)./mut.kappaFroz;

% kappaInt
transport.err.Yos67.kappaInt   = ...
    (Yos67.kappaInt - mut.kappaInt)./mut.kappaInt;
transport.err.CE_12.kappaInt   = ...
    (CE_12.kappaInt - mut.kappaInt)./mut.kappaInt;
transport.err.GY.kappaInt      = ...
    (GY.kappaInt - mut.kappaInt)./mut.kappaInt;
transport.err.GY2.kappaInt      = ...
    (GY2.kappaInt - mut.kappaInt)./mut.kappaInt;
transport.err.GY3.kappaInt      = ...
    (GY3.kappaInt - mut.kappaInt)./mut.kappaInt;

% kappaTrans
transport.err.Yos67.kappaHTrans = ...
    (Yos67.kappaHTrans - mut.kappaHTrans)./mut.kappaHTrans;
transport.err.CE_12.kappaHTrans = ...
    (CE_12.kappaHTrans - mut.kappaHTrans)./mut.kappaHTrans;
transport.err.GY.kappaHTrans    = ...
    (GY.kappaHTrans - mut.kappaHTrans)./mut.kappaHTrans;
transport.err.GY2.kappaHTrans    = ...
    (GY2.kappaHTrans - mut.kappaHTrans)./mut.kappaHTrans;
transport.err.GY3.kappaHTrans    = ...
    (GY3.kappaHTrans - mut.kappaHTrans)./mut.kappaHTrans;

% kappaReac
transport.err.Yos67.kappaReac = ...
    (Yos67.kappaReac - mut.kappaReac)./max(mut.kappaReac); % we normalize the reactive thermal conductivity error by the maximum, to avoid 1/0 situations
transport.err.CE_12.kappaReac = ...
    (CE_12.kappaReac - mut.kappaReac)./max(mut.kappaReac);

NPlots = 6;
iP = 1;
Styles.CE_12 = '-';    LW.CE_12 = 1;        color.CE_12 = color_rainbow(iP,NPlots)*0.8; iP = iP+1;
Styles.Yos67 = '-.';     LW.Yos67 = 1;      color.Yos67 = color_rainbow(iP,NPlots)*0.8; iP = iP+1;
Styles.GY = '--';       LW.GY = 1;          color.GY = color_rainbow(iP,NPlots)*0.8;    iP = iP+1;
Styles.GY2 = ':';       LW.GY2 = 1.5;       color.GY2 = color_rainbow(iP,NPlots)*0.8;    iP = iP+1;
Styles.GY3 = '-';       LW.GY3 = 1;         color.GY3 = color_rainbow(iP,NPlots)*0.8;    iP = iP+1;
Styles.Wilke = ':';     LW.Wilke = 1.5;     color.Wilke = color_rainbow(iP,NPlots)*0.8; iP = iP+1;
Styles.mut = '--';      LW.mut = 1;         color.mut = [0,0,0]; iP = iP+1;
% defining legends
leg = {'Chapman-Enskog 1st-2nd approx',...
       'Yos67 approx',...
       'Brokaw58 approx (Wright2005 col. data)',...
       'Brokaw58 approx (GuptaYos90 + Yos63 col. data)',...
       'Brokaw58 approx (GuptaYos90 col. data)',...
       'Blottner-Eucken-Wilke',...
       'Mutation++'};

plotLim = [0,10e3];

figure
try
suptitle('Comparison of errors of various transport models')
catch
end
% Dynamic viscosity, mu
subplot(2,3,1)
plot(T_mut,transport.err.CE_12.mu,Styles.CE_12,'LineWidth',LW.CE_12,'Color',color.CE_12); hold on;      % 1st-2nd CE
plot(T_mut,transport.err.Yos67.mu,Styles.Yos67,'LineWidth',LW.Yos67,'Color',color.Yos67); hold on;      % 1st CE
plot(T_mut,transport.err.GY.mu,   Styles.GY,   'LineWidth',LW.GY,   'Color',color.GY);    hold on;      % GuptaYos
plot(T_mut,transport.err.GY2.mu,  Styles.GY2,  'LineWidth',LW.GY2,  'Color',color.GY2);   hold on;      % GuptaYos
plot(T_mut,transport.err.GY3.mu,  Styles.GY3,  'LineWidth',LW.GY3,  'Color',color.GY3);   hold on;      % GuptaYos
plot(T_mut,transport.err.Wilke.mu,Styles.Wilke,'LineWidth',LW.Wilke,'Color',color.Wilke); hold on;      % Wilke's (Blottner + Eucken)
legend(leg(1:end-1),'location','best');
grid minor
title('Dynamic viscosity \mu');
xlabel('T [K]');
ylabel('(model - mutation++)/mutation++');
xlim(plotLim);

% Frozen thermal conductivity, kappaFroz
subplot(2,3,2)
plot(T_mut,transport.err.CE_12.kappaFroz,Styles.CE_12,'LineWidth',LW.CE_12,'Color',color.CE_12); hold on;      % 1st-2nd CE
plot(T_mut,transport.err.Yos67.kappaFroz,Styles.Yos67,'LineWidth',LW.Yos67,'Color',color.Yos67); hold on;      % 1st CE
plot(T_mut,transport.err.GY.kappaFroz,   Styles.GY,   'LineWidth',LW.GY,   'Color',color.GY);    hold on;      % GuptaYos
plot(T_mut,transport.err.GY2.kappaFroz,   Styles.GY2,   'LineWidth',LW.GY2,   'Color',color.GY2);    hold on;      % GuptaYos
plot(T_mut,transport.err.GY3.kappaFroz,   Styles.GY3,   'LineWidth',LW.GY3,   'Color',color.GY3);    hold on;      % GuptaYos
plot(T_mut,transport.err.Wilke.kappaFroz,Styles.Wilke,'LineWidth',LW.Wilke,'Color',color.Wilke); hold on;      % Wilke's (Blottner + Eucken)
grid minor
title('Frozen thermal conductivity, \kappa^{frozen}');
xlabel('T [K]');
ylabel('(model - mutation++)/mutation++');
xlim(plotLim);

% Internal thermal conductivity (rot + vib + elec)
subplot(2,3,3)
plot(T_mut,transport.err.CE_12.kappaInt,Styles.CE_12,'LineWidth',LW.CE_12,'Color',color.CE_12); hold on;      % 1st-2nd CE
plot(T_mut,transport.err.Yos67.kappaInt,Styles.Yos67,'LineWidth',LW.Yos67,'Color',color.Yos67); hold on;      % 1st CE
plot(T_mut,transport.err.GY.kappaInt,   Styles.GY,   'LineWidth',LW.GY,   'Color',color.GY);    hold on;      % GuptaYos
plot(T_mut,transport.err.GY2.kappaInt,   Styles.GY2,   'LineWidth',LW.GY2,   'Color',color.GY2);    hold on;      % GuptaYos
plot(T_mut,transport.err.GY3.kappaInt,   Styles.GY3,   'LineWidth',LW.GY3,   'Color',color.GY3);    hold on;      % GuptaYos
grid minor
title('Internal thermal conductivity, \Sigma\kappa^{int}');
xlabel('T [K]');
ylabel('(model - mutation++)/mutation++');
xlim(plotLim);

% Translational thermal conductivity
subplot(2,3,4)
plot(T_mut,transport.err.CE_12.kappaHTrans,Styles.CE_12,'LineWidth',LW.CE_12,'Color',color.CE_12); hold on;      % 1st-2nd CE
plot(T_mut,transport.err.Yos67.kappaHTrans,Styles.Yos67,'LineWidth',LW.Yos67,'Color',color.Yos67); hold on;      % 1st CE
plot(T_mut,transport.err.GY.kappaHTrans,   Styles.GY,   'LineWidth',LW.GY,   'Color',color.GY);    hold on;      % GuptaYos
plot(T_mut,transport.err.GY2.kappaHTrans,   Styles.GY2,   'LineWidth',LW.GY2,   'Color',color.GY2);    hold on;      % GuptaYos
plot(T_mut,transport.err.GY3.kappaHTrans,   Styles.GY3,   'LineWidth',LW.GY3,   'Color',color.GY3);    hold on;      % GuptaYos
grid minor
title('Translational thermal conductivity, \kappa^{trans}_H');
xlabel('T [K]');
ylabel('(model - mutation++)/mutation++');
xlim(plotLim);

% Reactiv thermal conductivity
subplot(2,3,5)
plot(T_mut,transport.err.CE_12.kappaReac,Styles.CE_12,'LineWidth',LW.CE_12,'Color',color.CE_12); hold on;      % 1st-2nd CE
plot(T_mut,transport.err.Yos67.kappaReac,Styles.Yos67,'LineWidth',LW.Yos67,'Color',color.Yos67); hold on;      % 1st CE
grid minor
title('Reactive thermal conductivity, \kappa^{reac}');
xlabel('T [K]');
ylabel('(model - mutation++)/mutation++');
xlim(plotLim);
% ylim([0,0.2]);

%% Final unified awesome plot

% obtaining plot size
var2Plot = {'mu',...
    'kappaFroz',...
    'kappaInt',...
    'kappaReac',...
    'kappaMix'};
nameFancy = {'\mu [kg/m-s]',...
    '\kappa^{frozen} [W/m-K]',...
    '\kappa^{int} [W/m-K]',...
    '\kappa^{Reac} [W/m-K]',...
    '\kappa^{Tot} [W/m-K]'};
titles = {'Dynamic viscosity',...
    'Frozen thermal conductivity',...
    'Internal thermal conductivity',...
    'Reactive thermal conductivity',...
    'Total thermal conductivity'};
plt4leg = 'mu'; % plot for the legend
Nplt = length(var2Plot);
[N_i,N_j] = optimalPltSize(Nplt+1);
it_plt = 0;

figure
for i_plt = 1:Nplt
    it_plt = it_plt+1; subplot(N_i,N_j,it_plt);
    if isfield(CE_12,var2Plot{i_plt})
        plot(T_mut,CE_12.(var2Plot{i_plt}),Styles.CE_12,'LineWidth',LW.CE_12,'Color',color.CE_12); hold on;      % 1st-2nd CE
    end
    if isfield(Yos67,var2Plot{i_plt})
        plot(T_mut,Yos67.(var2Plot{i_plt}),Styles.Yos67,'LineWidth',LW.Yos67,'Color',color.Yos67); hold on;      % 1st CE
    end
    if isfield(GY,var2Plot{i_plt})
        plot(T_mut,GY.(var2Plot{i_plt}),   Styles.GY,   'LineWidth',LW.GY,   'Color',color.GY);    hold on;      % GuptaYos
    end
    if isfield(GY2,var2Plot{i_plt})
        plot(T_mut,GY2.(var2Plot{i_plt}),   Styles.GY2,   'LineWidth',LW.GY2,   'Color',color.GY2);    hold on;      % GuptaYos
    end
    if isfield(GY3,var2Plot{i_plt})
        plot(T_mut,GY3.(var2Plot{i_plt}),   Styles.GY3,   'LineWidth',LW.GY3,   'Color',color.GY3);    hold on;      % GuptaYos
    end
    if isfield(Wilke,var2Plot{i_plt})
        plot(T_mut,Wilke.(var2Plot{i_plt}),Styles.Wilke,'LineWidth',LW.Wilke,'Color',color.Wilke); hold on;      % Wilke's (Blottner + Eucken)
    end
    if isfield(mut,var2Plot{i_plt})
        plot(T_mut,mut.(var2Plot{i_plt}),  Styles.mut,  'LineWidth',LW.mut,  'Color',color.mut);   hold on;      % mutation
    end
    if strcmp(var2Plot{i_plt},plt4leg)
        legend(leg,'location','best');
    end
    grid minor
    title(titles{i_plt});
    xlabel('T [K]');
    ylabel(nameFancy{i_plt});
    xlim(plotLim);
end
it_plt = it_plt+1; subplot(N_i,N_j,it_plt);     % Mole fractions
clear plots
for s=1:N_spec
    plots(s) = plot(T_mut,Yos67.X_s(:,s),'Color',color_rainbow(s,N_spec));  hold on;   % 1st CE
    plotMut = plot(T_mut,mut.X_s(:,s),'k--');                     % mutation
end
plots(N_spec+1) = plotMut;
legend(plots,[spec_list,'M++'],'location','northeast');
grid minor
title('Mole fractions X_s');
xlabel('T [K]');
ylabel('X_s [-]')
xlim(plotLim);

