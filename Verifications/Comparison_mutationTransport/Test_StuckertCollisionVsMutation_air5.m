% Test_StuckertCollisionVsMutation_air5 is a script to plot all the
% collision integrals coming from Stuckert's curve-fit data and compare
% them to the results returned by mutation.
%
% Author: Fernando Miro Miro
% Date: March 2018

clear; close all;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']));

%%%% INPUTS
mixture = 'air5mutation';
mixtureMut = 'air5';
opts.modelCollisionNeutral = 'Stuckert91';
opts.collisionRefExceptions.references = {'mars5dplr'};
opts.collisionRefExceptions.pairs = {{'O - O','O2 - O2','O2 - O'}};
p_chosen = 101325;
T_min = 200;
T_max = 20000;
dT = 50;
idx0 = 3;
%%%% END OF INPUTS

%% mutation
%{%
mutdatfile = 'trashme_mutation.dat';
string4mut = ['mppequil --no-header -T ',num2str(T_min),':',num2str(dT),':',num2str(T_max),' -P ',...
    num2str(p_chosen),' -m 0,1,3 -s 2 -o 11-13 ',mixtureMut,' > ',...
    mutdatfile];
unix(string4mut);
%}
% 11-12 are Q11ij, Q22ij
unix(string4mut);
%}

%% obtaining mixture data
[mixCnst,opts] = getAll_constants(mixture,opts);
N_spec = mixCnst.N_spec;
% fitting statistics
R2 = mixCnst.stats_fits.Rsq;
R2.Q11 = R2.Omega11;
R2.Q22 = R2.Omega22;
fldnms_mixCnst = fieldnames(mixCnst);
for ii=1:length(fldnms_mixCnst)
    eval([fldnms_mixCnst{ii},' = mixCnst.',fldnms_mixCnst{ii},';']); % e.g. hForm_s = mixCnst.hForm_s;
end

%% Load the daters
data = load(mutdatfile);
idx = 1;
T = data(:,idx); idx=idx+1;
p = data(:,idx); idx=idx+1;
rho = data(:,idx); idx=idx+1;
N_Qsl = (N_spec+1)*N_spec/2;
idx_ys    = idx:(idx+N_spec-1); idx = idx+N_spec;
idx_Q11sl = idx:(idx+N_Qsl-1);  idx = idx+N_Qsl;
idx_Q22sl = idx:(idx+N_Qsl-1);  idx = idx+N_Qsl;
idx_Bstsl = idx:(idx+N_Qsl-1);  idx = idx+N_Qsl;

y_s = data(:,idx_ys);
mut.Q11     = data(:,idx_Q11sl);
mut.Q22     = data(:,idx_Q22sl);
mut.Bstar   = data(:,idx_Bstsl);

unix(['rm ',mutdatfile]); % deleting generated file

%% Computing collision integrals in DEKAF
[lnOmega11_sl,Tstar] = getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega11,bPos);
lnOmega22_sl         = getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega22,bPos);
lnBstar_sl = getPair_ABCstar(Tstar,consts_Bstar);
lnCstar_sl = getPair_ABCstar(Tstar,consts_Cstar);

fit_data.Q11 = pi*exp(lnOmega11_sl);
fit_data.Q22 = pi*exp(lnOmega22_sl);
fit_data.Bstar = exp(lnBstar_sl);


%% plotting
list_vars = fieldnames(fit_data);
units = {' [m^2]',' [m^2]',' [-]'};
Nplts = N_spec*(N_spec+1)/2; % total number of collisions
[N_i,N_j] = optimalPltSize(Nplts);
for var = 1:length(list_vars)
    it = 0;
    figure(100+var)
    for s=1:N_spec
        for l=s:N_spec
                it=it+1;
                subplot(N_i,N_j,it);
                loglog(T,fit_data.(list_vars{var})(:,s,l),'r'); hold on
                loglog(T,mut.(list_vars{var})(:,it),'k--');
                title([mixCnst.spec_list{s},' <-> ',mixCnst.spec_list{l}]);
                xlabel('T [K]'); ylabel([list_vars{var},units{var}]); grid minor;
        end
    end
    legend(['DEKAF ',opts.modelCollisionNeutral],...
            'mutation++')
end
