% Test_CollisionPolyFit compares the fitted collisional data against the
% raw tabled values

clear;

mixture = 'air11mutation';
[mixCnst,opts] = getAll_constants(mixture,struct);
N_spec = mixCnst.N_spec;
N_charg = sum(mixCnst.bPos+mixCnst.bElectron);

spec_list = mixCnst.spec_list;
bCharged = getPair_bCharged(mixCnst.bPos,mixCnst.bElectron);
bIon_s = repmat(mixCnst.bPos',[N_spec,1]);
bIon_l = repmat(mixCnst.bPos,[1,N_spec]);
bIon = and(bIon_s,bIon_l);
bNeg_s = repmat(mixCnst.bElectron',[N_spec,1]);
bNeg_l = repmat(mixCnst.bElectron,[1,N_spec]);
bNeg = and(bNeg_s,bNeg_l);
bRep = or(bNeg,bIon);
bAtt = bCharged-bRep;

xlim_charg = [1e-1,1e8];
xlim_notcharg = [200,20000];

raw_data = mixCnst.stats_fits.raw_data;
R2 = mixCnst.stats_fits.Rsq;
R2.O11 = R2.Omega11;
R2.O22 = R2.Omega22;

T = logspace(log(0.1),log(1e8),4000)';
T_mat = repmat(T,[1,N_spec,N_spec]);

fit_data.O11 = exp(eval_Fit(T_mat,mixCnst.consts_Omega11));
fit_data.O22 = exp(eval_Fit(T_mat,mixCnst.consts_Omega22));
fit_data.Bstar = exp(eval_Fit(T_mat,mixCnst.consts_Bstar));
fit_data.Cstar = exp(eval_Fit(T_mat,mixCnst.consts_Cstar));

%% plotting
list_vars = fieldnames(fit_data);
Nplts = N_spec*(N_spec+1)/2 - N_charg*(N_charg+1)/2+2; % total number of collisions, minus number of charged collisions, plus two (att & rep)
[N_i,N_j] = optimalPltSize(Nplts);
for var = 1:length(list_vars)
    it = 1; AttFlag = false; RepFlag = false;
    figure(100+var)
    for s=1:N_spec
        for l=s:N_spec
            if bAtt(s,l) && ~AttFlag % repulsive collisions
                subplot(N_i,N_j,Nplts-1);
                loglog(T,fit_data.(list_vars{var})(:,s,l),'r'); hold on
                loglog(raw_data.T{s,l},raw_data.(list_vars{var}){s,l},'k--');
                xlim(xlim_charg);
                AttFlag = true;
                title([mixCnst.spec_list{s},' <-> ',mixCnst.spec_list{l},' with R^2 = ',num2str(R2.(list_vars{var})(s,l))]);
                xlabel('T [K]'); ylabel([list_vars{var},' [-]']); grid minor;
            elseif bRep(s,l) && ~RepFlag % repulsive collisions
                subplot(N_i,N_j,Nplts);
                loglog(T,fit_data.(list_vars{var})(:,s,l),'r'); hold on
                loglog(raw_data.T{s,l},raw_data.(list_vars{var}){s,l},'k--');
                xlim(xlim_charg);
                RepFlag = true;
                title([mixCnst.spec_list{s},' <-> ',mixCnst.spec_list{l},' with R^2 = ',num2str(R2.(list_vars{var})(s,l))]);
                xlabel('T [K]'); ylabel([list_vars{var},' [-]']); grid minor;
            elseif ~bCharged(s,l)                    % not charged
                subplot(N_i,N_j,it); it=it+1;
                loglog(T,fit_data.(list_vars{var})(:,s,l),'r'); hold on
                loglog(raw_data.T{s,l},raw_data.(list_vars{var}){s,l},'k--');
                xlim(xlim_notcharg);
                title([mixCnst.spec_list{s},' <-> ',mixCnst.spec_list{l},' with R^2 = ',num2str(R2.(list_vars{var})(s,l))]);
                xlabel('T [K]'); ylabel([list_vars{var},' [-]']); grid minor;
            end
        end
    end
end
