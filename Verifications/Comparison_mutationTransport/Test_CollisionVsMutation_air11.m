% Test_CollisionVsMutation_air11 is a script to plot all the collision
% integrals and compare them to the results returned by mutation.
%
% Author: Fernando Miro Miro
% Date: March 2018

clear;
%%%% INPUTS
mixture = 'air11mutation';
mixtureMut = 'air11';
p_chosen = 101325;
T_min = 200;
T_max = 60000;
dT = 50;
runMut = true; % true to run mutation with unix call. false to load a .dat file.
%%%% END OF INPUTS

%% mutation
%{%
if runMut
    mutdatfile = 'trashme_mutation.dat';
    string4mut = ['mppequil --no-header -T ',num2str(T_min),':',num2str(dT),':',num2str(T_max),' -P ',...
        num2str(p_chosen),' -m 0,1,3 -s 2 -o 11-23 ',mixtureMut,' > ',...
        mutdatfile];
    % 11-18 are Q11ij, Q22ij, Bstij, Cstij, Q11ei, Q22ei, Bstei & Cstei
    % 19-23 are Q13ei, Q14ei, Q15ei, Q23ee & Q24ee
    unix(string4mut);
else
    mutdatfile = 'mutationTables_T200-50-20000_P101325_air11mutation_collision.dat';
end
%}

%% obtaining mixture data
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% TESTING: setting data extrapolation to true!
opts.bCollisionExtrap = true;
% opts.collisionRefExceptions.references = {'mars5dplr'};
% opts.collisionRefExceptions.pairs = {{'O - O','O2 - O2','O2 - O'}};

[mixCnst,opts] = getAll_constants(mixture,opts);
[mixCnst.LTEtablePath,mixCnst.XEq] = checkCompositionLTE({0,0,0,0,0,0,0.767,0,0.232,0,0},mixCnst.Mm_s,mixCnst.R0,mixture,opts);
mixCnst.LTEtableData = load(mixCnst.LTEtablePath);
N_spec = mixCnst.N_spec;
% fitting statistics
R2 = mixCnst.stats_fits.Rsq;
R2.Q11 = R2.Omega11;
R2.Q22 = R2.Omega22;
R2.Q13 = R2.Bstar;
R2.Q14 = R2.Omega14;
R2.Q15 = R2.Omega15;
R2.Q23 = R2.Estar;
R2.Q24 = R2.Omega24;
fldnms_mixCnst = fieldnames(mixCnst);
for ii=1:length(fldnms_mixCnst)
    eval([fldnms_mixCnst{ii},' = mixCnst.',fldnms_mixCnst{ii},';']); % e.g. hForm_s = mixCnst.hForm_s;
end

%% Load the daters
data = load(mutdatfile);
idx = 1;
T = data(:,idx); idx=idx+1;
p = data(:,idx); idx=idx+1;
rho_mut = data(:,idx); idx=idx+1;
N_Qsl = (N_spec-1)*N_spec/2;
N_Qse = N_spec;
idx_ys    = idx:(idx+N_spec-1); idx = idx+N_spec;
idx_Q11sl = idx:(idx+N_Qsl-1);  idx = idx+N_Qsl;
idx_Q22sl = idx:(idx+N_Qsl-1);  idx = idx+N_Qsl;
idx_Bstsl = idx:(idx+N_Qsl-1);  idx = idx+N_Qsl;
idx_Cstsl = idx:(idx+N_Qsl-1);  idx = idx+N_Qsl;
idx_Q11el = idx:(idx+N_Qse-1);  idx = idx+N_Qse;
idx_Q22el = idx:(idx+N_Qse-1);  idx = idx+N_Qse;
idx_Bstel = idx:(idx+N_Qse-1);  idx = idx+N_Qse;
idx_Cstel = idx:(idx+N_Qse-1);  idx = idx+N_Qse;
idx_Q13el = idx:(idx+N_Qse-1);  idx = idx+N_Qse;
idx_Q14el = idx:(idx+N_Qse-1);  idx = idx+N_Qse;
idx_Q15el = idx:(idx+N_Qse-1);  idx = idx+N_Qse;
idx_Q23ee = idx;                idx = idx+1;
idx_Q24ee = idx;                idx = idx+1;

y_s_mut = data(:,idx_ys);
mut.Q11     = data(:,[idx_Q11el,idx_Q11sl]); % we put them in order e - l, and then s - l
mut.Q22     = data(:,[idx_Q22el,idx_Q22sl]);
mut.Bstar   = data(:,[idx_Bstel,idx_Bstsl]);
mut.Cstar   = data(:,[idx_Cstel,idx_Cstsl]);
mut.Q13     = data(:,idx_Q13el);
mut.Q14     = data(:,idx_Q14el);
mut.Q15     = data(:,idx_Q15el);
mut.Q23     = data(:,idx_Q23ee);
mut.Q24     = data(:,idx_Q24ee);

if runMut
    unix(['rm ',mutdatfile]); % deleting generated file
end

N_charg = sum(mixCnst.bPos+mixCnst.bElectron);
spec_list = mixCnst.spec_list;
bCharged = getPair_bCharged(mixCnst.bPos,mixCnst.bElectron);
bIon_s = repmat(mixCnst.bPos',[N_spec,1]);
bIon_l = repmat(mixCnst.bPos,[1,N_spec]);
bIon = and(bIon_s,bIon_l);
bNeg_s = repmat(mixCnst.bElectron',[N_spec,1]);
bNeg_l = repmat(mixCnst.bElectron,[1,N_spec]);
bNeg = and(bNeg_s,bNeg_l);
bRep = or(bNeg,bIon);
bAtt = bCharged-bRep;

% calculate DEKAF's mass fractions
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,T_mat] = ... % properties as needed for the enthalpy evaluation
                       reshape_inputs4enthalpy(R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,T);

% Equilibrium conditions
opts.flow = 'LTE';
[X_s,dXs_dT,~] = getEquilibrium_X_complete(p_chosen*ones(size(T)),T,'mtimesx',mixture,mixCnst,opts.modelEquilibrium,1,opts);
% X_s = getEquilibrium_X_complete(p_chosen,T,'mtimesx',mixture,mixCnst,opts.modelEquilibrium,0,opts);
y_s = getEquilibrium_ys(X_s,Mm_s);
y_s = limit_XYs(y_s,opts.ys_lim,mixCnst.idx_bath);
rho = getMixture_rho(y_s,p,T,T,R0,Mm_s,bElectron);  % computing mixture density and other properties ...

[Tred,lambda_Debye] = getPair_Tstar(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,bPos);
%
% Tstar = squeeze(Tred(:,1,1));
% figure(7)
% semilogy(T(:,1,1),Tstar(:,end,end),'-b')
%

%% Computing collision integrals in DEKAF
[lnOmega11_sl,Tstar] = getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega11,bPos);
lnOmega22_sl         = getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega22,bPos);
lnBstar_sl = getPair_ABCstar(Tstar,consts_Bstar);
lnCstar_sl = getPair_ABCstar(Tstar,consts_Cstar);
lnEstar_sl = getPair_ABCstar(Tstar,consts_Estar);

Omega11_sl = exp(lnOmega11_sl);
Omega22_sl = exp(lnOmega22_sl);
Bstar_sl = exp(lnBstar_sl);
Cstar_sl = exp(lnCstar_sl);
Estar_sl = exp(lnEstar_sl);

[Omega12_sl,Omega13_sl] = getPair_Omega12_Omega13(Omega11_sl,Bstar_sl,Cstar_sl);

lnOmega14_sl = getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega14,bPos);
lnOmega15_sl = getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega15,bPos);
lnOmega24_sl = getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega24,bPos);
% Obtaining Estar_sl from the curve fits (Estar=Omega23_sl/Omega22_sl)
lnEstar_sl = getPair_ABCstar(Tstar,consts_Estar);
% computing Omega23_sl from Estar
lnOmega23_sl = lnEstar_sl + lnOmega22_sl;

% undoing exponential
Omega14_sl = exp(lnOmega14_sl);
Omega15_sl = exp(lnOmega15_sl);
Omega23_sl = exp(lnOmega23_sl);
Omega24_sl = exp(lnOmega24_sl);

bCharged = getPair_bCharged(bPos,bElectron);
bCharged3D = repmat(permute(bCharged,[3,1,2]),[length(T),1,1]); % (s,l) --> (eta,s,l)
Omega14_sl(~bCharged3D) = Omega13_sl(~bCharged3D); % we apply it on all non-charged, but we only use the electron-neutral afterwards
Omega15_sl(~bCharged3D) = Omega13_sl(~bCharged3D);

fit_data.Q11 = pi*Omega11_sl;
fit_data.Q22 = pi*Omega22_sl;
fit_data.Bstar = Bstar_sl;
fit_data.Cstar = Cstar_sl;
fit_data.Q13 = pi*Omega13_sl;
fit_data.Q14 = pi*Omega14_sl;
fit_data.Q15 = pi*Omega15_sl;
fit_data.Q23 = pi*Omega23_sl;
fit_data.Q24 = pi*Omega24_sl;


%%
figure
subplot(1,2,1)
loglog(T,Tstar(:,1,1));xlabel('T'); ylabel('Tstar')
subplot(1,2,2)
loglog(T,lambda_Debye);xlabel('T'); ylabel('\lambda_D')


%% plotting
list_vars = fieldnames(fit_data);
units = {' [m^2]',' [m^2]',' [-]',' [-]',' [m^2]',' [m^2]',' [m^2]',' [m^2]',' [m^2]'};
% xlim_charg = [1e-1,1e8];
% xlim_notcharg = [200,20000];
Nplts = N_spec*(N_spec+1)/2 - N_charg*(N_charg+1)/2+2; % total number of collisions, minus number of charged collisions, plus two (att & rep)
[N_i,N_j] = optimalPltSize(Nplts);
for var = 5:length(list_vars)
    it = 1; AttFlag = false; RepFlag = false;
    figure(100+var)
    ii = 0;
    switch size(mut.(list_vars{var}),2)   % depending on the size we will have either:
        case N_spec*(N_spec+1)/2                % s-l collision
            for s=1:N_spec
                for l=s:N_spec
                    ii = ii+1;
                    if bAtt(s,l) && ~AttFlag % attractive collisions
                        subplot(N_i,N_j,Nplts-1);
                        loglog(T,fit_data.(list_vars{var})(:,s,l),'b'); hold on
                        loglog(T,mut.(list_vars{var})(:,ii),'k--');
                        %                 xlim(xlim_charg);
                        AttFlag = true;
                        title([mixCnst.spec_list{s},' <-> ',mixCnst.spec_list{l},' with R^2 = ',num2str(R2.(list_vars{var})(s,l))]);
                        xlabel('T [K]'); ylabel([list_vars{var},units{var}]); grid minor;
                    elseif bRep(s,l) && ~RepFlag % repulsive collisions
                        subplot(N_i,N_j,Nplts);
                        loglog(T,fit_data.(list_vars{var})(:,s,l),'b'); hold on
                        loglog(T,mut.(list_vars{var})(:,ii),'k--');
                        %                 xlim(xlim_charg);
                        RepFlag = true;
                        title([mixCnst.spec_list{s},' <-> ',mixCnst.spec_list{l},' with R^2 = ',num2str(R2.(list_vars{var})(s,l))]);
                        xlabel('T [K]'); ylabel([list_vars{var},units{var}]); grid minor;
                    elseif ~bCharged(s,l)                    % not charged
                        subplot(N_i,N_j,it); it=it+1;
                        loglog(T,fit_data.(list_vars{var})(:,s,l),'b'); hold on
                        loglog(T,mut.(list_vars{var})(:,ii),'k--');
                        %                 xlim(xlim_notcharg);
                        title([mixCnst.spec_list{s},' <-> ',mixCnst.spec_list{l},' with R^2 = ',num2str(R2.(list_vars{var})(s,l))]);
                        xlabel('T [K]'); ylabel([list_vars{var},units{var}]); grid minor;
                    end
                end
            end
        case N_spec                         % el-l collision
            [Ni_el,Nj_el] = optimalPltSize(N_spec);
            for s=1:N_spec
                subplot(Nj_el,Nj_el,s);
                loglog(T,fit_data.(list_vars{var})(:,s,bElectron),'b'); hold on
                loglog(T,mut.(list_vars{var})(:,s),'k--');
                title([mixCnst.spec_list{bElectron},' <-> ',mixCnst.spec_list{s},' with R^2 = ',num2str(R2.(list_vars{var})(bElectron,s))]);
                xlabel('T [K]'); ylabel([list_vars{var},units{var}]); grid minor;
            end
        case 1                              % el-el collision
            loglog(T,fit_data.(list_vars{var})(:,bElectron,bElectron),'b'); hold on
            loglog(T,mut.(list_vars{var})(:,1),'k--');
            title([mixCnst.spec_list{bElectron},' <-> ',mixCnst.spec_list{bElectron},' with R^2 = ',num2str(R2.(list_vars{var})(bElectron,bElectron))]);
            xlabel('T [K]'); ylabel([list_vars{var},units{var}]); grid minor;
        otherwise
            error('non-identified size');
    end

    legend(['DEKAF ',opts.modelCollisionNeutral,' & ',opts.modelCollisionChargeCharge],...
        'mutation++')
end
