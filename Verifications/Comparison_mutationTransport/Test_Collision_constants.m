% Test_Collision_constants.m is a script to test the curve fits of Gupta
% Yos 1990 paper for collision integrals and binary diffusion coefficients.
%
% Author: Ethan Beyak


%%%% START OF MAIN INPUTS

close all
clear

addpath(genpath('/home/ebeyak/workspace/DEKAF/'));
% addpath('./mat_files')
% T = linspace(10^3,3*10^4,3001)';
T = linspace(300,3*10^4,3001)';
spec_list = {'N','O','NO','N2','O2'};
csvFolder = 'GuptaYos_CollisionIntegrals_Digitized/';
fname.Bij_O2_O2_GY_89       = [csvFolder,'B_ij_O2_O2_GuptaYos89.csv'];
fname.pDij_O2_O2_GY_89      = [csvFolder,'pDij_O2_O2_GuptaYos89.csv'];
fname.piO11_O2_O2_GY_89     = [csvFolder,'pi_Omega11_O2_O2_GuptaYos89.csv'];
fname.piO22_O2_O2_GY_89     = [csvFolder,'pi_Omega22_O2_O2_GuptaYos89.csv'];
fname.piO11_O2_O2_Y_63      = [csvFolder,'pi_Omega11_O2_O2.csv'];
fname.piO22_O2_O2_Y_63      = [csvFolder,'pi_Omega22_O2_O2.csv'];
fname.piO11_N_N_Y_63        = [csvFolder,'pi_Omega11_N_N.csv'];
fname.piO22_N_N_Y_63        = [csvFolder,'pi_Omega22_N_N.csv'];
fname.piO11_O_O_Y_63        = [csvFolder,'pi_Omega11_O_O_Yos63.csv'];
fname.piO22_O_O_Y_63        = [csvFolder,'pi_Omega22_O_O_Yos63.csv'];

%%%% END OF MAIN INPUTS
Ang = char(197); % Angstrom character (for extra credit)

%% Load in external data
% Gupta Yos 1989 [pi*A^2]
digitdata.GY_89.Bij_O2_O2   = dlmread(fname.Bij_O2_O2_GY_89,'\t');
digitdata.GY_89.pDij_O2_O2  = dlmread(fname.pDij_O2_O2_GY_89,'\t');
digitdata.GY_89.piO11_O2_O2 = dlmread(fname.piO11_O2_O2_GY_89,'\t');
digitdata.GY_89.piO22_O2_O2 = dlmread(fname.piO22_O2_O2_GY_89,'\t');
% Yos 63 [T,pi*cm^2]
digitdata.Y_63.piO11_O2_O2  = dlmread(fname.piO11_O2_O2_Y_63,'\t');
digitdata.Y_63.piO22_O2_O2  = dlmread(fname.piO22_O2_O2_Y_63,'\t');
digitdata.Y_63.piO11_N_N    = dlmread(fname.piO11_N_N_Y_63,'\t');
digitdata.Y_63.piO22_N_N    = dlmread(fname.piO22_N_N_Y_63,'\t');
digitdata.Y_63.piO11_O_O    = dlmread(fname.piO11_O_O_Y_63,'\t');
digitdata.Y_63.piO22_O_O    = dlmread(fname.piO22_O_O_Y_63,'\t');
% Wright 2005 (aka, the data that Mutation++ uses currently)
% O2 to O2 collision
TMp_O2_O2     = [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000];
Q11_O2_O2     = [11.12  9.88  9.53  8.69  7.60  6.52  6.22  5.99  5.64  5.39  4.94];
Q22_O2_O2     = [12.62 11.06 10.65  9.72  8.70  7.70  7.38  7.12  6.73  6.42  5.89];
% N to N collision
TMp_N_N     = [300   500  1000  2000  4000  5000  6000  8000 10000 15000 20000];    % K
Q11_N_N     = [8.07  7.03  5.96  5.15  4.39  4.14  3.94  3.61  3.37  2.92  2.62];   % A^2;
Q22_N_N     = [9.11  7.94  6.72  5.82  4.98  4.70  4.48  4.14  3.88  3.43  3.11];
% O to O collision
TMp_O_O     = [300   500  1000  2000  4000  5000  6000  8000 10000 15000 20000];
Q11_O_O     = [8.53  7.28  5.89  4.84  4.00  3.76  3.57  3.27  3.05  2.65  2.39];
Q22_O_O     = [9.46  8.22  6.76  5.58  4.67  4.41  4.20  3.88  3.64  3.21  2.91];

%% Calculate DEKAF data
% Using GuptaYos90 as the collision data
[Omega11_sl,Omega22_sl,Bstar_sl,Dbar_binary_sl,stats_fits,cellPairs] = getPair_Collision_constants(spec_list,'GuptaYos90','');
N_spec = length(spec_list);

% hardcode the strings for 'GuptaYos90' modelDiffusion and modelCollisionNeutral
T_3D = repmat(T,[1,N_spec,N_spec]);
lnOmega11_sl    = eval_Fit(T_3D,Omega11_sl);
lnOmega22_sl    = eval_Fit(T_3D,Omega22_sl);
lnBstar_sl      = eval_Fit(T_3D,Bstar_sl);
lnDbar_sl       = eval_Fit(T_3D,Dbar_binary_sl);

%% Calculate the curve fit coefficients for Yos 63 data on O - O & N - N
TY63_N_N = digitdata.Y_63.piO11_N_N(:,1);
O11Y63_N_N = 10^16*digitdata.Y_63.piO11_N_N(:,2); % units pi*A^2

TY63_O_O = digitdata.Y_63.piO11_O_O(:,1);
O11Y63_O_O = 10^16*digitdata.Y_63.piO11_O_O(:,2); % units pi*A^2

% p's are the constants A,B,C,D for the cubic log-log fit
pY63_N_N = generateFit_PolyLogLog(TY63_N_N,O11Y63_N_N,3);
pY63_O_O = generateFit_PolyLogLog(TY63_O_O,O11Y63_O_O,3);

% exponentiate the logarithm for the raw values
fitY63_N_N = exp(polyval(pY63_N_N,log(TY63_N_N)));
fitY63_O_O = exp(polyval(pY63_O_O,log(TY63_O_O)));

%% Plot all constants calculated within DEKAF only
% figure(1)
% subplot(2,2,1)
% cnt=0;
% for s=1:N_spec
%     for l=1:s
%         cnt = cnt + 1;
%         semilogy(T,exp(lnOmega11_sl(:,s,l)),'Color',color_rainbow(cnt,15));
%         hold on
%     end
% end
% ylabel('ln(Omega_{sl}^{11}) with Omega_{sl}^{11} measured in m^2')
%
% subplot(2,2,2)
% cnt=0;
% for s=1:N_spec
%     for l=1:s
%         cnt = cnt + 1;
%         semilogy(T,exp(lnOmega22_sl(:,s,l)),'Color',color_rainbow(cnt,15));
%         hold on
%     end
% end
% ylabel('ln(Omega_{sl}^{22}) with Omega_{sl}^{22} measured in m^2')
%
% subplot(2,2,3)
% cnt=0;
% for s=1:N_spec
%     for l=1:s
%         cnt = cnt + 1;
%         semilogy(T,exp(lnDbar_sl(:,s,l)),'Color',color_rainbow(cnt,15));
%         hold on
%         lgndPair{cnt} = cellPairs{s,l};
%     end
% end
% legend(lgndPair,'location','best');
% ylabel('lnDbar_{sl}')
%
% subplot(2,2,4)
% cnt=0;
% for s=1:N_spec
%     for l=1:s
%         cnt = cnt + 1;
%         semilogy(T,exp(lnBstar_sl(:,s,l)),'Color',color_rainbow(cnt,15));
%         hold on
%     end
% end
% ylabel('lnB*_{sl}')

%% Recreate Figure 10(a) in Gupta Yos 1990
% O2 - O2 neutral-neutral molecular interaction plot
% Note unit conversions are done in-line to convert from SI to GuptaYos units
% Collision integrals are plotted in square Angstroms
% pD_ij (i.e., Dbar_ij) is plotted in cm^2*atm/sec
idx_O2 = ~cellfun(@isempty,regexp(spec_list,'^O2$','once'));
figure(2)
% Markers of DEKAF (convert from m^2 to pi*A^2 for O11,O22, from Pa*m^2/sec to cm^2*atm/sec for pDij)
semilogy(T(1:150:end),10^20*pi*exp(lnOmega11_sl(1:150:end,idx_O2,idx_O2))  ,'ko'); hold on;    % circles
semilogy(T(1:150:end),10^20*pi*exp(lnOmega22_sl(1:150:end,idx_O2,idx_O2))  ,'k^'); hold on;    % upward-pointing triangle
semilogy(T(1:150:end),exp(lnBstar_sl(1:150:end,idx_O2,idx_O2))              ,'ks'); hold on;    % squares
semilogy(T(1:150:end),10^4/101325*exp(lnDbar_sl(1:150:end,idx_O2,idx_O2))   ,'kd'); hold on;    % diamonds

% Gupta Yos 1989 Digitized (in units pi*A^2)
semilogy(digitdata.GY_89.piO11_O2_O2(:,1),digitdata.GY_89.piO11_O2_O2(:,2)  ,'bo-'); hold on;
semilogy(digitdata.GY_89.piO22_O2_O2(:,1),digitdata.GY_89.piO22_O2_O2(:,2)  ,'b^-'); hold on;
semilogy(digitdata.GY_89.Bij_O2_O2(:,1),digitdata.GY_89.Bij_O2_O2(:,2)      ,'bs-'); hold on;
semilogy(digitdata.GY_89.pDij_O2_O2(:,1),digitdata.GY_89.pDij_O2_O2(:,2)    ,'bd-'); hold on;

% Yos 1963 Digitized (converted from pi*cm^2 to pi*A^2)
semilogy(digitdata.Y_63.piO11_O2_O2(:,1),10^16*digitdata.Y_63.piO11_O2_O2(:,2)    ,'ro-'); hold on;
semilogy(digitdata.Y_63.piO22_O2_O2(:,1),10^16*digitdata.Y_63.piO22_O2_O2(:,2)    ,'r^-'); hold on;

% Mutation's Wright 2005 (converted from A^2 to pi*A^2)
semilogy(TMp_O2_O2,pi*Q11_O2_O2,'mo-');
semilogy(TMp_O2_O2,pi*Q22_O2_O2,'m^-');
% TMp_O2_O2     = [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000];
% Q11_O2_O2     = [11.12  9.88  9.53  8.69  7.60  6.52  6.22  5.99  5.64  5.39  4.94];
% Q22_O2_O2     = [12.62 11.06 10.65  9.72  8.70  7.70  7.38  7.12  6.73  6.42  5.89];

% Fill in the rest
semilogy(T,10^20*pi*exp(lnOmega11_sl(:,idx_O2,idx_O2)),'k'); hold on;
semilogy(T,10^20*pi*exp(lnOmega22_sl(:,idx_O2,idx_O2)),'k'); hold on;
semilogy(T,exp(lnBstar_sl(:,idx_O2,idx_O2)),'k'); hold on;
semilogy(T,10^4/101325*exp(lnDbar_sl(:,idx_O2,idx_O2)),'k'); hold on;
legend({'\pi\Omega^{1,1}_{i,j} DEKAF fit of GY90',...
        '\pi\Omega^{2,2}_{i,j} DEKAF fit of GY90',...
        'B^*_{i,j} DEKAF fit of GY90',...
        'pD_{i,j} DEKAF fit of GY90',...
        '\pi\Omega^{1,1}_{i,j} GY90 digitized',...
        '\pi\Omega^{2,2}_{i,j} GY90 digitized',...
        'B^*_{i,j} GY90 digitized',...
        'pD_{i,j} GY90 digitized',...
        '\pi\Omega^{1,1}_{i,j} Y63 digitized',...
        '\pi\Omega^{2,2}_{i,j} Y63 digitized',...
        '\pi\Omega^{1,1}_{i,j} M++ (Wright 2005)',...
        '\pi\Omega^{2,2}_{i,j} M++ (Wright 2005)',...
        },'location','best');
grid minor
xlabel('Temperature, K')
xlim([0 30000]);
title('Gupta Yos 1990 Figure 10(a) Comparison')
hold off

%% Plot N-N Comparison for Omega11 and Omega22
% N-N neutral-neutral molecular interaction plot
% Note unit conversions are done in-line to convert from SI to GuptaYos units
% Collision integrals are plotted in pi*square Angstroms
idx_N = ~cellfun(@isempty,regexp(spec_list,'^N$','once'));
figure(3)
% Markers of DEKAF (convert from m^2 to pi*A^2 for O11,O22)
semilogy(T(1:150:end),10^20*pi*exp(lnOmega11_sl(1:150:end,idx_N,idx_N))  ,'ko'); hold on;    % circles
semilogy(T(1:150:end),10^20*pi*exp(lnOmega22_sl(1:150:end,idx_N,idx_N))  ,'k^'); hold on;    % upward-pointing triangle

% Yos 1963 Digitized (converted from pi*cm^2 to pi*A^2)
semilogy(digitdata.Y_63.piO11_N_N(:,1),10^16*digitdata.Y_63.piO11_N_N(:,2)    ,'ro-'); hold on;
semilogy(digitdata.Y_63.piO22_N_N(:,1),10^16*digitdata.Y_63.piO22_N_N(:,2)    ,'r^-'); hold on;

% Yos 1963's digitized data curve fit in DEKAF for Omega11
semilogy(TY63_N_N,fitY63_N_N,'go-');

% Mutation's Wright 2005 (converted from A^2 to pi*A^2)
semilogy(TMp_N_N,pi*Q11_N_N,'mo-');
semilogy(TMp_N_N,pi*Q22_N_N,'m^-');

% TMp_N_N     = [300   500  1000  2000  4000  5000  6000  8000 10000 15000 20000];    % K
% Q11_N_N     = [8.07  7.03  5.96  5.15  4.39  4.14  3.94  3.61  3.37  2.92  2.62];   % A^2;
% Q22_N_N     = [9.11  7.94  6.72  5.82  4.98  4.70  4.48  4.14  3.88  3.43  3.11];

% Fill in the rest
semilogy(T,10^20*pi*exp(lnOmega11_sl(:,idx_N,idx_N)),'k'); hold on;
semilogy(T,10^20*pi*exp(lnOmega22_sl(:,idx_N,idx_N)),'k'); hold on;
legend({'\pi\Omega^{1,1}_{i,j} DEKAF fit of GY90',...
        '\pi\Omega^{2,2}_{i,j} DEKAF fit of GY90',...
        '\pi\Omega^{1,1}_{i,j} Y63 digitized',...
        '\pi\Omega^{2,2}_{i,j} Y63 digitized',...
        '\pi\Omega^{1,1}_{i,j} Y63 digitized DEKAF fit',...
        '\pi\Omega^{1,1}_{i,j} M++ (Wright 2005)',...
        '\pi\Omega^{2,2}_{i,j} M++ (Wright 2005)',...
        },'location','best');
grid minor
ylabel(['Collision integrals [\pi',Ang,'^2]']);
xlabel('Temperature, K')
xlim([0 30000]);
title('N-N Collision DEKAF v Yos ''63 v Mutation++ (Wright 2005) Comparison')
hold off

%% Plot O-O Comparison for Omega11 and Omega22
% O-O neutral-neutral molecular interaction plot
% Note unit conversions are done in-line to convert from SI to GuptaYos units
% Collision integrals are plotted in pi*square Angstroms
idx_O = ~cellfun(@isempty,regexp(spec_list,'^O$','once'));
figure(4)
% Markers of DEKAF (convert from m^2 to pi*A^2 for O11,O22)
semilogy(T(1:150:end),10^20*pi*exp(lnOmega11_sl(1:150:end,idx_O,idx_O))  ,'ko'); hold on;    % circles
semilogy(T(1:150:end),10^20*pi*exp(lnOmega22_sl(1:150:end,idx_O,idx_O))  ,'k^'); hold on;    % upward-pointing triangle

% Yos 1963 Digitized (converted from pi*cm^2 to pi*A^2)
semilogy(digitdata.Y_63.piO11_O_O(:,1),10^16*digitdata.Y_63.piO11_O_O(:,2)    ,'ro-'); hold on;
semilogy(digitdata.Y_63.piO22_O_O(:,1),10^16*digitdata.Y_63.piO22_O_O(:,2)    ,'r^-'); hold on;

% Yos 1963's digitized data curve fit in DEKAF for Omega11
semilogy(TY63_O_O,fitY63_O_O,'go-');

% Mutation's Wright 2005 (converted from A^2 to pi*A^2)
semilogy(TMp_O_O,pi*Q11_O_O,'mo-');
semilogy(TMp_O_O,pi*Q22_O_O,'m^-');

% Fill in the rest of DEKAF's data
semilogy(T,10^20*pi*exp(lnOmega11_sl(:,idx_O,idx_O)),'k'); hold on;
semilogy(T,10^20*pi*exp(lnOmega22_sl(:,idx_O,idx_O)),'k'); hold on;
legend({'\pi\Omega^{1,1}_{i,j} DEKAF fit of GY90',...
        '\pi\Omega^{2,2}_{i,j} DEKAF fit of GY90',...
        '\pi\Omega^{1,1}_{i,j} Y63 digitized',...
        '\pi\Omega^{2,2}_{i,j} Y63 digitized',...
        '\pi\Omega^{1,1}_{i,j} Y63 digitized DEKAF fit',...
        '\pi\Omega^{1,1}_{i,j} M++ (Wright 2005)',...
        '\pi\Omega^{2,2}_{i,j} M++ (Wright 2005)',...
        },'location','best');
grid minor
ylabel(['Collision integrals [\pi',Ang,'^2]']);
xlabel('Temperature, K')
xlim([0 30000]);
title('O-O Collision DEKAF v Yos ''63 v Mutation++ (Wright 2005) Comparison')
hold off

