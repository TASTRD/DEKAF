% Test_muKappa_DEKAFvsMutation_air11 is a script to test the
% thermodynamic transport properties as approximated by Gupta Yos 1990's
% paper by comparing them against mutation++'s results, as well as
% DEKAF's first approximation of Chapman Enskog equations.
%
% See generate_GuptaYos_MutationaTables.m
%
% Author: Ethan Beyak & Fernando Miro
% Date: January 2018

clear; %close all;
addpath(genpath(getenv('DEKAF_DIRECTORY')))

%%%% START OF INPUTS
% loading mutation properties
mixture = 'air11mutation';
mixtureMut = 'air11';
mutdatfile = 'mutationTables_T1000-10-20000_P100325-1000-102325_air11_transport_Ethan.dat';
% mutdatfile = 'mutation_trash.dat';
p_chosen = 1e6;
idx_start = 9; % T,P,mu,kappaMix,kappaReac,kappaInt,kappaHeavyTrans,kappaElTrans then all the species values

Sc = 0.71; % only to calc the D_s spec-mixture coeff... not a big deal.

%%%% END OF INPUTS
%{%
string4mut = ['mppequil --no-header -T ',num2str(200),':',num2str(10),':',num2str(20000),' -P ',...
    ...num2str(p_chosen-1000),':',num2str(1000),':',num2str(p_chosen+1000),' -m 0,1,32,33,34,37,38,39 -s 2,0,1,9,10 -o 10,11,12,0 ',mixtureMut,' > ',...
    num2str(p_chosen),                                                  ' -m 0,1,32,33,34,37,38,39 -s 2,0,1,9,10 -o 10,11,12,0 ',mixtureMut,' > ',...
    mutdatfile];
unix(string4mut);
%}
%% Load the daters
data = load(mutdatfile);

%% Extracting mutation data for chosen pressure
N_spec = str2double(regexp(mixture,'\d+','match','once')); % first digit in mixture string

% Starting indices for tables
idx_ys_start    = idx_start;
idx_ys_end      = idx_ys_start + N_spec - 1;
idx_xs_start    = idx_ys_end + 1;
idx_xs_end      = idx_xs_start + N_spec - 1;
idx_dXsdT_start   = idx_xs_end + 1;
idx_dXsdT_end     = idx_dXsdT_start + N_spec-1;
idx_cps_start   = idx_dXsdT_end + 1;
idx_cps_end     = idx_cps_start + N_spec-1;
idx_hs_start   = idx_cps_end + 1;
idx_hs_end     = idx_hs_start + N_spec-1;
idx_nDbsl_start   = idx_hs_end + 1;
idx_nDbsl_end     = idx_nDbsl_start + N_spec*(N_spec-1)/2-1;
idx_Q11sl_start   = idx_nDbsl_end + 1;
idx_Q11sl_end     = idx_Q11sl_start + N_spec*(N_spec-1)/2-1;
idx_Q22sl_start   = idx_Q11sl_end + 1;
idx_Q22sl_end     = idx_Q22sl_start + N_spec*(N_spec-1)/2-1;
% idx_Q23ee_start   = idx_Q22sl_end + 1;
% idx_Q23ee_end     = idx_Q23ee_start;
% idx_Q24ee_start   = idx_Q23ee_end + 1;
% idx_Q24ee_end     = idx_Q24ee_start;
% idx_Dsl_start   = idx_Q24ee_end + 1;
idx_Dsl_start   = idx_Q22sl_end + 1;
idx_Dsl_end     = idx_Dsl_start + N_spec^2-1;

% Index booleans
idx_ys  = idx_ys_start:idx_ys_end;
idx_xs  = idx_xs_start:idx_xs_end;
idx_dXsdT = idx_dXsdT_start:idx_dXsdT_end;
idx_cps = idx_cps_start:idx_cps_end;
idx_hs = idx_hs_start:idx_hs_end;
idx_nDbsl = idx_nDbsl_start:idx_nDbsl_end;
idx_Q11sl = idx_Q11sl_start:idx_Q11sl_end;
idx_Q22sl = idx_Q22sl_start:idx_Q22sl_end;
% idx_Q23ee = idx_Q23ee_start:idx_Q23ee_end;
% idx_Q24ee = idx_Q24ee_start:idx_Q24ee_end;
idx_Dsl = idx_Dsl_start:idx_Dsl_end;
p_mut   = data(:,2);        ip = (p_mut==p_chosen);
p_mut   = p_mut(ip);        N_p = length(p_mut);
T_mut   = data(ip,1);       N_T = length(T_mut);

mut.mu        =     reshape(data(ip,3),                                        [N_T,1]);            % 32: mu        [Pa-s]      dynamic viscosity
mut.kappaMix  =     reshape(data(ip,4),                                        [N_T,1]);            % 33: lambda    [W/m-K]     mixture equilibrium thermal conductivity
mut.kappaReac =     reshape(data(ip,5),                                        [N_T,1]);            % 34: lam_reac  [W/m-K]     reactive thermal conductivity
mut.kappaInt  =     reshape(data(ip,6),                                        [N_T,1]);            % 37: lam_int   [W/m-K]     internal energy thermal conductivity
mut.kappaHeavy   =  reshape(data(ip,7),                                        [N_T,1]);            % 38: lam_h     [W/m-K]     heavy particle translational thermal conductivity
mut.kappaEl      =  reshape(data(ip,8),                                        [N_T,1]);            % 39: lam_e     [W/m-K]     electron translational thermal conductivity
mut.kappaFroz    = mut.kappaInt + mut.kappaHeavy + mut.kappaEl;
ys_mut     = data(ip,idx_ys);
mut.X_s        =     reshape(data(ip,idx_xs),                                  [N_T,N_spec]);      % 0 : X         [-]         mole fractions
mut.dXsdT     =     reshape(data(ip,idx_dXsdT),                               [N_T,N_spec]);      % 1 : dXs/dT    [1/K]       temperature gradients of the mole fractions
mut.cp_s        =  reshape(data(ip,idx_cps),                                    [N_T,N_spec]);     % 9: cp_s
mut.h_s        =  reshape(data(ip,idx_hs),                                    [N_T,N_spec]);     % 10: h_s
mut.nDbsl        =  reshape(data(ip,idx_nDbsl),                                    [N_T,(N_spec-1)*N_spec/2]);     % 10: h_s
mut.Q11sl        =  reshape(data(ip,idx_Q11sl),                                    [N_T,(N_spec-1)*N_spec/2]);     % 11: Q11sl
mut.Q22sl        =  reshape(data(ip,idx_Q22sl),                                    [N_T,(N_spec-1)*N_spec/2]);     % 12: Q22sl
% mut.Q23ee        =  reshape(data(ip,idx_Q23ee),                                    [N_T,1]);                       % 13: Q24ee
% mut.Q24ee        =  reshape(data(ip,idx_Q24ee),                                    [N_T,1]);                       % 13: Q24ee
mut.D_sl        =  reshape(data(ip,idx_Dsl),                                    [N_T,N_spec,N_spec]);     % 10: Dsl
mut.D_sl = permute(mut.D_sl,[1,3,2]); % transposing the matrix to have the same row arrangement as we use

%% Obtaining tabulated values
[mixCnst,opts] = getAll_constants(mixture,struct);
[mixCnst.LTEtablePath,mixCnst.XEq] = checkCompositionLTE({0,0,0,0,0,0,0.767,0,0.232,0,0},mixCnst.Mm_s,mixCnst.R0,mixture,opts);
mixCnst.LTEtableData = load(mixCnst.LTEtablePath);
fldnms_mixCnst = fieldnames(mixCnst);
for ii=1:length(fldnms_mixCnst)
    eval([fldnms_mixCnst{ii},' = mixCnst.',fldnms_mixCnst{ii},';']); % e.g. hForm_s = mixCnst.hForm_s;
end

[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,T_mat] = ... % properties as needed for the enthalpy evaluation
                       reshape_inputs4enthalpy(R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,T_mut);

%% Obtaining properties from DEKAF
% Equilibrium conditions
%%%% FIXME: This function should work if length(p) = length(T) and not take
%%%% 250^2 shits on me
opts.flow = 'LTE';
[X_s,dXsdT,~] = getEquilibrium_X_complete(p_mut,T_mut,'mtimesx',mixture,mixCnst,opts.modelEquilibrium,1,opts);
y_s = getEquilibrium_ys(X_s,Mm_s);

% enthalpy
[~,~,~,h_s,~,~,cvMod_s]  = getMixture_h_cp(y_s,T_mat,T_mat,T_mat,T_mat,T_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,opts);
% density
rho                 = getMixture_rho(y_s,p_mut,T_mut,T_mut,R0,Mm_s,bElectron);  % computing mixture density and other properties ...
% reaction rates
dlnKeqrdT           = getReactionDer_dlnKeqr_dT(y_s,T_mut,delta_nuEq,Keq_struc,opts);
dlnKpeqrdT          = getEquilibriumDer_dlnKpeq_dT(dlnKeqrdT,T_mut,sum(delta_nuEq,1)');
% Mixture molar mass
Mm                  = getMixture_Mm_R(y_s,T_mut,T_mut,Mm_s,R0,bElectron);
% Transport
%%% PASSING STUFF THROUGH opts
Q11sl = zeros(N_T,N_spec,N_spec);
Q22sl = zeros(N_T,N_spec,N_spec);
it = 1;
for s=2:N_spec
    for l=s:N_spec
        Q11sl(:,s,l) = mut.Q11sl(:,it)/pi;
        Q22sl(:,s,l) = mut.Q22sl(:,it)/pi;
        Q11sl(:,l,s) = mut.Q11sl(:,it)/pi;
        Q22sl(:,l,s) = mut.Q22sl(:,it)/pi; it=it+1;
    end
end
%%%% UNTIL HERE
%%
mu = applyYos67(11,y_s,T_mut,T_mut,rho,p_mut,Mm_s,Mm,R0,consts_Omega11,...
                            consts_Omega22,nAvogadro,...
                            kBoltz,bElectron,qEl,epsilon0,bPos);
muMat = applyChapmanEnskog(11,y_s,T_mut,T_mut,rho,p_mut,Mm_s,Mm,consts_Omega11,...
                            consts_Omega22,nAvogadro,R0,kBoltz,...
                            bElectron,qEl,epsilon0,bPos);
% Diffusion coefficients from multi-component diffusion
[Db_sl,~] = getPair_nD_binary_FOCE(T_mut,T_mut,y_s,rho,consts_Omega11,Mm_s,...
                                                kBoltz,nAvogadro,bElectron,qEl,epsilon0,bPos);
% [D_sl,D_sm]     = getPair_D_multicomp_Ramshaw(Db_sl,y_s,T_mut,T_mut,Mm_s,bElectron,R0,bPos,true);
D_sl              = getPair_D_multicomp_StefanMaxwell(Db_sl,y_s,T_mut,T_mut,Mm_s,bElectron,bPos,qEl,kBoltz);
rho_s = y_s.*repmat(rho,[1,N_spec]);

% assume thermal equilibrium between heavy species and electrons (no free electrons so this doesnt even matter)
kappaHeavy = applyYos67(21,y_s,T_mut,T_mut,rho,p_mut,Mm_s,Mm,R0,consts_Omega11,...
                                    consts_Omega22,nAvogadro,kBoltz,bElectron,...
                                    qEl,epsilon0,bPos,consts_Bstar);
kappaHeavy2 = applyChapmanEnskog(22,y_s,T_mut,T_mut,rho,p_mut,Mm_s,Mm,consts_Omega11,...
                                    consts_Omega22,nAvogadro,R0,kBoltz,bElectron,...
                                    qEl,epsilon0,bPos,consts_Bstar);


[kappaInt,kappaRot,kappaVib,kappaElec] = getMixture_kappaInt_EuckenCorrection(T_mut,T_mut,T_mut,T_mut,T_mut,...
                                    y_s,rho,p_mut,R0,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,...
                                    bPos,hForm_s,nAvogadro,consts_Omega11,Mm_s,...
                                    kBoltz,qEl,epsilon0,opts);

kappaReac           = getEquilibrium_kappareac('spec-spec',h_s,rho_s,D_sl,dXsdT); % spec-spec coefficients
% kappaReac           = getEquilibrium_kappareac('spec-spec',h_s,rho_s,D_sl,dXsdT,bElectron,T_mut,rho,y_s,Mm_s,bPos,qEl,nAvogadro,kBoltz); % spec-spec coefficients
kappaEl             = getElectron_kappa(3,T_mut,T_mut,y_s,rho,p_mut,Mm_s,bElectron,bPos,...
                                    kBoltz,R0,nAvogadro,qEl,epsilon0,...
                                    consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,...
                                    consts_Estar,consts_Omega14,consts_Omega15,consts_Omega24);
kappaFroz           = kappaHeavy + kappaInt+kappaEl;
kappaFroz2          = kappaHeavy2 + kappaInt+kappaEl;
kappaMix            = kappaFroz + kappaReac; % neglect thermal (aka soret) diffusion for now
kappaMix2           = kappaFroz2 + kappaReac; % neglect thermal (aka soret) diffusion for now

% passing everything to a structure like in mutation
Yos67.mu          = reshape(mu,[N_T,1]);
Yos67.kappaMix    = reshape(kappaMix,[N_T,1]);
Yos67.kappaReac   = reshape(kappaReac,[N_T,1]);
Yos67.kappaFroz   = reshape(kappaFroz,[N_T,1]);
Yos67.kappaInt    = reshape(kappaInt,[N_T,1]);
Yos67.kappaRot    = reshape(kappaRot,[N_T,1]);
Yos67.kappaVib    = reshape(kappaVib,[N_T,1]);
Yos67.kappaElec   = reshape(kappaElec,[N_T,1]);
Yos67.kappaHeavy = reshape(kappaHeavy,[N_T,1]);
Yos67.kappaEl     = reshape(kappaEl,[N_T,1]);
Yos67.X_s         = reshape(X_s,[N_T,N_spec]);

CE_12 = Yos67;
CE_12.mu = muMat;
CE_12.kappaHeavy = reshape(kappaHeavy2,[N_T,1]);
CE_12.kappaFroz = reshape(kappaFroz2,[N_T,1]);
CE_12.kappaMix = reshape(kappaMix2,[N_T,1]);
CE_12.kappaEl    = reshape(kappaEl,[N_T,1]);

%% Intermediate comparisons with mutation
%{
figure
clear plots;
subplot(1,2,1)
[N_i,N_j] = optimalPltSize(N_spec);
for s=1:N_spec
    subplot(N_i,N_j,s)
    plots(s) = plot(T_mut,h_s(:,s)); hold on;
    plot(T_mut,mut.h_s(:,s),'--'); grid minor;
    title(spec_list{s});
end
xlabel('T [K]'); ylabel('h_s [J/kg]')
%}
%{%
figure
clear plots;
[N_i,N_j] = optimalPltSize(N_spec);
for s=1:N_spec
    subplot(N_i,N_j,s)
    plots(s) = plot(T_mut,dXsdT(:,s)); hold on;
    plot(T_mut,mut.dXsdT(:,s),'--'); grid minor;
    title(spec_list{s});
end
legend('DEKAF','mutation');
xlabel('T [K]'); ylabel('\partialX_s/\partialT [1/K]')
%% figure
figure
for s=1:N_spec
%     figure
    for l=1:N_spec
        subplot(N_spec,N_spec,(s-1)*N_spec+l)
%         subplot(N_i,N_j,l)
%         if s==l
%         semilogy(T_mut,D_sl(:,s,l)); hold on;
% %         semilogy(T_mut,mut.D_sl(:,(s-1)*N_spec+l),'--'); grid minor;
%         semilogy(T_mut,mut.D_sl(:,s,l),'--'); grid minor;
%         else
        semilogy(T_mut,abs(D_sl(:,s,l))); hold on;
%         plot(T_mut,mut.D_sl(:,(s-1)*N_spec+l),'--'); grid minor;
%         semilogy(T_mut,abs(mut.D_sl(:,s,l)),'--'); grid minor;
%         end
        xlabel('T [K]'); ylabel('D_{sl} [m^2/s]')
        title([spec_list{s},' <-> ',spec_list{l}]);
    end
end
% legend('DEKAF','mutation');
%}
%% figure
[Ni,Nj] = optimalPltSize(N_spec*(N_spec+1)/2);it=1;
figure
for s=1:N_spec
    for l=s:N_spec
%         subplot(N_spec,N_spec,(s-1)*N_spec+l)
        subplot(Ni,Nj,it);it=it+1;
        idxPos = D_sl(:,s,l)>0;
        idxNeg = D_sl(:,s,l)<0;
        semilogy(T_mut(idxPos), D_sl(idxPos,s,l),'b-'); hold on;
        semilogy(T_mut(idxNeg),-D_sl(idxNeg,s,l),'r-'); hold on;
        xlabel('T [K]'); ylabel('D_{sl} [m^2/s]')
        title([spec_list{s},' <-> ',spec_list{l}]);
        disp([spec_list{s},' <-> ',spec_list{l},' ----> ',num2str(max(abs(D_sl(:,s,l)-D_sl(:,l,s))))]);
        if s==N_spec-1 && l==N_spec;        legend('positive','negative');      end
    end
end
%% Q24ee & Q23ee
%{
Q24ee = evalin('base','mut.Q24ee');
Q23ee = evalin('base','mut.Q23ee');

T_mut = evalin('base','T_mut');

figure
semilogy(T_mut,Q24ee/pi,'-k','linewidth',1.5); hold on;
semilogy(T_mut,Omega24_sl(:,1,1),'-.g','linewidth',1.5); hold on;
semilogy(T_mut,Q23ee/pi,'-r','linewidth',1.5); hold on;
semilogy(T_mut,Omega23_sl(:,1,1),'-.m','linewidth',1.5); hold on;
legend('m++ Omega24','dekaf Omega24','m++ Omega23','dekaf Omega23','location','best')
%}

%% Compare various transport models

% Wilke's
% Note that this model does not compute mixture internal kappa or mixture
% translational kappa -- straight for mixture frozen kappa from Wilke's
% mixing rule
[Wilke.mu,Wilke.mu_s] = ...
    getMixture_mu_BlottnerWilke(y_s,T_mut,T_mut,Mm_s,...
        Amu_s,Bmu_s,Cmu_s,...
        R0,bElectron);

Wilke.kappaHeavy = ...
    getMixture_kappa_EuckenWilke(y_s,Wilke.mu_s,T_mut,T_mut,...
        cvMod_s.cvTrans_s,cvMod_s.cvRot_s,cvMod_s.cvVib_s,cvMod_s.cvElec_s,...
        Mm_s,R0,bElectron);

% Compute errors of models with respect to Mutation++'s 2nd C-E Approx.
% mu
transport.err.Yos67.mu  = abs((Yos67.mu - mut.mu)./mut.mu);
transport.err.CE_12.mu  = abs((CE_12.mu - mut.mu)./mut.mu);
transport.err.Wilke.mu  = abs((Wilke.mu - mut.mu)./mut.mu);

% kappaFrozen
transport.err.Yos67.kappaFroz   = abs(...
    (Yos67.kappaFroz - mut.kappaFroz)./mut.kappaFroz);
transport.err.CE_12.kappaFroz   = abs(...
    (CE_12.kappaFroz - mut.kappaFroz)./mut.kappaFroz);

% kappaInt
transport.err.Yos67.kappaInt   = abs(...
    (Yos67.kappaInt - mut.kappaInt)./mut.kappaInt);
transport.err.CE_12.kappaInt   = abs(...
    (CE_12.kappaInt - mut.kappaInt)./mut.kappaInt);

% kappaHeavy
transport.err.Yos67.kappaHeavy = abs(...
    (Yos67.kappaHeavy - mut.kappaHeavy)./mut.kappaHeavy);
transport.err.CE_12.kappaHeavy = abs(...
    (CE_12.kappaHeavy - mut.kappaHeavy)./mut.kappaHeavy);
transport.err.Wilke.kappaHeavy   = abs(...
    (Wilke.kappaHeavy - mut.kappaHeavy)./mut.kappaHeavy);

% kappaReac
transport.err.Yos67.kappaReac = abs(...
    (Yos67.kappaReac - mut.kappaReac)./mut.kappaReac); % we normalize the reactive thermal conductivity error by the maximum, to avoid 1/0 situations
transport.err.CE_12.kappaReac = abs(...
    (CE_12.kappaReac - mut.kappaReac)./mut.kappaReac);

% kappaEl
transport.err.Yos67.kappaEl = abs(...
    (Yos67.kappaEl - mut.kappaEl)./mut.kappaEl); % we normalize the reactive thermal conductivity error by the maximum, to avoid 1/0 situations
transport.err.CE_12.kappaEl = abs(...
    (CE_12.kappaEl - mut.kappaEl)./mut.kappaEl);

NPlots = 6;
iP = 1;
Styles.CE_12 = '-';    LW.CE_12 = 1;       color.CE_12 = color_rainbow(iP,NPlots)*0.8; iP = iP+1;
Styles.Yos67 = '-.';     LW.Yos67 = 1;       color.Yos67 = color_rainbow(iP,NPlots)*0.8; iP = iP+1;
Styles.Wilke = ':';     LW.Wilke = 1.5;     color.Wilke = color_rainbow(iP,NPlots)*0.8; iP = iP+1;
Styles.mut = '--';      LW.mut = 1;         color.mut = [0,0,0]; iP = iP+1;
% defining legends
leg = {'Chapman-Enskog 1st-2nd approx',...
       'Yos 64 approx',...
       'Blottner-Eucken-Wilke',...
       'Mutation++'};

plotLim = [0,20e3];

%% Error plot
figure
try
suptitle('Comparison of errors of various transport models')
catch
end
% Dynamic viscosity, mu
subplot(2,3,1)
semilogy(T_mut,transport.err.CE_12.mu,Styles.CE_12,'LineWidth',LW.CE_12,'Color',color.CE_12); hold on;      % 1st-2nd CE
semilogy(T_mut,transport.err.Yos67.mu,Styles.Yos67,'LineWidth',LW.Yos67,'Color',color.Yos67); hold on;      % 1st CE
semilogy(T_mut,transport.err.Wilke.mu,Styles.Wilke,'LineWidth',LW.Wilke,'Color',color.Wilke); hold on;      % Wilke's (Blottner + Eucken)
legend(leg(1:end-1),'location','best');
grid minor
title('Dynamic viscosity \mu');
xlabel('T [K]');
ylabel('(model - mutation++)/mutation++');
xlim(plotLim);

% Internal thermal conductivity (rot + vib + elec)
subplot(2,3,2)
semilogy(T_mut,transport.err.CE_12.kappaInt,Styles.CE_12,'LineWidth',LW.CE_12,'Color',color.CE_12); hold on;      % 1st-2nd CE
semilogy(T_mut,transport.err.Yos67.kappaInt,Styles.Yos67,'LineWidth',LW.Yos67,'Color',color.Yos67); hold on;      % 1st CE
grid minor
title('Internal thermal conductivity, \Sigma\kappa^{int}');
xlabel('T [K]');
ylabel('(model - mutation++)/mutation++');
xlim(plotLim);

% Heavy thermal conductivity
subplot(2,3,3)
semilogy(T_mut,transport.err.CE_12.kappaHeavy,Styles.CE_12,'LineWidth',LW.CE_12,'Color',color.CE_12); hold on;      % 1st-2nd CE
semilogy(T_mut,transport.err.Yos67.kappaHeavy,Styles.Yos67,'LineWidth',LW.Yos67,'Color',color.Yos67); hold on;      % 1st CE
semilogy(T_mut,transport.err.Wilke.kappaHeavy,Styles.Wilke,'LineWidth',LW.Wilke,'Color',color.Wilke); hold on;      % Wilke's (Blottner + Eucken)
grid minor
title('Heavy thermal conductivity, \kappa^{Heavy}');
xlabel('T [K]');
ylabel('(model - mutation++)/mutation++');
xlim(plotLim);

% Reactive thermal conductivity
subplot(2,3,4)
semilogy(T_mut,transport.err.CE_12.kappaReac,Styles.CE_12,'LineWidth',LW.CE_12,'Color',color.CE_12); hold on;      % 1st-2nd CE
semilogy(T_mut,transport.err.Yos67.kappaReac,Styles.Yos67,'LineWidth',LW.Yos67,'Color',color.Yos67); hold on;      % 1st CE
grid minor
title('Reactive thermal conductivity, \kappa^{reac}');
xlabel('T [K]');
ylabel('(model - mutation++)/mutation++');
xlim(plotLim);
% ylim([0,0.2]);

% electron thermal conductivity
subplot(2,3,5)
semilogy(T_mut,transport.err.CE_12.kappaEl,Styles.CE_12,'LineWidth',LW.CE_12,'Color',color.CE_12); hold on;      % 1st-2nd CE
semilogy(T_mut,transport.err.Yos67.kappaEl,Styles.Yos67,'LineWidth',LW.Yos67,'Color',color.Yos67); hold on;      % 1st CE
grid minor
title('electron thermal conductivity, \kappa^{e-}');
xlabel('T [K]');
ylabel('(model - mutation++)/mutation++');
xlim(plotLim);

% Frozen thermal conductivity, kappaFroz
subplot(2,3,6)
semilogy(T_mut,transport.err.CE_12.kappaFroz,Styles.CE_12,'LineWidth',LW.CE_12,'Color',color.CE_12); hold on;      % 1st-2nd CE
semilogy(T_mut,transport.err.Yos67.kappaFroz,Styles.Yos67,'LineWidth',LW.Yos67,'Color',color.Yos67); hold on;      % 1st CE
grid minor
title('Frozen thermal conductivity, \kappa^{frozen}');
xlabel('T [K]');
ylabel('(model - mutation++)/mutation++');
xlim(plotLim);

%% Final unified awesome plot

% obtaining plot size
var2Plot = {'mu',...
    'kappaInt',...
    'kappaHeavy',...
    'kappaReac',...
    'kappaEl',...
    'kappaFroz',...
    ...'kappaMix',...
    };
nameFancy = {'\mu [kg/m-s]',...
    '\kappa^{int} [W/m-K]',...
    '\kappa^{Heavy} [W/m-K]',...
    '\kappa^{Reac} [W/m-K]',...
    '\kappa^{e-} [W/m-K]',...
    '\kappa^{frozen} [W/m-K]',...
    ...'\kappa^{Tot} [W/m-K]',...
    };
titles = {'Dynamic viscosity',...
    'Internal thermal conductivity',...
    'Heavy thermal conductivity',...
    'Reactive thermal conductivity',...
    'Electron thermal conductivity',...
    'Frozen thermal conductivity',...
    ...'Total thermal conductivity',...
    };
plt4leg = 'mu'; % plot for the legend
Nplt = length(var2Plot);
[N_i,N_j] = optimalPltSize(Nplt);
it_plt = 0;

figure
for i_plt = 1:Nplt
    it_plt = it_plt+1; subplot(N_i,N_j,it_plt);
    if isfield(CE_12,var2Plot{i_plt})
        plot(T_mut,CE_12.(var2Plot{i_plt}),Styles.CE_12,'LineWidth',LW.CE_12,'Color',color.CE_12); hold on;      % 1st-2nd CE
    end
    if isfield(Yos67,var2Plot{i_plt})
        plot(T_mut,Yos67.(var2Plot{i_plt}),Styles.Yos67,'LineWidth',LW.Yos67,'Color',color.Yos67); hold on;      % 1st CE
    end
    if isfield(Wilke,var2Plot{i_plt})
        plot(T_mut,Wilke.(var2Plot{i_plt}),Styles.Wilke,'LineWidth',LW.Wilke,'Color',color.Wilke); hold on;      % Wilke's (Blottner + Eucken)
    end
    if isfield(mut,var2Plot{i_plt})
        plot(T_mut,mut.(var2Plot{i_plt}),  Styles.mut,  'LineWidth',LW.mut,  'Color',color.mut);   hold on;      % mutation
    end
    if strcmp(var2Plot{i_plt},plt4leg)
        legend(leg,'location','best');
    end
    grid minor
    title(titles{i_plt});
    xlabel('T [K]');
    ylabel(nameFancy{i_plt});
    xlim(plotLim);
end
%{
it_plt = it_plt+1; subplot(N_i,N_j,it_plt);     % Mole fractions
clear plots
for s=1:N_spec
    plots(s) = plot(T_mut,Yos67.X_s(:,s),'Color',color_rainbow(s,N_spec));  hold on;   % 1st CE
    plotMut = plot(T_mut,mut.X_s(:,s),'k--');                     % mutation
end
plots(N_spec+1) = plotMut;
legend(plots,[spec_list,'M++'],'location','northeast');
grid minor
title('Mole fractions X_s');
xlabel('T [K]');
ylabel('X_s [-]')
xlim(plotLim);
%}