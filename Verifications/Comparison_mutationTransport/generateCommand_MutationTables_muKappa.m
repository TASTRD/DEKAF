% generateCommand_MutationaTables_.m
%
% This script generates the unix command to call mutation, generating a
% table of values for transport properties, as well as mass fractions
% and mole fractions
%
% This is a wonky workaround until I figure out how to call mutation
% from MATLAB, fixing my environmental variable's paths to point
% to the MPP_DIRECTORY
%
% See GenerateMutationTables_noWrapper.m for more information on this script
% & Test_Transport_GuptaYos_DEKAFvMutation.m

%%%% START OF INPUTS
T_min = 1000;
T_max = 20000;
dT = 10;
N_T = int16((T_max-T_min)/dT + 1);

p_chosen = 101325;
dp = 1000;

noheader = '--no-header';
% noheader = '';

mixture = 'air11';
% mixture = 'air5';
%%%% END OF INPUTS

saveFileName = ['mutationTables_T',num2str(T_min),'-',num2str(dT),'-',num2str(T_max),'_P',...
    num2str(p_chosen-dp),'-',num2str(dp),'-',num2str(p_chosen+dp),'_',mixture,'_transport_Ethan.dat'];

string4mut = ['mppequil ',noheader,' -T ',num2str(T_min),':',num2str(dT),':',num2str(T_max),' -P ',...
    num2str(p_chosen-dp),':',num2str(dp),':',num2str(p_chosen+dp),' -m 0,1,32,33,34,37,38,39 -s 2,0,1,9,10 -o 10,11,12,13,14,0 ',mixture,' > ',...
    saveFileName];  % numbers correspond to T, P, mu, mixture thermal conductivity, reactive thermal conductivity, internal energy thermal conductivity...
                    % heavy particle translational thermal conductivity
                    % electron translational thermal conductivity
                    % (1,1) pure species collision integrals, (2,2) pure species collision integrals
                    % mass fractions, mole fractions, multicomponent diffusion coeff.

% To run Mutation++
fprintf(string4mut)
fprintf('\n')

% To view the csv output in the terminal
fprintf(['column -s, -t < ',saveFileName,' | less -#2 -N -S']);
fprintf('\n')
