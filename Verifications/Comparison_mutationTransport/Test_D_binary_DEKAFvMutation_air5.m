% Test_D_binary_DEKAFvMutation_air5 is a script to test the multicomponent
% diffusion coefficients of air5.
%
% Author: Fernando Miro Miro
% Date: February 2018

clear;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

%%%% START OF INPUTS
% loading mutation properties
mixture = 'air5mutation';
N_spec = 5;
mutdatfile = 'mutationTables_T700-10-20000_P3000-500-4000_air5_binary_D.dat';
p_chosen = 4000;
idx_start = 6; % T,P,mu,kappaInt,kappaHTrans then all the species values

% If planning for spec-mix diffusion...
Sc = 0.71; % only to calc the D_s spec-mixture coeff... not a big deal.

%%%% END OF INPUTS

%% Load the daters
% "you gotta write comments. Else how will anyone understand the code???"
data = load(mutdatfile);

% Starting indices for tables
idx_ys_start    = idx_start;
idx_ys_end      = idx_ys_start + N_spec - 1;
idx_xs_start    = idx_ys_end + 1;
idx_xs_end      = idx_xs_start + N_spec - 1;
idx_Dms_start    = idx_xs_end + 1;
idx_Dms_end      = idx_Dms_start + N_spec - 1;
idx_Dsl_start   = idx_Dms_end + 1;
idx_Dsl_end     = idx_Dsl_start + N_spec^2 - 1;
idx_nbDsl_start   = idx_Dsl_end + 1;
idx_nbDsl_end     = idx_nbDsl_start + N_spec*(N_spec+1)/2 - 1;

% Index booleans
idx_ys  = idx_ys_start:idx_ys_end;
idx_xs  = idx_xs_start:idx_xs_end;
idx_Dms  = idx_Dms_start:idx_Dms_end;
idx_Dsl = idx_Dsl_start:idx_Dsl_end;
idx_nbDsl = idx_nbDsl_start:idx_nbDsl_end;
p_mutLong   = data(:,2); ip = (p_mutLong==p_chosen);
p_mutLong   = p_mutLong(ip);
p_mut       = unique(p_mutLong);    N_p = length(p_mut);
T_mutLong   = data(ip,1);
T_mut       = unique(T_mutLong);    N_T = length(T_mut);

prop2comp_mut.mu        =     reshape(data(ip,3),                                        [N_T,N_p]);            % 32: mu        [Pa-s]      dynamic viscosity
prop2comp_mut.kappaInt  =     reshape(data(ip,4),                                        [N_T,N_p]);            % 37: lam_int   [W/m-K]     internal energy thermal conductivity
prop2comp_mut.kappaHTrans  =  reshape(data(ip,5),                                        [N_T,N_p]);            % 38: lam_h     [W/m-K]     heavy particle translational thermal conductivity
prop2comp_mut.kappaElTrans = zeros(size(prop2comp_mut.kappaHTrans)); % hardcode for now, just binary oxygen
prop2comp_mut.kappaFroz = prop2comp_mut.kappaInt + prop2comp_mut.kappaHTrans + prop2comp_mut.kappaElTrans;
ys_mutLong              = data(ip,idx_ys);
ys_mut                  =     reshape(ys_mutLong,                                       [N_T,N_p,N_spec]);      % -s 2 : Y         [-]         mass fractions
prop2comp_mut.X_s       =     reshape(data(ip,idx_xs),                                  [N_T,N_p,N_spec]);      % -s 0 : X         [-]         mole fractions
prop2comp_mut.D_sm      =     reshape(data(ip,idx_Dms),                                 [N_T,N_p,N_spec]);      % -s 18 : Dm        [-]         effective diffusion coefficients
prop2comp_mut.D_sl      =     reshape(data(ip,idx_Dsl),                                 [N_T,N_p,N_spec,N_spec]);       % -o 0 : Dsl       [m^2/s]     multicomponent diffusion coefficients
prop2comp_mut.D_sl = permute(prop2comp_mut.D_sl,[1,2,4,3]);                               % (eta,l,s) --> (eta,s,l)
prop2comp_mut.nbD_sl    =     reshape(data(ip,idx_nbDsl),                               [N_T,N_p,N_spec*(N_spec+1)/2]); % -o 10 : nbDsl    [m^2/s]     binary diffusion coefficients times number density

ys_mut = squeeze(ys_mut); % if doing one pressure, zap out the pressure dimension

%% Obtaining tabulated values
% % For Wright2005 (mutation data)
opts.modelCollisionNeutral = 'Wright2005';

opts.modelDiffusion = 'FOCE_Ramshaw';
mixCnst = getAll_constants(mixture,opts);

%% Obtaining properties from DEKAF
% density
rho_mutLong                 = getMixture_rho(ys_mutLong,p_mutLong,T_mutLong,T_mutLong,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron);  % computing mixture density and other properties ...
% binary diffusion coefficients
[D_sl_b_mut,nDsl_b_mut] = ...
    getPair_nD_binary_FOCE(T_mut,T_mut,ys_mutLong,rho_mutLong,...
        mixCnst.consts_Omega11,mixCnst.Mm_s,...
        mixCnst.kBoltz,mixCnst.nAvogadro,mixCnst.bElectron,mixCnst.qEl,...
        mixCnst.epsilon0,mixCnst.bPos);
% multicomponent diffusion coefficients
[D_sl_R,D_sm]     = getPair_D_multicomp_Ramshaw(D_sl_b_mut,ys_mutLong,T_mut,T_mut,...
                                        mixCnst.Mm_s,mixCnst.bElectron,mixCnst.R0,mixCnst.bPos);
% D_sl_Exact  = getPair_D_multicomp_Exact(D_sl_b_mut,ys_mutLong,T_mut,T_mut,...
%     mixCnst.Mm_s,mixCnst.bElectron,mixCnst.R0);
D_sl_SM     = getPair_D_multicomp_StefanMaxwell(D_sl_b_mut,ys_mutLong,T_mut,T_mut,...
                                mixCnst.Mm_s,mixCnst.bElectron,mixCnst.bPos,mixCnst.qEl,mixCnst.kBoltz);

%% Load Gupta Yos 90 Curve fit data
%{%
clear opts
opts.modelCollisionNeutral = 'GuptaYos90';
opts.collisionRefExceptions.references = {'Yos63'};
opts.collisionRefExceptions.pairs = {{'O - O','N - N'}};
opts.modelDiffusion = 'GuptaYos90_Ramshaw';
mixCnstGY = getAll_constants(mixture,opts);
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,T_mat] = ... % properties as needed for the enthalpy evaluation
                       reshape_inputs4enthalpy(mixCnstGY.R_s,mixCnstGY.nAtoms_s,mixCnstGY.thetaVib_s,mixCnstGY.thetaElec_s,mixCnstGY.gDegen_s,mixCnstGY.bElectron,mixCnstGY.hForm_s,T_mutLong);
% binary diffusion coefficients
p_vec = repmat(p_chosen,[N_T,1]);
n = getMixture_n(ys_mutLong,rho_mutLong,mixCnstGY.Mm_s,mixCnstGY.nAvogadro);
D_sl_b_YG = getPair_D_binary_GuptaYos(p_vec,T_mutLong,mixCnstGY.consts_Dbar_binary.Dsl);
nD_sl_b_YG = repmat(n,[1,N_spec,N_spec]) .* D_sl_b_YG;
% multicomponent diffusion coefficients
[D_sl_R_YG,D_sm_YG]     = getPair_D_multicomp_Ramshaw(D_sl_b_YG,ys_mutLong,T_mut,T_mut,...
                                mixCnstGY.Mm_s,mixCnstGY.bElectron,mixCnstGY.R0,mixCnst.bPos);
% D_sl_Exact_YG  = getPair_D_multicomp_Exact(D_sl_b,ys_mutLong,T_mut,T_mut,...
%     mixCnstGY.Mm_s,mixCnstGY.bElectron,mixCnstGY.R0);
% D_sl_SM_YG     = getPair_D_multicomp_StefanMaxwell(D_sl_b_YG,ys_mutLong,T_mut,T_mut,...
%                                             mixCnstGY.Mm_s,mixCnstGY.bElectron,mixCnstGY.bPos,mixCnstGY.qEl,mixCnstGY.kBoltz);%,a(ii));
%}
%% Plotting
spec_list = mixCnst.spec_list;
% Multicomponent diffusion coefficients
figure
for s=1:N_spec
    for l=1:N_spec
        subplot(N_spec,N_spec,(s-1)*(N_spec)+l)
        if s==l
        plots(1) = semilogy(T_mut,D_sl_SM(:,s,l),'r-');
        hold on;
        plots(2) = semilogy(T_mut,D_sl_R(:,s,l),'b-');
        plots(3) = semilogy(T_mut,D_sl_R_YG(:,s,l),'g-.');
        else
        plots(1) = plot(T_mut,D_sl_SM(:,s,l),'r-');
        hold on;
        plots(2) = plot(T_mut,D_sl_R(:,s,l),'b-');
        plots(3) = plot(T_mut,D_sl_R_YG(:,s,l),'g-.');
        end
        if s==l
        plots(4) = semilogy(T_mut,prop2comp_mut.D_sl(:,1,s,l),'k--');
        else
        plots(4) = plot(T_mut,prop2comp_mut.D_sl(:,1,s,l),'k--');
        end
        Dmax = max(prop2comp_mut.D_sl(:,1,s,l));
        Dmin = min(prop2comp_mut.D_sl(:,1,s,l));
        Dmarge = (Dmax-Dmin)/10;
%         ylim([Dmin-Dmarge , Dmax+Dmarge]);
        title([spec_list{s},' - ',spec_list{l}]);
        xlabel('T [K]'); ylabel(['D_{',spec_list{s},',',spec_list{l},'}']);
    end
end
legend('DEKAF Wright2005 Stefan-Maxwell',...
       'DEKAF Wright2005 Ramshaw',...
       'DEKAF GuptaYos90 Ramshaw',...
       'Mutation++'...
       );
% Effective diffusion coefficients
figure
for s=1:N_spec
    subplot(2,3,s);
    plot(T_mut,D_sm(:,s),'b-');
    hold on;
    plot(T_mut,D_sm_YG(:,s),'g-.');
    plot(T_mut,prop2comp_mut.D_sm(:,s),'k--');
    title(spec_list{s});
    xlabel('T [K]'); ylabel(['D_{',spec_list{s},'}']);
end
legend('DEKAF Wright2005 Ramshaw','DEKAF GuptaYos90 Ramshaw','Mutation++');

% Binary diffusion coefficients
figure
it = 0;
for s=1:N_spec
    for l=s:N_spec
        it = it+1;
        subplot(N_spec,N_spec,(s-1)*N_spec+l);
        plot(T_mut,nDsl_b_mut(:,s,l),'b-');
        hold on
        plot(T_mut,nD_sl_b_YG(:,s,l),'g-.');
        plot(T_mut,prop2comp_mut.nbD_sl(:,1,it),'k--');
        xlabel('T [K]'); ylabel(['binary n*D_{',spec_list{s},',',spec_list{l},'}']);
    end
end
legend('DEKAF Wright2005 Ramshaw','DEKAF GuptaYos90 Ramshaw','Mutation++');

%% difference between binary diffusion coefficient and mutation
figure
it = 0;
for s=1:N_spec
    for l=s:N_spec
        it = it+1;
        subplot(N_spec,N_spec,(s-1)*N_spec+l);
        plot(T_mut,nDsl_b_mut(:,s,l)./prop2comp_mut.nbD_sl(:,1,it),'b-');
        hold on
        plot(T_mut,nD_sl_b_YG(:,s,l)./prop2comp_mut.nbD_sl(:,1,it),'g-.');
        xlabel('T [K]'); ylabel(['binary n*D_{',spec_list{s},',',spec_list{l},'}']);
    end
end
legend('DEKAF Wright2005 Ramshaw','DEKAF GuptaYos90 Ramshaw');