% generateCommand_MutationaTables_.m
%
% This script generates the unix command to call mutation, generating a
% table of values for multicomponent diffusion coefficients of a binary
% mixture
%
% This is a wonky workaround until I figure out how to call mutation
% from MATLAB, fixing my environmental variable's paths to point
% to the MPP_DIRECTORY
%
% See GenerateMutationTables_noWrapper.m for more information on this script
% & Test_Transport_GuptaYos_DEKAFvMutation.m

%%%% START OF INPUTS
% Note that before 675 K, the mole fraction of O is too small and C++
% returns a floating point exception, creating a core dump.
% Start at 700 K to work around this.
T_min = 700;
T_max = 20000;
dT = 10;
N_T = int16((T_max-T_min)/dT + 1);

p_min = 3000;
p_max = 4000;
dp = 500;
N_p = int16((p_max-p_min)/dp + 1);

noheader = '--no-header';
% noheader = '';

% mixture = 'O2';
% mixture = 'air5';
mixture = 'air11';
% mixture = 'N2';

%%%% END OF INPUTS

saveFileName = ...
    ['mutationTables_T',num2str(T_min),'-',num2str(dT),'-',num2str(T_max),...
    '_P',num2str(p_min),'-',num2str(dp),'-',num2str(p_max),...
    '_',mixture,'_binary_D.dat'];

string4mut = ...
    ['mppequil ',noheader,' -T ',num2str(T_min),':',num2str(dT),':',num2str(T_max),...
    ' -P ',num2str(p_min),':',num2str(dp),':',num2str(p_max),...
    ' -m 0,1,32,37,38 -s 2,0,18 -o 0,10 ',mixture,' > ',...
    saveFileName];
    % -m 0,1: heavy particle temperature, pressure
    % -m 32,37,38: mu, kappaInt, kappaHTrans
    % -s 0,2,18: equilibrium mole fractions, equilibrium mass fractions,
    % effective diffusion coefficients
    % -o O,10  : multicomponent diffusion coeff. and binary diffusion coeff.

% To run Mutation++
fprintf(string4mut)
fprintf('\n')

% To view the csv output in the terminal
fprintf(['column -s, -t < ',saveFileName,' | less -#2 -N -S']);
fprintf('\n')
