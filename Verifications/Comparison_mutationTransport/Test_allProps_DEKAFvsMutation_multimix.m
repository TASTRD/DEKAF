% Test_allProps_DEKAFvsMutation_multimix is a script to perform a
% comparison of all the thermodynamic and transport properties computed by
% DEKAF with those computed with mutation for various mixtures.
%
% Author: Fernando Miro Miro
% Date: July 2019

clear;
addpath(genpath(getenv('DEKAF_DIRECTORY')))

%%% INPUTS
mixtures = {'air5mutation','air11mutation'}; ic=1;
mixFancy = {'Air-5','Air-11'};
ys_e{ic} = {0,0,0,0.767,0.232};ic=ic+1;
ys_e{ic} = {0,0,0,0,0,0,0.767,0,0.232,0,0};ic=ic+1;
T_vec = linspace(1000,20e3,1001);
% p_vec = logspace(2,6,17);
p_vec = logspace(3,5,9);
modelTransport = 'CE_123';
modelDiffusion = 'FOCE_StefanMaxwell';
%%% END OF INPUTS

Tmin = min(T_vec);
Tmax = max(T_vec);
dT = mean(diff(T_vec));
NT = length(T_vec);
Np = length(p_vec);
T = repmat(T_vec.',[1,Np]); T=T(:);
p = repmat(p_vec  ,[NT,1]); p=p(:);

for im=length(mixtures)
    mixture = mixtures{im};
    options.modelTransport = modelTransport;
    options.modelDiffusion = modelDiffusion;
    options.flow = 'LTE';
    options.T_tol = 1e-13;
    [mixCnst,options] = getAll_constants(mixture,options);
    flds = fieldnames(mixCnst);
    for ii=1:length(flds);      eval([flds{ii},' = mixCnst.',flds{ii},';']); end
    [mixCnst.LTEtablePath,mixCnst.XEq] = checkCompositionLTE(ys_e{im},Mm_s,R0,mixture,options);
    mixCnst.LTEtableData = load(mixCnst.LTEtablePath);
    [X_s,dXs_dT,dXs_dp] = getEquilibrium_X_complete(p,T,'mtimesx',mixture,mixCnst,options.modelEquilibrium,1,options);
    y_s = getEquilibrium_ys(X_s,Mm_s);
    dys_dT = getEquilibriumDer_dys_dq(X_s,dXs_dT,Mm_s);
    rho = getMixture_rho(y_s,p,T,T,R0,Mm_s,bElectron);
    Mm = getMixture_Mm_R(y_s,T,T,Mm_s,R0,bElectron);
    [h,cp,cv,h_s,hMod_s] = getMixture_h_cp_noMat(y_s,T,T,T,T,T,R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options);
    cp_reac = getEquilibrium_cpreac(h_s,dys_dT);
    [mu,kappa,~,kappaMod] = getTransport_mu_kappa(y_s,T,T,rho,p,Mm,options,mixCnst,T,T,T,cp);
    kappa_int = kappaMod.kappaRot + kappaMod.kappaVib + kappaMod.kappaElec;
    D_sl = getTransport_Ds(y_s,T,T,rho,p,Mm,mu,kappa,cp,mixCnst,NaN,NaN,options);
    rho_s = repmat(rho,[1,N_spec]) .* y_s;
%     kappa_reac = getEquilibrium_kappareac('spec-spec',h_s,rho_s,D_sl,dXs_dT,bElectron,T,rho,y_s,Mm_s,bPos,qEl,nAvogadro,kBoltz);
    kappa_reac = getEquilibrium_kappareac('spec-spec',h_s,rho_s,D_sl,dXs_dT);
    N_spec = mixCnst.N_spec;

    [lnOmega11_sl,Tstar] = getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega11,bPos);
    Omega11_sl = exp(lnOmega11_sl);
    Omega22_sl = exp(getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega22,bPos));
    Bstar_sl = exp(getPair_ABCstar(Tstar,consts_Bstar));
    Cstar_sl = exp(getPair_ABCstar(Tstar,consts_Cstar));

    spec_list = mixCnst.spec_list;

    data2save(im).DKF_T             = T_vec;
    data2save(im).DKF_p             = p_vec;
    data2save(im).DKF_Xs            = reshape(X_s,[NT,Np,N_spec]);
    data2save(im).DKF_dXsdT         = reshape(dXs_dT,[NT,Np,N_spec]);
    data2save(im).DKF_rho           = reshape(rho,[NT,Np]);
    data2save(im).DKF_Mm            = reshape(Mm,[NT,Np]);
    data2save(im).DKF_h             = reshape(h,[NT,Np]);
    data2save(im).DKF_cp            = reshape(cp,[NT,Np]);
    data2save(im).DKF_cp_reac       = reshape(cp_reac,[NT,Np]);
    data2save(im).DKF_mu            = reshape(mu,[NT,Np]);
    data2save(im).DKF_kappa         = reshape(kappa,[NT,Np]);
    data2save(im).DKF_kappa_reac    = reshape(kappa_reac,[NT,Np]);
    data2save(im).DKF_kappa_int     = reshape(kappa_int,[NT,Np]);
    data2save(im).DKF_Omega11_sl    = reshape(Omega11_sl,[NT,Np,N_spec,N_spec]);
    data2save(im).DKF_Omega22_sl    = reshape(Omega22_sl,[NT,Np,N_spec,N_spec]);
    data2save(im).DKF_Bstar_sl      = reshape(Bstar_sl,[NT,Np,N_spec,N_spec]);
    data2save(im).DKF_Cstar_sl      = reshape(Cstar_sl,[NT,Np,N_spec,N_spec]);

    % Mutation
    mixtureMut = regexp(mixture,'^[a-z|A-Z]+[0-9]+','match');
    clear rho Mm h cp cp_reac mu kappa kappa_reac kappa_int X_s dXs_dT Omega11_sl Omega22_sl Bstar_sl Cstar_sl varsout
    switch mixtureMut{1}
        case 'air5';        otherMutInputs = {'-o',11:14,N_spec*(N_spec+1)/2*[1,1,1,1]};                         Nout = 4;
        case 'air11';       otherMutInputs = {'-o',11:18,[N_spec*(N_spec-1)/2*[1,1,1,1] , N_spec*[1,1,1,1]]};    Nout = 8;
        otherwise;          error(['Error: mixtureMut=''',mixtureMut,''' is not supported']);
    end
    for ip=Np:-1:1
        [rho(:,ip),Mm(:,ip),h(:,ip),cp(:,ip),cp_eq(:,ip),mu(:,ip),kappa(:,ip),kappa_reac(:,ip),kappa_int(:,ip),Xs,dXsdT,varsout{1:Nout}] = mppequilWrap([Tmin,dT,Tmax],p_vec(ip),[3,5,10,16,9,32,33,34,37],[0,1],mixtureMut{1},otherMutInputs{:});
        ic=0; ie=0;
        for s=1:N_spec
            X_s(:,ip,s) = Xs{s};
            dXs_dT(:,ip,s) = dXsdT{s};
            for l=s:N_spec
                if bElectron(s)
                    ie=ie+1;
                    Omega11_sl(:,ip,s,l) = varsout{5}{ie};   Omega11_sl(:,ip,l,s) = varsout{5}{ie};
                    Omega22_sl(:,ip,s,l) = varsout{6}{ie};   Omega22_sl(:,ip,l,s) = varsout{6}{ie};
                    Bstar_sl(:,ip,s,l) = varsout{7}{ie};     Bstar_sl(:,ip,l,s) = varsout{7}{ie};
                    Cstar_sl(:,ip,s,l) = varsout{8}{ie};     Cstar_sl(:,ip,l,s) = varsout{8}{ie};
                else
                    ic=ic+1;
                    Omega11_sl(:,ip,s,l) = varsout{1}{ic};   Omega11_sl(:,ip,l,s) = varsout{1}{ic};
                    Omega22_sl(:,ip,s,l) = varsout{2}{ic};   Omega22_sl(:,ip,l,s) = varsout{2}{ic};
                    Bstar_sl(:,ip,s,l) = varsout{3}{ic};     Bstar_sl(:,ip,l,s) = varsout{3}{ic};
                    Cstar_sl(:,ip,s,l) = varsout{4}{ic};     Cstar_sl(:,ip,l,s) = varsout{4}{ic};
                end
            end
        end
    end

    dh = mean(data2save(im).DKF_h(:) - h(:));

    data2save(im).MUT_T             = T_vec;
    data2save(im).MUT_p             = p_vec;
    data2save(im).MUT_Xs            = X_s;
    data2save(im).MUT_dXsdT         = dXs_dT;
    data2save(im).MUT_rho           = rho;
    data2save(im).MUT_Mm            = Mm;
    data2save(im).MUT_h             = h+dh;
    data2save(im).MUT_cp            = cp;
    data2save(im).MUT_cp_reac       = cp_eq-cp;
    data2save(im).MUT_mu            = mu;
    data2save(im).MUT_kappa         = kappa - kappa_reac;
    data2save(im).MUT_kappa_reac    = kappa_reac;
    data2save(im).MUT_kappa_int     = kappa_int;
    data2save(im).MUT_Omega11_sl    = Omega11_sl/pi;
    data2save(im).MUT_Omega22_sl    = Omega22_sl/pi;
    data2save(im).MUT_Bstar_sl      = Bstar_sl;
    data2save(im).MUT_Cstar_sl      = Cstar_sl;

    %% plotting
    %{
    figure
    [Ni,Nj] = optimalPltSize(9); pltFlag = 'Rbw2';clear plots legends; stl = 'k--';
    for ip=1:Np
        ic=1;
        subplot(Ni,Nj,ic);
        for s=1:N_spec
            plot(data2save(im).DKF_T,data2save(im).DKF_Xs(:,ip,s),'Color',two_colors(ip,Np,pltFlag)); hold on;
            plot(data2save(im).MUT_T,data2save(im).MUT_Xs(:,ip,s),stl);
        end; ic=ic+1;
        subplot(Ni,Nj,ic);
        for s=1:N_spec
            plot(data2save(im).DKF_T,data2save(im).DKF_dXsdT(:,ip,s),'Color',two_colors(ip,Np,pltFlag)); hold on;
            plot(data2save(im).MUT_T,data2save(im).MUT_dXsdT(:,ip,s),stl);
        end; ic=ic+1;
        subplot(Ni,Nj,ic);  plot(data2save(im).DKF_T,data2save(im).DKF_Mm(:,ip),'Color',two_colors(ip,Np,pltFlag)); hold on; ic=ic+1;
        subplot(Ni,Nj,ic);  plot(data2save(im).DKF_T,data2save(im).DKF_h(:,ip),'Color',two_colors(ip,Np,pltFlag)); hold on; ic=ic+1;
        subplot(Ni,Nj,ic);  plot(data2save(im).DKF_T,data2save(im).DKF_cp(:,ip),'Color',two_colors(ip,Np,pltFlag)); hold on; ic=ic+1;
%         subplot(Ni,Nj,ic);  plot(data2save(im).DKF_T,data2save(im).DKF_cp_reac(:,ip),'Color',two_colors(ip,Np,pltFlag)); hold on; ic=ic+1;
        subplot(Ni,Nj,ic);  plot(data2save(im).DKF_T,data2save(im).DKF_mu(:,ip),'Color',two_colors(ip,Np,pltFlag)); hold on; ic=ic+1;
        subplot(Ni,Nj,ic);  plot(data2save(im).DKF_T,data2save(im).DKF_kappa(:,ip),'Color',two_colors(ip,Np,pltFlag)); hold on; ic=ic+1;
        subplot(Ni,Nj,ic);  plot(data2save(im).DKF_T,data2save(im).DKF_kappa_reac(:,ip),'Color',two_colors(ip,Np,pltFlag)); hold on; ic=ic+1;
        subplot(Ni,Nj,ic);  plots(ip) = plot(data2save(im).DKF_T,data2save(im).DKF_kappa_int(:,ip),'Color',two_colors(ip,Np,pltFlag));  hold on;%ic=ic+1;
        legends{ip} = ['p = ',num2str(p_vec(ip),'%0.0f'),' Pa'];
    end
    ic=1;
    subplot(Ni,Nj,ic);                                                              xlabel('T [K]'); ylabel('X_s [-]'); ic=ic+1;
    subplot(Ni,Nj,ic);                                                              xlabel('T [K]'); ylabel('dXs/dT [1/K]'); ic=ic+1;
    subplot(Ni,Nj,ic);  plot(data2save(im).MUT_T,data2save(im).MUT_Mm,stl);         xlabel('T [K]'); ylabel('M [kg/mol]'); ic=ic+1;
    subplot(Ni,Nj,ic);  plot(data2save(im).MUT_T,data2save(im).MUT_h,stl);          xlabel('T [K]'); ylabel('h [J/kg]'); ic=ic+1;
    subplot(Ni,Nj,ic);  plot(data2save(im).MUT_T,data2save(im).MUT_cp,stl);         xlabel('T [K]'); ylabel('c_p [J/kg-K]'); ic=ic+1;
%     subplot(Ni,Nj,ic);  plot(data2save(im).MUT_T,data2save(im).MUT_cp_reac,stl);    xlabel('T [K]'); ylabel('c_p^{Reac} [J/kg-K]'); ic=ic+1;
    subplot(Ni,Nj,ic);  plot(data2save(im).MUT_T,data2save(im).MUT_mu,stl);         xlabel('T [K]'); ylabel('\mu [kg/m-s]'); ic=ic+1;
    subplot(Ni,Nj,ic);  plot(data2save(im).MUT_T,data2save(im).MUT_kappa,stl);      xlabel('T [K]'); ylabel('\kappa [W/m-K]'); ic=ic+1;
    subplot(Ni,Nj,ic);  plot(data2save(im).MUT_T,data2save(im).MUT_kappa_reac,stl); xlabel('T [K]'); ylabel('\kappa^{Reac} [W/m-K]'); ic=ic+1;
    subplot(Ni,Nj,ic);  plot(data2save(im).MUT_T,data2save(im).MUT_kappa_int,stl);  xlabel('T [K]'); ylabel('\kappa^{Int} [W/m-K]'); ic=ic+1;
    legend(plots,legends);

    %% plotting collisions
    col_list = {'Omega11_sl','Omega22_sl','Bstar_sl','Cstar_sl'};
    units = {'m^2','m^2','-','-'};
    TDKF = data2save(im).DKF_T;
    TMUT = data2save(im).MUT_T;
    for jj=1:length(col_list)
        colDKF = data2save(im).(['DKF_',col_list{jj}]);
        colMUT = data2save(im).(['MUT_',col_list{jj}]);
    figure
    [Ni,Nj] = optimalPltSize(N_spec*(N_spec+1)/2); clear plots legends;
    for ip=1:Np
        ic=1;
        for s=1:N_spec
        for l=s:N_spec
        subplot(Ni,Nj,ic);  plot(TDKF,colDKF(:,ip,s,l),'Color',two_colors(ip,Np,pltFlag)); hold on;
                            plot(TMUT,colMUT(:,ip,s,l),stl); ic=ic+1;
        end
        end
    end
    ic=1;
    for s=1:N_spec
    for l=s:N_spec
        subplot(Ni,Nj,ic); xlabel('T [K]'); ylabel([col_list{jj},' [',units{jj},']']); title([spec_list{s},'-',spec_list{l}]); ic=ic+1;
    end
    end
    end
    %}
end

%% Saving
saveDEKAF('DKF_props.mat','data2save','mixFancy');
