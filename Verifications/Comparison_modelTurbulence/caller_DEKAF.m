% caller_DEKAF runs the SS solver for the test on the thermal models.
%
% Author: Fernando Miro Miro
% April 2020


clear; clc;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'_stable/SourceCode']))

% Flow conditions
M_e = 0.1;          % [-]
p_e = 101325;       % [Pa]
% M_e = 2;            % [-]
% p_e = 1e4;          % [Pa]
T_e = 300;          % [K]
% Tw  = 300;          % [K]
xMin = 1e-7;        % [m]
xMax = 0.01;        % [m]
N_x = 300;
% modelTurbulence_vec = {'none','SmithCebeci1967','SSTkomega'};
modelTurbulence_vec = {'none','SSTkomega'};
Nm = length(modelTurbulence_vec);

% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 15;                   % Domain size (in non-dimensional eta length)
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 1e-9;                 % Convergence tolerance
options.it_max                  = 1000;                 % maximum number of iterations
options.it_res_sat_diff_val     = 0.0001;               % relax saturation abortion criterion
options.perturbRelaxation       = 0.5;

% flow options
options.thermal_BC              = 'adiab';              % Wall bc
% options.thermal_BC              = 'Twall';              % Wall bc (isothermal)
% options.G_bc                    = Tw;                   % value of the wall temperature
% options.flow                    = 'TPG';                % flow assumption
% options.mixture                 = 'air2';               % mixture
% options.modelTransport          = 'CE_122';             % transport model
options.flow                    = 'CPG';                % flow assumption
options.mixture                 = 'air2';               % mixture
options.modelTransport          = 'Sutherland';         % transport model

options.BLproperties = true;
options.bGICM4delta99 = false;
options.bReturnNonDimFieldsIfDimFail = false;

options.gifConv = true;
% options.fields4gif = {'dfh_deta','gh','KTh','WTh'};
options.fields4gif = {'df_deta','df_deta2','g','KT','WT','C','CT'};
options.gifLogPlot = true;

% display options
options.dimoutput = true;
options.plotRes = false;                         % we don't want the residual plot
% options.quiet = true;

% flow conditions
intel.M_e               = M_e;                  % Mach number at boundary-layer edge            [-]
intel.T_e               = T_e;                  % Temperature at boundary-layer edge            [K]
intel.p_e               = p_e;                  % Pressure at boundary-layer edge               [Pa]

% marching options
options.ox              = 2;
options.mapOpts.N_x     = N_x;
options.mapOpts.x_start = xMin;
options.mapOpts.x_end   = xMax;
options.xSpacing        = 'tanh';

%% Executing DEKAF
for ii=Nm:-1:1
    options.modelTurbulence = modelTurbulence_vec{ii};
%     if strcmp(modelTurbulence_vec{ii},'none');  extraInputs = {};           intel.x = intel_all{end}.x(1,:); options.dimXQSS=true;
%     else;                                       extraInputs = {'marching'};
%     end
    extraInputs = {'marching'};
    [intel_all{ii},options_all{ii}] = DEKAF(intel,options,extraInputs{:});
end

% Saving
saveDEKAF('DKF_modelTurbulenceCompare.mat','intel_all','options_all','modelTurbulence_vec');

%% Compute eddy viscosity
% for ii=Nm:-1:1
%     if ~strcmp(options_all{ii}.modelTurbulence,'none')
%         mixCnst = intel_all{ii}.mixCnst;
%         cylCoordFuncs = intel_all{ii}.cylCoordFuncs;
%         for ix=options_all{ii}.mapOpts.separation_i_xi-1:-1:1
%             rho = intel_all{ii}.rho(:,ix);
%             p   = intel_all{ii}.p(:,ix);
%             T   = intel_all{ii}.T(:,ix);
%             y_s = cell1D2matrix2D(cellfun(@(cll)cll(:,ix),intel_all{ii}.ys,'UniformOutput',false));
%             Mm  = getMixture_Mm_R(y_s,[],[],mixCnst.Mm_s,[],[]);
%             mu{ii}(:,ix)  = getTransport_mu_kappa(y_s,T,T,rho,p,Mm,options_all{ii},mixCnst);
%             g         = intel_all{ii}.g(:,ix);
%             D_eta     = intel_all{ii}.D1;
%             eta       = intel_all{ii}.eta;
%             df_deta   = intel_all{ii}.df_deta(:,ix);
%             df_deta2  = intel_all{ii}.df_deta2(:,ix);
%             rho_e     = intel_all{ii}.rho_e(ix);
%             U_e       = intel_all{ii}.U_e(ix);
%             xi        = intel_all{ii}.xi(ix);
%             xBar      = get_xyTransformed(intel_all{ii}.x(ix),[],options_all{ii}.coordsys,cylCoordFuncs);
%             muT{ii}(:,ix) = getTurbulent_mu_Pr(options_all{ii}.modelTurbulence,rho,mu{ii}(:,ix),df_deta,df_deta2,g,eta,D_eta,rho_e,U_e,xi,xBar,options_all{ii}.coordsys,cylCoordFuncs);
%         end
%     end
% end

%% plotting
idxTurb = cellfun(@(cll)~strcmp(cll.modelTurbulence,'none'),options_all);
Nplts = 20;
idxEnd = min(cellfun(@(cll)cll.mapOpts.separation_i_xi,options_all(idxTurb)))-1;
idx2plt = round(linspace(1,idxEnd,Nplts+1)); idx2plt(1) = [];
% idx2plt = 1:Nplts;
x2plt = intel_all{end}.x(1,idx2plt);
figure;  Ni=2;Nj=Nplts/2;
styleS = {'-','-.','--'};
for ix=Nplts:-1:1
    ixx=idx2plt(ix);
    yMax = max(cellfun(@(cll)max(cll.y_i(ixx)),intel_all));
    for ii=1:Nm
        clr = two_colors(ii,Nm+1,'Rbw2');
        name = modelTurbulence_vec{ii};
        subplot(Ni,Nj,ix); plot(intel_all{ii}.u(:,ixx),  1e3*intel_all{ii}.y(:,ixx),styleS{ii},'Color',clr,'displayname',name); hold on;
    end
    xlabel('u [m/s]');      ylabel('y [mm]'); grid minor; ylim([0,1e3*yMax]); title(['x = ',num2str(x2plt(ix)*1e3,2),' mm']);
end
legend();

%%
NTurb = nnz(idxTurb);
figure; Ni=2; Nj=NTurb+1;
for ii=1:Nm
    clr = two_colors(ii,Nm+1,'Rbw2');
    name = modelTurbulence_vec{ii};
    muTRat = max(muT{ii}./mu{ii},[],1);
    ip=0;
    ip=ip+1;       subplot(Ni,Nj,ip); plot(intel_all{ii}.x(1,1:idxEnd)*1e3,intel_all{ii}.delta99(1:idxEnd)*1e3,styleS{ii},'Color',clr,'displayname',name); hold on;
    if ~strcmp(options_all{ii}.modelTurbulence,'none')
    ip=ip+1+NTurb; subplot(Ni,Nj,ip); plot(intel_all{ii}.x(1,1:idxEnd)*1e3,muTRat(1:idxEnd),styleS{ii},'Color',clr,'displayname',name); hold on;
    end
end
ip=0;
ip=ip+1;       subplot(Ni,Nj,ip); xlabel('x [mm]'); ylabel('\delta_{99} [mm]'); lgnd=legend(); set(lgnd,'location','northwest');
ip=ip+1+NTurb; subplot(Ni,Nj,ip); xlabel('x [mm]'); ylabel('max(\mu_T/\mu) [-]');


for ii=Nm:-1:1
    if ~strcmp(options_all{ii}.modelTurbulence,'none')
        subplot(1,Nj,ii);
        for ix=1:Nplts
            ixx=idx2plt(ix);
            clr = two_colors(ix,Nplts,'Rbw2');
            name = ['x = ',num2str(x2plt(ix)*1e3,2),' mm'];
        %     yVar = eta;
            yVar = intel_all{ii}.y(:,ixx)./intel_all{ii}.delta99(ixx);
            plot(muT{ii}(:,ixx)./mu{ii}(:,ixx) , yVar, 'Color',clr,'displayname',name); hold on;
        end
        % xlabel('\mu_T/\mu [-]'); ylabel('\eta [-]'); lgnd=legend(); set(lgnd,'location','eastoutside'); ylim([0,options_all{ii}.eta_i]); grid minor;
        xlabel('\mu_T/\mu [-]'); ylabel('y/\delta_{99} [-]'); lgnd=legend(); set(lgnd,'location','eastoutside'); ylim([0,1.3]); grid minor;
    end
end