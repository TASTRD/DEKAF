% compare_FalknerSkan_DEKAFvsVESTA is a script to run the DEKAF solver with
% a pressure gradient and compare it to VESTA's RK4
%
% Date: May 2018

clear
clc;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'_stable/']));
addpath(genpath([getenv('VESTA_DIRECTORY'),'/VESTA_code/']));

clear options intel

% beta_vec = [0,0.005,0.01,0.02]; % M=10
beta_vec = -0.02:0.01:0.05; % M=6
Nbeta = length(beta_vec);
for ii=Nbeta:-1:1

M_e = 6;
T_e = 300; % [K]
p_e = 4000; % [Pa]
cp = 1004.5; % [J/kg-K]
gam = 1.4;
Pr = 0.7;
g_bc = 0.4;
% beta = 0.01;
beta = beta_vec(ii);

% Optional inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.thermal_BC              = 'gwall';              % Wall bc
options.G_bc                    = g_bc;                 % BC on the non-dimensional semi-total enthalpy
options.flow                    = 'CPG';                % flow assumption
options.modelTransport          = 'Sutherland';
options.cstPr                   = true;                 % constant PRandtl number
options.specify_cp              = true;                 % user-specified cp
options.specify_gam             = true;                 % user-specified cp

options.tol                     = 1e-14;                % Convergence tolerance
options.it_max                  = 200;
options.T_tol                   = 1e-13;
options.T_itMax                 = 50;

options.plotRes                 = false;                % to plot the residual convergence

options.dimoutput               = true;                 % to output fields in dimensional form
options.dimXQSS                 = true;
intel.x                         = 1;                    % x location where we want the dimensional profiles

% Flow parameters
intel.M_e                       = M_e;                  % Mach number at boundary-layer edge                        [-]
intel.T_e                       = T_e;                  % Static temperature at boundary-layer edge                  [K]
intel.p_e                       = p_e;                  % Absolute static pressure at boundary-layer edge           [Pa]
intel.cp                        = cp;                   % Constant specific heat                                    [J/(kg K)]
intel.gam                       = gam;                  % Constant specific heat ratio                              [-]
options.Pr                      = Pr;                   % Constant Prandtl                              [-]
intel.beta = beta;

intels{ii} = DEKAF(intel,options);


%% Running VESTA
fluid.cp = cp;
fluid.mu = 'air';
fluid.k = [];
fluid.Pr = Pr;
fluid.R = [];
fluid.gam = gam;

clear options
options.tol = 1e-15;
options.L = 40;
options.N = 10001;
options.s_new = 1.0;
% options.H_F= true;
% options.G_bc = 0; % adiabatic flow
options.H_F= false;
options.G_bc = g_bc; % isothermal flow
options.import_sol = false;% false
options.fluid = fluid;
options.M_e = M_e;
options.T_e = T_e;
options.p_e = p_e;
options.beta = beta/2;
options.save = true;
% CBL_profile (options);

% options.beta = beta/2;
% options.save = true;
% options.import_sol = true;
% options.FileName = 'prevFile.mat';
% CBL_profile (options);

options.beta = beta;
options.save = false;
options.import_sol = true;
options.FileName = 'prevFile.mat';
compr_profile{ii} = CBL_profile (options);

eta_VESTA{ii} = linspace(0,options.L,options.N);
end

%% Plotting
if Nbeta==1; Nplts = 2; else Nplts=Nbeta; end;
ylims = [0,4];
clear plots legends
figure
for ii=1:Nbeta
    subplot(1,2,1)
    plot(intels{ii}.df_deta,intels{ii}.eta,'Color',two_colors(ii,Nplts,'KR')); hold on;
    plot(compr_profile{ii}.f_p,eta_VESTA{ii},'--','Color',two_colors(ii,Nplts,'GB'));
    xlabel('\partial f / \partial \eta [-]'); ylabel('\eta [-]');
    ylim(ylims);

    subplot(1,2,2)
    plots(ii) = plot(intels{ii}.g,intels{ii}.eta,'Color',two_colors(ii,Nplts,'KR')); hold on;
    plots(Nbeta+ii) = plot(compr_profile{ii}.g,eta_VESTA{ii},'--','Color',two_colors(ii,Nplts,'GB'));
    legends{ii} = ['DEKAF \beta = ',num2str(beta_vec(ii))];
    legends{Nbeta+ii} = ['VESTA \beta = ',num2str(beta_vec(ii))];
    xlabel('g [-]'); ylabel('\eta [-]');
    ylim(ylims);
end
% legend('DEKAF','VESTA');
legend(plots,legends,'location','best');