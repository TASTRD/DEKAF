% caller_BitterTNE_fig1b is a script to run the DEKAF solver to compare
% against Bitter & Shepherd's 2-T basic-state flowfields.
%
% Case corresponds to that in Fig. 1b) in Bitter, N. P., & Shepherd, J. E.
% (2015). Stability of highly cooled hypervelocity boundary layers. Journal
% of Fluid Mechanics, 778, 586–620. https://doi.org/10.1017/jfm.2015.358
%
% Date: December 2018

clear;
close all;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
M_e     = 5;
T_e     = 1500;      % K
ys_e    = {0.22,0.78};
R_e     = 288.1935;  % J/kg-K (corresponds to the chosen ys_e)
gam     = 1.4;       % vibrationally frozen edge
U_e     = M_e*sqrt(T_e*R_e*gam);
Tv_e    = T_e;      % K
x_max   = 0.7;      % m
p_e     = 20e3;     % Pa
T_w     = 300;      % K

% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 1e-11;                % Convergence tolerance
options.it_max                  = 500;                  % maximum number of iterations
options.it_res_sat              = options.it_max;       % residual saturation limit
options.T_tol                   = 1e-13;                % convergence tolerance for the NR to get T from the enthalpy
options.T_itMax                 = 200;                  % maximum number of iterations for the NR to get T from the enthalpy

% flow options
options.thermal_BC              = 'Twall';              % Wall bc (g or dg/deta)
options.G_bc                    = T_w;
options.flow                    = 'TPG';                % flow assumption
% options.mixture                 = 'air5Park85';         % mixture
options.mixture                 = 'air2';               % mixture
options.modelDiffusion          = 'cstSc';              % diffusion model
options.modelTransport          = 'BlottnerEucken';     % transport model
options.referenceVibRelax       = 'Mortensen2013';      % reference from which to obtain the vibrational relaxation constants
options.modelEnergyModes        = 'Bitter2015';
options.bParkCorrectionTauVib   = false;                % park's correction on the relaxation times
options.numberOfTemp            = '2T';                 % T-Tv model
options.dimoutput               = true;                 % we want dimensional profiles
options.mapOpts.x_start         = 1e-8;                 % m
options.mapOpts.x_end           = x_max;                % m
options.mapOpts.N_x             = 50;
options.xSpacing                = 'tanh';
options.perturbRelaxation       = 0.5;
options.Fields4conv = {'df_deta3','dg_deta2','momXeq','enereq'};

% display options
options.plotRes = false;                        % we don't want the residual plot
options.MarchingGlobalConv = false;             % nor the marching global plot

% options.gifConv = true;
options.fields4gif = {'tau','tauv'};

% options.plot_transient = true;
options.fields2plot = {'tauv','tau'};

options.xlim = [0,Inf];
options.ylim = [0,6];
options.gifxlim = options.xlim;

% flow conditions
Nx = 10; % just for the inflow profiles
intel.U_e               = U_e*ones(1,Nx);       % Mach number at boundary-layer edge        [-]
% intel.M_e               = M_e*ones(1,Nx);       % streamwise velocity at boundary-layer edge[m/s]
intel.T_e               = T_e*ones(1,Nx);       % temperature at boundary-layer edge        [K]
intel.Tv_e              = Tv_e*ones(1,Nx);
intel.x_e               = linspace(0,8.8e26,Nx);% limits of the observable universe         [m]
% intel.Re1_e0            = Re1_e;                % unit Reynolds number based on edge        [1/m]
intel.p_e0              = p_e;                  % pressure at boundary-layer edge           [Pa]

[intel,options] = DEKAF(intel,options,'marching');

saveLight('DEKAF_BitterTNE_fig1b.mat',intel,options);

%% plotting
bitter_u  = load('ref_data/Bitter_Fig1b_u_TNE.csv');
bitter_T  = load('ref_data/Bitter_Fig1b_T_TNE.csv');
bitter_Tv = load('ref_data/Bitter_Fig1b_Tv_TNE.csv');

figure
[Ni,Nj] = optimalPltSize(2); ic=1;
subplot(Ni,Nj,ic); ic=ic+1;
plot(intel.u(:,end),intel.y(:,end),'r-'); hold on;
plot(bitter_u(:,2),bitter_u(:,1),'ks'); hold on;
xlabel('u [m/s]'); ylabel('y [m]'); grid minor; ylim([0,intel.y_i(end)]);
subplot(Ni,Nj,ic); ic=ic+1;
plot(intel.T(:,end),intel.y(:,end),'r-','displayname','DEKAF TNE trans-rot'); hold on;
plot(intel.Tv(:,end),intel.y(:,end),'c-','displayname','DEKAF TNE vib'); hold on;
plot(bitter_T(:,2),bitter_T(:,1),'ks','displayname','Bitter & Shepherd TNE trans-rot'); hold on;
plot(bitter_Tv(:,2),bitter_Tv(:,1),'kd','displayname','Bitter & Shepherd TNE vib'); hold on;
xlabel('T, T_v [K]'); ylabel('y [m]'); grid minor; ylim([0,intel.y_i(end)]); legend();
