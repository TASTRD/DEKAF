% Prepare_mat4thesis is a script to pack together the results of the two
% comparisons with Bitter to include them in the thesis
%
% Author: Fernando Miro Miro
% Date: July 2019

clear;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

mat = load('DEKAF_BitterTNE_fig1a.mat');
bitter_u  = sortrows(load('ref_data/Bitter_Fig1a_u_TNE.csv'));
bitter_T  = sortrows(load('ref_data/Bitter_Fig1a_T_TNE.csv'));
bitter_Tv = sortrows(load('ref_data/Bitter_Fig1a_Tv_TNE.csv'));

ic=1;
data2save(ic).yDKF  = mat.y(:,end);
data2save(ic).uDKF  = mat.u(:,end);
data2save(ic).TDKF  = mat.T(:,end);
data2save(ic).TvDKF = mat.Tv(:,end);
data2save(ic).yuRef = bitter_u(:,1);
data2save(ic).uRef  = bitter_u(:,2);
data2save(ic).yTRef = bitter_T(:,1);
data2save(ic).TRef  = bitter_T(:,2);
data2save(ic).yTvRef = bitter_Tv(:,1);
data2save(ic).TvRef  = bitter_Tv(:,2);
Te_vec(ic) = mat.T_e(1);

mat = load('DEKAF_BitterTNE_fig1b.mat');
bitter_u  = sortrows(load('ref_data/Bitter_Fig1b_u_TNE.csv'));
bitter_T  = sortrows(load('ref_data/Bitter_Fig1b_T_TNE.csv'));
bitter_Tv = sortrows(load('ref_data/Bitter_Fig1b_Tv_TNE.csv'));

ic=ic+1;
data2save(ic).yDKF  = mat.y(:,end);
data2save(ic).uDKF  = mat.u(:,end);
data2save(ic).TDKF  = mat.T(:,end);
data2save(ic).TvDKF = mat.Tv(:,end);
data2save(ic).yuRef = bitter_u(:,1);
data2save(ic).uRef  = bitter_u(:,2);
data2save(ic).yTRef = bitter_T(:,1);
data2save(ic).TRef  = bitter_T(:,2);
data2save(ic).yTvRef = bitter_Tv(:,1);
data2save(ic).TvRef  = bitter_Tv(:,2);
Te_vec(ic) = mat.T_e(1);

mat = load('DEKAF_BitterTNE_fig4a.mat');
bitter_u  = sortrows(load('ref_data/Bitter_Fig4a_u_TNE.csv'));
bitter_T  = sortrows(load('ref_data/Bitter_Fig4a_T_TNE.csv'));
bitter_Tv = sortrows(load('ref_data/Bitter_Fig4a_Tv_TNE.csv'));

ic=ic+1;
data2save(ic).yDKF  = mat.y(:,end);
data2save(ic).uDKF  = mat.u(:,end);
data2save(ic).TDKF  = mat.T(:,end);
data2save(ic).TvDKF = mat.Tv(:,end);
data2save(ic).yuRef = bitter_u(:,1);
data2save(ic).uRef  = bitter_u(:,2);
data2save(ic).yTRef = bitter_T(:,1);
data2save(ic).TRef  = bitter_T(:,2);
data2save(ic).yTvRef = bitter_Tv(:,1);
data2save(ic).TvRef  = bitter_Tv(:,2);
Te_vec(ic) = mat.T_e(1);

saveDEKAF('DKF_TNEBitter.mat','data2save','Te_vec');