% caller_HudsonTCNE is a script to run the DEKAF solver to compare against
% Hudson's 2-T basic-state flowfields.
%
% Date: December 2018

clear;
close all;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
M_e     = 10;
T_e     = 278;          % K
Tv_e    = T_e;          % K
Re1_e   = 9.8425e6;     % 1/m
x_max   = 0.4;          % m
T_w     = 2000;         % K
trc = 1e-10;
ys_e = {trc,trc,trc,0.78-trc,0.22};

% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 1e-11;                % Convergence tolerance
options.it_max                  = 200;                  % maximum number of iterations
% options.it_res_sat              = options.it_max;       % residual saturation limit
options.T_tol                   = 1e-13;                % convergence tolerance for the NR to get T from the enthalpy
options.T_itMax                 = 200;                  % maximum number of iterations for the NR to get T from the enthalpy

% flow options
options.thermal_BC              = 'adiab';              % Wall bc (g or dg/deta)
options.G_bc                    = T_w;
options.flow                    = 'CNE';                % flow assumption
% options.mixture                 = 'air5mutation';       % mixture
options.mixture                 = 'air2';               % mixture
options.modelDiffusion          = 'cstSc';              % diffusion model
options.modelTransport          = 'BlottnerEucken';     % transport model
options.referenceVibRelax       = 'Mortensen2013';      % reference from which to obtain the vibrational relaxation constants
options.bParkCorrectionTauVib   = false;                % park's correction on the relaxation times
options.numberOfTemp            = '2T';                 % T-Tv model
options.dimoutput               = true;                 % we want dimensional profiles
options.mapOpts.x_start         = 1e-8;                 % m
options.mapOpts.x_end           = x_max;                % m
options.mapOpts.N_x             = 50;
options.xSpacing                = 'tanh';
% options.model_tauv              = 'straight';
% options.perturbRelaxation       = 0.5;
options.Fields4conv = {'df_deta3','dg_deta2','momXeq','enereq'};
options.ys_lim = 0;

% display options
options.plotRes = false;                        % we don't want the residual plot
options.MarchingGlobalConv = false;             % nor the marching global plot
options.fields2plot = {'gh','tauvh','fh','dfh_deta'};
options.fields2plot = {'tauv','tau'};
% options.fields2plot = {'OmegaV'};
% options.logPlots = true;

% options.gifConv = true;
options.fields4gif = {'gh','tauvh','fh','dfh_deta'};
options.gifLapse = 2;
options.gifLogPlot = true;
options.gifStep = 1;

% options.plot_transient = true;
options.xlim = [1e-16,Inf];
options.ylim = [0,6];
% options.xlim = [0,3];
% options.ylim = [0,0.5];

% options.gifMarch = true;
options.gifMarchxlim = options.xlim;
options.gifxlim = options.xlim;
options.fields4gifMarch = {'tauv','tau'};
options.fields4gifMarch = {'OmegaV'};

% flow conditions
Nx = 10; % just for the inflow profiles
intel.M_e               = M_e*ones(1,Nx);       % Mach number at boundary-layer edge        [-]
intel.T_e               = T_e*ones(1,Nx);       % temperature at boundary-layer edge        [K]
intel.Tv_e              = Tv_e*ones(1,Nx);
intel.x_e               = linspace(0,8.8e26,Nx);% limits of the observable universe         [m]
intel.Re1_e0            = Re1_e;                % unit Reynolds number based on edge        [1/m]
% intel.ys_e              = cellfun(@(cll)ones(1,Nx)*cll,ys_e,'UniformOutput',false);

[intel,options] = DEKAF(intel,options,'marching');

%% plotting
% hudson_u  = load('Hudson_0p05m_u_TCNE.csv');
% hudson_T  = load('Hudson_0p05m_T_TCNE.csv');
% hudson_Tv = load('Hudson_0p05m_Tv_TCNE.csv');
hudson_u  = load('Hudson_0p4m_u_TCNE.csv');
hudson_T  = load('Hudson_0p4m_T_TCNE.csv');
hudson_Tv = load('Hudson_0p4m_Tv_TCNE.csv');

ylims = real([0,intel.y_i(end)*1.3]);

figure
[Ni,Nj] = optimalPltSize(2); ic=1;
subplot(Ni,Nj,ic); ic=ic+1;
plot(intel.u(:,end),intel.y(:,end),'r-'); hold on;
plot(hudson_u(:,2),hudson_u(:,1),'ks'); hold on;
xlabel('u [m/s]'); ylabel('y [m]'); grid minor; ylim(ylims);
subplot(Ni,Nj,ic); ic=ic+1;
plot(intel.T(:,end),intel.y(:,end),'r-','displayname','DEKAF TNE trans-rot'); hold on;
plot(intel.Tv(:,end),intel.y(:,end),'c-','displayname','DEKAF TNE vib'); hold on;
plot(hudson_T(:,2),hudson_T(:,1),'ks','displayname','Hudson TNE trans-rot'); hold on;
plot(hudson_Tv(:,2),hudson_Tv(:,1),'kd','displayname','Hudson TNE vib'); hold on;
xlabel('T, T_v [K]'); ylabel('y [m]'); grid minor; ylim(ylims); legend();
