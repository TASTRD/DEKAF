% caller_DEKAF is an example of how to run the CPG test cases also run by
% Malik with DEKAF and VESTA's BL
%
% Conditions correspond to: Malik, M. R. (1990). Numerical methods for
% hypersonic boundary-layer stability. Journal of Computational Physics,
% 86(2), 376–413. https://doi.org/10.1016/0021-9991(90)90106-B
%
% Author: Fernando Miro Miro
% July 2019


clear;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))
addpath(genpath([getenv('VESTA_DIRECTORY'),'/VESTA_code']));

Rank2Kelv = 5/9;

% Flow conditions
M_vec  = [0.5 , 1e-6 , 2.5 ,   10  ,  10  , 4.5 ]; % [-]
T0_vec = [500 , 500  , 600 ,  4200 , 4200 , 1100]; % [R]
gam = 1.4;
T_vec  = T0_vec./(1+(gam-1)*M_vec.^2) * Rank2Kelv; % [K]
Re     = [2000, 580  , 3000 , 2000 , 1000 , 1500];
thermalBC = repmat({'adiab'},size(M_vec));  thermalBC{4} = 'Twall';
Trelax = 0.1;
Tadiab = NaN; % placeholder

provFileName = 'provVESTACBLFile.mat';
if exist(provFileName,'file'); delete(provFileName); end
data2save = struct; % allocating

Ncases = length(M_vec);
figure; [Ni,Nj] = optimalPltSize(Ncases*2);

vars2keep = who();
for ii=Ncases:-1:1
    keepVars(vars2keep{:},'vars2keep','ii');
% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size (in non-dimensional eta length)
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 5e-13;                % Convergence tolerance
options.it_max                  = 40;                   % maximum number of iterations

% flow options
options.thermal_BC              = thermalBC{ii};        % Wall bc (isothermal)
options.G_bc                    = Trelax*Tadiab;        % value of the wall temperature
options.flow                    = 'CPG';                % flow assumption
options.mixture                 = 'air2';               % mixture
options.modelTransport          = 'Sutherland';         % transport model
options.Cooke                   = false;                % we want to solve the Cooke equation too (quasi-3D)

% fluid options
options.specify_cp  = true;
options.specify_gam = true;
options.cstPr       = true;
intel.cp            = 1004.5;   % [J/kg-K]
intel.gam           = gam;      % [-]
options.Pr          = 0.7;      % [-]

% display options
options.plotRes = false;                         % we don't want the residual plot

% flow conditions
intel.M_e               = M_vec(ii);                  % Mach number at boundary-layer edge            [-]
intel.T_e               = T_vec(ii);                  % Temperature at boundary-layer edge            [K]
intel.Re1_e             = Re(ii);               % Unit Reynolds number at boundary-layer edge   [1/m]
options.dimoutput       = true;
options.fixReQSS        = true;
intel.Re                = Re(ii);

% Executing DEKAF
[intel,options] = DEKAF(intel,options);

Tadiab = intel.T(end);

%% Preparing VESTA CBL
VSTopts.H_F = strcmp(thermalBC{ii},'adiab');
if VSTopts.H_F;     VSTopts.G_bc = 0;
else;               VSTopts.G_bc = intel.g(end);
end
VSTopts.M_e = M_vec(ii);
VSTopts.T_e = T_vec(ii);
VSTopts.p_e = intel.p_e;
fluid.mu   = [intel.mixCnst.mu_ref, intel.mixCnst.T_ref, intel.mixCnst.Su_mu];
fluid.k    = [];
fluid.cp   = intel.cp_e;
fluid.Pr   = options.Pr;
fluid.gam  = intel.gam_e;
fluid.R    = [];
VSTopts.fluid = fluid;
VSTopts.save = true;
VSTopts.nodialog = true;
VSTopts.save_path = provFileName;
if M_vec(ii)==10
    VSTopts.import_sol = true;      VSTopts.FileName = provFileName;
end
compr_profile = CBL_profile(VSTopts); % solving SS equations

VSTopts.dim = true;
[mom,enr,eta,M,Pr,rho_e,mu_e,ni_e,cp_e,T_e,U_e,rho,mu,fluid,gam,fw] = import_flow_from_structure(compr_profile);
[U_dim,T_dim,W_dim,mu_dim,lambda_dim,k_dim,delta_1,y_dim] = profile_transform (mom,enr,eta,mu_e,rho_e,T_e,U_e,Re(ii),rho,Pr,fluid,VSTopts);

% Preparing to save
data2save(ii).uDKF = intel.u;
data2save(ii).TDKF = intel.T;
data2save(ii).yDKF = intel.y;

data2save(ii).uVST = U_dim.u;
data2save(ii).TVST = T_dim.T;
data2save(ii).yVST = y_dim;

data2save(ii).M_e = M_vec(ii);
data2save(ii).T_e = T_vec(ii);
data2save(ii).l_sc = intel.lBlasius;

%% plotting
subplot(Ni,Nj,2*(ii-1)+1);
plot(data2save(ii).uDKF,data2save(ii).yDKF,'-',...
     data2save(ii).uVST,data2save(ii).yVST,'--');  xlabel('u [m/s]'); ylabel('y [m]'); ylim([0,data2save(ii).yDKF(round(end/2))]);
 title(['Mach ',num2str(data2save(ii).M_e),', ',thermalBC{ii}]);
subplot(Ni,Nj,2*(ii-1)+2);
plot(data2save(ii).TDKF,data2save(ii).yDKF,'-',...
     data2save(ii).TVST,data2save(ii).yVST,'--');  xlabel('T [K]');   ylabel('y [m]'); ylim([0,data2save(ii).yDKF(round(end/2))]);
 title(['case ',num2str(ii)]);
end
legend('DEKAF','VESTA');

% Saving
saveDEKAF('DKF_CPGMalik.mat','data2save','thermalBC');
