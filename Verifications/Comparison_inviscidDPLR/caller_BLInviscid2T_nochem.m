% caller_BLInviscid2T_nochem is a script to run DEKAF's inviscid solver
% with the two-temperature model without the chemistry being on
%
% Date: December 2018

% close all;
clear intel options
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Optional inputs
% options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';
options.mixture                 = 'air2';               % mixture
options.flow                    = 'CNE';                % flow assumption
options.numberOfTemp            = '2T';                 % two-temperature model
options.inviscid_ox             = 6;                    % order of the inviscid solver
options.inviscidMap.x_end       = 1;                    % final x location
options.inviscidMap.x_start     = 1e-7;                 % initial x location
options.inviscidMap.N_x         = 3000;                 % number of points
options.xInviscidSpacing        = 'tanh';               % spacing
options.shockJump               = false;                % we don't want shock-jump relations
options.T_tol                   = 10*eps;               % tolerance for the NR on the temperature
options.neglect_hElec           = false;

options.dimXBL = true; % dummy options so that the defaults for inviscidMap.x_end and inviscidMap.x_start can be properly fixed
options.mapOpts.x_end       = 1;                    % final x location
options.mapOpts.x_start     = 1e-7;                 % initial x location

intel.U_e0 = 10000;
intel.T_e0 = 10000;
intel.Tv_e0 = 300;
intel.rho_e0 = 0.01;
options.marchYesNo = true;

% setting defaults and retrieving mixture constants
% options.referenceVibRelax = 'Mortensen2013';
% intelMort = intel;
% optionsMort = setDefaults(options);
% [intelMort.mixCnst,optionsMort] = getAll_constants(optionsMort.mixture,optionsMort);
% [intelMort,optionsMort] = DEKAF_inviscid(intelMort,optionsMort);

% Same story for Park93
options.referenceVibRelax = 'Park93';
intelPark = intel;
optionsPark = setDefaults(options);
[intelPark.mixCnst,optionsPark] = getAll_constants(optionsPark.mixture,optionsPark);
[intelPark,optionsPark] = DEKAF_inviscid(intelPark,optionsPark);

%% Importing DPLR solution
DPLR_data = load('DPLR_1Dinviscid2T_nochem.dat');
varList_DPLR = {'x','y','p','T','M','res','u','v','C_n2','C_o2','C_no','C_n','C_o','h_s','Tv','ev'}; % lists of variables outputted by DPLR
% varList_DPLR = {'x','y','p','T','M',      'u','v','C_n2','C_o2','C_no','C_n','C_o','h_s','Tv','ev'};

N_i = 5002; % size of the grid in the i direction (horizontal)
N_j = 22; % size of the grid in the j direction (vertical)

for ii=1:length(varList_DPLR) % looping DPLR variables
    DPLR_fields.(varList_DPLR{ii}) = reshape(DPLR_data(:,ii),[N_i,N_j])'; % extracting and reshaping variable
end % closing variable loop

% putting into a structure with the same names DEKAF outputs
DPLR2plt.x_e = DPLR_fields.x(int16(N_j/2),:).';
DPLR2plt.p_e = DPLR_fields.p(int16(N_j/2),:).';
DPLR2plt.TTrans_e = DPLR_fields.T(int16(N_j/2),:).';
DPLR2plt.TVib_e = DPLR_fields.Tv(int16(N_j/2),:).';
DPLR2plt.U_e = DPLR_fields.u(int16(N_j/2),:).';
DPLR2plt.h_e = DPLR_fields.h_s(int16(N_j/2),:).';
DPLR2plt.hv_e = DPLR_fields.ev(int16(N_j/2),:).';
DPLR2plt.y_se(:,1) = DPLR_fields.C_o2(int16(N_j/2),:).';
DPLR2plt.y_se(:,2) = DPLR_fields.C_n2(int16(N_j/2),:).';


%% plotting
fields2plt = {'U_e','p_e',{'TTrans_e','TVib_e'},'h_e'};%,'hv_e'};
units = {'[m/s]','[Pa]','[K]','[J/kg]','[J/kg]'};
N_spec = intelPark.mixCnst.N_spec;
N_plts = length(fields2plt);
[N_i,N_j] = optimalPltSize(N_plts);
xmax = 0.2;
figure
it = 1;
for ii=1:length(fields2plt)
    if ~iscell(fields2plt{ii})
        fields2plt{ii} = fields2plt(ii);
    end
    switch size(intelPark.(fields2plt{ii}{1}),2)
        case 1
            subplot(N_i,N_j,it); it = it+1;
            for jj=1:length(fields2plt{ii})
                plot(intelPark.x_e,intelPark.(fields2plt{ii}{jj}),'r-'); hold on;
%                 plot(intelMort.x_e,intelMort.(fields2plt{ii}{jj}),'b-.');
                plot(DPLR2plt.x_e,DPLR2plt.(fields2plt{ii}{jj}),'k--');
            end
            xlabel('x [m]'); ylabel(strrep([strjoin(fields2plt{ii},', '),' ',units{ii}],'_','\_')); grid minor; xlim([0,xmax]);
        case N_spec
            for s=1:N_spec
                subplot(N_i,N_j,it); it = it+1;
                for jj=1:length(fields2plt{ii})
                    plot(intelPark.x_e,intelPark.(fields2plt{ii}{jj})(:,s),'r-'); hold on;
%                     plot(intelMort.x_e,intelMort.(fields2plt{ii}{jj})(:,s),'b-.');
                    plot(DPLR2plt.x_e,DPLR2plt.(fields2plt{ii}{jj})(:,s),'k--');
                end
                xlabel('x [m]'); ylabel([strrep(fields2plt{ii}{1},'_','\_'),'(',intelPark.mixCnst.spec_list{s},') ',units{ii}]); grid minor; xlim([0,xmax]);
            end
    end
end
% legend('DEKAF Park93','DEKAF Mortensen2013','DPLR');
legend('DEKAF Park93','DPLR');