% caller_BLInviscid is a script to run DEKAF's inviscid solver
%
% Date: March 2018

% close all;
clear intel options
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Optional inputs
% options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';
options.mixture                 = 'air5mutation';       % mixture
options.flow                    = 'CNE';                % flow assumption
options.inviscid_ox             = 3;                    % order of the inviscid solver
options.mapOpts.x_end           = 1;                    % final x location
options.mapOpts.x_start         = 1e-7;                 % initial x location
options.inviscidMap.N_x         = 3000;                 % number of points
options.xInviscidSpacing        = 'tanh';               % spacing
options.shockJump               = false;                % we don't want shock-jump relations
options.T_tol                   = 10*eps;               % tolerance for the NR on the temperature

%%% DEBUGGING
% options.modelThermal = '2T';
% intel.Tv_e0 = 10000;
%%% UNTIL HERE

intel.U_e0 = 10000;
intel.T_e0 = 10000;
intel.rho_e0 = 0.01;
options.marchYesNo = true;

% setting defaults and retrieving mixture constants
options = setDefaults(options);
[intel.mixCnst,options] = getAll_constants(options.mixture,options);

% options for the inviscid solver

[intel,options] = DEKAF_inviscid(intel,options);

%% Importing DPLR solution
DPLR_data = load('DPLR_1Dinviscid.dat');

varList_DPLR = {'x','p','T','M','u','C_n2','C_o2','C_no','C_n','C_o','h_s'}; % lists of variables outputted by DPLR

N_i = 5002; % size of the grid in the i direction (horizontal)
N_j = 22; % size of the grid in the j direction (vertical)

for ii=1:length(varList_DPLR) % looping DPLR variables
    DPLR_fields.(varList_DPLR{ii}) = reshape(DPLR_data(:,ii),[N_i,N_j])'; % extracting and reshaping variable
end % closing variable loop

% putting into a structure with the same names DEKAF outputs
DPLR2plt.x_e = DPLR_fields.x(int16(N_j/2),:).';
DPLR2plt.p_e = DPLR_fields.p(int16(N_j/2),:).';
DPLR2plt.TTrans_e = DPLR_fields.T(int16(N_j/2),:).';
DPLR2plt.U_e = DPLR_fields.u(int16(N_j/2),:).';
DPLR2plt.h_e = DPLR_fields.h_s(int16(N_j/2),:).';
DPLR2plt.y_se(:,1) = DPLR_fields.C_n(int16(N_j/2),:).';
DPLR2plt.y_se(:,2) = DPLR_fields.C_o(int16(N_j/2),:).';
DPLR2plt.y_se(:,3) = DPLR_fields.C_no(int16(N_j/2),:).';
DPLR2plt.y_se(:,4) = DPLR_fields.C_n2(int16(N_j/2),:).';
DPLR2plt.y_se(:,5) = DPLR_fields.C_o2(int16(N_j/2),:).';


%% plotting
fields2plt = {'y_se','U_e','p_e','TTrans_e','h_e'};
units = {'[-]','[m/s]','[Pa]','[K]','[J/kg]'};
N_spec = intel.mixCnst.N_spec;
N_plts = length(fields2plt)-1+N_spec;
[N_i,N_j] = optimalPltSize(N_plts);
figure
it = 1;
for ii=1:length(fields2plt)
    switch size(intel.(fields2plt{ii}),2)
        case 1
            subplot(N_i,N_j,it); it = it+1;
            plot(intel.x_e,intel.(fields2plt{ii}),'r-'); hold on;
            plot(DPLR2plt.x_e,DPLR2plt.(fields2plt{ii}),'k--'); grid minor;
            xlabel('x [m]'); ylabel([fields2plt{ii},' ',units{ii}]);
        case N_spec
            for s=1:N_spec
            subplot(N_i,N_j,it); it = it+1;
            plot(intel.x_e,intel.(fields2plt{ii})(:,s),'r-'); hold on;
            plot(DPLR2plt.x_e,DPLR2plt.(fields2plt{ii})(:,s),'k--'); grid minor;
            xlabel('x [m]'); ylabel([fields2plt{ii},'_{',intel.mixCnst.spec_list{s},'} ',units{ii}]);
            end
    end
end