% caller_BLInviscid2T is a script to run DEKAF's inviscid solver with the
% two-temperature model
%
% Date: March 2018

% close all;
clear intel options
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Optional inputs
% options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';
options.mixture                 = 'air5mutation';       % mixture
options.flow                    = 'CNE';                % flow assumption
options.numberOfTemp            = '2T';                 % two-temperature model
options.inviscid_ox             = 6;                    % order of the inviscid solver
options.inviscidMap.x_end       = 1;                    % final x location
options.inviscidMap.x_start     = 1e-7;                 % initial x location
options.inviscidMap.N_x         = 3000;                 % number of points
options.xInviscidSpacing        = 'tanh';               % spacing
options.shockJump               = false;                % we don't want shock-jump relations
options.T_tol                   = 10*eps;               % tolerance for the NR on the temperature
options.neglect_hElec           = false;

options.dimXBL = true; % dummy options so that the defaults for inviscidMap.x_end and inviscidMap.x_start can be properly fixed
options.mapOpts.x_end       = 0.5;                    % final x location
options.mapOpts.x_start     = 1e-7;                 % initial x location

intel.U_e0 = 10000;
intel.T_e0 = 10000;
intel.Tv_e0 = 300;
intel.rho_e0 = 0.01;
options.marchYesNo = true;
options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';

% Same story for Park93
options.referenceVibRelax   = 'Park93';
options.inviscidMethod      = 'FD';
optionsFD   = setDefaults(options);
[intel.mixCnst,optionsFD] = getAll_constants(optionsFD.mixture,optionsFD);
intelFD     = intel;
[intelFD,optionsFD] = DEKAF_inviscid(intelFD,optionsFD);

intelODE    = intel;
optionsODE  = optionsFD;    optionsODE.inviscidMethod = 'ode45';
[intelODE,optionsODE] = DEKAF_inviscid(intelODE,optionsODE);

%% Importing DPLR solution
DPLR_data = load('DPLR_1Dinviscid2T.dat');

varList_DPLR = {'x','y','p','T','M','u','v','C_n2','C_o2','C_no','C_n','C_o','h_s','Tv','ev'}; % lists of variables outputted by DPLR

N_i = 5002; % size of the grid in the i direction (horizontal)
N_j = 22; % size of the grid in the j direction (vertical)

for ii=1:length(varList_DPLR) % looping DPLR variables
    DPLR_fields.(varList_DPLR{ii}) = reshape(DPLR_data(:,ii),[N_i,N_j])'; % extracting and reshaping variable
end % closing variable loop

% putting into a structure with the same names DEKAF outputs
DPLR2plt.x_e = DPLR_fields.x(int16(N_j/2),:).';
DPLR2plt.p_e = DPLR_fields.p(int16(N_j/2),:).';
DPLR2plt.TTrans_e = DPLR_fields.T(int16(N_j/2),:).';
DPLR2plt.TVib_e = DPLR_fields.Tv(int16(N_j/2),:).';
DPLR2plt.U_e = DPLR_fields.u(int16(N_j/2),:).';
DPLR2plt.h_e = DPLR_fields.h_s(int16(N_j/2),:).';
DPLR2plt.hv_e = DPLR_fields.ev(int16(N_j/2),:).';
DPLR2plt.y_se(:,1) = DPLR_fields.C_n(int16(N_j/2),:).';
DPLR2plt.y_se(:,2) = DPLR_fields.C_o(int16(N_j/2),:).';
DPLR2plt.y_se(:,3) = DPLR_fields.C_no(int16(N_j/2),:).';
DPLR2plt.y_se(:,4) = DPLR_fields.C_n2(int16(N_j/2),:).';
DPLR2plt.y_se(:,5) = DPLR_fields.C_o2(int16(N_j/2),:).';

spec_list = intelFD.mixCnst.spec_list;
save('data_inviscid2T.mat','intelFD','intelODE','DPLR2plt','spec_list');


%% plotting
fields2plt = {'y_se','U_e','p_e',{'TTrans_e','TVib_e'},'h_e'};%,'hv_e'};
units = {'[-]','[m/s]','[Pa]','[K]','[J/kg]','[J/kg]'};
N_spec = intelFD.mixCnst.N_spec;
N_plts = length(fields2plt)-1+N_spec;
[N_i,N_j] = optimalPltSize(N_plts);
figure
it = 1;
for ii=1:length(fields2plt)
    if ~iscell(fields2plt{ii})
        fields2plt{ii} = fields2plt(ii);
    end
    switch size(intelFD.(fields2plt{ii}{1}),2)
        case 1
            subplot(N_i,N_j,it); it = it+1;
            for jj=1:length(fields2plt{ii})
                plot(intelFD.x_e,intelFD.(fields2plt{ii}{jj}),'r-'); hold on;
                plot(intelODE.x_e,intelODE.(fields2plt{ii}{jj}),'c-.');
                plot(DPLR2plt.x_e,DPLR2plt.(fields2plt{ii}{jj}),'k--');
            end
            xlabel('x [m]'); ylabel(strrep([strjoin(fields2plt{ii},', '),' ',units{ii}],'_','\_')); grid minor;
        case N_spec
            for s=1:N_spec
                subplot(N_i,N_j,it); it = it+1;
                for jj=1:length(fields2plt{ii})
                    plot(intelFD.x_e,intelFD.(fields2plt{ii}{jj})(:,s),'r-'); hold on;
                    plot(intelODE.x_e,intelODE.(fields2plt{ii}{jj})(:,s),'c-.');
                    plot(DPLR2plt.x_e,DPLR2plt.(fields2plt{ii}{jj})(:,s),'k--');
                end
                xlabel('x [m]'); ylabel([strrep(fields2plt{ii}{1},'_','\_'),'(',intelFD.mixCnst.spec_list{s},') ',units{ii}]); grid minor;
            end
    end
end
legend('DEKAF FD','DEKAF ode45','DPLR');