clear all;
close all;
clc;

% Comparison of the self-similar boundary-layer profile in INCA, the DNS
% code of TU Delft, at M = 0.7, x = 0.133368342085521 [m] (input to DEKAF),
% corresponding to delta99 = 1.5727, Re_delta99 = 1101.4

fileID=fopen('XPlaneBLM07SFDCenterPlane.dat');
Data=textscan(fileID, '%f %f %f %f %f %f','headerLines',12,'commentstyle',{'ZONE',')'});
fclose(fileID);
m=size(Data{1,1});
m=m(1);
Data=cell2mat(Data);
y=unique(Data(:,1));
u=unique(Data(:,3));
v=unique(Data(:,4));
w=unique(Data(:,5));
T=unique(Data(:,6));

load('INCAinlet_comparison_xInlet0p13337')

figure(1)
plot(u(1:end-1),y,'k-o','LineWidth',1);
hold on;
plot(uEc0/uEc0(1),yEc0/0.002,'rx','LineWidth',1);
plot(uDEKAF/uEc0(1),yDEKAF/0.002,'bx','LineWidth',1);
grid on;
ylabel('y');
xlabel('u');
ylim([0 3])
legend({'INCA','$Ec = 0$ (old IC)','DEKAF','DEKAF scaled to prev.inlet Re'},...
    'interpreter','latex','location','northwest')

figure(2)
plot(v(1:end-1),y,'k-o','LineWidth',1);
hold on;
plot(vEc0/uEc0(1),yEc0/0.002,'rx','LineWidth',1);
plot(vDEKAF/uEc0(1),yDEKAF/0.002,'bx','LineWidth',1);
grid on;
ylabel('y');
xlabel('v');
ylim([0 3])
legend({'INCA','$Ec = 0$ (old IC)','DEKAF'},...
    'interpreter','latex','location','northwest')
%{
figure(3)
plot(w(1:end-1),y,'g-','LineWidth',1);
hold on;
plot(w(1:end-1),y,'go','LineWidth',1);
grid on;
ylabel('y');
xlabel('w');
%}
figure(4)
plot(flip(T),y(1:end-3),'k-o','LineWidth',1);
hold on;
plot(TEc0/TEc0(1),yEc0/0.002,'rx','LineWidth',1);
plot(TDEKAF/TEc0(1),yDEKAF/0.002,'bx','LineWidth',1);
grid on;
ylabel('y');
xlabel('T');
ylim([0 3])
legend({'INCA','$Ec = 0$ (old IC)','DEKAF'},...
    'interpreter','latex','location','northwest')
