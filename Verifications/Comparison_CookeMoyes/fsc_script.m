function [eta_final,data_final] = fsc_script(error,guess,etaspan,compare_eqns)

% ode45 options
options = odeset('MaxStep',0.1);

% compressible solution, adiabatic wall
% boundary conditions
f_0 = 0;
f_p0 = 0;
f_pinf = 1;
k_inf = 1;
k_p0 = 0;
g_0 = 0;
g_inf = 1;

% newton rhapson initialization
f_pp0guess = guess(1);
k_0guess = guess(2);
g_p0guess = guess(3);
stepsize = 1e-10;
convergence_f = 1;
convergence_k = 1;
convergence_g = 1;

% newton rhapson for f and k and g
counter = 0;
fprintf('\n');
fprintf('\n');
fprintf('\n');
if compare_eqns == 1
    fprintf('----------------- Moyes FSC with Gaponov 2008 Equations ------------------\n');
elseif compare_eqns == 2
    fprintf('------------------- Moyes FSC with Liu 1997 Equations --------------------\n');
end
fprintf('--- Running...\n');
while convergence_g > error && convergence_k > error && convergence_f > error
    g_p0 = g_p0guess;
    k_0 = k_0guess;
    f_pp0 = f_pp0guess;
    [~,data] = ode45(@fsc_compressible,etaspan,[f_0 f_p0 f_pp0 k_0 k_p0 g_0 g_p0],options);

    g_p0 = g_p0guess+stepsize;
    k_0 = k_0guess;
    f_pp0 = f_pp0guess;
    [~,datag] = ode45(@fsc_compressible,etaspan,[f_0 f_p0 f_pp0 k_0 k_p0 g_0 g_p0],options);

    g_p0 = g_p0guess;
    k_0 = k_0guess+stepsize;
    f_pp0 = f_pp0guess;
    [~,datak] = ode45(@fsc_compressible,etaspan,[f_0 f_p0 f_pp0 k_0 k_p0 g_0 g_p0],options);

    g_p0 = g_p0guess;
    k_0 = k_0guess;
    f_pp0 = f_pp0guess+stepsize;
    [~,dataf] = ode45(@fsc_compressible,etaspan,[f_0 f_p0 f_pp0 k_0 k_p0 g_0 g_p0],options);

    dg = (datag(end,6)-data(end,6))/stepsize;
    g_p0guess = g_p0guess-(data(end,6)-g_inf)/dg;
    convergence_g = abs((data(end,6)-g_inf)/g_inf);

    dk = (datak(end,4)-data(end,4))/stepsize;
    k_0guess = k_0guess-(data(end,4)-k_inf)/dk;
    convergence_k = abs((data(end,4)-k_inf)/k_inf);

    df_p = (dataf(end,2)-data(end,2))/stepsize;
    f_pp0guess = f_pp0guess-(data(end,2)-f_pinf)/df_p;
    convergence_f = abs((data(end,2)-f_pinf)/f_pinf);

    counter = counter+1;
    fprintf('--- Iteration %d, with error %.2e\n',counter,max([convergence_g convergence_k convergence_f]));
end
fprintf('--- Done\n');

% final solution
g_p0 = g_p0guess;
k_0 = k_0guess;
f_pp0 = f_pp0guess;
[eta_final,data_final] = ode45(@fsc_compressible,etaspan,[f_0 f_p0 f_pp0 k_0 k_p0 g_0 g_p0],options);

end
