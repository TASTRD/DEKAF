function prime = fsc_compressible(eta,f)

% % % f(1) = f, f(2) = f_p, f(3) = f_pp, f(4) = k, f(5) = k_p, f(6) = g, f(7) = g_p
global visc_type Pr Su_mu C_1 mu_e h_e U_e W_0 m cp R P_e rho_e gamma Lambda_sweep T_e M_e compare_eqns

if visc_type == 0
    % % % power law approximation from Viscous Fluid Flow, White
    C = (f(4)^(-1/3));
    C_p = ((-1/3)*(f(4)^(-4/3))*(f(5)));
elseif visc_type == 1
    % % % sutherlands law
    if compare_eqns == 1
        % Total Enthalpy, gaponov 2008
        H_e = h_e + U_e^2/2 + W_0^2/2;
        h = f(4)*H_e - U_e^2*(f(2)^2)/2 - W_0^2*(f(6)^2)/2;
        h_prime =  f(5)*H_e - U_e^2*(f(2)*f(3)) - W_0^2*(f(6)*f(7));
    elseif compare_eqns == 2
        % Static Enthalpy, liu 1997
        h = f(4)*h_e;
        h_prime = f(5)*h_e;
    end

    T = h/cp;
    T_prime = h_prime/cp;
    rho = (P_e/R)/T;
    rho_prime = -P_e/R*T_prime/(T^2);
    mu = C_1*T^(3/2)/(T+Su_mu);
    mu_T = C_1*(3/2*T^(1/2)*1/(T + Su_mu) - T^(3/2)/((T + Su_mu)^2));
    mu_prime = mu_T*T_prime;
    C = 1/(rho_e*mu_e)*(rho*mu);
    C_p = (1/(rho_e*mu_e)*(rho_prime*mu + mu_prime*rho));
end

if compare_eqns == 1
    % Equations, gaponov 2008
    prime = zeros(5,1);
    prime(1) = f(2);
    prime(2) = f(3);
    prime(3) = -(1/C)*(C_p*f(3)+f(1)*f(3)+(2*m/(m+1))*(rho_e/rho-(f(2)^2)));
    prime(4) = f(5);
    prime(5) = (Pr/C)*(-C_p/Pr*f(5)-f(1)*f(5)+((1-Pr)/Pr)*(((gamma-1)*M_e^2)/(1+((gamma-1)/2)*M_e^2))*(C_p*f(2)*f(3)*cosd(Lambda_sweep)^2)+((1-Pr)/Pr)*(((gamma-1)*M_e^2)/(1+((gamma-1)/2)*M_e^2))*(C_p*f(6)*f(7)*sind(Lambda_sweep)^2)+...
        ((1-Pr)/Pr)*(((gamma-1)*M_e^2)/(1+((gamma-1)/2)*M_e^2))*(C*f(3)*f(3)*cosd(Lambda_sweep)^2)+((1-Pr)/Pr)*(((gamma-1)*M_e^2)/(1+((gamma-1)/2)*M_e^2))*(C*f(2)*(-(1/C)*(C_p*f(3)+f(1)*f(3)+(2*m/(m+1))*(rho_e/rho-(f(2)^2))))*cosd(Lambda_sweep)^2)+...
        ((1-Pr)/Pr)*(((gamma-1)*M_e^2)/(1+((gamma-1)/2)*M_e^2))*(C*f(7)*f(7)*sind(Lambda_sweep)^2)+((1-Pr)/Pr)*(((gamma-1)*M_e^2)/(1+((gamma-1)/2)*M_e^2))*(C*f(6)*(-(1/C)*(C_p*f(7)+f(1)*f(7)))*sind(Lambda_sweep)^2));
    prime(6) = f(7);
    prime(7) = -(1/C)*(C_p*f(7)+f(1)*f(7));
elseif compare_eqns == 2
    % Equations, liu 1997
    prime = zeros(5,1);
    prime(1) = f(2);
    prime(2) = f(3);
    prime(3) = -(1/C)*((C_p)*f(3)+f(1)*f(3)+2*m*(f(4)-(f(2)^2)));
    prime(4) = f(5);
    prime(5) = -(Pr/C)*(C_p/Pr*f(5)+f(1)*f(5)+(C/h_e)*((U_e^2)*(f(3)^2)+(W_0^2)*(f(7)^2)));
    prime(6) = f(7);
    prime(7) = -(1/C)*((C_p)*f(7)+f(1)*f(7));
end

end
