% caller_FSC_Cooke_MoyesGapLiu is a script to test the z-momentum terms in the
% CPG FSC equations. DEKAF is verified against a lightweight solver
% developed by Alex Moyes, which recreates the equations of Gaponov 2008 and Liu 1997.
% Moyes' solver is detailed in two small scripts, fsc_script.m
% and fsc_compressible.m.
%
% References:
% Gaponov 2008.
% Liu, Z., Zhao, W., Lui, C. 1997. Direct Numerical Simulation of Flow
%   Transition in a Compressible Swept Wing Boundary Layer. Ruston.
%
% Date: May 2018

clear
close all
clc
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

%%
% Moyes inputs
global visc_type Pr Su_mu C_1 mu_e h_e U_e W_0 m cp R P_e rho_e gamma Lambda_sweep T_e M_e compare_eqns

compare_eqns = 1;                      % 1 = gaponov 2008, 2 = liu 1997

% gas properties and constants
visc_type = 1;                         % use Sutherland's if 1
Pr = 0.7;                              % constant prandtl number
R = 287.058;                           % gas constant
gamma = 1.4;                           % constant gamma
T_0 = 273.15;                          % reference temp of Sutherland's
mu_ref = 1.716e-5;                     % reference mu of Sutherland's
Su_mu = 110.6;                         % ref const. for mu Sutherland's law
cv = R/(gamma-1);
cp = gamma*cv;
C_1 = mu_ref/(T_0^(3/2))*(T_0+Su_mu);

% flow variables
M_e = 2;                               % edge Mach of resultant
T_e = 600/(1.8*(1+0.2*2.5^2));         % edge temperature, K
rho_e = 0.1042;                        % edge density, kg/m^3
P_e = rho_e*R*T_e;                     % edge pressure
h_e = cp*T_e;                          % static enthalpy
mu_e = C_1*((T_e^(3/2))/(T_e+Su_mu));
Lambda_sweep = 45;                     % sweep angle of flat plate, deg
Q_e = M_e*sqrt(gamma*R*T_e);           % resultant velocity magnitude at edge
U_e = Q_e*cosd(Lambda_sweep);          % streamwise velocity at edge
W_0 = Q_e*sind(Lambda_sweep);          % spanwise velocity at edge
m = 0.15;                                 % pressure gradient value

% numerics
tol = 1e-15;
N_eta = 25;
eta_max = 10;
eta_i = 5;

%%
% DEKAF inputs
options.Cooke                   = true;
options.N                       = N_eta;                % Number of points
options.L                       = eta_max;              % Domain size
options.eta_i                   = eta_i;                % Mapping parameter, eta_crit
options.thermal_BC              = 'adiab';              % Wall bc
options.flow                    = 'CPG';                % flow assumption
options.mixture                 = 'air2';               % mixture
options.modelTransport          = 'Sutherland';         % transport model
options.cstPr                   = true;                 % constant Prandtl number
options.specify_cp              = true;                 % user-specified cp
options.specify_gam             = true;                 % user-specified gam

options.tol                     = tol;                  % Convergence tolerance
options.it_max                  = 100;

options.plot_transient          = false;                % to plot the inter-iteration profiles
options.plotRes                 = true;                 % to plot the residual convergence
options.gifConv                 = false;

options.dimoutput               = false;                % to output fields in dimensional form
options.dimXQSS                 = false;                % we want to input the dimensional x location (not xi)

% Flow parameters
intel.M_e                       = M_e;                  % Mach number at boundary-layer edge                        [-]
intel.T_e                       = T_e;                  % Static temperature at boundary-layer edge                 [K]
intel.rho_e                     = rho_e;                % mixture mass density at boundary-layer edge               [kg/m^3]
intel.Lambda_sweep              = Lambda_sweep;
intel.cp                        = cp;                   % Constant specific heat                                    [J/(kg K)]
intel.gam                       = gamma;                % Constant specific heat ratio                              [-]
if compare_eqns == 1
    intel.beta                  = 2*m/(m+1);            % beta hartree parameter
elseif compare_eqns == 2
    intel.beta                  = 2*m;                  % beta hartree parameter
end
options.Su_mu                   = Su_mu;
options.Pr                      = Pr;                   % Constant specific heat ratio                              [-]

% --- Run DEKAF --- %
intel_out = DEKAF(intel,options);

%% Run Moyes FSC Solver
fpp_w = intel_out.df_deta2(end);                                          % note that DEKAF's wall is 'end' index
if compare_eqns == 1
    A = (1+intel_out.Ecu/2)/(1+intel_out.Ecu/2+intel_out.Ecw/2);
    B = (intel_out.Ecw/2)/(1+intel_out.Ecu/2+intel_out.Ecw/2);
    k_w = A*intel_out.g(end)+B*intel_out.k(end)^2;
elseif compare_eqns == 2
    k_w = intel_out.j(end);
end
gp_w = intel_out.dk_deta(end);
guess = [fpp_w,k_w,gp_w];
[eta_final,data_final] = fsc_script(tol,guess,flipud(intel_out.eta),compare_eqns);

%% Plotting
% data_final is [f, f', f'', k, k', g, g'], where
% f' = u/U_e
% k = h/h_e (corresponding enthalpy ratio)
% g = w/W_0
% ...following nomenclature in fsc_compressible.m

clear legends plots
moyes.eta        = eta_final;
moyes.f          = data_final(:,1);
moyes.df_deta    = data_final(:,2);
moyes.df_deta2   = data_final(:,3);
moyes.k          = data_final(:,6);
moyes.dk_deta    = data_final(:,7);
moyes.g          = data_final(:,4); % static enthalpy
moyes.dg_deta    = data_final(:,5);
if compare_eqns == 1
    moyes.semiH  = (moyes.g-B*moyes.k.^2)/A; % total enthalpy to semi-total enthalpy converter
    moyes.semiHp = (moyes.dg_deta-2*B*moyes.k.*moyes.dk_deta)/A;
elseif compare_eqns == 2
    moyes.semiH = 1/(1+intel_out.Ecu/2)*(moyes.g+intel_out.Ecu/2*moyes.df_deta.^2); % static enthalpy to semi-total enthalpy converter
    moyes.semiHp = 1/(1+intel_out.Ecu/2)*(moyes.dg_deta+intel_out.Ecu*moyes.df_deta.*moyes.df_deta2);
end

lw = 1.25;
p = 1;
figure

if compare_eqns == 1
    legend_data = [
        'f Gap. 08  ';
        'f DEKAF    ';
        'fp Gap. 08 ';
        'fp DEKAF   ';
        'fpp Gap. 08';
        'fpp DEKAF  ';
        'k Gap. 08  ';
        'k DEKAF    ';
        'kp Gap. 08 ';
        'kp DEKAF   ';
        'g Gap. 08  ';
        'g DEKAF    ';
        'gp Gap. 08 ';
        'gp DEKAF   '];
elseif compare_eqns == 2
    legend_data = [
        'f Liu 97  ';
        'f DEKAF   ';
        'fp Liu 97 ';
        'fp DEKAF  ';
        'fpp Liu 97';
        'fpp DEKAF ';
        'k Liu 97  ';
        'k DEKAF   ';
        'kp Liu 97 ';
        'kp DEKAF  ';
        'g Liu 97  ';
        'g DEKAF   ';
        'gp Liu 97 ';
        'gp DEKAF  '];
end

% --- f --- %
plots(p) = plotLessMarkers_VESTA(moyes.f,moyes.eta,73,'+',...
    ...'linestyle','-',...
    'color','k',...
    'linewidth',lw); p = p+1;
plots(p) = plotLessMarkers_VESTA(intel_out.f,intel_out.eta,37,'o',...
    ...'linestyle','-.',...
    'color','k',...
    'linewidth',lw); p = p+1;

% --- f' --- %
plots(p) = plotLessMarkers_VESTA(moyes.df_deta,moyes.eta,11,'+',...
    ...'linestyle','-',...
    'color','r',...
    'linewidth',lw); p = p+1;
plots(p) = plotLessMarkers_VESTA(intel_out.df_deta,intel_out.eta,9,'o',...
    ...'linestyle','-.',...
    'color','r',...
    'linewidth',lw); p = p+1;

% --- f'' --- %
plots(p) = plotLessMarkers_VESTA(moyes.df_deta2,moyes.eta,9,'+',...
    ...'linestyle','-',...
    'color','b',...
    'linewidth',lw); p = p+1;
plots(p) = plotLessMarkers_VESTA(intel_out.df_deta2,intel_out.eta,13,'o',...
    ...'linestyle','-.',...
    'color','b',...
    'linewidth',lw); p = p+1;

% --- k --- %
plots(p) = plotLessMarkers_VESTA(moyes.k,moyes.eta,15,'+',...
    ...'linestyle','-',...
    'color','g',...
    'linewidth',lw); p = p+1;
plots(p) = plotLessMarkers_VESTA(intel_out.k,intel_out.eta,13,'o',...
    ...'linestyle','-.',...
    'color','g',...
    'linewidth',lw); p = p+1;

% --- k' --- %
plots(p) = plotLessMarkers_VESTA(moyes.dk_deta,moyes.eta,11,'+',...
    ...'linestyle','-',...
    'color','m',...
    'linewidth',lw); p = p+1;
plots(p) = plotLessMarkers_VESTA(intel_out.dk_deta,intel_out.eta,14,'o',...
    ...'linestyle','-.',...
    'color','m',...
    'linewidth',lw); p = p+1;

% --- g --- %
plots(p) = plotLessMarkers_VESTA(moyes.semiH,moyes.eta,8,'+',...
    ...'linestyle','-',...
    'color','c',...
    'linewidth',lw); p = p+1;
plots(p) = plotLessMarkers_VESTA(intel_out.g,intel_out.eta,11,'o',...
    ...'linestyle','-.',...
    'color','c',...
    'linewidth',lw); p = p+1;

% --- g' --- %
plots(p) = plotLessMarkers_VESTA(moyes.semiHp,moyes.eta,22,'+',...
    ...'linestyle','-',...
    'color',viridis(3,5),...
    'linewidth',lw); p = p+1;
plots(p) = plotLessMarkers_VESTA(intel_out.dg_deta,intel_out.eta,34,'o',...
    ...'linestyle','-.',...
    'color',viridis(3,5),...
    'linewidth',lw); p = p+1;

axis([-0.1 1.5 0 6]);
ylabel('\eta (-)');
legend(plots,legend_data)