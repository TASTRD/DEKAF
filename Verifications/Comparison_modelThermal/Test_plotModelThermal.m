% Test_plotEoS simply plots the pressure for various equations of state
%
% Author: Fernando Miro Miro
% Date: April 2020


clear; %close all;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
p = 1e3;            % [Pa]
T = (100:1:35000).';      % [K]
modelThermal_vec = {'RRHO','NASA7','NASA7'};
bLinearInterp = [0,0,1];
% mixture = 'N2';     y_s = ones(size(T));
mixture = 'air2';   y_s = [0.232*ones(size(T)) , 0.767*ones(size(T))];
flow = 'TPG';


options.mixture = mixture;
options.flow = flow;
Nm = length(modelThermal_vec);
for ii=Nm:-1:1
    options.modelThermal = modelThermal_vec{ii};
    options.bLinearInterpThermalPoly = bLinearInterp(ii);
    [mixCnst,options] = getAll_constants(mixture,options);
    [h{ii},cp{ii}] = getMixture_h_cp_noMat(y_s,T,T,T,T,T,mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,...
                                mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,options,mixCnst.AThermFuncs_s);
    gibbs{ii} = getMixture_gibbs(y_s,T,p,mixCnst.R_s,mixCnst.Mm_s,mixCnst.thetaRot_s,mixCnst.L_s,mixCnst.sigma_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,...
                                mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.kBoltz,mixCnst.hPlanck,mixCnst.nAvogadro,mixCnst.hForm_s,options,mixCnst.AThermFuncs_s);
end

%% plotting
styleS = {'-','-.','--'};
figure;Ni=1;Nj=3;
    
for ii=1:Nm
    clr  = two_colors(ii,Nm+1,'Rbw2');
    if bLinearInterp(ii);   name = [modelThermal_vec{ii},' with coef. linear interp'];
    else;                   name = modelThermal_vec{ii};
    end
    ip=0;
    ip=ip+1; subplot(Ni,Nj,ip); plot(T,h{ii},    styleS{ii},'Color',clr,'displayname',name); hold on;
    ip=ip+1; subplot(Ni,Nj,ip); plot(T,cp{ii},   styleS{ii},'Color',clr,'displayname',name); hold on;
    ip=ip+1; subplot(Ni,Nj,ip); plot(T,gibbs{ii},styleS{ii},'Color',clr,'displayname',name); hold on;
end
ip=0;
ip=ip+1; subplot(Ni,Nj,ip); xlabel('T [K]'); ylabel('h [J/kg]'); grid minor;
ip=ip+1; subplot(Ni,Nj,ip); xlabel('T [K]'); ylabel('cp [J/kg-K]'); grid minor;legend();
ip=ip+1; subplot(Ni,Nj,ip); xlabel('T [K]'); ylabel('g [J/kg]'); grid minor;
