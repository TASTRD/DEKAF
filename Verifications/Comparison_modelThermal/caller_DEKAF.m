% caller_DEKAF runs the SS solver for the test on the thermal models.
%
% Author: Fernando Miro Miro
% April 2020


clear; clc;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
M_e = 15;            % [m/s]
T_e = 500;         % [K]
Tw  = 8000;         % [K]
p_e = 1e4;          % [Pa]
x   = 2;            % [m]
modelThermal_vec = {'RRHO','NASA7','NASA7'};
bLinearInterp = [0,0,1];
Nm = length(modelThermal_vec);

% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size (in non-dimensional eta length)
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 5e-13;                % Convergence tolerance
options.it_max                  = 40;                   % maximum number of iterations

% flow options
% options.thermal_BC              = 'adiab';              % Wall bc
options.thermal_BC              = 'Twall';              % Wall bc (isothermal)
options.G_bc                    = Tw;                   % value of the wall temperature
options.flow                    = 'TPG';                % flow assumption
options.mixture                 = 'air2';               % mixture
options.modelTransport          = 'CE_122';             % transport model

options.BLproperties = true;
options.dimoutput = true;
options.dimXQSS = true;

% display options
options.plotRes = false;                         % we don't want the residual plot

% customized polynomial coefficients (only used for modelThermal = 'customPoly7'
% s = 1;          % O2
% AThermNASA7_s{s,1} =  [0.36146E+01  0.35949E+01  0.38599E+01  0.34867E+01  0.39620E+01 ; 
%                       -0.18598E-02  0.75213E-03  0.32510E-03  0.52384E-03  0.39446E-03 ;
%                        0.70814E-05 -0.18732E-06 -0.92131E-08 -0.39123E-07 -0.29506E-07 ;
%                       -0.68070E-08  0.27913E-10 -0.78684E-12  0.10094E-11  0.73975E-12 ;
%                        0.21628E-11 -0.15774E-14  0.29426E-16 -0.88718E-17 -0.64209E-17 ;
%                       -0.10440E+04 -0.10440E+04 -0.10440E+04 -0.10440E+04 -0.10440E+04 ;
%                        0.43628E+01  0.38353E+01  0.23789E+01  0.48179E+01  0.13985E+01 ];
% TlimsNASA7_s{s,1} = [300,1000,6000,15000,25000,30000];
% s = s+1;        % N2
% AThermNASA7_s{s,1} = [  0.36748E+01  0.32125E+01  0.31811E+01  0.96377E+01 -0.51681E+01 ;
%                        -0.12081E-02  0.10137E-02  0.89745E-03 -0.25728E-02  0.23337E-02 ;
%                         0.23240E-05 -0.30467E-06 -0.20216E-06  0.33020E-06 -0.12953E-06 ;
%                        -0.63218E-09  0.41091E-10  0.18266E-10 -0.14315E-10  0.27872E-11 ;
%                        -0.22577E-12 -0.20170E-14 -0.50334E-15  0.20333E-15 -0.21360E-16 ;
%                        -0.10430E+04 -0.10430E+04 -0.10430E+04 -0.10430E+04 -0.10430E+04 ;
%                         0.23580E+01  0.43661E+01  0.46264E+01 -0.37587E+02  0.66217E+02  ];
% TlimsNASA7_s{s,1} = [300,1000,6000,15000,25000,30000];
% options.customPoly.ATherm_s = AThermNASA7_s;
% options.customPoly.Tlims_s  = TlimsNASA7_s;

% flow conditions
intel.M_e               = M_e;                  % Mach number at boundary-layer edge            [-]
intel.T_e               = T_e;                  % Temperature at boundary-layer edge            [K]
intel.p_e               = p_e;                  % Pressure at boundary-layer edge               [Pa]
intel.x                 = x;                    % dimensional streamwise location to evaluate   [m]

% Executing DEKAF
for ii=Nm:-1:1
    options.modelThermal = modelThermal_vec{ii};
    options.bLinearInterpThermalPoly = bLinearInterp(ii);
    [intel_all{ii},options_all{ii}] = DEKAF(intel,options);
end

% Saving
saveDEKAF('DKF_modelThermalCompare.mat','intel_all','options_all','modelThermal_vec');

%% plotting
figure;  Ni=1;Nj=3;
styleS = {'-','-.','--'};
yMax = max(cellfun(@(cll)max(cll.y_i),intel_all));
for ii=1:Nm
    ip=0;
    clr = two_colors(ii,Nm+1,'Rbw2');
    if bLinearInterp(ii);   name = [modelThermal_vec{ii},' with coef. linear interp'];
    else;                   name = modelThermal_vec{ii};
    end
ip=ip+1;subplot(Ni,Nj,ip); plot(intel_all{ii}.u,  intel_all{ii}.y,styleS{ii},'Color',clr,'displayname',name); hold on;
                xlabel('u [m/s]');      ylabel('y [m]'); grid minor; ylim([0,yMax]);
ip=ip+1;subplot(Ni,Nj,ip); plot(intel_all{ii}.T,  intel_all{ii}.y,styleS{ii},'Color',clr,'displayname',name); hold on;
                xlabel('T [K]');        ylabel('y [m]'); grid minor; ylim([0,yMax]);
ip=ip+1;subplot(Ni,Nj,ip); plot(intel_all{ii}.h,  intel_all{ii}.y,styleS{ii},'Color',clr,'displayname',name); hold on;
                xlabel('h [J/kg]'); ylabel('y [m]'); grid minor; ylim([0,yMax]);
end
legend();
