% caller_airCmixtures is a script to run the DEKAF solver to perform
% a comparison of the results obtained with different air mixtures
% containing a certain amount of CO2.
%
% Author: Fernando Miro Miro
% Date: November 2018

clear; close all;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
M_inf     = 25;
T_inf     = 269.4;  % [K]
p_inf     = 117.2;  % [Pa]
T_w     = 1900;     % [K]
x_min   = 1e-4;     % [m]
x_max   = 0.5;      % [m]
Nx      = 200;      % number of streamwise points for the BL evaluation
wedge_angle = 10;   % [deg]

thermalBC_vec = {'adiab'        ,'Twall'          ,'Twall'          ,'Twall'   ,'ablation'};
blowingBC_vec = {'none'         ,'none'           ,'ablation'       ,'ablation','ablation'};
YsBC_vec      = {'nonCatalytic' ,'nonCatalytic'   ,'nonCatalytic'   ,'ablation','ablation'};
names = {'adiabatic and no reactions',...
    ['Tw = ',num2str(T_w),' K and no reactions'],...
    ['Tw = ',num2str(T_w),' K, ablation blowing and no reactions'],...
    ['Tw = ',num2str(T_w),' K and ablation'],...
    'full ablation'...
    };

idx_run = [2,4,5];
thermalBC_vec = thermalBC_vec(idx_run);     blowingBC_vec = blowingBC_vec(idx_run);     YsBC_vec = YsBC_vec(idx_run);     names = names(idx_run);

trc = 1e-10;
ys_e_vec = {trc,trc,trc,trc,trc,trc,trc,trc,trc,0.767-8*trc,0.233};

Nbc = length(thermalBC_vec);

%%
for ii=Nbc%:-1:1
    clear options intel;
    % Solver inputs
    options.N                       = 100;                  % Number of points
    options.L                       = 40;                   % Domain size
    options.eta_i                   = 6;                    % Mapping parameter, eta_crit
    options.tol                     = 5e-11;                % Convergence tolerance
    options.it_max                  = 500;                   % maximum number of iterations
    options.T_tol                   = 5e-13;                % convergence tolerance for the NR to get T from the enthalpy
    options.T_itMax                 = 200;                  % maximum number of iterations for the NR to get T from the enthalpy
%     options.it_res_sat = options.it_max;
    options.it_res_sat              = 40;
    options.it_res_sat_diff_val     = 0.001;

    % flow options
    options.thermal_BC              = thermalBC_vec{ii};    % Wall bc on g or dg/deta
    options.G_bc                    = T_w;                  % wall temperature
    options.thermalVib_BC           = 'thermalEquil';    % Wall bc on g or dg/deta
%     options.Gv_bc                   = T_w;                  % wall temperature
    options.wallBlowing_BC          = blowingBC_vec{ii};    % Wall bc on f
    options.wallYs_BC               = YsBC_vec{ii};         % Wall bc on ys
    options.flow                    = 'CNE';                % flow assumption
    options.numberOfTemp            = '1T';                 % number of temperatures
%     options.numberOfTemp            = '2T';                 % number of temperatures
    options.mixture                 = 'airC11Mortensen';    % mixture
    options.modelTransport          = 'BlottnerEucken';     % transport model
    options.referenceVibRelax       = 'Mortensen2013';      % vibrational relaxation model
    options.ys_lim                  = 1e-12;                % minimum mass fraction tolerable
%     options.perturbRelaxation       = 0.5;
    options.perturbRelaxation       = 0;
    options.relaxFactFunction       = @(conv,it,i_xi)relaxFactFunction_table(conv,it,i_xi);
%     options.coordsys                = 'cone';

    % display options
    options.plot_marching = false;
    options.plotRes = false;                        % we don't want the residual plot
    options.MarchingGlobalConv = false;             % nor the marching convergence plot


% options.gifConv = true;
% % options.fields4gif = {'ysh','gh','fh','dfh_deta'};
options.fields4gif = {'tau','df_deta'};
% options.fields4gif = {'tau','tauv','df_deta'};
options.gifLogPlot = true;
% options.fields4gif = {'tau'};
options.gifLogPlot = false;
options.gifLapse = 1;
options.gifStep = 1;

options.gifMarch = true;
% options.fields4gifMarch = {'tau','tauv','df_deta'};
options.fields4gifMarch = {'tau','df_deta'};
options.xlim = [1e-12,10];
options.ylim = [0,10];

    % flow conditions
    intel.M_e0              = M_inf;                % Mach number at boundary-layer edge        [-]
    intel.T_e0              = T_inf;                % temperature at boundary-layer edge        [K]
    intel.Tv_e0             = T_inf;                % vib. temperature at boundary-layer edge   [K]
    intel.p_e0              = p_inf;                % pressure at boundary-layer edge           [Pa]
    intel.ys_e0             = ys_e_vec;             % mass fractions at the boundary-layer edge [-]
    options.inviscidFlowfieldTreatment = 'constant';
    options.plotInviscid    = false;                % we don't want the inviscid plot to appear
    options.shockJump       = true;                 % we are providing preshock conditions
    intel.wedge_angle       = wedge_angle;          % wedge angle [deg]
    intel.cone_angle        = wedge_angle;          % cone angle [deg]

    options.mapOpts.N_x     = Nx;                   % number of streamwise positions
    options.xSpacing        = 'tanh';               % xi spacing
    options.mapOpts.x_start = x_min;                % minimum x to be swept                     [m]
    options.mapOpts.x_end   = x_max;                % maximum x to be swept                     [m]
%     options.mapOpts.deltax_start   = 0.001 * (x_max-x_min) / (Nx+1);                % maximum x to be swept                     [m]
    [intel_all{ii},options_all{ii}] = DEKAF(intel,options,'marching');

end

% save('Ref_data_AblationBC.mat','-v7.3','intel_all','options_all');

%% Reference files
ic=1;
StyleS = {'--','-.','-',':'};
y_lim = options_all{end}.eta_i;
clear plots legends;
all_spec = intel_all{end}.mixCnst.spec_list; N_spec = length(all_spec);
idx_2plt = round(linspace(1,Nx,31)); idx_2plt(1) = [];
x_2plt = intel_all{end}.x(1,idx_2plt);
Nx2plt = length(idx_2plt);
pltFlags = {'Rbw','mgm','vrds','Rbw3'};
[Ni,Nj] = optimalPltSize(2+N_spec);
figure
for ii=length(intel_all)
    for jj=1:Nx2plt

        [~,idx_jj] = min(abs(intel_all{ii}.x(1,:) - x_2plt(jj)));
        clrVec = two_colors(jj,Nx2plt,pltFlags{ii});
        ic = 1;
        name = ['x = ',num2str(intel_all{ii}.x(1,idx_jj)),' m, ',names{ii},' BC'];

        subplot(Ni,Nj,ic); ic=ic+1;
        plot(intel_all{ii}.u(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec,'displayname',name); hold on; legend();
        xlabel('$$u$$ [m/s]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
        ylim([0,y_lim]);

        subplot(Ni,Nj,ic); ic=ic+1;
        plot(intel_all{ii}.T(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
        xlabel('$$T$$ [K]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
        ylim([0,y_lim]);

%         subplot(Ni,Nj,ic); ic=ic+1;
%         plots(Nbc*(jj-1)+ii) = plot(intel_all{ii}.du_dy(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
%         xlabel('$$\partial u/\partial y$$ [1/s]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
%         ylim([0,y_lim]);
%
%         subplot(Ni,Nj,ic); ic=ic+1;
%         plot(intel_all{ii}.dT_dy(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
%         xlabel('$$\partial T / \partial y$$ [K/m]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
%         ylim([0,y_lim]);

        for s=1:N_spec
            subplot(Ni,Nj,ic); ic=ic+1;
            idx_s = find(ismember(intel_all{ii}.mixCnst.spec_list,all_spec{s}));
            if ~isempty(idx_s)
                semilogx(intel_all{ii}.ys{idx_s}(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
            end
            xlabel(['$$Y_{',all_spec{s},'}$$ [m/s]'],'interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
            ylim([0,y_lim]);
        end
    end
end