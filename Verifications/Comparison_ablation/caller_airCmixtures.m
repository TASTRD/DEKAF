% caller_airCmixtures is a script to run the DEKAF solver to perform
% a comparison of the results obtained with different air mixtures
% containing a certain amount of CO2.
%
% Author: Fernando Miro Miro
% Date: November 2018

clear;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'_stable/SourceCode']))

% Flow conditions
M_e     = 25;
T_e     = 250;      % [K]
p_e     = 4000;     % [Pa]
T_w     = 1500;     % [K]
x_min   = 1e-7;     % [m]
x_max   = 0.01;      % [m]
Nx      = 100;      % number of streamwise points for the BL evaluation
wedge_angle = 20;   % [deg]
mixtures = {'air5Park85','airC6Mortensen','airC11Mortensen'};

ys_e_vec =                  {{1e-10,1e-10,1e-10,0.767-3e-10,0.233},...       %                   {N,O,NO,N2,O2}
       {0.1,                  1e-10,1e-10,1e-10,0.667-3e-10,0.233},...       %    {CO2,           N,O,NO,N2,O2}
 {1e-10,0.1,1e-10,1e-10,1e-10,1e-10,1e-10,1e-10,1e-10,0.667-8e-10,0.233}...  % {C3,CO2,C2,CO,CN,C,N,O,NO,N2,O2}
        };

Nf = length(mixtures);

%%
% for ii=Nf:-1:1
for ii=[2,3,1] % 1:Nf
    clear options intel;
    % Solver inputs
    options.N                       = 100;                  % Number of points
    options.L                       = 40;                   % Domain size
    options.eta_i                   = 6;                    % Mapping parameter, eta_crit
    options.tol                     = 5e-11;                % Convergence tolerance
    options.it_max                  = 500;                   % maximum number of iterations
    options.T_tol                   = 5e-13;                % convergence tolerance for the NR to get T from the enthalpy
    options.T_itMax                 = 200;                  % maximum number of iterations for the NR to get T from the enthalpy
%     options.it_res_sat = options.it_max;

    % flow options
    options.thermal_BC              = 'Twall';              % Wall bc (g or dg/deta)
    options.G_bc                    = T_w;                  % wall temperature
    options.flow                    = 'CNE';                % flow assumption
    options.mixture                 = mixtures{ii};          % mixture
    options.modelTransport          = 'CE_122';             % transport model

    % display options
    options.plot_marching = true;
    options.plotRes = false;                        % we don't want the residual plot
    options.MarchingGlobalConv = false;             % nor the marching convergence plot

    % flow conditions
    intel.M_e0              = M_e;                  % Mach number at boundary-layer edge        [-]
    intel.T_e0              = T_e;                  % temperature at boundary-layer edge        [K]
    intel.p_e0              = p_e;                  % pressure at boundary-layer edge           [Pa]
    intel.ys_e0             = ys_e_vec{ii};         % mass fractions at the boundary-layer edge [-]
    options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';
%     options.plotInviscid    = false;                % we don't want the inviscid plot to appear
    options.shockJump       = true;                 % we are providing preshock conditions
    intel.wedge_angle       = wedge_angle;          % wedge angle [deg]

    options.mapOpts.N_x     = Nx;                   % number of streamwise positions
    options.xSpacing        = 'tanh';               % linear xi spacing
    options.mapOpts.x_start = x_min;                % minimum x to be swept                     [m]
    options.mapOpts.x_end   = x_max;                % maximum x to be swept                     [m]
    [intel_all{ii},options_all{ii}] = DEKAF(intel,options,'marching');

end

save('Ref_data_airCmixtures.mat','-v7.3','intel_all','options_all');

%% Reference files
ic=1;
StyleS = {'--','-.','-'};
y_lim = options_all{ii}.eta_i;
clear plots legends;
all_spec = intel_all{end}.mixCnst.spec_list; N_spec = length(all_spec);
idx_2plt = round(linspace(1,Nx,11)); idx_2plt(1) = [];
x_2plt = intel_all{1}.x(1,idx_2plt);
Nx2plt = length(idx_2plt);
% pltFlags = {'Rbw','mgm','vrds'};
pltFlags = {'Rbw','Rbw2','Rbw3'};
[Ni,Nj] = optimalPltSize(4+N_spec);
figure
for ii=Nf:-1:1
    for jj=1:Nx2plt

        [~,idx_jj] = min(abs(intel_all{ii}.x(1,:) - x_2plt(jj)));
        clrVec = two_colors(jj,Nx2plt,pltFlags{ii});
        ic = 1;
        legends{Nf*(jj-1)+ii} = ['x = ',num2str(intel_all{ii}.x(1,idx_jj)),' m, ',mixtures{ii},' BC'];

        subplot(Ni,Nj,ic); ic=ic+1;
        plots(Nf*(jj-1)+ii) = plot(intel_all{ii}.u(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
        xlabel('$$u$$ [m/s]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
        ylim([0,y_lim]);

        subplot(Ni,Nj,ic); ic=ic+1;
        plot(intel_all{ii}.T(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
        xlabel('$$T$$ [K]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
        ylim([0,y_lim]);

        subplot(Ni,Nj,ic); ic=ic+1;
        plots(Nf*(jj-1)+ii) = plot(intel_all{ii}.du_dy(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
        xlabel('$$\partial u/\partial y$$ [1/s]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
        ylim([0,y_lim]);

        subplot(Ni,Nj,ic); ic=ic+1;
        plot(intel_all{ii}.dT_dy(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
        xlabel('$$\partial T / \partial y$$ [K/m]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
        ylim([0,y_lim]);

        for s=1:N_spec
            subplot(Ni,Nj,ic); ic=ic+1;
            idx_s = find(ismember(intel_all{ii}.mixCnst.spec_list,all_spec{s}));
            if ~isempty(idx_s)
                semilogx(intel_all{ii}.ys{idx_s}(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
            end
            xlabel(['$$Y_{',all_spec{s},'}$$ [m/s]'],'interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
            ylim([0,y_lim]);
        end
    end
end
legend(plots,legends);