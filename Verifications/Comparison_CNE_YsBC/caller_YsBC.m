% caller_blowingBCComparison is a script to run the DEKAF solver to perform
% a comparison of the results obtained with the several blowing BCs.
%
% Author: Fernando Miro Miro
% Date: November 2018

clear;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'_stable/SourceCode']))

% Flow conditions
M_e     = 25;
T_e     = 250;      % [K]
p_e     = 4000;     % [Pa]
T_w     = 500;      % [K]
x_min   = 1e-4;     % [m]
x_max   = 0.1;      % [m]
Nx      = 1000;     % number of streamwise points for the BL evaluation
wedge_angle = 10;   % [deg]
mixture = 'air5mutation'; % air-5 mixture (N,O,NO,N2,O2)

BC_vec  = {'nonCatalytic','ysWall','ysWall'};
BC_vecFancy  = {'nonCatalytic','ysWall with y_{s,e}','ysWall with 0.01% N diss. and 10% O diss.'};
ys_e    = {1e-10,1e-10,1e-10,0.767-3e-10,0.233};

N2_diss = 1e-4;
O2_diss = 0.1;
ic = 1;
ys_w{ic} = []; ic=ic+1;
ys_w{ic} = ys_e; ic=ic+1;
ys_w{ic} = {ys_e{1} + N2_diss*ys_e{4}, ...
            ys_e{2} + O2_diss*ys_e{5}, ...
            ys_e{3},...
            (1-N2_diss)*ys_e{4},...
            (1-O2_diss)*ys_e{5}...
            }; % ic=ic+1;

Nf = length(BC_vec);

for ii=Nf:-1:1
% for ii=1:Nf
    clear options intel;
    % Solver inputs
    options.N                       = 100;                  % Number of points
    options.L                       = 40;                   % Domain size
    options.eta_i                   = 6;                    % Mapping parameter, eta_crit
    options.tol                     = 5e-11;                % Convergence tolerance
    options.it_max                  = 500;                   % maximum number of iterations
    options.T_tol                   = 5e-13;                % convergence tolerance for the NR to get T from the enthalpy
    options.T_itMax                 = 200;                  % maximum number of iterations for the NR to get T from the enthalpy
%     options.it_res_sat = options.it_max;

    % flow options
    options.thermal_BC              = 'Twall';              % Wall bc (g or dg/deta)
    options.G_bc                    = T_w;                  % wall temperature
    options.flow                    = 'CNE';                % flow assumption
    options.mixture                 = mixture;              % mixture
    options.modelTransport          = 'CE_122';             % transport model
    options.wallYs_BC               = BC_vec{ii};           % wall-mass-fraction boundary condition
    options.Ys_bc                   = ys_w{ii};             % wall mass fractions

    % display options
    options.plot_marching = true;
    options.plotRes = false;                        % we don't want the residual plot
    options.MarchingGlobalConv = false;             % nor the marching convergence plot

    % flow conditions
    intel.M_e0              = M_e;                  % Mach number at boundary-layer edge        [-]
    intel.T_e0              = T_e;                  % temperature at boundary-layer edge        [K]
    intel.p_e0              = p_e;                  % pressure at boundary-layer edge           [Pa]
    intel.ys_e0             = ys_e;                 % mass fractions at the boundary-layer edge [-]
    options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';
%     options.plotInviscid    = false;                % we don't want the inviscid plot to appear
    options.shockJump       = true;                 % we are providing preshock conditions
    intel.wedge_angle       = wedge_angle;          % wedge angle [deg]

    options.mapOpts.N_x     = Nx;                   % number of streamwise positions
    options.xSpacing        = 'tanh';               % linear xi spacing
    options.mapOpts.x_start = x_min;                % minimum x to be swept                     [m]
    options.mapOpts.x_end   = x_max;                % maximum x to be swept                     [m]
    [intel_all{ii},options_all{ii}] = DEKAF(intel,options,'marching');

end

save('Ref_data_YsBC.mat','-v7.3','intel_all','options_all');

%% Reference files
ic=1;
StyleS = {'-','-.','--'}; y_lim = options_all{ii}.eta_i; clear plots legends;
idx_2plt = round(linspace(1,Nx,11)); idx_2plt(1) = [];
x_2plt = intel_all{1}.x(1,idx_2plt);
Nx2plt = length(idx_2plt);
% pltFlags = {'Rbw','mgm','vrds'};
pltFlags = {'Rbw','Rbw2','Rbw3'};
[Ni,Nj] = optimalPltSize(4+length(ys_e));
figure
for ii=1:Nf
    for jj=1:Nx2plt

        [~,idx_jj] = min(abs(intel_all{ii}.x(1,:) - x_2plt(jj)));
        clrVec = two_colors(jj,Nx2plt,pltFlags{ii});
        ic = 1;
        legends{Nf*(jj-1)+ii} = ['x = ',num2str(intel_all{ii}.x(1,idx_jj)),' m, ',BC_vecFancy{ii},' BC'];

        subplot(Ni,Nj,ic); ic=ic+1;
        plots(Nf*(jj-1)+ii) = plot(intel_all{ii}.u(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
        xlabel('$$u$$ [m/s]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
        ylim([0,y_lim]);

        subplot(Ni,Nj,ic); ic=ic+1;
        plots(Nf*(jj-1)+ii) = plot(intel_all{ii}.du_dy(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
        xlabel('$$\partial u/\partial y$$ [1/s]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
        ylim([0,y_lim]);

        subplot(Ni,Nj,ic); ic=ic+1;
        plot(intel_all{ii}.T(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
        xlabel('$$T$$ [K]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
        ylim([0,y_lim]);

        subplot(Ni,Nj,ic); ic=ic+1;
        plot(intel_all{ii}.dT_dy(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
        xlabel('$$\partial T / \partial y$$ [K/m]','interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
        ylim([0,y_lim]);

        for s=1:intel_all{ii}.mixCnst.N_spec
            subplot(Ni,Nj,ic); ic=ic+1;
            semilogx(intel_all{ii}.ys{s}(:,idx_jj),intel_all{ii}.eta,StyleS{ii},'Color',clrVec); hold on;
            xlabel(['$$Y_{',intel_all{ii}.mixCnst.spec_list{s},'}$$ [m/s]'],'interpreter','latex'); ylabel('$$\eta$$ [-]','interpreter','latex'); box on; grid minor;
            ylim([0,y_lim]);
        end
    end
end
legend(plots,legends);