% Comparison_LTEMuKappa is a script to run the DEKAF solver with different
% transport models in LTE and compare the results
%
% Date: February 2018
%{%
clear;
close all;
%clc
%}

addpath(genpath('/home/miromiro/workspace/DEKAF/'))

modelTransport_list = {'BlottnerEucken','CE_11','CE_12','Brokaw58','Brokaw58','Brokaw58'};
name_list = {'Blottner-Eucken-Wilke','1st-order Chapman-Enskog','1st-2nd-order Chapman-Enskog','Gupta-Yos 90 with Wright2005 data','Gupta-Yos 90','Gupta-Yos 90 with Yos63 data'};
modelCollisionNeutral = {'Wright2005','Wright2005','Wright2005','Wright2005','GuptaYos90','GuptaYos90'};
N_mod = length(modelTransport_list);

M_e = 10;
T_e = 600;
Re1_e = 6.6e6;

for ii=1:N_mod
    clear options intel

    % Optional inputs
    options.N                       = 100;                  % Number of points
    options.L                       = 40;                   % Domain size
    options.eta_i                   = 6;                    % Mapping parameter, eta_crit
    options.thermal_BC              = 'adiab';              % Wall bc
    %options.G_bc                    = 0.1;                  % Value for g boundary condition
    options.flow                    = 'LTE';                % flow assumption
    options.mixture                 = 'air11mutation';       % mixture
    options.modelTransport          = modelTransport_list{ii}; % transport model
    options.modelDiffusion          = 'cstSc';       % diffusion model
    options.modelCollisionNeutral   = modelCollisionNeutral{ii};
    if ii==N_mod
        options.collisionRefExceptions.references = {'Yos63'};
        options.collisionRefExceptions.pairs = {{'O - O','N - N'}};
    end

    options.tol                     = 1e-14;                % Convergence tolerance
    options.it_max                  = 40;
    options.T_tol                   = 1e-13;
    options.T_itMax                 = 50;

    options.plot_transient          = false;                % to plot the inter-iteration profiles
    options.plotRes                 = false;                 % to plot the residual convergence

    options.dimoutput               = true;                % to output fields in dimensional form
    intel.xi                        = 1e-3;                 % xi location where we want the dimensional profiles

    % Flow parameters
    intel.M_e                       = M_e;                   % Mach number at boundary-layer edge                        [-]
    intel.T_e                       = T_e;                  % Static temperature at boundary-layer edge                 [K]
    intel.Re1_e                     = Re1_e;
    intel.Sc_e = 0.71;

    [intel,options] = DEKAF(intel,options);

    all_intels(ii) = intel; % storing
    all_options(ii) = options;
end

%% plotting
pltStyles = {'-','-.','--',':'}; pltStyles = [pltStyles ,pltStyles ,pltStyles];
LineWidths = [1,1,1,1.5]; LineWidths = [LineWidths ,LineWidths ,LineWidths ];
prop2plot = {'u','T'};
units4plot = {' [m/s]',' [K]'};
N_prop = length(prop2plot);
[N_i,N_j] = optimalPltSize(N_prop);

figure;
for jj=1:N_prop
    clear plots;
    subplot(N_i,N_j,jj);
    for ii=1:N_mod
        plots(ii) = plot(all_intels(ii).(prop2plot{jj}),all_intels(ii).y,pltStyles{ii},'LineWidth',LineWidths(ii),'Color',RainbowColors(ii,N_mod)*0.8);
        hold on;
    end
    xlabel([prop2plot{jj},units4plot{jj}]); ylabel('y [m]');
    ylim([0,0.005]);
    grid minor;
end
legend(plots,name_list);

save(['DEKAF_',options.flow,'_muKappa_M',num2str(M_e),'_T',num2str(T_e),'_adiab_',options.mixture,'.mat'],'all_intels','all_options');

