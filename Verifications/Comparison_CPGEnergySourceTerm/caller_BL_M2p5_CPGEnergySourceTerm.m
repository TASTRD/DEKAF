% caller_BL_M2p5_CPGEnergySourceTerm is a script to run the DEKAF marching
% solver for a verification run with the M2p5 case but with the following
% energy source term:
%
%  - 2*xi.*(0.1*(1+5*g).*exp(5*g))
%
% Author: Fernando Miro Miro
% Date: November 2017
%{%
clear
close all;
%clc
%}

addpath(genpath('/home/miromiro/Dropbox/Project_DEKAF/DEKAF'))
clear options intel intel_QSS
% Optional inputs for the QSS run
options.N               = 200;                      % Number of points
options.L               = 40;                      % Domain size
options.eta_i           = 6;                        % Mapping parameter, eta_crit
options.beta            = 0;                        % Hartree parameter (non-dimensional pressure gradient)
options.fw              = 0;                        % Wall-blowing parameter
options.Cooke           = false;                    % Include constant z-component of velocity
options.H_F             = true;                     % If true, set heat flux dg_deta at wall. If false, set temperature g at wall.
options.G_bc            = 0;                        % Value for g boundary condition, corresponding to options.H_F choice.
options.importSolution  = false;                    % Import initial condition profile
options.chebydist       = true;                     % true = chebyshev distribution, false = finite difference
options.tol             = 1e-16;                    % Convergence tolerance
options.it_max          = 100;                       % maximum number of iterations
options.ox              = 4;                        % order of the marching scheme
options.flow            = 'CPG';                % flow assumption
options.quiet           = false;                    % to make all messages be displayed
options.dimoutput       = false;                    % dimensional output
options.plot_transient  = false;                    % to make intermediate plots for the different iterations
options.plotRes         = true;                    % to plot the residual convergence

% Edge conditions
U_e         = 500;
T_e         = 100;
p_e         = 4000;

% Flow parameters for the QSS run
intel.cp    = 1004.5;               % Constant specific heat                                    [J/(kg K)]
intel.gam   = 1.4;                  % Constant specific heat ratio                              [-]
intel.W_0   = 0;                    % Constant z-component of velocity at boundary-layer edge   [m/s]
intel.mu_ref= 1.716e-5;             % Reference viscosity of air, used in Sutherland's law      [m^2/s]
intel.Su_mu = 110.6;                % Sutherland's constant corresponding to viscosity          [K]
intel.T_ref = 273.15;               % Reference temperature of air, used in Sutherland's law    [K]
intel.Pr    = 0.7;                  % Constant Prandtl number of air                            [-]

N_x = 10;
intel.x_e       = linspace(0,0.25,N_x); % expanding everything over x
intel.U_e       = U_e * ones(1,N_x);
intel.T_e       = T_e * ones(1,N_x);
intel.p_e0      = p_e;

% New marching-specific options
options.flow                = 'CPG';                    % flow assumption
intel                       = rmfield(intel,{'p_e'});   % removing pressure, which will now depend on the velocity gradient (bernouilly)
options.tol                 = 1e-14;                     % Convergence tolerance
options.it_max              = 200;                      % maximum number of iterations
%options.H_F                 = false;                    % wall boundary condition
%options.G_bc                = intel_QSS.g(end);
options.mapOpts.x_start     = 1e-5;                     % initial value of the xi marching coordinate
options.mapOpts.x_end       = intel.x_e(end);
options.mapOpts.N_x         = 100;                      % number of xi locations to be used
options.subplot_vertical    = true;                     % we want the subplots vertical so that we can dock it
options.plot_transient      = false;                    % we don't want the transients to be plotted
options.plot_transient      = false;                    % we don't want the transients to be plotted
options.plot_marching       = true;                     % if we want to plot the profiles as we march
options.BLproperties        = false;                    % we don't want the integral properties to be computed
options.Fields4conv         = {'df_deta3','dg_deta2','momXeq','enereq'}; % fields to be analyzed for the convergence of the method
%}

options.dimoutput = true;
[intel,options] = DEKAF(intel,options,'marching');
%%

plot_marching_profiles(intel_dimVars,{'u','T'},50);
%{%
savepath = ['DEKAF_CPG_TsourceTerm_Neta',num2str(options.N),'_Nxi',num2str(options.mapOpts.N_x),'_x',num2str(options.mapOpts.x_start),'to',num2str(options.mapOpts.x_end),'_VerifMarching.mat'];
save(savepath,'-struct','intel_dimVars');
save(savepath,'-append','options');
%}
% end % caller_BL.m