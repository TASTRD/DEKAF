% Comparison_Props_LTEDEKAFvsVESTAvsMutation is a script to test the BL properties
% by comparing them against mutation++'s results as well as those in VESTA's AutoThermoTrans
% module.
%
% Author: Fernando Miro Miro
% Date: January 2018
%{%
% clear;
addpath(genpath('/home/miromiro/workspace/VESTA/'));
addpath(genpath('/home/miromiro/workspace/DEKAF_stable/'));

% loading mutation properties
data = load('mutationTables_T200-10-20000_P400-100-20000_air5_withExtraStuff.dat');
mixture = 'air5mutation';
N_spec = 5; % air 5 or 11
N_reac = length(data(1,11+3*N_spec:end))/2; % numer of reactions is half of the total amount of forward and backward reaction rates combined
reacID = [1,6,15];
N_reacID = length(reacID);
%}%
%% Extracting mutation data for chosen pressure
p_chosen = 10000;
p_mutLong   = data(:,2); ip = (p_mutLong==p_chosen);
p_mutLong   = p_mutLong(ip);
p_mut       = unique(p_mutLong);    N_p = length(p_mut);
T_mutLong   = data(ip,1);
T_mut       = unique(T_mutLong);    N_T = length(T_mut);
ys_mutLong  = data(ip,11:10+N_spec);
ys_mut                  =     reshape(ys_mutLong,                                       [N_T,N_p,N_spec]);
rho_mut                 =     reshape(data(ip,8),                                        [N_T,N_p]);
prop2comp_mut.mu        =     reshape(data(ip,3),                                        [N_T,N_p]);
prop2comp_mut.thermcon  =     reshape(data(ip,4),                                        [N_T,N_p]);
prop2comp_mut.h         =     reshape(data(ip,5),                                        [N_T,N_p]);
prop2comp_mut.Mm        =     reshape(data(ip,9),                                        [N_T,N_p]);
prop2comp_mut.X_s       =     reshape(data(ip,11+N_spec:10+2*N_spec)                  ,  [N_T,N_p,N_spec]);
prop2comp_mut.Dens      = rho_mut;
prop2comp_mut.lnkf_r    = log(reshape(data(ip,11+3*N_spec       :10+3*N_spec+N_reac  ),  [N_T,N_p,N_reac]));
prop2comp_mut.lnkb_r    = log(reshape(data(ip,11+3*N_spec+N_reac:10+3*N_spec+2*N_reac),  [N_T,N_p,N_reac]));

%% Obtaining tabulated values
Sc = 0.5; % Schmidt number
opts.modelEnergyModes = 'mutation';
opts.modelCollisionNeutral = 'GuptaYos90';
opts.modelEquilibrium = 'RRHO';
opts.collisionRefExceptions.references = [];
opts.collisionRefExceptions.pairs = [];
opts.mixture = mixture;
[mixCnst,opts] = getAll_constants(mixture,opts);
mixCnst.XEq = 0.21/0.79; % ratio of equilibrium compositions
mixCnst.LTEtablePath = 'LTETables_air5_N79O21_1000T5-30000_57p1-10000000.mat';
mixCnst.LTEtableData = load(mixCnst.LTEtablePath);

[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,T_mat] = ... % properties as needed for the enthalpy evaluation
                       reshape_inputs4enthalpy(mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,T_mutLong);

%% Obtaining properties from DEKAF
% enthalpy
[h_mutLong,~,~,hs_mutLong,~,~,cvMod_s]  = getMixture_h_cp(ys_mutLong,T_mat,T_mat,T_mat,T_mat,T_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,opts);
% density
rho_mutLong                 = getMixture_rho(ys_mutLong,p_mutLong,T_mutLong,T_mutLong,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron);  % computing mixture density and other properties ...
drhodT_mutLong              = getMixtureDer_drho_dT(p_mutLong,ys_mutLong,mixCnst.R0,mixCnst.Mm_s,T_mutLong);
% reaction rates
T_r = getReaction_T(ones(size(mixCnst.nT_r)),T_mutLong);
lnkfr_mutLong               = getReaction_lnkf(T_r,mixCnst.A_r,mixCnst.nT_r,mixCnst.theta_r); % forward rates
lnKeqr_mutLong              = getReaction_lnKeq(T_r,T_mutLong,T_mutLong,T_mutLong,T_mutLong,T_mutLong,mixCnst.delta_nuEq,mixCnst.Keq_struc,opts);     % equilibrium constants
lnkbr_mutLong               = lnkfr_mutLong(:,reacID) - lnKeqr_mutLong; % backward rates
dlnKeqrdT_mutLong           = getReactionDer_dlnKeqr_dT(ys_mutLong,T_mutLong,mixCnst.delta_nuEq,mixCnst.Keq_struc,opts);
lnKpeqr_mutLong             = getEquilibrium_lnKpeq(lnKeqr_mutLong,mixCnst.R0,T_mutLong,sum(mixCnst.delta_nuEq,1)');
dlnKpeqrdT_mutLong          = getEquilibriumDer_dlnKpeq_dT(dlnKeqrdT_mutLong,T_mutLong,sum(mixCnst.delta_nuEq,1)');
% Mole fraction gradient with temperature
Mm_mutLong                  = getMixture_Mm_R(ys_mutLong,T_mutLong,T_mutLong,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron);
Xs_mutLong                  = getSpecies_X(ys_mutLong,Mm_mutLong,mixCnst.Mm_s);
[dXsdT_mutLong,~]           = getEquilibriumDer_X_air5(1,Xs_mutLong,p_mutLong,mixCnst.XEq,mixCnst.spec_list,mixCnst.reac_list_eq,dlnKpeqrdT_mutLong);
% Transport
[mu_mutLong,mu_s_mutLong]   = getMixture_mu_BlottnerWilke(ys_mutLong,T_mutLong,T_mutLong,mixCnst.Mm_s,mixCnst.Amu_s,mixCnst.Bmu_s,mixCnst.Cmu_s,mixCnst.R0,mixCnst.bElectron);
kappa_mutLong               = getMixture_kappa_EuckenWilke(ys_mutLong,mu_s_mutLong,T_mutLong,T_mutLong,cvMod_s.cvTrans_s,cvMod_s.cvRot_s,cvMod_s.cvVib_s,cvMod_s.cvElec_s,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron);
Ds_mutLong                  = getSpecies_D_cstSchmidt(Sc,mu_mutLong,rho_mutLong,N_spec);
kappa_eq                    = getEquilibrium_kappareac('spec-mix',hs_mutLong,rho_mutLong,Ds_mutLong,dXsdT_mutLong) + kappa_mutLong;

% passing everything to a structure like in mutation
prop2comp_DEKAF.mu          = reshape(mu_mutLong,[N_T,N_p]);
prop2comp_DEKAF.thermcon    = reshape(kappa_eq,[N_T,N_p]);
prop2comp_DEKAF.h           = reshape(h_mutLong,[N_T,N_p]);
prop2comp_DEKAF.Mm          = reshape(Mm_mutLong,[N_T,N_p]);
prop2comp_DEKAF.Dens        = reshape(rho_mutLong,[N_T,N_p]);
prop2comp_DEKAF.X_s         = reshape(Xs_mutLong,[N_T,N_p,N_spec]);
prop2comp_DEKAF.lnkf_r      = reshape(lnkfr_mutLong(:,reacID),   [N_T,N_p,N_reacID]);
prop2comp_DEKAF.lnkb_r      = reshape(lnkbr_mutLong,   [N_T,N_p,N_reacID]);

%% Running AutoThermoTrans
tab_val = mixCnst2tabval(mixCnst);
tab_val.Sc = Sc; % we exceptionally append also the schmidt number
fields_in.T.T = T_mutLong;
for s=1:N_spec
fields_in.(['rho_s',num2str(s)]).(['rho_s',num2str(s)]) = ys_mutLong(:,s) .* rho_mutLong;
end
fields_dep = ThermoTransportProperties(fields_in,tab_val,'BlottnerEucken','cstSc','RRHO','polyLogLog','RRHO',1,false);
[fields_ind,fields_dep] = species2LTE(fields_in,fields_dep,opts.mixture,opts.modelEquilibrium,mixCnst,opts,false); % false for no DDVSpa

% passing everything to a structure like in mutation
prop2comp_AutoThermoTrans.mu                = reshape(fields_dep.mu.mu,[N_T,N_p]);
prop2comp_AutoThermoTrans.thermcon          = reshape(fields_dep.thermcon.thermcon,[N_T,N_p]);
prop2comp_AutoThermoTrans.h                 = reshape(fields_dep.h.h,[N_T,N_p]);
prop2comp_AutoThermoTrans.Mm                = reshape(fields_dep.Mm.Mm,[N_T,N_p]);
prop2comp_AutoThermoTrans.Dens              = reshape(fields_dep.Dens.Dens,[N_T,N_p]);
% for r=1:N_reac
%     prop2comp_AutoThermoTrans.lnkf_r(:,:,r) = reshape(fields_dep.(['lnK_fk',num2str(reacID(r))]).(['lnK_fk',num2str(reacID(r))]),   [N_T,N_p]);
%     prop2comp_AutoThermoTrans.lnkb_r(:,:,r) = reshape(fields_dep.(['lnK_bk',num2str(reacID(r))]).(['lnK_bk',num2str(reacID(r))]),   [N_T,N_p]);
% end
%}

%% plotting
clear plots legends
fields2plot = fieldnames(prop2comp_AutoThermoTrans); % fields to plot
% figure
N_plots = 0; % initializing
LineWidth = 2;
for ii = 1:length(fields2plot)
    N_plots = N_plots + size(prop2comp_AutoThermoTrans.(fields2plot{ii}),3); % for instance, for lnkf_r there are N_reac plots to make
end
[N_i,N_j] = optimalPltSize(N_plots+1);
p_Step = 10;
N_p = 2;
for iP = 1:p_Step:N_p % looping pressure
    it = 0; % initializing
    for ii = 1:length(fields2plot)
        N_ii = size(prop2comp_AutoThermoTrans.(fields2plot{ii}),3); % number of subvariables to be plotted
        if N_ii==1
            it=it+1; % increasing count
            subplot(N_i,N_j,it);
%             figure(it);
            plots(1,it,iP) = plot(T_mut,prop2comp_mut.(fields2plot{ii})(:,iP),             '-','Color',two_colors(iP,N_p,'KR'),'LineWidth',LineWidth);
            legends{1,it,iP} = ['mutation p = ',   num2str(p_mut(iP)),' Pa'];
            hold on;
            plots(2,it,iP) = plot(T_mut,prop2comp_DEKAF.(fields2plot{ii})(:,iP),           '-.','Color',two_colors(iP,N_p,'GB'),'LineWidth',LineWidth);
            legends{2,it,iP} = ['DEKAF p = ',      num2str(p_mut(iP)),' Pa'];
            plots(3,it,iP) = plot(T_mut,prop2comp_AutoThermoTrans.(fields2plot{ii})(:,iP), '--','Color',two_colors(iP,N_p,'YM'),'LineWidth',LineWidth);
            legends{3,it,iP} = ['VESTA p = ',      num2str(p_mut(iP)),' Pa'];
            xlabel('T [K]'); ylabel([fields2plot{ii},'[??]']);
        else
            for jj=1:N_ii
            it=it+1; % increasing count
            subplot(N_i,N_j,it);
%             figure(it);
            plots(1,it,iP) = plot(T_mut,prop2comp_mut.(fields2plot{ii})(:,iP,jj),             '-','Color',two_colors(iP,N_p,'KR'),'LineWidth',LineWidth);
            legends{1,it,iP} = ['mutation p = ',   num2str(p_mut(iP)),' Pa'];
            hold on;
            plots(2,it,iP) = plot(T_mut,prop2comp_DEKAF.(fields2plot{ii})(:,iP,jj),           '-.','Color',two_colors(iP,N_p,'GB'),'LineWidth',LineWidth);
            legends{2,it,iP} = ['DEKAF p = ',      num2str(p_mut(iP)),' Pa'];
            plots(3,it,iP) = plot(T_mut,prop2comp_AutoThermoTrans.(fields2plot{ii})(:,iP,jj), '--','Color',two_colors(iP,N_p,'YM'),'LineWidth',LineWidth);
            legends{3,it,iP} = ['VESTA p = ',      num2str(p_mut(iP)),' Pa'];
            xlabel('T [K]'); ylabel([fields2plot{ii},num2str(jj),' [??]']);
            end
        end
    end
end
subplot(N_i,N_j,N_plots+1)
plot(T_mut,reshape(prop2comp_mut.X_s(:,1,:),[N_T,N_spec]),'r-',T_mut,reshape(prop2comp_DEKAF.X_s(:,1,:),[N_T,N_spec]),'b-.');
xlabel('T [K]'); ylabel('X_s [-]');
for it=1%1:N_plots
    plots4names = plots(:,it,[1,size(plots,3)]);
    legends4names = legends(:,it,[1,size(plots,3)]);
    legend([plots4names(:)],{legends4names{:}});
end
