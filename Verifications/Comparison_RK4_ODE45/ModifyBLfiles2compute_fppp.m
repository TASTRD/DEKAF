% ModifyBLfiles2compute_fppp is a script to change the BL files generated
% with a bugged version of VESTA's BL solver, that was setting f_ppp=0

N_vec = round(logspace(log10(300),log10(4e6),15));
N_vec = {N_vec,2*N_vec-1};
for jj=1:length(N_vec)
    for ii=1:length(N_vec{jj})
        disp(['Running N=',num2str(N_vec{jj}(ii))]);
        pathname_ode45 = ['BL_files/ODE45_BL_N',num2str(N_vec{jj}(ii)),'_M2.5T148.1481p4000gwp0fw0.mat'];
        savename_ode45 = ['BL_files_withfppp/ODE45_BL_N',num2str(N_vec{jj}(ii)),'_M2.5T148.1481p4000gwp0fw0.mat'];
        if ii==1 % initial run to check what fields we actually have
            allfields = load(pathname_ode45);
            allfieldnames = fieldnames(allfields);
        end
        % loading and modifying f_ppp
        load(pathname_ode45);
        c = mu_mu.*rho_rho / (mu_mu(end)*rho_rho(end));
        c_prime = (mu_prime_mu_prime.*rho_rho + mu_mu.*rho_prime_rho_prime) / (mu_mu(end)*rho_rho(end));
        a1 = c./Pr_e;
        a1_prime = c_prime./Pr_e;
        a2 = ((gamma -1)*M_e^2)/(1+((gamma -1)/2) * M_e^2) * (1 - 1./Pr_e).*c;
        a2_prime = ((gamma -1)*M_e^2)/(1+((gamma -1)/2) * M_e^2) * (1 - 1./Pr_e).*c_prime;
        f_ppp = -(1./c).*(c_prime + f).*f_pp;
        g_pp = -1./a1 .* (a1_prime.*g_p + a2_prime.*f_p.*f_pp + a2.*f_pp.^2 + a2.*f_p.*f_ppp + f.*g_p);
        save(savename_ode45,allfieldnames{:},'g_pp');
        if ii>=3
            pathname_rk4 = ['BL_files/VESTA_BL_N',num2str(N_vec{jj}(ii)),'_M2.5T148.1481p4000gwp0fw0.mat'];
            savename_rk4 = ['BL_files_withfppp/RK4_BL_N',num2str(N_vec{jj}(ii)),'_M2.5T148.1481p4000gwp0fw0.mat'];
            % loading and modifying f_ppp
            load(pathname_rk4);
            c = mu_mu.*rho_rho / (mu_mu(end)*rho_rho(end));
            c_prime = (mu_prime_mu_prime.*rho_rho + mu_mu.*rho_prime_rho_prime) / (mu_mu(end)*rho_rho(end));
            a1 = c./Pr_e;
            a1_prime = c_prime./Pr_e;
            a2 = ((gamma -1)*M_e^2)/(1+((gamma -1)/2) * M_e^2) * (1 - 1./Pr_e).*c;
            a2_prime = ((gamma -1)*M_e^2)/(1+((gamma -1)/2) * M_e^2) * (1 - 1./Pr_e).*c_prime;
            f_ppp = -(1./c).*(c_prime + f).*f_pp;
            g_pp = -1./a1 .* (a1_prime.*g_p + a2_prime.*f_p.*f_pp + a2.*f_pp.^2 + a2.*f_p.*f_ppp + f.*g_p);
            save(savename_rk4 ,allfieldnames{:},'g_pp');
        end
    end
end