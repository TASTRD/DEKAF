% Sample launch script to compute the compressible self-similar boundary
% layer profile for a basic case (no blowing or shocks)
%
% It is used for the error convergence study to compare against DEKAF. This
% script runs VESTA's ODE45 solver
%
% Author: Fernando Miro Miro
% Date: October, 2017

clear;

options.path = '../../VESTA/';
addpath(genpath(options.path));
N_vec = round(logspace(2.477121254719663,6.602059991327963,15)); % from 300 to 4M
N_vec = [N_vec,2*N_vec-1];
for i=1:length(N_vec)
options.tol = 1e-15;
options.L = 40;
options.N = N_vec(i);
options.s_new = 1.0;
options.H_F= true;
options.G_bc = 0; % adiabatic flow
options.beta = 0;
options.import_sol = false;
options.fluid = 'air';
options.method = 'ode45';
options.M_e = 2.5;
options.T_e = 333.3333;
options.p_e = 4000;
options.save = true;
options.autoname = true;
options.save_path = ['BL_files_new/ODE45_BL_N',num2str(N_vec(i)),'_'];
compr_profile = CBL_profile (options);
end