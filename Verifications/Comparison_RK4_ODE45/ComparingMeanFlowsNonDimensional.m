% ComparingMeanFlowsNonDimensional is a script to compare the meanflows coming from
% VESTA's RK4 and ODE45 solvers to DEKAF.
%
% It corresponds to the Mode 1 Mach 2.5 case
%
% Author: Fernando Miro Miro
% Date: November, 2017

%{%
%% Defining parameters and paths
clear;
addpath(genpath('/home/miromiro/workspace/VESTA/'));
addpath(genpath('/home/miromiro/Dropbox/Project_DEKAF/DEKAF/'));

savepath = 'data_NonDimensionalMeanflowComparison_M2p5.mat';
%{%
Ni = 300;
Nf = 4*10^6;
N_files = 15;
N = logspace(log10(Ni),log10(Nf),N_files);
N=round(N(3:end-1));                          % Magic number warning...
N_alt = 2*N-1;

% file location and extension
%files_loc = '/home/ebeyak/Desktop/BL_files_new/';
files_loc = '/home/miromiro/workspace/18.DEKAF_aviation2018/1.Convergence_tests_VESTA_RK4/BL_files_withfppp/';

%% Load data
% waitbar
wb_str=sprintf('Importing solutions...');
%h = waitbar(0,wb_str);

% importing DEKAF solutions
DEKAF_path = [files_loc,'DEKAF_BL_N30-200.mat'];
DEKAF_intels = load(DEKAF_path);
N_DEKAF = DEKAF_intels.N_vec{1};

% importing ODE45 & RK4 solutions
for ii = 1:length(N)
    %waitbar((ii-1)/length(N),h,wb_str);

    blODE45{ii}     = load([files_loc,'ODE45_BL_N',num2str(N(ii)),'_M2.5T148.1481p4000gwp0fw0.mat']);                     %#ok<*SAGROW>
    %blODE45_alt{ii} = load([files_loc,'ODE45_BL_N',num2str(N_alt(ii)),'_M2.5T148.1481p4000gwp0fw0.mat']);
    blRK4{ii}       = load([files_loc,'RK4_BL_N',num2str(N(ii)),'_M2.5T148.1481p4000gwp0fw0.mat']);
    %blRK4_alt{ii}   = load([files_loc,'RK4_BL_N',num2str(N_alt(ii)),'_M2.5T148.1481p4000gwp0fw0.mat']);

    blODE45{ii}.C       = blODE45{ii}.rho_rho.*blODE45{ii}.mu_mu/(blODE45{ii}.rho_rho(end).*blODE45{ii}.mu_mu(end));
    %blODE45_alt{ii}.C       = blODE45_alt{ii}.rho_rho.*blODE45_alt{ii}.mu_mu/(blODE45_alt{ii}.rho_rho(end).*blODE45_alt{ii}.mu_mu(end));
    blRK4{ii}.C       = blRK4{ii}.rho_rho.*blRK4{ii}.mu_mu/(blRK4{ii}.rho_rho(end).*blRK4{ii}.mu_mu(end));
    %blRK4_alt{ii}.C       = blRK4_alt{ii}.rho_rho.*blRK4_alt{ii}.mu_mu/(blRK4_alt{ii}.rho_rho(end).*blRK4_alt{ii}.mu_mu(end));

    blODE45{ii}.dC_deta       = (blODE45{ii}.rho_prime_rho_prime.*blODE45{ii}.mu_mu + blODE45{ii}.rho_rho.*blODE45{ii}.mu_prime_mu_prime)/(blODE45{ii}.rho_rho(end).*blODE45{ii}.mu_mu(end));
    %blODE45_alt{ii}.dC_deta       = (blODE45alt{ii}.rho_prime_rho_prime.*blODE45alt{ii}.mu_mu + blODE45alt{ii}.rho_rho.*blODE45alt{ii}.mu_prime_mu_prime)/(blODE45alt{ii}.rho_rho(end).*blODE45alt{ii}.mu_mu(end));
    blRK4{ii}.dC_deta       = (blRK4{ii}.rho_prime_rho_prime.*blRK4{ii}.mu_mu + blRK4{ii}.rho_rho.*blRK4{ii}.mu_prime_mu_prime)/(blRK4{ii}.rho_rho(end).*blRK4{ii}.mu_mu(end));
    %blRK4_alt{ii}.dC_deta       = (blRK4alt{ii}.rho_prime_rho_prime.*blRK4alt{ii}.mu_mu + blRK4alt{ii}.rho_rho.*blRK4alt{ii}.mu_prime_mu_prime)/(blRK4alt{ii}.rho_rho(end).*blRK4alt{ii}.mu_mu(end));

    % if memory is an issue, look up renaming a field ( https://blogs.mathworks.com/loren/2010/05/13/rename-a-field-in-a-structure-array/ )
    blODE45{ii}.eta     = blODE45{ii}.bl_mesh;
    %blODE45_alt{ii}.eta = blODE45_alt{ii}.bl_mesh;
    blRK4{ii}.eta       = blRK4{ii}.bl_mesh;
    %blRK4_alt{ii}.eta   = blRK4_alt{ii}.bl_mesh;

end

%% Calculate that error!
% 0.  ode45 against rk4 (no interpolation - equispaced, same n values :) )
%
% check two interpolations
%
% 1a. rk4 onto cheb (spline)
% 1b. cheb onto rk4 (gicm - need differentiation matrix onto equispaced grid -- fernando is working on this now)
% 2a. ode45 onto cheb (spline)
% 2b. cheb onto ode45 (gicm work to be done)
%
% calcError should be max of two vectors

varFields_VESTA = {'f','f_p','f_pp','f_ppp','g','g_p','g_pp','C','dC_deta'};
varFields_DEKAF = {'f','df_deta','df_deta2','df_deta3','g','dg_deta','dg_deta2','C','dC_deta'};
varNiceNames = {'$$f$$','$$\partial f/\partial \eta$$','$$\partial^2 f/\partial \eta^2$$','$$\partial^3 f/\partial \eta^3$$', ...
    '$$g$$','$$\partial g/\partial \eta$$','$$\partial^2 g/\partial \eta^2$$','$$C$$','$$\partial C/\partial \eta$$'};

% grab finest resolution of each case
% finest_blODE45  = blODE45_alt{end};
% finest_blRK4    = blRK4_alt{end};

% actually we only want to compare against the converged solution of DEKAF,
% to see if they converge towards it
DEKAF_sols = DEKAF_intels.all_intels{1,end}; % we use the 200-point solution
DEKAF_opts = DEKAF_intels.all_options{1,end};
[G,dG_deta,dG_deta2] = giveme_VESTAg(DEKAF_sols); % obtaining the non-dimensional static enthalpy (what VESTA solves for)
DEKAF_sols.g = G;
DEKAF_sols.dg_deta = dG_deta;
DEKAF_sols.dg_deta2 = dG_deta2;
eta = DEKAF_sols.eta;
eta_i = DEKAF_opts.eta_i;
eta_max = DEKAF_opts.L;

list_comparisonSs = {'ode45 -- RK4','DEKAF -- RK4','RK4 -- DEKAF','DEKAF -- ode45','ode45 -- DEKAF'};
clear deltaQ
for ii=1:length(N)
    eta_ODE45 = blODE45{ii}.bl_mesh;
    eta_RK4 = blRK4{ii}.bl_mesh;
    X_ODE45 = kilamMapping(eta_max,eta_i,eta_ODE45);
    X_RK4 = kilamMapping(eta_max,eta_i,eta_RK4);
    for jj=1:length(varFields_VESTA)
        myODE45vec = blODE45{ii}.(varFields_VESTA{jj});
        myRK4vec = blRK4{ii}.(varFields_VESTA{jj})'; % we wanna transpose it
        myDEKAFvec = DEKAF_sols.(varFields_DEKAF{jj});

        disp(['Running N=',num2str(N(ii)),' and for ',varFields_DEKAF{jj},'...']);
        % Case 0 - Compare ODE45 against RK4
        deltaQ.ODE45vsRK4(jj,ii) = norm(myODE45vec-myRK4vec,inf)/max(abs(myODE45vec));
        disp('Done case 0');
        % Case 1a
        % See chebinterp and FDdif
        myRK4onDEKAF = interp1(eta_RK4,myRK4vec,eta,'spline');
        deltaQ.DEKAFvsRK4(jj,ii) = norm(myRK4onDEKAF-myDEKAFvec,inf)/max(abs(myDEKAFvec)); % home team first
        disp('Done case 1a');
        % Case 1b
        myDEKAFonRK4 = chebinterp(myDEKAFvec,X_RK4);
        deltaQ.RK4vsDEKAF(jj,ii) = norm(myDEKAFonRK4-myRK4vec,inf)/max(abs(myRK4vec));
        disp('Done case 1b');
        % Case 2a
        myODE45onDEKAF = interp1(eta_ODE45,myODE45vec,eta,'spline');
        deltaQ.DEKAFvsODE45(jj,ii) = norm(myODE45onDEKAF-myDEKAFvec,inf)/max(abs(myDEKAFvec)); % home team first
        disp('Done case 2a');
        % Case 2b
        myDEKAFonODE45 = chebinterp(myDEKAFvec,X_ODE45);
        deltaQ.ODE45vsDEKAF(jj,ii) = norm(myDEKAFonODE45-myODE45vec,inf)/max(abs(myODE45vec));
        disp('Done case 2b');
    end
end

%% determining which variable has the maximum and minimum error
fields_err = fieldnames(deltaQ);
N_fieldsErr = length(fields_err);
for i=1:N_fieldsErr
    logerr = log10(deltaQ.(fields_err{i}));
    meanlogerr = mean(logerr,2);
    [~,jj_max(i)] = max(meanlogerr);
    [~,jj_min(i)] = min(meanlogerr);
end
%}
list_comparisons = {'RK4 vs ode45','DEKAF vs RK4 (DEKAF grid)','ode45 vs RK4 (RK4 grid)','DEKAF vs ode45 (DEKAF grid)','ode45 vs DEKAF (ode45 grid)'};
save(savepath,'deltaQ','list_comparisons','varFields_VESTA');

%% Plot that data! g and dg_deta profiles for the most resolved cases
%{%
eta_ODE45 = blODE45{end}.bl_mesh;
G_ODE45 = blODE45{end}.g;
dGdeta_ODE45 = blODE45{end}.g_p;
dfdeta3_ODE45 = blODE45{end}.f_ppp;
eta_RK4 = blRK4{end}.bl_mesh;
G_RK4 = blRK4{end}.g;
dGdeta_RK4 = blRK4{end}.g_p;
dfdeta3_RK4 = blRK4{end}.f_ppp;
G_DEKAF = DEKAF_sols.g;
dGdeta_DEKAF = DEKAF_sols.dg_deta;
dfdeta3_DEKAF = DEKAF_sols.df_deta3;
figure
subplot(1,3,1)
plot(G_ODE45,eta_ODE45,'k-',G_RK4,eta_RK4,'r-.',G_DEKAF,eta,'b--');
xlabel('G'); ylabel('\eta');
subplot(1,3,2)
plot(dGdeta_ODE45,eta_ODE45,'k-',dGdeta_RK4,eta_RK4,'r-.',dGdeta_DEKAF,eta,'b--');
xlabel('dGdeta'); ylabel('\eta');
subplot(1,3,3)
plot(dfdeta3_ODE45,eta_ODE45,'k-',dfdeta3_RK4,eta_RK4,'r-.',dfdeta3_DEKAF,eta,'b--');
xlabel('dfdeta3'); ylabel('\eta');
%}
%% Plot that data! (max & min)
%{%
styleS = {'o','x','s','v','h','p','d','*','+','^','>','<'};
styleS = [styleS ,styleS ,styleS ,styleS ];
RGB_vecR = [1 , 1 , 1 ,0.5, 0 , 0 , 0 , 0 , 0 ,0.5, 1 ];
RGB_vecG = [0 ,0.5, 1 , 1 , 1 , 1 , 1 ,0.5, 0 , 0 , 0 ];
RGB_vecB = [0 , 0 , 0 , 0 , 0 ,0.5, 1 , 1 , 1 , 1 , 1 ];
RGB_vecx = linspace(-1e-8,1+1e-8,11);

clear legends plots
figure
for ii = 1:length(fields_err) % we will plot only the best and worse case of each
    Rainbow_vector_it = [interp1(RGB_vecx,RGB_vecR,(ii-1)/(N_fieldsErr-1)),interp1(RGB_vecx,RGB_vecG,(ii-1)/(N_fieldsErr-1)),interp1(RGB_vecx,RGB_vecB,(ii-1)/(N_fieldsErr-1))];
    plots(2*(ii-1)+1) = loglog(N,deltaQ.(fields_err{ii})(jj_max(ii),:),['-',styleS{ii}],'Color',Rainbow_vector_it);
    legends{2*(ii-1)+1} = [list_comparisonSs{ii},', max $$\epsilon$$ - ',varNiceNames{jj_max(ii)}];
    hold on
    %{
    plots(2*(ii-1)+2) = loglog(N,deltaQ.(fields_err{ii})(jj_min(ii),:),['--',styleS{ii}],'Color',Rainbow_vector_it);
    legends{2*(ii-1)+2} = [list_comparisons{ii},', min $$\epsilon$$ - ',varNiceNames{jj_min(ii)}];
    %}
end
%legend(plots,legends,'interpreter','latex');
legend(plots(1:2:end),legends(1:2:end),'interpreter','latex');
xlabel('$$\overline{N}$$','interpreter','latex');
ylabel('$$\epsilon$$','interpreter','latex');
%}
%% Plot that data! (all)
styleS = {'o','x','s','v','h','p','d','*','+','^','>','<'};
styleS = [styleS ,styleS ,styleS ,styleS ];
RGB_vecR = [1 , 1 , 1 ,0.5, 0 , 0 , 0 , 0 , 0 ,0.5, 1 ];
RGB_vecG = [0 ,0.5, 1 , 1 , 1 , 1 , 1 ,0.5, 0 , 0 , 0 ];
RGB_vecB = [0 , 0 , 0 , 0 , 0 ,0.5, 1 , 1 , 1 , 1 , 1 ];
RGB_vecx = linspace(-1e-8,1+1e-8,11);

N_varFields = length(varFields_VESTA);
clear legends plots
figure
for ii = 2:length(fields_err) % we will plot only the best and worse case of each
    for jj = 1:N_varFields
    Rainbow_vector_it = [interp1(RGB_vecx,RGB_vecR,(jj-1)/(N_varFields-1)),interp1(RGB_vecx,RGB_vecG,(jj-1)/(N_varFields-1)),interp1(RGB_vecx,RGB_vecB,(jj-1)/(N_varFields-1))];
    plots((ii-2)*length(varFields_VESTA)+jj) = loglog(N,deltaQ.(fields_err{ii})(jj,:),['-',styleS{ii}],'Color',Rainbow_vector_it);
    hold on
    legends{(ii-2)*length(varFields_VESTA)+jj} = [list_comparisonSs{ii},' - ',varNiceNames{jj}];
    end
end
legend(plots,legends,'interpreter','latex');
xlabel('$$\overline{N}$$','interpreter','latex');
ylabel('$$\epsilon$$','interpreter','latex');