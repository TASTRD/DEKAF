% MeanFlowNonDimSelfConvergence is a script to make a convergence study of
% the meanflows generated with ode45, rk4 and dekaf. It also stores the
% interpolation error commited using the different interpolation techniques
% (linear, spline, etc).
%
% Author: Fernando Miro Miro
% Date: November 2017

clear;
addpath(genpath('/home/miromiro/Dropbox/Project_DEKAF/DEKAF/'));
addpath(genpath('/home/miromiro/workspace/VESTA/'));

% Base path to the different BL files
base_paths_VESTA{1} = 'BL_files_withfppp/RK4_BL_N';
base_paths_VESTA{2} = 'BL_files_withfppp/ODE45_BL_N';
base_path = 'BL_files_withfppp/DEKAF_BL_N30-200.mat';
path_ID = {'RK4','ODE45'};
load(base_path); % the dekaf solutions, since they're all in one file, are simply loaded to the workspace

savepath = 'data_NonDimensionalMeanflowSelfConvergence_M2p5.mat';

N_vec_rk4 = round(logspace(log10(300),log10(4e6),15)); % from 300 to 4M
N_vec_rk4 = N_vec_rk4(3:end); % the RK4 method did not converge for less than 1000 points

fields2compare = {'f','f_p','f_pp','f_ppp','g','g_p','C','C_prime'}; % ,'mu_mu','mu_prime_mu_prime','rho_rho','rho_prime_rho_prime'
names_fields = {'$$f$$','$$\partial f / \partial \eta$$','$$\partial^2 f / \partial \eta^2$$','$$\partial^3 f / \partial \eta^3$$', ...
    '$$g$$','$$\partial g / \partial \eta$$' ,'$$C$$','$$\partial C / \partial \eta$$' }; % ,'$$\mu$$','$$\partial \mu / \partial \eta$$','$$\rho$$','$$\partial \rho / \partial \eta$$'
styles = {'o','s','*','+','d','p','h','^','v','<','>'};
N_fields2compare = length(fields2compare);

for k = 1:length(base_paths_VESTA)
    for i=1:length(N_vec_rk4)
        disp(['Running N=',num2str(N_vec_rk4(i))]);
        filepath_N = [base_paths_VESTA{k},num2str(N_vec_rk4(i)),'_M2.5T148.1481p4000gwp0fw0.mat'];
        flowfield_N = load(filepath_N);
        filepath_2Np1 = [base_paths_VESTA{k},num2str(2*N_vec_rk4(i)-1),'_M2.5T148.1481p4000gwp0fw0.mat'];
        flowfield_2Np1 = load(filepath_2Np1);

        flowfield_N.C = flowfield_N.rho_rho .* flowfield_N.mu_mu;
        flowfield_N.C_prime = flowfield_N.rho_prime_rho_prime .* flowfield_N.mu_mu + flowfield_N.rho_rho .* flowfield_N.mu_prime_mu_prime;

        flowfield_2Np1.C = flowfield_2Np1.rho_rho .* flowfield_2Np1.mu_mu;
        flowfield_2Np1.C_prime = flowfield_2Np1.rho_prime_rho_prime .* flowfield_2Np1.mu_mu + flowfield_2Np1.rho_rho .* flowfield_2Np1.mu_prime_mu_prime;

        for j = 1:N_fields2compare
            err_base.(path_ID{k}).(fields2compare{j})(i) = norm(flowfield_N.(fields2compare{j}) - flowfield_2Np1.(fields2compare{j})(1:2:end),inf)/max(abs(flowfield_2Np1.(fields2compare{j}))); % we take only positions 1, 3, 5, etc, which coincide with 1, 2, 3 etc from the first grid

            flowfield_intLinear = interp1(flowfield_N.bl_mesh,flowfield_N.(fields2compare{j}),flowfield_2Np1.bl_mesh,'linear');
            err_linear.(path_ID{k}).(fields2compare{j})(i) = norm(flowfield_intLinear - flowfield_2Np1.(fields2compare{j}),inf) / max(abs(flowfield_2Np1.(fields2compare{j})));

            flowfield_intSpline = interp1(flowfield_N.bl_mesh,flowfield_N.(fields2compare{j}),flowfield_2Np1.bl_mesh,'spline');
            err_spline.(path_ID{k}).(fields2compare{j})(i) = norm(flowfield_intSpline - flowfield_2Np1.(fields2compare{j}),inf) / max(abs(flowfield_2Np1.(fields2compare{j})));
        end
    end
end


%% DEKAF analysis
fields2compareDEKAF = {'f','df_deta','df_deta2','df_deta3','g','dg_deta','C','dC_deta'};
for i=1:length(N_vec{1})
    disp(['Running N=',num2str(N_vec{1}(i))]);
    for j = 1:N_fields2compare
        err_base.DEKAF.(fields2compareDEKAF{j})(i) = norm(all_intels{1,i}.(fields2compareDEKAF{j}) - all_intels{2,i}.(fields2compareDEKAF{j})(1:2:end),inf)/max(abs(all_intels{2,i}.(fields2compareDEKAF{j}))); % we take only positions 1, 3, 5, etc, which coincide with 1, 2, 3 etc from the first grid

        flowfield_intLinear = interp1(all_intels{1,i}.eta,all_intels{1,i}.(fields2compareDEKAF{j}),all_intels{2,i}.eta,'linear');
        err_linear.DEKAF.(fields2compareDEKAF{j})(i) = norm(flowfield_intLinear - all_intels{2,i}.(fields2compareDEKAF{j}),inf)/max(abs(all_intels{2,i}.(fields2compareDEKAF{j})));

        flowfield_intSpline = interp1(all_intels{1,i}.eta,all_intels{1,i}.(fields2compareDEKAF{j}),all_intels{2,i}.eta,'spline'); % we interpolate the coarse mesh onto the fine one
        err_spline.DEKAF.(fields2compareDEKAF{j})(i) = norm(flowfield_intSpline - all_intels{2,i}.(fields2compareDEKAF{j}),inf)/max(abs(all_intels{2,i}.(fields2compareDEKAF{j})));

        [X_fine,~] = chebDif(N_vec{2}(i),1); % obtaining chebyshev computational grid for the fine mesh (on which we have to interpolate)
        flowfield_intCheby = chebinterp(all_intels{1,i}.(fields2compareDEKAF{j}),X_fine);
        err_chebinterp.DEKAF.(fields2compareDEKAF{j})(i) = norm(flowfield_intCheby - all_intels{2,i}.(fields2compareDEKAF{j}),inf)/max(abs(all_intels{2,i}.(fields2compareDEKAF{j})));
    end
end

%% Saving
save(savepath ,'err_base','err_linear','err_spline','err_chebinterp');

%% plotting

% vectors for the rainbow plot
RGB_vecR = [1 , 1 , 1 ,0.5, 0 , 0 , 0 , 0 , 0 ,0.5, 1 ];
RGB_vecG = [0 ,0.5, 1 , 1 , 1 , 1 , 1 ,0.5, 0 , 0 , 0 ];
RGB_vecB = [0 , 0 , 0 , 0 , 0 ,0.5, 1 , 1 , 1 , 1 , 1 ];
RGB_vecx = linspace(-1e-8,1+1e-8,11);

% DEKAF plot
clear plots
figure
for j=1:N_fields2compare
    Rainbow_vector = [interp1(RGB_vecx,RGB_vecR,(j-1)/(N_fields2compare-1)),interp1(RGB_vecx,RGB_vecG,(j-1)/(N_fields2compare-1)),interp1(RGB_vecx,RGB_vecB,(j-1)/(N_fields2compare-1))];
    plots(j) = semilogy(N_vec{1},err_base.DEKAF.(fields2compareDEKAF{j}),[styles{j},'-'],'Color',Rainbow_vector);
    hold on
    semilogy(N_vec{1},err_spline.DEKAF.(fields2compareDEKAF{j}),[styles{j},'-.'],'Color',Rainbow_vector);
    semilogy(N_vec{1},err_linear.DEKAF.(fields2compareDEKAF{j}),[styles{j},'--'],'Color',Rainbow_vector);
    semilogy(N_vec{1},err_chebinterp.DEKAF.(fields2compareDEKAF{j}),[styles{j},':'],'Color',Rainbow_vector);
end
legend(plots,names_fields,'interpreter','latex');
xlabel('$$N$$','interpreter','latex');
ylabel('$$|\epsilon|$$','interpreter','latex');
%grid on;
%title('ode45');
%title('VESTA''s RK4');
%title('DEKAF');
ylim([1e-16,1])
xlim([30,200])

% RK4 plot
clear plots
figure
for j=1:N_fields2compare
    Rainbow_vector = [interp1(RGB_vecx,RGB_vecR,(j-1)/(N_fields2compare-1)),interp1(RGB_vecx,RGB_vecG,(j-1)/(N_fields2compare-1)),interp1(RGB_vecx,RGB_vecB,(j-1)/(N_fields2compare-1))];
    plots(j) = loglog(N_vec_rk4,err_base.RK4.(fields2compare{j}),[styles{j},'-'],'Color',Rainbow_vector);
    hold on
    loglog(N_vec_rk4,err_spline.RK4.(fields2compare{j}),[styles{j},'-.'],'Color',Rainbow_vector);
    loglog(N_vec_rk4,err_linear.RK4.(fields2compare{j}),[styles{j},'--'],'Color',Rainbow_vector);
end
legend(plots,names_fields,'interpreter','latex');
xlabel('$$N$$','interpreter','latex');
ylabel('$$|\epsilon|$$','interpreter','latex');
%grid on;
%title('ode45');
%title('VESTA''s RK4');
%title('DEKAF');
ylim([1e-16,1])

% ODE45 plot
clear plots
figure
for j=1:N_fields2compare
    Rainbow_vector = [interp1(RGB_vecx,RGB_vecR,(j-1)/(N_fields2compare-1)),interp1(RGB_vecx,RGB_vecG,(j-1)/(N_fields2compare-1)),interp1(RGB_vecx,RGB_vecB,(j-1)/(N_fields2compare-1))];
    plots(j) = loglog(N_vec_rk4,err_base.ODE45.(fields2compare{j}),[styles{j},'-'],'Color',Rainbow_vector);
    hold on
    loglog(N_vec_rk4,err_spline.ODE45.(fields2compare{j}),[styles{j},'-.'],'Color',Rainbow_vector);
    loglog(N_vec_rk4,err_linear.ODE45.(fields2compare{j}),[styles{j},'--'],'Color',Rainbow_vector);
end
legend(plots,names_fields,'interpreter','latex');
xlabel('$$N$$','interpreter','latex');
ylabel('$$|\epsilon|$$','interpreter','latex');
%grid on;
%title('ode45');
%title('VESTA''s RK4');
%title('DEKAF');
ylim([1e-16,1])