% DEKAF_caller is a script to run the DEKAF solver
%
% Date: October 2017
%{
clear all
close all;
%}

addpath(genpath('./DEKAF'))
addpath(genpath('/home/miromiro/workspace/VESTA'))
addpath('./mat_files')
%savepath = 'miromiro_LTEStuff/LTE_fast_M5_noyMatch.mat';
savepath = '/home/miromiro/workspace/18.DEKAF_aviation2018/1.Convergence_tests_VESTA_RK4/BL_files/DEKAF_BL_N30-200.mat';
N_vec = {30:10:200,2*(30:10:200)-1};

clear options intel
for i=1:length(N_vec)
for j=1:length(N_vec{i})
    clear options intel
    N = N_vec{i}(j);
    % Optional inputs
    options.N       = N;              % Number of points
    options.L       = 40;               % Domain size
    options.eta_i   = 6;                % Mapping parameter, eta_crit
    options.beta    = 0;                % Hartree parameter (non-dimensional pressure gradient)
    options.fw      = 0;                % Wall-blowing parameter
    options.Cooke   = false;             % Include constant z-component of velocity
    %options.sweep   = 45;               % Set sweep angle [deg]
    options.H_F     = true;             % If true, set heat flux dg_deta at wall. If false, set temperature g at wall.
    options.G_bc    = 0;                % Value for g boundary condition, corresponding to options.H_F choice.
    options.importSolution  = false;    % Import initial condition profile
    options.chebydist = true;          % true = chebyshev distribution, false = finite difference
    %options.tol     = 10^-11;           % Convergence tolerance
    % options.FileName= 'mat_files/BL_solutions/ParallelBlasiusBLfile.mat';
    % options.tol = 7e-13;

    options.flow            = 'CPG'; % CPG, LTE
    options.mutationFast    = true;
    options.display         = true;
    options.iter = 1;
    options.plot_transient  = false;

    options.dimensionalinoutput = true;

    % Flow parameters
    intel.M_e    = 2.5;                  % Mach number at boundary-layer edge                        [-]
    intel.T_e    = 600/(1.8*(1+0.2*2.5^2));% Static temperature at boundary-layer edge               [K]
    intel.p_e    = 4000;                 % Absolute static pressure at boundary-layer edge           [Pa]
    intel.cp     = 1004.5;               % Constant specific heat                                    [J/(kg K)]
    intel.gam    = 1.4;                  % Constant specific heat ratio                              [-]
    % Constant z-component of velocity at boundary-layer edge   [m/s]
    %intel.W_0    = sind(options.sweep) * intel.M_e*sqrt(intel.cp*(intel.gam-1)*intel.T_e);
    options.mu_ref = 1.716e-5;             % Reference viscosity of air, used in Sutherland's law      [m^2/s]
    options.Su_mu  = 110.6;                % Sutherland's constant corresponding to viscosity          [K]
    options.T_ref  = 273.15;               % Reference temperature of air, used in Sutherland's law    [K]
    options.Pr   = 0.7;                  % Constant Prandtl number of air                            [-]
    %%
    options.tol = 1e-17;

    options.dimoutput       = true;
    options.ynodesrequested = false;
    mu_e = get_prop(intel.T_e,'air',{'mu'}); % getting the dynamic viscosity from VESTA's get_prop
    Re = 3000;
    intel.xi = (Re*mu_e.mu)^2;

    %options.y_i   = 0.07;
    %options.y_max = 0.5;

    intel = DEKAF(intel,options);
    all_intels{i,j} = intel;
    all_options{i,j} = options;
end
end
save(savepath,'all_intels','all_options','N_vec');


%% obtaining VESTA profiles
%{
options.LTE = true;
addpath(genpath('../../workspace/VESTA/'));
VESTA_file = '/home/miromiro/Dropbox/Project_DEKAF/LTE_VESTAref/bl_LTE_M10T278p4079.4584gwp0fw0.mat';
x = intel.xi/(intel.U_e*intel.rho_e*intel.mu_e);
[mom,enr,eta,~,Pr,rho_e,mu_e,~,~,T_e,U_e,rho,mu,fluid,gamma,fw,p_e,h_e,properties] = import_flow(VESTA_file,options,'mutation_T50_5000_P200_8000.dat');

Re = sqrt(U_e*x*rho_e/mu_e);  % We fix a Reynolds number in order to obtain the corresponding
% dimensional profile Note that it is the Reynolds with respect
% to the blasius length, which is equal to Re_x^(1/2).
options.dim = true; % we want the profiles in dimensional form
[U_dim,T_dim,~,~,~,~,~,y_dim,~,l_sc,P_dim,~,~,~,~,~,~] = profile_transform(mom,enr,eta,mu_e,rho_e,T_e,U_e,Re,rho,Pr,fluid,options,p_e,h_e,properties);
V_dim.v = zeros(size(U_dim.u));

%%
figure
subplot(2,3,1)
plot(intel.u,intel.y,'bo-',U_dim.u,y_dim,'r --');
xlabel('$$u \,$$ [m/s]','interpreter','latex');
ylabel('$$y \,$$ [m]','interpreter','latex');

subplot(2,3,2)
plot(intel.v,intel.y,'bo-',V_dim.v,y_dim,'r --');
xlabel('$$v \,$$ [m/s]','interpreter','latex');
ylabel('$$y \,$$ [m]','interpreter','latex');
legend('DEKAF','VESTA');

subplot(2,3,3)
plot(intel.w,intel.y,'bo-',V_dim.v,y_dim,'r --');
xlabel('$$w \,$$ [m/s]','interpreter','latex');
ylabel('$$y \,$$ [m]','interpreter','latex');

subplot(2,3,4)
plot(intel.T,intel.y,'bo-',T_dim.T,y_dim,'r --');
xlabel('$$T \,$$ [K]','interpreter','latex');
ylabel('$$y \,$$ [m]','interpreter','latex');

subplot(2,3,5)
plot(intel.rho,intel.y,'bo-',rho,y_dim,'r --');
xlabel('$$\rho \,$$ [kg/m\textsuperscript{3}]','interpreter','latex');
ylabel('$$y \,$$ [m]','interpreter','latex');

subplot(2,3,6)
plot(intel.mu,intel.y,'bo-',mu,y_dim,'r --');
xlabel('$$\mu \,$$ [kg/(m\, s)]','interpreter','latex');
ylabel('$$y \,$$ [m]','interpreter','latex');
%}
%%
% end % DEKAF_caller.m