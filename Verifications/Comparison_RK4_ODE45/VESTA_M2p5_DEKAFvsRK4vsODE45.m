% This file is to compare stability spectra with mean-flows coming from
% DEKAF, and VESTA's RK4 and ODE45 BL solvers.
%
% This is for a first case - adiabatic flat plate at mach 2.5 at T_e =
% 333.3333K
%
% Author: Fernando Miro Miro
% Date: 2/11/2017
%{%
%% Defining parameters and paths
clear;
addpath(genpath('/home/miromiro/workspace/VESTA/'));
addpath(genpath('/home/miromiro/Dropbox/Project_DEKAF/DEKAF/'));

DEKAF_path = 'BL_files_withfppp/DEKAF_BL_N30-200.mat';
DEKAF_intels = load(DEKAF_path);
N_vec_DEKAF = DEKAF_intels.N_vec{1};
N_N_vec_DEKAF = length(N_vec_DEKAF);
DEKAF_opts_ref = DEKAF_intels.all_options{2,end}; % we keep the most resolved solution as a reference
DEKAF_intels_ref = DEKAF_intels.all_intels{2,end};
N_ref_DEKAF = DEKAF_intels.N_vec{2}(end);
DEKAF_opts = DEKAF_intels.all_options(1,:);
DEKAF_intels = DEKAF_intels.all_intels(1,:); % getting rid of the 3N-1 points

N_vec_ODE45 = round(logspace(2.477121254719663,6.602059991327963,15)); % number of points considered in each case
N_N_vec_ODE45 = length(N_vec_ODE45);
%{%
for i=1:N_N_vec_ODE45
    ODE45_paths{i} = ['BL_files_withfppp/ODE45_BL_N',num2str(N_vec_ODE45(i)),'_M2.5T148.1481p4000gwp0fw0.mat'];
end
ODE45_refpath = 'BL_files_withfppp/ODE45_BL_N7999999_M2.5T148.1481p4000gwp0fw0.mat';

N_vec_RK4 = N_vec_ODE45(3:end);  % RK4 didn't converge with less than 1000 points
N_N_vec_RK4 = length(N_vec_RK4);
for i=1:N_N_vec_RK4
    RK4_paths{i} = ['BL_files_withfppp/RK4_BL_N',num2str(N_vec_RK4(i)),'_M2.5T148.1481p4000gwp0fw0.mat'];
end
RK4_refpath = 'BL_files_withfppp/RK4_BL_N7999999_M2.5T148.1481p4000gwp0fw0.mat';

%}
filename_save_Freqsweep = 'data_StabilityConvergenceAnalysis_M2p5.mat'; % save filename

% Constant parameters for all cases
beta = 0.1;
alpha = 0.06;
Re = 3000;
analysis = 'temporal';
flow = 'compressibleT';
coordsys = 'cartesian3d';
wall_var = 'y';
extra_info.solver = '';
extra_info.BC = '';

options.BCnames = {'momentum2'};
%options.BCnames = {'continuity'}; % we use the continuity eq. as the compatibility condition (same as EPIC)
options.nonDirichBCeq = {[1],[1]};
options.nonDirichBCloc = {'y0','y1'};
options.nonDirichBCvar = {{'p'},{'p'}};

% plotting styles
styleS = {'o','x','s','v','p','h','d','*','+','^','>','<'};
styleS = [styleS ,styleS ,styleS ,styleS ];
RGB_vecR = [1 , 1 , 1 ,0.5, 0 , 0 , 0 , 0 , 0 ,0.5, 1 ];
RGB_vecG = [0 ,0.5, 1 , 1 , 1 , 1 , 1 ,0.5, 0 , 0 , 0 ];
RGB_vecB = [0 , 0 , 0 , 0 , 0 ,0.5, 1 , 1 , 1 , 1 , 1 ];
RGB_vecx = linspace(-1e-8,1+1e-8,11);


%% DEKAF Stability analysis (running stability with the same number of points that DEKAF calculated the profile with)
%{
fields2interpGICM = {'u','du_dy','du_dy2','v','w','dw_dy','dw_dy2','T','dT_dy','dT_dy2','p'};
options.GICMsuffix = ''; % we want the variables to keep the same name as they originally had
clear plots legends
specID = figure; % plot for the whole spectrum
convID = figure; % plot for the growthrate convergence
eigval_DEKAF = zeros(size(N_vec_DEKAF)); % allocating
replot = true; % initialising
for it=1:N_N_vec_DEKAF
    N = N_vec_DEKAF(it);
    U_e = DEKAF_intels{it}.U_e;
    xi = DEKAF_intels{it}.xi;
    eta_max = DEKAF_opts{it}.L;
    eta_i = DEKAF_opts{it}.eta_i;
    fields_postGICM = GICM_interp(eta_i,eta_max,N,fields2interpGICM,U_e,xi,DEKAF_intels{it},options); % we don't input a grid, because we want it on the outputted grid
    % converting to the structure form used by the multisolver
    [fields4stab,parameters4stab,mapping] = fields_DEKAF2VESTA(fields_postGICM,flow);
    % adding missing parameters
    mapping.N = N;
    parameters4stab.beta = beta;
    parameters4stab.alpha = alpha;
    wall = fields4stab.y.y;
    %{%
    tic
    [eigval,eigvec,y_init,mat_info] = LST_Multisolver_Global(wall,fields4stab,parameters4stab,flow,analysis,coordsys,wall_var,mapping,extra_info,options);
    toc
    epsilon_mat(it) = eps*mat_info.frob; % storing the precision of the eigenvalue solver for every run
    all_eigval{it} = eigval;
    %}%
    % plotting etc.
    figure(specID);
    Rainbow_vector = [interp1(RGB_vecx,RGB_vecR,(it-1)/(N_N_vec_DEKAF-1)),interp1(RGB_vecx,RGB_vecG,(it-1)/(N_N_vec_DEKAF-1)),interp1(RGB_vecx,RGB_vecB,(it-1)/(N_N_vec_DEKAF-1))];
    plots(it) = plot(real(diag(eigval)),imag(diag(eigval)),styleS{it},'Color',Rainbow_vector);
    legends{it} = ['N = ',num2str(N_vec_DEKAF(it))];
    legend(plots,legends);
    hold on; daspect([1,1,1]);
    xlim([-0.1,0.5]); ylim([-0.5,0.5]);

    if it==1 || replot % first search for the eigenvalue
        tic
        [S_plot,yvec,V_plot] = spectral_plot_Multi(eigval,eigvec,y_init,analysis,N,{'U','V','W','T','p'},{'U','V','W','T','p'},2);
        toc
        reply = input('Which eigenvalue number do you want to select? ');
        replot = input('For the next N, should we replot the spectrum? (true/false) ');
        eigval_DEKAF(it) = eigval(reply,reply); % storing eigenvalue
        eigval_ref = eigval(reply,reply); % used to locate posterior eigenvalues
    else % we already know more or less where the eigenvalue should be
        [~,pos] = min(abs(eigval_ref-diag(eigval)));
        eigval_DEKAF(it) = eigval(pos,pos);
        eigval_ref = eigval(pos,pos);
        figure(convID)
        eigval_diff = abs(imag(eigval_DEKAF(1:it-1)-eigval_DEKAF(it)));
        semilogy(N_vec_DEKAF(1:it-1),eigval_diff,'k-', ...
                 N_vec_DEKAF(1:it),epsilon_mat,'r-');
        legend('|\omega_i^{j+1}-\omega_i^j|','\epsilon_{eig}');
        ylabel('$$\epsilon_{\alpha_i}$$','interpreter','latex'); xlabel('$$N$$','interpreter','latex');
    end
    pause(1);
end

% saving
save(filename_save_Freqsweep,'N_vec_DEKAF','eigval_DEKAF');
%}

%% DEKAF Stability analysis (running stability with every number of points for each DEKAF mean flow)
%{%
% INitial reference case
fields2interpGICM = {'u','du_dy','du_dy2','v','w','dw_dy','dw_dy2','T','dT_dy','dT_dy2','p'};
options.GICMsuffix = ''; % we want the variables to keep the same name as they originally had
U_e = DEKAF_intels_ref.U_e; % freestream temperature
xi = DEKAF_intels_ref.xi; % streamwise generalized coordinate
y1 = DEKAF_intels_ref.y; % dimensional y for the meanflow
y_i = DEKAF_intels_ref.y_i; % dimensional y_i for the meanflow
eta_max = DEKAF_opts_ref.L; % maximum value of the self-similar coordinate eta
eta_i = DEKAF_opts_ref.eta_i; % middle value of the self-similar coordinate eta
l_sc = DEKAF_intels_ref.llBlasius;
N = 200;
[~,DM] = chebDif(N,1); % obtaining chebyshev derivation matrix
D1=DM(:,:,1);
map_dim.y_max = y1(1); % preparing malik mapping for the stability grid
map_dim.yi = y_i; %6*l_sc;
map_dim.N = N;
map_dim.met = 'MAL';
[y2,f1,~,~]=Define_Mapping_Fun(map_dim,false); % the false is because we don't want messages from the mapping func.
y2 = y2';
D_y2=diag(1./f1)*D1; % derivation and integration matrices for the stability grid
I_y2 = integral_mat(D_y2,'du');
[fields_postGICM,~] = GICM_interp(eta_i,eta_max,N_ref_DEKAF,fields2interpGICM,U_e,xi,DEKAF_intels_ref,y2,I_y2,options); % we don't input a grid, because we want it on the outputted grid
% converting to the structure form used by the multisolver
[fields4stab,parameters4stab] = fields_DEKAF2VESTA(fields_postGICM,flow);
% adding missing parameters
wall = fields4stab.y.y;
mapping.N = N;
mapping.yi = y_i/l_sc; %6;
mapping.y_max = max(wall);
mapping.met = 'MAL';
parameters4stab.beta = beta;
parameters4stab.alpha = alpha;
%{%
tic
[eigval,eigvec,y_init,~] = LST_Multisolver_Global(wall,fields4stab,parameters4stab,flow,analysis,coordsys,wall_var,mapping,extra_info,options);
toc
tic
[S_plot,yvec,V_plot] = spectral_plot_Multi(eigval,eigvec,y_init,analysis,N,{'U','V','W','T','p'},{'U','V','W','T','p'},2);
toc
reply = input('Which eigenvalue number do you want to select? ');
eigval_ref_DEKAF = eigval(reply,reply); % used as a reference to calculate discrepancies

% Starting the parametric convergence analysis
convID = figure; % plot for the growthrate convergence
GICMID = figure; % plot of the error of the GICM interpolation
clear plotss legendss plotsS legendsS
eigval_DEKAF_N2 = zeros(N_N_vec_DEKAF,N_N_vec_DEKAF); % allocating
GICM_err = zeros(N_N_vec_DEKAF,N_N_vec_DEKAF);
epsilon_mat_DEKAF = zeros(N_N_vec_DEKAF,N_N_vec_DEKAF);
all_eigval_DEKAF{N_N_vec_DEKAF,N_N_vec_DEKAF} = [];
for it=1:N_N_vec_DEKAF % mean flow number of points
    %specID = figure; % plot for the whole spectrum at every mean-flow N
    clear plots legends % cleaning plot options
    Rainbow_vector_it = [interp1(RGB_vecx,RGB_vecR,(it-1)/(N_N_vec_DEKAF-1)),interp1(RGB_vecx,RGB_vecG,(it-1)/(N_N_vec_DEKAF-1)),interp1(RGB_vecx,RGB_vecB,(it-1)/(N_N_vec_DEKAF-1))];
    % preparing meanflow to be interpolated
    N_meanflow = N_vec_DEKAF(it);
    U_e = DEKAF_intels{it}.U_e; % freestream temperature
    xi = DEKAF_intels{it}.xi; % streamwise generalized coordinate
    y1 = DEKAF_intels{it}.y; % dimensional y for the meanflow
    y_i = DEKAF_intels{it}.y_i; % dimensional y_i for the meanflow
    eta_max = DEKAF_opts{it}.L; % maximum value of the self-similar coordinate eta
    eta_i = DEKAF_opts{it}.eta_i; % middle value of the self-similar coordinate eta
    for jt = 1:N_N_vec_DEKAF % stability mesh size number of points
        N = N_vec_DEKAF(jt);
        [~,DM] = chebDif(N,1); % obtaining chebyshev derivation matrix
        D1=DM(:,:,1);
        map_dim.y_max = y1(1); % preparing malik mapping for the stability grid
        map_dim.yi = y_i; %6*l_sc;
        map_dim.N = N_vec_DEKAF(jt);
        map_dim.met = 'MAL';
        [y2,f1,~,~]=Define_Mapping_Fun(map_dim,false); % the false is because we don't want messages from the mapping func.
        y2 = y2';
        D_y2=diag(1./f1)*D1; % derivation and integration matrices for the stability grid
        I_y2 = integral_mat(D_y2,'du');
        [fields_postGICM,~,GICM_err(it,jt)] = GICM_interp(eta_i,eta_max,N_meanflow,fields2interpGICM,U_e,xi,DEKAF_intels{it},y2,I_y2,options); % we don't input a grid, because we want it on the outputted grid

        % converting to the structure form used by the multisolver
        [fields4stab,parameters4stab] = fields_DEKAF2VESTA(fields_postGICM,flow);
        % adding missing parameters
        wall = fields4stab.y.y;
        mapping.N = N;
        mapping.yi = y_i/l_sc; %6;
        mapping.y_max = max(wall);
        mapping.met = 'MAL';
        parameters4stab.beta = beta;
        parameters4stab.alpha = alpha;
        %{%
        tic
        [eigval,~,~,mat_info] = LST_Multisolver_Global(wall,fields4stab,parameters4stab,flow,analysis,coordsys,wall_var,mapping,extra_info,options);
        toc
        all_eigval_DEKAF{it,jt} = diag(eigval); % storing the eigenvalues for all combinations of mean-flow and stability grid sizes
        epsilon_mat_DEKAF(it,jt) = eps*mat_info.frob; % storing the precision of the eigenvalue solver for every run
        %}%
        [~,pos] = min(abs(eigval_ref_DEKAF-diag(eigval)));
        eigval_DEKAF_N2(it,jt) = eigval(pos,pos);
        disp('-----------------------------------------------------------------');
        disp(['-----------------------MEAN FLOW ',num2str(it),'/',num2str(N_N_vec_DEKAF),'--------------------------']);
        disp(['-----------------------STABILITY ',num2str(jt),'/',num2str(N_N_vec_DEKAF),'--------------------------']);
        disp('-----------------------------------------------------------------');
    end

    % GICM error plot
    %{
    figure(GICMID)
    GICM_err(GICM_err<eps) = eps;
    plotsS(it) = semilogy(N_vec_DEKAF,GICM_err(it,:),'-','Color',Rainbow_vector_it);
    hold on;
    legendsS{it} = ['Meanflow $$N = ',num2str(N_meanflow),'$$'];
    legend(plotsS,legendsS,'interpreter','latex');
    ylabel('$$\epsilon_{GICM}$$','interpreter','latex'); xlabel('$$N$$','interpreter','latex');
    %}
    % eigenvalue convergence error
    figure(convID)
    eigval_diff(it,:) = abs(imag(eigval_DEKAF_N2(it,:)-eigval_ref_DEKAF));
    plotss(it) = semilogy(N_vec_DEKAF,eigval_diff(it,:),'-','Color',Rainbow_vector_it);
    hold on;
    semilogy(N_vec_DEKAF,epsilon_mat_DEKAF(it,:),'--','Color',Rainbow_vector_it);
    legendss{it} = ['Meanflow $$N = ',num2str(N_meanflow),'$$'];
    legend(plotss,legendss,'interpreter','latex');
    ylabel('$$|\omega_i^{j+1}-\omega_i^j|$$','interpreter','latex'); xlabel('$$N$$','interpreter','latex');
    %}
end
pause(0.5);

% saving
%save(filename_save_Freqsweep,'N_vec_DEKAF','eigval_DEKAF_N2','all_eigval_DEKAF','epsilon_mat_DEKAF','eigval_ref_DEKAF');
%}
%load(filename_save_Freqsweep);

%% RK4 Stability analysis (running stability with every number of points for each RK4 mean flow)
%{%
% First we find our reference solution - N = 200 and maximum mean-flow
% resolution
N = 200;
[mom,enr,eta,M_e,Pr_e,rho_e,mu_e,ni_e,cp,T_e,U_e,rho,~,fluid,gamma_] = import_flow(RK4_refpath);
[U_adim,T_adim,W_adim,mu_adim,lambda_adim,k_adim,delta_1,y_adim,Pr_e,l_sc,P_adim] = profile_transform(mom,enr,eta,mu_e,rho_e,T_e,U_e,Re,rho,Pr_e,fluid,options);
wall = y_adim;
fields4stab.y.y = y_adim;
fields4stab.U.U = U_adim.u; fields4stab.U.y = U_adim.y; fields4stab.U.yy = U_adim.yy;
fields4stab.V.V = W_adim.w; % it's zero anyway
fields4stab.W.W = W_adim.w; fields4stab.W.y = W_adim.y; fields4stab.W.yy = W_adim.yy;
fields4stab.T.T = T_adim.T; fields4stab.T.y = T_adim.y; fields4stab.T.yy = T_adim.yy;
fields4stab.p.p = P_adim.p; fields4stab.p.y = P_adim.y;
fields4stab.mu.mu = mu_adim.mu; fields4stab.mu.T = mu_adim.T; fields4stab.mu.TT = mu_adim.TT;
fields4stab.lam.lam = lambda_adim.lambda; fields4stab.lam.T = lambda_adim.T; fields4stab.lam.TT = lambda_adim.TT;
fields4stab.thermcon.thermcon = k_adim.k; fields4stab.thermcon.T = k_adim.T; fields4stab.thermcon.TT = k_adim.TT;
parameters4stab.Re = Re;
parameters4stab.M = M_e;
parameters4stab.Pr = Pr_e;
parameters4stab.gamma_ = gamma_;
parameters4stab.beta = beta;
parameters4stab.alpha = alpha;
mapping.yi = DEKAF_intels{end}.y_i/l_sc;
mapping.y_max = max(wall);
mapping.met = 'MAL';
mapping.N = N;
eigval_ref_RK4 = eigval_ref_DEKAF;
%{
tic
[eigval,eigvec,y_init,~] = LST_Multisolver_Global(wall,fields4stab,parameters4stab,flow,analysis,coordsys,wall_var,mapping,extra_info,options);
toc
%[~,pos_ref] = min(diag(eigval)-eigval_ref_DEKAF);
%eigval_ref_RK4 = eigval(pos_ref,pos_ref);
%{%
tic
[S_plot,yvec,V_plot] = spectral_plot_Multi(eigval,eigvec,y_init,analysis,N,{'U','V','W','T','p'},{'U','V','W','T','p'},2);
toc
reply = input('Which eigenvalue number do you want to select? ');
eigval_ref_RK4 = eigval(reply,reply);
%}
%{%
% Starting the iterative study
convID = figure; % plot for the growthrate convergence
clear plotss legendss plotsS legendsS
eigval_RK4_N2 = zeros(N_N_vec_RK4,N_N_vec_DEKAF); % allocating
epsilon_mat_RK4 = zeros(N_N_vec_RK4,N_N_vec_DEKAF);
all_eigval_RK4{N_N_vec_RK4,N_N_vec_DEKAF} = [];
for it=1:N_N_vec_RK4 % mean flow number of points
    clear plots legends % cleaning plot options
    Rainbow_vector_it = [interp1(RGB_vecx,RGB_vecR,(it-1)/(N_N_vec_RK4-1)),interp1(RGB_vecx,RGB_vecG,(it-1)/(N_N_vec_RK4-1)),interp1(RGB_vecx,RGB_vecB,(it-1)/(N_N_vec_RK4-1))];
    % preparing meanflow to be interpolated
    N_meanflow = N_vec_RK4(it);

    % Exporting and preparing for stability
    [mom,enr,eta,M_e,Pr_e,rho_e,mu_e,ni_e,cp,T_e,U_e,rho,~,fluid,gamma_] = import_flow(RK4_paths{it});
    [U_adim,T_adim,W_adim,mu_adim,lambda_adim,k_adim,delta_1,y_adim,Pr_e,l_sc,P_adim] = profile_transform(mom,enr,eta,mu_e,rho_e,T_e,U_e,Re,rho,Pr_e,fluid,options);
    wall = y_adim;
    fields4stab.y.y = y_adim;
    fields4stab.U.U = U_adim.u; fields4stab.U.y = U_adim.y; fields4stab.U.yy = U_adim.yy;
    fields4stab.V.V = W_adim.w; % it's zero anyway
    fields4stab.W.W = W_adim.w; fields4stab.W.y = W_adim.y; fields4stab.W.yy = W_adim.yy;
    fields4stab.T.T = T_adim.T; fields4stab.T.y = T_adim.y; fields4stab.T.yy = T_adim.yy;
    fields4stab.p.p = P_adim.p; fields4stab.p.y = P_adim.y;
    fields4stab.mu.mu = mu_adim.mu; fields4stab.mu.T = mu_adim.T; fields4stab.mu.TT = mu_adim.TT;
    fields4stab.lam.lam = lambda_adim.lambda; fields4stab.lam.T = lambda_adim.T; fields4stab.lam.TT = lambda_adim.TT;
    fields4stab.thermcon.thermcon = k_adim.k; fields4stab.thermcon.T = k_adim.T; fields4stab.thermcon.TT = k_adim.TT;
    parameters4stab.Re = Re;
    parameters4stab.M = M_e;
    parameters4stab.Pr = Pr_e;
    parameters4stab.gamma_ = gamma_;
    parameters4stab.beta = beta;
    parameters4stab.alpha = alpha;
    mapping.yi = DEKAF_intels{end}.y_i/l_sc;
    mapping.y_max = max(wall);
    mapping.met = 'MAL';

    for jt = 1:N_N_vec_DEKAF % stability mesh size number of points
        N = N_vec_DEKAF(jt);
        %{%
        mapping.N = N;
        tic
        [eigval,~,~,mat_info] = LST_Multisolver_Global(wall,fields4stab,parameters4stab,flow,analysis,coordsys,wall_var,mapping,extra_info,options);
        toc
        all_eigval_RK4{it,jt} = diag(eigval); % storing the eigenvalues for all combinations of mean-flow and stability grid sizes
        epsilon_mat_RK4(it,jt) = eps*mat_info.frob; % storing the precision of the eigenvalue solver for every run
        %}%
        [~,pos] = min(abs(eigval_ref_RK4-diag(eigval)));
        eigval_RK4_N2(it,jt) = eigval(pos,pos);
        disp('-----------------------------------------------------------------');
        disp(['-----------------------MEAN FLOW ',num2str(it),'/',num2str(N_N_vec_RK4),'--------------------------']);
        disp(['-----------------------STABILITY ',num2str(jt),'/',num2str(N_N_vec_DEKAF),'--------------------------']);
        disp('-----------------------------------------------------------------');
        %pause(0.5);
    end

    % eigenvalue convergence error
    %{%
    figure(convID)
    eigval_diff(it,:) = abs(imag(eigval_RK4_N2(it,:)-eigval_ref_RK4));
    plotss(it) = semilogy(N_vec_DEKAF,eigval_diff(it,:),'-','Color',Rainbow_vector_it);
    hold on;
    semilogy(N_vec_DEKAF,epsilon_mat_RK4(it,:),'--','Color',Rainbow_vector_it);
    legendss{it} = ['Meanflow $$N = ',num2str(N_meanflow),'$$'];
    legend(plotss,legendss,'interpreter','latex');
    ylabel('$$|\omega_i^{j+1}-\omega_i^j|$$','interpreter','latex'); xlabel('$$N$$','interpreter','latex');
    %}%
    if it==6
        pause(0.5);
    end
end
pause(0.5);
%% saving
%save(filename_save_Freqsweep,'N_vec_DEKAF','N_vec_RK4','eigval_DEKAF_N2','eigval_RK4_N2','all_eigval_DEKAF','all_eigval_RK4','epsilon_mat_DEKAF','epsilon_mat_RK4','eigval_ref_DEKAF','eigval_ref_RK4');


%% ODE45 Stability analysis (running stability with every number of points for each ODE45 mean flow)
%{%
% First we find our reference solution - N = 200 and maximum mean-flow
% resolution
N = 200;
[mom,enr,eta,M_e,Pr_e,rho_e,mu_e,ni_e,cp,T_e,U_e,rho,~,fluid,gamma_] = import_flow(ODE45_refpath);
[U_adim,T_adim,W_adim,mu_adim,lambda_adim,k_adim,delta_1,y_adim,Pr_e,l_sc,P_adim] = profile_transform(mom,enr,eta,mu_e,rho_e,T_e,U_e,Re,rho,Pr_e,fluid,options);
wall = y_adim;
fields4stab.y.y = y_adim;
fields4stab.U.U = U_adim.u; fields4stab.U.y = U_adim.y; fields4stab.U.yy = U_adim.yy;
fields4stab.V.V = W_adim.w; % it's zero anyway
fields4stab.W.W = W_adim.w; fields4stab.W.y = W_adim.y; fields4stab.W.yy = W_adim.yy;
fields4stab.T.T = T_adim.T; fields4stab.T.y = T_adim.y; fields4stab.T.yy = T_adim.yy;
fields4stab.p.p = P_adim.p; fields4stab.p.y = P_adim.y;
fields4stab.mu.mu = mu_adim.mu; fields4stab.mu.T = mu_adim.T; fields4stab.mu.TT = mu_adim.TT;
fields4stab.lam.lam = lambda_adim.lambda; fields4stab.lam.T = lambda_adim.T; fields4stab.lam.TT = lambda_adim.TT;
fields4stab.thermcon.thermcon = k_adim.k; fields4stab.thermcon.T = k_adim.T; fields4stab.thermcon.TT = k_adim.TT;
parameters4stab.Re = Re;
parameters4stab.M = M_e;
parameters4stab.Pr = Pr_e;
parameters4stab.gamma_ = gamma_;
parameters4stab.beta = beta;
parameters4stab.alpha = alpha;
mapping.yi = DEKAF_intels_ref.y_i/l_sc;
mapping.y_max = max(wall);
mapping.met = 'MAL';
mapping.N = N;
eigval_ref_ODE45 = eigval_ref_DEKAF;
%{
tic
[eigval,eigvec,y_init,mat_info] = LST_Multisolver_Global(wall,fields4stab,parameters4stab,flow,analysis,coordsys,wall_var,mapping,extra_info,options);
toc
[~,pos_ref] = min(diag(eigval)-eigval_ref_DEKAF);
eigval_ref_ODE45 = eigval(pos_ref,pos_ref);
%{%
tic
[S_plot,yvec,V_plot] = spectral_plot_Multi(eigval,eigvec,y_init,analysis,N,{'U','V','W','T','p'},{'U','V','W','T','p'},2);
toc
reply = input('Which eigenvalue number do you want to select? ');
eigval_ref_ODE45 = eigval(reply,reply);
%}

% Starting the iterative study
convID = figure; % plot for the growthrate convergence
clear plotss legendss plotsS legendsS
eigval_ODE45_N2 = zeros(N_N_vec_ODE45,N_N_vec_DEKAF); % allocating
epsilon_mat_ODE45 = zeros(N_N_vec_ODE45,N_N_vec_DEKAF);
all_eigval_ODE45{N_N_vec_ODE45,N_N_vec_DEKAF} = [];
for it=1:N_N_vec_ODE45 % mean flow number of points
    clear plots legends % cleaning plot options
    Rainbow_vector_it = [interp1(RGB_vecx,RGB_vecR,(it-1)/(N_N_vec_ODE45-1)),interp1(RGB_vecx,RGB_vecG,(it-1)/(N_N_vec_ODE45-1)),interp1(RGB_vecx,RGB_vecB,(it-1)/(N_N_vec_ODE45-1))];
    % preparing meanflow to be interpolated
    N_meanflow = N_vec_ODE45(it);

    % Exporting and preparing for stability
    [mom,enr,eta,M_e,Pr_e,rho_e,mu_e,ni_e,cp,T_e,U_e,rho,~,fluid,gamma_] = import_flow(ODE45_paths{it});
    [U_adim,T_adim,W_adim,mu_adim,lambda_adim,k_adim,delta_1,y_adim,Pr_e,l_sc,P_adim] = profile_transform(mom,enr,eta,mu_e,rho_e,T_e,U_e,Re,rho,Pr_e,fluid,options);
    wall = y_adim;
    fields4stab.y.y = y_adim;
    fields4stab.U.U = U_adim.u; fields4stab.U.y = U_adim.y; fields4stab.U.yy = U_adim.yy;
    fields4stab.V.V = W_adim.w; % it's zero anyway
    fields4stab.W.W = W_adim.w; fields4stab.W.y = W_adim.y; fields4stab.W.yy = W_adim.yy;
    fields4stab.T.T = T_adim.T; fields4stab.T.y = T_adim.y; fields4stab.T.yy = T_adim.yy;
    fields4stab.p.p = P_adim.p; fields4stab.p.y = P_adim.y;
    fields4stab.mu.mu = mu_adim.mu; fields4stab.mu.T = mu_adim.T; fields4stab.mu.TT = mu_adim.TT;
    fields4stab.lam.lam = lambda_adim.lambda; fields4stab.lam.T = lambda_adim.T; fields4stab.lam.TT = lambda_adim.TT;
    fields4stab.thermcon.thermcon = k_adim.k; fields4stab.thermcon.T = k_adim.T; fields4stab.thermcon.TT = k_adim.TT;
    parameters4stab.Re = Re;
    parameters4stab.M = M_e;
    parameters4stab.Pr = Pr_e;
    parameters4stab.gamma_ = gamma_;
    parameters4stab.beta = beta;
    parameters4stab.alpha = alpha;
    mapping.yi = DEKAF_intels{end}.y_i/l_sc;
    mapping.y_max = max(wall);
    mapping.met = 'MAL';

    for jt = 1:N_N_vec_DEKAF % stability mesh size number of points
        N = N_vec_DEKAF(jt);
        %{%
        mapping.N = N;
        tic
        [eigval,~,~,mat_info] = LST_Multisolver_Global(wall,fields4stab,parameters4stab,flow,analysis,coordsys,wall_var,mapping,extra_info,options);
        toc
        all_eigval_ODE45{it,jt} = diag(eigval); % storing the eigenvalues for all combinations of mean-flow and stability grid sizes
        epsilon_mat_ODE45(it,jt) = eps*mat_info.frob; % storing the precision of the eigenvalue solver for every run
        %}%
        [~,pos] = min(abs(eigval_ref_ODE45-diag(eigval)));
        eigval_ODE45_N2(it,jt) = eigval(pos,pos);
        disp('-----------------------------------------------------------------');
        disp(['-----------------------MEAN FLOW ',num2str(it),'/',num2str(N_N_vec_ODE45),'--------------------------']);
        disp(['-----------------------STABILITY ',num2str(jt),'/',num2str(N_N_vec_DEKAF),'--------------------------']);
        disp('-----------------------------------------------------------------');
        %pause(0.5);
    end

    % eigenvalue convergence error
    figure(convID)
    eigval_diff(it,:) = abs(imag(eigval_ODE45_N2(it,:)-eigval_ref_ODE45));
    plotss(it) = semilogy(N_vec_DEKAF,eigval_diff(it,:),'-','Color',Rainbow_vector_it);
    hold on;
    semilogy(N_vec_DEKAF,epsilon_mat_ODE45(it,:),'--','Color',Rainbow_vector_it);
    legendss{it} = ['Meanflow $$N = ',num2str(N_meanflow),'$$'];
    legend(plotss,legendss,'interpreter','latex');
    ylabel('$$|\omega_i^{j+1}-\omega_i^j|$$','interpreter','latex'); xlabel('$$N$$','interpreter','latex');
    if it==6 || it==12
        pause(0.5);
    end
end
pause(0.5);

%% saving
save(filename_save_Freqsweep,'N_vec_DEKAF','N_vec_RK4','N_vec_ODE45','eigval_DEKAF_N2','eigval_RK4_N2','eigval_ODE45_N2','all_eigval_DEKAF','all_eigval_RK4','all_eigval_ODE45','epsilon_mat_DEKAF','epsilon_mat_RK4','epsilon_mat_ODE45','eigval_ref_DEKAF','eigval_ref_RK4','eigval_ref_ODE45');
%}