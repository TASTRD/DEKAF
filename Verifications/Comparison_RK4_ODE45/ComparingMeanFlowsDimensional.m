% ComparingMeanFlowsDimensional is a script to compare the meanflows coming from
% VESTA's RK4 and ODE45 solvers to DEKAF.
%
% It corresponds to the Mode 1 Mach 2.5 case
%
% Author: Fernando Miro Miro
% Date: November, 2017

%{%
%% Defining parameters and paths
clear;
addpath(genpath('/home/miromiro/workspace/VESTA/'));
addpath(genpath('/home/miromiro/Dropbox/Project_DEKAF/DEKAF/'));

savepath = 'data_DimensionalMeanflowComparison_M2p5.mat';

DEKAF_path = 'BL_files_withfppp/DEKAF_BL_N30-200.mat';
DEKAF_intels = load(DEKAF_path);
DEKAF_opts_ref = DEKAF_intels.all_options{1,end}; % for DEKAF we keep the most resolved solution only
DEKAF_intels_ref = DEKAF_intels.all_intels{1,end};
N_DEKAF = DEKAF_opts_ref.N;

%{%
N_vec = round(logspace(log10(300),log10(4e6),15)); % number of points considered in each case
N_vec = N_vec(3:end);
N_N_vec= length(N_vec);
for i=1:N_N_vec
    %ODE45_paths{i} = ['BL_files_withfppp/ODE45_BL_N',num2str(N_vec(i)),'_M2.5T148.1481p4000gwp0fw0.mat'];
    ODE45_paths{i} = ['BL_files_withfppp/ODE45_BL_N',num2str(N_vec(i)),'_M2.5T148.1481p4000gwp0fw0.mat'];
end
%ODE45_refpath = 'BL_files_withfppp/ODE45_BL_N7999999_M2.5T148.1481p4000gwp0fw0.mat';

for i=1:N_N_vec
    %RK4_paths{i} = ['BL_files_withfppp/RK4_BL_N',num2str(N_vec(i)),'_M2.5T148.1481p4000gwp0fw0.mat'];
    RK4_paths{i} = ['BL_files_withfppp/RK4_BL_N',num2str(N_vec(i)),'_M2.5T148.1481p4000gwp0fw0.mat'];
end
%}%
%RK4_refpath = 'BL_files_withfppp/RK4_BL_N7999999_M2.5T148.1481p4000gwp0fw0.mat';

% Constant parameters for all cases
beta = 0.1;
alpha = 0.06;
Re = 3000;
flow = 'compressibleT';
fields2plot = {'U','T','mu','thermcon'};
labels2plot = {'u','T','\mu','k'};

%% extracting DEKAF profiles
% INitial reference case
fields2interpGICM = {'u','du_dy','du_dy2','v','w','dw_dy','dw_dy2','T','dT_dy','dT_dy2','p'};
options.GICMsuffix = ''; % we want the variables to keep the same name as they originally had
U_e = DEKAF_intels_ref.U_e; % freestream temperature
xi = DEKAF_intels_ref.xi; % streamwise generalized coordinate
y1 = DEKAF_intels_ref.y; % dimensional y for the meanflow
%y_i = DEKAF_intels_ref.y_i; % dimensional y_i for the meanflow
eta_max = DEKAF_opts_ref.L; % maximum value of the self-similar coordinate eta
eta_i = DEKAF_opts_ref.eta_i; % middle value of the self-similar coordinate eta
l_sc = DEKAF_intels_ref.llBlasius;
N = 200;
[~,DM] = chebDif(N,1); % obtaining chebyshev derivation matrix
D1=DM(:,:,1);
map_dim.y_max = y1(1); % preparing malik mapping for the stability grid
map_dim.yi = 6*l_sc;
map_dim.N = N;
map_dim.met = 'MAL';
[y2,f1,~,~]=Define_Mapping_Fun(map_dim,false); % the false is because we don't want messages from the mapping func.
y2 = y2';
D_y2=diag(1./f1)*D1; % derivation and integration matrices for the stability grid
I_y2 = integral_mat(D_y2,'du');
[fields_postGICM,~] = GICM_interp(eta_i,eta_max,N_DEKAF,fields2interpGICM,U_e,xi,DEKAF_intels_ref,y2,I_y2,options); % we don't input a grid, because we want it on the outputted grid
% converting to the structure form used by the multisolver
[fieldsDEKAF,parametersDEKAF] = fields_DEKAF2VESTA(fields_postGICM,flow);

%% extracting RK4 profiles
for ii = 1:N_N_vec
    disp(['Running N=',num2str(N_vec(ii)),'...'])
    [mom,enr,eta,M_e,Pr_e,rho_e,mu_e,ni_e,cp,T_e,U_e,rho,~,fluid,gamma_] = import_flow(RK4_paths{ii});
    [U_adim,T_adim,W_adim,mu_adim,lambda_adim,k_adim,delta_1,y_adim,Pr_e,l_sc,P_adim] = profile_transform(mom,enr,eta,mu_e,rho_e,T_e,U_e,Re,rho,Pr_e,fluid,options);
    wall = y_adim;
    fieldsRK4.y.y = y_adim;
    fieldsRK4.U.U = U_adim.u; fieldsRK4.U.y = U_adim.y; fieldsRK4.U.yy = U_adim.yy;
    fieldsRK4.V.V = W_adim.w; % it's zero anyway
    fieldsRK4.W.W = W_adim.w; fieldsRK4.W.y = W_adim.y; fieldsRK4.W.yy = W_adim.yy;
    fieldsRK4.T.T = T_adim.T; fieldsRK4.T.y = T_adim.y; fieldsRK4.T.yy = T_adim.yy;
    fieldsRK4.p.p = P_adim.p; fieldsRK4.p.y = P_adim.y;
    fieldsRK4.mu.mu = mu_adim.mu; fieldsRK4.mu.T = mu_adim.T; fieldsRK4.mu.TT = mu_adim.TT;
    fieldsRK4.lam.lam = lambda_adim.lambda; fieldsRK4.lam.T = lambda_adim.T; fieldsRK4.lam.TT = lambda_adim.TT;
    fieldsRK4.thermcon.thermcon = k_adim.k; fieldsRK4.thermcon.T = k_adim.T; fieldsRK4.thermcon.TT = k_adim.TT;
    parametersRK4.Re = Re; parametersRK4.M = M_e; parametersRK4.Pr = Pr_e;
    parametersRK4.gamma_ = gamma_; parametersRK4.beta = beta; parametersRK4.alpha = alpha;
    mappingRK4.yi = 6; % DEKAF_intels{end}.y_i/l_sc;
    mappingRK4.y_max = max(wall); mappingRK4.met = 'MAL';
    % interpolating onto the computational grid
    fieldsRK4_interp = Interpolate_all_fields_1D(fieldsRK4,fieldsDEKAF.y.y,wall);

    [mom,enr,eta,M_e,Pr_e,rho_e,mu_e,ni_e,cp,T_e,U_e,rho,~,fluid,gamma_] = import_flow(ODE45_paths{ii});
    [U_adim,T_adim,W_adim,mu_adim,lambda_adim,k_adim,delta_1,y_adim,Pr_e,l_sc,P_adim] = profile_transform(mom,enr,eta,mu_e,rho_e,T_e,U_e,Re,rho,Pr_e,fluid,options);
    wall = y_adim;
    fieldsODE45.y.y = y_adim;
    fieldsODE45.U.U = U_adim.u; fieldsODE45.U.y = U_adim.y; fieldsODE45.U.yy = U_adim.yy;
    fieldsODE45.V.V = W_adim.w; % it's zero anyway
    fieldsODE45.W.W = W_adim.w; fieldsODE45.W.y = W_adim.y; fieldsODE45.W.yy = W_adim.yy;
    fieldsODE45.T.T = T_adim.T; fieldsODE45.T.y = T_adim.y; fieldsODE45.T.yy = T_adim.yy;
    fieldsODE45.p.p = P_adim.p; fieldsODE45.p.y = P_adim.y;
    fieldsODE45.mu.mu = mu_adim.mu; fieldsODE45.mu.T = mu_adim.T; fieldsODE45.mu.TT = mu_adim.TT;
    fieldsODE45.lam.lam = lambda_adim.lambda; fieldsODE45.lam.T = lambda_adim.T; fieldsODE45.lam.TT = lambda_adim.TT;
    fieldsODE45.thermcon.thermcon = k_adim.k; fieldsODE45.thermcon.T = k_adim.T; fieldsODE45.thermcon.TT = k_adim.TT;
    parametersODE45.Re = Re; parametersODE45.M = M_e; parametersODE45.Pr = Pr_e;
    parametersODE45.gamma_ = gamma_; parametersODE45.beta = beta; parametersODE45.alpha = alpha;
    mappingODE45.yi = 6; % DEKAF_intels{end}.y_i/l_sc;
    mappingODE45.y_max = max(wall); mappingODE45.met = 'MAL';
    % interpolating onto the computational grid
    fieldsODE45_interp = Interpolate_all_fields_1D(fieldsODE45,fieldsDEKAF.y.y,wall);

    % computing differences
    %fields2plot = fieldnames(fieldsODE45_interp);
    N_fields = length(fields2plot);
    jj = 0; % initializing
    for j1=1:N_fields
        subfields = fieldnames(fieldsODE45_interp.(fields2plot{j1}));
        N_subfields = length(subfields);
        for j2 = 1:N_subfields
            jj = jj+1;
            diff_struc.RK4vsODE45.(fields2plot{j1}).(subfields{j2})(ii) = norm(fieldsRK4_interp.(fields2plot{j1}).(subfields{j2})-fieldsODE45_interp.(fields2plot{j1}).(subfields{j2}),inf) / max(abs(fieldsODE45_interp.(fields2plot{j1}).(subfields{j2})));
            diff_struc.DEKAFvsRK4.(fields2plot{j1}).(subfields{j2})(ii) = norm(fieldsDEKAF.(fields2plot{j1}).(subfields{j2})-fieldsRK4_interp.(fields2plot{j1}).(subfields{j2}),inf) / max(abs(fieldsDEKAF.(fields2plot{j1}).(subfields{j2})));
            diff_struc.ODE45vsDEKAF.(fields2plot{j1}).(subfields{j2})(ii) = norm(fieldsODE45_interp.(fields2plot{j1}).(subfields{j2})-fieldsDEKAF.(fields2plot{j1}).(subfields{j2}),inf) / max(abs(fieldsDEKAF.(fields2plot{j1}).(subfields{j2})));
        end
    end
end
N_fieldsTot = jj;
%}
%% Plotting individual profile
%{
figure;
subplot(2,2,1);
plot(fieldsODE45_interp.U.y,fieldsODE45_interp.y.y,'k-',fieldsRK4_interp.U.y,fieldsRK4_interp.y.y,'b-.',fieldsDEKAF.U.y,fieldsDEKAF.y.y,'r--');
xlabel('U_{yy}'); ylabel('y');
subplot(2,2,2);
plot(fieldsODE45_interp.U.yy,fieldsODE45_interp.y.y,'k-',fieldsRK4_interp.U.yy,fieldsRK4_interp.y.y,'b-.',fieldsDEKAF.U.yy,fieldsDEKAF.y.y,'r--');
xlabel('U_{yy}'); ylabel('y');
subplot(2,2,3);
plot(fieldsODE45_interp.T.y,fieldsODE45_interp.y.y,'k-',fieldsRK4_interp.T.y,fieldsRK4_interp.y.y,'b-.',fieldsDEKAF.T.y,fieldsDEKAF.y.y,'r--');
xlabel('T_{yy}'); ylabel('y');
subplot(2,2,4);
plot(fieldsODE45_interp.T.yy,fieldsODE45_interp.y.y,'k-',fieldsRK4_interp.T.yy,fieldsRK4_interp.y.y,'b-.',fieldsDEKAF.T.yy,fieldsDEKAF.y.y,'r--');
xlabel('T_{yy}'); ylabel('y');
%}
%% Plot that data! (all)
styleS = {'o','x','s','v','h','p','d','*','+','^','>','<'};
styleS = [styleS ,styleS ,styleS ,styleS ];
lineStyle = {'-','-.','--',':'};
RGB_vecR = [1 , 1 , 1 ,0.5, 0 , 0 , 0 , 0 , 0 ,0.5, 1 ];
RGB_vecG = [0 ,0.5, 1 , 1 , 1 , 1 , 1 ,0.5, 0 , 0 , 0 ];
RGB_vecB = [0 , 0 , 0 , 0 , 0 ,0.5, 1 , 1 , 1 , 1 , 1 ];
RGB_vecx = linspace(-1e-8,1+1e-8,11);

list_comparisonsDim = {'RK4 vs ode45','DEKAF vs RK4','ode45 vs DEKAF'};
clear legends plots
figure
diffName = fieldnames(diff_struc);
for ii = 1:length(diffName)
    jj = 0;
    for j1 = 1:length(fields2plot)
        subfields = fieldnames(diff_struc.(diffName{ii}).(fields2plot{j1}));
        N_subfields = length(subfields);
        for j2 = 1:N_subfields
            jj = jj+1;
            Rainbow_vector_it = [interp1(RGB_vecx,RGB_vecR,(jj-1)/(N_fieldsTot-1)),interp1(RGB_vecx,RGB_vecG,(jj-1)/(N_fieldsTot-1)),interp1(RGB_vecx,RGB_vecB,(jj-1)/(N_fieldsTot-1))];
            plots((ii-1)*N_fieldsTot+jj) = loglog(N_vec,diff_struc.(diffName{ii}).(fields2plot{j1}).(subfields{j2}),[lineStyle{ii},styleS{jj}],'Color',Rainbow_vector_it);
            hold on
            if strcmp(subfields{j2},fields2plot{j1}) % it means it's the 0th-order derivative
                str_der = '';
            else
                str_der = ['\partial_{',subfields{j2},'} '];
            end
            legends{(ii-1)*N_fieldsTot+jj} = [list_comparisonsDim{ii},' - $$',str_der,labels2plot{j1},'$$'];
            legend(plots,legends,'interpreter','latex');
        end
    end
end
legend(plots,legends,'interpreter','latex');
xlabel('$$\overline{N}$$','interpreter','latex');
ylabel('$$\epsilon$$','interpreter','latex');

save(savepath,'diff_struc','list_comparisonsDim');