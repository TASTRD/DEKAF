% Test_CNEvsLTE is a script to check that for high Reynolds numbers, the
% CNE profiles converge to equilibrium (LTE).
%
% Author: Fernando Miro Miro
% Date: January 2018

%{%
clear;
close all;
%clc
%}
addpath(genpath('/home/miromiro/workspace/DEKAF/'))

% defining parameters
p_e = 10000;
T_e = 700;
M_e = 10;
Sc_e = 0.5;
N = 100;
L = 40;
eta_i = 6;
thermal_BC = 'adiab';
mixture = 'air5mutation';
modelTransport = 'CE_12';
modelDiffusion = 'FOCE_Ramshaw';

%% Common options
options.N                       = N;                     % Number of points
options.L                       = L;                     % Domain size
options.eta_i                   = eta_i;                 % Mapping parameter, eta_crit
options.thermal_BC              = thermal_BC;            % Wall bc
options.mixture                 = mixture;               % mixture
options.modelTransport          = modelTransport;        % transport model
options.modelDiffusion          = modelDiffusion;        % diffusion model

options.tol                     = 1e-14;                 % Convergence tolerance
options.it_max                  = 40;
options.T_tol                   = 1e-13;                 % tolerance of the solver for the temperature
options.T_itMax                 = 50;

options.plot_transient          = false;                 % to plot the inter-iteration profiles
options.plotRes                 = false;                 % to plot the residual convergence
options.DEBUGvar = 1;
options.max_residual = 1e-2;

% Flow parameters
intel.M_e                       = M_e;                   % Mach number at boundary-layer edge                        [-]
intel.T_e                       = T_e;                   % Static temperature at boundary-layer edge                 [K]
intel.p_e                       = p_e;                   % Absolute static pressure at boundary-layer edge           [Pa]
intel.Sc_e                      = Sc_e;                  % Schmidt number

%% LTEED
options.flow = 'LTEED'; % flow assumption
[intelLTEED,optionsLTEED] = DEKAF(intel,options);

%% LTEED with halved coefficients
options.DEBUGvar = 0.5;
[intelLTEED05,optionsLTEED05] = DEKAF(intel,options);
options.DEBUGvar = 1;

%% LTE
options.flow = 'LTE'; % flow assumption
[intelLTE,optionsLTE] = DEKAF(intel,options);

%% TPG
options.flow = 'TPGD'; % flow assumption
[intelTPGD,optionsTPGD] = DEKAF(intel,options);

%% CNE first block x = 0 - 0.5

x0 = 1e-5; x1 = 0.5; x2 = 100; x3 = 10000;

N_x  = 200;
options.flow            = 'CNE';
options.tol             = 1e-9;
options.mapOpts.x_start = x0;
options.mapOpts.x_end   = x1;
options.mapOpts.N_x     = N_x ;
options.ox              = 2;

[intelCNE1,~] = DEKAF(intel,options,'marching');

%% CNE second block x = 0.5 - 100

options.mapOpts.x_start      = x1;
options.mapOpts.x_end        = x2;
options.restartFromMarching  = true;
options.intelQSS             = intelCNE1;
[intelCNE2,~] = DEKAF(intel,options,'marching');

%% CNE third block x = 100 - 10 000

options.mapOpts.x_start      = x2;
options.mapOpts.x_end        = x3;
options.mapOpts.N_x          = 100;
options.restartFromMarching  = true;
options.intelQSS             = intelCNE2;
[intelCNE3,optionsCNE] = DEKAF(intel,options,'marching');

%% Putting CNE blocks together and evaluating QSS cases
intel_all = build_intelDimFromParts(intelCNE1,intelCNE2,intelCNE3);

intelLTEED05.x = intel_all.x(1,:);    optionsLTEED05.dimXQSS = true;
intelLTEED.x   = intel_all.x(1,:);    optionsLTEED.dimXQSS = true;
intelTPGD.x    = intel_all.x(1,:);    optionsTPGD.dimXQSS = true;
intelLTE.x     = intel_all.x(1,:);    optionsLTE.dimXQSS = true;

intelLTEED05_dimVars = eval_dimVars(intelLTEED05,optionsLTEED05);
intelLTEED_dimVars   = eval_dimVars(intelLTEED,optionsLTEED);
intelTPGD_dimVars    = eval_dimVars(intelTPGD, optionsTPGD);
intelLTE_dimVars     = eval_dimVars(intelLTE,  optionsLTE);

%%
figure
semilogx(intel_all.x(1,:),                    intel_all.T(end,:),                 'displayname','CNE'); hold on;
semilogx(intelLTEED05_dimVars.x(1,[1,end]),   intelLTEED05_dimVars.T(end,[1,end]),'displayname','LTEED with 0.5 C_{EF}');
semilogx(intelLTEED_dimVars.x(1,[1,end]),     intelLTEED_dimVars.T(end,[1,end]),  'displayname','LTEED');
semilogx(intelLTE_dimVars.x(1,[1,end]),       intelLTE_dimVars.T(end,[1,end]),    'displayname','LTE');
ylabel('T_w'); xlabel('x'); xlim([1e-3,Inf]); legend();

%% Plotting comparison
xplot = [0.001,0.01,0.1,1,10,100,1000,10000];
el2plt = 'O';
Np=5;ip=0;
ip=ip+1; stylTPGD    = {'-',                 'Color',0.8*color_rainbow(ip,Np),'displayname','TPG'};
ip=ip+1; stylCNE     = {'-.',                'Color',0.8*color_rainbow(ip,Np),'displayname','CNE'};
ip=ip+1; stylLTEED   = {'--',                'Color',0.8*color_rainbow(ip,Np),'displayname','LTEED'};
ip=ip+1; stylLTEED05 = {':','linewidth',1.5, 'Color',0.8*color_rainbow(ip,Np),'displayname','LTEED with 0.5 C_{EF}'};
ip=ip+1; stylLTE     = {'-',                 'Color',0.8*color_rainbow(ip,Np),'displayname','LTE'};
close all;
figID1 = figure;        figID2 = figure;
figID3 = figure;        figID4 = figure;
Ni=2; Nj=4;
for ii = 1:length(xplot)
    [~,ixi] = min(abs(intel_all.x(1,:)-xplot(ii)));
    name = ['x = ',num2str(intel_all.x(1,ixi)),' m'];
    E = find(strcmp(intel_all.mixCnst.elem_list,el2plt));
    yMax = intelTPGD_dimVars.y_i(ixi);
    
    figure(figID1); subplot(Ni,Nj,ii);
    plot(intelTPGD_dimVars.T(:,ixi),   intelTPGD_dimVars.y(:,ixi),   stylTPGD{:}); hold on;
    plot(intel_all.T(:,ixi),           intel_all.y(:,ixi),           stylCNE{:});
    plot(intelLTEED_dimVars.T(:,ixi),  intelLTEED_dimVars.y(:,ixi),  stylLTEED{:});
    plot(intelLTEED05_dimVars.T(:,ixi),intelLTEED05_dimVars.y(:,ixi),stylLTEED05{:});
    plot(intelLTE_dimVars.T(:,ixi),    intelLTE_dimVars.y(:,ixi),    stylLTE{:});
    ylim([0,yMax]);  xlabel('T [K]'); ylabel('y [m]');  title(name);
    
    figure(figID2); subplot(Ni,Nj,ii);
    plot(intelTPGD_dimVars.h(:,ixi),   intelTPGD_dimVars.y(:,ixi),   stylTPGD{:}); hold on;
    plot(intel_all.h(:,ixi),           intel_all.y(:,ixi),           stylCNE{:});
    plot(intelLTEED_dimVars.h(:,ixi),  intelLTEED_dimVars.y(:,ixi),  stylLTEED{:});
    plot(intelLTEED05_dimVars.h(:,ixi),intelLTEED05_dimVars.y(:,ixi),stylLTEED05{:});
    plot(intelLTE_dimVars.h(:,ixi),    intelLTE_dimVars.y(:,ixi),    stylLTE{:});
    ylim([0,yMax]);  xlabel('h [J/kg]'); ylabel('y [m]');  title(name);
    
    figure(figID3); subplot(Ni,Nj,ii);
    for s=1:intel_all.mixCnst.N_spec
    ip=0;
    ip=ip+1;plots(ip) = plot(intel_all.ys{s}(:,ixi),           intel_all.y(:,ixi),           stylCNE{1:end-2}); hold on;
    ip=ip+1;plots(ip) = plot(intelLTEED_dimVars.ys{s}(:,ixi),  intelLTEED_dimVars.y(:,ixi),  stylLTEED{1:end-2});
    ip=ip+1;plots(ip) = plot(intelLTEED05_dimVars.ys{s}(:,ixi),intelLTEED05_dimVars.y(:,ixi),stylLTEED05{1:end-2});
    ip=ip+1;plots(ip) = plot(intelLTE_dimVars.ys{s}(:,ixi),    intelLTE_dimVars.y(:,ixi),    stylLTE{1:end-2});
    end
    ylim([0,yMax]);  xlabel('y_s [-]'); ylabel('y [m]');  title(name);
    ip=0;
    ip=ip+1;legends{ip} = stylCNE{end};
    ip=ip+1;legends{ip} = stylLTEED{end};
    ip=ip+1;legends{ip} = stylLTEED05{end};
    ip=ip+1;legends{ip} = stylLTE{end};
    
    figure(figID4); subplot(Ni,Nj,ii);
    plot(intel_all.yE{E}(:,ixi),           intel_all.y(:,ixi),           stylCNE{:}); hold on;
    plot(intelLTEED_dimVars.yE{E}(:,ixi),  intelLTEED_dimVars.y(:,ixi),  stylLTEED{:});
    plot(intelLTEED05_dimVars.yE{E}(:,ixi),intelLTEED05_dimVars.y(:,ixi),stylLTEED05{:});
    plot(intelLTE_dimVars.yE{E}(:,ixi),    intelLTE_dimVars.y(:,ixi),    stylLTE{:});
    ylim([0,yMax]);  xlabel(['elemental fraction y_{',el2plt,'} [-]']); ylabel('y [m]');  title(name);
    
end

figure(figID1); legend();
figure(figID2); legend();
figure(figID3); legend(plots,legends);
figure(figID4); legend();
vars2save = {'intel_all','intelLTEED05''intelLTEED','intelLTE','intelTPG','optionsCNE','optionsLTEED05','optionsLTEED','optionsLTE','optionsTPG'};
% save('-v7.3',['intels_CNEvsLTE_M',num2str(M_e),'_T',num2str(T_e),'_p',num2str(p_e),'_x',num2str(intel_all.x(1,end)),'_N',num2str(length(intel_all.x(1,:))),'_',modelTransport,'_',modelDiffusion,'.mat']);
