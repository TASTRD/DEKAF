% caller_Cases_123 is a script to run the Klentzman 2013's Case #1
%
% Date: February 2018
%
clear;
close all;
%clc
%

T_e_list = [278;556;834];
outfile = 'DEKAF_Cases_123';

for ii = length(T_e_list):-1:1

    clear intel options
    addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

    % Optional inputs
    options.N                       = 100;                                      % Number of points
    options.L                       = 40;                                       % Domain size
    options.eta_i                   = 6;                                        % Mapping parameter, eta_crit
    options.thermal_BC              = 'adiab';                                  % Wall bc (g or dg/deta)
    options.flow                    = 'TPGD';                                   % flow assumption
    options.mixture                 = 'oxygen2Bortner';                         % mixture
    options.modelTransport          = 'Brokaw58';                               % transport model
    options.modelDiffusion          = 'FOCE_Klentzman';                         % diffusion model
    options.molarDiffusion          = false;                                    % mass-fraction diffusion
    options.modelCollisionNeutral   = 'GuptaYos90';                             % collision integral data source
    options.modelEquilibrium        = 'Arrhenius';
    options.enhanceChemistry        = [25 9];               % IMPORTANT!!! Klentzman's unexplained factors
    options.collisionRefExceptions.references = {'Yos63'};                      % the Yos exception
    options.collisionRefExceptions.pairs = {{'O - O','N - N'}};

    options.tol                     = 1e-14;                % Convergence tolerance
    options.it_max                  = 100;
    options.T_tol                   = 1e-13;
    options.T_itMax                 = 200;

    options.plot_transient          = false;                % to plot the inter-iteration profiles
    options.plotRes                 = false;                % to plot the residual convergence
    options.MarchingGlobalConv      = false;                % to plot the global convergence plot

    % Flow parameters
    M_e = 10.545;                                   % frozen edge Mach number, -
    p_e = 0.0433*101325;                            % edge pressure, 0.0433 atm to Pa
    ys_e = {1e-10,1-1e-10}; % mass fractions of O, O2
    N_x                     = 20;
    xMax                    = 0.4; % [m]
    intel.x_e               = linspace(0,1.1*xMax,N_x);
    intel.M_e               = M_e * ones(1,N_x);
    intel.T_e               = T_e_list(ii) * ones(1,N_x);
    for s=1:length(intel.ys_e)
    intel.ys_e{s}           = ys_e{s} * ones(1,N_x);
    end
    options.flow            = 'CNE';
    intel.p_e0              = p_e;
    intel                   = rmfield(intel,{'p_e'}); % removing pressure, which will now depend on the velocity gradient (bernouilli)
    intel.Sc_e              = intel.Sc_e * ones(1,N_x);

    options.mapOpts.x_start = 1e-5;
    options.tol             = 1e-10;
    options.it_max          = 400;
    options.mapOpts.x_end   = xMax;
    options.mapOpts.N_x     = 50;
    options.ox              = 2;

    options.plot_marching = false;
    options.dimoutput = true;
    [intel1,options] = DEKAF(intel,options,'marching');

    intel_dimVars = eval_dimVars(intel1,options);

    all_intels(ii) = intel_dimVars;
    all_options(ii) = options;
end

%%
% figure
% plot(intel_dimVars.y(:,end)./0.05596e-2,intel_dimVars.T(:,end)/intel_dimVars.T_e(end));
% grid minor;
% xlim([0,26]); xlabel('y/H [-]'); ylabel('T/T_e');

% save([outfile,'_Ramshaw.mat'],'all_intels','all_options');
save([outfile,'NEW.mat'],'all_intels','all_options');

process_cases
