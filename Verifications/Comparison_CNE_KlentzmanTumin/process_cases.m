clear;
clc;

%% Extract data for Klentzman and DEKAF

% absolute path to this comparison directory
this_folder = ...
    [getenv('DEKAF_DIRECTORY'),'/Verifications/Comparison_CNE_KlentzmanTumin/'];

% location of Klentzman data
KT2013_folder = [this_folder,'digitized_data/'];

% location of DEKAF data
% DEKAF_mat = [this_folder,'DEKAF_Cases_123.mat'];
DEKAF_mat = [this_folder,'DEKAF_Cases_123NEW.mat'];
% DEKAF_mat = [this_folder,'DEKAF_Cases_123_Ramshaw.mat']; % very wrong
% DEKAF_mat = [this_folder,'DEKAF_Cases_123_x4Points.mat']; % didn't change
% DEKAF_mat = [this_folder,'DEKAF_Cases_123_FOCEDiff.mat']; % didn't change
% DEKAF_mat = [this_folder,'DEKAF_Cases_123_cstSc07.mat']; % very wrong
% DEKAF_mat = [this_folder,'DEKAF_Cases_123_noYosCorrection.mat']; % didn't change

% Klentzman edge values
cases   = [1;2;3;];
H_list  = [0.03273;0.04555;0.05596]*10^-2;      % length scales, m
ue_list = [3.351;4.668;5.648]*10^3;             % edge velocity, m/s
Te_list = [278;556;834];                        % edge temp, K
x_list  = [0.4;0.4;0.4];                        % x station, m

% Klentzman csv files for u, T, YO2
csv_u = {...
    'Figure_4a_Case_1_Te_278K.csv',...
    'Figure_4a_Case_2_Te_556K.csv',...
    'Figure_4a_Case_3_Te_834K.csv',...
    };
csv_T = {...
    'Figure_4b_Case_1_Te_278K.csv',...
    'Figure_4b_Case_2_Te_556K.csv',...
    'Figure_4b_Case_3_Te_834K.csv',...
    };
csv_YO2 = {...
    'Figure_5_Case_1_Te_278K.csv',...
    'Figure_5_Case_2_Te_556K.csv',...
    'Figure_5_Case_3_Te_834K.csv',...
    };

% Populate KT2013 struct with Klentzman data
for ii=1:length(csv_u)
    % Initialize case parameters
    KT2013(ii).H    = H_list(ii); %#ok<*SAGROW>
    KT2013(ii).u_e  = ue_list(ii);
    KT2013(ii).T_e  = Te_list(ii);
    KT2013(ii).x    = x_list(ii);

    % Read in data
    KT2013(ii).data.u   = dlmread([KT2013_folder,csv_u{ii}]);
    KT2013(ii).data.T   = dlmread([KT2013_folder,csv_T{ii}]);
    KT2013(ii).data.YO2 = dlmread([KT2013_folder,csv_YO2{ii}]);

    % Extract non-dimensional components
    KT2013(ii).nd.u     = KT2013(ii).data.u(:,2);
    KT2013(ii).nd.T     = KT2013(ii).data.T(:,2);
    KT2013(ii).nd.YO2   = KT2013(ii).data.YO2(:,2);
    KT2013(ii).nd.y_u   = KT2013(ii).data.u(:,1);
    KT2013(ii).nd.y_T   = KT2013(ii).data.T(:,1);
    KT2013(ii).nd.y_YO2 = KT2013(ii).data.YO2(:,1);

    % Calculate dimensional quantities
    KT2013(ii).dm.u     = KT2013(ii).nd.u*KT2013(ii).u_e;
    KT2013(ii).dm.T     = KT2013(ii).nd.T*KT2013(ii).T_e;
    KT2013(ii).dm.y_u   = KT2013(ii).nd.y_u*KT2013(ii).H;
    KT2013(ii).dm.y_T   = KT2013(ii).nd.y_T*KT2013(ii).H;
    KT2013(ii).dm.y_YO2 = KT2013(ii).nd.y_YO2*KT2013(ii).H;
end

% Populate DEKAF struct with our own data
load(DEKAF_mat); % loads all_intels, all_options

% Assume that the DEKAF test cases were saved as a multiple of the Klentzman test cases
% If DEKAF ran more test cases than Klentzman's, do this loop.
if length(all_intels) ~= length(csv_u)
    H_list_rep  = repmat(H_list,[length(all_intels)-length(csv_u),1]);
    ue_list_rep = repmat(ue_list,[length(all_intels)-length(csv_u),1]);
    Te_list_rep = repmat(Te_list,[length(all_intels)-length(csv_u),1]);
    x_list_rep  = repmat(x_list,[length(all_intels)-length(csv_u),1]);       % x station, m
    cases_rep   = repmat(cases,[length(all_intels)-length(csv_u),1]);
else % DEKAF ran the same number of test cases as Klentzman
    H_list_rep  = H_list;
    ue_list_rep = ue_list;
    Te_list_rep = Te_list;
    x_list_rep  = x_list;
    cases_rep   = cases;
end

for ii = 1:length(all_intels)
    [~,idx_x] = min(abs(all_intels(ii).x(1,:) - x_list_rep(ii)));

    % Extract dimensional quantities (and mass fraction)
    DEKAF(ii).dm.y = all_intels(ii).y(:,idx_x);
    DEKAF(ii).dm.u = all_intels(ii).u(:,idx_x);
    DEKAF(ii).dm.T = all_intels(ii).T(:,idx_x);
    DEKAF(ii).nd.YO2 = all_intels(ii).ys{2}(:,idx_x);

    % Reconstruct nondimensional quantities
    DEKAF(ii).nd.y = DEKAF(ii).dm.y/H_list_rep(ii);
    DEKAF(ii).nd.u = DEKAF(ii).dm.u/ue_list_rep(ii);
    DEKAF(ii).nd.T = DEKAF(ii).dm.T/Te_list_rep(ii);

end

%% Plotting results
LW = [1.5,1.5,1.5];
clr = {'k','r','b'};
LS = {'-.','-'};
ybound = 0.01;

legnd = cell(length(all_intels) + length(csv_u),1);
idx = 1;
for ii=1:length(all_intels)
    legnd{ii,1} = sprintf('DEKAF all intels(%d): Case %d',ii,cases_rep(ii));
end
legnd{length(all_intels)+1,1} = 'Klentzman 2013: Case 1';
legnd{length(all_intels)+2,1} = 'Klentzman 2013: Case 2';
legnd{length(all_intels)+3,1} = 'Klentzman 2013: Case 3';

%%%%%%%%%%%%%%%%%%%%%%%%%%%% DIMENSIONAL PLOTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure
suptitle(sprintf('Binary Oxygen, Plate, x = %.1f m, dimensional u and T',x_list_rep(1)))

% u profile
subplot(1,3,1);
for ii=1:length(all_intels)
    plot(DEKAF(ii).dm.y,DEKAF(ii).dm.u,'color',clr{ii},'linestyle',LS{1},'linewidth',LW(ii));
    hold on
end
plot(KT2013(1).dm.y_u,KT2013(1).dm.u,'color',clr{1},'linestyle',LS{2},'linewidth',LW(1));
plot(KT2013(2).dm.y_u,KT2013(2).dm.u,'color',clr{2},'linestyle',LS{2},'linewidth',LW(1));
plot(KT2013(3).dm.y_u,KT2013(3).dm.u,'color',clr{3},'linestyle',LS{2},'linewidth',LW(1));
xlabel('y (m)');
ylabel('u (m/s)');
xlim([0 ybound]);
grid minor
legend(legnd,'location','best');

% T profile
subplot(1,3,2);
for ii=1:length(all_intels)
    plot(DEKAF(ii).dm.y,DEKAF(ii).dm.T,'color',clr{ii},'linestyle',LS{1},'linewidth',LW(ii));
    hold on
end
plot(KT2013(1).dm.y_T,KT2013(1).dm.T,'color',clr{1},'linestyle',LS{2},'linewidth',LW(1));
plot(KT2013(2).dm.y_T,KT2013(2).dm.T,'color',clr{2},'linestyle',LS{2},'linewidth',LW(1));
plot(KT2013(3).dm.y_T,KT2013(3).dm.T,'color',clr{3},'linestyle',LS{2},'linewidth',LW(1));
xlabel('y (m)');
ylabel('T (K)');
xlim([0 ybound]);
grid minor

% YO2 profile
subplot(1,3,3);
for ii=1:length(all_intels)
    plot(DEKAF(ii).dm.y,DEKAF(ii).nd.YO2,'color',clr{ii},'linestyle',LS{1},'linewidth',LW(ii));
    hold on
end
plot(KT2013(1).dm.y_YO2,KT2013(1).nd.YO2,'color',clr{1},'linestyle',LS{2},'linewidth',LW(1));
plot(KT2013(2).dm.y_YO2,KT2013(2).nd.YO2,'color',clr{2},'linestyle',LS{2},'linewidth',LW(1));
plot(KT2013(3).dm.y_YO2,KT2013(3).nd.YO2,'color',clr{3},'linestyle',LS{2},'linewidth',LW(1));
xlabel('y (m)');
ylabel('O_2 mass fraction');
xlim([0 ybound]);
grid minor

%%%%%%%%%%%%%%%%%%%%%%%%%% NONDIMENSIONAL PLOTS %%%%%%%%%%%%%%%%%%%%%%%%%%%

ybound = 22;

figure
suptitle(sprintf('Binary Oxygen, Plate, x = %.1f m, nondimensional u and T',x_list_rep(1)))

% u profile
subplot(1,3,1);
for ii=1:length(all_intels)
    plot(DEKAF(ii).nd.y,DEKAF(ii).nd.u,'color',clr{ii},'linestyle',LS{1},'linewidth',LW(ii));
    hold on
end
plot(KT2013(1).nd.y_u,KT2013(1).nd.u,'color',clr{1},'linestyle',LS{2},'linewidth',LW(1));
plot(KT2013(2).nd.y_u,KT2013(2).nd.u,'color',clr{2},'linestyle',LS{2},'linewidth',LW(1));
plot(KT2013(3).nd.y_u,KT2013(3).nd.u,'color',clr{3},'linestyle',LS{2},'linewidth',LW(1));
xlabel('y/H');
ylabel('u/u_e');
xlim([0 ybound]);
grid minor
legend(legnd,'location','best');

% T profile
subplot(1,3,2);
for ii=1:length(all_intels)
    plot(DEKAF(ii).nd.y,DEKAF(ii).nd.T,'color',clr{ii},'linestyle',LS{1},'linewidth',LW(ii));
    hold on
end
plot(KT2013(1).nd.y_T,KT2013(1).nd.T,'color',clr{1},'linestyle',LS{2},'linewidth',LW(1));
plot(KT2013(2).nd.y_T,KT2013(2).nd.T,'color',clr{2},'linestyle',LS{2},'linewidth',LW(1));
plot(KT2013(3).nd.y_T,KT2013(3).nd.T,'color',clr{3},'linestyle',LS{2},'linewidth',LW(1));
xlabel('y/H');
ylabel('T/T_e');
xlim([0 ybound]);
grid minor

% YO2 profile
subplot(1,3,3);
for ii=1:length(all_intels)
    plot(DEKAF(ii).nd.y,DEKAF(ii).nd.YO2,'color',clr{ii},'linestyle',LS{1},'linewidth',LW(ii));
    hold on
end
plot(KT2013(1).nd.y_YO2,KT2013(1).nd.YO2,'color',clr{1},'linestyle',LS{2},'linewidth',LW(1));
plot(KT2013(2).nd.y_YO2,KT2013(2).nd.YO2,'color',clr{2},'linestyle',LS{2},'linewidth',LW(1));
plot(KT2013(3).nd.y_YO2,KT2013(3).nd.YO2,'color',clr{3},'linestyle',LS{2},'linewidth',LW(1));
xlabel('y/H');
ylabel('O_2 mass fraction');
xlim([0 ybound]);
grid minor

