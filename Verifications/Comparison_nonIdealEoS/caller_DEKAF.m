% caller_DEKAF runs the SS solver for the test on the ideal-gas equation of
% state.
%
% Author: Fernando Miro Miro
% April 2020


clear; clc;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
U_e = 350;          % [m/s]
T_e = 1000;         % [K]
Tw  = 300;          % [K]
p_e = 50e6;         % [Pa]
x   = 2;            % [m]
EoS_vec = {'idealGas','vanDerWaals0','vanDerWaals'};
NE = length(EoS_vec);

% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size (in non-dimensional eta length)
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 5e-13;                % Convergence tolerance
options.it_max                  = 40;                   % maximum number of iterations

% flow options
% options.thermal_BC              = 'adiab';              % Wall bc
options.thermal_BC              = 'Twall';              % Wall bc (isothermal)
options.G_bc                    = Tw;                   % value of the wall temperature
options.flow                    = 'CPG';                % flow assumption
options.mixture                 = 'N2';                 % mixture
options.modelTransport          = 'Sutherland';         % transport model
options.cstPr                   = false;                % non-constant Prandtl

options.BLproperties = true;
options.dimoutput = true;
options.dimXQSS = true;

% display options
options.plotRes = false;                         % we don't want the residual plot

% flow conditions
intel.U_e               = U_e;                  % streamwise velocity at boundary-layer edge    [m/s]
intel.T_e               = T_e;                  % Temperature at boundary-layer edge            [K]
intel.p_e               = p_e;                  % Pressure at boundary-layer edge               [Pa]
intel.x                 = x;                    % dimensional streamwise location to evaluate   [m]

% Executing DEKAF
for ii=NE:-1:1
    options.EoS = EoS_vec{ii};
    [intel_all{ii},options_all{ii}] = DEKAF(intel,options);
end

% Saving
saveDEKAF('DKF_EoScompare.mat','intel_all','options_all','EoS_vec');

%% plotting
figure;  Ni=1;Nj=3;
styleS = {'-','-.','--'};
yMax = max(cellfun(@(cll)max(cll.y_i),intel_all));
for ii=1:NE
    ip=0;
ip=ip+1;subplot(Ni,Nj,ip); plot(intel_all{ii}.u,  intel_all{ii}.y,styleS{ii},'Color',two_colors(ii,NE,'Rbw2'),'displayname',EoS_vec{ii}); hold on;
                xlabel('u [m/s]');      ylabel('y [m]'); grid minor; ylim([0,yMax]);
ip=ip+1;subplot(Ni,Nj,ip); plot(intel_all{ii}.T,  intel_all{ii}.y,styleS{ii},'Color',two_colors(ii,NE,'Rbw2'),'displayname',EoS_vec{ii}); hold on;
                xlabel('T [K]');        ylabel('y [m]'); grid minor; ylim([0,yMax]);
ip=ip+1;subplot(Ni,Nj,ip); plot(intel_all{ii}.rho,intel_all{ii}.y,styleS{ii},'Color',two_colors(ii,NE,'Rbw2'),'displayname',EoS_vec{ii}); hold on;
                xlabel('rho [kg/m^3]'); ylabel('y [m]'); grid minor; ylim([0,yMax]);
end
legend();
