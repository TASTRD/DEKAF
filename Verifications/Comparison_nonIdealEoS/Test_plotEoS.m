% Test_plotEoS simply plots the pressure for various equations of state
%
% Author: Fernando Miro Miro
% Date: April 2020


clear; %close all;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
p_vec = 10.^(2:8);            % [Pa]
T_e = 30:1:1000;      % [K]
EoS_vec = {'idealGas','vanDerWaals0','vanDerWaals'};
mixture = 'N2';
flow = 'CPG';

Np = length(p_vec);
NE = length(EoS_vec);
for ip=Np:-1:1
for ii=NE:-1:1
    options.mixture = mixture;
    options.EoS = EoS_vec{ii};
    [mixCnst,options] = getAll_constants(mixture,options);
    R = mixCnst.R0/mixCnst.Mm_s;
    rho{ii,ip}  = getMixture_rho([],p_vec(ip),T_e,[],[],[],[],'specify_R',R,mixCnst.EoS_params,options);
end
end

%% plotting
styleS = {'-','-.','--'};
figure;[Ni,Nj] = optimalPltSize(Np);
for ip=1:Np
    subplot(Ni,Nj,ip); 
for ii=1:NE
    semilogy(T_e,rho{ii,ip}, styleS{ii},'Color',two_colors(ii,NE,'Rbw2'),'displayname',EoS_vec{ii}); hold on;
end
    xlabel('T [K]'); ylabel('\rho [kg/m^3]'); grid minor; title(['p = 10^',num2str(log10(p_vec(ip))),' Pa']);
end
 legend(); 


%% other plotting
clear plots legends
T_vec = 30:10:300;
[aEoS,bEoS] = getVanDerWaals_ab(mixCnst.EoS_params.pCrit_s,mixCnst.EoS_params.TCrit_s,R);
NT = length(T_vec);
p_e = 1e6;
[~,idxp] = min(abs(p_vec-p_e));
figure;
for iT=1:NT
A = aEoS.*bEoS;
B = -aEoS;
C = bEoS.*p_e + R.*T_vec(iT);
D = -p_e;
rhon = 1:0.1:1000;
vn = 1./rhon;
F_vec = A.*rhon.^3 + B.*rhon.^2 + C.*rhon + D;

clr = two_colors(iT,NT,'Rbw2');
plots(iT) = plot(rhon,F_vec,'Color',clr); hold on;
legends{iT} = ['T = ',num2str(T_vec(iT)),' K'];

[~,idxT] = min(abs(T_e - T_vec(iT)));
plots(NT+1) = plot(rho{2,idxp}(idxT),0,'o','Color',clr); hold on;
plots(NT+2) = plot(rho{1,idxp}(idxT),0,'x','Color',clr); hold on;
end
legends{NT+1} = 'vanDerWaals0';
legends{NT+2} = 'idealGas';
grid on; xlabel('\rho'); ylabel('F(\rho)'); 
legend(plots,legends,'location','eastoutside'); ylim([-5e6,5e6]); xlim([0,670]);
title(['Residuals of the van der Waals equation of state for p=',num2str(p_e*1e-6),'MPa']);