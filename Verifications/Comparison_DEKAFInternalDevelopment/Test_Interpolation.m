clear all
close all
clc

N       = 40;
y_max   = 2*pi;
y_i     = 1;

[eta_star,DM] = chebDif(N,4);

[eta,D1,D2,D3,D4]  = MappingMalik_uptoD4(y_max,y_i,eta_star',...
    DM(:,:,1),DM(:,:,2),DM(:,:,3),DM(:,:,4));

figure
plot(eta,cos(eta),'-k')
hold on
plot(eta,D1*cos(eta),'--k')
plot(eta,D2*cos(eta),'-.k')
plot(eta,D3*cos(eta),':k')
plot(eta,D4*cos(eta),'-g')

M = N*50;
eta_star_equi = linspace(1,-1,M);
[~,DMnull] = chebDif(M,4);
eta_equi = MappingMalik_uptoD4(y_max,y_i,eta_star_equi',...
    DMnull(:,:,1),DMnull(:,:,2),DMnull(:,:,3),DMnull(:,:,4));

chebcos = chebinterp(cos(eta),eta_star_equi);
D1chebcos = chebinterp(D1*cos(eta),eta_star_equi);

plot(eta_equi,chebcos,'-r')
plot(eta_equi,D1chebcos,'--r')
