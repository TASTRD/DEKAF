% Test_debugging_multiSpec is a script to run DEKAF several times to try
% and debug the spec-spec implementation
%
% Date: March 2018
%{%
clear;

clear intel options
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']));

modelDiff_vec = {'FOCE_Ramshaw','FOCE_Effective'};
% modelDiff_vec = {'FOCE_Effective','FOCE_Ramshaw'};
M_e = 10;
T_e = 600;
Re1_e = 6.6e6;

Nrun = length(modelDiff_vec);
for ii=1:Nrun
% Optional inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.thermal_BC              = 'adiab';              % Wall bc (g or dg/deta)
% options.G_bc                    = 3000;                 % Value for g boundary condition
options.flow                    = 'CNE';               % flow assumption
options.mixture                 = 'air5mutation';       % mixture
options.modelTransport          = 'CE_12';               % transport model
options.modelDiffusion          = modelDiff_vec{ii};    % diffusion model

options.tol                     = 1e-14;                % Convergence tolerance
options.it_max                  = 40;
options.T_tol                   = 1e-13;
options.T_itMax                 = 200;

options.plot_transient          = false;                % to plot the inter-iteration profiles
options.plotRes                 = false;                % to plot the residual convergence

% Flow parameters
N_x                     = 10;
intel.x_e               = linspace(0.001,0.6,N_x);
intel.M_e               = M_e * ones(1,N_x);
intel.T_e               = T_e * ones(1,N_x);
% intel.Sc_e              = Sc_e * ones(1,N_x);
intel.Re1_e0            = Re1_e;

options.mapOpts.x_start = 1e-5;
options.tol             = 1e-9;
options.it_max          = 40;
options.mapOpts.x_end   = intel.x_e(end);
options.mapOpts.N_x     = 200;
options.ox              = 2;

options.plot_marching = false;
options.dimoutput     = true;
[intel1,options] = DEKAF(intel,options,'marching');

all_intels{ii} = intel_dimVars;
all_options{ii} = options;
end
%}
%% plotting
Nplots = 10;
idx_plot = int16(linspace(1,options.mapOpts.N_x ,Nplots));
colors = {'KR','GB'};
Styles = {'-','-.','--'};
ylimit = [0,0.008];
itplt = 0; % initializing
clear plots legends
figure
for ii=1:Nrun
    for jj=1:Nplots
        itplt = itplt+1;
        subplot(1,3,1);
        plots(itplt) = plot(all_intels{ii}.u(:,idx_plot(jj)),all_intels{ii}.y(:,idx_plot(jj)),Styles{ii},'Color',two_colors(jj,Nplots,colors{ii}));
        ylabel('y [m]'); xlabel('u [m/s]'); grid minor; hold on;
        legends{itplt} = [all_options{ii}.modelDiffusionMulti,' at x = ',num2str(all_intels{ii}.x(1,idx_plot(jj))),' m'];
        ylim(ylimit);

        subplot(1,3,2);
        plot(all_intels{ii}.T(:,idx_plot(jj)),all_intels{ii}.y(:,idx_plot(jj)),Styles{ii},'Color',two_colors(jj,Nplots,colors{ii}));
        ylabel('y [m]'); xlabel('T [K]'); grid minor; hold on;
        ylim(ylimit);

        subplot(1,3,3);
        plot(all_intels{ii}.ys{2}(:,idx_plot(jj)),all_intels{ii}.y(:,idx_plot(jj)),Styles{ii},'Color',two_colors(jj,Nplots,colors{ii}));
        ylabel('y [m]'); xlabel('Y_O [-]'); grid minor; hold on;
        ylim(ylimit);
    end
end