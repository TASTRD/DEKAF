clear all
close all
clc

N = 100;
L = 40;
eta_i = 6;

[eta_star,DM]   = chebDif(N,4);
[eta,D1,D2,D3]  = MappingMalik_uptoD4(L,eta_i,eta_star',DM(:,:,1),DM(:,:,2),DM(:,:,3),DM(:,:,4));

Ddu = D1;
Ddu(end,:) = [zeros(1,N-1) 1];

Idu = inv(Ddu);
Idu(:,end) = Idu(:,end)-ones(N,1);

f1 = eta.^0;pf1 = eta;
f2 = eta;pf2 = eta.^2/2;

figure(1)
plot(Idu*f1-pf1,eta)


figure(1)