% Test_Collision_FatFingering.m is a script to test the curve
% fits of DEKAF against Mutation's data points
%
% Author: Ethan Beyak

close all
clear

addpath(genpath(getenv('DEKAF_DIRECTORY')));
% addpath('./mat_files')
T_i = 300;
T_f = 2*10^4;
N_T = 3000;
spec_list = {'N','O','NO','N2','O2'};
% spec_list = {'O','O2'};

% HARDCODE WARNING: THIS SCRIPT ONLY WORKS FOR AIR5 and BINARY OXYGEN
% you have to comment in/out big blocks of code down below

%% Load data from Mutation
% See ../data/transport/collisions.xml for the entire list of collision data
% HARDCODE WARNING
% The highlights are hardcoded here - neutrals only
% N to N collision
TMp_N_N     = [300   500  1000  2000  4000  5000  6000  8000 10000 15000 20000];    % K
Q11_N_N     = [8.07  7.03  5.96  5.15  4.39  4.14  3.94  3.61  3.37  2.92  2.62];   % A^2;
Q22_N_N     = [9.11  7.94  6.72  5.82  4.98  4.70  4.48  4.14  3.88  3.43  3.11];
% N to N2 collision
TMp_N_N2     = [300   600  1000  2000  4000  6000  8000 10000];     % K
Q11_N_N2     = [10.10  8.57  7.70  6.65  5.65  5.05  4.61  4.25];   % A^2;
Q22_N_N2     = [11.21  9.68  8.81  7.76  6.73  6.18  5.74  5.36];
% N to NO collision
TMp_N_NO     = [500   600  1000  2000  4000  5000  6000  8000 10000 15000];   % K
Q11_N_NO     = [8.21  7.86  6.99  5.90  4.91  4.61  4.37  4.01  3.73  3.27];   % A^2;
Q22_N_NO     = [9.65  9.26  8.29  7.07  5.94  5.60  5.33  4.91  4.60  4.06];
% N to O collision
TMp_N_O     = [300   500  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
Q11_N_O     = [8.32  7.34  6.22  5.26  4.45  4.21  4.01  3.69  3.43  2.98  2.66];   % A^2;
Q22_N_O     = [9.08  8.15  7.09  6.06  5.14  4.88  4.67  4.34  4.07  3.56  3.21];
% N to O2 collision
TMp_N_O2     = [500   600  1000  2000  4000  5000  6000  8000 10000 15000];   % K
Q11_N_O2     = [7.56  7.26  6.55  5.60  4.75  4.49  4.28  3.96  3.72  3.31];   % A^2;
Q22_N_O2     = [8.79  8.47  7.68  6.63  5.67  5.38  5.14  4.78  4.51  4.04];
% N2 to N2 collision
TMp_N2_N2    = [300   600  1000  2000  4000  6000  8000 10000];   % K
Q11_N2_N2    = [12.23 10.60  9.79  8.60  7.49  6.87  6.43  6.06];   % A^2;
Q22_N2_N2    = [13.72 11.80 10.94  9.82  8.70  8.08  7.58  7.32];
% N2 to NO collision
TMp_N2_NO    = [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000];   % K
Q11_N2_NO    = [11.88 10.61 10.24  9.35  8.12  6.82  6.43  6.12  5.66  5.31  4.71];   % A^2;
Q22_N2_NO    = [13.44 11.87 11.44 10.48  9.32  8.04  7.61  7.27  6.74  6.33  5.62];
% N2 to O collision
TMp_N2_O    = [300  1000  2000  4000  5000 10000 15000];   % K
Q11_N2_O    = [8.07  5.93  5.17  4.77  4.31  3.71  3.38];   % A^2;
Q22_N2_O    = [8.99  6.72  5.91  5.22  5.01  4.36  3.95];
% N2 to O2 collision
TMp_N2_O2   = [300  1000  2000  4000  5000 10000 15000];
Q11_N2_O2   = [10.16  7.39  6.42  5.59  5.35  4.60  4.20];
Q22_N2_O2   = [11.23  8.36  7.35  6.47  6.21  5.42  4.94];
% NO to NO collision
TMp_NO_NO   = [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000];
Q11_NO_NO   = [11.66 10.33  9.97  9.09  7.90  6.60  6.24  5.96  5.54  5.23  4.70];
Q22_NO_NO   = [13.25 11.58 11.15 10.16  9.07  7.91  7.53  7.21  6.73  6.36  5.72];
% NO to O collision
TMp_NO_O   = [500   600  1000  2000  4000  5000  6000  8000 10000 15000];
Q11_NO_O   = [7.57  7.27  6.55  5.62  4.78  4.52  4.31  4.00  3.76  3.35];
Q22_NO_O   = [8.79  8.47  7.66  6.64  5.69  5.40  5.17  4.82  4.55  4.08];
% NO to O2 collision
TMp_NO_O2   = [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000];
Q11_NO_O2   = [11.39 10.10  9.75  8.89  7.74  6.56  6.23  5.98  5.59  5.31  4.82];
Q22_NO_O2   = [12.93 11.32 10.90  9.94  8.89  7.80  7.45  7.17  6.73  6.39  5.80];
% O to O collision
TMp_O_O     = [300   500  1000  2000  4000  5000  6000  8000 10000 15000 20000];
Q11_O_O     = [8.53  7.28  5.89  4.84  4.00  3.76  3.57  3.27  3.05  2.65  2.39];
Q22_O_O     = [9.46  8.22  6.76  5.58  4.67  4.41  4.20  3.88  3.64  3.21  2.91];
% O to O2 collision
TMp_O_O2     = [300   600  1000  2000  4000  6000  8000 10000];
Q11_O_O2     = [9.10   7.58  6.74  5.70  4.78  4.29  3.96  3.71];
Q22_O_O2     = [10.13  8.61  7.78  6.71  5.67  5.13  4.78  4.50];
% O2 to O2 collision
TMp_O2_O2     = [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000];
Q11_O2_O2     = [11.12  9.88  9.53  8.69  7.60  6.52  6.22  5.99  5.64  5.39  4.94];
Q22_O2_O2     = [12.62 11.06 10.65  9.72  8.70  7.70  7.38  7.12  6.73  6.42  5.89];

% HARDCODE WARNING for binary oxygen
% TMp{1} = TMp_O_O;       Q11{1} = Q11_O_O;       Q22{1} = Q22_O_O;
% TMp{2} = TMp_O_O2;      Q11{2} = Q11_O_O2;      Q22{2} = Q22_O_O2;
% TMp{3} = TMp_O2_O2;     Q11{3} = Q11_O2_O2;     Q22{3} = Q22_O2_O2;

% % HARDCODE WARNING
% Stack all the hardcode in a matrix that agrees with the spec_list order of spec_list = {'N','O','NO','N2','O2'};
TMp{1} = TMp_N_N;       Q11{1} = Q11_N_N;       Q22{1} = Q22_N_N;
TMp{2} = TMp_N_O;       Q11{2} = Q11_N_O;       Q22{2} = Q22_N_O;
TMp{3} = TMp_N_NO;      Q11{3} = Q11_N_NO;      Q22{3} = Q22_N_NO;
TMp{4} = TMp_N_N2;      Q11{4} = Q11_N_N2;      Q22{4} = Q22_N_N2;
TMp{5} = TMp_N_O2;      Q11{5} = Q11_N_O2;      Q22{5} = Q22_N_O2;
TMp{6} = TMp_O_O;       Q11{6} = Q11_O_O;       Q22{6} = Q22_O_O;
TMp{7} = TMp_NO_O;      Q11{7} = Q11_NO_O;      Q22{7} = Q22_NO_O;
TMp{8} = TMp_N2_O;      Q11{8} = Q11_N2_O;      Q22{8} = Q22_N2_O;
TMp{9} = TMp_O_O2;      Q11{9} = Q11_O_O2;      Q22{9} = Q22_O_O2;
TMp{10} = TMp_NO_NO;    Q11{10} = Q11_NO_NO;    Q22{10} = Q22_NO_NO;
TMp{11} = TMp_N2_NO;    Q11{11} = Q11_N2_NO;    Q22{11} = Q22_N2_NO;
TMp{12} = TMp_NO_O2;    Q11{12} = Q11_NO_O2;    Q22{12} = Q22_NO_O2;
TMp{13} = TMp_N2_N2;    Q11{13} = Q11_N2_N2;    Q22{13} = Q22_N2_N2;
TMp{14} = TMp_N2_O2;    Q11{14} = Q11_N2_O2;    Q22{14} = Q22_N2_O2;
TMp{15} = TMp_O2_O2;    Q11{15} = Q11_O2_O2;    Q22{15} = Q22_O2_O2;
%
%% Calculate collisional data for DEKAF
T_DEKAF = linspace(T_i,T_f,N_T)';
exceptions.references = {'Yos63'};
exceptions.pairs = {{'O - O','N - N'}};

% GuptaYos90 with Yos67 exceptions
[Omega11_sl,Omega22_sl,Bstar_sl,Dbar_binary_sl,stats_fits,cellPairs] = getPair_Collision_constants(spec_list,'GuptaYos90','',exceptions);

% GuptaYos90 without exceptions
% [Omega11_sl,Omega22_sl,Bstar_sl,Dbar_binary_sl,stats_fits,cellPairs] = getPair_Collision_constants(spec_list,'GuptaYos90','');

% hardcode the strings for 'GuptaYos90' modelDiffusion and modelCollisionNeutral
T_3D = repmat(T_DEKAF,[1,N_spec,N_spec]);
lnOmega11_sl   = eval_Fit_PolyLogLog(T_3D,Omega11_sl);
lnOmega22_sl   = eval_Fit_PolyLogLog(T_3D,Omega22_sl);
lnBstar_sl     = eval_Fit_PolyLogLog(T_3D,Bstar_sl);
lnDbar_sl      = eval_Fit_PolyLogLog(T_3D,Dbar_binary_sl);

% % Wright2005
% [Omega11_sl,Omega22_sl,Bstar_sl,~,stats_fits,cellPairs] = getPair_Collision_constants(spec_list,'Wright2005','');
% lnOmega11_sl   = eval_Fit_PolyLogLog(T_DEKAF,Omega11_sl); % Already in m^2
% lnOmega22_sl   = eval_Fit_PolyLogLog(T_DEKAF,Omega22_sl); % Already in m^2
% lnBstar_sl      = eval_Fit_PolyLogLog(T_DEKAF,Bstar_sl);

N_spec = length(spec_list);

%% Plotting

Q11_DEKAF = exp(lnOmega11_sl)*10^20; % converted to A^2
Q22_DEKAF = exp(lnOmega22_sl)*10^20; % converted to A^2

% Then plot all at once
ii = 1;
jj = 1;
n = 150; % skip every 150 for plotting DEKAF markers
% AIR5
nRow = 3; % HARDCODE WARNING
nCol = 5; % HARDCODE WARNING
% BINARY MIXTURE
% nRow = 2; % HARDCODE WARNING
% nCol = 2; % HARDCODE WARNING
figure(1)
for cc=1:size(TMp,2)
    subplot(nRow,nCol,cc)
    plot(TMp{cc},Q11{cc},'r--+'); hold on
    plot(TMp{cc},Q22{cc},'r--*'); hold on
%     % Plot extras for markers
%     plot(T_DEKAF(1:n:end),Q11_DEKAF(1:n:end,ii,jj),'b--+');
%     plot(T_DEKAF(1:n:end),Q22_DEKAF(1:n:end,ii,jj),'b--*');
    % Fill in all temperatures
    plot(T_DEKAF,Q11_DEKAF(:,ii,jj),'b--');
    plot(T_DEKAF,Q22_DEKAF(:,ii,jj),'b--');
%     ylim([2 14])
    ylabel('\Omega^{1,1} & \Omega^{2,2} [A^2]');
    xlabel('T [K]')
    title([cellPairs{ii,jj},' Collision'])
    grid minor
    legend({'\Omega^{1,1} M++','\Omega^{2,2} M++','\Omega^{1,1} DEKAF w/ GY90 Fit','\Omega^{2,2} DEKAF w/ GY90 Fit'},'location','best')
    % Increment ii and jj
    jj = jj + 1;
    if jj>nCol
        ii = ii + 1;
        jj = ii;    % Only hit the upper triangular to avoid repeating pairs
    end
end

% Plot all Omega11's on the same figure to identify any trends
ii = 1;
jj = 1;
nRow = 3;
nCol = 5;
figure
N_p = size(TMp,2);
cellPairs_uniq = cell(N_p,1);
for cc=1:N_p
    plot(TMp{cc},Q11{cc},'Color',color_rainbow(cc,N_p),'linewidth',2); hold on;
    cellPairs_uniq{cc,1} = cellPairs{ii,jj};
    jj = jj + 1;
    if jj>nCol
        ii = ii + 1;
        jj = ii;    % Only hit the upper triangular to avoid repeating pairs
    end
end
ylabel('\Omega^{1,1} [A^2]');
xlabel('T [K]')
title('\Omega^{1,1} for Air 5: Mutation++ values');
legend(strcat(cellPairs_uniq,' Collision'))
grid minor
