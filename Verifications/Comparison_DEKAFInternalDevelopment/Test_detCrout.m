% Test_detCrout

close all;

N = 5;
N_it = 1e3;

minor_max = zeros(N_it,1);
det_max = zeros(N_it,1);
for it = 1:N_it
    mat = rand(N);

N = size(mat,1);

    mat_3D = permute(mat,[3,1,2]);
    [det_Crout,minor_Crout_3D] = detCrout_3D(mat_3D);
    minor_Crout = permute(minor_Crout_3D,[2,3,1]);

    minor = zeros(N);
    for ii=1:N
        for jj=1:N
            mat_minor = mat;
            mat_minor(ii,:) = [];
            mat_minor(:,jj) = [];
            minor(ii,jj) = (-1)^(ii+jj) * det(mat_minor);
%             [L, U] = LUdecompCrout(mat_minor);
%             minor_LU(ii,jj) = (-1)^(ii+jj) * prod(diag(L)) * prod(diag(U));
%             [L,U] = lu(mat_minor);
%             minor_LU_mtlb(ii,jj) = (-1)^(ii+jj) * prod(diag(L)) * prod(diag(U));
        end
    end

    minor_max(it) = max(max(abs(minor-minor_Crout)));
    det_max(it) = abs(det(mat)-det_Crout);
    disp(['Completed ',num2str(it),'/',num2str(N_it)]);
end

pdf(minor_max); title('minors');
pdf(det_max); title('det');