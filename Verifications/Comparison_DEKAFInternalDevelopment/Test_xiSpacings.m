% Test_xiSpacings is a script to run DEKAF with several spacings in
% the xi direction and make sure that the results coincide
%
% Author: Fernando Miro Miro
% Date: March 2018
%{%
clear;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']));

xMappings = {'log','linear'};
N_map = length(xMappings);

for imap = 1:N_map
clear intel options
addpath(genpath('../SourceCode'))

% Optional inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.thermal_BC              = 'adiab';              % Wall bc (g or dg/deta)
% options.G_bc                    = 3000;                 % Value for g boundary condition
options.flow                    = 'CNE';
options.mixture                 = 'air5mutation';       % mixture

options.tol                     = 1e-14;                % Convergence tolerance
options.it_max                  = 40;
options.T_tol                   = 1e-13;
options.T_itMax                 = 200;
% options.perturbRelaxation       = 2;

options.plot_transient          = false;                % to plot the inter-iteration profiles
options.plotRes                 = false;                % to plot the residual convergence

% Flow parameters
M_e = 15;
T_e = 600;
Re1_e = 3.4e6;
Sc_e = 0.71;

N_x                     = 10;
intel.x_e               = linspace(0.001,0.5,N_x);
intel.M_e               = M_e * ones(1,N_x);
intel.T_e               = T_e * ones(1,N_x);
intel.Sc_e              = Sc_e;                 % Schmidt number
intel.Re1_e0            = Re1_e;

options.mapOpts.x_start = 1e-5;
options.tol             = 1e-10;
options.it_max          = 300;
options.mapOpts.x_end   = intel.x_e(end);
options.mapOpts.N_x     = 500;
options.ox              = 2;
options.xSpacing        = xMappings{imap};

% options.plot_marching = true;
[intel,options] = DEKAF_BL(intel,intel_QSS,options);

all_intels(imap) = intel;
all_options(imap) = options;
end
%}
%% Plotting results

ColorFlag = {'KR','GB','Rbw'}; % color scheme for the profiles
Styles = {'-','-.','--'}; % line styles
Nprof = 10; % number of profiles we want to plot
idx = int16(linspace(1,options.mapOpts.N_x ,Nprof)); % what these indices will be in
x_plot = all_intels(2).x(1,idx); % x locations to be plotted
for imap=1:N_map
    all_x{imap} = all_intels(imap).x(1,:);
end
% [idx{1:N_map}] = find_closestND(Nprof,all_x{:});
ylims = [0,0.015];

clear plots legends
figure
itp = 0; % initializing counter
for imap=1:N_map
    for ix = 1:Nprof
%         idxLoop = idx{imap}(ix);
        [~,idxLoop] = min(abs(x_plot(ix)-all_x{imap}));
        itp = itp+1;
        legends{itp} = [xMappings{imap},' at x = ',num2str(all_intels(imap).x(1,idxLoop)),' m'];

        subplot(1,2,1);
        plots(itp) = plot(all_intels(imap).u(:,idxLoop),all_intels(imap).y(:,idxLoop),Styles{imap},'Color',two_colors(ix,Nprof,ColorFlag{imap}));
        xlabel('U [m/s]'); ylabel('y [m]'); grid minor; hold on;
        ylim(ylims);

        subplot(1,2,2);
        plot(all_intels(imap).T(:,idxLoop),all_intels(imap).y(:,idxLoop),Styles{imap},'Color',two_colors(ix,Nprof,ColorFlag{imap}));
        xlabel('T [K]'); ylabel('y [m]'); grid minor; hold on;
        ylim(ylims);
    end
end
legend(plots,legends);