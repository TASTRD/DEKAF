% Test_lnkb.m tests the evaluation of the backward reaction rates
% for various methods, 'RRHO' and 'Arrhenius', in a binary oxygen mixture
%
% Author: Ethan Beyak & Fernando Miro
% Date: January 2018

clear; close all;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

mixture = 'oxygen2Bortner';
Ti = 200;
Tf = 10000;
nT = 200;

%% Obtaining tabulated values
T = linspace(Ti,Tf,nT)';

% Define opts struct
opts.collisionRefExceptions.references = [];
opts.collisionRefExceptions.pairs = [];
mixCnstGY2 = getAll_constants(mixture,opts);
opts.modelEnergyModes = 'mutation';
opts.modelDiffusion = 'GuptaYos90_Klentzman';
opts.modelEquilibrium = 'RRHO';
opts.neglect_hElec = false;

optsGibbs = opts;
optsBort = opts;

optsGibbs.modelEquilibrium = 'RRHO';
optsBort.modelEquilibrium = 'Arrhenius';

mixCnstBort = getAll_constants(mixture,optsBort);
mixCnstGibbs = getAll_constants(mixture,optsBort);

%% Obtaining properties
% BORTNER EVALUATION
Keq_strucBort   = mixCnstBort.Keq_struc;

T_r = getReaction_T(ones(size(mixCnstGibbs.nT_r)),T);
lnkf_r = getReaction_lnkf(T_r,mixCnstBort.A_r ,mixCnstBort.nT_r ,mixCnstBort.theta_r ); % forward reaction rate
lnkb_r = getReaction_lnkf(T_r,Keq_strucBort.A_br,Keq_strucBort.nT_br,Keq_strucBort.theta_br); % backward reaction rate

% GIBBS EVALUATION
Keq_strucGibbs = mixCnstGibbs.Keq_struc;
nu_reac     = Keq_strucGibbs.nu_reac;
nu_prod     = Keq_strucGibbs.nu_prod;
delta_nu = nu_prod - nu_reac; % stoichiometric difference matrix

% Obtaining reaction rates
lnkf_rGibbs = getReaction_lnkf(T_r,mixCnstGibbs.A_r,mixCnstGibbs.nT_r,mixCnstGibbs.theta_r); % forward reaction rate
lnKeq_r = getReaction_lnKeq(T_r,T,T,T,T,T,delta_nu,Keq_strucGibbs,optsGibbs); % equilibrium constant
lnkb_rGibbs = lnkf_rGibbs - lnKeq_r; % backward reaction rate

%% Plotting
figure
LW = 2;
plot(T,lnkb_rGibbs(:,1),'linewidth',LW); hold on
plot(T,lnkb_r(:,1),'linewidth',LW);
grid minor
legend('Gibbs','Bortner','location','best')
xlabel('T [K]');
ylabel('ln k_{br} [S.I.]');
title(sprintf('Comparison of backward reaction rates: %s',mixture))

