% Test_edge_values_BL is a script to test the function build_edge_values_BL
% using the potential flow around a cylinder of radius R, with a
% cylindrical coordinate system centered on the center of the radius:
%
%     U_e(theta) = U_inf*(1-R^2/r^2) * sin(theta)
%
% which for curvilinear coordinates on the surface of this radius (r=R &
% x=theta*R) returns a streamwise velocity profile:
%
%     U_e(x) = 2*U_inf*sin(x/R)
%
% We asume an upstream velocity of 10m/s, a surface radius of 10m, and a
% constant temperature equal to 293K. We also asume a pressure at the first
% x location of 1e5 Pa

addpath(genpath('./DEKAF_miromiro/'));

clear all;

R = 10; % radius of 10m
x_e = linspace(0.1,0.9*pi*R,1000); % curvilinear surface coordinate
U_inf = 100; % upstream velocity

U_e = 2*U_inf*sin(x_e/R);
T_e = 293*ones(size(x_e));
p_e0 = 1e5;

intel.U_e = U_e;
intel.T_e = T_e;
intel.p_e0 = p_e0;
intel.x_e = x_e;
intel.W_0 = 0;

options.mu_ref = 1.716e-5;             % Reference viscosity of air, used in Sutherland's law      [m^2/s]
options.Su_mu  = 110.6;                % Sutherland's constant corresponding to viscosity          [K]
options.T_ref  = 273.15;               % Reference temperature of air, used in Sutherland's law    [K]
options.Pr   = 0.7;                  % Constant Prandtl number of air                            [-]
intel.cp     = 1004.5;               % Constant specific heat                                    [J/(kg K)]
intel.gam    = 1.4;                  % Constant specific heat ratio                              [-]

%% Calling script
options.flow = 'CPG'; % we take all defaults
intel = build_edge_values_BL(intel,options);

%% we plot the entire semicircle, and the section of the surface that is
% being studied
x_surf = -10:0.1:10;
y_surf = sqrt(R^2-x_surf.^2);
x_traj = R*cos(pi-x_e/R);
y_traj = R*sin(x_e/R);

lnwdth = 1;
figure
subplot(2,4,1)
plot(x_surf,y_surf,'b-',x_traj,y_traj,'r-') % plotting path
legend('surface','path');
daspect([1 1 1]); xlim([-1.1*R,1.1*R]); ylim([0,1.5*R]);

subplot(2,4,2)
plot(intel.x,intel.U_e,'.-','LineWidth',lnwdth);
xlabel('x'); xlabel('U_e(x)');

subplot(2,4,3)
plot(intel.x,intel.p_e,'.-','LineWidth',lnwdth);
xlabel('x'); xlabel('p_e(x)');

subplot(2,4,4)
plot(intel.x,intel.beta,'.-','LineWidth',lnwdth);
xlabel('x'); xlabel('\beta(x)');

subplot(2,4,6)
plot(intel.x,intel.U_e,'.-','LineWidth',lnwdth);
xlabel('\xi'); xlabel('U_e(\xi)');

subplot(2,4,7)
plot(intel.x,intel.p_e,'.-','LineWidth',lnwdth);
xlabel('\xi'); xlabel('p_e(\xi)');

subplot(2,4,8)
plot(intel.x,intel.beta,'.-','LineWidth',lnwdth);
xlabel('\xi'); xlabel('\beta(\xi)');

