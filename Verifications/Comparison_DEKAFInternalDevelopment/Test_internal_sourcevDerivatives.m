% Test_internal_sourcevDerivatives is a script to perform a test on the
% derivatives of the vibrational relaxation source term with respect to
% temperature
%
% Author: Fernando Miro Miro
% Date: December 2018

clear;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

options.mixture = 'air2';
options.numberOfTemp = '2T';
options.bParkCorrectionTauVib = false;
[mixCnst,options] = getAll_constants(options.mixture,options);

% defining temperature vectors
NT = 100;
Tmin = 300; % K
Tmax = 20e3; % K
T_vec = linspace(Tmin,Tmax,NT);
dT = mean(diff(T_vec));

NTv = 100;
Tvmin = 500; % K
Tvmax = 20e3; % K
% NTv = 1;
% Tvmin = 5000; % K
% Tvmax = 5000; % K
Tv_vec = linspace(Tvmin,Tvmax,NTv);
dTv = mean(diff(Tv_vec));

T_mat  = repmat(T_vec,  [NTv,1]);
Tv_mat = repmat(Tv_vec.',[1,NT]);
T   = T_mat(:);
Tv  = Tv_mat(:);
Ntot = NT*NTv;

% defining other inputs
y_s = repmat([0.22,0.78],[Ntot,1]);
p = repmat(4000,[Ntot,1]);
TTrans = T;
TVib = Tv;
Tel = Tv;
rho = getMixture_rho(y_s,p,TTrans,Tel,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron);
drho_dT = getMixtureDer_drho_dT(p,y_s,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,[TTrans,TVib],options.numberOfTemp);

% computing source terms
sourcev_vec = getMixture_sourcev(TTrans,TVib,Tel,y_s,p,rho,mixCnst.Mm_s,mixCnst.thetaVib_s,mixCnst.R_s,...
                            mixCnst.aVib_sl,mixCnst.bVib_sl,mixCnst.sigmaVib_s,mixCnst.bMol,mixCnst.bElectron,mixCnst.R0,...
                            mixCnst.nAvogadro,mixCnst.pAtm,options);
dsourcev_dT_vec = getMixtureDer_dsourcev_dT(TTrans,TVib,Tel,y_s,p,rho,mixCnst.pAtm,mixCnst.R0,...
                            mixCnst.nAvogadro,mixCnst.aVib_sl,mixCnst.bVib_sl,mixCnst.sigmaVib_s,mixCnst.bMol,mixCnst.Mm_s,...
                            mixCnst.R_s,mixCnst.bElectron,mixCnst.thetaVib_s,options.numberOfTemp,options);

sourcev      = reshape(sourcev_vec,         [NTv,NT]);
dsourcev_dT  = reshape(dsourcev_dT_vec(:,1),[NTv,NT]);
dsourcev_dTv = reshape(dsourcev_dT_vec(:,2),[NTv,NT]);
% computing FD derivatives
dsourcev_dT_FD  = diff(sourcev,1,2)/dT;
dsourcev_dTv_FD = diff(sourcev,1,1)/dTv;

% Same story but with the vibrational relaxation times
tauVibMW_s_vec  = getSpecies_tauVibMW(TTrans,y_s,p,mixCnst.Mm_s,mixCnst.aVib_sl,mixCnst.bVib_sl,mixCnst.bElectron,mixCnst.pAtm);
dtauVibMWs_dT_vec = getSpeciesDer_dtauVibMWs_dT(TTrans,y_s,p,mixCnst.Mm_s,mixCnst.aVib_sl,mixCnst.bVib_sl,mixCnst.bElectron,mixCnst.pAtm,options.numberOfTemp);

tauVibMW_s      = reshape(permute(tauVibMW_s_vec,   [1,3,2]),[NTv,NT,mixCnst.N_spec]);
dtauVibMWs_dT   = reshape(permute(dtauVibMWs_dT_vec(:,:,1),[1,3,2]),[NTv,NT,mixCnst.N_spec]);
dtauVibMWs_dTv  = reshape(permute(dtauVibMWs_dT_vec(:,:,2),[1,3,2]),[NTv,NT,mixCnst.N_spec]);
% computing FD derivatives
dtauVibMWs_dT_FD  = diff(tauVibMW_s,1,2)/dT;

%% plotting comparison
Nprof = 5;
idx_plt = round(linspace(1,NTv,Nprof+2)); idx_plt([1,end]) = [];
idx_plt2 = round(linspace(1,NT,Nprof+2)); idx_plt2([1,end]) = [];
pltF1 = 'vrds';
pltF2 = 'mgm';

%% plotting sourcev
%{%
figure
clear plots legends;
[Ni,Nj] = optimalPltSize(3); ic=1;
subplot(Ni,Nj,ic); ic=ic+1;
for ii=1:Nprof
    clr = two_colors(ii,Nprof+1,pltF1);
    plot(T_vec,sourcev(idx_plt(ii),:),'Color',clr); hold on;
    plot(Tv_vec(idx_plt(ii))*[1,1],[min(sourcev(:)),max(sourcev(:))],'--','Color',clr);
end
plot(T_vec,zeros(size(T_vec)),'k-');
xlabel('T [K]'); ylabel('\omega_V [J/s-m^3]');

subplot(Ni,Nj,ic); ic=ic+1;
for ii=1:Nprof
    plots(ii)         = plot(T_vec,         dsourcev_dT(idx_plt(ii),:),        'Color',two_colors(ii,Nprof+1,pltF1)); hold on;
    plots(Nprof + ii) = plot(T_vec(1:end-1),dsourcev_dT_FD(idx_plt(ii),:),'--','Color',two_colors(ii,Nprof+1,pltF2));
                        plot(T_vec(2:end),  dsourcev_dT_FD(idx_plt(ii),:),'--','Color',two_colors(ii,Nprof+1,pltF2));
    legends{ii}         = ['Tv=',num2str(Tv_vec(idx_plt(ii)),'%0.2f'),'K (analytical)'];
    legends{Nprof + ii} = ['Tv=',num2str(Tv_vec(idx_plt(ii)),'%0.2f'),'K (FD)'];
end
xlabel('T [K]'); ylabel('\partial \omega_V / \partial T [J/s-m^3-K]'); legend(plots,legends);
%}
%{%
subplot(Ni,Nj,ic); ic=ic+1;
for ii=1:Nprof
    plots(ii)         = plot(Tv_vec,         dsourcev_dTv(:,idx_plt2(ii)),        'Color',two_colors(ii,Nprof,pltF1)); hold on;
    plots(Nprof + ii) = plot(Tv_vec(1:end-1),dsourcev_dTv_FD(:,idx_plt2(ii)),'--','Color',two_colors(ii,Nprof,pltF2));
                        plot(Tv_vec(2:end),  dsourcev_dTv_FD(:,idx_plt2(ii)),'--','Color',two_colors(ii,Nprof,pltF2));
    legends{ii}         = ['T=',num2str(T_vec(idx_plt2(ii)),'%0.2f'),'K (analytical)'];
    legends{Nprof + ii} = ['T=',num2str(T_vec(idx_plt2(ii)),'%0.2f'),'K (FD)'];
end
xlabel('Tv [K]'); ylabel('\partial \omega_V / \partial Tv [J/s-m^3-K]'); legend(plots,legends);

%% surf plot
%{
figure
surf(T_mat,Tv_mat,sourcev);
xlabel('T [K]'); ylabel('Tv [K]'); zlabel('\omega_V [J/s-m^3]');
%}%

%% plotting rho
%{%
figure
semilogy(T_vec,drho_dT(:,1),'r-'); hold on;
semilogy(T_vec(1:end-1),diff(rho)/dT,'k--');
semilogy(T_vec(2:end),diff(rho)/dT,'k--');
xlabel('T [K]'); ylabel('\rho [kg/m^3]');
%}

%% ploting tau

figure
clear plots legends;
% [Ni,Nj] = optimalPltSize(2);
Ni = 2;
Nj = mixCnst.N_spec;
ic=1;
for s=1:mixCnst.N_spec
    subplot(Ni,Nj,ic); ic=ic+1;
    for ii=1:Nprof
        clr = two_colors(ii,Nprof+1,pltF1);
        semilogy(T_vec,tauVibMW_s(idx_plt(ii),:,s),'Color',clr); hold on;
        semilogy(Tv_vec(idx_plt(ii))*[1,1],[min(min(tauVibMW_s(:,:,s))),max(max(tauVibMW_s(:,:,s)))],'--','Color',clr);
    end
    plot(T_vec,zeros(size(T_vec)),'k-');
    xlabel('T [K]'); ylabel(['\tau^{MW}_{',num2str(mixCnst.spec_list{s}),'} [s]']);
end

for s=1:mixCnst.N_spec
    subplot(Ni,Nj,ic); ic=ic+1;
    for ii=1:Nprof
        plots(ii)         = semilogy(T_vec,         dtauVibMWs_dT(idx_plt(ii),:,s),        'Color',two_colors(ii,Nprof+1,pltF1)); hold on;
        plots(Nprof + ii) = semilogy(T_vec(1:end-1),dtauVibMWs_dT_FD(idx_plt(ii),:,s),'--','Color',two_colors(ii,Nprof+1,pltF2));
        semilogy(T_vec(2:end),  dtauVibMWs_dT_FD(idx_plt(ii),:,s),'--','Color',two_colors(ii,Nprof+1,pltF2));
        legends{ii}         = ['Tv=',num2str(Tv_vec(idx_plt(ii)),'%0.2f'),'K (analytical)'];
        legends{Nprof + ii} = ['Tv=',num2str(Tv_vec(idx_plt(ii)),'%0.2f'),'K (FD)'];
    end
    xlabel('T [K]'); ylabel(['\partial \tau^{MW}_{',num2str(mixCnst.spec_list{s}),'} / \partial T [s/K]']);
    if s==1
        legend(plots,legends);
    end
end