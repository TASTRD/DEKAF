% Test_LTEConicalShock is a script to verify the implementation of the LTE
% conical shock relations
%
% References:
%{%
clear;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']));
savepath = 'conicalShock_comparison.mat';

% Inputs
%{%
opts.mixture = 'air5mutation';
T_vec = [   288.15, 227.13, 242.762];    % [K]
p_vec = [101325,   1090.16,  17.7603];   % [Pa]
h_vec = [     0,    100,    200];        % [kft]
y_s1 = [1e-10,1e-10,1e-10,0.767-3e-10,0.233];
theta = 10; % deg

% Obtaining mixture constants, lengths etc.
opts.T_tol = 1e-14;
opts.shock_tol = 1e-13;
[mixCnst,opts] = getAll_constants(opts.mixture,opts);
[mixCnst.LTEtablePath,mixCnst.XEq] = checkCompositionLTE({0,0,0,0.767,0.233},mixCnst.Mm_s,mixCnst.R0,opts.mixture,opts);
mixCnst.LTEtableData = load(mixCnst.LTEtablePath);
X_E1 = mixCnst.XEq;

N_spec = mixCnst.N_spec;
%}
%% Mach number sweeping
%{%
Msin_vec = [2,logspace(log10(5),log10(30),10)];
for jj=length(p_vec):-1:1
    % Obtaining preshock gas properties
    T1 = T_vec(jj);
    p1 = p_vec(jj);
    [~,R] = getMixture_Mm_R(y_s1,T1,T1,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron);
    gam = getMixture_gam(T1,T1,T1,T1,T1,y_s1,mixCnst.R_s,mixCnst.nAtoms_s,...
        mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,opts);

    % Obtaining velocity
    for ii=length(Msin_vec):-1:1
        M1 = Msin_vec(ii)/sind(theta);
        u1 = M1*sqrt(T1*287*1.4);

        [M2C, betaC, Twall_C, pwall_C, ~, converged]=TaylorMaccoll(theta, M1, gam, false);                     % CPG conical shock
        T2C = Twall_C*T1; p2C = pwall_C*p1; u2C = M2C*sqrt(gam*R*T2C);
        if ~(jj==1 && ii>=8)                                                                        % we cannot compute Msin > 13.5 at sea-level, because the post-shock pressures become of the order of 40MPa
        [T2T,p2T,u2T,y_s2T,betaT] = getHighEnth_ConicalShock_Props('TPG1T',u1,T1,p1,y_s1,mixCnst,theta,opts,betaC);  % TPG conical shock
        [T2, p2, u2, y_s2, beta ] = getHighEnth_ConicalShock_Props('LTE',  u1,T1,p1,X_E1,mixCnst,theta,opts,betaT);  % LTE conical shock

        T21(ii,jj) = T2/T1;
        T21T(ii,jj) = T2T/T1;
        T21C(ii,jj) = T2C/T1;
        end
    end
end

save(savepath,'T21','T21T','T21C','Msin_vec','theta','h_vec');
%}
%% plotting comparison
% load(savepath);
Tratio_CPG = load('Tratio_CPG.csv');
Tratio_LTE0 = load('Tratio_LTE_0ft.csv');
Tratio_LTE100 = load('Tratio_LTE_100kft.csv');
Tratio_LTE200 = load('Tratio_LTE_200kft.csv');
mark_size = 10;
clrs = 'Rbw2';
Nh = length(h_vec);
Mplot = true; % if we want to plot just Mach or Mach times sind(theta)
if Mplot
    scl = sind(theta);  % we scale the Msin_vec list with sind(theta)
else
    scl = 1;            % no scaling
end

figure
loglog(Tratio_CPG(:,1)/scl,   Tratio_CPG(:,2),'k-','displayname','CPG anderson'); hold on;
loglog(Tratio_LTE0(:,1)/scl,  Tratio_LTE0(:,2),  '-.',                'Color',two_colors(1,Nh,clrs),'displayname','LTE anderson 0ft');
loglog(Tratio_LTE100(:,1)/scl,Tratio_LTE100(:,2),'--',                'Color',two_colors(2,Nh,clrs),'displayname','LTE anderson 100kft');
loglog(Tratio_LTE200(:,1)/scl,Tratio_LTE200(:,2),':','linewidth',1.5, 'Color',two_colors(3,Nh,clrs),'displayname','LTE anderson 200kft');
loglog(Msin_vec/scl,T21C(:,end),'ks','markersize',mark_size,'displayname','CPG DEKAF');
for jj=1:Nh
    loglog(Msin_vec/scl,T21T(:,jj),'^','Color',two_colors(jj,Nh,clrs),'markersize',mark_size,'displayname',['TPG DEKAF ',num2str(h_vec(jj)),'kft']);
    loglog(Msin_vec/scl,T21(:,jj) ,'o','Color',two_colors(jj,Nh,clrs),'markersize',mark_size,'displayname',['LTE DEKAF ',num2str(h_vec(jj)),'kft']);
end
if Mplot
xlabel('M_\infty [-]');
else
xlabel('M_\infty sin(\theta_c) [-]');
end
ylabel('T_c/T_\infty'); lgnd = legend(); set(lgnd,'location','northwest');
