% Test_LTEShock is a script to verify the implementation of the LTE oblique
% shock relations
%
% References:
% Moeckel 1957. Oblique-Shock Relations at Hypersonic Speeds for Air in
% Chemi- cal Equilibrium,” NACA TN 3895

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']));

%{
% Inputs
opts.mixture = 'air5mutation';
T1 = 227.13; % K at 100 000 ft
p1 = 1090.16; % Pa at 100 000 ft
ft2m = 0.3048; % feet to meters
theta_lims = [40,50,54,57,60]; % deg
Ntheta = 100;
u1_vec = (5000:5000:25000)*ft2m;

% Obtaining mixture constants, lengths etc.
opts.T_tol = 1e-14;
[mixCnst,opts] = getAll_constants(opts.mixture,opts);
[mixCnst.LTEtablePath,mixCnst.XEq] = checkCompositionLTE({0,0,0,0.767,0.232},mixCnst.Mm_s,mixCnst.R0,opts.mixture,opts);
mixCnst.LTEtableData = load(mixCnst.LTEtablePath);
Nu = length(u1_vec);
N_spec = mixCnst.N_spec;

% building theta matrix
theta_mat = zeros(Ntheta,Nu);
for ii=1:Nu % we make it go up to the limit twice and then back down
    theta_mat(:,ii) = [linspace(1,theta_lims(ii),Ntheta/2),linspace(1,theta_lims(ii),Ntheta/2)];
end

% allocating
p_mat = zeros(Ntheta,Nu);
u_mat = zeros(Ntheta,Nu);
T_mat = zeros(Ntheta,Nu);
beta_mat = zeros(Ntheta,Nu);
ys_mat = zeros(Ntheta,Nu,N_spec);
%%
for jj=1%:Nu
    for ii=1:Ntheta
        if ii==1 || (ii<=Ntheta/2 && (beta_mat(ii-1,jj)>100 || beta_mat(ii-1,jj)<0))
            beta0 = 1.2*theta_mat(ii,jj); % weak shock
        elseif ii==Ntheta/2+1 || (ii>Ntheta/2 && (beta_mat(ii-1,jj)>100 || beta_mat(ii-1,jj)<0))
            beta0 = 89 - 0.1*theta; % strong shock
        else
            beta0 = beta_mat(ii-1,jj);
        end
        [T2,p2,u2,y_s2,beta] = getLTEShock_Props(u1_vec(jj),T1,p1,mixCnst,theta_mat(ii,jj),opts,beta0);
        p_mat(ii,jj) = p2;
        T_mat(ii,jj) = T2;
        u_mat(ii,jj) = u2;
        beta_mat(ii,jj) = beta;
        ys_mat(ii,jj,:) = permute(y_s2,[3,1,2]); % (1,s) --> (1,1,s)
        disp(['Completed ',num2str(((jj-1)*Ntheta+ii)/(Nu*Ntheta)*100),'%']);
    end
end

save('DEKAF_LTEshock.mat','theta_mat','beta_mat','u_mat','T_mat','p_mat','u1_vec');
%}
%% Plotting
load('DEKAF_LTEshock.mat');
[Ntheta,Nu] = size(beta_mat);
ft2m = 0.3048; % feet to meters
ic = 1;
Moeckel = load('v1_5000ftps.csv');
Moeckel = [Moeckel;load('v1_10000ftps.csv')];
Moeckel = [Moeckel;load('v1_15000ftps.csv')];
Moeckel = [Moeckel;load('v1_20000ftps.csv')];
Moeckel = [Moeckel;load('v1_25000ftps.csv')];
idx4plt = [1:(Ntheta/2),Ntheta:-1:(Ntheta/2+1)]; % order in which we want to plot them (weak shock and then strong shock)

clear plots legends
figure
for jj=1:Nu
    plots(jj) = plot(theta_mat(idx4plt,jj),beta_mat(idx4plt,jj),'Color',color_rainbow(jj,Nu)); hold on;
    legends{jj} = ['u = ',num2str(u1_vec(jj)/ft2m),' [ft/s]'];
end
plots(Nu+1) = plot(Moeckel(:,1),Moeckel(:,2),'ks');
legends{Nu+1} = 'Moeckel 1957';
legend(plots,legends);
xlabel('\theta [deg]');
ylabel('\beta [deg]');
ylim([0,100]);xlim([0,65]);