DEKAF developers manual
=======================

The DEKAF flow solver is distributed under a GNU Lesser General
Public License 3.0

DEKAF is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU Lesser General Public License for more details
http://www.gnu.org/licenses.

A short description of its capabilities can be found in the [readme file](README.md).

Commits
--------------

Commits should be treated as an addition/fix of a single part of the code. Care should be taken to not mix multiple additions/fixes into a single commit, as this tends to make `git bisect` less useful while also obscuring the history of the repository.

Commit messages should start with the author's initials, followed by the general subject of the commit, and, if necessary, a bullet list of the changes it includes. If it is a part of a larger set of commits taking care of a given task, numbered identifiers can be addded. An empty line should separate the first line of the commit and any additional comments below to allow use of `git log --one-line`. Additionally, if the commit addresses a given issue on DEKAF's Gitlab, include the issue number in the commit subject for traceability.

Example:
```
FMM: Issue #39: towards a vib-elec-el equation with the electron-pressure terms It. 1

0. blabla
1. some more blabla
   1.1. which includes one sub-modification
   1.2. and another one
```


Merging to master
-----------------

The master branch must always remain clean, meaning that the regression test must pass.

When a user makes a merge request, they should attach to the request a proof that the regression test has passed after the included changes.

Function structure
------------------

There are a few style considerations to satisfy when writing a function:

1. All functions must start with a documentation including:
   1. A short (and concise!) description of its task.
   1. The various usages. If a set of inputs/outputs is repeated between usages, three points can be used to avoid overly charged documentations.
   1. A description of the inputs and outputs, starting with the type of variable that each of them is
   1. The GNU LGPL disclaimer.
1. The body of the function should contain comments, whether they are in-line or preceding the line of code itself. If they are in-line, take extra effort to tab the comments to follow the tabbing of the code itself.
1. Functions from `SourceCode/Utilities/DataHandling/` should be used whenever possible to handle varargins and similar. Some useful ones (as of 2020-01-28) are:
   * `find_flagAndRemove`: returns a boolean for whether a flag is found or not
   * `parse_optional_input`: looks for a `...,flag,value,...` pair and sets a default for the value if it is not found.
   * `standardvalue`, `neededvalue` and `protectedvalue`: allow to handle inputs in the intel or options structure, depending on their nature. They are contained in the `SourceCode/Defaults` folder.
1. Simple conditionals or switches, followed by a one-line evaluation can be done in line. Any conditional or switch that contains an evaluation greater than one line should be written on distinct lines.
1. Simple counter increases (`ic=ic+1`) are also preferred in line.
1. Error messages due to invalid inputs or values should always include the non-supported value in question.
1. Trailing whitespace should be omitted so that `git` doesn't yell at us. Note that MATLAB's "Smart Indentation" (ctrl-I shortcut) does automatically add trailing whitespace on empty lines within a construct with leading whitespace. This is tolerated, as it's MATLAB's convention.
1. All functions should finish with an end.

An example of a function structure would be:
```
function [out1,out2] = func(in1,in2,in3,varargin)
% func has a very concise task.
%
% Usage:
%   (1)
%       [out1,out2] = func(in1,in2,in3)
%       |--> returns the result of some operations with in1, in2 and in3
%
%   (2)
%       [...] = func(in1,in2,in3,...,inN)
%       |--> returns the result of the same operations, but on additional
%       inputs
%
%   (3)
%       [...] = func(...,flag1,...,flagN)
%       |--> allows to pass additional flags conditioning the operations
%
% Inputs and outputs:
%   in1, ..., inN
%       vectors with the various inputs onto which the operations must be
%       done
%   flag1, ..., flagN
%       string identifying the various flags that can be passed to do
%       different things.
%   out1, out2
%       vectors containing the result of the various operations
%
% Author: Name and surname
% Date: October 2019
%--------------------------------------------------------------------------
% GNU Lesser General Public License 3.0
%
% This file is part of the DEKAF flow solver.
%
% DEKAF is distributed in the hope that it will be useful, but WITHOUT ANY
% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
% FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
% more details http://www.gnu.org/licenses.
%--------------------------------------------------------------------------

[~,bWhatever] = find_flagAndRemove('whatever',varargin);                    % checking for flags

out1 = in1+in2;                                                             % performing some operation on the inputs
for ii=1:in3                                                                % looping through in3
    out1=out1*in1;                                                              % performing another operation
    if bWhatever;       out2 = out1;                                            % some simple conditional, evaluated in-line
    else;               out2 = sqrt(out1);                                      % second option of the conditional
    end
    switch in1                                                                  % depending on the value of in1
        case 1;         out1=out1*0.1;                                              % we do this
        case 2;         out1=out1*0.01;                                             % ... or this
        case 3;         out1=out1*0.001;                                            % ... or this
        case 4;
            J2 = besselj(2,1i);                                                     % ... or for a more complication operation,
            out1 = J2^out2;                                                         % we split it onto distinct lines of code.
        otherwise;      error(['Error: in1=',num2str(in1),' is not supported']);    % ... or break
    end
end

end % func
```


Test-driven development
-----------------------

The development of new functions must be preceded by writing the unit test that is to evaluate its correct regression. This is what is known as [test-driven development](https://en.wikipedia.org/wiki/Test-driven_development).

Examples of such unit tests can be found in `SourceCode/UnitTests/`.


General code structure considerations
-------------------------------------

All option default definitions are made in `SourceCode/Defaults/setDefaults.m`, which also keeps the documentation of the various options available.

Similarly, whatever variables are needed through the intel structure must be properly documented in the central `DEKAF` function.

