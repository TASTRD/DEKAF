function [q,dq_deta,dq_deta2] = update_wallNeumann(q,dq_deta,dq_deta2,eta,dq_deta_wall)
% update_wallNeumann re-enforces a Neumann condition at the wall.
%
% A Neumann condition is that where we impose:
%       dq_deta(eta=eta_wall) = dq_deta_wall
%
% It subtracts the jump in mass fraction gradient at the wall caused by
% numerical artifacts (maybe).
%
% Usage:
%   (1) [ys{s},dys_deta{s},dys_deta2{s}] = update_wallNeumann(ys{s},dys_deta{s},dys_deta2{s},eta,0)
%   |---> imposes a non-catalytic wall condition on the mass fractions of
%   species s
%
%   (2) [g,dg_deta,dg_deta2] = update_wallNeumann(g,dg_deta,dg_deta2,eta,HF_w)
%   |---> imposes a constant wall heat flux condition
%
% See also: listOfVariablesExplained, update_g_isoThermalWall
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

a = 2; % HARCODE ALERT!
% gives a good decay such that at eta=6 (typical end of the BL) the
% distortion of the fields is <1e-5 times the introduced delta
delta_dysdeta_wall = dq_deta_wall - dq_deta(end);
dq_deta     = dq_deta   +       delta_dysdeta_wall*exp(-a*eta);
q           = q         - 1/a * delta_dysdeta_wall*exp(-a*eta);
dq_deta2    = dq_deta2  - a   * delta_dysdeta_wall*exp(-a*eta);
