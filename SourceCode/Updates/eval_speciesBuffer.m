function ys2 = eval_speciesBuffer(ys0,varargin)
% eval_speciesBuffer modifies the species mass fractions by creating a
% logarithmic buffer region.
%
% Usage:
%   (1)
%       ys2 = eval_speciesBuffer(ys0)
%       |--> assumes the two defining points of the buffer to be 1e-10 and
%       1e-30
%
%   (2)
%       ys2 = eval_speciesBuffer(ys0,ysInt)
%       |--> allows to fix the intermediate point of the buffer (the point
%       below which it will start to act - 1e-10 by default) and assumes
%       the minimum point of the buffer to be 1e-30
%
%   (3)
%       ys2 = eval_speciesBuffer(ys0,ysInt,ysMin)
%       |--> allows to fix both the intermediate and the minimum point of
%       the buffer
%
% It essentially does the following transformations:
%
%   ys0  -->   ys1   -->   Ys1       -->       Ys2        -->    ys2
%
%   _ 1        _ 1e0       _ 0                 _ 0               _ 1
%   |          |           |                   |                 |
%   |          |           |                   |                 |
%   + ysInt    + ysInt     + log10(ysInt)      + log10(ysInt)    + ysInt
%   |          |           |                   |                 |
%   |          |           |                   |                 |
%   |          - ysMin     - log10(ysMin)      |                 |
%   + 0  -->          -->               -->    |            -->  |
%   |          - -ysMin    - log10(ysMin)      |                 |
%   |          |           |                   |                 |
%   |          |           |                   |                 |
%   |          |           |                   |                 |
%   |          |           |                   |                 |
%   - -1       - -1e0      - 2*log10(ysMin)    - log10(ysMin)    - ysMin
%
% Examples:
%   (1) With default options: ys2 = eval_speciesBuffer(ys0)
%   ys0 = {[0.5,0.3,0.1,1e-10,1e-15,-1e-50,-1e-20,-0.4].'}
%   ys1 = {[0.5,0.3,0.1,1e-10,1e-15,-1e-30,-1e-20,-0.4].'}
%   Ys1 = {[-0.301029995663981
%           -0.522878745280338
%           -1.000000000000000
%          -10.000000000000000
%          -15.000000000000000
%          -30.000000000000000
%          -40.000000000000000
%          -59.602059991327963]}
%   Ys2 = {[-0.301029995663981
%           -0.522878745280338
%           -1.000000000000000
%          -10.000000000000000
%          -12.000000000000000
%          -18.000000000000000
%          -22.000000000000000
%          -29.840823996531185]}
%   ys2 = {[0.5,0.3,0.1,1e-10,1e-12,1e-18,1e-22,1.4427e-30].'}
%
% Inputs and outputs:
%   ys0 and ys2
%       cell of mass fractions with the initial and final mass fractions
%       before and after applying the buffer
%   ysInt
%       intermediate point of the buffer region. Any mass fraction above
%       this value will remain untouched
%   ysMin
%       minimum limit of the buffer region. Any mass fraction whose
%       absolute value is below this limit will be assumed equal to it.
%       This is used to ensure that for mass fractions around 0 the
%       logarithm doesn't tend to -Inf, which would break the purpose of
%       the buffer region
%
% Author(s): Fernando Miro Miro
% Date: December 2018
% GNU Lesser General Public License 3.0

% analyzing inputs
switch length(varargin)
    case 0
        ysInt = 1e-10;
        ysMin = 1e-30;
    case 1
        ysInt = varargin{1};
        ysMin = 1e-30;
    case 2
        ysInt = varargin{1};
        ysMin = varargin{2};
    otherwise
        error(['the number of inputs (',num2str(nargin),') is not supported']);
end
lgInt = log10(ysInt);
lgMin = log10(ysMin);

% applying buffer
for s=length(ys0):-1:1 % looping species
    ys1{s} = ys0{s};
    ys1{s}(ys1{s}<ysMin  & ys1{s}>=0) =  ysMin;     % cutting positive values
    ys1{s}(ys1{s}>-ysMin & ys1{s}<=0) = -ysMin;     % cutting negative values
    Ys1{s} = log10(ys1{s});                         % obtaining logarithm of positive values
    Ys1{s}(ys1{s}<0) = 2*lgMin - log10(-ys1{s}(ys1{s}<0)); % obtaining logarithm of negative values and moving them to be in the [-30,-60] range
    Ys2{s} = Ys1{s};
    Ys2{s}(Ys2{s}<lgInt) = lgInt + (lgMin-lgInt)/(2*lgMin-lgInt) * (Ys1{s}(Ys1{s}<lgInt) - lgInt); % applying buffer to the logarithms bellow the interrmediate point
    ys2{s} = 10.^(Ys2{s});                           % transforming back to non-logarithmic values
end
