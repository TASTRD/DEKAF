function [U_e,W_0] = getVelocity_U_W(U_e,U_e0,Lambda_sweep)
%GETVELOCITY_U_W Update velocities U and W by accounting for constant sweep
% of the geometry.
%
% Usage:
%   [U_e,W_0] = getVelocity_U_W(U_e,U_e0,Lambda_sweep);
%
% Inputs and Outputs
%   U_e     N_eta x 1
%   U_e0    1 x 1
%   Lambda_sweep    1 x 1   (deg)
%
%   U_e     N_eta x 1
%   W_0     1 x 1
%
% Date: 29 March 2018
% GNU Lesser General Public License 3.0

U_e = U_e * cosd(Lambda_sweep);
W_0 = sind(Lambda_sweep) * U_e0;

end % getVelocity_U_W

