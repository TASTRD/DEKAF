function [intel,options] = update_solution(intel,options,varargin)
% update_solution(intel,options) updates the previous base solution with
% the perturbation terms obtained in the previous run. intel is a structure
% containing everything, such as:
%     .f, df_deta, etc: vectors with the base solutions for each of the
%      system variables. These will be updated.
%     .sol_hat: vector with the perturbation solutions.
%     .D1: first derivation matrix with respect to eta
%     .D2: second derivation matrix with respect to eta
% options is a structure that allows to fix several options (complete list
% in setDefaults). The options used in here are:
% --> perturbRelaxation, N, flow, Cooke, dxi, thermal_BC, G_bc, ys_lim
%
% the options structure is also outputted, updating the value of
% options.G_bc if options.thermal_BC was chosen together with a
% chemically-reacting flow assumption. This is done in eval_thermo.
%
% update_solution(intel,options,intelp) will also update the xi derivatives
%
% The output will be the same structure with the updated fields
%
% see also: DEKAF_QSS, DEKAF_BL, setDefaults, listOfVariablesExplained,
% eval_thermo
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

% checking inputs
if nargin==3
    intelp = varargin{1};
end

% unloading options and fields from intel
N  = options.N;
if options.perturbRelaxation==0                                                     % we use the annonymous function passed by the user
    perturbRelaxation = options.relaxFactFunction(intel.conv,intel.it,intel.i_xi);
    intel.dynamicRelaxFact = perturbRelaxation;                                         % storing it for the display message
else                                                                                % we just take the constant value of perturbRelaxation
    perturbRelaxation  = options.perturbRelaxation;
end
D1 = intel.D1;
D2 = intel.D2;

% updating hatted variables
ic = 1; % initializing index counter
intel.dfh_deta      = intel.sol_hat((ic-1)*N+1:ic*N); ic=ic+1;
intel.fh            = intel.sol_hat((ic-1)*N+1:ic*N); ic=ic+1;
if options.Cooke
    intel.kh        = intel.sol_hat((ic-1)*N+1:ic*N); ic=ic+1;
end
intel.gh            = intel.sol_hat((ic-1)*N+1:ic*N); ic=ic+1;
if ismember(options.modelTurbulence,{'SSTkomega'})
    intel.KTh       = intel.sol_hat((ic-1)*N+1:ic*N); ic=ic+1;
    intel.WTh       = intel.sol_hat((ic-1)*N+1:ic*N); ic=ic+1;
end
if ismember(options.flow,{'TPGD','CNE'})
    for s=1:intel.mixCnst.N_spec
        intel.ysh{s}= intel.sol_hat((ic-1)*N+1:ic*N); ic=ic+1;
    end
elseif ismember(options.flow,{'LTEED'})
    for E=1:intel.mixCnst.N_elem
        intel.yEh{E}= intel.sol_hat((ic-1)*N+1:ic*N); ic=ic+1;
    end
end
if strcmp(options.numberOfTemp,'2T')
    intel.tauvh       = intel.sol_hat((ic-1)*N+1:ic*N); %ic=ic+1;
end

intel.df_deta3  = intel.df_deta3 + perturbRelaxation*D2*intel.dfh_deta;
intel.df_deta2  = intel.df_deta2 + perturbRelaxation*D1*intel.dfh_deta;
intel.df_deta   = intel.df_deta  +    perturbRelaxation*intel.dfh_deta;
intel.f         = intel.f        +    perturbRelaxation*intel.fh;

intel.dg_deta2  = intel.dg_deta2 + perturbRelaxation*D2*intel.gh;
intel.dg_deta   = intel.dg_deta  + perturbRelaxation*D1*intel.gh;
intel.g         = intel.g        +    perturbRelaxation*intel.gh;
if options.Cooke
    intel.dk_deta2  = intel.dk_deta2 + perturbRelaxation*D2*intel.kh;
    intel.dk_deta   = intel.dk_deta  + perturbRelaxation*D1*intel.kh;
    intel.k         = intel.k        +    perturbRelaxation*intel.kh;
end
%%%% TURBULENT TERMS
if ismember(options.modelTurbulence,{'SSTkomega'})
    intel.dKT_deta2  = intel.dKT_deta2 + perturbRelaxation*D2*intel.KTh;
    intel.dKT_deta   = intel.dKT_deta  + perturbRelaxation*D1*intel.KTh;
    intel.KT         = intel.KT        +    perturbRelaxation*intel.KTh;
    intel.dWT_deta2  = intel.dWT_deta2 + perturbRelaxation*D2*intel.WTh;
    intel.dWT_deta   = intel.dWT_deta  + perturbRelaxation*D1*intel.WTh;
    intel.WT         = intel.WT        +    perturbRelaxation*intel.WTh;
end
%%%% MULTI-SPECIES/ELEMENTS
if ismember(options.flow,{'TPGD','CNE'})
    for s=1:intel.mixCnst.N_spec
        intel.ys{s}         = intel.ys{s}        +    perturbRelaxation*intel.ysh{s};
    end
    if ismember(options.wallYs_BC,{'ysWall'})
        intel.ys = update_ys_fullCatalyticWall(options.Ys_bc,intel.ys,[],[],intel.eta); % we only want to update ys (for now)
    end
    y_s = cell1D2matrix2D(intel.ys);
    if options.bVariableIdxBath             % the user selected a variable bath index
        [~,idx_bath] = max(y_s,[],2);               % species with the highest concentration at each station
    else                                    % the user selected a fixed bath index
        idx_bath = intel.mixCnst.idx_bath;
    end
    intel.ys = matrix2D2cell1D(limit_XYs(y_s,options.ys_lim,idx_bath));   % setting species limit without species buffer
    for s=1:intel.mixCnst.N_spec
        intel.dys_deta{s}   = D1*intel.ys{s};
        intel.dys_deta2{s}  = D2*intel.ys{s};
    end
elseif ismember(options.flow,{'LTEED'})
    for E=1:intel.mixCnst.N_elem
        intel.yE{E}         = intel.yE{E}        +    perturbRelaxation*intel.yEh{E};
    end
    if ismember(options.wallYE_BC,{'yEWall'})
        intel.yE = update_ys_fullCatalyticWall(options.YE_bc,intel.yE,[],[],intel.eta); % we only want to update yE (for now)
    end
%     intel.yE = matrix2D2cell1D(limit_XYs(cell1D2matrix2D(intel.yE),options.ys_lim));    % setting species limit without species buffer
    for E=1:intel.mixCnst.N_elem
        intel.dyE_deta{E}   = D1*intel.yE{E};
        intel.dyE_deta2{E}  = D2*intel.yE{E};
    end
end
%%%% MULTI-TEMPERATURE
if strcmp(options.numberOfTemp,'2T')
    intel.tauv        = intel.tauv        +    perturbRelaxation*intel.tauvh;
    intel.dtauv_deta  = D1*intel.tauv;
    intel.dtauv_deta2 = D2*intel.tauv;
end

% Calculating xi derivatives
if options.marchYesNo
    FDc = intel.FDc_xi;
    intel.df_dxi     = ([intel.f       flip(intelp.f,2)      ]*FDc'); % computing streamwise derivatives
    intel.dg_dxi     = ([intel.g       flip(intelp.g,2)      ]*FDc');
    intel.df_detadxi = ([intel.df_deta flip(intelp.df_deta,2)]*FDc');
    if options.Cooke
        intel.dk_dxi = ([intel.k       flip(intelp.k,2)      ]*FDc');
    end
    if ismember(options.modelTurbulence,{'SSTkomega'})
        intel.dKT_dxi = ([intel.KT     flip(intelp.KT,2)     ]*FDc');
        intel.dWT_dxi = ([intel.WT     flip(intelp.WT,2)     ]*FDc');
    end
    if ismember(options.flow,{'TPGD','CNE'})
        intel.dys_dxi{intel.mixCnst.N_spec} = []; % allocating
        for s=1:intel.mixCnst.N_spec
            intel.dys_dxi{s} = ([intel.ys{s} flip(intelp.ys{s},2)]*FDc');
        end
    elseif ismember(options.flow,{'LTEED'})
        intel.dyE_dxi{intel.mixCnst.N_elem} = []; % allocating
        for E=1:intel.mixCnst.N_elem
            intel.dyE_dxi{E} = ([intel.yE{E} flip(intelp.yE{E},2)]*FDc');
        end
    end
    if strcmp(options.numberOfTemp,'2T')
        intel.dtauv_dxi = ([intel.tauv       flip(intelp.tauv,2)      ]*FDc');
        if ismember(options.flow,{'TPGD','CNE'})
            for s=intel.mixCnst.N_spec:-1:1
                intel.dzetas_dxi{s} = ([intel.dzetas_dxi{s}       flip(intelp.dzetas_dxi{s},2)      ]*FDc');
            end
        end
    end
end

% Re-enforcing Neumann conditions
%if strcmp(options.thermal_BC,'H_F')                                         % condition on the heat flux
%    [intel.g,intel.dg_deta,intel.dg_deta2] = update_wallNeumann(...
%        intel.g,intel.dg_deta,intel.dg_deta2,intel.eta,options.G_bc);
%    if strcmp(options.numberOfTemp,'2T')
%        % FIXME: adiabatic wall condition on a two-temperature model must
%        % be revisited
%        [intel.tauv,intel.dtauv_deta,intel.dtauv_deta2] = update_wallNeumann(...
%            intel.tauv,intel.dtauv_deta,intel.dtauv_deta2,intel.eta,options.G_bc);
%    end
%end
%{
if ismember(options.flow,{'CNE'})
    for s=1:intel.mixCnst.N_spec                                            % non-catalytic condition
        [intel.ys{s},intel.dys_deta{s},intel.dys_deta2{s}] = update_wallNeumann(...
            intel.ys{s},intel.dys_deta{s},intel.dys_deta2{s},intel.eta,0);
    end
end
%}
% evaluating coefficients
[intel,options] = eval_thermo(intel,options);

end % update_solution
