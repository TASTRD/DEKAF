function [inteln,options] = update_edge_values_BL(inteln,intel,options,i_xi)
% UPDATE_EDGE_VALUES_BL(intel,options) updates the boundary-edge values for
% the specific xi position being run identified by i_xi. inteln is the
% structure with the fields of the current iteration, whereas intel is the
% original structure with the edge vectors mu_e, rho_e, etc.
%
% The values will be updated depending on the flow condition present in
% options.flow.
%
% When the edge conditions change with xi, update_edge_values_BL(...) also
% updates the initial guess used for the boundary-layer profiles.

% If the edge conditions don't change, they can simply be taken as the ones
% at the previous location. For stream-wise varying edge conditions, one
% must scale the initial guess accordingly. Not doing so would mean that
% the edge boundary conditions, which are imposed on the fluctuation
% "hatted" variables (gh, fh, etc.) would not change. This is a constraint
% of the linearised resolution method, which requires the profiles used as
% initial guesses to satisfy the boundary conditions.
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

% actual edge quantities
inteln.i_xi         = i_xi;
cols_ixi            = getIdxBackwardDiff(i_xi,options.ox);      % columns within D1_xi that we're interested in
inteln.FDc_xi       = flip(intel.D1_xi(i_xi,cols_ixi));         % FD stencil to be used at the particular xi position
inteln.xi           = intel.xi(i_xi);
inteln.x            = intel.x(i_xi);            % if another coordsys is used (like the conical) this is the transformed x (xPE)
inteln.xPhys        = intel.xPhys(i_xi);
inteln.Theta        = intel.Theta(i_xi);
inteln.theta        = intel.theta(i_xi);
inteln.beta         = intel.beta(i_xi);
inteln.Ecu          = intel.Ecu(i_xi);
inteln.Ecw          = intel.Ecw(i_xi);
inteln.Ecp          = intel.Ecp(i_xi);
inteln.U_e          = intel.U_e(i_xi);
inteln.W_0          = intel.W_0;
inteln.T_e          = intel.T_e(i_xi);
inteln.h_e          = intel.h_e(i_xi);
inteln.H_e          = intel.H_e(i_xi);
inteln.p_e          = intel.p_e(i_xi);
inteln.rho_e        = intel.rho_e(i_xi);
inteln.mu_e         = intel.mu_e(i_xi);
inteln.kappa_e      = intel.kappa_e(i_xi);
inteln.R_e          = intel.R_e(i_xi);
if strcmp(options.flow,'CPG')
    inteln.cp_e     = intel.cp_e(i_xi);
end
for s=1:intel.mixCnst.N_spec
    inteln.ys_e{s}  = intel.ys_e{s}(i_xi);
end
for E=1:intel.mixCnst.N_elem
    inteln.yE_e{E}  = intel.yE_e{E}(i_xi);
end
if strcmp(options.wallYs_BC,'ysWall')
    for s=1:intel.mixCnst.N_spec
        options.Ys_bc{s} = options.trackerYsWall{s}(i_xi);
    end
elseif strcmp(options.wallYE_BC,'yEWall')
    for E=1:intel.mixCnst.N_elem
        options.YE_bc{E} = options.trackerYEWall{E}(i_xi);
    end
end
if strcmp(options.modelDiffusion,'cstSc')
    inteln.Sc_e     = intel.Sc_e(i_xi);
elseif strcmp(options.modelDiffusion,'cstLe')
    inteln.Le_e     = intel.Le_e(i_xi);
end
inteln.cylCoordFuncs = intel.cylCoordFuncs;
switch options.numberOfTemp
    case '1T'
        % nothing extra
    case '2T'
        inteln.hv_e = intel.hv_e(i_xi); % vibrational quantities
        inteln.Tv_e = intel.Tv_e(i_xi);
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end
switch options.modelTurbulence
    case {'none','SmithCebeci1967'}     % nothing to add
    case 'SSTkomega'                    % reference turbulent quantities
        inteln.ThetaK = intel.ThetaK(i_xi);
        inteln.ThetaW = intel.ThetaW(i_xi);
        inteln.kT_e    = intel.kT_e(i_xi);
        inteln.wT_e    = intel.wT_e(i_xi);
    otherwise
        error(['The chosen turbulence model ''',options.modelTurbulence,''' is not supported']);
end

% relevant constants
inteln.mixCnst = intel.mixCnst;
inteln.g_lim   = intel.g_lim;

% updating fields that are not scaled with their edge values (like g,
% df_deta or k are with H_e, U_e and W_0)
if i_xi>1
    if intel.mixCnst.N_spec>1
        ys_e = intel.ys_e; % we want to update the mass fractions
        % constant addend correction
        for s=1:inteln.mixCnst.N_spec
            deltays = ys_e{s}(i_xi)-ys_e{s}(i_xi-1); % increase/decrease in the mass fractions
            inteln.ys{s} = deltays + inteln.ys{s};
        end
    end
    if intel.mixCnst.N_elem>1
        yE_e = intel.yE_e; % we want to update the mass fractions
        % constant addend correction
        for E=1:inteln.mixCnst.N_elem
            deltayE = yE_e{E}(i_xi)-yE_e{E}(i_xi-1); % increase/decrease in the mass fractions
            inteln.yE{E} = deltayE + inteln.yE{E};
        end
    end
end
