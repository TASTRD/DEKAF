function flag = check_res_saturation(conv_list,it_res_sat,diff_val)
%CHECK_RES_SATURATION Check if residuals have become numerically saturated
%during the marching loop.
%
% Usage:
%   flag = check_res_saturation(conv_list,it_res_sat,diff_val);
%
% Inputs and Outputs
%   conv_list           vector whose entries are the convergences values of
%                       the ith marching iteration
%   it_res_sat          double. This input tells the function how many
%                       previous iterations to check for numerical
%                       saturation. By numerical saturation, we mean that
%                       the residuals have leveled off at a certain order
%                       of magnitude
%   diff_val            double. represents the limiting value for the
%                       average rate of change of the maximum residual
%                       before the iterations are stopped due to numerical
%                       saturation
%
%   flag                boolean, true indicates residual saturation has
%                       been reached. false indicates otherwise.
%
% Note:
%   It is assumed that length(conv_list) > it_res_sat to avoid
%   out-of-bounds indexing.
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

% Remove any artificial zeros placed in conv_list
% Keep the number of zeros found to avoid out-of-bounds indexing
k = nnz(conv_list==0);
conv_list(conv_list==0) = [];

% Extract the order of magnitudes of the convergence vector
OoM = log10(conv_list);

% Metric: if the average delta over the last it_res_sat iterations
% is approximately leveling off (i.e., no longer spectral convergence)
flag = abs(mean(diff(OoM(end-it_res_sat+1+k:end)))) < diff_val;

end % check_res_saturation
