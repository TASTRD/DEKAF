function intel_newRun = update_intel4restart(intel_newRun,intel_oldRun,options)
% UPDATE_INTEL4RESTART makes the necessary changes on an intel comming from
% a marching, so that it can be used to initialize another marching.
%
%   Examples:
%       (1)     intel_newRun = update_intel4restart(intel_newRun,intel_oldRun,options)
%
% If options.restartFromMarching_ixi = NaN the last computed profile is
% taken
%
% See also: DEKAF_BL, setDefaults
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

% We first obtain the fieldnames of fields that are to be maintained
[fld_nms,n_subidx] = getFlowVariableNames(options);
N = options.N;                                      % number of points in the wall-normal direction
N_spec = intel_oldRun.mixCnst.N_spec;                   % number of species
N_elem = intel_oldRun.mixCnst.N_elem;                   % number of species

if isnan(options.restartFromMarching_ixi);  ix_restart = size(intel_oldRun.f,2);            % final position
else;                                       ix_restart = options.restartFromMarching_ixi;   % chosen position
end

% We now populate intel4restart with the final profile of each of these
% variables contained in intel
for ii=1:length(fld_nms)                            % looping fields to be passed
    if ~isfield(intel_oldRun,fld_nms{ii})                   % this means the field was not contained in the previous, so we populate it with zeros
        dispif(['Setting ',fld_nms{ii},' at the initial xi to zero'],options.display);
        switch n_subidx(ii)                                 % depending on the number of subindices
            case 1                                              % we will initialize only one
                intel_newRun.(fld_nms{ii}) = zeros(N,1);
            case {N_spec,N_elem}                                % or one for every species/element
                N_cllPos = n_subidx(ii);
                for s=1:N_cllPos
                    intel_newRun.(fld_nms{ii}){s} = zeros(N,1);
                end
            case {N_spec^2,N_elem^2}                            % or one for every species/element pair
                N_cllPos = sqrt(n_subidx(ii));
                for s=1:N_cllPos
                    for l=1:N_cllPos
                        intel_newRun.(fld_nms{ii}){s,l} = zeros(N,1);
                    end
                end
            otherwise                                           % any other number of subindices will return an error
                error(['Wrong number of indices ',num2str(n_subidx(ii)),' for property ',fld_nms{ii}]);
        end                                                 % close switch on the number of subindices
    else                                                % this means the field is contained, so we simply take the last xi location
        switch n_subidx(ii)                                 % depending on the number of subindices
            case 1                                              % we will initialize only one
                intel_newRun.(fld_nms{ii}) = intel_oldRun.(fld_nms{ii})(:,end);
            case {N_spec,N_elem}                                % or one for every species/element
                N_cllPos = n_subidx(ii);
                for s=1:N_cllPos
                    intel_newRun.(fld_nms{ii}){s} = intel_oldRun.(fld_nms{ii}){s}(:,end);
                end
            case {N_spec^2,N_elem^2}                            % or one for every species/element pair
                N_cllPos = sqrt(n_subidx(ii));
                for s=1:N_cllPos
                    for l=1:N_cllPos
                        intel_newRun.(fld_nms{ii}){s,l} = intel_oldRun.(fld_nms{ii}){s,l}(:,end);
                    end
                end
            otherwise                                           % any other number of subindices will return an error
                error(['Wrong number of indices ',num2str(n_subidx(ii)),' for property ',fld_nms{ii}]);
        end                                                 % close switch on the number of subindices
    end                                                 % close if on wether the field exists in intel_in or not
end                                                 % close for loop on the fields

intel_newRun.norms_ref = intel_oldRun.norms_ref; % passing also reference norms
