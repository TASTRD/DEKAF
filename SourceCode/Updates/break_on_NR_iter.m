function break_or_not = break_on_NR_iter(intel, options, varargin)
%BREAK_ON_NR_ITER Returns true if the encompassing Newton-Raphson loop
% ought to break due to an error encountered, summarized by:
% a) complex perturbations
% b) nan perturbations
% c) residuals have saturated
%
% Note for cases a)-b), the intel passed into from DEKAF_BL needs to be
% inteln for this function to work properly. For case c), we're checking
% multiple iterations, so we need intel, *not* inteln.
%
% Example usage:
% (1)
%    break_or_not = break_on_NR_iter(inteln, options, 'cmplx', 'nans');
% (2)
%    break_or_not = break_on_NR_iter(intel, options, 'residuals');
%
% Author(s): Ethan Beyak

i_xi = intel.i_xi;

[~,cmplx] = find_flagAndRemove('cmplx',varargin);
[~,nans] = find_flagAndRemove('nans',varargin);
[~,residuals] = find_flagAndRemove('residuals',varargin);

break_or_not = false;

if isfield(intel,'t') && any(abs(imag(intel.t))>options.tol*max(abs(real(intel.t))))
    break_or_not = true;
    warning('Imaginary y reached during the computation of the curvature term - breaking iterations');
    return
end

if cmplx && any(imag(intel.sol_hat) > options.tol)
    if any(intel.g < 0)
        warning('!!! Perturbation is complex, g is negative.')
    else
        pos_imag = 1:length(intel.sol_hat);
        pos_imag = pos_imag(imag(intel.sol_hat)~=0);
        pos_imag = unique(ceil(pos_imag/options.N));
        warning(sprintf('\n!!! Perturbation is complex for variable %d',pos_imag));
    end
    break_or_not = true;
    warning('Consider setting options.perturbRelaxation < 1');
    return
end

if nans && nnz(isnan(intel.sol_hat))
    warning('The solution NaNed');
    break_or_not = true;
    return
end

if residuals && length(intel.all_conv{i_xi}(:)) >= options.it_res_sat(i_xi)
    break_or_not = check_res_saturation(intel.all_conv{i_xi}(:),...
        options.it_res_sat(i_xi),options.it_res_sat_diff_val(i_xi));
    if break_or_not
        fprintf(['--- Residuals have become numerically saturated ', ...
            'over the last %d iterations.\n'], options.it_res_sat(i_xi));
        return
    end
end

end % break_on_NR_iter
