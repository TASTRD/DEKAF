function [fld_nms,n_subidx,state_fldnms,n_subidx_state] = getFlowVariableNames(options)
%GETFLOWVARIABLENAMES Obtaining field names corresponding to flow and
% diffusion assumptions
%
% Examples:
%   (1) [fld_nms,n_subidx] = getFlowVariableNames(options)
%
%   (2) [fld_nms,n_subidx,state_fldnms,n_subidx_state] = getFlowVariableNames(options)
%       |--> returns also the fieldnames of state variables (f, g, etc.)
%
% Inputs:
%   options     -   needed for fields .flow and .DiffAssump
%
% Outputs:
%   fld_nms     -   cell of strings whose entries correspond to the flow's
%                   system variables
%   n_subidx    -
%
% See also DEKAF_BL.m, update_intels_BL.m
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

% NOTE: from now (Feb. 2018) on we don't expand about the base flow (it's
% too 2017), so all d/dg, d/ddfdeta,... variables are eliminated

state_fldnms = {'f', 'df_deta', 'df_deta2','df_deta3',...
    'g', 'dg_deta', 'dg_deta2',...
    'k', 'dk_deta', 'dk_deta2',...
    };
if options.N_spec>1
    state_fldnms = [state_fldnms {'ys','dys_deta','dys_deta2','yE','dyE_deta','dyE_deta2'}];
end
switch options.numberOfTemp
    case '1T'   % nothing to add
    case '2T';  state_fldnms = [state_fldnms,{'tauv', 'dtauv_deta', 'dtauv_deta2'}];
    otherwise;  error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
end
if ismember(options.modelTurbulence,{'SSTkomega'})
    state_fldnms = [state_fldnms {'KT','dKT_deta','dKT_deta2','WT','dWT_deta','dWT_deta2'}];
end

fld_nms = [state_fldnms,...
    {'C', 'dC_deta', ...'dC_dg','ddC_dg_deta','dC_ddfdeta','ddC_ddfdeta_deta'...
    'a1', 'da1_deta', ...'da1_dg','dda1_dg_deta','da1_ddfdeta','dda1_ddfdeta_deta'...
    'a2', 'da2_deta', ...'da2_dg','dda2_dg_deta','da2_ddfdeta','dda2_ddfdeta_deta'...
    'a0', 'da0_deta', ...'da0_dg','dda0_dg_deta','da0_ddfdeta','dda0_ddfdeta_deta'...
    'j', ...'dj_dg','dj_ddfdeta',...
    'tau','dtau_deta','dtau_deta2', ...
    'fh','dfh_deta','gh','kh','dkh_deta',...
    }];
switch options.numberOfTemp
    case '1T' % nothing to add
    case '2T';  fld_nms = [fld_nms,{'av1', 'dav1_deta','av2', 'dav2_deta','tauv','dtauv_deta','dtauv_deta2',...
                                    'OmegaV','dOmegaV_ddfdeta','dOmegaV_dg','tauvh','Cpv'}];
    otherwise;  error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
end

if ismember(options.modelTurbulence,{'SSTkomega'})
    fld_nms = [fld_nms,{'KTh','WTh','AK','AW','BW','aK0','aK1','aK2','aW0','aW1','aW2','daK2_deta','daW2_deta'}];
end

if ismember(options.flow,{'CNE','TPG','TPGD'})
    if options.bPairDiff
        fld_nms = [fld_nms,{'ysh','zetas','dzetas_deta','dzetas_deta2',...
            'Csl', 'dCsl_deta', ...'dCsl_dg','ddCsl_dg_deta','dCsl_ddfdeta','ddCsl_ddfdeta_deta',...
            'as1', 'das1_deta', ...'das1_dg','ddas1_dg_deta','das1_ddfdeta','ddas1_ddfdeta_deta',...
            'asl2', 'dasl2_deta', ...'dasl2_dg','ddasl2_dg_deta','dasl2_ddfdeta','ddasl2_ddfdeta_deta',...
            'as2', 'das2_deta','Cs', 'dCs_deta',... DANGER!! THIS LINE SHOULD GO AWAY
            'Omegas','dOmegas_ddfdeta','dOmegas_dg','dOmegas_dyl'}];
        switch options.numberOfTemp
            case '1T'   % nothing to add
            case '2T';  fld_nms = [fld_nms,{'avsl2', 'davsl2_deta'}];
            otherwise;  error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
        end
    else
        fld_nms = [fld_nms,{'ysh','zetas','dzetas_deta','dzetas_deta2',...
            'Cs', 'dCs_deta', ...'dCs_dg','ddCs_dg_deta','dCs_ddfdeta','ddCs_ddfdeta_deta'...
            'as1', 'das1_deta', ...'das1_dg','ddas1_dg_deta','das1_ddfdeta','ddas1_ddfdeta_deta',...
            'as2', 'das2_deta', ...'das2_dg','ddas2_dg_deta','das2_ddfdeta','ddas2_ddfdeta_deta',...
            'Omegas','dOmegas_ddfdeta','dOmegas_dg','dOmegas_dyl'}];
        switch options.numberOfTemp
            case '1T'   % nothing to add
            case '2T';  fld_nms = [fld_nms,{'avs2', 'davs2_deta','dOmegaV_dyl'}];
            otherwise;  error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
        end
    end
elseif ismember(options.flow,{'LTEED'})
    fld_nms = [fld_nms,{'ysh','agE','auE','aYE','CYEF','dagE_deta','dauE_deta','daYE_deta','dCYEF_deta'}];
end

if options.marchYesNo % we include also the xi derivatives
    fld_nms = [fld_nms,{'df_dxi','dg_dxi','df_detadxi','dk_dxi'}];
    if sum(strcmp(options.flow,{'CNE','TPGD','LTEED'}))
        fld_nms = [fld_nms,{'dys_dxi','dyE_dxi'}];
    end
    switch options.numberOfTemp
        case '1T'   % nothing to add
        case '2T';  fld_nms = [fld_nms,'dtauv_dxi','dzetas_dxi'];
        otherwise;  error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
    end
    if ismember(options.modelTurbulence,{'SSTkomega'})
        fld_nms = [fld_nms {'dKT_dxi','dWT_dxi'}];
    end
end
n_subidx = variableDatabase(fld_nms,options);
n_subidx_state = variableDatabase(state_fldnms,options);

end % getFlowVariableNames.m
