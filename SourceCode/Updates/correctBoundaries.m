function [Q_out,varargout] = correctBoundaries(Q,Qc,eta,varargin)
% correctBoundaries makes a correction on the values of a certain
% computational variable Q at the top and bottom boundaries, according to
% the values of a corrected variable Qc.
%
% Usage:
%   (1)
%       Q = correctBoundaries(Q,Qc,eta)
%
%   (2)
%       [Q,dQ_deta,dQ_deta2] = correctBoundaries(Q,Qc,eta,dQ_deta,dQ_deta2)
%       |--> also corrects the first and second derivative
%
%   (3)
%       [...] = correctBoundaries(...,'onlyPos1')
%       |--> corrects only the top boundary (1st position of eta)
%
%   (4)
%       [...] = correctBoundaries(...,'onlyPosEnd')
%       |--> corrects only the bottom boundary (final position of eta)
%
%   (5)
%       [...] = correctBoundaries(...,'fix_a',a)
%       |--> allows to fix the value of a in the correction (default 2)
%
% Inputs and outputs:
%   Q
%       variable (N_eta x 1)
%   Qc
%       variable with correct boundaries (N_eta x 1)
%   eta
%       wall-normal position vector (N_eta x 1)
%
% Author: Fernando Miro Miro
% Date: July 2019

[a,varargin] = parse_optional_input(varargin,'fix_a',2);
% a=2 gives a good decay such that at eta=6 (typical end of the BL) the
% distortion of the fields is <1e-5 times the introduced delta

dQ1     = Qc(1)   - Q(1);
dQEnd   = Qc(end) - Q(end);
etaMax = max(eta);

[varargin,bOnlyPos1  ] = find_flagAndRemove('onlyPos1'  ,varargin);
[varargin,bOnlyPosEnd] = find_flagAndRemove('onlyPosEnd',varargin);
if bOnlyPos1;       dQEnd = 0;      end
if bOnlyPosEnd;     dQ1 = 0;        end

Q_out = Q                      +     dQEnd * exp(-a*eta) +     dQ1 * exp(a*(eta-etaMax));
if length(varargin)>=1
    varargout{1} = varargin{1} -   a*dQEnd * exp(-a*eta) +   a*dQ1 * exp(a*(eta-etaMax));
end
if length(varargin)>=2
    varargout{2} = varargin{2} + a^2*dQEnd * exp(-a*eta) + a^2*dQ1 * exp(a*(eta-etaMax));
end


end % correctBoundaries