function [break_or_not, separation_i_xi_new] = break_on_x_iter(...
    separation_i_xi_old, conv, max_residual, iXi_stop, i_xi, df_deta2_w)
%BREAK_ON_X_ITER Returns true if the encompassing x-marching loop
% ought to break due to an error encountered, that is,
% a) residuals are too large
% b) separation has been reached
% c) the stopping marching station has been reached
%
% As a side-effect, the xi marching index indicating the station of
% separation/divergence is updated. Cases b) and c) assign the index to the
% current marching index; whereas case a) shifts the separation index to
% the previous step to bypass the "nonsolution" (unphysically large
% residuals).
%
% Usage:
%   (1)
%       [break_or_not, separation_i_xi_new] = break_on_x_iter(separation_i_xi_old, ...
%                                       conv, max_residual, iXi_stop, i_xi, df_deta2_w)
%
% Author(s):    Ethan Beyak
%               Fernando Miro Miro
% GNU Lesser General Public License 3.0

break_or_not = false;
separation_i_xi_new = separation_i_xi_old;

% check if residuals are ridiculously large, check for "nonsolutions"
if conv > max_residual
    warning('Residuals extremely large, "non-solution" removed and breaking loop!')
    separation_i_xi_new = i_xi - 1;
    break_or_not = true;
end

% check if separation has occurred
if df_deta2_w(i_xi) < 0
    warning('Separation detected: breaking loop!')
    separation_i_xi_new = i_xi;
    break_or_not = true;
end

% check if the user chose to halt the marching
if i_xi>=iXi_stop
    warning(['marching reached iXi_stop=',num2str(iXi_stop)]);
    separation_i_xi_new = i_xi;
    break_or_not = true;
end

end % break_on_x_iter
