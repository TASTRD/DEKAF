function [ys,dys_deta,dys_deta2] = update_ys_fullCatalyticWall(Ys_bc,ys,dys_deta,dys_deta2,eta)
% update_ys_fullCatalyticWall updates the ys fields as well as their
% derivatives to account for a possible variation of the mass fraction at
% the wall.
%
% Usage:
%   (1)
%       [ys,dys_deta,dys_deta2] = update_ys_fullCatalyticWall(Ys_bc,ys,dys_deta,dys_deta2,eta)
%
% the ys fields are updated by adding to them a negative exponential times
% the delta_ys necessary to correct the variation in the mass fraction.
% This negative exponential is of the form:
%
%       delta_ys * exp(-2*eta)
%
% See also: update_g_isoThermalWall
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

a = 2; % HARCODE ALERT!
% gives a good decay such that at eta=6 (typical end of the BL) the
% distortion of the fields is <1e-5 times the introduced delta
for s=1:length(ys)
    delta_ys        = Ys_bc{s} - ys{s}(end);
    ys{s}           = ys{s}         + delta_ys       * exp(-a*eta);
    if nargout>1
    dys_deta{s}     = dys_deta{s}   - a*delta_ys     * exp(-a*eta);
    dys_deta2{s}    = dys_deta2{s}  + a^2*delta_ys   * exp(-a*eta);
    end
end