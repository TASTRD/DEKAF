function [intel,intelp,inteln] = update_intels_BL(i_xi,intel_QSS,intel,intelp,inteln,options)
%UPDATE_INTELS_BL Update intel structures within the marching loop
%
% Inputs:
%   i_xi        -   index of xi marching location, scalar
%   intel_QSS   -   self-similar solution intel, used to initialize intel
%                   and intelp
%   intel       -   "total" intel, stores the entire solution across the
%                   marching domain
%   intelp      -   "previous" intel to store the solutions in a struct
%                   across the computational marching stencil, as defined
%                   by options.ox
%   inteln      -   "next" intel to translate the solution to the next,
%                   downstream location in xi
%   options     -   needed for its field .ox, which specifies the order of
%                   finite difference derivatives in the xi direction
%                   and mapOpts.N_x, the number of marching domain points
%
% See also DEKAF_BL.m, getFlowVariableNames.m
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

fld_nms = intel.fld_nms;
n_subidx = intel.n_subidx;
N_spec = options.N_spec;
N_elem = options.N_elem;
N_x = options.mapOpts.N_x;
N = options.N;
cll_flds = {'zetas','ys','dys_deta','dys_deta2','ysh'}; % cell fields

if i_xi==1  % initial condition -
    % Initialising all the fields
    % loop over all variable field names
    for jj = 1:length(fld_nms)
        if ~isfield(intel_QSS,fld_nms{jj})                                  % this means that the field didn't exist for QSS
            dispif(['Setting ',fld_nms{jj},' at the initial xi to zero'],options.display);
            if n_subidx(jj)==1 && ~ismember(fld_nms{jj},cll_flds) % it means there is no species indexing
                intel.(fld_nms{jj}) = zeros(N,N_x);
                intelp.(fld_nms{jj}) = zeros(N,1);
            elseif n_subidx(jj)==N_spec || n_subidx(jj)==N_elem                                         % there will be one species index
                N_cllPos = n_subidx(jj);
                for s=1:N_cllPos
                    intel.(fld_nms{jj}){s} = zeros(N,N_x);
                    intelp.(fld_nms{jj}){s} = zeros(N,1);
                end
            elseif n_subidx(jj)==N_spec^2  || n_subidx(jj)==N_elem^2                                      % there will be two species indices
                N_cllPos = sqrt(n_subidx(jj));
                for s=1:N_cllPos
                    for l=1:N_cllPos
                        intel.(fld_nms{jj}){s,l} = zeros(N,N_x);
                        intelp.(fld_nms{jj}){s,l} = zeros(N,1);
                    end
                end
            else
                error(['something went pretty wrong - the length of field ',fld_nms{jj},' is neither 1, nor N_spec, nor N_spec^2.']);
            end
        else                                                                % otherwise we extract it from intel_QSS
            if n_subidx(jj)==1                                                  % it means there is no species indexing
                % copy initial condition to the previous step
                intelp.(fld_nms{jj}) = intel_QSS.(fld_nms{jj}); % this structure will expand to contain all options.ox previous steps
                % intelp - intel previous

                % intel itself will be used to contain the whole solution
                % matrices, importing initial condition:
                intel.(fld_nms{jj}) = [intel_QSS.(fld_nms{jj}) zeros(N,N_x-1)];

            elseif n_subidx(jj)==N_spec || n_subidx(jj)==N_elem % there will be
                N_cllPos = n_subidx(jj);
                for s=1:N_cllPos
                    intelp.(fld_nms{jj}){s} = intel_QSS.(fld_nms{jj}){s}; % strcture with the previous steps
                    intel.(fld_nms{jj}){s} = [intel_QSS.(fld_nms{jj}){s} zeros(N,N_x-1)]; % solution structure
                end
           elseif n_subidx(jj)==N_spec^2 || n_subidx(jj)==N_elem^2 % there will be two species indices
                N_cllPos = sqrt(n_subidx(jj));
                for s=1:N_cllPos
                    for l=1:N_cllPos
                        intelp.(fld_nms{jj}){s,l} = intel_QSS.(fld_nms{jj}){s,l}; % strcture with the previous steps
                        intel.(fld_nms{jj}){s,l} = [intel_QSS.(fld_nms{jj}){s,l} zeros(N,N_x-1)]; % solution structure
                    end
                end
            else
                error(['something went pretty wrong - the length of field ',fld_nms{jj},' is neither 1, nor N_spec, nor N_spec^2.']);
            end
        end
    end

    % copy initial condition to the next step
    inteln = intelp;
    % this structure will contain the solution at the next step and the
    % edge values
    inteln.D1 = intel.D1;
    inteln.D2 = intel.D2;
    inteln.eta = intel.eta;
    % passing the norm references (for the flow equations)
    inteln.norms_ref = intel_QSS.norms_ref;
    % passing any guesses (Tguess, Tvguess, etc)
    flds_QSS = fieldnames(intel_QSS);
    flds_guess = flds_QSS(~cellfun(@isempty,regexp(flds_QSS,'(\w)+guess$')));
    for ii=1:length(flds_guess)
        inteln.(flds_guess{ii}) = intel_QSS.(flds_guess{ii});
    end
elseif i_xi<options.ox+1
    for jj = 1:length(fld_nms)
        if n_subidx(jj)==1  && ~ismember(fld_nms{jj},cll_flds)
            intel.(fld_nms{jj})(:,i_xi)  = inteln.(fld_nms{jj});% store last solution in intel
            intelp.(fld_nms{jj})(:,i_xi) = inteln.(fld_nms{jj});% add last solution to intelp
        elseif n_subidx(jj)==N_spec || n_subidx(jj)==N_elem 
            N_cllPos = n_subidx(jj);
            for s=1:N_cllPos
                intel.(fld_nms{jj}){s}(:,i_xi)  = inteln.(fld_nms{jj}){s};% store last solution in intel
                intelp.(fld_nms{jj}){s}(:,i_xi) = inteln.(fld_nms{jj}){s};% add last solution to intelp
            end
        elseif n_subidx(jj)==N_spec^2 || n_subidx(jj)==N_elem^2
            N_cllPos = sqrt(n_subidx(jj));
            for s=1:N_cllPos
                for l=1:N_cllPos
                    intel.(fld_nms{jj}){s,l}(:,i_xi)  = inteln.(fld_nms{jj}){s,l};% store last solution in intel
                    intelp.(fld_nms{jj}){s,l}(:,i_xi) = inteln.(fld_nms{jj}){s,l};% add last solution to intelp
                end
            end
        else
            error(['something went pretty wrong - the length of field ',fld_nm,' is neither 1, nor N_spec, nor N_spec^2.']);
        end
    end
elseif i_xi~=N_x % as long as the station is not the last one
    for jj = 1:length(fld_nms)
        if n_subidx(jj)==1 && ~ismember(fld_nms{jj},{'zetas','ys','dys_deta','dys_deta2','ysh'})
            intel.(fld_nms{jj})(:,i_xi)  = inteln.(fld_nms{jj});% store last solution in intel
            intelp.(fld_nms{jj})(:,1) = [];% remove most upstream solution
            intelp.(fld_nms{jj})(:,options.ox) = inteln.(fld_nms{jj});% add last solution to intelp
        elseif n_subidx(jj)==N_spec || n_subidx(jj)==N_elem 
            N_cllPos = n_subidx(jj);
            for s=1:N_cllPos
                intel.(fld_nms{jj}){s}(:,i_xi)  = inteln.(fld_nms{jj}){s};% store last solution in intel
                intelp.(fld_nms{jj}){s}(:,1) = [];% remove most upstream solution
                intelp.(fld_nms{jj}){s}(:,options.ox) = inteln.(fld_nms{jj}){s};% add last solution to intelp
            end
        elseif n_subidx(jj)==N_spec^2 || n_subidx(jj)==N_elem^2
            N_cllPos = sqrt(n_subidx(jj));
            for s=1:N_cllPos
                for l=1:N_cllPos
                    intel.(fld_nms{jj}){s,l}(:,i_xi)  = inteln.(fld_nms{jj}){s,l};% store last solution in intel
                    intelp.(fld_nms{jj}){s,l}(:,1) = [];% remove most upstream solution
                    intelp.(fld_nms{jj}){s,l}(:,options.ox) = inteln.(fld_nms{jj}){s,l};% add last solution to intelp
                end
            end
        else
            error(['something went pretty wrong - the length of field ',fld_nm,' is neither 1, nor N_spec, nor N_spec^2.']);
        end
    end
else % if i_xi==N_x
    for jj = 1:length(fld_nms)
        if n_subidx(jj)==1 && ~ismember(fld_nms{jj},{'zetas','ys','dys_deta','dys_deta2','ysh'})
            intel.(fld_nms{jj})(:,i_xi)  = inteln.(fld_nms{jj});% store last solution in intel
        elseif n_subidx(jj)==N_spec || n_subidx(jj)==N_elem 
            N_cllPos = n_subidx(jj);
            for s=1:N_cllPos
                intel.(fld_nms{jj}){s}(:,i_xi)  = inteln.(fld_nms{jj}){s};% store last solution in intel
            end
        elseif n_subidx(jj)==N_spec^2 || n_subidx(jj)==N_elem^2
            N_cllPos = sqrt(n_subidx(jj));
            for s=1:N_cllPos
                for l=1:N_cllPos
                    intel.(fld_nms{jj}){s,l}(:,i_xi)  = inteln.(fld_nms{jj}){s,l};% store last solution in intel
                end
            end
        else
            error(['something went pretty wrong - the length of field ',fld_nm,' is neither 1, nor N_spec, nor N_spec^2.']);
        end
    end
    % tau, g, df_deta, ys, k: get D_xi, D_xi2, D_etaxi
end

end % update_intels_BL.m
