function err = eqCheck_totalEnth(G,dG_deta,dG_deta2,f,df_deta,df_deta2,df_deta3,k,dk_deta,dk_deta2,C,dC_deta,a1,da1_deta,Ecu,Ecw)
% eqCheck_totalEnth checks the f-g-k self-similar profiles, by evaluating
% the total enthalpy equation, as proposed by Gaponov.
%
% The variable names however, correspond to DEKAF's naming convention.
%
% Usage:
%   (1)
%       err = eqCheck_totalEnth(G,dG_deta,dG_deta2,f,df_deta,df_deta2,df_deta3,k,...
%                                   dk_deta,dk_deta2,C,dC_deta,a1,da1_deta,Ecu,Ecw)
%
% Note that the G inputs are the total enthalpy (non-dimensional h+0.5*u^2+0.5*w^2)
%
% References:
% - S. A. Gaponov and B. V. Smorodskii 2008. "LINEAR STABILITY OF
% THREE-DIMENSIONAL BOUNDARY LAYERS".
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

Pr = C./a1;

err = da1_deta.*dG_deta + a1.*dG_deta2 + f.*dG_deta + (1-1./Pr).*1./(1+Ecu/2+Ecw/2).*(Ecu*(dC_deta.*df_deta.*df_deta2 + C.*df_deta2.^2 + C.*df_deta.*df_deta3) + Ecw*(dC_deta.*k.*dk_deta + C.*dk_deta.^2 + C.*k.*dk_deta2));

