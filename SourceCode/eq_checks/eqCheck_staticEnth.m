function err = eqCheck_staticEnth(g,dg_deta,dg_deta2,f,df_deta,df_deta2,k,dk_deta,C,dC_deta,a1,da1_deta,Ecu,Ecw,beta)
% eqCheck_staticEnth checks the f-g-k self-similar profiles, by evaluating
% the static enthalpy equation, as proposed by Liu.
%
% The variable names however, correspond to DEKAF's naming convention.
%
% Usage:
%   (1)
%       err = eqCheck_staticEnth(g,dg_deta,dg_deta2,f,df_deta,df_deta2,k,...
%                                   dk_deta,C,dC_deta,a1,da1_deta,Ecu,Ecw)
%
% Note that the g inputs are the static enthalpy, which is different from
% what is normally used in DEKAF
%
% References:
% - Liu 1997. "Direct Numerical Simulation of Flow Transition In a
% Compressible Swept-Wing Boundary Layer".
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

err = da1_deta.*dg_deta + a1.*dg_deta2 + f.*dg_deta + C.*(Ecu*df_deta2.^2 + Ecw*dk_deta.^2) - Ecu*df_deta.*beta.*(g-df_deta.^2); % Eq. 6 (swapping k and g, to agree with DEKAF's naming)
