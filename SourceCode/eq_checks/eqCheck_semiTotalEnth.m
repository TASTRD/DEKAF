function err = eqCheck_semiTotalEnth(g,dg_deta,dg_deta2,f,df_deta,df_deta2,df_deta3,k,dk_deta,dk_deta2,C,dC_deta,a1,da1_deta,a2,da2_deta,a0,Ecu,Ecw)
% eqCheck_semiTotalEnth checks the f-g-k self-similar profiles, by evaluating
% the semi-total enthalpy equation.
%
% Usage:
%   (1)
%       err = eqCheck_semiTotalEnth(g,dg_deta,dg_deta2,f,df_deta,df_deta2,df_deta3,k,...
%                                   dk_deta,dk_deta2,C,dC_deta,a1,da1_deta,Ecu,Ecw)
%
% Note that the G inputs are the total enthalpy (non-dimensional h+0.5*u^2+0.5*w^2)
%
% References:
% - K. Groot, F. Miro Miro, E. Beyak, A. Moyes, F. Pinna and H. Reed. 2018.
% "DEKAF: Spectral Multi-Regime Basic-State Solver for Boundary-Layer
% Stability."
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

Pr = C./a1;

err = f.*dg_deta + da1_deta.*dg_deta + a1.*dg_deta2 + Ecu/(1+Ecu/2)*(1-1./Pr).*(dC_deta.*df_deta.*df_deta2 + C.*df_deta2.^2 + C.*df_deta.*df_deta3) + Ecw/(1+Ecu/2).*C.*dk_deta.^2;
