function [intel,options] = DEKAF_BL(intel,intel_QSS,options)
% DEKAF_BL Solve the non-self-similar (marching) compressible
% boundary-layer system of ODEs, using the Chebyshev pseudo-spectral
% collocation method.
%
% [intel,options] = DEKAF_BL(intel,intel_QSS,options);
%
% Notes:
% For an explanation of inputs, see listOfVariablesExplained.m and
% setDefaults.m
%
% Children and Related Functions:
% See also build_edge_values.m, build_mat.m,, eval_thermo.m,
%          build_BC.m, update_solution.m, eval_fullFlowField.m
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
%            Koen Groot
%            Alexander Moyes
%
% GNU Lesser General Public License 3.0

% Displaying DEKAF logo
if options.displayHeader
displayTestcaseInfo(intel,options);
end
disp('--- Running...')
tic;

% obtaining variables' fields
[intel.fld_nms,intel.n_subidx] = getFlowVariableNames(options);
intelp = struct;
inteln = struct;
if options.plot_marching % we initialize the figure
    figID = figure;
end

% Preparing all moptions for the gif marching
if options.gifMarch
    opts4gifMarch = options;
    opts4gifMarch.savePath_gif      = options.savePath_gifMarch;
    opts4gifMarch.gifLapse          = options.gifMarchLapse;
    opts4gifMarch.gifStep           = options.gifMarchStep;
    opts4gifMarch.gifxlim           = options.gifMarchxlim;
    opts4gifMarch.gifylim           = options.gifMarchylim;
    opts4gifMarch.gifLogPlot        = options.gifMarchLogPlot;
    opts4gifMarch.fields4gif        = options.fields4gifMarch;
    opts4gifMarch.fields4gifNames   = options.fields4gifMarchNames;
    opts4gifMarch.gif_Nsubplots     = options.gifMarch_Nsubplots;
end

%% STARTING MAIN LOOP
for i_xi = 2:options.mapOpts.N_x
    disp(['--- xi station number ',num2str(i_xi),' -->  ',...
            num2str(intel.xi(i_xi)/intel.xi(end)*100),'% of the xi range'])
    intel.i_xi = i_xi;

    % update intel structures, inteln and intelp, for the next and previous xi locations
    [intel,intelp,inteln] = update_intels_BL(i_xi-1,intel_QSS,intel,intelp,inteln,options);

    % Obtaining edge coefficients for the chosen position
    [inteln,options] = update_edge_values_BL(inteln,intel,options,i_xi);

    inteln = eval_thermo(inteln,options);

    conv = 1;
    inteln.norms = struct;
    inteln.it = 1;
    while conv>options.tol && inteln.it<options.it_max

        % plotting transient BL profiles
        if options.plot_transient && sum(inteln.it==options.it_plot)
            inteln.i_xi = 1; % there is only one profile in the xi direction in inteln, so we make i_xi one
            plotProfiles(inteln,options);
        end

        % creating convergence GIF
        if options.gifConv && floor(inteln.it/options.gifStep) == inteln.it/options.gifStep
            inteln = plot_gifConvergence(inteln,options,conv,1,['_xi',num2str(i_xi)],inteln.it==1);
        end

        % storing previous convergence in intel
        inteln.conv = conv;

        % defining LHS matrix and RHS forcing term vector
        inteln = build_mat(intel.xi(i_xi),inteln,intelp,options);
        inteln = build_BC(inteln,options,intelp);

        % regularizing
        [inteln.mat,inteln.forcing] = matrixRegularization(inteln.mat,inteln.forcing);

        % solving
        inteln.sol_hat = inteln.mat \ inteln.forcing;

        % checking for NaNs and complex solutions
        if break_on_NR_iter(inteln, options, 'cmplx', 'nans')
            break
        end

        % extracting, rebuilding derivatives and coefficients
        [inteln,options] = update_solution(inteln,options,intelp);
        inteln.it = inteln.it+1;
        [conv,inteln]= eval_norm(inteln,options);
        intel.all_conv{i_xi}(inteln.it) = conv;
        intel.all_it{i_xi}(inteln.it) = inteln.it;

        % Check if the residuals have saturated at approx. the same order
        % of magnitude for several iterations
        if break_on_NR_iter(intel, options, 'residuals')
            break
        end
    end

    fprintf('--- Exited Newton-Raphson with error: %7.4e; NR iterations: %3i\n', conv, inteln.it);

    % plotting profiles as the marching advances
    if options.plot_marching && sum(i_xi==options.plot_marchPos)
        clf(figID);
        figID = plotProfiles(intel,options,i_xi,figID);
        pause(0.1);
    end

    % preparing marching GIF
    if options.gifMarch && floor(i_xi/options.gifMarchStep) == i_xi/options.gifMarchStep
        intel = plot_gifConvergence(intel,opts4gifMarch,conv,i_xi-1);
    end

    % check if the x loop needs to be broken (e.g., separation
    % reached/expected, residuals are too large, etc.)
    [break_x_iter_or_not, options.mapOpts.separation_i_xi] = break_on_x_iter(...
        options.mapOpts.separation_i_xi, conv, options.max_residual, options.iXi_stop, ...
        i_xi, intel.df_deta2(end,:));
    if break_x_iter_or_not
        break
    end
end

% add last profile at index i_xi+1; & calculate xi derivatives
[intel,~,inteln] = update_intels_BL(i_xi,intel_QSS,intel,intelp,inteln,options);

% Evaluating equations on the complete flowfield and plot
if options.MarchingGlobalConv
    intel.norms_ref = intel_QSS.norms_ref;
    intel.err = eval_fullFlowField(intel,options);
end

%%
if inteln.it==options.it_max
    disp(['--- Concluded at the maximum iteration (',num2str(options.it_max),')']);
end
runtime = toc;
disp(['--- Runtime of ',num2str(runtime),' seconds']);
disp('--- Done');

end % DEKAF_BL.m
