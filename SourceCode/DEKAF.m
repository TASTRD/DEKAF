function [intel_out,options] = DEKAF(intel,options,varargin)
%DEKAF Run the boundary-layer solver DEKAF for provided input structs of
% intel and options.
%
% Usage:
% (1) [intel_out,options] = DEKAF(intel,options);
%   |-> Run the quasi-self-similar boundary-layer solver (denoted QSS).
%
% (2) [intel_out,options] = DEKAF(intel,options,'marching');
%   |-> Run the quasi-self-similar boundary-layer solver, followed by 
%       solving the non-self-similar boundary-layer equations
%       (denoted BL).
%
% Environment:
%   To run DEKAF, the environmental variable $DEKAF_DIRECTORY must be
%   defined.
%
%   a) OS Linux: in your ~/.bashrc file, append the following line:
%       export DEKAF_DIRECTORY=/path/to/your/DEKAF
%
%       ...where /path/to/your is the absolute path from root to the
%       DEKAF-git-clone'd directory location.
%
% Inputs:
%   intel       
%       struct whose fields contain the essential flow parameters.
%       Depending on whether the BL or the QSS equations are to be solved
%       (flag 'marching' passed or not), and depending on the
%       inviscid-flowfield treatment chosen through
%       options.inviscidFlowfieldTreatment, different inputs are required:
%
%-----> Non-marching QSS solution
%           Three inviscid edge quantities must be specified within intel
%           and DEKAF currently supports several different combinations:
%
%           i)  Always specify T_e
%           ii) Choose two out of three from the options below:
%                   1) rho_e or p_e
%                   2) Re1_e
%                   3) U_e   or M_e
%
%           ...where U_e is the edge streamwise velocity        [m/s]
%                    T_e is the edge equilibrium temperature    [K]
%                    p_e is the edge static pressure            [Pa]
%                    rho_e is the edge mixture mass density     [kg/m^3]
%                    M_e is the edge Mach number                [-]
%                    Re1_e is the edge unit Reynolds number     [1/m]
%           iii) Specify additional temperatures if needed. For instance,
%           if options.numberOfTemp = '2T', one must provide Tv_e
%                    Tv_e is the edge vibrational-electronic-electron
%                    temperature    [K] 
%           iv)  Specify additional species mass fractions. For
%           multi-species or multi-element flow assumptions, like TPGD, CNE
%           or LTEED, one must also provide ys_e. For LTEED one can
%           actually provide either the species mass fractions (ys_e) or
%           the elemental mass fractions (yE_e) if
%           options.passElementalFractions = true. Both ys_e and yE_e must
%           be 1-D cells of length N_spec or N_elem respectively.
%                    ys_e are the edge species mass fractions   [-] 
%                    yE_e are the edge elemental mass fractions [-] 
%           The species order is whatever is detailed for the chosen
%           mixture in <a href="matlab:help getMixture_constants">getMixture_constants</a>, whilst the element order is
%           always alphabetical, with eventual electron elements in the
%           first position.
%
%           Here are some accepted input combinations as examples:
%               T_e, rho_e, Re1_e
%               T_e, p_e, M_e
%               T_e, Re1_e, M_e
%
%           Here are some combinations that are not supported as examples:
%               rho_e, Re1_e, U_e
%               T_e, rho_e, p_e
%
%-----> Marching BL solution
%           Depending on the chosen inviscid flowfield treatment (specified
%           through options.inviscidFlowfieldTreatment):
%           1) 'constant'
%                   the inviscid flow quantities are considered constant
%                   along the streamwise stretch. The required intel fields
%                   are the same as in the non-marching QSS case above. 
%           2) '1DNonEquilibriumEuler' 
%                   employs a one-dimensional non-equilibrium Euler solver
%                   along the inviscid streamline (see group of options F
%                   in <a href="matlab:help setDefaults">setDefaults</a>). Note that this option is ONLY
%                   supported for options.flow = 'CNE'. The required intel
%                   fields are the same as in the non-marching QSS case
%                   above, only with a 0 at the end, to note that they
%                   correspond to the initial streamwise station. Therefore
%                   one must provide U_e0 instead of U_e, p_e0 instead of
%                   p_e, etc.
%           3) 'airfoil' 
%                   the airfoil preprocessing routine is run, to convert a
%                   pressure coefficient of an airfoil into an edge
%                   distribution. For a thorough usage explanation please
%                   see <a href="matlab:help convert_airfoil">convert_airfoil</a>. In essence, one must provide:
%                       T_inf                           [K]
%                           freestream temperature
%                       M_inf_Q or M_inf_UV             [-]
%                           freestream Mach number based on the total
%                           velocity (Q) or the velocity in the x-y plane
%                           (UV)
%                       p_inf                           [Pa]
%                           freestream static pressure
%                       Angle of attack and sweep       [deg]
%                           a. alpha_v_u  and Lambda_w_uv OR
%                           b. alpha_v_uw and Lambda_w_u  OR
%                           c. alpha_v_u  and Lambda_w_u  OR
%                           d. alpha_v_uw and Lambda_w_uv
%                       airfoil
%                           structure containing the essential geometrical
%                           and Cp data. Especifically:
%                               xc_data and Cp_UV_data              [m] [-]
%                                   position along the LE-TE axis and value
%                                   of the pressure coefficient associated
%                                   to it.
%                               xc_geom_data and yc_geom_data       [m] [m]
%                                   full geometry of the airfoil
%                               chord_U                             [m]
%                                   length of the chord in the LE-normal
%                                   direction
%                   for more information, please see <a href="matlab:help convert_airfoil">convert_airfoil</a>
%           4) 'custom'
%                   allows the user to provide a series of custom profiles
%                   along the boundary-layer edge. Two inviscid edge
%                   distributions and one initial edge scalar quantity must
%                   be specified within intel. DEKAF currently supports
%                   several different combinations:
%
%                   -- For edge distributions one must specify:
%                   a) T_e(x)
%                   b) Either U_e(x) or M_e(x)
%                   c) If not b), either p_e(x) or rho_e(x)
%
%                   -- and an edge scalar quantity:
%                   if c) either U_e0 or M_e0 or Re1_e0
%                   if b) either p_e0 or rho_e0 or Re1_e0
%
%           Only usages 1) and 2) allow for the evaluation of shock-jump
%           relations.
%
%-----> Miscellaneous fields required in intel
%           Depending on the specifics of the test case to be run, one may
%           be required to provide:
%               .Lambda_sweep       [deg]
%                   Sweep angle for flat plate flows
%               .beta               [-]
%                   Self-similar pressure gradient Hartree parameter
%               .wedge_angle        [deg]
%                   Wedge angle for compression ramp flows
%               .cone_angle         [deg]
%                   Constant cone angle for flows over straight cones
%               .cp                 [J/kg-K]
%                   Specific heat constant of the gas for
%                   options.specify_cp = true 
%               .gam                [-]
%                   Specific heat ratio of the gas for
%                   options.specify_gam = true
%               .Sc_e               [-]
%                   Schmidt number for options.modelDiffusion = 'cstSc'
%               .Le_e           
%                   Lewis number for options.modelDiffusion = 'cstLe'
%
%   options     
%       struct whose fields contain the possible options to specify various
%       models implemented in DEKAF. A complete description of the fields
%       are given in <a href="matlab:help setDefaults">setDefaults</a>.
%
% Outputs:
%   intel_out   
%       intel struct containing the boundary-layer solution obtained by
%       DEKAF, along with many other variables used throughout the solver.
%
%   options
%       struct containing all options used throughout the DEKAF simulation
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
%               Alex Moyes
% GNU Lesser General Public License 3.0

errmsg_ID = 'DEKAF';
dispMug(); % displaying mug at the beginning

% Check input structures for valid and possibly deprecated options
options = assess_inputs(intel, options);

% Checking option defaults
[~,options.marchYesNo] = find_flagAndRemove('marching',varargin);
options = setDefaults(options);
options.Cooke = setCooke(intel);

if options.silenceWarnings % toggling warnings off
    warning ('off','all');
end

% Domain definition and computing edge conditions
[intel.eta,intel.D1,intel.D2] = build_mapping_vars(options.L,options.eta_i,options.N,options.chebydist);
switch options.inviscidFlowfieldTreatment
    case 'airfoil'                                                          % we want the airfoil treatment
        [intel, options] = convert_airfoil(intel, options);
    case {'custom','constant','1DNonEquilibriumEuler'}                      % we don't want the airfoil treatment
        [intel,options] = build_edge_values(intel,options);                     % Building edge quantities
    otherwise
        error(['The chosen inviscidFlowfieldTreatment ''',options.inviscidFlowfieldTreatment,''' is not supported']);
end

% If running CNE, initialize with TPG self-similar profile
optionsQSS = options;
optionsQSS.flow = options.noseFlow;
intelQSS = intel;

% If marching, remove dimoutput option for QSS
if options.marchYesNo
    [intelQSS,optionsQSS] = getQSS_dataStructures(intelQSS,optionsQSS);
end

% Run quasi-self-similar solver or use restart
if options.restartFromMarching                              % restart
    neededvalue(options,'intelQSS','a restart was chosen (options.restartFromMarching=true), but options.intelQSS was not found');
    intelQSS = update_intel4restart(intel,options.intelQSS,options);
    optionsQSS = rmfield(options,'intelQSS');
else                                                        % quasi-self-similar solver
    [intelQSS,optionsQSS] = DEKAF_QSS(intelQSS,optionsQSS);
end

% If the user wants to run a marching case
if options.marchYesNo
    [intel_out,optionsBL] = DEKAF_BL(intel,intelQSS,options);       % Run the marching solver
    if optionsBL.dimoutput
        if options.bReturnNonDimFieldsIfDimFail                     % we want to return the non-dimensional fields if the dimensionalization fails
            try                                                         % expecting errors
                intel_out = eval_dimVars(intel_out,optionsBL);              % Evaluate the dimensional variables
            catch ME                                                    % exiting with warning instead
                warning([parse_exception(ME), '\n', ...
                    'Skipping and outputting nondimensional self-similar solution set.'],errmsg_ID);
            end
        else                                                        % we want the code to break if the dimensionalization fails
            intel_out = eval_dimVars(intel_out,optionsBL);              % Evaluate the dimensional variables
        end
    end
    options = optionsBL;
else
    intel_out = intelQSS;
    options = optionsQSS;
end

% Get Git Info
intel_out.gitInfo=getGitInfo();
displayEpilog('Computed with');

if options.silenceWarnings % toggling warnings back on
    warning ('on','all');
end

end % DEKAF
