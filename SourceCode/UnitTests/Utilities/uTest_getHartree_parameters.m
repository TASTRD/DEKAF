function bPass = uTest_getHartree_parameters()
% uTest_getHartree_parameters is the unit test for getHartree_parameters
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% setting expected inputs and outputs
ic=1; % test index counter
%%% First test
dxi_dx = (1:10).';
xi_e = (0.01:0.01:0.1).';
U_e = repmat(1000,[1,10]).';
H_e = logspace(3,10,10).';
T_e = logspace(2,4,10).';
Dxe = magic(10);
inputs{ic}      = {dxi_dx,xi_e,U_e,H_e,T_e,Dxe};
Theta = 2*xi_e./H_e ./ dxi_dx .* (Dxe*H_e);
beta  = 2*xi_e./U_e ./ dxi_dx .* (Dxe*U_e);
theta = 2*xi_e./T_e ./ dxi_dx .* (Dxe*T_e);
expected{ic}    = {theta,Theta,beta}; ic=ic+1;

%%% Second test
inputs{ic}      = {1,2,4,10,55,pi}; % dxi_dx,xi_e,U_e,H_e,Ecu,Dxe
expected{ic}    = {0,0,0};          % theta,Theta,beta


%%% running tests
for ii=length(inputs):-1:1                                                              % looping tests
    [outputs{ii}{1:3}] = getHartree_parameters(inputs{ii}{:});                              % running function
    if isequal(expected{ii},outputs{ii});                               bPass(ii) = true;   % success
    elseif max(abs(1-expected{ii}{1}(:)./outputs{ii}{1}(:))) <10*eps && ...
           max(abs(1-expected{ii}{2}(:)./outputs{ii}{2}(:))) <10*eps && ...
           max(abs(1-expected{ii}{3}(:)./outputs{ii}{3}(:))) <10*eps
                                                                        bPass(ii) = true;   % success
    else;                                                               bPass(ii) = false;  % failure
    end
end

end % uTest_getHartree_parameters