function bPass = uTest_cell1D2matrix2D()
% uTest_cell1D2matrix2D is the unit test for cell1D2matrix2D
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% setting expected inputs and outputs
ic=1; % test index counter
inputs{ic}      = {1:5,9:13,1001:50:1201};
expected{ic}    = [1:5;9:13;1001:50:1201].'; ic=ic+1;
inputs{ic}      = {rand(5,5),rand(25,1)};
expected{ic}    = [inputs{ic}{1}(:),inputs{ic}{2}(:),]; ic=ic+1;
% running test
for ii=length(inputs):-1:1                  % looping tests
    outputs{ii} = cell1D2matrix2D(inputs{ii});     % running function
    bPass(ii) = isequal(outputs{ii},expected{ii});  % success condition
end

end % uTest_cell1D2matrix2D