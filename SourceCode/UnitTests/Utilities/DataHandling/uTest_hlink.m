function bPass = uTest_hlink()
% uTest_hlink is the unit test for hlink
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% setting expected inputs and outputs
ic=1; % test index counter
inputs{ic}      = {'DEKAF'};
expected{ic}    = ['<a href="matlab: opentoline(''',which('DEKAF.m'),''',1)">DEKAF</a>']; ic=ic+1;
inputs{ic}      = {'DEKAF','here'};
expected{ic}    = ['<a href="matlab: opentoline(''',which('DEKAF.m'),''',1)">here</a>']; ic=ic+1;
inputs{ic}      = {'DEKAF','here',25};
expected{ic}    = ['<a href="matlab: opentoline(''',which('DEKAF.m'),''',25)">here</a>']; ic=ic+1;
% running test
for ii=length(inputs):-1:1                  % looping tests
    outputs{ii} = hlink(inputs{ii}{:});         % running function
    bPass(ii) = isequal(outputs{ii},expected{ii});  % success condition
end

end % uTest_hlink