function [p_e,M_e,Re1_e,U_e,rho_e,intel] = build_edge_p_M_Re1_U_rho(TTrans_e,...
    mu_e,gam_e,R_e,Rstar_e,Dx,intel,options)
%BUILD_EDGE_P_M_RE1_U_RHO Calculate the edge pressure, Mach, unit Reynolds
% number, U velocity, and mass density using Bernoulli's inviscid
% streamline relation. Additionally, we update intel's fields while doing
% so and remove fields that are only used within this block and no further.
%
% Usage:
%   [p_e,M_e,Re1_e,U_e,rho_e,intel] = build_edge_p_M_Re1_U_rho(TTrans_e,...
%       mu_e,gam_e,R_e,Rstar_e,Dx,intel,options)
%
%   Inputs and Outputs:
%
%   TTrans_e        N_x x 1
%   mu_e            N_x x 1
%   gam_e           N_x x 1
%   R_e             N_x x 1
%   Rstar_e         N_x x 1
%   N_x             1 x 1
%   Dx              N_x x N_x
%   intel           struct (see build_edge_values)
%   options         struct with necessary fields
%                       .marchYesNo
%                       .flow
%
%   p_e             N_x x 1
%   M_e             N_x x 1
%   Re1_e           N_x x 1
%   U_e             N_x x 1
%   rho_e             N_x x 1
%   intel           struct
%
% Change log:
%   5/7/2018    F. Miro Miro    corrected bug #8
%
% Related functions
% See also build_edge_values
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% Making into column vectors
TTrans_e    = TTrans_e(:);
mu_e        = mu_e(:);
gam_e       = gam_e(:);
R_e         = R_e(:);
Rstar_e     = Rstar_e(:);
EoS_params   = intel.mixCnst.EoS_params;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% MARCHING BLOCK %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% Calculating the non-specified edge quantities
if length(TTrans_e)>1
    % then we have the additional condition of satisfying the bernoulli
    % equation along the inviscid streamline

    % we want the up-down integration matrix
    % (the initial x will be the first one)
    Ix = integral_mat(Dx,'ud');

    % We first check if a valid combination of inputs was provided
    if (isfield(intel,'p_e') && isfield(intel,'U_e'))       || ...
            (isfield(intel,'p_e') && isfield(intel,'M_e'))  || ...
            (isfield(intel,'M_e') && isfield(intel,'U_e'))

        error('Only one out of the three profiles p_e, U_e & M_e can be inputted.');
    end

    % If input is unit Reynolds number
    % Profiles are along x marching direction
    if isfield(intel,'Re1_e0')
        mu_e0 = mu_e(1);
        if isfield(intel,'p_e')
            % Velocity must be obtained from the Reynolds
            p_e = intel.p_e(:);
            rho_e0 = getMixture_rho([],p_e(1),TTrans_e(1),[],[],[],[],'specify_R',Rstar_e(1),EoS_params,options);
            intel.U_e0 = mu_e0 * intel.Re1_e0 / rho_e0;

        elseif isfield(intel,'U_e')
            rho_e0 = mu_e0 * intel.Re1_e0 / intel.U_e(1);
            intel.p_e0 = getMixture_p([],rho_e0,TTrans_e(1),[],[],[],[],'specify_R',Rstar_e(1),EoS_params,options);

        elseif isfield(intel,'M_e')
            intel.p_e0 = getInviscid_p_rho_U_fromMachRe(intel.M_e(1),intel.Re1_e0,mu_e0,gam_e(1),Rstar_e(1),TTrans_e(1),EoS_params,options);
        else
            error('Unit Reynolds Re1_e0 was input, but neither p_e, nor M_e, nor U_e were input!')
        end
    elseif isfield(intel,'rho_e0')
        rho_e0 = intel.rho_e0;
        intel.p_e0 = getMixture_p([],rho_e0,TTrans_e(1),[],[],[],[],'specify_R',Rstar_e(1),EoS_params,options);
    end

    % obtaining non-specified profiles
    if ~isfield(intel,'p_e') && ~isfield(intel,'U_e') && ~isfield(intel,'M_e')
        error('either the p_e(x), the M_e(x), or the U_e(x) profiles must be given as inputs');

    elseif isfield(intel,'p_e') && isfield(intel,'T_e') && isfield(intel,'U_e')
        error('all three profiles p_e(x), U_e(x) & T_e(x) were given as inputs, when in fact they cannot be satisfied simultaneously');

    elseif isfield(intel,'p_e') && isfield(intel,'T_e') && isfield(intel,'M_e')
        error('all three profiles p_e(x), M_e(x) & T_e(x) were given as inputs, when in fact they cannot be satisfied simultaneously');

    elseif isfield(intel,'p_e') && isfield(intel,'T_e')
        %%% we need to retrieve U_e & rho_e
        % To get the missing parameter (U_e) we use the bernoulli equation
        if ~isfield(intel,'U_e0')
            error('the p_e and T_e profiles were inputted, but not the U_e value at the first x_e location.');
        end

        U_e0 = intel.U_e0;
        p_e = intel.p_e(:);
        rho_e = getMixture_rho([],p_e,TTrans_e,[],[],[],[],'specify_R',Rstar_e,EoS_params,options);
        % derivative of p_e with x
        dpe_dx = Dx*p_e;
        % integrating Bernouilli's equation on the inviscid streamline
        bernoulliInt = Ix*(1./rho_e.*dpe_dx);
        if any(U_e0^2<2*bernoulliInt)
            error('the prescribed edge pressure profile leads to flow reversal at some point.');
        end
        U_e = sqrt(U_e0^2 - 2*bernoulliInt);
        % obtaining Mach number
        M_e = U_e./sqrt(gam_e.*p_e./rho_e);

    elseif isfield(intel,'U_e') && isfield(intel,'T_e')
        %%% we need to retrieve p_e & rho_e
        % To get the missing parameter (p_e) we use the bernoulli equation
        if ~isfield(intel,'p_e0')
            error('the U_e and T_e profiles were inputted, but not the p_e value at the first x_e location.');
        end

        p_e0 = intel.p_e0;
        U_e = intel.U_e(:);
        % computing with Bernouilli's equation
        [p_e,rho_e] = getInviscid_p_rho_Bernouilli(U_e,TTrans_e,Rstar_e,Dx,Ix,p_e0,EoS_params,options);
        % obtaining Mach number
        M_e = U_e./sqrt(gam_e.*p_e./rho_e);

    elseif isfield(intel,'M_e') && isfield(intel,'T_e')
        %%% we need to retrieve p_e & rho_e
        % To get the missing parameter (p_e) we use the bernoulli equation
        if ~isfield(intel,'p_e0')
            error('the M_e and T_e profiles were inputted, but not the p_e value at the first x_e location.');
        end

        p_e0 = intel.p_e0;
        M_e = intel.M_e(:);
        % computing with Bernouilli's equation
        [p_e,rho_e] = getInviscid_p_rho_Bernouilli_fromMach(M_e,gam_e,TTrans_e,Rstar_e,Dx,Ix,p_e0,EoS_params,options);
        % obtaining velocity
        U_e = M_e.*sqrt(gam_e.*p_e./rho_e);
    end
    % storing value of the Reynolds number at the edge (for display)
    Re1_e = rho_e .* U_e ./ mu_e;
    intel.Re1_e0 = Re1_e(1);

    %%%% DONE WITH MARCHING BLOCK

else
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%% NON-MARCHING BLOCK %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % We are not marching, so we don't have to satisfy the bernoulli
    % equation on the inviscid streamline
    %
    % Checking which fields have been inputted, and computing the rest. There
    % are several possibilities:
    %
    %  - The temperature must always be provided
    %  - Either the Mach or the Streamwise velocity must be provided
    %  - Either the pressure, the density, or the unit Reynolds number must be
    %  provided

    % error checking
    if isfield(intel,'M_e') && isfield(intel,'U_e')
        error('M_e and U_e were both defined, when there should only be one of them.');
    elseif sum(isfield(intel,{'rho_e','p_e','Re1_e'}))>1
        error('only one parameter out of p_e, rho_e and Re1 can be fixed.');
    elseif ~isfield(intel,'M_e') && ~isfield(intel,'U_e')
        error('neither M_e nor U_e were defined. One of them is needed.');
    elseif ~isfield(intel,'rho_e') && ~isfield(intel,'p_e') && ...
            ~isfield(intel,'Re1_e')
        error('neither rho_e, p_e nor Re1_e were defined. One of them is needed.');
    end

    % Checking on M_e or U_e
    if isfield(intel,'M_e') && intel.M_e == 0
        % Incompressible case, must be CPG
        % This block hasn't been changed. This is your area Koen!
        protectedvalue(options,'flow','CPG',['you chose a ',options.flow,' flow assumtion with a zero mach number. Most likely you''ve messed up somewhere.']);
        options     = standardvalue(options,'U_e',  30);
        options     = standardvalue(options,'rho_e',1.225);
        rho_e       = options.rho_e;
        U_e         = options.U_e;
        M_e         = intel.M_e;
        p_e         = getMixture_p([],rho_e,TTrans_e,[],[],[],[],'specify_R',Rstar_e,EoS_params,options);
    end

    % Checking on p_e, rho_e or Re1_e
    if isfield(intel,'p_e')
        p_e         = intel.p_e;      % extracting
        rho_e       = getMixture_rho([],p_e,TTrans_e,[],[],[],[],'specify_R',Rstar_e,EoS_params,options);
        if isfield(intel,'M_e')
            M_e = intel.M_e;
            U_e = M_e*sqrt(gam_e*p_e/rho_e);
        elseif isfield(intel,'U_e')
            U_e = intel.U_e;
            M_e = U_e/sqrt(gam_e*p_e/rho_e);
        end
        Re1_e       = rho_e*U_e/mu_e;
    elseif isfield(intel,'rho_e')
        rho_e       = intel.rho_e;    % extracting
        p_e         = getMixture_p([],rho_e,TTrans_e,[],[],[],[],'specify_R',Rstar_e,EoS_params,options);
        if isfield(intel,'M_e')
            M_e = intel.M_e;
            U_e = M_e*sqrt(gam_e*p_e/rho_e);
        elseif isfield(intel,'U_e')
            U_e = intel.U_e;
            M_e = U_e/sqrt(gam_e*p_e/rho_e);
        end
        Re1_e       = rho_e*U_e/mu_e;
    elseif isfield(intel,'Re1_e')
        Re1_e       = intel.Re1_e;
        if isfield(intel,'M_e')
            M_e = intel.M_e;
            [p_e,rho_e,U_e] = getInviscid_p_rho_U_fromMachRe(M_e,Re1_e,mu_e,gam_e,Rstar_e,TTrans_e,EoS_params,options);
        elseif isfield(intel,'U_e')
            U_e   = intel.U_e;
            rho_e = Re1_e*mu_e/U_e;
            p_e   = getMixture_p([],rho_e,TTrans_e,[],[],[],[],'specify_R',Rstar_e,EoS_params,options);
        end
    end

    %%%% DONE WITH NON-MARCHING BLOCK

end

% Adjust for sweep
[U_e,intel.W_0] = getVelocity_U_W(U_e,U_e(1),intel.Lambda_sweep);

end % build_edge_p_M_Re1_U_rho
