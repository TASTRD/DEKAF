function intel = build_mat(xi,intel,intelp,options)
% BUILD_MAT(xi,intel,intelp,options) builds the system matrices
% and forcing terms for all flow assumptions using the total-enthalpy
% energy equation.
%
% It receives as inputs:
% - xi at the current location
% - intel: structure containing the required flow field variables at the
% current location (sizes N_eta x 1)
% - intelp: structure containing the same fields as intel but for Previous
% xi locations. The fields will therefore have size N_eta x ox where ox is
% the order of the xi-marching algorithm.
% - options: structure containing the required options (further details in
% setDefaults.m
%
% The outputted matrix mat is the matrix operating on [f' f k g y1 y2 ...
% yNs ] where Ns is the number of species/elements minus one. If the
% spanwise velocity is zero, then k and the z-momentum equation are not
% part of the matrix system. The same goes for the species/elemental mass
% fractions and continuity equations if the flow assumption neglects
% diffusion (like in CPG, TPG or LTE).
%
% The use of turbulent models such as SSTkomega, which requires two
% conservation equations, claims for the corresponding KT and WT (turbulent
% kinetic energy and vorticity).
%
% intelp must also have the fluctuation quantities of all previous runs,
% named fh, dfh_deta, etc (h=hat), which are used in the evaluation of the
% xi derivatives of the fluctuation variables.
%
% For the non-marching modality (options.marchYesNo=false) intelp must
% be passed as empty and xi is irrelevant.
%
% Inside the function, three matrices are distinguished:
%   - A contains the base-state terms varying only in eta, that multiply
%   the fluctuation quantities.
%   - B contains the base-state terms varying in xi, that multiply the
%   fluctuation quantities.
%   - C contains the component of the discretization xi derivative of the
%   fluctuation quantities corresponding to the station being calculated at
%   that moment.
%   E.g.:  1st order FD - df/dxi = (f(xi_i+1)-f(xi_i))/delta_xi
%   C will account for the f(xi_i+1)/delta_xi and whatever base-state
%   quantities are multiplying it
%
% NOTE FOR DEVELOPERS: the work-flow of this function is better understood
% going from down to up - from the big picture to the particular terms in
% the matrices. Start at: mat = A+B+FDc(1)*Cc;
%
% The structure of the computational matrices is the following:
%
% A =                                                                                                 __  __    __
% |                                     |                                               |              |  |      |
% | momX,f'   momX,f       0        0         0        ...       0               0             0       |  |  f'  |
% |                                     |                                               |              |  |      |
% |   -I         D1        0        0         0        ...       0               0             0       |  |  f   |
% |                                     |                                               |              |  |      |
% |    0      momZ,f    momZ,k      0         0        ...       0               0             0       |  |  k   |
% |                                     |                                               |              |  |      |
% | ener,f'   ener,f    ener,k    ener,g   ener,y1     ...   ener,yNs-1      ener,yNs      ener,tauv   |  |  g   |
% | - - - - - - - - - - - - - - - - - - | - - - - - - - - - - - - - - - - - - - - - - - | - - - - - - -|  |- - - |
% |    0      spec1,f      0        0      spec1,y1    ...   spec1,yNs-1     spec1,yNs         0       |  |  y1  |
% |    .          .        .        .   |     .        .         .               .      |              |  |  .   |
% |    .          .        .        .         .         .        .               .                     |  |  .   |
% |    .          .        .        .   |     .          .       .               .      |              |  |  .   |
% |    0     specNs-1,f    0        0    specNs-1,y1   ... specNs-1,yNs-1  specNs-1,yNs        0       |  | yNs-1|
% | - - - - - - - - - - - - - - - - - - | - - - - - - - - - - - - - - - - - - - - - - - | - - - - - - -|  |- - - |
% |    0          0        0        0         I        ...       I               I             0       |  | yNs  |
% | - - - - - - - - - - - - - - - - - - | - - - - - - - - - - - - - - - - - - - - - - - | - - - - - - -|  |- - - |
% | Venr,f'   Venr,f       0      Venr,g   Venr,y1     ...   Venr,yNs-1      Venr,yNs      Venr,tauv   |  | tauv |
% |_                                    |                                               |             _|  |_    _|
%
% B =                                                                                                 __  __    __
% |                                     |                                               |              |  |      |
% | momX,f'       0        0        0         0        ...       0               0             0       |  |  f'  |
% |                                     |                                               |              |  |      |
% |   -I         D1        0        0         0        ...       0               0             0       |  |  f   |
% |                                     |                                               |              |  |      |
% | momZ,f'       0     momZ,k      0         0        ...       0               0             0       |  |  k   |
% |                                     |                                               |              |  |      |
% | ener,f'       0        0      ener,g      0        ...       0               0             0       |  |  g   |
% | - - - - - - - - - - - - - - - - - - | - - - - - - - - - - - - - - - - - - - - - - - | - - - - - - -|  |- - - |
% | spec1,f'      0        0     spec1,g   spec1,y1    ...   spec1,yNs-1     spec1,yNs     spec1,tauv  |  |  y1  |
% |    .          .        .        .   |     .        .         .               .      |              |  |  .   |
% |    .          .        .        .         .         .        .               .                     |  |  .   |
% |    .          .        .        .   |     .          .       .               .      |              |  |  .   |
% |specNs-1,f'    0        0  specNs-1,g specNs-1,y1   ... specNs-1,yNs-1  specNs-1,yNs  specNs-1,tauv |  | yNs-1|
% | - - - - - - - - - - - - - - - - - - | - - - - - - - - - - - - - - - - - - - - - - - | - - - - - - -|  |- - - |
% |    0          0        0        0         0        ...       0               0             0       |  | yNs  |
% | - - - - - - - - - - - - - - - - - - | - - - - - - - - - - - - - - - - - - - - - - - | - - - - - - -|  |- - - |
% | Venr,f'       0        0      Venr,g   Venr,y1     ...   Venr,yNs-1      Venr,yNs      Venr,tauv   |  | tauv |
% |_                                    |                                               |             _|  |_    _|
%
% C =                                                                                               __  __    __
% |                                     |                                               |            |  |      |
% | momX,f'   momX,f       0        0         0        ...       0               0            0      |  |  f'  |
% |                                     |                                               |            |  |      |
% |   -I         D1        0        0         0        ...       0               0            0      |  |  f   |
% |                                     |                                               |            |  |      |
% |    0      momZ,f    momZ,k      0         0        ...       0               0            0      |  |  k   |
% |                                     |                                               |            |  |      |
% |    0      ener,f       0      ener,g      0        ...       0               0            0      |  |  g   |
% | - - - - - - - - - - - - - - - - - - | - - - - - - - - - - - - - - - - - - - - - - - | - - - - - -|  |- - - |
% |    0     spec1,f       0        0      spec1,y1    ...       0               0            0      |  |  y1  |
% |    .          .        .        .   |     .        .         .               .      |            |  |  .   |
% |    .          .        .        .         .         .        .               .                   |  |  .   |
% |    .          .        .        .   |     .          .       .               .      |            |  |  .   |
% |    0     specNs-1,f    0        0         0        ... specNs-1,yNs-1        0            0      |  | yNs-1|
% | - - - - - - - - - - - - - - - - - - | - - - - - - - - - - - - - - - - - - - - - - - | - - - - - -|  |- - - |
% |    0          0        0        0         0        ...       0               0            0      |  | yNs  |
% | - - - - - - - - - - - - - - - - - - | - - - - - - - - - - - - - - - - - - - - - - - | - - - - - -|  |- - - |
% |    0      Venr,f       0        0         0        ...       0               0        Venr,tauv  |  | tauv |
% |_                                    |                                               |           _|  |_    _|
%
% If we don't have the Cooke option on, the 4th row and column will
% disappear (no w in the equations). Same with the species rows and columns
% for non-diffusing flow assumptions, and for the vibrational energy and
% energy conservation rows and columns for 1-T models.
%
% Additional rows and columns are added when using turbulent models such as
% the SSTkomega, which requires conservation equations for the turbulent
% kinetic energy and vorticity.
%
% Note that the last species row imposes that the sum of the species mass
% fractions must be identically one.
%
% General comments:
%    1) the species inputs (ys, Cs, etc) are cells with matrices
%    corresponding to the non-dimensional coordinate locations. For
%    example, ys{1} will correspond to the mass fraction of species 1, or
%    Csl{1,2} will be associated to the diffusion pair with species 1 and 2.
%    2) The size of the matrices will now be (4+Ns)N x (4+Ns)N, with
%    N_spec-1 species equations & mass fractions, one momentum x equation,
%    one momentum z equation, and one energy equation.
%    3) The expansion around zero of the perturbation terms is only done
%    for C, a0, a1, a2, j and Omegas. Moreover, this expansion is done only
%    considering g and df_deta variations, thus neglecting the species
%    jacobians (E.g. dC/dys), with the exception of the species source term
%    Omegas, that also includes the species jacobians. Not expanding
%    certain fluctuation quantities around zero does not neglect them, it
%    simply does not linearize them. They will still be updated with every
%    loop run, eventually reaching convergence.
%
% For a full explanation of the different variables and available options,
% see also: setDefaults, listOfVariablesExplained, DEKAF_QSS, DEKAF_BL,
% eval_thermo.
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

%% Extract variables from structures
% options and constants
if ~options.marchYesNo
    xi = 0; % we turn off the marching terms
end
D1      = intel.D1;
D2      = intel.D2;
norms   = intel.norms;
Theta   = intel.Theta;
theta   = intel.theta;
beta    = intel.beta;
if ismember(options.flow,{'TPGD','CNE'}) % setting up booleans for the multi-species case
    MultiSpec = true;
    N_spec  = options.N_spec;
    MultiElem = false;
    N_elem = 0;
    if options.bVariableIdxBath
        [~,idx_bath] = max(cell1D2matrix2D(intel.ys),[],2);
    else
        idx_bath = intel.mixCnst.idx_bath;
    end
elseif ismember(options.flow,{'LTEED'})
    MultiSpec = false;
    N_spec = 0;
    MultiElem = true;
    N_elem = intel.mixCnst.N_elem;
    idx_bath = intel.mixCnst.idx_bathElement;
else
    MultiSpec = false;
    N_spec = 0;
    MultiElem = false;
    N_elem = 0;
end

% flow properties
a0 = intel.a0;
a1 = intel.a1;
da1_deta = intel.da1_deta;
a2 = intel.a2;
da2_deta = intel.da2_deta;
jj = intel.j;  % we avoid using j, since matlab understands it as the complex unity
C = intel.C;
dC_deta = intel.dC_deta;

if MultiSpec
    bPairDiff = options.bPairDiff;
    zetas = intel.zetas;
    dzetas_deta = intel.dzetas_deta;
    dzetas_deta2 = intel.dzetas_deta2;
    as1 = intel.as1;
    das1_deta = intel.das1_deta;
    if bPairDiff   % for species-species diffusion, we will have asl2 and Csl
        asl2 = intel.asl2;
        dasl2_deta = intel.dasl2_deta;
        Csl = intel.Csl;
        dCsl_deta = intel.dCsl_deta;
    else                        % for species-mixture diffusion, we will have as2 and Cs
        as2 = intel.as2;
        das2_deta = intel.das2_deta;
        Cs = intel.Cs;
        dCs_deta = intel.dCs_deta;
    end
    % Omegas = intel.Omegas; % doesn't appear
    dOmegas_dg = intel.dOmegas_dg; % derivatives of the source term with the enthalpy (obtained using chain rule a million times)
    dOmegas_dyl = intel.dOmegas_dyl; % jacobian of the source term with the species concentrations
    dOmegas_ddfdeta = intel.dOmegas_ddfdeta;
    % if the user chose the diffusion coefficients to be applied on the
    % mass fluxes, we must make zetas equal to zero
    if ~options.molarDiffusion
        ambipolar.zetas = zetas; % storing the actual zetas for when we impose the ambipolar condition
        N_eta = length(zetas{1});
        for s=1:N_spec
            zetas{s} = ones(N_eta,1);
            dzetas_deta{s} = zeros(N_eta,1);
            dzetas_deta2{s} = zeros(N_eta,1);
        end
        intel.zetas = zetas;                % throwing them back into intel so that eval_forcing also has these changes
        intel.dzetas_deta = dzetas_deta;
        intel.dzetas_deta2 = dzetas_deta2;
    else
        ambipolar.zetas = zetas;
    end
elseif MultiElem
    CYEF        = intel.CYEF;
    agE         = intel.agE;
    auE         = intel.auE;
    aYE         = intel.aYE;
    dCYEF_deta  = intel.dCYEF_deta;
    dagE_deta   = intel.dagE_deta;
    dauE_deta   = intel.dauE_deta;
    daYE_deta   = intel.daYE_deta;
end

% State variables
f          = intel.f;
df_deta    = intel.df_deta;
df_deta2   = intel.df_deta2;
df_deta3   = intel.df_deta3;
g          = intel.g;
dg_deta    = intel.dg_deta;

dk_deta    = intel.dk_deta;

if MultiSpec
    ys          = intel.ys;
    dys_deta    = intel.dys_deta;
elseif MultiElem
    dyE_deta    = intel.dyE_deta;
end

if options.marchYesNo
    df_dxi      = intel.df_dxi;
    dg_dxi      = intel.dg_dxi;
    df_detadxi  = intel.df_detadxi;
    dk_dxi      = intel.dk_dxi;
    if MultiSpec
        dys_dxi     = intel.dys_dxi;
    elseif MultiElem
        dyE_dxi     = intel.dyE_dxi;
    end

    % retrieving Finite-Difference stencil and applying scaling factor
    FDc = intel.FDc_xi.';
    intelp.FDc = FDc; % storing for the final evaluation
end

if strcmp(options.numberOfTemp,'2T')
    bElectron = intel.mixCnst.bElectron;
    Ecu = intel.Ecu;
    Ecp = intel.Ecp;

    av1 = intel.av1;
    dav1_deta = intel.dav1_deta;
    av2 = intel.av2;
    dav2_deta = intel.dav2_deta;
    Cpv = intel.Cpv;
    dCpv_dg = intel.dCpv_dg;
    dCpv_ddfdeta = intel.dCpv_ddfdeta;
    dCpv_dtauv = intel.dCpv_dtauv;

    tauv          = intel.tauv;
    dtauv_deta    = intel.dtauv_deta;
    if options.marchYesNo
    %     OmegaV = intel.OmegaV; % doesn't appear
        dOmegaV_dg = intel.dOmegaV_dg; % derivatives of the vibrational source term with the enthalpy (obtained using chain rule a million times)
        dOmegaV_ddfdeta = intel.dOmegaV_ddfdeta;
        dOmegaV_dtauv = intel.dOmegaV_dtauv;
        dtauv_dxi      = intel.dtauv_dxi;
    end
    if MultiSpec
        dCpv_dys = intel.dCpv_dys;
        dOmegaV_dys = intel.dOmegaV_dys; % jacobian of the source term with the species concentrations
        dOmegas_dtauv = intel.dOmegas_dtauv; % derivatives of the source term with the enthalpy (obtained using chain rule a million times)
        if bPairDiff    % we will have asl2 and Csl
            avsl2 = intel.avsl2;
        else                    % we will have as2 and Cs
            avs2 = intel.avs2;
        end
        if options.marchYesNo
            dzetas_dxi = intel.dzetas_dxi;
            for s=1:N_spec
                dzetas_dxi{s} = zeros(N_eta,1);
            end
            intel.dzetas_dxi = dzetas_dxi;
        end
    end
end

if ismember(options.modelTurbulence,{'SSTkomega'})
    KT          = intel.KT;
    dKT_deta    = intel.dKT_deta;
    WT          = intel.WT;
    dWT_deta    = intel.dWT_deta;
    AK          = intel.AK;
    AW          = intel.AW;
    BW          = intel.BW;
    ThetaK      = intel.ThetaK;
    ThetaW      = intel.ThetaW;
    aK0         = intel.aK0;
    aK1         = intel.aK1;
    aK2         = intel.aK2;
    daK2_deta   = intel.daK2_deta;
    aW0         = intel.aW0;
    aW1         = intel.aW1;
    aW2         = intel.aW2;
    daW2_deta   = intel.daW2_deta;
    if options.marchYesNo
        dKT_dxi = intel.dKT_dxi;
        dWT_dxi = intel.dWT_dxi;
    end
end

%% Define the matrix entries (the Cooke entries are evaluated later on)
% We distinguish two matrices:
%  - A: the one without marching terms (varying in xi)
%  - B: the one with marching terms
%  - C: the one containing the terms resulting from the evaluation of the
%  xi derivatives of the fluctuating variables

N = length(f);

% A matrix
% X-momentum equation
A11 = sparse(diag(C)*D2 + diag(dC_deta+f)*D1 + diag(-2.*beta.*df_deta)); % term multiplying dfh_deta
A12 = sparse(diag(df_deta2)); % term multiplying fh
if options.Cooke
    % Z-momentum equation
    A32 = sparse(diag(dk_deta)); % term multiplying fh
    A33 = sparse(diag(C)*D2 + diag(dC_deta+f)*D1); % term multiplying kh
end
% Energy equation
A41 = sparse(diag(a2.*df_deta)*D2 + diag(2.*a2.*df_deta2+da2_deta.*df_deta)*D1 + diag(-g.*Theta+a2.*df_deta3+da2_deta.*df_deta2)); % term multiplying dfh_deta
A42 = sparse(diag(dg_deta)); % term multiplying fh
A43 = sparse(diag(2.*a0.*dk_deta)*D1); % term multiplying kh
A44 = sparse(diag(a1)*D2 + diag(f+da1_deta)*D1 + diag(-df_deta.*Theta)); % term multiplying gh
if strcmp(options.numberOfTemp,'2T')
    A4v = sparse(diag(av2)*D2 + diag(dav2_deta)*D1); % term multiplying tauvF
    % Vibrational energy equation
    Av1 = sparse(diag((-dCpv_ddfdeta.*df_deta.*tauv.*theta)-Cpv.*tauv.*theta+dCpv_ddfdeta.*dtauv_deta.*f)); % term multiplying dfh_deta
    Av2 = sparse(diag(Cpv.*dtauv_deta)); % term multiplying fh
    Av4 = sparse(diag(dCpv_dg.*dtauv_deta.*f-dCpv_dg.*df_deta.*tauv.*theta)); % term multiplying gh
    Avv = sparse(diag(av1)*D2 + diag(Cpv.*f+dav1_deta)*D1 + diag((-dCpv_dtauv.*df_deta.*tauv.*theta)-Cpv.*df_deta.*theta+dCpv_dtauv.*dtauv_deta.*f)); % term multiplying tauvh
end

if options.marchYesNo
    % B & C matrix
    % X-momentum equation
    B11 = sparse(diag(2.*df_dxi.*xi)*D1 + diag(-2.*df_detadxi.*xi)); % term multiplying dfh_deta
    C11 = sparse(diag(-2.*df_deta.*xi)); % term multiplying .*dfh_detadxi
    C12 = sparse(diag(2.*df_deta2.*xi)); % term multiplying dfh_dxi
    if options.Cooke
        % Z-momentum equation
        B31 = sparse(diag(-2.*dk_dxi.*xi)); % term multiplying dfh_deta
        B33 = sparse(diag(2.*df_dxi.*xi)*D1); % term multiplying kh
        C32 = sparse(diag(2.*dk_deta.*xi)); % term multiplying dfh_dxi
        C33 = sparse(diag(-2.*df_deta.*xi)); % term multiplying dkh_dxi
    end
    % Energy equation
    B41 = sparse(diag(-2.*dg_dxi.*xi)); % term multiplying dfh_deta
    B44 = sparse(diag(2.*df_dxi.*xi)*D1); % term multiplying gh
    C42 = sparse(diag(2.*dg_deta.*xi)); % term multiplying dfh_dxi
    C44 = sparse(diag(-2.*df_deta.*xi)); % term multiplying dgh_dxi
    if strcmp(options.numberOfTemp,'2T')
        Bv1 = sparse(diag((-2.*dCpv_ddfdeta.*df_deta.*dtauv_dxi.*xi)-2.*Cpv.*dtauv_dxi.*xi+2.*dCpv_ddfdeta.*df_dxi.*dtauv_deta.*xi+2.*dOmegaV_ddfdeta.*xi)); % term multiplying dfh_deta
        Bv4 = sparse(diag((-2.*dCpv_dg.*df_deta.*dtauv_dxi.*xi)+2.*dCpv_dg.*df_dxi.*dtauv_deta.*xi+2.*dOmegaV_dg.*xi)); % term multiplying gh
        Bvv = sparse(diag(2.*Cpv.*df_dxi.*xi)*D1 + diag((-2.*dCpv_dtauv.*df_deta.*dtauv_dxi.*xi)+2.*dCpv_dtauv.*df_dxi.*dtauv_deta.*xi+2.*dOmegaV_dtauv.*xi)); % term multiplying tauvh
        Cv2 = sparse(diag(2.*Cpv.*dtauv_deta.*xi)); % term multiplying dfh_dxi
        Cvv = sparse(diag(-2.*Cpv.*df_deta.*xi)); % term multiplying dtauvh_dxi
    end
end

% Until here (almost) everything is monospecies. From now on starts the
% funky multispecies stuff
if MultiSpec
    % building species columns corresponding to species terms in the momentum
    % and energy equations
    if strcmp(options.numberOfTemp,'2T') % additional terms for two-temperature models
        Avs{N_spec} = [];
        Bvs{N_spec} = [];
        Cvs{N_spec} = [];
    end
    if bPairDiff % spec-spec diffusion
        % Energy equation
        A4s = repmat({zeros(N,N)},[1,N_spec]); % allocating
        for s=1:N_spec
            A4s{s} = A4s{s} + sparse(diag(-as1{s})*D2 + diag(-das1_deta{s})*D1);     % term multiplying ysh{s}
            for l=1:N_spec % terms that need to be summed over the s index
                A4s{l} = A4s{l} + sparse(diag(asl2{s,l}.*zetas{l})*D2 + diag(dasl2_deta{s,l}.*zetas{l}+2.*asl2{s,l}.*dzetas_deta{l})*D1 + diag(asl2{s,l}.*dzetas_deta2{l}+dasl2_deta{s,l}.*dzetas_deta{l})); % term multiplying ysh{l}
            end
        end
        if strcmp(options.numberOfTemp,'2T') % additional terms for two-temperature models
            % terms multiplying ysh{s}
            for s=1:N_spec
                if options.marchYesNo
                    Bv1 = Bv1 + sparse(diag(2.*Ecp.*bElectron(s).*dys_dxi{s}.*jj.*xi.*zetas{s}+2.*Ecp.*bElectron(s).*dzetas_dxi{s}.*jj.*xi.*ys{s})); % term multiplying dfh_deta
                    Bvs{s} = sparse(diag(-2.*Ecp.*bElectron(s).*df_dxi.*jj.*xi.*zetas{s})*D1 + diag(2.*Ecp.*bElectron(s).*df_deta.*dzetas_dxi{s}.*jj.*xi-2.*Ecp.*bElectron(s).*df_dxi.*dzetas_deta{s}.*jj.*xi)) + sparse(diag((-2.*dCpv_dys{s}.*df_deta.*dtauv_dxi.*xi)+2.*dCpv_dys{s}.*df_dxi.*dtauv_deta.*xi+2.*dOmegaV_dys{s}.*xi)); % term multiplying ysh{s}
                    Cvs{s} = sparse(diag(2.*Ecp.*bElectron(s).*df_deta.*jj.*xi.*zetas{s})); % term multiplying dysh_dxi{s}
                    Cv2 = Cv2 + sparse(diag(-2.*Ecp.*bElectron(s).*dys_deta{s}.*jj.*xi.*zetas{s}-2.*Ecp.*bElectron(s).*dzetas_deta{s}.*jj.*xi.*ys{s})); % term multiplying dfh_dxi
                end
            end
            % terms multiplying ysh{l}
            Avs = repmat({zeros(N,N)},[1,N_spec]); % allocating
            for s=1:N_spec % terms that need to be summed over the s index
                Av1 = Av1 + sparse(diag(-Ecu.*bElectron(s).*beta.*jj.*ys{s}.*zetas{s})); % term multiplying dfh_deta
                Av2 = Av2 + sparse(diag((-Ecp.*bElectron(s).*dys_deta{s}.*jj.*zetas{s})-Ecp.*bElectron(s).*dzetas_deta{s}.*jj.*ys{s})); % term multiplying fh
                Avs{s} = Avs{s} + sparse(diag(-Ecp.*bElectron(s).*f.*jj.*zetas{s})*D1 + diag((-Ecu.*bElectron(s).*beta.*df_deta.*jj.*zetas{s})-Ecp.*bElectron(s).*dzetas_deta{s}.*f.*jj)) + sparse(diag(dCpv_dys{s}.*dtauv_deta.*f-dCpv_dys{s}.*df_deta.*tauv.*theta)); % term multiplying ysh{s}
                for l=1:N_spec
                    Avs{l} = Avs{l} + sparse(diag(avsl2{s,l}.*dtauv_deta.*zetas{l})*D1 + diag(avsl2{s,l}.*dtauv_deta.*dzetas_deta{l})); % term multiplying ysh{l}
                    Avv = Avv + sparse(diag(avsl2{s,l}.*dys_deta{l}.*zetas{l}+avsl2{s,l}.*dzetas_deta{l}.*ys{l})*D1); % term multiplying tauvh
                end
            end
        end
    else % spec-mix diffusion
        % Energy equation
        A4s = repmat({zeros(N,N)},[1,N_spec]); % allocating
        for s=1:N_spec % there are no convective terms involving the species mass fractions in the
            % energy equations, which means that the corresponding B submatrices will be empty
            A4s{s} = sparse(diag(as2{s}.*zetas{s}-as1{s})*D2 + diag(das2_deta{s}.*zetas{s}+2.*as2{s}.*dzetas_deta{s}-das1_deta{s})*D1 + diag(as2{s}.*dzetas_deta2{s}+das2_deta{s}.*dzetas_deta{s})); % term multiplying ysh{s}
            if strcmp(options.numberOfTemp,'2T') % additional terms for two-temperature models
                Av1 = Av1 + sparse(diag(-Ecu.*bElectron(s).*beta.*jj.*ys{s}.*zetas{s})); % term multiplying dfh_deta
                Av2 = Av2 + sparse(diag((-Ecp.*bElectron(s).*dys_deta{s}.*jj.*zetas{s})-Ecp.*bElectron(s).*dzetas_deta{s}.*jj.*ys{s})); % term multiplying fh
                Avv = Avv + sparse(diag(avs2{s}.*dys_deta{s}.*zetas{s}+avs2{s}.*dzetas_deta{s}.*ys{s})*D1); % term multiplying tauvh
                Avs{s} = sparse(diag(avs2{s}.*dtauv_deta.*zetas{s}-Ecp.*bElectron(s).*f.*jj.*zetas{s})*D1 + diag((-Ecu.*bElectron(s).*beta.*df_deta.*jj.*zetas{s})-Ecp.*bElectron(s).*dzetas_deta{s}.*f.*jj+avs2{s}.*dtauv_deta.*dzetas_deta{s})) + sparse(diag(dCpv_dys{s}.*dtauv_deta.*f-dCpv_dys{s}.*df_deta.*tauv.*theta)); % term multiplying ysh{s}  in the vibrational energy equation
                if options.marchYesNo
                    Bv1 = Bv1 + sparse(diag(2.*Ecp.*bElectron(s).*dys_dxi{s}.*jj.*xi.*zetas{s}+2.*Ecp.*bElectron(s).*dzetas_dxi{s}.*jj.*xi.*ys{s})); % term multiplying dfh_deta
                    Bvs{s} = sparse(diag(-2.*Ecp.*bElectron(s).*df_dxi.*jj.*xi.*zetas{s})*D1 + diag(2.*Ecp.*bElectron(s).*df_deta.*dzetas_dxi{s}.*jj.*xi-2.*Ecp.*bElectron(s).*df_dxi.*dzetas_deta{s}.*jj.*xi)) + sparse(diag((-2.*dCpv_dys{s}.*df_deta.*dtauv_dxi.*xi)+2.*dCpv_dys{s}.*df_dxi.*dtauv_deta.*xi+2.*dOmegaV_dys{s}.*xi)); % term multiplying ysh{s}  in the vibrational energy equation
                    Cvs{s} = sparse(diag(2.*Ecp.*bElectron(s).*df_deta.*jj.*xi.*zetas{s})); % term multiplying dysh_dxi{s}
                    Cv2 = Cv2 + sparse(diag(-2.*Ecp.*bElectron(s).*dys_deta{s}.*jj.*xi.*zetas{s}-2.*Ecp.*bElectron(s).*dzetas_deta{s}.*jj.*xi.*ys{s})); % term multiplying dfh_dxi
                end
            end
        end
    end % close spec-spec / spec-spec loop

    % Species conservation equations
    As1{N_spec} = [];
    As2{N_spec} = [];
    As4{N_spec} = [];
    Asl = repmat({zeros(N,N)},[N_spec,N_spec]); % allocating
    if options.marchYesNo
        Bs1{N_spec} = []; % allocating
        Bs4{N_spec} = [];
        Bsl{N_spec,N_spec} = [];
        Cs2{N_spec} = [];
        Csd{N_spec} = []; % diagonal terms in the 2N_spec x 2N_spec block
        if strcmp(options.numberOfTemp,'2T')% additional fields for 2-T models
            Bsv{N_spec} = [];
        end
    end
    for s=1:N_spec % looping equations
        As1{s} = sparse(N,N);                   % term multiplying dfh_deta
        As2{s} = sparse(diag(dys_deta{s}));     % term multiplying fh
        As4{s} = sparse(N,N);                   % term multiplying gh
        if options.marchYesNo
            Bs1{s} = sparse(diag(2.*xi.*dOmegas_ddfdeta{s}-2.*dys_dxi{s}.*xi)); % term multiplying dfh_deta
            Bs4{s} = sparse(diag(2.*xi.*dOmegas_dg{s})); % term multiplying gh
            Cs2{s} = sparse(diag( 2.*dys_deta{s}.*xi)); % term multiplying dfh_dxi
            Csd{s} = sparse(diag(-2.*df_deta.*xi)); % term multiplying dysh{s}_dxi
            if strcmp(options.numberOfTemp,'2T')
                Bsv{s} = sparse(diag(2.*xi.*dOmegas_dtauv{s})); % term multiplying tauvh
            end
        end
        if bPairDiff
            Asl{s,s} = Asl{s,s} + sparse(diag(f)*D1); % term multiplying ysh{s} without s,l index
            for l=1:N_spec
                if options.marchYesNo
                    Bsl{s,l} = sparse(diag(2.*xi.*dOmegas_dyl{s,l})); % term multiplying ysh{l}
                else
                    Bsl{s,l} = sparse(N,N);
                end
                % term multiplying yl in equation s
                Asl{s,l} = Asl{s,l} + sparse(diag(Csl{s,l}.*zetas{l})*D2 + diag(zetas{l}.*dCsl_deta{s,l}+2.*Csl{s,l}.*dzetas_deta{l})*D1 + diag(dzetas_deta{l}.*dCsl_deta{s,l}+Csl{s,l}.*dzetas_deta2{l})); % term multiplying ysh{l}
                if s==l
                    if options.marchYesNo
                        Bsl{s,l} = Bsl{s,l} + sparse(diag(2.*df_dxi.*xi)*D1); % term multiplying ysh{s}
                    end
                end
            end
        else                                        % spec-mix diffusion
            for l=1:N_spec % looping variables
                if options.marchYesNo
                    Bsl{s,l} = sparse(diag(2.*xi.*dOmegas_dyl{s,l})); % term multiplying ysh{l}
                else
                    Bsl{s,l} = sparse(N,N);
                end
                if s==l % this diffusion theory only has terms in each species equation depending on that species
                    Asl{s,l} = sparse(diag(Cs{s}.*zetas{s})*D2 + diag(zetas{s}.*dCs_deta{s}+f+2.*Cs{s}.*dzetas_deta{s})*D1 + diag(dzetas_deta{s}.*dCs_deta{s}+Cs{s}.*dzetas_deta2{s})); % term multiplying ysh{s}
                    if options.marchYesNo
                        Bsl{s,l} = Bsl{s,l} + sparse(diag(2.*df_dxi.*xi)*D1); % term multiplying ysh{s}
                    end
                else
                    % we normally don't fix submatrices to zero, such that when
                    % assembling, it is clear which matrices must be zero and
                    % which don't. However, in this case, since this is the only
                    % difference between the multisolver cases, we let it like
                    % this in order to then make a unified assembling instead of
                    % one for each case
                    Asl{s,l} = sparse(N,N);
                end
                % Csl{s,l} only has components on its diagonal, which is why they are defined as Csd
            end
        end
    end
elseif MultiElem
    % building species columns corresponding to element terms in the momentum
    % and energy equations
    A4s{N_elem} = [];
    As1{N_elem} = [];
    As2{N_elem} = [];
    As4{N_elem} = [];
    Asl = repmat({zeros(N,N)},[N_elem,N_elem]); % allocating
    Bs1{N_elem} = [];
    Bs4{N_elem} = [];
    Bsl = repmat({zeros(N,N)},[N_elem,N_elem]); % allocating
    Cs2{N_elem} = [];
    Csd{N_elem} = []; % diagonal terms in the N_elem x N_elem block
    
    for E=1:N_elem % looping elements
        A4s{E} = sparse(diag(aYE{E})*D2 + diag(daYE_deta{E})*D1);                       % term multiplying yEh{E} in the energy equation
        As1{E} = sparse(diag(-auE{E}.*df_deta)*D2 + diag((-2.*auE{E}.*df_deta2)-dauE_deta{E}.*df_deta)*D1 + diag((-auE{E}.*df_deta3)-dauE_deta{E}.*df_deta2)); % term multiplying dfh_deta
        As2{E} = sparse(diag(dyE_deta{E}));                                             % term multiplying fh
        As4{E} = sparse(diag(agE{E})*D2 + diag(dagE_deta{E})*D1);                       % term multiplying gh
        Asl{E,E} = Asl{E,E} + sparse(diag(f)*D1);                                       % term multiplying yEh{E}
        for F=1:N_elem                                                                  % looping elements
            Asl{E,F} = Asl{E,F} + sparse(diag(CYEF{E,F})*D2 + diag(dCYEF_deta{E,F})*D1);    % term multiplying yEh{F}
        end
        if options.marchYesNo
            Bs1{E} = sparse(diag(-2.*dyE_dxi{E}.*xi));                                      % term multiplying dfh_deta
            Bs4{E} = zeros(N,N);
            Bsl{E,E} = sparse(diag(2.*df_dxi.*xi)*D1);                                      % term multiplying yEh{E}
            Cs2{E} = sparse(diag( 2.*dyE_deta{E}.*xi)); % term multiplying dfh_dxi
            Csd{E} = sparse(diag(-2.*df_deta.*xi)); % term multiplying dyEh{E}_dxi
        end
    end
end

% Additional turbulent equations
if ismember(options.modelTurbulence,{'SSTkomega'})
    % Turbulent-kinetic-energy equation
    AT11 = sparse(diag(2.*aK1.*df_deta2)*D1 + diag(-KT.*ThetaK));                   % term multiplying dfh_deta
    AT12 = sparse(diag(dKT_deta));                                                  % term multiplying fh
    AT13 = sparse(diag(2.*aK0.*dk_deta)*D1);                                        % term multiplying kh
    AT15 = sparse(diag(aK2)*D2 + diag(f+daK2_deta)*D1 + diag(-ThetaK.*df_deta));    % term multiplying KTh
    if options.marchYesNo
        BT11 = sparse(diag(-2.*dKT_dxi.*xi));                                       % term multiplying dfh_deta
        BT15 = sparse(diag(2.*df_dxi.*xi)*D1 + diag(-2.*AK.*WT.*xi));               % term multiplying KTh
        BT16 = sparse(diag(-2.*AK.*KT.*xi));                                        % term multiplying WTh
        CT12 = sparse(diag(2.*dKT_deta.*xi));                                       % term multiplying fh
        CT15 = sparse(diag(-2.*df_deta.*xi));                                       % term multiplying KTh
    end
    % Turbulent-vorticity-energy equation
    AT21 = sparse(diag(2.*aW1.*df_deta2)*D1 + diag(-ThetaW.*WT));                   % term multiplying dfh_deta
    AT22 = sparse(diag(dWT_deta));                                                  % term multiplying fh
    AT23 = sparse(diag(2.*aW0.*dk_deta)*D1);                                        % term multiplying kh
    AT25 = sparse(diag(BW.*dWT_deta)*D1);                                           % term multiplying KTh
    AT26 = sparse(diag(aW2)*D2 + diag(f+daW2_deta+BW.*dKT_deta)*D1 + diag(-ThetaW.*df_deta)); % term multiplying WTh
    if options.marchYesNo
        BT21 = sparse(diag(-2.*dWT_dxi.*xi));                                       % term multiplying dfh_deta
        BT26 = sparse(diag(2.*df_dxi.*xi)*D1 + diag(-4.*AW.*WT.*xi));               % term multiplying WTh
        CT22 = sparse(diag(2.*dWT_deta.*xi));                                       % term multiplying fh
        CT26 = sparse(diag(-2.*df_deta.*xi));                                       % term multiplying WTh
    end
end
%% Assembling matrices
O = sparse(N,N);
I = speye(N);

if options.Cooke
    A_fkg = [   A11 A12  O   O  ; % submatrix of mom/ener eqs with fkg variables
                -I   D1  O   O  ;
                 O  A32 A33  O  ;
                A41 A42 A43 A44 ]; % be careful, do not confuse Os with zeros 0
    if options.marchYesNo
        B_fkg =  [  B11  O   O   O  ;
                     O   O   O   O  ;
                    B31  O  B33  O  ;
                    B41  O   O  B44 ];

        C_fkg =  [ C11 C12  O   O  ;
                    O   O   O   O  ;
                    O  C32 C33  O  ;
                    O  C42  O  C44 ];
    end
    N_var = 4;
    if ismember(options.modelTurbulence,{'SSTkomega'})
        A_fkg = [      A_fkg         zeros(N_var*N,2*N) ;
                  AT11 AT12 AT13  O   AT15    O         ;
                  AT21 AT22 AT23  O   AT25   AT26       ];
        if options.marchYesNo
            B_fkg = [      B_fkg         zeros(N_var*N,2*N) ;
                      BT11  O    O    O   BT15   BT16       ;
                      BT21  O    O    O    O     BT26       ];
            C_fkg = [      C_fkg         zeros(N_var*N,2*N) ;
                       O   CT12  O    O   CT15    O         ;
                       O   CT22  O    O    O     CT26       ];
        end
        N_var = N_var+1;
    end
else
    A_fkg = [   A11 A12  O  ; % submatrix of mom/ener eqs with fkg variables
                -I   D1  O  ;
                A41 A42 A44 ]; % be careful, do not confuse Os with zeros 0
    if options.marchYesNo
        B_fkg =  [  B11  O   O  ;
                     O   O   O  ;
                    B41  O  B44 ];

        C_fkg =  [ C11 C12  O  ;
                    O   O   O  ;
                    O  C42 C44 ];
    end
    N_var = 3;
    if ismember(options.modelTurbulence,{'SSTkomega'})
        A_fkg = [      A_fkg         zeros(N_var*N,2*N) ;
                  AT11 AT12  O   AT15    O         ;
                  AT21 AT22  O   AT25   AT26       ];
        if options.marchYesNo
            B_fkg = [      B_fkg         zeros(N_var*N,2*N) ;
                      BT11   O    O   BT15   BT16       ;
                      BT21   O    O    O     BT26       ];
            C_fkg = [      C_fkg         zeros(N_var*N,2*N) ;
                       O   CT12   O   CT15    O         ;
                       O   CT22   O    O     CT26       ];
        end
        N_var = N_var+1;
    end
end

if MultiSpec||MultiElem % only for multi-species or multi-element cases
    if MultiElem
        N_spec = N_elem;            % we define N_spec to N_elem, such that the assembling is the same
    end
    A_rows = zeros(N_spec*N,N_var*N); % submatrix of rows (species equations with fkg variables)
    if options.marchYesNo
        B_rows = zeros((N_spec-1)*N,N_var*N);
        C_rows = zeros((N_spec-1)*N,N_var*N);
    end
    if options.Cooke
        for s=1:N_spec % looping equations
            A_rows(N*(s-1)+1 : N*s,:) = [ As1{s} As2{s}  O  As4{s} ];
            if options.marchYesNo
                B_rows(N*(s-1)+1 : N*s,:) = [Bs1{s}  O    O Bs4{s} ];
                C_rows(N*(s-1)+1 : N*s,:) = [  O   Cs2{s} O   O    ];
            end
        end
    else
        for s=1:N_spec % looping equations
            A_rows(N*(s-1)+1 : N*s,:) = [ As1{s} As2{s}  As4{s} ];
            if options.marchYesNo
                B_rows(N*(s-1)+1 : N*s,:) = [Bs1{s}  O    Bs4{s} ];
                C_rows(N*(s-1)+1 : N*s,:) = [  O   Cs2{s}   O    ];
            end
        end
    end
    A_rows = sparse(A_rows);
    if options.marchYesNo
        B_rows = sparse(B_rows);
        C_rows = sparse(C_rows);
    end

    A_cols = zeros(N_var*N,N_spec*N); % submatrix of columns (mom/ener eqs with species variables)
    if options.marchYesNo
        B_cols = zeros(N_var*N,N_spec*N);
        C_cols = zeros(N_var*N,N_spec*N);
    end
    if options.Cooke
        for s=1:N_spec % looping variables
            A_cols(:,N*(s-1)+1 : N*s) = [ O ; O ; O ;A4s{s}];
            if options.marchYesNo
                B_cols(:,N*(s-1)+1 : N*s) = [ O ; O ; O ; O ]; % these definitions are quite useless, but they are done nonetheless for clarity
                C_cols(:,N*(s-1)+1 : N*s) = [ O ; O ; O ; O ];
            end
        end
    else
        for s=1:N_spec % looping variables
            A_cols(:,N*(s-1)+1 : N*s) = [ O ; O ; A4s{s}];
            if options.marchYesNo
                B_cols(:,N*(s-1)+1 : N*s) = [ O ; O ; O ]; % these definitions are quite useless, but they are done nonetheless for clarity
                C_cols(:,N*(s-1)+1 : N*s) = [ O ; O ; O ];
            end
        end
    end
    A_cols = sparse(A_cols);
    if options.marchYesNo
        B_cols = sparse(B_cols);
        C_cols = sparse(C_cols);
    end

    A_spec = zeros(N_spec*N,N_spec*N); % submatrix with species equations with species variables
    if options.marchYesNo
        B_spec = zeros(N_spec*N,N_spec*N);
        C_spec = zeros(N_spec*N,N_spec*N);
    end
    for s=1:N_spec % looping equations
        for l=1:N_spec % looping variables
            A_spec(N*(s-1)+1 : N*s , N*(l-1)+1 : N*l) = Asl{s,l}; % yl in eq of spec s
            B_spec(N*(s-1)+1 : N*s , N*(l-1)+1 : N*l) = Bsl{s,l}; % yl in eq of spec s
            if s==l % we must include the definition of the derivative of ys
                if options.marchYesNo
                    C_spec(N*(s-1)+1 : N*s , N*(s-1)+1 : N*s) = Csd{s}; % ys in eq of spec s
                end
            end
        end
    end
    A_spec = sparse(A_spec);
    if options.marchYesNo
        B_spec = sparse(B_spec);
        C_spec = sparse(C_spec);
    end

    % Defining the two submatrices corresponding to the imposition of the sum
    % of the concentrations equal to zero
    A_cncf = zeros(N,N_var*N); % CoNcentration Condition for Fkg variables (all zeros)
    A_cncS = zeros(N,N_spec*N); % CoNcentration Condition for Species variables
    if options.marchYesNo
        B_cncf = sparse(N,N_var*N); % CoNcentration Condition for Fkg base state gradients in xi (all zeros)
        B_cncS = sparse(N,N_spec*N); % CoNcentration Condition for Species base state gradients in xi (all zeros)
        C_cncf = sparse(N,N_var*N); % CoNcentration Condition for Fkg fluctuation gradients in xi (all zeros)
        C_cncS = sparse(N,N_spec*N); % CoNcentration Condition for Species fluctuation gradients in xi (all zeros)
    end
    for s=1:N_spec
        A_cncS(: , (s-1)*N+1 : s*N) = I;
    end
    A_cncf = sparse(A_cncf);
    A_cncS = sparse(A_cncS);
end

if strcmp(options.numberOfTemp,'2T') % additional columns and rows for the additional energy equation and variable tauv
    % terms corresponding to the main variables fkg and the momX, momZ and
    % ener conservation eqs.
    if options.Cooke
        A_tauv_rows = [Av1 , Av2 , O , Av4]; % terms in the vibrational energy equation multiplying fkg
        A_tauv_cols = [ O  ;  O  ; O ; A4v]; % terms in the momX, momZ, ener eqs. multiplying tauv
        if options.marchYesNo
            B_tauv_rows = [Bv1 ,  O  , O , Bv4];
            C_tauv_rows = [ O  , Cv2 , O ,  O ];
            B_tauv_cols = [ O  ;  O  ; O ;  O ];
            C_tauv_cols = [ O  ;  O  ; O ;  O ];
        end
    else
        A_tauv_rows = [Av1 , Av2 , Av4]; % terms in the vibrational energy equation multiplying fkg
        A_tauv_cols = [ O  ;  O  ; A4v]; % terms in the momX, momZ, ener eqs. multiplying tauv
        if options.marchYesNo
            B_tauv_rows = [Bv1 ,  O  , Bv4];
            C_tauv_rows = [ O  , Cv2 ,  O ];
            B_tauv_cols = [ O  ;  O  ;  O ];
            C_tauv_cols = [ O  ;  O  ;  O ];
        end
    end

    % terms corresponding to the species mass fractions and conservation
    % equations
    if MultiSpec
        A_tauv_Srows = zeros(N,N_spec*N); % terms in vibrational energy equation multiplying species mass fractions
        A_tauv_Scols = zeros(N_spec*N,N); % terms in species eqs. multiplying tauv
        % note: there are N_spec-1 species conservation equations, so you might
        % think that A_tauv_Srows should have (N_spec-1)*N rows. However, here we
        % also include the rows cor the concentration condition (which will be
        % a submatrix of zeros)
        if options.marchYesNo
            B_tauv_Srows = zeros(N,N_spec*N);
            C_tauv_Srows = zeros(N,N_spec*N);
            B_tauv_Scols = zeros(N_spec*N,N);
            C_tauv_Scols = zeros(N_spec*N,N);
        end
        for s=1:N_spec
            A_tauv_Srows(: , (s-1)*N+1:s*N) = Avs{s}; % terms in vibrational energy equation multiplying species mass fractions
            if options.marchYesNo
                B_tauv_Srows(: , (s-1)*N+1:s*N) = Bvs{s};
                C_tauv_Srows(: , (s-1)*N+1:s*N) = Cvs{s};
            end
            A_tauv_Scols((s-1)*N+1:s*N , :) = O;        % terms in species eqs. multiplying tauv
            if options.marchYesNo
                B_tauv_Scols((s-1)*N+1:s*N , :) = Bsv{s};
                C_tauv_Scols((s-1)*N+1:s*N , :) = O;
            end
        end
    else
        A_tauv_Srows = [];
        A_tauv_Scols = [];
        if options.marchYesNo
            B_tauv_Srows = [];
            C_tauv_Srows = [];
            B_tauv_Scols = [];
            C_tauv_Scols = [];
        end
    end

    % assembling rows and columns
    A_tauv_allRows = [A_tauv_rows , A_tauv_Srows];
    A_tauv_allCols = [A_tauv_cols ; A_tauv_Scols];
    if options.marchYesNo
        B_tauv_allRows = [B_tauv_rows , B_tauv_Srows];
        C_tauv_allRows = [C_tauv_rows , C_tauv_Srows];
        B_tauv_allCols = [B_tauv_cols ; B_tauv_Scols];
        C_tauv_allCols = [C_tauv_cols ; C_tauv_Scols];
    end
end

% assembling completely
if MultiSpec||MultiElem
    idxBath_rows = (N_var+idx_bath.'-1)*N + (1:N);
    A = [ A_fkg   A_cols ;
        A_rows  A_spec ];
    A(idxBath_rows,:) = [ A_cncf  A_cncS ];
    if options.marchYesNo
        B = [ B_fkg   B_cols ;
            B_rows  B_spec ];

        Cc = [ C_fkg   C_cols ; % The name is different because C is already being used for the non-dimensional viscosity times density
            C_rows  C_spec ];
        
        B(idxBath_rows,:)  = [ B_cncf  B_cncS ];
        Cc(idxBath_rows,:) = [ C_cncf  C_cncS ];
    end
else
    A = A_fkg;
    if options.marchYesNo
        B = B_fkg;
        Cc = C_fkg;
    end
end
% additional rows and columns due to the additional energy equation
if strcmp(options.numberOfTemp,'2T')
    A = [     A       A_tauv_allCols ;
         A_tauv_allRows      Avv     ];
     if options.marchYesNo
        B =  [     B       B_tauv_allCols ;
              B_tauv_allRows      Bvv     ];
        Cc = [     Cc      C_tauv_allCols ;
              C_tauv_allRows      Cvv     ];
     end
end
% assembling
if options.marchYesNo
    mat = A+B+FDc(1)*Cc;
else
    mat = A;
end

%% Building forcing term
options.globalEval = false;
options.MultiSpec = MultiSpec;

[a_forc,b_forc,c_forc] = eval_forcing(intel,intelp,options);

% evaluating and adding higher-order terms
if options.highOrderTerms
    [a_HO,b_HO,c_HO] = eval_highOrderTerms(intel,options);
    a_forc = a_forc + a_HO;
    b_forc = b_forc + b_HO;
    c_forc = c_forc + c_HO;
end

if options.marchYesNo
    forcing = -a_forc - b_forc - c_forc; % we've defined the vectors like they appear on the system equation, and now we make them negative
    eq_vec  = -a_forc - b_forc - c_forc;
else
    forcing = -a_forc;
    eq_vec  = -a_forc;
end

%% Imposing charge-neutrality condition (if necessary)
if MultiSpec && nnz(intel.mixCnst.bElectron) && options.bChargeNeutrality
    % note: only applicable to multi-species flows, with electrons and with
    % the charge-neutrality option toggled on
    [mat,forcing] = impose_chargeNeutralityCondOnMat(mat,forcing,ambipolar.zetas,ys,...
                                intel.mixCnst.bElectron,intel.mixCnst.bPos,idx_bath.',N,N_var);
elseif MultiElem && nnz(intel.mixCnst.bElectron) && options.bChargeNeutrality
    [mat,forcing] = impose_chargeNeutralityCondOnMat_elem(mat,forcing,intel.mixCnst.elem_list,N,N_var);
end

%% Evaluating residuals and plotting
if ~isfield(intel,'ixi')
    intel.ixi = 1;
elseif intel.it==1
    intel.ixi = intel.ixi + 1;
end

rhs_momentx  = eq_vec(1:N); % The x-momentum equation is the 1st N positions of the eq. vector
if options.Cooke
    rhs_momentz  = eq_vec(2*N+1:3*N); % The z-momentum equation is between the 2nd & 3rd N positions of the eq. vector
    rhs_energy   = eq_vec(3*N+1:4*N); % The energy equation is between the 3rd & 4th N positions of the eq. vector
else
    rhs_energy   = eq_vec(2*N+1:3*N); % The energy equation is between the 2nd & 3rd N positions of the eq. vector
end
if MultiSpec||MultiElem
    for s=1:N_spec
        rhs_specCons((s-1)*N+1:s*N) = eq_vec((N_var+s-1)*N+1:(N_var+s)*N); % The species equations are between the (N_var+s-1)th and the (N_var+s)th positions of the eq. vector
    end
end
if strcmp(options.numberOfTemp,'2T')
    rhs_vibEnergy = eq_vec((N_var+N_spec)*N+1:(N_var+N_spec+1)*N); % the vibrational energy equation will go after the species eqs
end

norms.res.momXeq = norm(rhs_momentx(2:end-1),inf);
if options.Cooke
    norms.res.momZeq = norm(rhs_momentz(2:end-1),inf);
end
norms.res.enereq = norm(rhs_energy(2:end-1),inf);
if MultiSpec||MultiElem
    for s=1:N_spec
        norms.res.speceq{s} = norm(rhs_specCons((s-1)*N+2:s*N-1),inf);
    end
end
if strcmp(options.numberOfTemp,'2T')
    norms.res.vibenereq = norm(rhs_vibEnergy(2:end-1),inf);
end
intel.forcing = forcing;
intel.mat     = mat;
intel.norms   = norms;

end % build_mat.m
