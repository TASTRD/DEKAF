function intel = build_importedSolution(intel,options)
% BUILD_IMPORTEDSOLUTION(intel,options) extracts the necessary data from
% options or from the given file path, in order to use it as an initial
% guess for the flowfield.
%
% See also: setDefaults, DEKAF_QSS
%
% Author(s):    Koen Groot
%               Ethan Beyak
% GNU Lesser General Public License 3.0

if strcmp(options.importSolution,'file')
    disp('--- Loading initial guess from file...');
    importedFile = load(options.FileName,'intel');
    PrevSol = importedFile.intel;
elseif strcmp(options.importSolution,'struc')
    disp('--- Loading initial guess from structure...');
    PrevSol = options.importedIntel;
else
    error(['the chosen importSolution option ''', options.importSolution,''' is not valid.']);
end
eta = intel.eta; % non-dimensional variable on which we have to interpolate the imported solution
D1 = intel.D1; % extracting derivation matrix
intel.f         = interp1(PrevSol.eta,PrevSol.f,eta,'spline');
intel.df_deta   = interp1(PrevSol.eta,PrevSol.df_deta,eta,'spline');
intel.df_deta2  = interp1(PrevSol.eta,PrevSol.df_deta2,eta,'spline');
intel.df_deta3  = D1*intel.df_deta2;
intel.g         = interp1(PrevSol.eta,PrevSol.g,eta,'spline');
intel.dg_deta   = interp1(PrevSol.eta,PrevSol.dg_deta,eta,'spline');
intel.dg_deta2  = D1*intel.dg_deta;
if isfield(PrevSol,'k')
    intel.k         = interp1(PrevSol.eta,PrevSol.k,eta,'spline');
    intel.dk_deta   = interp1(PrevSol.eta,PrevSol.dk_deta,eta,'spline');
    if isfield(PrevSol,'kpp')
        intel.dk_deta2 = interp1(PrevSol.eta,PrevSol.dk_deta2,eta,'spline');
    else
        intel.dk_deta2  = D1*intel.dk_deta;
    end
elseif ~options.Cooke
    intel.k         = zeros(size(intel.f));
    intel.dk_deta   = zeros(size(intel.f));
    intel.dk_deta2  = zeros(size(intel.f));
    intel.kh        = zeros(size(intel.f));
else
    temp            = build_IC(intel,options);
    intel.k         = temp.k;
    intel.dk_deta   = temp.dk_deta;
    intel.dk_deta2  = temp.dk_deta2;
end
if isfield(PrevSol,'ys')
    for s=1:intel.mixCnst.N_spec
        intel.ys{s} = interp1(PrevSol.eta,PrevSol.ys{s},eta,'spline');
        intel.dys_deta{s} = interp1(PrevSol.eta,PrevSol.dys_deta{s},eta,'spline');
        intel.dys_deta2{s} = interp1(PrevSol.eta,PrevSol.dys_deta2{s},eta,'spline');
    end
end
