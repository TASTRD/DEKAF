function [mat,forcing] = impose_chargeNeutralityCondOnMat(mat,forcing,zetas,ys,bElectron,bPos,idxBath,N,N_var)
% impose_chargeNeutralityCondOnMat imposes the charge-neutrality condition
% on the system matrices and forcing vectors.
%
% It does so by substituting the species conservation equation of a bath
% species for the charge-neutrality condition - the net sum of the charges
% must be equal to zero.
%
% Examples:
%   (1) [mat,forcing] = impose_chargeNeutralityCondOnMat(mat,forcing,zetas,ys,...
%                                                bElectron,bPos,idxBath,N,N_var)
%
% Inputs and outputs:
%   mat         computational matrix built with build_mat
%   forcing     forcing vector built with eval_forcing
%   zetas       inverse of the molar mass fraction (zetas = Mm/Mm_s) sizes:
%               {N_spec x 1} (N_eta x 1)
%   ys          mass fraction. sizes: {N_spec x 1} (N_eta x 1)
%   bElectron   electron boolean (N_spec x 1)
%   bPos        positive particle boolean (N_spec x 1)
%   idxBath     species index of the bath species (having the concentration
%               condition rather than a dedicated conservation equation)
%   N           number of points in the computational (eta) domain
%   N_var       number of computational variables preceding the mass
%               fractions in the variable vector (df_deta, f, etc.)
%
% See also: setDefaults, build_mat
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

idxBath_chargeNeut = find(bElectron)*ones(1,N);                 % by default we take electrons as the bath for charge neutrality
idxBath_chargeNeut(idxBath_chargeNeut==idxBath) = find(bPos,1,'first'); % if they are themselves the bath for the concentration condition we take the first ion
idx_eq = (N_var+idxBath_chargeNeut-1)*N+(1:N);                  % position of the equation corresponding to the bath species

mat(idx_eq,:) = 0;                                              % clearing matrix positions
forcing(idx_eq) = 0;                                            % clearing forcing positions

idx_charged = find(bElectron|bPos);                             % positions of charged species
for s=idx_charged(:).'                                          % looping charged species
    idx_s = (N_var+s-1)*N+1:(N_var+s)*N;                            % position of the species in question in the matrix
    mat(idx_eq,idx_s) = (bPos(s)-bElectron(s))*diag(zetas{s});      % zetas converts from mass to mole fraction
    forcing(idx_eq) = forcing(idx_eq) - (bPos(s)-bElectron(s))*zetas{s}.*ys{s};
    % note: the equations in mat and forcing must have opposite signs,
    % since the forcing term comes from moving to the RHS the base-flow
    % components of the different variables in the equations
end

end % impose_chargeNeutralityCondOnMat