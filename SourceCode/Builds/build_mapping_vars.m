function [eta,D1,D2,eta_star] = build_mapping_vars(eta_max,eta_i,N,bChebydist,varargin)
%BUILD_MAPPING_VARS Initialize eta and differentiation matrices
%
% Usage:
%   (1)
%       [eta,D1,D2,eta_star] = build_mapping_vars(eta_max,eta_i,N,bChebydist)
%   
%   (2)
%       [...] = build_mapping_vars(...,eta_min)
%       |--> allows to specify also the minimum height (by default 0)
%
% Inputs:
%   eta_max, eta_min
%       maximum and minimum value of the wall-normal variable (eta)
%   eta_i
%       value of eta determining the point distribution (half of the points
%       will be below, and the other half above this position).
%   N
%       number of points
%   bChebydist
%       boolean determining whether to use Chebyshev and Malik
%       distributions (true) or finite difference and Malik distribution
%       for EPIC (false)
%
% Outputs:
%   eta         size N x 1
%       vector of wall-normal heights
%   D1, D2      size N x N
%       first- and second-order differentiation matrices wrt eta
%   eta_star    size N x 1
%       Chebyshev-Gauss-Lobatto points
%
% Author(s): Fernando Miro Miro
%            Koen Groot
% GNU Lesser General Public License 3.0


if bChebydist
    % Chebyshev and Malik distribution for other applications
    [eta_star,DM]   = chebDif(N,4);
else
    % Finite difference and Malik distribution for EPIC
    eta_star = linspace(1,-1,N);
    deta_star = eta_star(1)-eta_star(2);
    [DM1,DM2] = FDdif(N,deta_star);
    DM(:,:,1) = DM1;
    DM(:,:,2) = DM2;
    DM(:,:,3) = 0*DM1;
    DM(:,:,4) = 0*DM1;
end

[eta,D1,D2]  = MappingMalik_uptoD4(eta_max,eta_i,eta_star',...
    DM(:,:,1),DM(:,:,2),DM(:,:,3),DM(:,:,4),varargin{:});

% Set differentiation matrices to sparse to hasten computation time
D1 = sparse(D1);
D2 = sparse(D2);

end % build_mapping_vars.m
