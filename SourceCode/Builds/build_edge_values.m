function [intel,options] = build_edge_values(intel,options)
% BUILD_EDGE_VALUES(intel,options) builds the necessary edge quantities
% as a function of csi for DEKAF's boundary-layer solver. It must receive
% as inputs, either the edge velocity or pressure profiles U_e(x) or p_e(x)
% and the temperature profile T=T(x). For whichever of the two first is not
% given (U_e(x) or p_e(x)), their value at the initial x_e0 must be
% inputted. The reference initial value may be substituted for the unit
% Reynolds number. In other words, the valid combinations of inputs are:
%        1) U_e(x), T_e(x) & p_e0
%        2) M_e(x), T_e(x) & p_e0
%        3) p_e(x), T_e(x) & U_e0
%        4) p_e(x), T_e(x) & M_e0
%        5) U_e(x), T_e(x) & Re1_e0
%        6) M_e(x), T_e(x) & Re1_e0
%        7) p_e(x), T_e(x) & Re1_e0
% The profiles must be inputted on an x_e grid, passed through intel.x_e.
% The profiles must also be LINE VECTORS, NOT COLUMNS.
%
% intel must also include the constant spanwise velocity W_0
%
% This function may also be used to compute the edge quantities for the
% self-similar non-marching solver. In that case the option marchYesNo will
% have to be set to false. In that case, T_e must always be fixed, and then
% either M_e or U_e, and either p_e, rho_e or Re1_e.
%
% The outputted profiles will be a function of the computational variable
% xi defined such that dxi = U_e*rho_e*mu_e * dx
% They will be interpolated onto an equispaced xi grid, fixed by the user
% through options.mapOpts.xi_start, options.mapOpts.xi_end and
% options.mapOpts.N_x . If nothing is inputed, then an equispaced grid with
% size options.mapOpts.N_x  (by default 100) will be created between the
% maximum and minimum xi resulting from the inputs.
%
% intel should contain:
% --> M_e, U_e, rho_e, p_e and Re1_e as described above, T_e, ys_e", x_e@1,
% cp*1, gam*1", Pr*2", W0", mu_ref*b", Su_mu*b", T_ref*b", kappa_ref*2b",
% Su_kappa*2b", Sc_e*a", beta@2", Theta@2",
%
% and will be outputted with the additional fields:
% --> M_e, U_e, T_e, rho_e, mu_e, kappa_e, h_e, H_e, Ecu, Ecw, R_e, fi,
% g_lim, beta, theta, mixCnst, xi@1, cp_e*1, Sc_e*a,
%
% *1 - only CPG
% *2 - only for options.cstPr true
% *a - only cstSc in options.modelDiffusion
% *b - only Sutherland in options.modelDiffusion
% @1 - only for marching
% @2 - only for non-marching
% "  - defaults will be imposed otherwise
%  intel's defaults:
%       - ys_e - depends on options.mixture (fixed by defaultComposition)
%       - gam - 1.4
%       - mu_ref, Su_mu, T_ref, kappa_ref, Su_kappa - depends on
%       options.mixture (fixed by getDefaultsSutherland).
%       - Pr - depends on options.mixture (fixed by getDefaultsPr)
%       - Sc_e - 0.7
%       - Le_e - 1.4
%       - beta, theta - 0
%       - W0 - 0
%
% options is a structure that allows to fix several options. For a full
% list of the available options see setDefaults, which also sets its
% defaults. This function uses the following options:
% --> flow, mixture, modelTransport, marchYesNo, specify_cp, H_F, G_bc,
% N_x
%
% The options structure is also outputted, with the updated value of
% options.G_bc, on the non-dimensional computational quantity g, rather
% than on the dimensional temperature T. (only for options.thermal_BC =
% 'Twall').
%
% Parent and child functions see also: DEKAF_BL, setDefaults,
% listOfVariablesExplained, getAll_constants, getDefault_composition,
% getDefault_Sutherland, getDefault_Pr
%
% functions used for the properties: getMixture_h_cp,
% getTransport_mu_kappa, getMixture_rho, getMixture_p, and maaany more...
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

% Developers note: All BL vectors in this function should be received as
% rows. They are then transposed, and operations are performed on columns
% all along. At the end they are transposed back when interpolating onto
% the xi vector, to have the outputs in rows.

if isfield(intel, 'airfoil')
    warning(['You supplied a field called airfoil to intel, but didn''t indicate options.inviscidFlowfieldTreatment = ''airfoil''.',newline, ...
        'This seems incomplete. Either assign options.inviscidFlowfieldTreatment = ''airfoil'' or exclude the airfoil field from intel.'],errmsg_ID);
end

% retrieving mixture constants
mixCnst = getAll_constants(options.mixture,options);
intel.mixCnst = mixCnst;
N_spec = mixCnst.N_spec;
   
% Checking if the user fixed the elemental fractions instead of the species (LTE or LTEED only)
if strcmp(options.inviscidFlowfieldTreatment,'1DNonEquilibriumEuler');  ysName = 'ys_e0';
else;                                                                   ysName = 'ys_e';
end
intel = getDefault_composition(intel,options.mixture,false,ysName);     % if the user didn't specify the mixture composition, we fix the default
if ismember(options.noseFlow,{'LTE','LTEED'})
    if options.passElementalFractions                                   % we must obtain the species fraction from the elemental
        trc = 1e-10;                                                        % trace species value
        ys_e = repmat({trc*ones(size(intel.yE_e{1}))},[1,mixCnst.N_spec]);  % allocating with (almost) zeros
        idx_EinS = cellfun(@(cll)find(ismember(mixCnst.spec_list,cll)),mixCnst.elem_list); % location of elemental species
        ys_e(idx_EinS) = intel.yE_e;                                        % populating mass fractions of elemental species
        intel.(ysName) = ys_e;                                              % populating inputs
    else                                                                % we just get the provided species fraction
        ys_e = intel.(ysName);
    end
    [mixCnst.LTEtablePath,mixCnst.XEq] = checkCompositionLTE(ys_e,mixCnst.Mm_s,mixCnst.R0,options.mixture,options);
    mixCnst.LTEtableData = load(mixCnst.LTEtablePath);
else                                                                    % we just get the provided species fraction
    ys_e = intel.(ysName);
end

% set defaults
% obtaining edge spanwise velocity
intel = standardvalue(intel,'Lambda_sweep',0); % sweep angle in degrees
intel = standardvalue(intel,'cone_angle',NaN); % cone angle - placeholder
% Saving power law exponent into mixCnst
if strcmp(options.modelTransport,'powerLaw')
    intel = standardvalue(intel,'mPowerLaw',1); % setting default to the exponent of the power law
    mixCnst.mPowerLaw = intel.mPowerLaw;
end
intel.mixCnst = mixCnst;
intel = setDiffusionDefaultIntel(intel,options);

% Obtaining cylindrical-coordinate functions
intel.cylCoordFuncs = populateProbsteinElliotFunctions(intel,options);

% Checking if the user asked for the inviscid solver, and if so, running it
switch options.inviscidFlowfieldTreatment
    case '1DNonEquilibriumEuler'
        if ~options.marchYesNo
            error('The inviscid 1D non-equilibrium solver is not supported for QSS.');
        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%% INVISCID SOLVER %%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        [intel,options] = DEKAF_inviscid(intel,options);

        % plotting if necessary
        if options.plotInviscid
            plotInviscid(intel,options);
        end

        % Adjust for sweep from the 1D inviscid solver
        [intel.U_e,intel.W_0] = getVelocity_U_W(intel.U_e,intel.U_e(1),intel.Lambda_sweep);

        % Extract inputs from intel to be locals (all are columns except x_e)
        DxPhyse     = intel.Dxe;
        TTrans_e    = intel.TTrans_e;
        TRot_e      = intel.TRot_e;
        TVib_e      = intel.TVib_e;
        TElec_e     = intel.TElec_e;
        Tel_e       = intel.Tel_e;
        U_e         = intel.U_e;
        y_se        = intel.y_se;
        xPhys_e     = intel.x_e;
        p_e         = intel.p_e;
        rho_e       = intel.rho_e;

        % Elemental fractions
        X_Ee = getElement_XE_from_ys(y_se,mixCnst.Mm_s,mixCnst.elemStoich_mat,options.bChargeNeutrality,mixCnst.elem_list); % elemental mole fraction at the edge
        y_Ee = getEquilibrium_ys(X_Ee , mixCnst.Mm_E);                          % elemental mass fraction at the edge

        % Performing eventual coordinate-system transformations
        [x_e,~,~,~,~,~,~,dx_dxPhys] = get_xyTransformed(xPhys_e,[],options.coordsys,intel.cylCoordFuncs);
        Dxe = diag(1./dx_dxPhys)*DxPhyse;

        % building *_mat quantities for the evaluation of the enthalpy functions
        [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
            bElectron_mat,hForm_s_mat,TTrans_mat,TRot_mat,TVib_mat,...
            TElec_mat,Tel_mat] = reshape_inputs4enthalpy(mixCnst.R_s,...
            mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,...
            mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,...
            TTrans_e,TRot_e,TVib_e,TElec_e,Tel_e);

        % get mixture enthalpy and cp
        [h_e,cp_e,~,~,~,~,~,hv_e,~,cpv_e,~] = getMixture_h_cp(y_se,TTrans_mat,...
            TRot_mat,TVib_mat,TElec_mat,Tel_mat,R_s_mat,nAtoms_s_mat,...
            thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,...
            hForm_s_mat,options,mixCnst.AThermFuncs_s);

        % get mixture frozen ratio of specific heats
        gam_e = getMixture_gam(TTrans_e,TRot_e,TVib_e,TElec_e,Tel_e,y_se,...
            mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,...
            mixCnst.gDegen_s,mixCnst.bElectron,options,mixCnst.AThermFuncs_s);

        % get mixture molar mass and gas constant
        [Mm_e,R_e,~,~] = getMixture_Mm_R(y_se,TTrans_e,Tel_e,mixCnst.Mm_s,...
            mixCnst.R0,mixCnst.bElectron);

        % get mach number
        M_e = U_e./sqrt(gam_e.*R_e.*TTrans_e);

        % get mixture dynamic viscosity and thermal conductivity
        [mu_e,kappa_e,kappav_e] = getTransport_mu_kappa(y_se,TTrans_e,Tel_e,rho_e,p_e,...
            Mm_e,options,mixCnst,TRot_e,TVib_e,TElec_e,cp_e);

        % finally, calculate the unit Reynolds number on the edge
        Re1_e = rho_e.*U_e./mu_e;
        intel.Re1_e0 = Re1_e(1); % save just for the display message

    case {'constant','custom'}

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%% USER-SPECIFIED EDGE OR PRESHOCK CONDITIONS %%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        neededvalue(intel,'T_e','T_e not defined.');     % edge temperature profile
        if length(intel.T_e)>1
            neededvalue(intel,'x_e','x_e not defined.'); % x coordinate for the edge profiles
            xPhys_e = intel.x_e(:);
            N_x = length(xPhys_e);

            % Performing eventual coordinate-system transformations
            [x_e,~] = get_xyTransformed(xPhys_e,[],options.coordsys,intel.cylCoordFuncs);

            % obtain differentiation matrix for arbitrary input grid x_e
            DxPhyse = FDdif_nonEquis(xPhys_e,['backward',num2str(min(options.ox,length(x_e)))]);
            Dxe     = FDdif_nonEquis(x_e,    ['backward',num2str(min(options.ox,length(x_e)))]);
        else
            % set dummy values for marching variables
            N_x     = 1; % if we don't march, there is only one edge location
            DxPhyse = 0; % no need for a differentiation matrix
            Dxe     = 0;
            x_e     = 0; % placeholders. In QSS situations needing them (like for turbulence modeling) they will be recomputed at the end
            xPhys_e = 0;
        end

        % Checking the necessary inputs
        T_e = intel.T_e(:); % we make them column vectors for now, and then transpose them at the end
        switch options.numberOfTemp
            case '1T'
                TTrans_e = T_e;     TRot_e = T_e;	TVib_e = T_e;	TElec_e = T_e;	Tel_e = T_e;
            case '2T'
                neededvalue(intel,'Tv_e','Tv_e not defined.');     % edge temperature profile
                Tv_e = intel.Tv_e(:); % we make them column vectors for now, and then transpose them at the end
                TTrans_e = T_e;     TRot_e  = T_e;
                TVib_e   = Tv_e;	TElec_e = Tv_e;     Tel_e = Tv_e;
            otherwise
                error(['options.numberOfTemp of ''',options.numberOfTemp,''' is currently not supported!']);
        end

        % reshaping to have the size of the T_e vector
        if length(ys_e{1})==1
            for s=1:length(ys_e)
                ys_e{s} = ys_e{s} * ones(size(T_e));
            end
        end
        y_se = cell1D2matrix2D(ys_e); % making cell into matrix form (columns)
        if length(ys_e)~=N_spec % check on the ys input
            error(['Error: the inputted ys_e has a length of ',num2str(length(ys_e)),' which does not match the number of species for this mixture ''',options.mixture,''' (',num2str(N_spec),')']);
        end
        X_Ee = getElement_XE_from_ys(y_se,mixCnst.Mm_s,mixCnst.elemStoich_mat,options.bChargeNeutrality,mixCnst.elem_list); % elemental mole fraction at the edge
        y_Ee = getEquilibrium_ys(X_Ee , mixCnst.Mm_E);                          % elemental mass fraction at the edge

        intel.mixCnst = mixCnst;

        % building *_mat quantities for the evaluation of the enthalpy functions
        [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,...
            hForm_s_mat,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat] = ...
            reshape_inputs4enthalpy(mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,...
            mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,TTrans_e,TRot_e,TVib_e,TElec_e,Tel_e);

        % Preparing initial guess for the iteration process to compute the accurate
        % edge properties (depending weakly on p)
        p_dum = 101325;         % initial guess for the pressure (Pa)
        convPRho = 1;           % initializing convergence criterion on p & rho
        tolPRho = eps;          % tolerance for the iteration on p & rho
        itPRhoMax = 50;         % maximum iteration count for p & rho
        itPRho = 1;             % initializing
        while convPRho>tolPRho && itPRho<=itPRhoMax
            if ismember(options.flow,{'LTE','LTEED'}) % for LTE, we must recompute our y_se depending on the updated pressure
                optsEq = options; optsEq.display = false; % silencing equilibrium computation
                X_se = getEquilibrium_X_complete(p_dum,T_e,'mtimesx',options.mixture,mixCnst,options.modelEquilibrium,0,optsEq,'variable_XE',X_Ee);
                y_se = getEquilibrium_ys(X_se,mixCnst.Mm_s);
            end

            %%% Obtaining properties depending only on the temperature
            % obtaining cp and enthalpies
            [h_e,cp_e,~,~,~,~,~,hv_e,~,cpv_e] = getMixture_h_cp(y_se,...
                TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat,R_s_mat,nAtoms_s_mat,...
                thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);
            gam_e = getMixture_gam(TTrans_e,TRot_e,TVib_e,TElec_e,Tel_e,y_se,...
                mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,...
                mixCnst.gDegen_s,mixCnst.bElectron,options,mixCnst.AThermFuncs_s);

            % obtaining mixture molar mass
            [Mm_e,R_e,~,Rstar_e] = getMixture_Mm_R(y_se,TTrans_e,Tel_e,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron);
            rho_dum = getMixture_rho(y_se,p_dum,TTrans_e,Tel_e,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options);

            % storing cp and R for the CPG case
            if strcmp(options.flow,'CPG')
                if options.specify_cp % the user wants to pass a specific cp
                    neededvalue(intel,'cp','the specify_cp was chosen, yet no cp field is found in intel.');
                    cp_e = repmat(intel.cp,[N_x,1]);
                else
                    cp_e = repmat(cp_e(1) ,[N_x,1]);
                end
                intel.cp_e  = cp_e;
                h_e         = cp_e.*T_e;
                if options.specify_gam          % if the user specifies the value of gamma
                    gam_e = repmat(intel.gam,[N_x,1]);  % we take it from intel
                    R_e = cp_e.*(1-1./gam_e);           % and compute the gas constant accordingly
                else                            % otherwise we take the gas constant computed from the composition
                    gam_e = cp_e./(cp_e-R_e);       % and compute gamma accordingly
                end
                Rstar_e = R_e;                  % one-temperature
                Mm_e = R_e/mixCnst.R0;
            end

            % obtaining mixture dynamic viscosity and thermal conductivity
            [mu_e,kappa_e,kappav_e] = getTransport_mu_kappa(y_se,TTrans_e,Tel_e,rho_dum,p_dum,...
                Mm_e,options,mixCnst,TRot_e,TVib_e,TElec_e,cp_e);

            % If marching, use Bernoulli's to construct edge pressure, Mach, and
            % unit Reynolds. If not marching, the relations for p_e, M_e, Re1_e
            % follow naturally from what user-specified fields are provided in
            % intel.
            [p_e,M_e,Re1_e,U_e,rho_e,intel] = build_edge_p_M_Re1_U_rho(TTrans_e,...
                mu_e,gam_e,R_e,Rstar_e,DxPhyse,intel,options);

            % convergence criterion for the looping on the p & rho
            convPRho = norm((p_e - p_dum)./p_dum,Inf);  % convergence criterion (variation in p and rho normalized with the previous value)
            p_dum = p_e;                                % assigning values for next iteration count
            itPRho = itPRho+1;                          % increasing counter
        end

        if ismember(options.flow,{'LTE'})
            disp(['--- The p-rho iteration converged to ',num2str(convPRho),' after ',num2str(itPRho),' iterations']);
        end

        if options.shockJump
            % storing pre-shock variables
            intel.M_inf     = M_e;
            intel.U_inf     = U_e;
            intel.rho_inf   = rho_e;
            intel.p_inf     = p_e;
            intel.T_inf     = T_e;
            intel.h_inf     = h_e;
            intel.ys_inf    = matrix2D2cell1D(y_se);
            switch options.coordsys
                case 'cartesian2D';     wedgeCone_angle = intel.wedge_angle;
                case 'cone';            wedgeCone_angle = intel.cone_angle;
                otherwise;              error(['the selected coordsys ''',options.coordsys,''' is not supported for shock-jump relations']);
            end
            switch options.noseFlow
                case {'CPG','TPG','TPGD','CNE'};    XY_se = y_se;
                case {'LTE','LTEED'};               XY_se = X_Ee;
                otherwise;                          error(['The chosen flow assumption ''',options.flow,''' is not supported']);
            end
            %%%%%%%%% JUMPING SHOCK
            [U_e,TTrans_e,TRot_e,TVib_e,TElec_e,Tel_e,p_e,y_se,intel.shock_angle] = eval_shockJump(U_e,TTrans_e,TRot_e,TVib_e,TElec_e,Tel_e,p_e,XY_se,gam_e,R_e,wedgeCone_angle,mixCnst,options);

            %%%%%%%%% RECOMPUTE PROPERTIES WITH UPDATED POST SHOCK VALUES %%%%%%%%%
            if ~strcmp(options.flow,'CPG') % properties don't change through the shock for CPG
                % Reshape for enthalpy calculation
                [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
                    bElectron_mat,hForm_s_mat,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat] = ...
                    reshape_inputs4enthalpy(mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,...
                    mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,...
                    TTrans_e,TRot_e,TVib_e,TElec_e,Tel_e);

                % get mixture enthalpy and cp
                [h_e,cp_e,~,~,~,~,~,hv_e,~,cpv_e,~] = getMixture_h_cp(y_se,...
                    TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat,R_s_mat,nAtoms_s_mat,...
                    thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);

                % get mixture mass density
                rho_e = getMixture_rho(y_se,p_e,TTrans_e,Tel_e,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options);

                % get mixture frozen ratio of specific heats
                gam_e = getMixture_gam(TTrans_e,TRot_e,TVib_e,TElec_e,Tel_e,y_se,...
                    mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,...
                    mixCnst.gDegen_s,mixCnst.bElectron,options,mixCnst.AThermFuncs_s);

                % get mixture molar mass and gas constant
                [Mm_e,R_e,~,~] = getMixture_Mm_R(y_se,TTrans_e,Tel_e,mixCnst.Mm_s,...
                    mixCnst.R0,mixCnst.bElectron);
            else
                rho_e = getMixture_rho([],p_e,TTrans_e,[],[],[],[],'specify_R',R_e,mixCnst.EoS_params,options);
                h_e = cp_e*TTrans_e;
            end

            % get mixture dynamic viscosity and thermal conductivity
            [mu_e,kappa_e,kappav_e] = getTransport_mu_kappa(y_se,TTrans_e,Tel_e,rho_e,p_e,...
                                        Mm_e,options,mixCnst,TRot_e,TVib_e,TElec_e,cp_e);

            % finally, calculate the unit Reynolds number on the edge
            Re1_e = rho_e.*U_e./mu_e;

            % Recomputing also Mach number (not outputted for LTE)
            M_e = U_e./sqrt(gam_e.*R_e.*TTrans_e);

        end
    otherwise
        error(['The chosen inviscidFlowfieldTreatment ''',options.inviscidFlowfieldTreatment,''' is not supported']);
end

% obtaining semi-total enthalpy
H_e = getEnthalpy_semitotal(h_e, U_e);

% obtaining Eckert numbers
[Ecu, Ecw, Ecp] = getEckert_numbers(U_e, intel.W_0, h_e , p_e, rho_e);

% obtaining REFERENCE edge prandtl number
switch options.numberOfTemp
    case '1T'
        Pr_e = mu_e.*cp_e./kappa_e;
    case '2T'                                           % For two temperatures we use the geometric average of the two thermal conductivities
        Pr_e = mu_e.*cp_e./sqrt(kappa_e.*kappav_e);     % This is just a REFERENCE Prandtl number. No need to get serious and separate into
    otherwise                                           % two components. We do in eval_thermo.
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

% Additional variables of interest are the relative angle of the
% streamlines with respect to the x direction
fi = getStreamlineFi(U_e, intel.W_0, options.marchYesNo);

%%% Other arrangements, and interpolation
[ys_w,options] = rearrange_wallYsBC(intel,options,y_se);                    % Defining mass-fraction BC options
[yE_w,options] = rearrange_wallYEBC(intel,options,y_Ee);                    % Defining elemental-mass-fraction BC options
% Setting minimum enthalpy - to be used throughout the iteration
[T_w,Tv_w,g_lim,options] = rearrange_thermalBC(intel,options,ys_w,y_se,TTrans_e,TVib_e,p_e,H_e,h_e,cp_e,Pr_e,x_e);

% Interpolating fields (only for the marching case) and defining outputs
if options.marchYesNo
    % applying illingworth transformation on provided edge vectors
    [xi_e,dxi_dx_e] = eval_illingworth(x_e,U_e,rho_e,mu_e,Dxe);

    % Obtaining xi
    [xi,D1_xi,D2_xi,x,xPhys] = get_xi(options.xSpacing,options.mapOpts,options.ox,xPhys_e,U_e,rho_e,mu_e,options.coordsys,intel.cylCoordFuncs);
    
    % Obtaining turbulent reference quantities
    L = max(xPhys);
    wT_e = U_e./L;                              % reference turbulent vorticity (Menter Eq. A11)
    kT_e = 1e-3*wT_e.*mu_e./rho_e;              % reference turbulent kinetic energy (Menter Eq. A11)

    % Obtaining hartree parameters
    [theta,Theta,beta,ThetaW,ThetaK] = getHartree_parameters(dxi_dx_e,xi_e,U_e,H_e,TVib_e,Dxe,wT_e,kT_e);

    % Checking if there will have to be an extrapolation of the inputed edge
    % profiles
    if ~(max(xi_e)-max(xi)>options.tol || min(xi_e)-min(xi)<options.tol)
        error(['The edge profile that was inputted is not sufficiently large for the selected x range - there will have',...
            ' to be an extrapolation of the profiles. Recommended limits for x for this profile are ',num2str(min(x_e)),' to ',num2str(max(x_e)),' [m]']);
    end

    intel.xi_e      = xi_e;
    intel.xi        = xi;
    intel.x         = x;        % obtaining x for each xi (transformed if necessary)
    intel.xPhys     = xPhys;    % obtaining physical x for each x (not transformed)

    % Interpolating onto computational grid
    vars2interp.M_e   = M_e;        vars2interp.U_e     = U_e;      vars2interp.T_e      = TTrans_e;
    vars2interp.rho_e = rho_e;      vars2interp.Re1_e   = Re1_e;    vars2interp.gam_e    = gam_e;
    vars2interp.p_e   = p_e;        vars2interp.mu_e    = mu_e;     vars2interp.kappa_e  = kappa_e;
    vars2interp.h_e   = h_e;        vars2interp.H_e     = H_e;      vars2interp.fi       = fi;
    vars2interp.beta  = beta;       vars2interp.theta   = theta;    vars2interp.Theta    = Theta;
    vars2interp.Ecu   = Ecu;        vars2interp.Ecw     = Ecw;      vars2interp.Ecp      = Ecp;
    vars2interp.R_e   = R_e;        vars2interp.ys_e    = matrix2D2cell1D(y_se); vars2interp.yE_e = matrix2D2cell1D(y_Ee);
    switch options.numberOfTemp
        case '1T'   % nothing else needed
        case '2T';  vars2interp.Tv_e = TVib_e;          vars2interp.hv_e = hv_e;          vars2interp.cpv_e = cpv_e;
        otherwise;  error(['the chosen number of temperatures ''',numberOfTemp,''' is not supported']);
    end
    % turbulent quantities
    switch options.modelTurbulence
        case {'none','SmithCebeci1967'}         % no additional arrangements needed
        case 'SSTkomega'
            vars2interp.ThetaW = ThetaW;    vars2interp.ThetaK = ThetaK;
            vars2interp.wT_e    = wT_e;     vars2interp.kT_e    = kT_e;
        otherwise
            error(['the chosen modelTurbulence ''',options.modelTurbulence,''' is not supported']);
    end
    switch options.xSpacing % depending on how the computational grid was computed, we interpolate onto one or another
        case 'custom_xi';                           intel = interpolateAllVariables(xi_e,    xi,   vars2interp,options.meth_edge_interp,intel); % we interpolate on xi
        case {'linear','log','tanh','custom_x'};    intel = interpolateAllVariables(xPhys_e, xPhys,vars2interp,options.meth_edge_interp,intel); % we interpolate on x
        otherwise;                                  error(['Error: the chosen xSpacing ''',options.xSpacing,''' is not supported']);
    end
    intel.g_lim     = g_lim;
    intel.D1_xi     = D1_xi;
    intel.D2_xi     = D2_xi;
    if nnz(mixCnst.bElectron) && options.bChargeNeutrality                         % enforcing charge-neutrality condition
        intel.ys_e = enforce_chargeNeutralityCond(intel.ys_e,mixCnst.bElectron,mixCnst.bPos,... % we don't pass a bath index, so it is always the electrons
            mixCnst.Mm_s,mixCnst.R0,'massFrac','ys_lim',options.ys_lim);
        intel.ys_e = enforce_concCond(intel.ys_e,mixCnst.idx_bath);            % and concentration condition
    else                                                                    % if not, we enforce only the concentration
        intel.ys_e = enforce_concCond(intel.ys_e,mixCnst.idx_bath,'ys_lim',options.ys_lim); % this time with the chosen bath index (different from electrons)
    end
    if strcmp(options.modelDiffusion,'cstSc')
        intel.Sc_e       = repmat(intel.Sc_e,[1,length(xi)]);
    elseif strcmp(options.modelDiffusion,'cstLe')
        intel.Le_e       = repmat(intel.Le_e,[1,length(xi)]);
    end
    if strcmp(options.flow,'CPG')
        intel.cp_e       = repmat(cp_e(1),[1,length(xi)]);
        intel.mixCnst.cp_CPG = cp_e(1); % heat capacity used if CPG is assumed
    else
        intel.cp_e = cp_e;
    end
    % Storing temperature boundary conditions
    if strcmp(options.thermal_BC,'Twall')
        options.trackerTwall = interp1OrRepmat(options.x_bc,T_w,xPhys,options.meth_edge_interp);
    end
    switch options.numberOfTemp
        case '1T'
            options.trackerTvwall = NaN * ones(size(xPhys));
        case '2T'
            if ismember(options.thermalVib_BC,{'Twall','frozen','ablation','thermalEquil'})
                options.trackerTvwall = interp1OrRepmat(options.x_bc,Tv_w,xPhys,options.meth_edge_interp);
            end
        otherwise; error(['the chosen number of tempertures ''',options.numberOfTemp,''' is not supported']);
    end
    % Storing composition boundary conditions
    if strcmp(options.wallYs_BC,'ysWall')
        for s=mixCnst.N_spec:-1:1
            options.trackerYsWall{s} = interp1OrRepmat(options.x_bc,ys_w{s},xPhys,options.meth_edge_interp);
        end
        options.trackerYsWall = matrix2D2cell1D(limit_XYs(cell1D2matrix2D(options.trackerYsWall),options.ys_lim,mixCnst.idx_bath));
    end
    if strcmp(options.wallYE_BC,'yEWall')
        for E=mixCnst.N_elem:-1:1
            options.trackerYEWall{E} = interp1OrRepmat(options.x_bc,yE_w{E},xPhys,options.meth_edge_interp);
        end
        options.trackerYEWall = matrix2D2cell1D(limit_XYs(cell1D2matrix2D(options.trackerYEWall),options.ys_lim));
    end

    % Changing f boundary condition (if chosen a constant-V or constant-massflow BC)
    if ismember(options.wallBlowing_BC,{'Vwall','VwProfile','mwall','mwProfile'})
        switch options.coordsys
            case 'cartesian2D';                                         r0 = 1;                                     % factor is irrelevant in rearrange_wallBlowingBC
            case {'cone','cylindricalExternal','cylindricalInternal'};  r0 = intel.cylCoordFuncs.rc(intel.xPhys);   % computing surface radius
            otherwise;                                                  error(['the chosen coordsys ''',options.coordsys,''' is not supported']);
        end
        options = rearrange_wallBlowingBC(options,intel.rho_e(1),intel.mu_e(1),intel.U_e(1),intel.p_e(1),...
                                        intel.R_e(1),intel.T_e(1),intel.gam_e(1),intel.M_e(1),intel.xi,intel.xPhys,'cone',r0);
        % IMPORTANT! This imposition of the wall BC only concerns the initial guess. That's why there are so many (1)'s around
    end
else % if there is no marching, then we simply place the edge values into intel
    intel.M_e       = M_e;
    intel.U_e       = U_e;
    intel.T_e       = TTrans_e;
    intel.ys_e      = matrix2D2cell1D(y_se); % mass fractions
    intel.yE_e      = matrix2D2cell1D(y_Ee); % elemental fractions
    switch options.numberOfTemp
        case '2T'
            intel.Tv_e  = TVib_e;
            intel.hv_e  = hv_e;
            intel.cpv_e = cpv_e;
    end
    intel.rho_e     = rho_e;
    intel.p_e       = p_e;
    intel.mu_e      = mu_e;
    intel.kappa_e   = kappa_e;
    intel.h_e       = h_e;
    intel.H_e       = H_e;
    intel.cp_e      = cp_e;
    intel.Ecu       = Ecu;
    intel.Ecw       = Ecw;
    intel.Ecp       = Ecp;
    intel.R_e       = R_e;
    intel.gam_e     = gam_e;
    intel.Re1_e     = Re1_e;
    intel.fi        = fi;
    intel.g_lim     = g_lim;
    intel           = standardvalue(intel,'beta' ,0,'A default value of the beta Hartree parameter of 0 was taken.');  % beta and Theta defaults
    intel           = standardvalue(intel,'Theta',0,'A default value of the Theta Hartree parameter of 0 was taken.');
    intel.theta     = (1+intel.Ecu./2).*intel.Theta-intel.Ecu.*intel.beta;        % Hartree static enthalpy parameter
    
    % Check if you've neared the separation value of Falkner-Skan-Cooke
    % according to Frank White, ed. 2, p. 242
    sep_beta_FSC = -0.198838; %% HARDCODE ALERT
    if intel.beta < sep_beta_FSC
        warning(['Note that the provided value of intel.beta of ',num2str(intel.beta)...
            ,' is less than the limit of separation, beta = ',num2str(sep_beta_FSC)...
            ,' and there is no guarantee the solution will converge!']);
        asking_user_about_beta = true;
        while asking_user_about_beta
            sep_input_QSS = input('Type 1 to stop DEKAF, or 2 to continue');
            switch sep_input_QSS
                case 1
                    error('User-specified abort from the provided beta value!');
                case 2
                    % continue onward -- triumphantly! -- toward the danger zone
                    asking_user_about_beta = false;
                otherwise
                    fprintf('Invalid input! Try again\n');
            end
        end
    end

    if strcmp(options.wallYs_BC,'ysWall')
        for s=mixCnst.N_spec:-1:1
            options.Ys_bc{s} = ys_w{s}(1);
        end
    end

    if strcmp(options.wallYE_BC,'yEWall')
        for E=mixCnst.N_elem:-1:1
            options.YE_bc{E} = yE_w{E}(1);
        end
    end
    
    intel.mixCnst.cp_CPG = cp_e(1); % heat capacity used if CPG is assumed

    % Changing f boundary condition (if chosen a constant-V or constant
    % massflow BC)
    if ismember(options.wallBlowing_BC,{'Vwall','VwProfile','mwall','mwProfile'}) || ...    % in the event of a dimensional wall-blowing BC
            ismember(options.modelTurbulence,{'SmithCebeci1967','SSTkomega'})               % or of a turbulence modeling
        [intel,intel.xi,~,~,~,intel.x,intel.xPhys] = getXi_QSS(intel,options,intel.beta,intel.Theta,U_e,intel.W_0,rho_e,p_e,TTrans_e,TVib_e,H_e,h_e,mu_e,ys_e);
        L = intel.xPhys;
        intel.wT_e = U_e./L;                              % reference turbulent vorticity (Menter Eq. A11)
        intel.kT_e = 1e-3*intel.wT_e.*mu_e./rho_e;        % reference turbulent kinetic energy (Menter Eq. A11) % beta and Theta defaults
        intel = standardvalue(intel,'ThetaK',0,'A default value of the turbulent ThetaK Hartree parameter of 0 was taken.');
        intel = standardvalue(intel,'ThetaW',0,'A default value of the turbulent ThetaW Hartree parameter of 0 was taken.');
    end
    if ismember(options.wallBlowing_BC,{'Vwall','VwProfile','mwall','mwProfile'})   % in the event of a dimensional wall-blowing BC
        switch options.coordsys
            case 'cartesian2D';                                         r0 = 1;                                     % factor is irrelevant in rearrange_wallBlowingBC
            case {'cone','cylindricalExternal','cylindricalInternal'};  r0 = intel.cylCoordFuncs.rc(xPhys_e);   % computing surface radius
            otherwise;                                                  error(['the chosen coordsys ''',options.coordsys,''' is not supported']);
        end
        options = rearrange_wallBlowingBC(options,intel.rho_e,intel.mu_e,intel.U_e,intel.p_e,...
                                        intel.R_e,intel.T_e,intel.gam_e,intel.M_e,intel.xi,intel.xPhys,'cone',r0);
    end

end
intel.N_spec            = N_spec;


end % build_edge_values
