function intel = build_IC(intel,options)
%BUILD_IC Initializes analytical profiles of flow solution
%
% INPUT
%   eta : eta-coordinate ([y_max; ...; 0])
%   options: structure containing misc. variables
%       .dn:        value representing 99% boundary-layer thickness
%       .model_f:   analytical model used to determine the f velocity
%                   profile in the first guess. Possibilities:
%                   'quad'      -   quadratic polynomial
%                   'tanh'      -   hyperbolic tangent (default)
%                   'poly'      -   2nd order polynomials
%           Default:    'polyexp' for supersonic and hypersonic flows, and
%                       'tanh' for subsonic
%       .model_g:   analytical model used to determine the g total enthalpy
%                   profile in the first guess. Possibilities:
%                   'const'     -   constant
%                   'polyexp'   -   cubic polynomial times exponential of
%                                   a quadratic polynomial  (default)
%                   'tanh'      -   hyperbolic tangent
%                   'poly'      -   2nd order polynomials
%           Default:    'polyexp' for supersonic and hypersonic flows, and
%                       'const' for subsonic
%       .model_tauv: analytical model used to determine the tauv
%                    vibrational temperature profile in the first guess.
%                    Possibilities:
%                   'ODE'       -   solves the vib-elec-el frozen ODE for
%                                   using the g, df_deta, etc obtained with
%                                   the other model_*
%                   'polyexp'   -   using the g profile obtained with
%                                   polyexp expressions, getting the static
%                                   and then approximating the vibrational
%                                   enthalpy from it.
%                   'exp'       -   using an exponentially decreassing
%                                   function to match the boundaries
%                   'straight'  -   uses a straight line going from the
%                                   wall value to the edge, adapting it
%                                   with a tanh in order to have continuous
%                                   derivatives
%           Default:    'ODE'
%       .fw: wall blowing boundary condition
%       .flow: string identifying the flow assumption
%
% OUTPUT
% f, f', f'', f''', k, k', k'', g, g', g'', ys, ys', ys''
% some of these outputs may not appear for certain flow assumptions
%
% Author(s): Koen Groot
%            Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% Define default options

eta     = intel.eta;
dn      = options.dn;
fw      = options.F_bc;
Pr_e    = intel.mixCnst.Pr;
L       = options.L;
G_bc    = options.G_bc;
Ecu     = intel.Ecu;
Ecw     = intel.Ecw;
M_e     = intel.M_e;

% setting defaults
if M_e<1
    default_modelg = 'poly';
    default_modelf = 'tanh';
else
    default_modelg = 'polyexp';
    default_modelf = 'polyexp';
end
options = standardvalue(options,'model_g',default_modelg);
options = standardvalue(options,'model_f',default_modelf);
options = standardvalue(options,'model_tauv','ODE');
model_f  = options.model_f;
model_g  = options.model_g;
model_tauv = options.model_tauv;

switch model_f
    case 'polyexp'
        % see model_g -> polyexp for explanations
        dataStruc       = load('polyexp_db_goatVersion_totalEnthalpy.mat');
        M_mat           = dataStruc.M_mat;      % size (N_M x N_gw), Mach number, repeated in 2nd dimension
        gw_mat          = dataStruc.gw_mat;     % size (N_M x N_gw), gw is the value of g at the wall
        %fw_mat          = dataStruc.fw_mat;     % size (N_M x N_gw), fw is the value of f at the wall (not used for the moment, but kept here to remember that it is stored)
        df_deta_w_mat   = dataStruc.felix_mat;  % size (N_M x N_gw), felix is the value of df_deta at the wall
        df_deta2_w_mat  = dataStruc.fox_mat;    % size (N_M x N_gw), fox is the value of df_deta2 at the wall
        df_deta3_w_mat  = dataStruc.frog_mat;   % size (N_M x N_gw), frog is the value of df_deta3 at the wall
        B_mat           = dataStruc.B_mat;      % size (N_M x N_gw x 4), poly exp coefficients; 3rd dim: coefficients

        % Extract vec from mat
        M_vec   = M_mat(:,1);

        % Calculate the step in M_vec.
        % Take third and second in order to avoid the first point of M = 0.01;
        dM_vec = M_vec(3) - M_vec(2);

        % Get the index of the Mach of the lower neighbor when compared with
        % the database. For example, if M_e = 2.8, and M_vec is [... 2.5 3.0...],
        % the index of 2.5 will be selected.
        % This is done so that the initial guess does not overshoot the exact solution.
        idx_M       = floor(M_e/dM_vec) + 1;
     %%%% WARNING: this is highly empirical!
        % We observed that the fitting polynomial coefficients are pretty
        % weird for M_e>10 (highly fluctuating). So that leads to very bad
        % initial guesses. However, we saw that fixing the initial guess to
        % that of mach 10, it converged without problems. So if Mach is
        % larger than 10 (idx_M=21) we just take the mach 10
        % initialization.
        if idx_M>21
            idx_M       = 21;
        end
        gw_M_vec    = gw_mat(idx_M,:);

        % Grab the wall conditions on g and dg_deta
        switch options.thermal_BC
            case {'H_F','ablation'} % condition on the wall heat flux
                gw = dataStruc.gw_mat(idx_M,1);
            case {'Twall','gwall'} % condition on the wall temperature
                gw  = G_bc;
            otherwise
                error(['non-identified thermal boundary condition ''',options.thermal_BC,'''']);
        end

        % Interpolate over g wall at given Mach for A coefficients,
        % as well as the derivatives of f at the wall.
        [~,idx_gwMax] = max(gw_M_vec);
        if gw>gw_M_vec(idx_gwMax)                               % if gw is larger than any of the gw_M_vec (heated wall)
            B = permute(B_mat(idx_M,idx_gwMax,:),[3,1,2]);          % we take that of the maximum (adiabatic)
            df_deta_w = df_deta_w_mat(idx_M,idx_gwMax);
            df_deta2_w = df_deta2_w_mat(idx_M,idx_gwMax);
            df_deta3_w = df_deta3_w_mat(idx_M,idx_gwMax);
        else                                                    % otherwise
            B(1)        = interp1(gw_M_vec,B_mat(idx_M,:,1)         ,gw,'pchip');
            B(2)        = interp1(gw_M_vec,B_mat(idx_M,:,2)         ,gw,'pchip');
            B(3)        = interp1(gw_M_vec,B_mat(idx_M,:,3)         ,gw,'pchip');
            df_deta_w   = interp1(gw_M_vec,df_deta_w_mat(idx_M,:)   ,gw,'pchip');
            df_deta2_w  = interp1(gw_M_vec,df_deta2_w_mat(idx_M,:)  ,gw,'pchip');
            df_deta3_w  = interp1(gw_M_vec,df_deta3_w_mat(idx_M,:)  ,gw,'pchip');
        end

        % Get initial guess for g and its derivatives
        df_deta     = poly3exp2_g_func(B,eta,df_deta_w,df_deta2_w,df_deta3_w);
        df_deta2     = poly3exp2_dg_deta_func(B,eta,df_deta_w,df_deta2_w,df_deta3_w);
        df_deta3     = poly3exp2_dg_deta2_func(B,eta,df_deta_w,df_deta2_w,df_deta3_w);

        % integrating df_deta profile to get f
        D1 = intel.D1;
        I1 = integral_mat(D1,'du');
        f = I1*df_deta + fw;
    case 'quad'
        f       = eta - dn*(eta/dn-1).^3/3 - dn/3 + fw;
        df_deta = 1 - (eta/dn-1).^2;
        df_deta2= -2*(eta/dn-1)/dn;
        df_deta3= -2/dn^2*eta.^0;

        f(eta>dn)           = eta(eta>dn) - 1/3;
        df_deta(eta>dn)     = 1;
        df_deta2(eta>dn)    = 0;
        df_deta3(eta>dn)    = 0;
    case 'tanh'
        a       = atanh(0.99);
        f       = dn/a*log(cosh(a*eta/dn)) + fw;
        df_deta = tanh(a*eta/dn);
        df_deta2= a/dn*sech(a*eta/dn).^2;
        df_deta3= -2*(a/dn)^2*sech(a*eta/dn).^2.*tanh(a*eta/dn);
    case 'poly' % we build polynomials of the kind: b*eta^2 + c*eta + d
        b = 1/(2*L);
        c = 0;
        d = 0;
        f = b*eta.^2 + c*eta + d + fw;
        df_deta = 2*b*eta+c;
        df_deta2 = 2*b*ones(size(eta));
        df_deta3 = ones(size(eta));
    otherwise
        error(['the chosen model for f ''',options.model_f,''' is not supported']);
end
switch model_g
    case 'polyexp' % Choose cubic function multiplied by an exponential of a quadratic

        % Suppose g = (ax^3 + bx^2 + cx + d)exp(-e^2*x^2 + fx) + 1
        %
        % Let g at the wall be gw.
        % Let dg_deta at the wall be gary.
        % Let dg_deta2 at the wall be goat.
        % The authors have chosen to fix coefficients c, d, f
        %
        % VESTA profiles were generated over a range of Mach numbers and gw
        % values. Ultimately, these solutions were curve-fitted to this
        % ansatz. See generate_ICProfiles_g.m
        %
        % The coefficients a, b, e are stored in A as the first, second, &
        % third elements, respectively.

        % This model was only tested for isothermal or adiabatic walls.
        % Errors from a nonzero wall Neumann condition may arise. Tread lightly.
        dataStruc       = load('polyexp_db_goatVersion_totalEnthalpy.mat');
        M_mat           = dataStruc.M_mat;      % size (N_M x N_gw), Mach number, repeated in 2nd dimension
        gw_mat          = dataStruc.gw_mat;     % size (N_M x N_gw), gw is the value of g at the wall
        dg_deta_w_mat   = dataStruc.gary_mat;   % size (N_M x N_gw), gary is the value of dg_deta at the wall
        dg_deta2_w_mat  = dataStruc.goat_mat;   % size (N_M x N_gw), goat is the value of dg_deta2 at the wall
        A_mat           = dataStruc.A_mat;      % size (N_M x N_gw x 4), poly exp coefficients; 3rd dim: coefficients

        % Extract vec from mat
        M_vec   = M_mat(:,1);

        % Calculate the step in M_vec.
        % Take third and second in order to avoid the first point of M = 0.01;
        dM_vec = M_vec(3) - M_vec(2);

        % Get the index of the Mach of the lower neighbor when compared with
        % the database. For example, if M_e = 2.8, and M_vec is [... 2.5 3.0...],
        % the index of 2.5 will be selected.
        % This is done so that the initial guess does not overshoot the exact solution.
        idx_M       = floor(M_e/dM_vec) + 1;
     %%%% WARNING: this is highly empirical!
        % We observed that the fitting polynomial coefficients are pretty
        % weird for M_e>10 (highly fluctuating). So that leads to very bad
        % initial guesses. However, we saw that fixing the initial guess to
        % that of mach 10, it converged without problems. So if Mach is
        % larger than 10 (idx_M=21) we just take the mach 10
        % initialization.
        if idx_M>21
            idx_M       = 21;
        elseif idx_M<=3
            warning(['For the chosen Mach number (',num2str(M_e),') we recommend to use the constant initial guess (options.model_g=''const'')']);
            pause(1);
        end
        gw_M_vec    = gw_mat(idx_M,:);

        % Grab the wall conditions on g and dg_deta
        switch options.thermal_BC
            case {'H_F','ablation'} % condition on the wall heat flux
                gw = dataStruc.gw_mat(idx_M,1);
            case {'Twall','gwall'} % condition on the wall temperature
                gw  = G_bc;
            otherwise
                error(['non-identified thermal boundary condition ''',options.thermal_BC,'''']);
        end

        % Interpolate over g wall at given Mach for A coefficients,
        % as well as the derivatives of f at the wall.
        [~,idx_gwMax] = max(gw_M_vec);
        if gw>gw_M_vec(idx_gwMax)                               % if gw is larger than any of the gw_M_vec (heated wall)
            A = permute(A_mat(idx_M,idx_gwMax,:),[3,1,2]);          % we take that of the maximum (adiabatic)
            dg_deta_w = dg_deta_w_mat(idx_M,idx_gwMax);
            dg_deta2_w = dg_deta2_w_mat(idx_M,idx_gwMax);
            gw = gw_M_vec(idx_gwMax);
        else                                                    % otherwise
            A(1)        = interp1(gw_M_vec,A_mat(idx_M,:,1)         ,gw,'pchip'); % we interpolate
            A(2)        = interp1(gw_M_vec,A_mat(idx_M,:,2)         ,gw,'pchip');
            A(3)        = interp1(gw_M_vec,A_mat(idx_M,:,3)         ,gw,'pchip');
            dg_deta_w   = interp1(gw_M_vec,dg_deta_w_mat(idx_M,:)   ,gw,'pchip');
            dg_deta2_w  = interp1(gw_M_vec,dg_deta2_w_mat(idx_M,:)  ,gw,'pchip');
        end

        % Get initial guess for g and its derivatives
        g           = poly3exp2_g_func(A,eta,gw,dg_deta_w,dg_deta2_w);
        dg_deta     = poly3exp2_dg_deta_func(A,eta,gw,dg_deta_w,dg_deta2_w);
        dg_deta2    = poly3exp2_dg_deta2_func(A,eta,gw,dg_deta_w,dg_deta2_w);

    case 'exp'
        warning('The exp initial guess for the temperature was designed for static enthalpy, it may not work for the current nomenclature (total enthalpy)');
        pause(1);
        if sum(strcmp(options.thermal_BC,{'Twall','gwall'}))
            D1 = intel.D1;
            D2 = intel.D2;
            A       = options.G_bc-1;
            a       = 4;
            g       = 1 + A./(eta+1).^a;
            dg_deta = D1*g;
            dg_deta2= D2*g;
        else
            error('adiabatic condition not ready');
        end
    case 'const'
        g        = 1*eta.^0;
        dg_deta  = 0*eta.^0;
        dg_deta2 = 0*eta.^0;

        g(eta>dn)       = 1;
        dg_deta(eta>dn) = 0;
    case 'tanh' % Choose a hyperbolic tangent function for infinitely smooth first guess
        warning('The tanh initial guess for the temperature was designed for static enthalpy, it may not work for the current nomenclature (total enthalpy)');
        pause(1);
        if sum(strcmp(options.thermal_BC,{'Twall','gwall'})) % isothermal boundary condition
            gw  = G_bc;
            a   = 2*Pr_e/dn*atanh(0.99);
            n   = 1+.1;
            b   = (dn/(2*Pr_e))^n;
            g       = 1+(gw-1)/2*( 1 + tanh( a*(b-eta.^n)./eta.^(n-1) ) );
            dg_deta = (gw-1)/2 * sech(a*(b-eta.^n)./eta.^(n-1)).^2 * a .* ( (b-eta.^n)*(1-n)./eta.^n-n);
            dg_deta2= -(gw-1)/2 * a * eta.^(-2*n-1).*sech( a*(b-eta.^n)./eta.^(n-1) ).^2 ...
                .* ( 2*a*eta.*( b*(n-1)+eta.^n ).^2 .* tanh( a*(b-eta.^n)./eta.^(n-1) ) - b*(n-1)*n*eta.^n );
            % Analytical derivatives behave better than numerical derivatives
            %             dg_deta = intel.D1*g;
            %             dg_deta2 = intel.D2*g;
            %dg_deta(end)  = dg_deta(end-1);
            %dg_deta2(end) = dg_deta2(end-1); % this is an erroneous value, but allowed, because this value ...
            % does not appear in the boundary condition
        else % prescribed heat flux boundary condition
            if G_bc==0 % adiabatic condition
                A       = (Ecu + Ecw)*sqrt(Pr_e)/2;         % See definition of Recovery factor
                a       = dn/Pr_e/15;
                g       = 1 + A*exp(-eta.^2/(2*a));
                dg_deta = A*exp(-eta.^2/(2*a)).*-eta/a;
                dg_deta2= A*exp(-eta.^2/(2*a)).*((eta/a).^2 - 1/a);
            else % non-adiabatic heat flux
                dgw     = G_bc;
                a       = Pr_e/dn*atanh(0.99);
                g       = 1+dgw*(tanh(a*eta)-1)/a;
                dg_deta =  dgw*sech(a*eta).^2;
                dg_deta2= -dgw*sech(a*eta).^2.*tanh(a*eta);
            end
        end
    case 'poly' % we build polynomials of the kind: b*eta^2 + c*eta + d
        if sum(strcmp(options.thermal_BC,{'Twall','gwall'}))
            b = 0;
            c = (1-G_bc)/L;
            d = G_bc;
        else % constant heat flux condition
            g_adiab = 1+Ecu/2;
            b =(1-g_adiab-G_bc*L)/L^2;
            c = G_bc;
            d = g_adiab+G_bc*L;
        end
        g           = b*eta.^2 + c*eta + d; % the polynomial expression is the same, the only thing changing is the value
        dg_deta     = 2*b*eta+c;      % of the coefficients such that it matches the required boundary conditions
        dg_deta2    = 2*b*ones(size(eta));
    otherwise
        error(['the chosen model for g ''',options.model_g,''' is not supported']);
end

intel.g         = g;
intel.dg_deta   = dg_deta;
intel.dg_deta2  = dg_deta2;
intel.gh         = zeros(size(f));
intel.dgh_deta   = zeros(size(f));
intel.dgh_deta2  = zeros(size(f));

intel.f         = f;
intel.df_deta   = df_deta;
intel.df_deta2  = df_deta2;
intel.df_deta3  = df_deta3;
intel.fh         = zeros(size(f));
intel.dfh_deta   = zeros(size(f));
intel.dfh_deta2  = zeros(size(f));
intel.dfh_deta3  = zeros(size(f));

if options.Cooke
    intel.k         = df_deta; % the non-dimensional profile is the same for w and u
    intel.dk_deta   = df_deta2;
    intel.dk_deta2  = df_deta3;
    % correcting the semi-total enthalpy (h+0.5*u^2 instead of
    % h+0.5*u^2+0.5*w^2)
%     A = 1-Ecw/2 / (1+Ecu/2+Ecw/2);
%     B = Ecw / (1+Ecu/2+Ecw/2);
%     intel.g = 1/A * intel.g - B/A*0.5*intel.k.^2;
%     intel.dg_deta = 1/A * intel.dg_deta - B/A*intel.k.*intel.dk_deta;
%     intel.dg_deta2 = 1/A * intel.dg_deta2 - B/A*intel.dk_deta.^2 - B/A*intel.k.*intel.dk_deta2;
else
    intel.k         = zeros(size(f));
    intel.dk_deta   = zeros(size(f));
    intel.dk_deta2  = zeros(size(f));
end
intel.kh         = zeros(size(f));
intel.dkh_deta   = zeros(size(f));
intel.dkh_deta2  = zeros(size(f));

if ismember(options.flow,{'CNE','TPGD'})
    switch options.wallYs_BC
        case {'nonCatalytic','ablation','catalyticGamma','catalyticK'} % classic non-catalytic condition. We can start by making the ys profile constant and equal to ys_e
            for s=1:intel.mixCnst.N_spec
                intel.ys{s} = ones(size(f)) * intel.ys_e{s};
                intel.dys_deta{s} = zeros(size(f));
                intel.dys_deta2{s} = zeros(size(f));
            end
        case 'ysWall' % constant-wall-mass-fraction condition
            a = 2; % HARCODE ALERT!
            % gives a good decay such that at eta=6 (typical end of the BL) the
            % distortion of the fields is <1e-5 times the introduced delta
            for s=1:intel.mixCnst.N_spec
                dys = options.Ys_bc{s} - intel.ys_e{s};
                intel.ys{s}         = intel.ys_e{s} + dys * exp(-a*eta);
                intel.dys_deta{s}   =              -a*dys * exp(-a*eta);
                intel.dys_deta2{s}  =             a^2*dys * exp(-a*eta);
            end
        otherwise
        error(['the chosen wallYs_BC ''',options.wallYs_BC,''' is not supported']);
    end

    intel.ysh = repmat({zeros(size(f))},[1,intel.mixCnst.N_spec]);
    intel.dysh_deta = repmat({zeros(size(f))},[1,intel.mixCnst.N_spec]);
    intel.dysh_deta2 = repmat({zeros(size(f))},[1,intel.mixCnst.N_spec]);
elseif ismember(options.flow,{'LTEED'})
    switch options.wallYE_BC
        case 'zeroYEgradient' % zero-Y_E-gradient condition. We can start by making the yE profile constant and equal to yE_e
            for E=1:intel.mixCnst.N_elem
                intel.yE{E}         = ones(size(f)) * intel.yE_e{E};
                intel.dyE_deta{E}   = zeros(size(f));
                intel.dyE_deta2{E}  = zeros(size(f));
            end
        case 'yEWall' % constant-wall-mass-fraction condition
            a = 2; % HARCODE ALERT!
            % gives a good decay such that at eta=6 (typical end of the BL) the
            % distortion of the fields is <1e-5 times the introduced delta
            for E=1:intel.mixCnst.N_elem
                dyE = options.YE_bc{E} - intel.yE_e{E};
                intel.yE{E}         = intel.yE_e{E} + dyE * exp(-a*eta);
                intel.dyE_deta{E}   =              -a*dyE * exp(-a*eta);
                intel.dyE_deta2{E}  =             a^2*dyE * exp(-a*eta);
            end
        otherwise
        error(['the chosen wallYE_BC ''',options.wallYE_BC,''' is not supported']);
    end
    intel.yEh = repmat({zeros(size(f))},[1,intel.mixCnst.N_elem]);
    intel.dyEh_deta = repmat({zeros(size(f))},[1,intel.mixCnst.N_elem]);
    intel.dyEh_deta2 = repmat({zeros(size(f))},[1,intel.mixCnst.N_elem]);
end

switch options.numberOfTemp
    case '1T'
        % nothing additional
    case '2T'
        tauv_e = 1;
        if strcmp(options.thermalVib_BC,'thermalEquil')% if the user chose thermal equilibrium for the wall BC
            % we have to get the wall vib-elec-el temperature from the
            % trans-rot imposed on the g
            y_s = cell1D2matrix2D(intel.ys);
            mixCnst = intel.mixCnst;
            cptr_w = getMixture_cptr(y_s(end,:),mixCnst.R_s.',mixCnst.nAtoms_s.',mixCnst.bElectron.',options);
            h_w = intel.g(end)*intel.H_e;
            Tv_w = get_NRTfromh(h_w,y_s(end,:),h_w/cptr_w,mixCnst.R_s.',mixCnst.nAtoms_s.',...
                            permute(mixCnst.thetaVib_s,[3,1,2]),permute(mixCnst.thetaElec_s,[3,1,2]),permute(mixCnst.gDegen_s,[3,1,2]),mixCnst.bElectron.',mixCnst.hForm_s.',options,mixCnst.AThermFuncs_s);
            tauv_w = Tv_w/intel.Tv_e;
        else % otherwise, we take what ever was passed
            tauv_w  = options.trackerTvwall(1)/intel.Tv_e;                       % initial value of tauv
        end
        switch options.thermalVib_BC
            case {'H_F','frozen'}
                intel.tauv = tauv_e * ones(size(g));
                intel.dtauv_deta = zeros(size(g));
                intel.dtauv_deta2 = zeros(size(g));
            case {'Twall','gwall','ablation','thermalEquil'}
                a = 2; % HARCODE ALERT!                                          % initial guess for the NR algorithm
                % obtaining y_s with which to estimate tauv
                if isfield(intel,'ys')
                    y_s = cell1D2matrix2D(intel.ys);
                else
                    y_s = repmat(cell1D2matrix2D(intel.ys_e),[length(eta),1]);
                end
                switch model_tauv
                    case 'ODE' % solving the vib-elec-el ODE knowing df_deta, g, etc
                        intel.tauv = eval_ODEtauv(tauv_e,tauv_w,intel,options,y_s);       % defining IC for tauv
                        intel.dtauv_deta = intel.D1*intel.tauv;
                        intel.dtauv_deta2 = intel.D2*intel.tauv;
                    case 'polyexp' % Choose cubic function multiplied by an exponential of a quadratic
                        % We will work with the expression obtained for g (total enthalpy),
                        % convert it to a static enthalpy, and then scale it to have the
                        % freestream and wall values wanted for tauv.
                        gv_e = intel.hv_e / intel.h_e;
                        gg = getEnthalpy_semitotal2static_nonDim(g,dg_deta,dg_deta2,df_deta,df_deta2,df_deta3,Ecu); % getting static enthalpy
                        gv_bcRat = options.gv_bc/gg(end); % ratio of values of gv and gg at the boundary
                        gv        =  gv_e* (1-exp(-a*eta)) + gg       *gv_bcRat.*exp(-a*eta);
                        [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...               % reshaping mixCnst
                            bElectron_mat,hForm_s_mat,~] = reshape_inputs4enthalpy(intel.mixCnst.R_s,...
                            intel.mixCnst.nAtoms_s,intel.mixCnst.thetaVib_s,intel.mixCnst.thetaElec_s,...
                            intel.mixCnst.gDegen_s,intel.mixCnst.bElectron,intel.mixCnst.hForm_s,gv);
                        Tv0 = getGuess_Tv(intel,options);
                        [~,Tv] = get_NRTTvfromhhv(g*intel.H_e,gv*intel.h_e,y_s,Tv0,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,... % obtaining Tv using NR from gv (hv)
                                                        thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options);
                        intel.tauv = Tv/intel.Tv_e;                                                          % defining IC for tauv
                        intel.dtauv_deta = intel.D1*intel.tauv;
                        intel.dtauv_deta2 = intel.D2*intel.tauv;
                    case 'exp'
                        deltatauv = options.tauv_bc - tauv_e;
                        intel.tauv       = tauv_e + deltatauv * exp(-a*eta);
                        intel.dtauv_deta  = - a * deltatauv * exp(-a*eta);
                        intel.dtauv_deta2 = a^2 * deltatauv * exp(-a*eta);
                    case 'straight'
                        a = 2;
                        etaCut = 3;
                        tauv_w      = options.trackerTwall(1)/intel.Tv_e;   % initial value of tauv
                        tauv_e      = 1;                                    % final value of tauv
                        gEta = 0.5*(1-tanh(a*(eta-etaCut)));                % unit function to join two discontinuous ones
                        intel.tauv = (1-gEta)*tauv_e + gEta.*(tauv_w + (tauv_e-tauv_w)/etaCut.*eta);
                        intel.dtauv_deta  = intel.D1*intel.tauv;
                        intel.dtauv_deta2 = intel.D2*intel.tauv;
                    otherwise
                        error(['the chosen model_tauv ''',model_tauv,''' is not supported']);
                end
            otherwise
                error(['the chosen thermalVib_BC ''',options.thermalVib_BC,''' is not supported']);
        end
        intel.tauvh         = zeros(size(f));
        intel.dtauvh_deta   = zeros(size(f));
        intel.dtauvh_deta2  = zeros(size(f));
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

% Turbulent quantities
switch options.modelTurbulence
    case {'none','SmithCebeci1967'} % nothing to add
    case 'SSTkomega'
        a = 2; % HARDCODE ALERT!!
        intel.KT        = (1 - exp(-a*eta));
        intel.dKT_deta  =  a  *exp(-a*eta);
        intel.dKT_deta2 = -a^2*exp(-a*eta);
        
        y_s     = repmat(cell1D2matrix2D(intel.ys_e) , size(eta));          % species mass fractions
        p       = intel.p_e * ones(size(eta));                              % pressure
        mixCnst = intel.mixCnst;                                            % mixture constants
        D1      = intel.D1;                                                 % differentiation matrix
        h       = intel.g*intel.H_e - 0.5*(intel.df_deta.*intel.U_e).^2;    % static enthalpy
        T       = getMixture_T(h,y_s,p,mixCnst,options);                    % temperature
        rho     = getMixture_rho(y_s,p,T,T,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options); % density
        mu      = getTransport_mu_kappa(y_s,T,T,rho,p,[],options,mixCnst);  % viscosity
        [~,y]   = getDimensional_xy(D1,rho,intel.U_e,intel.xi,intel.x,options.coordsys,intel.cylCoordFuncs); % wall-normal coordinate
        dy0     = y(end-1)-y(end);                                          % first step in y
        wT_w    = 800.*mu(end)./(rho(end).*dy0^2);                          % wall value of the vorticity
        dWT_w   = wT_w/intel.wT_e - 1;                                      % difference between freestream and wall vorticity
        a = 4; % HARDCODE ALERT!!
        intel.WT        =  1 + dWT_w.*exp(-a*eta);
        intel.dWT_deta  = -a  *dWT_w.*exp(-a*eta);
        intel.dWT_deta2 =  a^2*dWT_w.*exp(-a*eta);
        
        intel.KTh         = zeros(size(f));
        intel.dKTh_deta   = zeros(size(f));
        intel.dKTh_deta2  = zeros(size(f));
        intel.WTh         = zeros(size(f));
        intel.dWTh_deta   = zeros(size(f));
        intel.dWTh_deta2  = zeros(size(f));
    otherwise
        error(['The chosen turbulence model ''',options.modelTurbulence,''' is not supported']);
end

end % build_IC.m
