function intel_dimVars = build_intelDimFromParts(varargin)
% build_intelDimFromParts puts together a series of intels created from
% running the marching code on different consecutive xi stencils.
%
%   Examples:
%       (1)  intel_dimVars = build_intelDimFromParts(intel1,intel2,...)
%
% See also: DEKAF_BL, build_intel4restart, setDefaults
%
% Author(s):    Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

% analyzing inputs and determining sizes
N_xis = zeros(nargin,1);                                                    % initializing vector of xi lengths
for ii=1:nargin                                                             % looping intels
    N_xis(ii) = length(varargin{ii}.xi);                                        % determining sizes of the different xi stencils
end
N_xiTot = sum(N_xis);                                                       % total number of xis
N_xisCum = cumsum(N_xis);                                                   % cumulative sum of xis
doublePoints = N_xisCum(1:end-1) + 1;                                       % positions that will be repeated

fldnms = fieldnames(varargin{1});                                           % fieldnames in the intels
N_spec = varargin{1}.mixCnst.N_spec;                                        % number of species
N_eta = length(varargin{1}.eta);                                            % number of points in the wall-normal direction

% Appending everything
for ii=1:length(fldnms)                                                     % looping field names
    if iscell(varargin{1}.(fldnms{ii}))                                         % checking if the field is a cell
        cellSizes = size(varargin{1}.(fldnms{ii}));                                 % size of the cell
        intel_dimVars.(fldnms{ii}) = cell(cellSizes);                               % allocating
        Ntot = prod(cellSizes);                                                     % total number of cell positions
        for ic=1:Ntot                                                               % looping cell positions
            sizes = size(varargin{1}.(fldnms{ii}){ic});                                 % size of the field
            if sizes(1)==N_eta && sizes(2)==N_xis(1)                                    % it means it's a flow-field quantity to be appended
                intel_dimVars.(fldnms{ii}){ic} = zeros(N_eta,N_xiTot);                      % allocating
                for jj=1:nargin                                                             % looping intels
                    N_etajj = size(varargin{jj}.(fldnms{ii}){ic},1);                            % number of points in the wall-normal direction
                    if N_etajj ~= N_eta                                                         % if the number of wall-normal points is not consistent, we throw an error
                        error(['the number of points in the eta direction of intel ',num2str(jj),' - ',num2str(N_etajj),' is not consistent with the initial ',numstr(N_eta)]);
                    end
                    N_prev = sum(N_xis(1:jj-1));                                                % total number of xi locations in the previous intels
                    intel_dimVars.(fldnms{ii}){ic}(:,N_prev+1:N_prev+N_xis(jj)) = real(varargin{jj}.(fldnms{ii}){ic}); % apending field
                end                                                                         % closing intel loop
                intel_dimVars.(fldnms{ii}){ic}(:,doublePoints) = [];                        % removing double points
            elseif sizes(1)==1 && sizes(2)==N_xis(1)                                    % it means it's an edge quantity to be appended
                intel_dimVars.(fldnms{ii}){ic} = zeros(1,N_xiTot);                          % allocating
                for jj=1:nargin                                                             % looping intels
                    N_prev = sum(N_xis(1:jj-1));                                                % total number of xi locations in the previous intels
                    intel_dimVars.(fldnms{ii}){ic}(N_prev+1:N_prev+N_xis(jj)) = real(varargin{jj}.(fldnms{ii}){ic}); % apending field
                end                                                                         % closing intel loop
                intel_dimVars.(fldnms{ii}){ic}(:,doublePoints) = [];                        % removing double points
            end                                                                         % closing size if
        end
    elseif isstruct(varargin{1}.(fldnms{ii}))                                   % if the field is a structure, we pass it as-is
        intel_dimVars.(fldnms{ii}) = varargin{1}.(fldnms{ii});
    else                                                                        % not a cell nor a structure
        sizes = size(varargin{1}.(fldnms{ii}));                                     % size of the field
        if length(sizes)==3 && sizes(1)==N_eta && sizes(2)==N_eta && sizes(3)==N_xis(1)% it means it's a derivation matrix to be appended
            intel_dimVars.(fldnms{ii}) = zeros(N_eta,N_eta,N_xiTot);                    % allocating
            for jj=1:nargin                                                             % looping intels
                N_etajj = size(varargin{jj}.(fldnms{ii}),1);                                % number of points in the wall-normal direction
                if N_etajj ~= N_eta                                                         % if the number of wall-normal points is not consistent, we throw an error
                    error(['the number of points in the eta direction of intel ',num2str(jj),' - ',num2str(N_etajj),' is not consistent with the initial ',numstr(N_eta)]);
                end
                N_prev = sum(N_xis(1:jj-1));                                                % total number of xi locations in the previous intels
                intel_dimVars.(fldnms{ii})(:,:,N_prev+1:N_prev+N_xis(jj)) = real(varargin{jj}.(fldnms{ii})); % apending field
            end                                                                         % closing intel loop
            intel_dimVars.(fldnms{ii})(:,:,doublePoints) = [];                          % removing double points
        elseif sizes(1)==N_eta && sizes(2)==N_xis(1)                                % it means it's a flow-field quantity to be appended
            intel_dimVars.(fldnms{ii}) = zeros(N_eta,N_xiTot);                          % allocating
            for jj=1:nargin                                                             % looping intels
                N_etajj = size(varargin{jj}.(fldnms{ii}),1);                                % number of points in the wall-normal direction
                if N_etajj ~= N_eta                                                         % if the number of wall-normal points is not consistent, we throw an error
                    error(['the number of points in the eta direction of intel ',num2str(jj),' - ',num2str(N_etajj),' is not consistent with the initial ',numstr(N_eta)]);
                end
                N_prev = sum(N_xis(1:jj-1));                                                % total number of xi locations in the previous intels
                intel_dimVars.(fldnms{ii})(:,N_prev+1:N_prev+N_xis(jj)) = real(varargin{jj}.(fldnms{ii})); % apending field
            end                                                                         % closing intel loop
            intel_dimVars.(fldnms{ii})(:,doublePoints) = [];                            % removing double points
        elseif sizes(1)==1 && sizes(2)==N_xis(1)                                    % it means it's an edge quantity to be appended
            intel_dimVars.(fldnms{ii}) = zeros(1,N_xiTot);                              % allocating
            for jj=1:nargin                                                             % looping intels
                N_prev = sum(N_xis(1:jj-1));                                                % total number of xi locations in the previous intels
                intel_dimVars.(fldnms{ii})(N_prev+1:N_prev+N_xis(jj)) = real(varargin{jj}.(fldnms{ii})); % apending field
            end                                                                         % closing intel loop
            intel_dimVars.(fldnms{ii})(:,doublePoints) = [];                            % removing double points
        else                                                                        % otherwise, we simply pass it as it is (constants, the eta vector, etc.)
            intel_dimVars.(fldnms{ii}) = real(varargin{1}.(fldnms{ii}));                % passing the field in the first intel
        end                                                                         % closing size if
    end                                                                         % closing cell/no cell if
end                                                                         % closing field-name loop