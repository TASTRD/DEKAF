function [mat_reg,forcing_reg] = matrixRegularization(mat,forcing)
% matrixRegularization is a function that regularizes a matrix and it's
% associated forcing vector, by scaling each row such that the maximum
% value is 1.
%
% Usage:
%   (1)
%       [mat_reg,forcing_reg] = matrixRegularization(mat,forcing)
%
% Examples:
%   (1)
%       mat = magic(5)
%                 17    24     1     8    15
%                 23     5     7    14    16
%                  4     6    13    20    22
%                 10    12    19    21     3
%                 11    18    25     2     9
%       forcing = [1 ; 2 ; 2 ; 3 ; 1]
%     [mat_reg,forcing_reg] = matrixRegularization(mat,forcing)
%       mat_reg =
%             0.7083    1.0000    0.0417    0.3333    0.6250
%             1.0000    0.2174    0.3043    0.6087    0.6957
%             0.1818    0.2727    0.5909    0.9091    1.0000
%             0.4762    0.5714    0.9048    1.0000    0.1429
%             0.4400    0.7200    1.0000    0.0800    0.3600
%       forcing_reg =
%             0.0417
%             0.0870
%             0.0909
%             0.1429
%             0.0400
%
% Inputs and outputs:
%   mat
%       system matrix with the linearized coefficients
%   forcing
%       system forcing vector
%   mat_reg, forcing_reg
%       same as mat and forcing but after the regularization.
%
% Author: Fernando Miro Miro
% Date: April 2019
% GNU Lesser General Public License 3.0

assembled   = [mat,forcing];
scl         = max(abs(assembled),[],2);         % maximum of each row
scl2D       = repmat(scl,[1,size(assembled,2)]);    % extending over the columns
regularized = assembled./scl2D;                 % regularizing
mat_reg     = regularized(:,1:end-1);           % extracting
forcing_reg = regularized(:,end);

end % matrixRegularization