function [mat,forcing] = impose_chargeNeutralityCondOnMat_elem(mat,forcing,elem_list,N,N_var)
% impose_chargeNeutralityCondOnMat_elem imposes the charge-neutrality
% condition on the system matrices and forcing vectors in LTEED.
%
% It does so by substituting the elemental conservation equation of
% electrons for a simple X_E=0 (the electron ELEMENT is zero if charge
% neutrality is assumed).
%
% Usage:
%   (1) 
%       [mat,forcing] = impose_chargeNeutralityCondOnMat_elem(mat,forcing,elem_list,N,N_var)
%
% Inputs and outputs:
%   mat         computational matrix built with build_mat
%   forcing     forcing vector built with eval_forcing
%   elem_list   cell of strings with the names of all elements
%   N           number of points in the computational (eta) domain
%   N_var       number of computational variables preceding the mass
%               fractions in the variable vector (df_deta, f, etc.)
%
% See also: setDefaults, build_mat
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

idx_el = find(strcmp(elem_list,'e-')); % position of the electron element
idx_eq = (N_var+idx_el-1)*N+1:(N_var+idx_el)*N; % positions spanned by the electron equation

mat(idx_eq,:) = 0;                                              % clearing matrix rows for e-
mat(:,idx_eq) = 0;                                              % clearing matrix columns for e-
mat(idx_eq,idx_eq) = speye(N);                                  % populating with the identity matrix
forcing(idx_eq) = 0;                                            % clearing forcing positions

end % impose_chargeNeutralityCondOnMat_elem