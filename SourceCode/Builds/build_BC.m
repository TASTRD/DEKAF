function [intel] = build_BC(intel,options,varargin)
% BUILD_BC(intel,options) applies the boundary conditions on the
% perturbation terms in both the system matrix and the forcing term. All
% the magnitudes of interest are passed through intel - a structure with:
%     .mat: system matrix
%     .forcing: forcing vector in the linearised system
%
% BUILD_BC(intel,options,intelp) does the same for marching cases where the
% intel at previous xi stations is required.
%
% options is a structure with options. The full details and defaults of
% every option are given in setDefaults. The options used in this func are:
% ---> N, Cooke, thermal_BC, flow, ys_e
%
% DEVELOPERS NOTE: removing the generalized i/o is easy - we just work with
% mat, and forcing.
%
% NOTE: the structure of the computational matrices is the following can be
% found in the build_mat functions
%
% See also: build_mat, setDefaults, listOfVariablesExplained, eval_ablationMassBC,
% eval_ablationEnergyBC, eval_ablationConcentrationBC, eval_blowingBC,
% eval_thermalEquilibriumBC
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

N = options.N;
D1 = intel.D1;

% finding entries in a sparse matrix is very time consuming, so here we're
% converting to full and, at the end after the values are allocated, back
% to sparse
mat     = full(intel.mat);
forcing = intel.forcing;

ic = 1; % initializing index counter
mat(1,:) = 0; % condition on f_p in the freestream
mat(1,1) = 1;
forcing(1) = 0;

mat(ic*N,:) = 0; % condition on f_p at the wall
mat(ic*N,ic*N) = 1;
forcing(ic*N) = 0; ic=ic+1;

switch options.wallBlowing_BC
    case {'none','SS'}
        mat(ic*N,:) = 0; % condition on f at the wall
        mat(ic*N,ic*N) = 1;
        forcing(ic*N) = 0; % ic is 2 at this point
    case {'Vwall','mwall'}
        [mat,forcing] = eval_blowingBC(mat,forcing,intel,options,varargin{:});
    case 'ablation'
        [mat,forcing] = eval_ablationMassBC(mat,forcing,intel,options,varargin{:});
    otherwise
        error(['the chosen wallBlowing_BC ''',options.wallBlowing_BC,''' is not supported']);
end

if options.Cooke
    mat(ic*N+1,:) = 0; % condition on k in the freestream
    mat(ic*N+1,ic*N+1) = 1;
    forcing(ic*N+1) = 0; ic = ic+1;

    mat(ic*N,:) = 0; % condition on k at the wall
    mat(ic*N,ic*N) = 1;
    forcing(ic*N) = 0; % ic is 3 at this point
end

mat(ic*N+1,:) = 0; % condition on g in the freestream
mat(ic*N+1,ic*N+1) = 1;
forcing(ic*N+1) = 0; ic=ic+1;

switch options.thermal_BC
    case 'H_F' % condition on the wall heat flux
        mat(ic*N,:)                 = 0; % condition on g_p at the wall
        mat(ic*N,(ic-1)*N+1:ic*N)   = D1(end,:);
        forcing(ic*N)               = 0; % ic is 3 or 4 at this point
    case {'Twall','gwall'} % condition on the wall temperature
        mat(ic*N,:)         = 0; % condition on g at the wall
        mat(ic*N,ic*N)      = 1;
        forcing(ic*N)       = 0; % ic is 3 or 4 at this point
    case {'ablation'} % ablative wall condition
        [mat,forcing] = eval_ablationEnergyBC(mat,forcing,intel,options,varargin{:});
    otherwise
        error(['non-identified thermal boundary condition ''',options.thermal_BC,'''']);
end
%%% TURBULENT QUANTITIES
switch options.modelTurbulence
    case {'none','SmithCebeci1967'}     % no additional arrangements needed
    case 'SSTkomega'
        mat(ic*N+1,:) = 0;                  % condition on KT in the freestream
        mat(ic*N+1,ic*N+1) = 1;
        forcing(ic*N+1) = 0; ic=ic+1;

        mat(ic*N,:) = 0;                    % condition on KT at the wall
        mat(ic*N,ic*N) = 1;
        forcing(ic*N) = 0;
        
        mat(ic*N+1,:) = 0;                  % condition on WT in the freestream
        mat(ic*N+1,ic*N+1) = 1;
        forcing(ic*N+1) = 0; ic=ic+1;

        mat(ic*N,:) = 0;                    % condition on WT at the wall
        mat(ic*N,ic*N) = 1;
        forcing(ic*N) = 0;
    otherwise
        error(['the chosen modelTurbulence ''',options.modelTurbulence,''' is not supported']);
end
%%% MULTISPECIES/MULTIELEMENT
if ismember(options.flow,{'CNE','TPGD'})
    for s=1:options.N_spec                                      % looping species equations
        mat(ic*N+1,:)                    = 0;                       % condition on the mass fraction in the freestream
        mat(ic*N+1,ic*N+1)               = 1;
        forcing(ic*N+1)                  = 0; ic=ic+1;
        switch options.wallYs_BC                                    % depending on the wall mass-fraction BC
            case 'nonCatalytic'                                         % for the non-catalytic conditi
                mat(ic*N,:)                 = 0;                            % condition on the wall mass fraction gradient
                mat(ic*N,(ic-1)*N+1:ic*N)   = D1(end,:);                    % (non-catalytic condition)
                forcing(ic*N)               = 0;                            % zero forcing
            case 'ysWall'                                               % for the fully catalytic condition
                mat(ic*N,:)                 = 0;                            % condition on the wall mass fraction
                mat(ic*N,ic*N)              = 1;
                forcing(ic*N)               = 0;                            % forcing term such that ys(wall) = Ys_bc
            case {'ablation','catalyticGamma','catalyticK'}             % ablation or catalytic BCs
                [mat,forcing] = eval_ablationConcentrationBC(mat,forcing,intel,options,varargin{:});
            otherwise
                error(['the chosen wallYs_BC ''',options.wallYs_BC,''' is not supported']);
        end
    end
elseif ismember(options.flow,{'LTEED'})
    for E=1:intel.mixCnst.N_elem                                % looping species equations
        mat(ic*N+1,:)               = 0;                            % condition on the mass fraction in the freestream
        mat(ic*N+1,ic*N+1)          = 1;
        forcing(ic*N+1)             = 0; ic=ic+1;
        switch options.wallYE_BC                                    % depending on the wall elemental-mass-fraction BC
            case 'zeroYEgradient'                                       % for the zero-gradient condition
                mat(ic*N,:)                 = 0;                            % condition on the wall elemental mass fraction gradient
                mat(ic*N,(ic-1)*N+1:ic*N)   = D1(end,:);                    % (non-catalytic condition)
                forcing(ic*N)               = 0;                            % zero forcing
            case 'yEWall'                                               % for a constant elemental-fraction condition
                mat(ic*N,:)                 = 0;                            % condition on the wall elemental mass fraction
                mat(ic*N,ic*N)              = 1;
                forcing(ic*N)               = 0;                            % forcing term such that yE(wall) = YE_bc
            otherwise
                error(['the chosen wallYE_BC ''',options.wallYE_BC,''' is not supported']);
        end
    end
end
%%% MULTI-TEMPERATURE
if strcmp(options.numberOfTemp,'2T')
    mat(ic*N+1,:)       = 0; % condition on tauv in the freestream
    mat(ic*N+1,ic*N+1)  = 1;
    forcing(ic*N+1)     = 0; ic=ic+1;
    switch options.thermalVib_BC
        case 'H_F' % condition on the wall heat flux
            % FIXME: adiabatic wall condition on a two-temperature model must
            % be revisited
            mat(ic*N,:)                 = 0; % condition on tauv_p at the wall
            mat(ic*N,(ic-1)*N+1:ic*N)   = D1(end,:);
            forcing(ic*N)               = 0; % ic is 4+N_spec or 5+N_spec at this point
        case {'Twall','gwall','frozen'} % condition on the wall vib-elec-el temperature
            mat(ic*N,:)         = 0; % condition on gv at the wall
            mat(ic*N,ic*N)      = 1;
            forcing(ic*N)       = 0; % ic is 4+N_spec or 5+N_spec at this point
        case {'ablation','thermalEquil'} % thermal equilibrium wall condition
            [mat,forcing] = eval_thermalEquilibriumBC(mat,forcing,intel,options,varargin{:});
        otherwise
            error(['non-identified thermal boundary condition ''',options.thermal_BC,'''']);
    end
end

intel.mat     = sparse(mat);
intel.forcing = forcing;

end % build_BC
