function [dy]=TM_eq(theta,y,gamma)

% y is a vector containing vr, vr' (equivalent to [vr; vtheta] since vr'=vtheta)
% Governing equations are continuity, irrotationality, & Euler's equation.

dy=zeros(2,1);
dy(1)=y(2);                                     % vr', the first derivative of vr

% Taylor-Maccoll's equation rearranged to express vr", the second derivative of vr. Checked ok.
dy(2)=(y(2)^2*y(1)-(gamma-1)/2*(1-y(1)^2-y(2)^2)*(2*y(1)+y(2)*cot(theta)))/((gamma-1)/2*(1-y(1)^2-y(2)^2)-y(2)^2);