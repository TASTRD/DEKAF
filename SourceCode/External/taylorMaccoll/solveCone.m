function [thetac,Mc,sol]=solveCone(thetas,Minf,gamma)

% Solves the right circular cone at zero angle of attack in supersonic flow
% thetas - shock angle [degrees]
% Minf - upstream Mach number
% gamma - ratio of specific heats

sol=0;                                          % initialization if there is no convergence

thetasr=thetas*pi/180;                          % Convert to radians
if (thetasr<=asin(1/Minf))
    thetac=0.0; Mc=Minf;
    return;
end

% Calculate initial flow deflection (delta) just after shock...
delta=thetaBetaM(thetasr,Minf,gamma);           % computing the flow deflection right after the shock, can be checked using Chart.2 of NACA1135, valid only right after the shock since we have a three dimensional flow.
Mn1=Minf*sin(thetasr);                          % normal flow component to the shock wave, projection
Mn2=sqrt((Mn1^2+(2/(gamma-1)))/(2*gamma/(gamma-1)*Mn1^2-1)); % eq.96 NACA1135, Mach number behind a normal shock, used here for the normal flow component
M2=Mn2/(sin(thetasr-delta));                    % flow Mach number behind the oblique shock, using projections

% All values are non-dimensionalized!
% Calculate the non-dimensional velocity just after the oblique shock
V0=(2/((gamma-1)*M2^2)+1)^(-0.5);               % see eq10.16 Anderson "Modern compressible flow". V0 here corresponds to his V'=V/Vmax

% Calculate velocity components in spherical coordinates, ok
Vr0=V0*cos(thetasr-delta);
Vtheta0=-V0*sin(thetasr-delta);

% Calculate initial values for derivatives
dVr0=Vtheta0;

% Assemble initial value for ODE solver
y0=[Vr0;dVr0];

% Set up ODE solver
% Quit integration when vtheta=0
% See: coneEvent.m
% Set relative error tolerance small enough to handle low M
options=odeset('Events',@coneEvent,'RelTol',1e-10);

% Solve by marching solution away from shock until either 0 degrees or flow
% flow tangency reached as indicated by y(2)==0.
% See cone.m
[sol]=ode15s(@TM_eq,[thetasr 1e-10],y0,options,gamma);

% Check if we have a solution, as ode15s may not converge for some values.
[n,m]=size(sol.ye);
thetac=0.0;                                     % initialization in case of non-convergence
Mc=Minf;                                        % initialization in case of non-convergence

% If ODE solver worked, calculate the angle and Mach number at the cone.
if (n>0 && m>0 && abs(sol.ye(2))<1e-12)
    thetac=sol.xe*180.0/pi;                     % angle of the cone corresponding to the shock angle specified
    Vc2=sol.ye(1)^2+sol.ye(2)^2;                % square of the normalized velocity along the surface of the cone
    Mc=((1/Vc2-1)*(gamma-1)/2)^-0.5;            % eq10.16 Anderson "Modern compressible flow", inverted to express Mc as a function of Vc
end


