function [Mc, thetas, Twall_inf, pwall_inf, rhowall_inf, converged]=TaylorMaccoll(cone_angle, Minf, gamma, silent, thetas, varargin)

%%% This function was retrieved from "http://www.ryerson.ca/~jvl/2008-09/ae8121/conenotes.pdf" as
%%% written by Dr. J. V. Lassaline and slightly modified to meet the present goals.
%%%
%%% The function receives:
%%%    - the half-angle in degree of the cone considered,
%%%    - the freestream Mach number,
%%%    - the specific heat ratio of the medium considered (assumed to be constant through the
%%%    shock waves),
%%%    - a boolean variable silent to print convergence or hide it.
%%%    - eventually an initial solution for the shock angle. If it is not specified or set to a
%%%    value smaller than the cone half-angle, it will be set automatically to an arbitrary value.
%%%    - varargin can be tolerance on the flow tangency at the surface of the cone
%%%      'tol': default 1E-6
%%%
%%% The function returns:
%%%    - the Mach number along the surface of the cone,
%%%    - the angle of the shock in degrees,
%%%    - the temperature ratio between the wall and the freestream,
%%%    - the pressure ratio between the wall and the freestream,
%%%    - the density ratio between the wall and the freestream,
%%%    - whether or not the solution is converged.
%%%
%%% original code from Dr. J. V. Lassaline, 2009.
%%% modified and commented by G. Grossir, 13th September 2013

if nargin<5 || thetas<=cone_angle               % completing or correcting the input parameters
    thetas = cone_angle+5;                      % initial guess for shock angle, used as an initial solution
end
deftol = 1E-6;
if ~isempty(varargin)
    [~,btol,itol] = find_flagAndRemove('tol',varargin);
    if btol;    tol = varargin{itol+1}; % tolerance on the flow tangency at the surface of the cone
    else;       tol = deftol;
    end
else;   tol = deftol;
end

Twall_inf=0;                                    % initialization
pwall_inf=0;                                    % initialization
rhowall_inf=0;                                  % initialization

if Minf>5
    conv=0.88;                                      % convergence speed
elseif Minf>2
    conv=0.5;
else
    conv=0.1;
end

converged=1;
iter_max=150;                                   % prescribing a maximum number of iterations
dtheta=zeros(length(iter_max),1);               % initialization
dtheta(1)=tol+1;                                % initialization
iter=0;
while abs(dtheta(end))>=tol                     % as long as the cone angle determined does not match the one specified, iterate on the shock angle.
    iter=iter+1;                                % next iteration
    if iter>iter_max                            % security if it does not converge
        converged=0;
        fprintf('--- Maximum iteration reached before convergence. Stopping here.\n')
        break
    end
    [thetac, Mc, ~] = solveCone(thetas, Minf, gamma); % solving the Taylor-Maccoll equation for the specified Mach number and an assumed shock angle
    dtheta(iter) = thetac-cone_angle;           % difference between the chosen cone angle and the one computed from the given shock angle
    if ~silent
        fprintf('--- Iteration %2.0f : Assuming shock angle=%1.3f, leads to cone angle=%1.3f (Difference with prescribed: %+.4e) with a surface Mach number of %1.3f.\n', iter, thetas, thetac, dtheta(iter), Mc)
    end
    thetas = thetas-conv*dtheta(iter)/2;        % next choice for the shock angle to match better the conical angle.
    %{
    if iter>1 && abs(dtheta(iter))>abs(dtheta(iter-1)) % diverging in the case of strong shocks
        converged=0;
        break
    end
    %}
end

%% Once the position of the shock has been found (matching the flow angle along the surface of the
% cone), the flow properties within the shock layer can be determined assuming an isentropic
% compression. Classical relationships of the NACA1135 report can be used in such a case.

if iter<=iter_max                               % if solution has converged
    % temperature ratio along the cone with respect to the freestream
    Twall_Tt2 = (1+(gamma-1)/2*Mc^2)^(-1);      % eq.43 NACA1135, valid for an adiabatic perfect gas.
    Tinf_Tt1 = (1+(gamma-1)/2*Minf^2)^(-1);     % same in the freestream
    Twall_inf = Twall_Tt2/Tinf_Tt1;             % ratio between static Twall/Tinf, since Tt1=Tt2 across a shock for a perfect gas.

    % pressure ratio along the cone with respect to the freestream
    pt2_pt1=(((gamma+1)*Minf^2*sind(thetas)^2)/((gamma-1)*Minf^2*sind(thetas)^2+2))^(gamma/(gamma-1))*...
        ((gamma+1)/(2*gamma*Minf^2*sind(thetas)^2-(gamma-1)))^(1/(gamma-1)); % eq.142 NACA1135, total pressure ratio across an oblique shock (the oblique shock wave around a cone can be identically viewed as an oblique shock wave around a 2D wedge)
    pwall_pt2 = (1+(gamma-1)/2*Mc^2)^(-gamma/(gamma-1)); % eq.44 NACA1135, valid for an isentropic perfect gas, applied here within the shock layer
    pinf_pt1 = (1+(gamma-1)/2*Minf^2)^(-gamma/(gamma-1)); % same but applied here within the freestream
    pwall_inf = pwall_pt2/pinf_pt1*pt2_pt1;     % ratio between static pwall/pinf

    % density ratio along the cone with respect to the freestream
    rhot2_rhot1=pt2_pt1;                        % eq.142 NACA1135, as for total pressure ratio
    rhowall_rhot2 = (1+(gamma-1)/2*Mc^2)^(-1/(gamma-1)); % eq.45 NACA1135, valid for an isentropic perfect gas, applied here within the shock layer
    rhoinf_rhot1 = (1+(gamma-1)/2*Minf^2)^(-1/(gamma-1)); % same but applied here within the freestream
    rhowall_inf = rhowall_rhot2/rhoinf_rhot1*rhot2_rhot1; % ratio between static rhowall/rhoinf

    if ~silent
        fprintf('\n')
        fprintf('Temperature ratio Twall/Tinf: %1.5f\n', Twall_inf)
        fprintf('Pressure ratio pwall/pinf: %1.5f\n', pwall_inf)
        fprintf('Density ratio rhowall/rhoinf: %1.5f\n\n', rhowall_inf)
    end
end


