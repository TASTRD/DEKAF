function [delta]=thetaBetaM(theta,M,gamma)

%%% Return delta for delta-theta-M relationship for oblique shocks.
%%% It requires:
%%%   - theta, shock angle in radians,
%%%   - M, freestream Mach number,
%%%   - gamma, ratio of specific heat.
%%% It returns:
%%%   - delta, the flow deflection after the oblique shock wave, in radians.

% Cut off at Mach wave angle
if (theta<=asin(1/M))
    delta=0;
    return;
end

delta=atan((2*cot(theta) * ((M*sin(theta))^2 - 1)) / (2 + M^2*(gamma+cos(2*theta)) )); % eq.139a of NACA1135 further simplified with (1-2*sin(theta)^2)=cos(2*theta) as in eq.139b

