function [value, isterminal, direction]=coneEvent(theta, y, varargin) %#ok<INUSL>

% Check cone solution for point where vtheta=0
% theta - current angle
% y- current solution vector
% gamma - ratio of specific heats

value=zeros(2,1);                               % initialization
isterminal=zeros(2,1);                          % initialization
direction=zeros(2,1);                           % initialization

% Quit if Vr goes negative (which shouldn't happen!)
value(1)=1.0;
if (y(1)<0.0)
    value(1)=0.0;
end

isterminal(1)=1;
direction(1)=0;

% Quit if Vtheta goes positive (in spherical coordinates) (which occurs at the wall)
value(2)=1.0;
if (y(2)>0.0)
    value(2)=0.0;
end

isterminal(2)=1;
direction(2)=0;