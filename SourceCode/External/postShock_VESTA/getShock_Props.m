function [T2,p2,u2,M2,beta] = getShock_Props(T1,p1,M1,R,theta,gamma)
%GETSHOCK_PROPS Compute the post-shock conditions after an oblique shock
% originated by a compression corner.
%
% Usage:
%   [T2,p2,u2,M2,beta] = getShock_Props(T1,p1,M1,R,theta,gamma,options)
%
% INPUTS
%   - T1:           temperature before the shock
%   - p1:           pressure before the shock
%   - M1:           Mach number before the shock
%   - R:            gas constant
%   - theta:        wedge angle in degrees
%   - gamma         frozen specific heat constant ratio
%
% OUTPUTS
%   - T2:           temperature after the shock
%   - p2:           pressure after the shock
%   - u2:           inviscid flow velocity at the edge of the boundary layer
%                   parallel to the wedge
%   - M2:           Mach number after the shock
%   - beta:         shock angle in degrees
%
% Notes:
%   This function applies for nonequilibrium flows at the inviscid edge of
%   the boundary layer, because flow across a shock is frozen, both
%   chemically and thermally. Hence, properties from calorically perfect
%   gas relations are applicable. In this case, nonequilibrium effects will
%   occur downstream of the frozen oblique shock wave and ultimately
%   reduce the angle of the shock toward an equilibrium value. This is due
%   to the normal component of velocity across the oblique shock
%   decreasing, with the tangential component remaining constant, curving
%   the streamlines away from their equilibrium asymptotes.
%
% See also: getLTEShock_Props
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
%
% Date: March, 12, 2015
%       March, 21, 2018

% frozen oblique shock wave angle
beta = givemeBeta(theta,M1,gamma);  % degrees

% normal component of mach wrt to the oblique wave
Mn1 = M1 * sind(beta);

% normal shock relation,
Mn2 = sqrt((Mn1^2 + 2/(gamma-1))/(2*gamma/(gamma-1)*Mn1^2 -1));
M2 = Mn2/sind(beta-theta);

% isentropic relations for thermodynamic properties
rho_ratio = ((gamma+1) * Mn1^2)/((gamma-1) * Mn1^2 + 2);
p_ratio = 1 + 2*gamma/(gamma+1) * (Mn1^2 - 1);
T_ratio = p_ratio/rho_ratio;
T2 = T_ratio * T1;
p2 = p_ratio * p1;
u2=M2*sqrt(gamma*R*T2);

end
