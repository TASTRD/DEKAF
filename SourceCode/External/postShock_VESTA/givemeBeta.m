function beta=givemeBeta(theta,M,gamma)
% This function solves the theta-beta-M equation used for the calculation
% of the position of oblique shocks. Beta (shock angle) is obtained from
% theta (post-shock velocity angle, normally coincident with the surface
% angle) and M (Mach number).
%
%  INPUT
%    - theta:   post-shock velocity angle in DEGREES, equal to the surface
%               angle for the case of a wedge
%    - M:       upstream freestream Mach number
%    - gamma:   frozen specific heat ratio
%
%  OUTPUT
%    - beta:    frozen oblique shock angle in DEGREES
%
% Author: Fernando Miro Miro
% copyright 2007-2015


first_beta=1/M*180/pi + 1.2*theta; % First guess for the value of beta
theta_max=giveme_thetamax(M,gamma);

if theta>theta_max
    error('The shock is detatched for this wedge angle')
else
    beta=fzero(@(x) func(theta,x,M,gamma),first_beta);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function f=func(theta,beta,M,gamma)

f=tand(theta)-2*cotd(beta).*(M^2*sind(beta).^2-1)./(M^2*(gamma+cosd(2*beta))+2); % note that angles are at all times in DEGREES

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function theta_max = giveme_thetamax(M,gamma) % Function used to obtain the maximum theta that will give an oblique shock
N_vec = 1e3;
tol = 1e-16;
beta_max=90;
beta_min=60;
err=1;
SOL=1;
while err>tol
    SOL_old=SOL;
    beta_vec=linspace(beta_min,beta_max,N_vec);
    [SOL,i_max]=min(func2(beta_vec,M,gamma));
    beta_max=beta_vec(i_max)+(beta_max-beta_min)/N_vec;
    beta_min=beta_vec(i_max)-(beta_max-beta_min)/N_vec;
    err=abs(SOL_old-SOL);
end
theta_max=acotd(SOL);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function f=func2(beta,M,gamma) % Function to obtain cot(theta) from beta and the Mach number.
                               % It is used in the computing of theta_max

f=1./(2*cotd(beta).*(M^2*sind(beta).^2-1)./(M^2*(gamma+cosd(2*beta))+2));

