function varargout = giveme_VESTAg(intel)
% GIVEME_VESTAG(intel) returns the self-similar variable G used by VESTA's
% boundary-layer solver - the non-dimensional TOTAL enthalpy. The input
% must be a structure with the needed fields: were g corresponds to the
% non-dimensional STATIC enthalpy, h_e is the boundary-layer edge static
% enthalpy, df_deta is the first derivative of the self-similar variable f,
% and U_e is the boundary-layer edge streamwise velocity.
%
% [G,dG_deta] = GIVEME_VESTAG(intel) also outputs the derivative of G with
% respect to the self-similar coordinate eta. In this case two additional
% fields are required: dg_deta and df_deta2.
%
% [G,dG_deta,dG_deta2] = GIVEME_VESTAG(intel) also outputs the second
% derivative of G, which requires dg_deta2 and df_deta3 as inputs.
%
% Author: Fernando Miro Miro
% Date: November, 2017

% extracting necessary variables
df_deta = intel.df_deta;
h_e = intel.h_e;
U_e = intel.U_e;
g = intel.g;

% calculating G
G = (h_e*g + 0.5*df_deta.^2*U_e^2)/(h_e + 0.5*U_e^2);

switch nargout
    case 1
        varargout{1} = G;
    case 2
        df_deta2 = intel.df_deta2;
        dg_deta = intel.dg_deta;
        dG_deta = (h_e*dg_deta + U_e^2*df_deta.*df_deta2)/(h_e + 0.5*U_e^2);
        varargout{1} = G;
        varargout{2} = dG_deta;
    case 3
        df_deta2 = intel.df_deta2;
        df_deta3 = intel.df_deta3;
        dg_deta = intel.dg_deta;
        dg_deta2 = intel.dg_deta2;
        dG_deta = (h_e*dg_deta + U_e^2*df_deta.*df_deta2)/(h_e + 0.5*U_e^2);
        dG_deta2 = (h_e*dg_deta2 + U_e^2*(df_deta2.^2 + df_deta.*df_deta3))/(h_e + 0.5*U_e^2);
        varargout{1} = G;
        varargout{2} = dG_deta;
        varargout{3} = dG_deta2;
    otherwise
        error('wrong number of outputs');
end