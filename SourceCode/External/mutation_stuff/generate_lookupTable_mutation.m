function [] = generate_lookupTable_mutation(T_range,p_range,varargin)
% GENERATE_LOOKUPTABLE_MUTATION(T_range,p_range) generates a look-up table
% for LTE conditions in the range defined by T_range and p_range.
% temperature steps are taken at 1K, and pressure ones at 10Pa. The file is
% otherwise saved with the name
%     " mutation_table_[N_T]T[Tmin]_[Tmax]_[N_p]p[pmin]_[pmax].dat "
%
% GENERATE_LOOKUPTABLE_MUTATION(T_range,p_range,N_T,N_p) allows to specify
% the number of points in the temperature and in the pressure stencil.
%
% GENERATE_LOOKUPTABLE_MUTATION(T_range,p_range,N_T,N_p,savepath) allows to
% specify a different savepath
%
% GENERATE_LOOKUPTABLE_MUTATION(T_range,p_range,N_T,N_p,savepath,var_list)
% allows to specify what variables are to be tabled. Otherwise, the
% defaults are:
%
% GENERATE_LOOKUPTABLE_MUTATION(...,options) allows to pass an options
% structre to be used by eval_prop_muatation
%
% [T p mu kappa h gamma cp rho Mm a0 ]
%
% see also eval_prop_muatation
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

structYesNo = cellfun(@isstruct,varargin); % checking what positions contain a structure
if sum(structYesNo)
    options = varargin{structYesNo}; % defining it as options
    varargin{structYesNo} = []; % removing it from
    N_args = nargin-1;
else
    options = struct;
    N_args = nargin;
end

% Checking inputs
switch N_args
    case 2
        N_T = length(T_range(1):T_range(2)); % the default is a spacing of 1K and 10Pa
        N_p = length(p_range(1):10:p_range(2));
        savepath = ['mutation_table_',num2str(N_T),'T',num2str(min(T_range)),'_',num2str(max(T_range)),'_',num2str(N_p),'p',num2str(min(p_range)),'_',num2str(max(p_range)),'.dat'];
        var_list = {'mu' 'kappa_eq' 'h' 'gamma' 'cp' 'rho' 'Mm' 'a0'};
    case 4
        N_T = varargin{1};
        N_p = varargin{2};
        savepath = ['mutation_table_',num2str(N_T),'T',num2str(min(T_range)),'_',num2str(max(T_range)),'_',num2str(N_p),'p',num2str(min(p_range)),'_',num2str(max(p_range)),'.dat'];
        var_list = {'mu' 'kappa_eq' 'h' 'gamma' 'cp' 'rho' 'Mm' 'a0'};
    case 5
        N_T = varargin{1};
        N_p = varargin{2};
        savepath = varargin{3};
        var_list = {'mu' 'kappa_eq' 'h' 'gamma' 'cp' 'rho' 'Mm' 'a0'};
    case 6
        N_T = varargin{1};
        N_p = varargin{2};
        savepath = varargin{3};
        var_list = varargin{4};
    otherwise
        error('wrong number of inputs');
end

% generating table of cases
T_vec_simp = linspace(T_range(1),T_range(2),N_T)';
p_vec_simp = linspace(p_range(1),p_range(2),N_p);
T_mat = repmat(T_vec_simp,[1,N_p]);
p_mat = repmat(p_vec_simp,[N_T,1]);
T_vec = T_mat(:);
p_vec = p_mat(:);

% if we want h but don't have cp
pos_h = find(strcmp(var_list,'h'),1);
pos_cp = find(strcmp(var_list,'cp'),1);
if ~isempty(pos_h) && isempty(pos_cp)
    var_list_mut = [var_list,'cp'];
else
    var_list_mut = var_list;
end

% evaluating mutation
state_var.p = p_vec;
state_var.T = T_vec;
mutOut = eval_prop_mutation(state_var,'LTE',var_list_mut,options);

% calculating equilibrium enthalpy from cp
% if ~isempty(pos_h)
%     cp = mutOut.cp;
%     h = mutOut.h;
%     dT = mean(diff(T_vec_simp));
%     DT = FDdif(N_T,dT,'forward6'); % obtaining derivation matrix
%     IT = integral_mat(DT,'ud'); % obtaining integration matrix
%     cp_mat = reshape(cp,[N_T,N_p]); % transforming cp and h into matrices to make the T integration
%     h_mat = reshape(h,[N_T,N_p]);
%     h0_mat = repmat(h_mat(1,:),[N_T,1]); % obtaining h at the first location (integration constant)
%     h_mateq = IT*cp_mat + h0_mat; % we integrate the cp over T and add the h at the first T (integration constant)
%     mutOut.h = reshape(h_mateq,[N_T*N_p,1]);
% end

% reshaping
mat_out(:,1) = T_vec;
mat_out(:,2) = p_vec;
it = 2; % initialising
for i=1:length(var_list)
    if length(mutOut.(var_list{i}))~=N_T*N_p % it means that the field in question is a list,
        % meaning that mutation outputted a species-dependent variable (rho_s, or something like that)
        for s=1:length(mutOut.(var_list{i}))
            it = it+1;
            mat_out(:,it) = mutOut.(var_list{i}){s};
        end
    else
        it = it+1;
        mat_out(:,it) = mutOut.(var_list{i});
    end
end

% Writing to file
dlmwrite(savepath,mat_out,'\t');

disp(['Mutation table written to ',savepath]);
