function varargout = mppequilWrap(varargin)
% mppequilWrap is a "fast-and-dirty" wrapper for mutation++'s mppequil
% function.
%
% Usage:
%   (1)
%       varargout = mppequilWrap(T,p,m_vec,s_vec,mixture)
%       |--> evaluates at a single temperature and pressure
%
%   (2)
%       varargout = mppequilWrap([Tmin,dT,Tmax],[pmin,dp,pmax],m_vec,s_vec,mixture)
%       |--> evaluates in a range of temperatures and pressures
%
%   (3)
%       varargout = mppequilWrap(...,'-r',r_vec,N_reac)
%       |--> allows to choose also reaction quantities, but requires the
%       user to pass the number of reactions (and therefore fields) that
%       are expected.
%
%   (4)
%       varargout = mppequilWrap(...,'-o',o_vec,No_vec)
%       |--> same story but for the "other" properties
%
% Inputs and outputs:
%   T, Tmin, dT, Tmax
%       temperatures
%   p, pmin, dp, pmax
%       pressures
%   m_vec
%       vector of mixture properties to be computed (see mppequil catalogue
%       at the end of the help)
%   s_vec
%       vector of species properties to be computed (see mppequil catalogue
%       at the end of the help)
%   r_vec
%       vector of reaction properties to be computed (see mppequil catalogue
%       at the end of the help)
%   N_reac
%       number of reactions expected from the mixture
%   o_vec
%       vector of other properties to be computed (see mppequil catalogue
%       at the end of the help)
%   No_vec
%       number of outputs expected from each "other property" requested
%   varargout
%       matrices or cells of matrices of size N_T x N_p with the different
%       properties chosen in m_vec, s_vec and o_vec
%
% mppequil help:
% Mixture values (example format: "1-3,7,9-11"):
%     0 : Th        [K]         heavy particle temperature
%     1 : P         [Pa]        pressure
%     2 : B         [T]         magnitude of magnetic field
%     3 : rho       [kg/m^3]    density
%     4 : nd        [1/m^3]     number density
%     5 : Mw        [kg/mol]    molecular weight
%     6 : Cp_eq     [J/mol-K]   equilibrium specific heat at constant pressure
%     7 : H         [J/mol]     mixture enthalpy
%     8 : S         [J/mol-K]   entropy
%     9 : Cp_eq     [J/kg-K]    equilibrium specific heat at constant pressure
%     10: H         [J/kg]      mixture enthalpy
%     11: H-H0      [J/kg]      mixture enthalpy minus the enthalpy at 0K
%     12: S         [J/kg-K]    entropy
%     13: Cv_eq     [J/kg-K]    equilibrium specific heat at constant volume
%     14: Cp        [J/mol-K]   frozen specific heat at constant pressure
%     15: Cv        [J/mol-K]   frozen specific heat at constant volume
%     16: Cp        [J/kg-K]    frozen specific heat at constant pressure
%     17: Cv        [J/kg-K]    frozen specific heat at constant volume
%     18: gam_eq    [-]         equilibrium ratio of specific heats
%     19: gamma     [-]         frozen ratio of specific heat
%     20: Ht        [J/mol]     translational enthalpy
%     21: Hr        [J/mol]     rotational enthalpy
%     22: Hv        [J/mol]     vibrational enthalpy
%     23: Hel       [J/mol]     electronic enthalpy
%     24: Hf        [J/mol]     formation enthalpy
%     25: Ht        [J/kg]      translational enthalpy
%     26: Hr        [J/kg]      rotational enthalpy
%     27: Hv        [J/kg]      vibrational enthalpy
%     28: Hel       [J/kg]      electronic enthalpy
%     29: Hf        [J/kg]      formation enthalpy
%     30: e         [J/mol]     mixture energy
%     31: e         [J/kg]      mixture energy
%     32: mu        [Pa-s]      dynamic viscosity
%     33: lambda    [W/m-K]     mixture equilibrium thermal conductivity
%     34: lam_reac  [W/m-K]     reactive thermal conductivity
%     35: lam_bb    [W/m-K]     Butler-Brokaw reactive thermal conductivity
%     36: lam_soret [W/m-K]     Soret thermal conductivity
%     37: lam_int   [W/m-K]     internal energy thermal conductivity
%     38: lam_h     [W/m-K]     heavy particle translational thermal conductivity
%     39: lam_e     [W/m-K]     electron translational thermal conductivity
%     40: sigma     [S/m]       electric conductivity (B=0)
%     41: a_f       [m/s]       frozen speed of sound
%     42: a_eq      [m/s]       equilibrium speed of sound
%     43: Eam       [V/K]       ambipolar electric field (SM Ramshaw)
%     44: drho/dP   [kg/J]      equilibrium density derivative w.r.t pressure
%
% Species values (same format as mixture values):
%     0 : X         [-]         mole fractions
%     1 : dX/dT     [1/K]       partial of mole fraction w.r.t. temperature
%     2 : Y         [-]         mass fractions
%     3 : rho       [kg/m^3]    mass densities
%     4 : conc      [mol/m^3]   molar concentrations
%     5 : Cp        [J/mol-K]   specific heats at constant pressure
%     6 : H         [J/mol]     enthalpies
%     7 : S         [J/mol-K]   entropies
%     8 : G         [J/mol]     Gibbs free energies
%     9 : Cp        [J/kg-K]    specific heats at constant pressure
%     10: H         [J/kg]      enthalpies
%     11: S         [J/kg-K]    entropies
%     12: G         [J/kg]      Gibbs free energies
%     13: J         [kg/m^2-s]  Species diffusion fluxes (SM Ramshaw)
%     14: omega     [kg/m^3-s]  production rates due to reactions
%     15: Omega11   [m^2]       (1,1) pure species collision integrals
%     16: Omega22   [m^2]       (2,2) pure species collision integrals
%     17: Chi       [-]         species thermal diffusion ratios
%     18: Dm        [m^2/s]     mixture averaged diffusion coefficients
%
% Reaction values (same format as mixture values):
%     0 : kf        [mol,m,s,K] forward reaction rate coefficients
%     1 : kb        [mol,m,s,K] backward reaction rate coefficients
%
% Other values (same format as mixture values):
%     0 : Dij       [m^2/s]     multicomponent diffusion coefficients
%     1 : pi_i      [-]         element potentials
%     2 : N_p       [mol]       phase moles
%     3 : iters     [-]         number of continuation step iterations
%     4 : newts     [-]         total number of newton iterations
%     5 : Fp_k      [kg/m-Pa-s] elemental diffusion fluxes per pressure gradient
%     6 : Ft_k      [kg/m-K-s]  elemental diffusion fluxes per temperature gradient
%     7 : Fz_k      [kg/m-s]    elemental diffusion fluxes per element mole fraction gradient
%     8 : sigmaB    [S/m]       anisotropic electric conductivity
%     9 : lamB_e    [W/m-K]     anisotropic electron thermal conductivity
%     10: nDij      [1/m-s]     binary diffusion coefficients times number density
%     11: Q11ij     [m^2]       (1,1) binary collision integral
%     12: Q22ij     [m^2]       (2,2) binary collision integral
%     13: Bstij     [-]         ratio of binary collision integrals
%     14: Cstij     [-]         ratio of binary collision integrals
%     15: Q11ei     [m^2]       (1,1) binary collision integral with electrons
%     16: Q22ei     [m^2]       (2,2) binary collision integral with electrons
%     17: Bstei     [-]         ratio of binary collision integrals with electrons
%     18: Cstei     [-]         ratio of binary collision integrals with electrons
%     19: Q13ei     [m^2]       (1,3) binary collision integral with electrons
%     20: Q14ei     [m^2]       (1,4) binary collision integral with electrons
%     21: Q15ei     [m^2]       (1,5) binary collision integral with electrons
%     22: Q23ee     [m^2]       (2,3) binary collision integral between electrons
%     23: Q24ee     [m^2]       (2,4) binary collision integral between electrons
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

mutdatfile = 'temporal_mutationFile.dat';

[varargin,~,idx_r] = find_flagAndRemove('-r',varargin);
if ~isempty(idx_r)
    r_vec = varargin{idx_r};
    N_reac = varargin{idx_r+1};
    varargin(idx_r:idx_r+1) = []; % eliminating from varargin
else
    r_vec = [];
    N_reac = 0;
end
N_r = length(r_vec);

[varargin,~,idx_o] = find_flagAndRemove('-o',varargin);
if ~isempty(idx_o)
    o_vec = varargin{idx_o};
    No_vec = varargin{idx_o+1};
    varargin(idx_o:idx_o+1) = []; % eliminating from varargin
else
    o_vec = [];
    No_vec = [];
end
N_o = length(o_vec);

ic = 1;
switch length(varargin{ic})
    case 1
        T_vec = varargin{ic};
    case 3
        T_vec = varargin{ic}(1):varargin{ic}(2):varargin{ic}(3);
    otherwise
        error(['the length of input 1 (',num2str(length(varargin{ic})),') is not supported.']);
end
N_T = length(T_vec);
ic=ic+1;

switch length(varargin{ic})
    case 1
        p_vec = varargin{ic};
    case 3
        p_vec = varargin{ic}(1):varargin{ic}(2):varargin{ic}(3);
    otherwise
        error(['the length of input 1 (',num2str(length(varargin{ic})),') is not supported.']);
end
N_p = length(p_vec);
ic=ic+1;

m_vec = varargin{ic}; N_m = length(m_vec);ic=ic+1;
s_vec = varargin{ic}; N_s = length(s_vec);ic=ic+1;

mixture = varargin{ic};

T_in = cellfun(@num2str,num2cell(varargin{1}),'UniformOutput',false);
p_in = cellfun(@num2str,num2cell(varargin{2}),'UniformOutput',false);
m_in = cellfun(@num2str,num2cell(m_vec),'UniformOutput',false);
s_in = cellfun(@num2str,num2cell(s_vec),'UniformOutput',false);
r_in = cellfun(@num2str,num2cell(r_vec),'UniformOutput',false);
o_in = cellfun(@num2str,num2cell(o_vec),'UniformOutput',false);

if ~isempty(m_in)
    m_str = ['-m ',strjoin(m_in,','),' '];
else
    m_str = '';
end

if ~isempty(s_in)
    s_str = ['-s ',strjoin(s_in,','),' '];
else
    s_str = '';
end

if ~isempty(r_in)
    r_str = ['-r ',strjoin(r_in,','),' '];
else
    r_str = '';
end

if ~isempty(o_in)
    o_str = ['-o ',strjoin(o_in,','),' '];
else
    o_str = '';
end

%%%% END OF INPUTS
%{%
string4mut = ['mppequil --no-header -T ',strjoin(T_in,':'),' -P ',strjoin(p_in,':'),' ',m_str,s_str,r_str,o_str,mixture,' > ',...
    mutdatfile];
unix(string4mut);
%}
%% Load the data and delete provisional file
data = load(mutdatfile);
delete(mutdatfile);

%% Extracting mutation data for chosen pressure
N_spec = str2double(regexp(mixture,'\d+','match','once')); % first digit in mixture string
iv = 1; % varargout counter
ic = 1; % column counter
for im = 1:N_m
    varargout{iv} = reshape(data(:,ic),[N_T,N_p]);
    iv=iv+1; ic=ic+1;
end
for is = 1:N_s
    for s=1:N_spec
        varargout{iv}{s} = reshape(data(:,ic),[N_T,N_p]);
        ic=ic+1;
    end
    iv=iv+1;
end
for ir = 1:N_r
    for r=1:N_reac
        varargout{iv}{r} = reshape(data(:,ic),[N_T,N_p]);
        ic=ic+1;
    end
    iv=iv+1;
end
for io = 1:N_o
    for o=1:No_vec(io)
        varargout{iv}{o} = reshape(data(:,ic),[N_T,N_p]);
        ic=ic+1;
    end
    iv=iv+1;
end

