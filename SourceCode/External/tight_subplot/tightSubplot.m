function [ha, pos] = tightSubplot(Nh, Nw, gap, marg_h, marg_w,varargin)
% tight_subplot creates "subplot" axes with adjustable gaps and margins
%
% Usage:
%   (1)
%       [ha, pos] = tightSubplot(Nh, Nw, gap, marg_h, marg_w)
%
%   in:  Nh      number of axes in hight (vertical direction)
%        Nw      number of axes in width (horizontaldirection)
%        gap     gaps between the axes in normalized units (0...1)
%                   or [gap_h gap_w] for different gaps in height and width
%        marg_h  margins in height in normalized units (0...1)
%                   or [lower upper] for different lower and upper margins
%        marg_w  margins in width in normalized units (0...1)
%                   or [left right] for different left and right margins
%
%  out:  ha     array of handles of the axes objects
%                   starting from upper left corner, going row-wise as in
%                   subplot
%        pos    positions of the axes objects
%
%  Example: ha = tightSubplot(3,2,[.01 .03],[.1 .01],[.01 .01])
%           for ii = 1:6; axes(ha(ii)); plot(randn(10,ii)); end
%           set(ha(1:4),'XTickLabel',''); set(ha,'YTickLabel','')
%
%   (2)
%       [...] = tightSubplot(...,'groups',groupID1,groupID2,...)
%       |--> allows to define groups of subplot positions that will be
%       merged into a single axes. For instance:
%           tightSubplot(1,4,...,'groups',[1,2])
%       returns a 1 x 3 subplot like:
%                      __________________________________
%                     |                |        |        |
%                     |                |        |        |
%                     |                |        |        |
%                     |       1        |    2   |    3   |
%                     |                |        |        |
%                     |                |        |        |
%                     |________________|________|________|
%
%       Regarding the numbering, a subplot is considered to start
%       wherever its top-left corner is placed. For example, inputing:
%           tightSubplot(4,4,...,'groups',[1,2,5,6],[12,16],[14,15])
%       returns:
%                      __________________________________
%                     |                |        |        |
%                     |                |        |        |
%                     |                |        |        |
%                     |                |    2   |    3   |
%                     |                |        |        |
%                     |                |        |        |
%                     |       1        |--------+--------|
%                     |                |        |        |
%                     |                |        |        |
%                     |                |        |        |
%                     |                |    4   |    5   |
%                     |                |        |        |
%                     |                |        |        |
%                     |----------------+--------+--------|
%                     |        |       |        |        |
%                     |        |       |        |        |
%                     |        |       |        |        |
%                     |   6    |   7   |    8   |        |
%                     |        |       |        |        |
%                     |        |       |        |        |
%                     |--------+-------+--------|    9   |
%                     |        |                |        |
%                     |        |                |        |
%                     |        |                |        |
%                     |   10   |       11       |        |
%                     |        |                |        |
%                     |        |                |        |
%                     |________|________________|________|
%
%   (3)
%       [...] = tightSubplot(...,'custom_gapw',idx_gap,gapw,...)
%       |--> allows to customize a series of width gaps, identified by
%       idx_gap, and set them to a value gapw. (idx_gap and gapw can both
%       be vectors, such that for instance gap idx_gap(3) would be gapw(3)
%       long.
%
%   (4)
%       [...] = tightSubplot(...,'custom_gaph',idx_gap,gaph,...)
%       |--> same story but for the height gaps.
%
%   Author: Pekka Kumpulainen 21.5.2012   @tut.fi
%   Tampere University of Technology / Automation Science and Engineering
%   Modifications: Fernando Miro Miro 13.2.2019
%   von Karman Institute for Fluid Dynamics

% analyzing inputs:
[varargin,~,idx] = find_flagAndRemove('groups',varargin); % searching for flag
eos = isempty(idx);                                     % end-of-search boolean (if the flag is not found, then there's no need to loop at all
ii=1;                                                   % group counter
sbplt_groups = {};                                      % allocating
while ~eos                                              % until we reach the end of the search
    if length(varargin)<idx || ischar(varargin{idx})        % if there are no more entries in varargin, or if the entry is a string
        eos=true;                                               % end of search
    else                                                    % otherwise
        sbplt_groups{ii} = varargin{idx};                       % take the group from varargin
        varargin(idx) = [];                                     % remove it from varargin
        ii=ii+1;                                                % increase the counter
    end
end
if length(unique([sbplt_groups{:}])) ~= length([sbplt_groups{:}])           % checking that a certain subplot was not used more than twice
    error('one same subplot position cannot be grouped into more than one group');
end
[varargin,~,idx] = find_flagAndRemove('custom_gapw',varargin); % searching for flag
if isempty(idx)                                         % if we didn't find it
    idx_gapw = [];              gapw = [];                  % make everything empty
else                                                    % if we did
    idx_gapw = varargin{idx};   gapw = varargin{idx+1};     % extract from varargin
    varargin(idx:idx+1) = [];                               % empty varargin
end
[varargin,~,idx] = find_flagAndRemove('custom_gaph',varargin); % searching for flag
if isempty(idx)                                         % if we didn't find it
    idx_gaph = [];              gaph = [];                  % make everything empty
else                                                    % if we did
    idx_gaph = varargin{idx};   gaph = varargin{idx+1};     % extract from varargin
    varargin(idx:idx+1) = [];                               % empty varargin
end

if nargin<3;                        gap = .02;      end
if nargin<4 || isempty(marg_h);     marg_h = .05;   end
if nargin<5;                        marg_w = .05;   end

if numel(gap)==1;
    gap = [gap gap];
end
if numel(marg_w)==1;
    marg_w = [marg_w marg_w];
end
if numel(marg_h)==1;
    marg_h = [marg_h marg_h];
end

% Setting up gaps
gaph_vec = repmat(gap(1) , [1,Nh]);     % start by making all gaps the same
gapw_vec = repmat(gap(2) , [1,Nw]);
gaph_vec(end) = 0;                      % making dummy last gap equal to zero
gapw_vec(end) = 0;
gaph_vec(idx_gaph) = gaph;              % impose gaps other than the standard
gapw_vec(idx_gapw) = gapw;

% obtaining vertical and horizontal separations
axh = (1-sum(marg_h)-sum(gaph_vec))/Nh;
axw = (1-sum(marg_w)-sum(gapw_vec))/Nw;

% building corner position matrices
%  Each subplot will have 4 corners, of which we only want the bottom-left
%  and top-right:
%           _____ tr
%          |     |
%          |     |
%          |_____|
%        bl
corn_blx = zeros(Nw,Nh);     corn_trx = zeros(Nw,Nh);
corn_bly = zeros(Nw,Nh);     corn_try = zeros(Nw,Nh);
py = 1-marg_h(2);                                       % initial y position (top-right corner)
for iy=1:Nh                                             % looping in y
    px = marg_w(1);                                         % initial x position (top-right corner)
    for ix=1:Nw                                             % looping in x
        corn_trx(ix,iy) = px+axw;   corn_try(ix,iy) = py;       % top-right
        corn_blx(ix,iy) = px;       corn_bly(ix,iy) = py-axh;   % bottom-right
        px = px+axw+gapw_vec(ix);                               % increasing x position
    end
    py = py-axh-gaph_vec(iy);                               % increasing y position
end

% creating subplot ID matrix
sbpltID_mat = reshape(1:Nh*Nw,[Nw,Nh]);                 % E.g.: for 3 x 2 [[1,3,5] ; [2,4,6]]
for ig=1:length(sbplt_groups)                           % looping subplot groups
    minInGroup = min(sbplt_groups{ig});                     % minimum member of the group (top-left-most point)
    sbpltID_mat(sbplt_groups{ig}) = minInGroup;             % making all numbers equal to the minimum in the group (top-left corner)
end

% creating subplots
ip_unique = unique(sort(sbpltID_mat(:)));               % list of unique indices remaining in
for ip=1:length(ip_unique)                              % looping unique subplot numbers
    px = min(corn_blx(sbpltID_mat==ip_unique(ip)));         % bottom-left postiion
    py = min(corn_bly(sbpltID_mat==ip_unique(ip)));
    pX = max(corn_trx(sbpltID_mat==ip_unique(ip)));         % top-right postiion
    pY = max(corn_try(sbpltID_mat==ip_unique(ip)));
    ha(ip) = axes('Units','normalized', ...                 % defining axes
            'Position',[px py pX-px pY-py] ...
            ...,'XTickLabel','' ...
            ...,'YTickLabel',''...
            );
end
if nargout > 1
    pos = get(ha,'Position');
end
ha = ha(:);
