function [intel,options] = eval_dimVars(intel,options,varargin)
%EVAL_DIMVARS Calculate dimensional state variables from
% non-dimensionalized variables
%
% Usage (basic):
%   (1)
%       intel = eval_dimVars(intel,options)
%
% Usage (for self-similar cases with a non-zero beta):
%   (2)
%       intel = eval_dimVars(...,'x_ref',x_ref)
%       |--> one can fix the value of x for which the edge conditions
%       correspond to the provided *_e. Only supported if
%       options.dimXQSS=true or options.fixReQSS=true
%   (3)
%       intel = eval_dimVars(...,'Re_ref',Re_ref)
%       |--> one can fix the value of the Reynolds number for which the
%       edge conditions correspond to the provided *_e. Only supported if
%       options.fixReQSS=true
%   (4)
%       intel = eval_dimVars(...,'xi_ref',xi_ref)
%       |--> one can fix the value of xi for which the edge conditions
%       correspond to the provided *_e. Only supported if
%       options.dimXQSS=false or options.fixReQSS=false
%
% EXPLANATION for usages 2-4:
%   When working with self-similar solutions with a non-zero beta or Theta,
% it is necessary to specify a reference streamwise position, which
% corresponds to the location at which the edge conditions (U_e, T_e, etc.)
% are equal to the values passed by the user.
%   This is simply a consequence of the fact that, in such cases, the U_e
% profile, for instance, is equal to (see White1991):
%
%       U_e(xi) = U_ref * (xi/xi_ref)^(beta/2)
%
%   The user provides U_ref through intel.U_e, but it is also necessary to
% provide xi_ref. Alternatively one can also provide the streamwise
% position through the physical streamwise position (x_ref) or the Reynolds
% number (Re_ref). From them, one can then retrieve xi_ref.
%
% Examples for self-similar solutions:
%   (1)
%       % How to obtain the streamwise location from fixing its dimensional
%       value (x).
%       [intel,options] = DEKAF(intel,options);
%       options.dimXQSS     = true;     % fixing boolean to pass x
%       intel.x             = 0.5;      % [m]
%       [intel,options] = eval_dimVars(intel,options);
%
%   (2)
%       % How to obtain the streamwise location from fixing a value of the
%       Reynolds number based on the Blasius length (Re).
%       [intel,options] = DEKAF(intel,options);
%       options.fixReQSS    = true;     % fixing boolean to pass Reynolds
%       intel.Re            = 2000;     % [-]
%       [intel,options] = eval_dimVars(intel,options);
%
%
% eval_dimVars appends the following dimensional variables as new fields
%   onto the struct, intel if options.BLproperties = true.
%   .u      : Horizontal velocity component             [m/s]
%   .v      : Vertical velocity component               [m/s]
%   .w      : Spanwise velocity component               [m/s]
%   .rho    : Mass density of fluid mixture             [kg/m^3]
%   .p      : Static pressure                           [Pa]
%   .mu     : Viscosity of fluid mixture                [m^2/s]
%   .kappa  : Thermal conductivity of fluid mixture     [W/(m*K)]
%   .h      : Static enthalpy                           [J/kg]
%   .htot   : Total enthalpy                            [J/kg]
%
% Additionally, the derivative of rho with respect to eta is appended as a
%   new field onto intel (intel.drho_deta), as it is required for alternate
%   variable mapping.
%
% Wall values calculated:
%   .St     : Stanton number (non-reactive)             [-]
%
% Boundary-layer characteristics calculated (when options.BLproperties=true):
%   .delta99        : 99% boundary-layer y thickness based on u     [m]
%   .eta99          : 99% boundary-layer eta thickness based on u   [-]
%   .deltaH999      : 99.9% boundary-layer y thickness based on H   [m]
%   .etaH999        : 99.9% boundary-layer eta thickness based on H [-]
%   .glBlasius      : global Blasius length scale       [m]
%   .x              : local streamwise coordinate       [m]
%   .llBlasius      : local  Blasius length scale       [m]
%   .Re_x           : streamwise coordinate Reynolds number [-]
%   .Re_l           : local Blasius length Reynolds number [-]
%   .deltastar      : Displacement thickness            [m]
%   .thetastar      : Momentum thickness                [m]
%   .deltaestar     : Energy thickness                  [m]
%   .deltahstar     : Static Enthalpy thickness         [m]
%   .shapefactor    : Shape factor                      [-]
%
% Truncation errors are returned through structs for some boundary-layer
%   characteristics. These can be used to determine if the y_max value is
%   too small (within the boundary layer).
%   .deltastar_te   : Displacement thickness error      [m]
%   .thetastar_te   : Momentum thickness error          [m]
%   .deltaestar_te  : Energy thickness error            [m]
%   .shapefactor_te : Shape factor error                [-]
%
% Notes:
%   Translational temperature T is not appended as a new field onto the
%   struct, as it calculated in the main DEKAF solver. There is no
%   assumption on flow condition, i.e., the program can handle the
%   following cases:
%     a) Calorically Perfect Gas (CPG)
%     b) Thermally Perfect Gas (TPG)
%     c) Local thermodynamic equilibrium (LTE)
%
% On the calculation of the derivative of eta with respect to x:
%   Solve a linear, first-order, variable-coefficient, ordinary
%   differential equation for variable a = deta_dx, where
%     da_dy = = M1 + M2*a
%       with prescribed boundary condition a = 0 @ y = 0.
%     M2 = U_e./sqrt(2*xi).*drho_deta;
%     M1 = (dU_e_dx./sqrt(2*xi) - U_e.*(2*xi).^(-3/2).*dxi_dx).*rho + ...
%           U_e./sqrt(2*xi).*drho_dxi.*dxi_dx;
%
% References:
%   Probstein, R. F., & Elliott, D. (1956). The Transverse Curvature Effect
% in Compressible Axially Symmetric Laminar Boundary-Layer Flow. Journal of
% Aeronautical Sciences, 23(3), 208--224.
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

[varargin,bCompute_yE] = find_flagAndRemove('compute_yE',varargin);

% setting defaults
options.dimoutput = true;                                                   % we obviously want dimensional quantities, since we're calling eval_dimVars
[intel,options] = setDefaults4evalDimVars(intel,options);

% Computing the elemental mass fractions if necessary
if bCompute_yE
    X_E = getElement_XE_from_ys(cell1D2matrix2D(intel.ys),intel.mixCnst.Mm_s,intel.mixCnst.elemStoich_mat);
    intel.yE = matrix2D2cell1D(getEquilibrium_ys(X_E,intel.mixCnst.Mm_E));
end

% checking if x or xi were passed
if ~options.dimXQSS && ~options.fixReQSS && ~isfield(intel,'xi')
    error('dimensional profiles were requested, and the options dimXQSS and fixReQSS were set to false, yet there was no dimensional value of xi in intel.xi');
elseif options.dimXQSS && ~isfield(intel,'x')
    error('dimensional profiles were requested, and the option dimXQSS was set to true, yet there was no dimensional value of x in intel.x');
elseif options.fixReQSS && ~isfield(intel,'Re')
    error('dimensional profiles were requested, and the option fixReQSS was set to true, yet there was no value of Re in intel.Re');
end

% checking how much we must extend our edge profiles to account for the
% various introduced x/xi locations
if ~options.marchYesNo && options.dimXQSS && length(intel.x)>1
    N_xtnd = length(intel.x);
elseif ~options.marchYesNo && options.fixReQSS && length(intel.Re)>1
    N_xtnd = length(intel.Re);
elseif ~options.marchYesNo && isfield(intel,'xi') && length(intel.xi)>1
    N_xtnd = length(intel.xi);
else
    N_xtnd = 1;
end

if isfield(options, 'mapOpts') && isfield(options.mapOpts, 'separation_i_xi') && isfield(intel, 'xi') && ...
        options.mapOpts.separation_i_xi < length(intel.xi)
    % account for a reduced number of xi stations if separation is reached
    N_x_reached = options.mapOpts.separation_i_xi;
    % put dummy nonzero value in post-separation region to avoid infs on rho
    intel.j(:,N_x_reached+1:end) = 1;
else
    N_x_reached = NaN; % placeholder
end

intel.U_e = repmat(intel.U_e,[1,N_xtnd]);
intel.p_e = repmat(intel.p_e,[1,N_xtnd]);
intel.mu_e = repmat(intel.mu_e,[1,N_xtnd]);
intel.rho_e = repmat(intel.rho_e,[1,N_xtnd]);
intel.h_e = repmat(intel.h_e,[1,N_xtnd]);
intel.H_e = repmat(intel.H_e,[1,N_xtnd]);
intel.T_e = repmat(intel.T_e,[1,N_xtnd]);
intel.beta = repmat(intel.beta,[1,N_xtnd]);
intel.Theta = repmat(intel.Theta,[1,N_xtnd]);
intel.M_e = repmat(intel.M_e,[1,N_xtnd]);
intel.gam_e = repmat(intel.gam_e,[1,N_xtnd]);

% Extract necessary parameters
N_eta    = options.N;
mixCnst  = intel.mixCnst;
N_spec   = mixCnst.N_spec;
beta     = intel.beta;
Theta    = intel.Theta;
U_e      = intel.U_e;
W_0      = intel.W_0;
p_e      = intel.p_e;
mu_e     = intel.mu_e;
% kappa_e  = intel.kappa_e;
rho_e    = intel.rho_e;
h_e      = intel.h_e;
H_e      = intel.H_e;
T_e      = intel.T_e;
ys_e     = intel.ys_e;
f        = intel.f;
df_deta  = intel.df_deta;
df_deta2 = intel.df_deta2;
df_deta3 = intel.df_deta3;
G        = intel.g;
dG_deta  = intel.dg_deta;
dG_deta2 = intel.dg_deta2;
k        = intel.k;
dk_deta  = intel.dk_deta;
dk_deta2 = intel.dk_deta2;
if N_spec>1
ys        = intel.ys;
dys_deta  = intel.dys_deta;
dys_deta2 = intel.dys_deta2;
else
    ys = {ones(size(f))};
    dys_deta = {zeros(size(f))};
    dys_deta2 = {zeros(size(f))};
end
tau        = intel.tau;
dtau_deta  = intel.dtau_deta;
dtau_deta2 = intel.dtau_deta2;
j        = intel.j;
% C        = intel.C;
% a1       = intel.a1;
switch options.numberOfTemp
    case '1T'
        Tv_e0       = T_e(1);
    case '2T'
        intel.Tv_e = repmat(intel.Tv_e,[1,N_xtnd]);
        Tv_e        = intel.Tv_e;
        Tv_e0       = Tv_e(1);
        tauv        = intel.tauv;
        dtauv_deta  = intel.dtauv_deta;
        dtauv_deta2 = intel.dtauv_deta2;
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

% Determine if input is marching or non-marching
marchYesNo = options.marchYesNo;

% Correct edge values for nonzero beta values in QSS
% WARNING: This section is currently not valid for LTE when mu_e is a
% function of pressure!
switch options.flow % for LTEED, all functions need XE_s instead of ys_e as inputs
    case {'CPG','TPG','TPGD','CNE','LTE'};  XYs_e = ys_e;                                                                       % passing ys_e
    case 'LTEED';                           XYs_e = matrix2D2cell1D(getSpecies_X(cell1D2matrix2D(intel.yE_e),[],mixCnst.Mm_E)); % passing XE_e
    otherwise;                              error(['The chosen flow assumption ''',options.flow,''' is not supported']);
end
H_e0    = H_e(1);       % reference values
U_e0    = U_e(1);
p_e0    = p_e(1);
T_e0    = T_e(1);
XYs_e0   = cellfun(@(cll)cll(1),XYs_e(:).','UniformOutput',false);
if marchYesNo           % If the solution was marched
    xi = intel.xi;          % we can extract it directly
    xi_ref = xi(1);         % NOTE that these values are for the application of the QSS solution BEFORE THE FIRST XI STATION
    xi_max = min(xi);       % ... that's why xi_max is min(xi), because xi_max actually refers to the maximum of the pre-marching region
    xi_min = xi_max / 100;
    xPhys = intel.xPhys;
else
    [intel,xi,xi_ref,xi_min,xi_max,x,xPhys,U_e,rho_e,p_e,T_e,H_e,h_e,mu_e] = ...
    getXi_QSS(intel,options,beta,Theta,U_e,W_0,rho_e,p_e,T_e,Tv_e0,H_e,h_e,mu_e,XYs_e,varargin{:});
end

N_x = length(xi);
if isnan(N_x_reached);     N_x_reached = N_x; end

% Resize so that this function is generalized for marching and non-marching
xi       = repmat(xi,     [N_eta,1]);
U_e      = repmat(U_e,    [N_eta,1]);
p_e      = repmat(p_e,    [N_eta,1]);
T_e      = repmat(T_e,    [N_eta,1]);
rho_e    = repmat(rho_e,  [N_eta,1]);
mu_e     = repmat(mu_e,   [N_eta,1]);
% kappa_e  = repmat(kappa_e,[N_eta,1]);
h_e      = repmat(h_e,    [N_eta,1]);
H_e      = repmat(H_e,    [N_eta,1]);
beta     = repmat(beta,   [N_eta,1]);
% For stagnation flow, we need dUe_dx at the first point as extra
% information. It cannot be inferred from the other variables, as it would
% require evaluating expressions that are indeterminate, 0/0.
if U_e(1) == 0
    if ~isfield(intel, 'dUe0_dx')
        error(['Stagnation flow has been detected in your boundary layer\n',...
            'In order to redimensionalize, the field ''dUe0_dx'' must be supplied to intel!']);
    end
    dUe0_dx = intel.dUe0_dx;
    if dUe0_dx < 0
        error(['The orientation of the velocity gradient at stagnation is upstream, not downstream!\n',...
            'This ultimately leads to complex arithmetic -- please correct your assignment of intel.dUe0_dx']);
    end
    % This term Ue_sqrt2xi0 represents the limiting case of U_e divided by
    % sqrt(2*xi) for stagnation flow only (when U_e = K*x)
    Ue_sqrt2xi0 = sqrt(dUe0_dx/(rho_e(1,1)*mu_e(1,1)));
else
    Ue_sqrt2xi0 = U_e(1,1)./sqrt(2*xi(1,1));
end

% Resizing for the case where several x locations are to be evaluated for
% an QSS profile
f           = repmat(f,[1,N_xtnd]);
df_deta     = repmat(df_deta,[1,N_xtnd]);
df_deta2    = repmat(df_deta2,[1,N_xtnd]);
df_deta3    = repmat(df_deta3,[1,N_xtnd]);
G           = repmat(G,[1,N_xtnd]);
dG_deta     = repmat(dG_deta,[1,N_xtnd]);
dG_deta2    = repmat(dG_deta2,[1,N_xtnd]);
k           = repmat(k,[1,N_xtnd]);
dk_deta     = repmat(dk_deta,[1,N_xtnd]);
dk_deta2    = repmat(dk_deta2,[1,N_xtnd]);
for s=intel.mixCnst.N_spec:-1:1
    ys{s}       = repmat(ys{s},[1,N_xtnd]);
    dys_deta{s} = repmat(dys_deta{s},[1,N_xtnd]);
    dys_deta2{s}= repmat(dys_deta2{s},[1,N_xtnd]);
end
tau         = repmat(tau,[1,N_xtnd]);
dtau_deta   = repmat(dtau_deta,[1,N_xtnd]);
dtau_deta2  = repmat(dtau_deta2,[1,N_xtnd]);
j           = repmat(j,[1,N_xtnd]);
switch options.numberOfTemp
    case '1T'
        % no additional arrangements
    case '2T'
tauv         = repmat(tauv,[1,N_xtnd]);
dtauv_deta   = repmat(dtauv_deta,[1,N_xtnd]);
dtauv_deta2  = repmat(dtauv_deta2,[1,N_xtnd]);
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

% Transforming to static enthalpy
[h,dh_deta,dh_deta2] = getEnthalpy_semitotal2static(G.*H_e,dG_deta.*H_e,dG_deta2.*H_e,df_deta.*U_e,df_deta2.*U_e,df_deta3.*U_e);
g = h./h_e;
dg_deta = dh_deta./h_e;
dg_deta2 = dh_deta2./h_e;

% Build differentiation matrices for xi and eta
D1_eta  = intel.D1;
D2_eta  = intel.D2;
if marchYesNo
    D1_xi = intel.D1_xi;
    D2_xi = intel.D2_xi;
else
    D1_xi = zeros(length(x));
    D2_xi = zeros(length(x));
end

% Calculate rho and its derivatives with respect to eta and xi
rho         = rho_e ./ j;
drho_deta   = D1_eta * rho;                     %  [N_eta,N_eta] * [N_eta,N_x ]    -> [N_eta ,N_x]
if marchYesNo
    drho_dxi= (D1_xi * rho')';                  % [[N_x ,N_x ] * [N_x ,N_eta]]'  -> [N_eta ,N_x]
else
    drho_dxi= zeros(size(rho));
end

% Calculate parameters needed for deta_dx-problem
dxi_dx      =  rho_e .*U_e.*mu_e;
deta_dy     =  rho   .*U_e./sqrt(2*xi);
if U_e(1) == 0
    deta_dy(:,1) = rho(:,1)*Ue_sqrt2xi0;
end
dj_deta     = D1_eta*j;
drho_dy     = -rho_e ./j.^2.*dj_deta.*deta_dy;
deta_dy2    = drho_dy.*U_e./sqrt(2*xi);
if U_e(1) == 0
    deta_dy2(:,1) = drho_dy(:,1)*Ue_sqrt2xi0;
end

% Note that for every xi station, D1_y is a [N_eta, N_eta] sized differentiation operator
D1_y = zeros(N_eta,N_eta,N_x);
D2_y = zeros(N_eta,N_eta,N_x);
for i = 1:N_x_reached
    D1_y(:,:,i)   = diag(deta_dy (:,i))   *D1_eta;
    D2_y(:,:,i)   = diag(deta_dy (:,i).^2)*D2_eta ...
        + diag(deta_dy2(:,i))   *D1_eta;
end
dU_e_dxi    = beta.*U_e./(2*xi);
% Note, for stagnation dU_e_dxi approaches infinity as 1/x (x = 0 being
% stagnation)
dU_e_dx     =  dU_e_dxi.*dxi_dx;
if U_e(1) == 0
    dU_e_dx(:,1) = dUe0_dx;
end
M2          = U_e./sqrt(2*xi).*drho_deta;
M1          = (dU_e_dx./sqrt(2*xi) - U_e.*(2*xi).^(-3/2).*dxi_dx).*rho + U_e./sqrt(2*xi).*drho_dxi.*dxi_dx;
% Note for non-shearing stagnation lines, dT_dx = 0, implying drho_dx and then
% deta_dx = 0. To accomplish this, we supply a reasonable value to M2 to
% help the condition number of A, and set M1 to 0 to fix deta_dx = 0.
if U_e(1) == 0
    M2(:,1) = Ue_sqrt2xi0*drho_deta(:,1);
    M1(:,1) = 0;
end

% Set up ODE for deta_dx
deta_dx = zeros(N_eta,N_x);
for i = 1:N_x_reached
    A           = D1_y(:,:,i) - diag(M2(:,i));  % Initialize LHS Matrix for given xi station
    A(end,:)    = [zeros(1,N_eta-1) 1];         % Enforce boundary condition in LHS matrix
    M1(end,i)   = 0;                            % Enforce boundary condition in RHS vector
    deta_dx(:,i)= A\M1(:,i);                    % Solve ODE at xi station
end

% Calculate partial derivatives for the streamfunction and v-component
if marchYesNo
    df_dxi  = (D1_xi * f')'; % [[N_x ,N_x ] * [N_x ,N_eta]]' -> [N_eta ,N_x]
else
    df_dxi  = zeros(size(f));
end
dpsi_dxi    = (2*xi).^(-1/2).*f + sqrt(2*xi).*df_dxi;
dpsi_deta   = sqrt(2*xi).*df_deta;
dpsi_dx     = dpsi_dxi.*dxi_dx + dpsi_deta.*deta_dx;
if U_e(1) == 0
    % Taylor series of (dxi/dx)*(1/sqrt(2*xi))
    dxi_dx_sqrt2xi0 = sqrt(rho_e(1,1)*dUe0_dx*mu_e(1,1));
    dpsi_dx(:,1) = dxi_dx_sqrt2xi0.*f(:,1)...
        + sqrt(2*xi(1,1)).*df_dxi(:,1).*dxi_dx(:,1)...
        + dpsi_deta(:,1).*deta_dx(:,1);
end

% Calculate integration operators for xi and eta
if marchYesNo
    I1_xi   = integral_mat(D1_xi,'ud');     % xi = 0 in top row
    dx_dxi  = 1./dxi_dx;
    % For stagnation flows, dx_dxi is 1/0. We do not want this artifact to
    % propagate throughout the entire domain upon integration.
    % To nullify this, we set dx_dxi(1) to a value of 0 to eliminate its
    % influence upon integration.
    if U_e(1) == 0
        dx_dxi(:,1) = 0;
    end
end
I1_eta  = integral_mat(D1_eta,'du');        % eta = 0 in bottom row
dy_deta = 1./deta_dy;
% dpsi_dy = sqrt(2*xi).*deta_dy.*df_deta;

% Extract nondimensional thermophysical constants
% kappa_nondim= a1 ./ intel.j;
% mu_nondim   = C  .* intel.j;
% Pr          = C  ./ a1; % Commented out because the variable is currently unused.

% Redimensionalize
if marchYesNo
    if options.xDim0                     % if the dimensional x must start at zero
        x0 = 0;
    else                                 % otherwise we set it to the corresponding self-similar x at the initial station
        if options.bVaryingEdgeQSS
            x0 = getFalknerSkan_x(xi(1,1),beta(1),Theta(1),xi_ref,H_e0,U_e0,p_e0,XYs_e0,T_e0,Tv_e0,intel.mixCnst,intel,options,'xi_max',xi_max,'xi_min',xi_min);
        else
            x0 = xi(1) / (mu_e(1) * rho_e(1) * U_e(1));
        end
    end
    x = x0 + (I1_xi*dx_dxi')';    % Physical x coordinates             [m]
    switch options.inviscidFlowfieldTreatment
        case {'constant','1DNonEquilibriumEuler','custom'}                  % no additional change needed
        case 'airfoil'                                                      % Use the imported x (from intel) instead of the calculated x based on xi
            getImportedEdge_x(intel.x, x, options.ox, options.display);
            x = repmat(intel.x,[N_eta,1]);
        otherwise
            error(['The chosen inviscidFlowfieldTreatment ''',options.inviscidFlowfieldTreatment,''' is not supported']);
    end

else
    if options.bVaryingEdgeQSS
        x = getFalknerSkan_x(xi(1,:),beta(1),Theta(1),xi_ref,H_e0,U_e0,p_e0,XYs_e0,T_e0,Tv_e0,intel.mixCnst,intel,options,'xi_max',xi_max,'xi_min',xi_min,'check_EcVariation');
    else
        x = xi ./ (mu_e .* rho_e .* U_e);
    end
end
y      = I1_eta * dy_deta;         % Physical y coordinates                 [m]
u      = U_e.* df_deta;            % Horizontal velocity component          [m/s]
v      = -1./rho.*dpsi_dx;         % Vertical velocity component            [m/s]

% Note: when working on a conical reference system, all the above development
% would not refer to the transformed x and y variables (after the
% probstein-elliot transformation).
%
% To retrieve the actual physical values of x and y, one must undo also
% this transformation.
%
% The expression of v is also slightly different when undoing this
% transformation.
switch options.coordsys
    case 'cartesian2D'                                              % nothing to do
    case {'cone','cylindricalExternal','cylindricalInternal'}       % undoing Probstein-Elliot transformation
        % Converting from transformed x and y to physical coordinates
        [x,y,r,r0,dyPE_dx,dyPE_dy,dyPE_dy2,dxPE_dx] = get_xyInvTransformed(x,y,options.coordsys,intel.cylCoordFuncs); % actually inputting xPE and yPE

        % Transforming wall-normal velocity
        v = r0.^2./r.*v - dyPE_dx./r.*u; % Probstein-Elliot
        % Note that this expression does not tend to the non-conical one for
        % cone_angle -> 0, as one could expect. The reason is that a cone with
        % an angle of zero is not a flat plate, but rather an infinitely thin
        % rod. It is therefore understandable that the velocity normal to this
        % rod (v) tends to zero as cone_angle -> 0.

        % Converting eta & xi derivatives wrt the transformed y and x
        % (Probstein-Elliot) to derivatives wrt the dimensional y and x
        dxi_dx = dxi_dx.*dxPE_dx;
        deta_dx = deta_dx.*dxPE_dx + deta_dy.*dyPE_dx;
        deta_dy = deta_dy .* dyPE_dy;
        deta_dy2 = deta_dy2 .* dyPE_dy.^2 + deta_dy.*dyPE_dy2;
        dy_deta = dy_deta./dyPE_dy;
        for i_xi = 1:N_x_reached
            D1_y(:,:,i_xi) = diag(dyPE_dy(:,i_xi))*D1_y(:,:,i_xi);
            D2_y(:,:,i_xi) = diag(dyPE_dy2(:,i_xi))*D1_y(:,:,i_xi) + diag(dyPE_dy(:,i_xi).^2)*D2_y(:,:,i_xi);
        end
    otherwise                                                       % break
        error(['Error: the chosen coordsys ''',options.coordsys,''' is not supported']);
end

% --- x, y, and state variables --- %
intel.x      = x;                                                           % streamwise spatial coordinate             [m]
intel.y      = y;                                                           % wall-normal spatial coordinate            [m]
intel.u      = u;                                                           % Horizontal velocity component             [m/s]
intel.v      = v;                                                           % Vertical velocity component               [m/s]
intel.w      = W_0 *  k;                                                    % Spanwise velocity component, W_0 constant [m/s]
intel.rho    = rho;                                                         % Mass density of fluid mixture             [kg/m^3]
intel.p      = p_e;                                                         % Static pressure, fundamental assumption   [Pa]
intel.h      = h_e.*  g;                                                    % Static enthalpy                           [m^2/s^2]
intel.htot   = intel.h + (intel.u.^2 + intel.v.^2 + intel.w.^2)/2;          % Total enthalpy                            [m^2/s^2]
% intel.mu     = mu_nondim.*mu_e;                                             % Viscosity of fluid mixture                [m^2/s]
% intel.kappa  = kappa_nondim.*kappa_e;                                       % Thermal conductivity of fluid mixture     [W/(m*K)]

% --- evaluate spatial derivatives of state quantities --- %
intel.du_dy  = U_e.* df_deta2.*deta_dy;
intel.du_dy2 = U_e.*(df_deta3.*deta_dy.^2 + df_deta2.*deta_dy2);
intel.dv_dy  = zeros(N_eta,N_x);
intel.dv_dy2 = zeros(N_eta,N_x);
for i_xi = 1:N_x_reached
    intel.dv_dy(:,i_xi)  = D1_y(:,:,i_xi) * intel.v(:,i_xi);
    intel.dv_dy2(:,i_xi) = D2_y(:,:,i_xi) * intel.v(:,i_xi);
end
intel.dw_dy  = W_0.* dk_deta .*deta_dy;
intel.dw_dy2 = W_0.*(dk_deta2.*deta_dy.^2 + df_deta .*deta_dy2);
intel.dh_dy  = h_e.* dg_deta .*deta_dy;
intel.dh_dy2 = h_e.*(dg_deta2.*deta_dy.^2 + dg_deta .*deta_dy2);
intel.dT_dy  = T_e.* dtau_deta.*deta_dy;
intel.dT_dy2 = T_e.*(dtau_deta2.*deta_dy.^2 + dtau_deta .*deta_dy2);
intel.dhtot_dy  = intel.dh_dy + intel.u.*intel.du_dy + intel.v.*intel.dv_dy + intel.w.*intel.dw_dy;
intel.dhtot_dy2 = intel.dh_dy2 + intel.du_dy.^2 + intel.dv_dy.^2 + intel.dw_dy.^2 + ...
                                 intel.u.*intel.du_dy2 + intel.v.*intel.dv_dy2 + intel.w.*intel.dw_dy2;

if ~marchYesNo
    intel.ys = cellfun(@(cll)repmat(cll,[1,N_x]),intel.ys,'UniformOutput',false);
elseif intel.mixCnst.N_spec==1
    intel.ys{1} = ones(N_eta,N_x);
end
if ~marchYesNo
    intel.yE = cellfun(@(cll)repmat(cll,[1,N_x]),intel.yE,'UniformOutput',false);
elseif intel.mixCnst.N_elem==1
    intel.yE{1} = ones(N_eta,N_x);
end
switch options.numberOfTemp
    case '1T'
% [T,~,~,~,~,~,y_s] = getMixture_T(intel.h(:),y_s,intel.p(:),mixCnst,options,'Tguess',reshape(tau .* T_e , [N_eta*N_x,1]));
    case '2T'
intel.Tv      = Tv_e.* tauv;
intel.dTv_dy  = Tv_e.* dtauv_deta.*deta_dy;
intel.dTv_dy2 = Tv_e.*(dtauv_deta2.*deta_dy.^2 + dtauv_deta .*deta_dy2);
% T = getMixture_T(intel.h(:),y_s,intel.p(:),mixCnst,options,intel.Tv(:));
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end
% intel.T      = reshape(T,[N_eta,N_x]);
intel.T      = T_e.*tau;
intel.dT_dy  = deta_dy .*(D1_eta*intel.T);
intel.dT_dy2 = deta_dy2.*(D1_eta*intel.T)   + deta_dy.^2.*(D2_eta*intel.T);

% --- rotate velocity vector and derivatives if desired --- %
switch options.rotateVelocityVector
    case 'streamline'
    % Neglect other cases, as they haven't been thoroughly tested: 2019-07-29
    %{
    case 'cst_sweep'
        error('cst_sweep option is currently unsupported!');
    case 'rhow_IP'
        error('rho*w inflection point option is currently unsupported!');
    case 'w_IP'
        error('w inflection point option is currently unsupported!');
    %}
    [intel. u_s,    intel. w_s,intel.phi_s] = eval_rotationVelVec(intel.u,intel.w,options,intel.Lambda_sweep);
    [intel.du_s_dy, intel.dw_s_dy] = eval_rotationVelVec(intel.du_dy,intel.dw_dy,options,intel.Lambda_sweep);
    [intel.du_s_dy2,intel.dw_s_dy2] = eval_rotationVelVec(intel.du_dy2,intel.dw_dy2,options,intel.Lambda_sweep);
end

% species quantities...
for s=1:N_spec
    % Define species partial density
    intel.rhos{s}       = ys{s}.*rho;
    % Compute y first and second derivatives
    intel.drhos_dy{s}   = deta_dy.*(D1_eta*intel.rhos{s});
    intel.dys_dy{s}     = deta_dy.*(D1_eta*ys{s});

    intel.dys_dy2{s}    = deta_dy2.*(D1_eta*ys{s})   + (deta_dy).^2.*(D2_eta*ys{s});
    intel.drhos_dy2{s}  = deta_dy2.*(D1_eta*intel.rhos{s}) + (deta_dy).^2.*(D2_eta*intel.rhos{s});
end

intel.D1_y = D1_y;
intel.D2_y = D2_y;

% Compute x first and second derivatives, as well as mixed partials
if length(x)>1
    % First derivative in x
    intel.du_dx         = D1_x_func(intel.u     ,D1_eta,D1_xi,deta_dx,dxi_dx);
    intel.dv_dx         = D1_x_func(intel.v     ,D1_eta,D1_xi,deta_dx,dxi_dx);
    intel.dw_dx         = D1_x_func(intel.w     ,D1_eta,D1_xi,deta_dx,dxi_dx);
    intel.drho_dx       = D1_x_func(intel.rho   ,D1_eta,D1_xi,deta_dx,dxi_dx);
    intel.dp_dx         = D1_x_func(intel.p     ,D1_eta,D1_xi,deta_dx,dxi_dx);
%     intel.dmu_dx        = D1_x_func(intel.mu    ,D1_eta,D1_xi,deta_dx,dxi_dx);
%     intel.dkappa_dx     = D1_x_func(intel.kappa ,D1_eta,D1_xi,deta_dx,dxi_dx);
    intel.dh_dx         = D1_x_func(intel.h     ,D1_eta,D1_xi,deta_dx,dxi_dx);
    intel.dhtot_dx      = D1_x_func(intel.htot  ,D1_eta,D1_xi,deta_dx,dxi_dx);
    intel.dT_dx         = D1_x_func(intel.T     ,D1_eta,D1_xi,deta_dx,dxi_dx);

    % Mixed partial in x and y
    intel.du_dxdy       = D2_xy_func(intel.u    ,D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy);
    intel.dv_dxdy       = D2_xy_func(intel.v    ,D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy);
    intel.dw_dxdy       = D2_xy_func(intel.w    ,D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy);
    intel.drho_dxdy     = D2_xy_func(intel.rho  ,D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy);
    intel.dp_dxdy       = D2_xy_func(intel.p    ,D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy);
%     intel.dmu_dxdy      = D2_xy_func(intel.mu   ,D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy);
%     intel.dkappa_dxdy   = D2_xy_func(intel.kappa,D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy);
    intel.dh_dxdy       = D2_xy_func(intel.h    ,D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy);
    intel.dhtot_dxdy    = D2_xy_func(intel.htot ,D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy);
    intel.dT_dxdy       = D2_xy_func(intel.T    ,D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy);

    % Second derivative in x
    intel.du_dx2        = D2_x_func(intel.u     ,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx);
    intel.dv_dx2        = D2_x_func(intel.v     ,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx);
    intel.dw_dx2        = D2_x_func(intel.w     ,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx);
    intel.drho_dx2      = D2_x_func(intel.rho   ,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx);
    intel.dp_dx2        = D2_x_func(intel.p     ,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx);
%     intel.dmu_dx2       = D2_x_func(intel.mu    ,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx);
%     intel.dkappa_dx2    = D2_x_func(intel.kappa ,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx);
    intel.dh_dx2        = D2_x_func(intel.h     ,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx);
    intel.dhtot_dx2     = D2_x_func(intel.htot  ,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx);
    intel.dT_dx2        = D2_x_func(intel.T     ,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx);

    % Considering chemical effects...
    for s=1:N_spec
        % First order in x
        intel.drhos_dx{s}   =   D1_x_func(intel.rhos{s} ,D1_eta,D1_xi,deta_dx,dxi_dx);
        intel.dys_dx{s}     =   D1_x_func(ys{s}   ,D1_eta,D1_xi,deta_dx,dxi_dx);

        % Mixed partials
        intel.drhos_dxdy{s} =   D2_xy_func(intel.rhos{s},D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy);
        intel.dys_dxdy{s}   =   D2_xy_func(ys{s}  ,D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy);

        % Second order in x
        intel.drhos_dx2{s}  =   D2_x_func(intel.rhos{s} ,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx);
        intel.dys_dx2{s}    =   D2_x_func(ys{s}   ,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx);
    end

    switch options.numberOfTemp
        case '1T'
            % no additional arrangements
        case '2T'
            intel.dTv_dx         = D1_x_func(intel.Tv     ,D1_eta,D1_xi,deta_dx,dxi_dx);
            intel.dTv_dxdy       = D2_xy_func(intel.Tv    ,D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy);
            intel.dTv_dx2        = D2_x_func(intel.Tv     ,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx);
        otherwise
            error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
    end
end

% Additional output needed for mapping variables
intel.drho_deta = drho_deta;

% Outputting properties needed for streamwise differentiation
intel.D1_xi     = D1_xi;
intel.D2_xi     = D2_xi;
intel.deta_dx   = deta_dx;
intel.deta_dy   = deta_dy;
intel.dxi_dx    = dxi_dx;

% Find y_i value corresponding to eta_i
intel.y_i = interp1(intel.eta,intel.y,options.eta_i,'spline');
lBlasius = sqrt(mu_e.*x./(rho_e.*U_e));
if U_e(1) == 0
    lBlasius(:,1) = sqrt(mu_e(1,1)/(rho_e(1,1)*dUe0_dx));
end
intel.lBlasius = lBlasius(1,:);

%% Wall quantities of interest
if options.shockJump                                                % Reference quantities when there is a shock jump (preshock)
    rhoRefSt = intel.rho_inf;
    URefSt   = intel.U_inf;
    HRefSt   = (intel.h_inf + 0.5*intel.U_inf^2);
else                                                                % Reference quantities without a shock jump (first edge)
    rhoRefSt = rho_e(1,1);
    URefSt   = U_e(1,1);
    HRefSt   = H_e(1,1);
end
switch options.numberOfTemp
    case '1T'
        intel.St = getWall_Stanton(intel.dT_dy,intel.T,intel.ys,p_e,intel.mixCnst,options,rhoRefSt,URefSt,HRefSt);
    case '2T'
        intel.St = getWall_Stanton(intel.dT_dy,intel.T,intel.ys,p_e,intel.mixCnst,options,rhoRefSt,URefSt,HRefSt,options.numberOfTemp,intel.dT_dy,intel.Tv);
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end


%% Boundary-layer properties
% Local properties
if options.BLproperties
    dispif('--- Computing various boundary-layer parameters ...', options.display);

    % Unload certain options fields to limit parfor broadcasting requirements
    eta_i = options.eta_i;
    L = options.L;
    N = options.N;
    display = options.display;

    % Initialize intel_GICM as a variable (not a function) for the parfor's parser

    % Initialize 99% BL property outputs
    delta99   = zeros(1,N_x_reached);
    eta99     = zeros(1,N_x_reached);
    deltaH999 = zeros(1,N_x_reached);
    etaH999   = zeros(1,N_x_reached);

    % Run GICM to extract machine accurate values of delta99 and eta99
     if options.N_thread > 1
        check_parpool_init(options.N_thread);
        parfor ii = 1:N_x_reached
            dispif(['--- xi-station #',num2str(ii),'/',num2str(N_x_reached),' on GICM for delta_99'],...
                display && ~mod(ii,10));
            [delta99(ii), eta99(ii), deltaH999(ii), etaH999(ii)] = getBL_delta( df_deta(:,ii), df_deta2(:,ii), y(:,ii), intel.eta,options.bGICM4delta99, G(:,ii),...
                                        Ue_sqrt2xi0, rho(:,ii), eta_i, L, N, U_e(1,ii), xi(1,ii), options,intel.cylCoordFuncs,xPhys(ii));
        end
    else
        for ii = 1:N_x_reached
            dispif(['--- xi-station #',num2str(ii),'/',num2str(N_x_reached),' on GICM for delta_99'],...
                display && ~mod(ii,10));
            [delta99(ii), eta99(ii), deltaH999(ii), etaH999(ii)] = getBL_delta( df_deta(:,ii), df_deta2(:,ii), y(:,ii), intel.eta,options.bGICM4delta99, G(:,ii),...
                                        Ue_sqrt2xi0, rho(:,ii), eta_i, L, N, U_e(1,ii), xi(1,ii), options,intel.cylCoordFuncs,xPhys(ii));
        end
    end

    % Load delta's into intel, respecting the parfor requirements
    intel.delta99    = delta99;
    intel.eta99      = eta99;
    intel.deltaH999  = deltaH999;
    intel.etaH999    = etaH999;

    % (global) Blasius thickness (1/Re_unit), per definition: mu_e/rho_e/U_e
    intel.glBlasius = intel.mu_e./intel.rho_e./intel.U_e;
    % assuming constant edge conditions:
    %                xi = rho_e*U_e*mu_e*x = (rho_e*U_e*l)^2
    %                   = (mu_e*Re_l)^2 = mu_e^2*Re_x
    % (local) Blasius thickness, let x be the local streamwise station's coordinate
    intel.llBlasius = sqrt(intel.mu_e.*intel.x./intel.rho_e./intel.U_e);
    % per definition: l = sqrt(mu_e*x/rho_e/U_e)
    %              Re_x = rho_e*U_e*x/mu_e
    %              Re_l = rho_e*U_e*l/mu_e = rho_e*U_e/mu_e*sqrt(mu_e*x/rho_e/U_e)
    %                   = sqrt(rho_e*U_e*x/mu_e) = sqrt(Re_x)
    %                 x = rho_e*U_e*l^2/mu_e = l*Re_l
    %                 l = x/Re_l = x/sqrt(Re_x)
    % Reynolds number based on streamwise coordinate
    intel.Re_x = intel.xi/(intel.mu_e.^2);
    % Reynolds number based on local Blasius length
    intel.Re_l = sqrt(intel.Re_x);

    % Integral properties
    % displacement thickness
    deltastarv = I1_eta([1 2],:)*(dy_deta.*(1-intel.df_deta./intel.j));
    % note: this operation determines the full integral; extract integral value:
    intel.deltastar = deltastarv(1,:);
    % extract truncation error:
    intel.deltastar_te = deltastarv(1,:)-deltastarv(2,:);

    % momentum thickness
    thetastarv = I1_eta([1 2],:)*(dy_deta.*(intel.df_deta./intel.j.*(1 - intel.df_deta)));
    intel.thetastar = thetastarv(1,:);
    intel.thetastar_te = thetastarv(1,:)-thetastarv(2,:);
    % energy thickness
    deltaestarv = I1_eta([1 2],:)*(dy_deta.*(intel.df_deta./intel.j.*(1 - intel.df_deta.^2)));
    intel.deltaestar = deltaestarv(1,:);
    intel.deltaestar_te = deltaestarv(1,:)-deltaestarv(2,:);
    % static enthalpy thickness
    gw = repmat(g(end,:),[options.N,1]);
    gw(gw == 1) = nan; % preparing for division by 0 in the integrand
    deltahstarv = I1_eta([1 2],:)*(dy_deta.*(intel.df_deta./intel.j.*((g - 1)./(gw - 1))));
    intel.deltahstar = deltahstarv(1,:);
    intel.deltahstar_te = deltahstarv(1,:)-deltahstarv(2,:);
    % shape factor
    shapefactorv = deltastarv./thetastarv;
    intel.shapefactor = shapefactorv(1,:);
    intel.shapefactor_te = shapefactorv(1,:)-shapefactorv(2,:);

    dispif('--- Boundary-layer parameters calculated', options.display);
end

% alphabetize fields
intel = orderfields(intel);

end % eval_dimVars.m
