function [U_e2,TTrans_e2,TRot_e2,TVib_e2,TElec_e2,Tel_e2,p_e2,y_se2,beta] = eval_shockJump(U_e1,TTrans_e1,TRot_e1,TVib_e1,TElec_e1,Tel_e1,p_e1,y_se1,gam_e1,R_e1,wedgeCone_angle,mixCnst,options)
% eval_shockJump evaluates the shock-jump relations for a variety of
% surface geometries and flow assumptions.
%
% Usage:
%   (1)   
%                [U_e2,TTrans_e2,TRot_e2,TVib_e2,TElec_e2,Tel_e2,p_e2,y_se2,beta] = ...
%  eval_shockJump(U_e1,TTrans_e1,TRot_e1,TVib_e1,TElec_e1,Tel_e1,p_e1,y_se1,...
%                               gam_e1,R_e1,wedgeCone_angle,mixCnst,options)
%
%   (2)   --> options.noseFlow = 'LTEED' or 'LTE'
%                [U_e2,TTrans_e2,TRot_e2,TVib_e2,TElec_e2,Tel_e2,p_e2,y_se2,beta] = ...
%  eval_shockJump(U_e1,TTrans_e1,TRot_e1,TVib_e1,TElec_e1,Tel_e1,p_e1,X_E1,...
%                               gam_e1,R_e1,wedgeCone_angle,mixCnst,options)
%
% Inputs and outputs:
%   U_e1            preshock velocity [m/s]
%   TTrans_e1       preshock translational temperature [K]
%   TRot_e1         preshock rotational temperature [K]
%   TVib_e1         preshock vibrational temperature [K]
%   TElec_e1        preshock electronic temperature [K]
%   Tel_e1          preshock electron temperature [K]
%   p_e1            preshock pressure [Pa]
%   y_se1           preshock species mass fractions [-]
%   X_Ee1           preshock elemental mole fractions [-]
%   gam_e1          preshock heat-capacity ratio [-]
%   R_e1            preshock mixture gas constant [J/kg-K]
%   wedgeCone_angle wedge or cone half-angle [deg]
%   mixCnst         structure containing the mixture constants
%   options         structure containing the system options. Important ones
%                   for the shock jump are:
%       .coordsys
%           string fixing the coordinate system. Supported values:
%               'cartesian2D'
%               'cone'
%       .noseFlow
%           string fixing the flow at the nose assumption. Supported values:
%               'CPG'           'TPG'           'TPGD'
%               'CNE'           'LTE'           'LTEED'
%       .numberOfTemp
%           string fixing the number of temperatures. Supported values:
%               '1T'            '2T'
%   U_e2            postshock velocity [m/s]
%   TTrans_e2       postshock translational temperature [K]
%   TRot_e2         postshock rotational temperature [K]
%   TVib_e2         postshock vibrational temperature [K]
%   TElec_e2        postshock electronic temperature [K]
%   Tel_e2          postshock electron temperature [K]
%   p_e2            postshock pressure [Pa]
%   y_se2           postshock species mass fractions [-]
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

options.EoS = protectedvalue(options,'EoS','idealGas');

M_e1 = U_e1./sqrt(gam_e1.*R_e1.*TTrans_e1); % computing preshock mach

if any(M_e1<=1)
    error(['The provided preshock Mach number (',num2str(min(M_e1(:))),') is smaller than one - there will be no shock!']);
end

%%%%%%%%%%%%%%%% SHOCK JUMP %%%%%%%%%%%%%%%%%
%
% Given an upstream mach and compression corner angle, get the frozen
% oblique shock wave angle (beta)
switch options.coordsys
    case 'cartesian2D'
        switch options.noseFlow
            case 'CPG'                                              % calorically perfect shock
                [TTrans_e2,p_e2,U_e2,~,beta] = getShock_Props(TTrans_e1,p_e1,M_e1,R_e1,wedgeCone_angle,gam_e1);
                y_se2 = y_se1;
            case {'TPG','TPGD','CNE'}
                switch options.numberOfTemp
                    case '1T'                                       % chemically frozen, but in vibrational equilibrium
                        [TTrans_e2,p_e2,U_e2,beta] = getTPG1TShock_Props(U_e1,TTrans_e1,p_e1,y_se1,mixCnst,wedgeCone_angle,options);
                    case '2T'                                       % chemically and vibrationally frozen
                        [TTrans_e2,p_e2,U_e2,~,beta] = getShock_Props(TTrans_e1,p_e1,M_e1,R_e1,wedgeCone_angle,gam_e1);
                    otherwise
                        error(['the chosen thermal model ''',options.numberOfTemp,''' is not supported']);
                end
                y_se2 = y_se1;
            case {'LTE','LTEED'}                                    % equilibrium shock
                [TTrans_e2,p_e2,U_e2,y_se2,beta] = getLTEShock_Props(U_e1,TTrans_e1,p_e1,y_se1,mixCnst,wedgeCone_angle,options); % y_se1 is actually X_Ee1 (see usage 2)
            otherwise
                error(['the chosen flow at the nose assumption ''',options.noseFlow,''' is not supported']);
        end
    case 'cone'
        [M_e2, beta, Tratio, pratio, ~, converged]=TaylorMaccoll(wedgeCone_angle, M_e1, gam_e1, true, wedgeCone_angle*1.5); % HARDCODE ALERT! initial guess for the shock angle - 1.5 times the cone_angle
        % We compute the CPG shock jump for all assumptions (since it is rocket fast)
        % and then use it as an initial guess for the others if necessary
        if options.CPGconeShock                                             % we simply take the CPG shock-jump
            TTrans_e2 = TTrans_e1*Tratio;
            p_e2 = p_e1*pratio;
            U_e2 = M_e2.*sqrt(gam_e1.*R_e1.*TTrans_e2);
            y_se2 = y_se1;
        else                                                                % we use it as an initial guess for the actual shock
            switch options.noseFlow
                case 'CPG'
                    TTrans_e2 = TTrans_e1*Tratio;
                    p_e2 = p_e1*pratio;
                    U_e2 = M_e2.*sqrt(gam_e1.*R_e1.*TTrans_e2);
                    y_se2 = y_se1;
                case {'TPG','TPGD','CNE'}
                    switch options.numberOfTemp
                        case '1T'                                       % chemically frozen, but in vibrational equilibrium
                            [TTrans_e2,p_e2,U_e2,y_se2,beta,converged] = getHighEnth_ConicalShock_Props('TPG1T',U_e1,TTrans_e1,p_e1,y_se1,mixCnst,...
                                                                                                    wedgeCone_angle,options,beta);                % we use the CPG shock angle as a guess
                        case '2T'                                       % chemically and vibrationally frozen
                            % same shock jump as in CPG!! (we already have it)
                            TTrans_e2 = TTrans_e1*Tratio;
                            p_e2 = p_e1*pratio;
                            U_e2 = M_e2.*sqrt(gam_e1.*R_e1.*TTrans_e2);
                            y_se2 = y_se1;
                        otherwise
                            error(['the chosen thermal model ''',options.numberOfTemp,''' is not supported']);
                    end
                    % Since flow is frozen across a shock wave, mass fractions are equal
                    % before and after the jump.
                case {'LTE','LTEED'}                                   % thermo-chemical equilibrium shock
                    [TTrans_e2,p_e2,U_e2,y_se2,beta,converged] = getHighEnth_ConicalShock_Props('LTE',U_e1,TTrans_e1,p_e1,y_se1,mixCnst,...     % y_se1 is actually X_Ee1 (see usage 2)
                                                                                                    wedgeCone_angle,options,beta);              % we use the CPG shock angle as a guess
                otherwise
                    error(['the chosen flow assumption at the nose ''',options.noseFlow,''' is not supported']);
            end
        end
        if converged
            dispif('Taylor-Maccoll converged',options.display);
        else
            warning('Taylor-Maccoll did not converge.');
        end
    otherwise
        error(['the chosen coordsys ''',options.coordsys,''' is not supported for shock-jump relations']);
end
%
%%%%%%%%%% ... STUCK THE LANDING! %%%%%%%%%%%
%
% Note that across a shock, only translational and rotational temperatures
% discontinuously change. The other internal temperatures (vibrational,
% electronic, electron) are frozen through the shock wave and are equal for
% pre- and post-shock conditions. The authors will assume this.
TRot_e2     = TTrans_e2;
switch options.numberOfTemp
    case '1T' % for 1T we assume the internal modes to be in thermal equilibrium with the translational
        TVib_e2     = TTrans_e2;
        TElec_e2    = TTrans_e2;
        Tel_e2      = TTrans_e2;
    case '2T' % for 2T we assume the internal modes to be frozen through the shock
        TVib_e2     = TVib_e1;
        TElec_e2    = TElec_e1;
        Tel_e2      = Tel_e1;
end
