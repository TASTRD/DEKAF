function [tauv,conv] = solve_ODECheby_vibEnergy(tauv_w,tauv_e,eta,y_s,intel,options,tol)
% solve_ODECheby_vibEnergy uses Chebyshev differentiation matrices together
% with a NR method to solve the vibrational-energy ODE.
%
% Usage:
%   (1)
%       tauv = solve_ODECheby_vibEnergy(tauv_w,tauv_e,eta,y_s,intel,options,tol)
%
%   (2)
%       [tauv,conv] = solve_ODECheby_vibEnergy(...)
%       |--> returns also the convergence that was reached
%
% Inputs and outputs:
%   tauv_w, tauv_e
%       values of tauv in the boundaries (wall and edge) (size 1 x 1)
%   eta
%       wall-normal self-similar coordinate (size N_eta x 1)
%   y_s
%       species mass fractions (size N_eta x N_spec)
%   intel
%       classic DEKAF intel structure
%   options
%       classic DEKAF options structure
%   tol
%       tolerance with which the NR method will be considered converged
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% building initial guess for the NR method
a = 2; % HARDCODE ALERT!!
% tauv = tauv_e + (tauv_w-tauv_e)*exp(-a*sqrt(eta)); % exponential expression
etaCut = 4;
gEta = 0.5*(1-tanh(a*(eta-etaCut))); % unit function to join two discontinuous ones
tauv = (1-gEta)*tauv_e + gEta.*(tauv_w + (tauv_e-tauv_w)/etaCut.*eta);
% extracting all others
f = intel.f;
df_deta = intel.df_deta;
g = intel.g;
p = intel.p_e*ones(size(eta));
mixCnst = intel.mixCnst;
D1 = intel.D1;
D2 = intel.D2;

% defining limitting temperature
Tvmin = 0.5*min(tauv*intel.Tv_e);

% passing to static enthalpy
h = g*intel.H_e - 0.5*(intel.U_e*df_deta).^2;
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
             bElectron_mat,hForm_s_mat,~] = reshape_inputs4enthalpy(mixCnst.R_s,...
             mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,tauv);

% starting NR
conv = 1; it=1; % initializing
while conv>tol && it<options.T_itMax
    % ----> 1st) compute properties
    Tv = tauv*intel.Tv_e;
    Tv(Tv<Tvmin) = Tvmin; % enforcing the lower limit on Tv
    Tv_mat = repmat(Tv,[1,mixCnst.N_spec]);
    [hv,cpv] = getMixture_hv_cpv(y_s,Tv_mat,Tv_mat,Tv_mat,R_s_mat,...
        thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,options,mixCnst.AThermFuncs_s,nAtoms_s_mat,hForm_s_mat);                         % vibrational enthalpy and heat capacity
    cptr = getMixture_cptr(y_s,R_s_mat,nAtoms_s_mat,bElectron_mat,options);
    hForm = (y_s.*hForm_s_mat)*ones(mixCnst.N_spec,1);                              % mixture formation enthalpy
    Cpv = cpv * intel.Tv_e/intel.h_e;
    htr = h-hv-hForm;                                                               % mixture sensible trans-rot enthalpy
    T   = htr./cptr;                                                                % trans-rot temperature
    cp  = cptr+cpv;                                                                 % total heat capacity
    rho = getMixture_rho(y_s,p,T,Tv,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options); % mixture density
    Mm  = getMixture_Mm_R(y_s,T,Tv,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron);      % mixture molar mass
    [~,~,kappav] = getTransport_mu_kappa(y_s,T,Tv,rho,p,Mm,options,mixCnst,T,Tv,Tv,cp,'2T');                        % vibrational thermal conductivity
    av1 = intel.Tv_e/(intel.h_e*intel.rho_e*intel.mu_e) * kappav.*rho;              % computing av1
    dtauv_deta  = D1*tauv;                                                          % computing spatial derivatives
    dtauv_deta2 = D2*tauv;
    dav1_deta   = D1*av1;
    % ---> 2nd) build forcing vector (the equation itself but negative)
    forcing = -(Cpv.*f.*dtauv_deta - Cpv.*intel.theta.*df_deta.*tauv + dav1_deta.*dtauv_deta + av1.*dtauv_deta2);
    % ---> 3rd) build matrix (whatever multiplies tauv)
    mat = diag(av1)*D2 + diag(Cpv.*f + dav1_deta)*D1 + diag(- Cpv.*intel.theta.*df_deta);
    % ---> 4th) Enforcing boundarry conditions
    mat(1,:) = 0;       mat(end,:) = 0;                                             % clearing first and last row
    mat(1,1) = 1;       mat(end,end) = 1;                                           % we want to fix the value of tauv at the beginning and the end
    forcing([1,end]) = 0;                                                           % since the initial guess already had the right BCs, the forcing will be 0
    % ---> 5th) obtain update and convergence criterion
    tauvh = mat \ forcing;
    tauv = tauv + 0.5*tauvh;
    conv = norm(tauvh,inf);
    it=it+1;
    disp(['--- Cheby-NR for tauv, it #',num2str(it-1),' conv=',num2str(conv,4)]);
end

end % solve_ODECheby_vibEnergy
