function tauv = eval_ODEtauv(tauv_e,tauv_w,intel,options,y_s)
% eval_ODEtauv obtains tauv by solving the frozen non-reacting ODE
% resulting from the vib-elec-el energy equation.
%
% This solver is intended for isothermal walls. For adiabatic walls one
% should just fix tauv = 1 everywhere.
%
% Usage:
%   (1)
%       tauv = eval_ODEtauv(tauv_e,tauv_w,intel,options,y_s)
%
% Inputs and outputs:
%   tauv_e
%       non-dimensional vibrational temperature at the boundary-layer edge
%   tauv_w
%       non-dimensional vibrational temperature at the wall
%   intel
%       DEKAF's classic intel structure
%   options
%       DEKAF's classic options structure, including (among others):
%           .ODEtauvMethod
%               string identifying the method to be used in the resolution
%               of the ODE of the vibrational energy equation. Supported
%               values:
%                   'ode45'         Matlab's ode45 method
%                   'RK4'           Runge-Kutta fourth order
%                   'Euler'         Euler first order
%                   'ChebyNR'       Chebyshev's differentiation matrices
%                                   together with a newton-raphson
%                                   iteration (similar to DEKAF's core
%                                   solver). This one has been seen to work
%                                   best.
%   y_s
%       mass fraction profiles (size N_eta x N_spec)
%
% See also: func_ODEtauv
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% initial checks
if ~ismember(options.thermal_BC,{'Twall','ablation'})
    error(['the chosen thermal BC ''',options.thermal_BC,''' is not supported in this function']);
end
if strcmp(options.flow,'TPGD')
    warning('The chosen flow assumption ''TPGD'' allows for self-similar diffusion. However, eval_ODEtauv neglects vibrational diffusion fluxes. Major errors may arise');
end

% setting eta range
Neta = 1000;                                                             % number of points for ode45
eta_spanBig = 0:options.eta_i/(2*Neta):options.eta_i;                   % range of etas to solve
eta_span    = eta_spanBig(1:2:end);

% interpolating fields, so that they can be easily accessible in the ODE
% solver
interpMeth = 'spline';
flds.H       = interp1(intel.eta,intel.g,        eta_spanBig,interpMeth)*intel.H_e;
flds.df_deta = interp1(intel.eta,intel.df_deta,  eta_spanBig,interpMeth);
flds.f       = interp1(intel.eta,intel.f,        eta_spanBig,interpMeth);
flds.y_s     = interp1(intel.eta,y_s,            eta_spanBig,interpMeth);
flds.eta     = eta_spanBig;

% Preparing inputs for ODE
[~,~,~,av1_w] = props4ODEtauv(0,tauv_w,intel,options,flds);           % thermal conductivity variable at the wall
% [~,~,~,av1_e] = props4ODEtauv(options.N,tauv_e,intel,options,flds);   % thermal conductivity variable at the freestream
A0 = av1_w*(tauv_e - tauv_w)/2.5;
% This slope would make the function reach tauv_e at eta=2.5 if it were
% straight until there
delta_A0 = 0.01;                                                        % step for the computation of the numerical derivative of the objective function
% tauv_max = max([tauv_w,tauv_e]);
% tauv_min = 0.1*min([tauv_w,tauv_e]);

% Solving ODE by shooting
it=1;                                                                   % initializing loop variables
conv = 1;
tol = options.T_tol*1e6;                                                % we don't need the results to be ultra converged
% HARDCODE ALERT!!
switch options.ODEtauvMethod
    case {'ode45','RK4','Euler'} % these methods require shooting of the ODE
        disp('--- Obtaining initial guess for tauv by solving vib-elec-el ODE through shooting');
        while conv>tol && it<options.T_itMax
            % we will shoot an initial A0 value, and then update it with a
            % Newton-Raphson iteration, where the objective function is to make the
            % tauv in the edge Y_out0(end,1) reach it's expected value tauv_e.
            % The derivative of this objective function with the A0 is obtained by
            % increasing A0 in delta_A0 (reaching A1), evaluating in A1 and then
            % making a numerical derivative between the two obtained tauv(end)
            % values.
            t0 = [tauv_w,A0];                                                        % initial value of the variables
            t1 = [tauv_w,A0+delta_A0];                                               % increasing initial guess to compute derivative of the NR objective function
            switch options.ODEtauvMethod
                case 'ode45'
                    [eta_out0,Y_out0] = ode45(@(eta,Y)func_ODEtauv(eta,Y,intel,options,flds),eta_span,t0);
                    [~,       Y_out1] = ode45(@(eta,Y)func_ODEtauv(eta,Y,intel,options,flds),eta_span,t1);
                case 'RK4'
                    Y_out0 = solve_ODERK4(@(eta,Y)func_ODEtauv(eta,Y,intel,options,flds),eta_span,t0);
                    Y_out1 = solve_ODERK4(@(eta,Y)func_ODEtauv(eta,Y,intel,options,flds),eta_span,t1);
                    eta_out0 = eta_span;
                case 'Euler'
                    Y_out0 = solve_ODEEuler(@(eta,Y)func_ODEtauv(eta,Y,intel,options,flds),eta_span,t0);
                    Y_out1 = solve_ODEEuler(@(eta,Y)func_ODEtauv(eta,Y,intel,options,flds),eta_span,t1);
                    eta_out0 = eta_span;
                otherwise
                    error(['the chosen ODE method for determining the initial guess for tauv ''',options.ODEtauvMethod,''' is not supported']);
            end
            % NOTE: the solution of the ODE is [tauv,av1*dtauv_deta]
            obj = Y_out0(end,1) - tauv_e;                                           % objective function to make 0
            dobj_dA0 = (Y_out1(end,1) - Y_out0(end,1))/delta_A0;                    % numerical derivative of the objective function
            dA = - obj/dobj_dA0;
            A0_new = A0 + dA;                                                       % updating shooting based on NR objective function and its derivative
            conv = abs(1-A0_new/A0);                                                % convergence criterion
            A0 = A0_new;                                                            % preparing for next loop run
            disp(['--- Shooting for tauv, it #',num2str(it),' conv=',num2str(conv,4),' with A0=',num2str(A0)]);
            it=it+1;
        end

        % preparing output
        eta_out0(end) = max(intel.eta);                     % making sure that the interpolation is not an extrapolation
        tauv_out = Y_out0(:,1);
        tauv = interp1(eta_out0,tauv_out,intel.eta,'spline');
    case 'ChebyNR' % the chebyshev-Newton-Raphson method
        disp('--- Obtaining initial guess for tauv by solving vib-elec-el ODE through Chebyshev-NR');
        [tauv,conv] = solve_ODECheby_vibEnergy(tauv_w,tauv_e,intel.eta,y_s,intel,options,tol);
    otherwise
            error(['the chosen ODE method for determining the initial guess for tauv ''',options.ODEtauvMethod,''' is not supported']);
end

if conv<tol
    disp(['--- Shooting solved with conv=',num2str(conv,4),' < tol=',num2str(tol)]);
elseif isnan(conv)
    error('shooting method NaN''d. Consider using another options.model_tauv for the initial guess of tauv');
else
    warning(['--- WARNING! Shooting did not converge conv=',num2str(conv,4),' > tol=',num2str(tol)]);
    pause(1);
end

end % eval_ODEtauv