function [f,df_deta,Cpv,av1] = props4ODEtauv(eta,tauv,intel,options,flds)
% props4ODEtauv computes the needed properties for the tauv ODE.
%
% Usage:
%   (1)
%       [f,df_deta,Cpv,av1] = props4ODEtauv(eta,tauv,intel,options,y_s_in)
%
% Inputs and outputs:
%   eta
%       self-similar non-dimensional coordinate
%   tauv
%       value of tauv for which the properties are to be computed
%   intel
%       classic DEKAF intel structure that must contain (at least):
%           g, f, df_deta
%               profiles generated with build_IC
%           eta
%               coordinate corrersponding to the profiles generated with build_IC
%           mixCnst
%               structure containing all the necessary species properties
%           U_e, Tv_e, p_e, h_e, rho_e, mu_e
%               values of the different flow variables at the BL edge
%   options
%       classic DEKAF options structure
%   flds
%       structure containing the values of the different field variables
%       (H, df_deta, f, and y_s) at each eta position (also stored in
%       flds.eta)
%   f, df_deta
%       velocity computational variables
%   Cpv
%       heat capacity ratio appearing in the vib-elec-el energy equation
%   av1
%       vib-elec-el thermal conductivity computational variable
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0


% extracting fields
[min_deta,idx_eta] = min(abs(flds.eta-eta));
if min_deta>options.T_tol
    interpMeth = 'spline';
    H = interp1(flds.eta,flds.H,eta,interpMeth);
    df_deta = interp1(flds.eta,flds.df_deta,eta,interpMeth);
    f = interp1(flds.eta,flds.f,eta,interpMeth);
    y_s = interp1(flds.eta,flds.y_s,eta,interpMeth);
    % error(['the difference (',num2str(min_deta),...
    %     ') between the requested eta and that for which values are stored is higher than the chosen tolerance (options.T_tol=',...
    %     num2str(options.T_tol),'). Consider changing it or choosing another ODE solving method (options.ODEtauvMethod) for the tauv initial guess.']);
else
    H       = flds.H(idx_eta);
    df_deta = flds.df_deta(idx_eta);
    f       = flds.f(idx_eta);
    y_s     = flds.y_s(idx_eta,:);
end
p       = intel.p_e*ones(size(eta));
% Computing properties from them
u = df_deta*intel.U_e;
h = H-0.5*u.^2;
Tv = tauv*intel.Tv_e;
mixCnst = intel.mixCnst;
NTv = length(Tv);
thetaVib_s  = repmat(permute(mixCnst.thetaVib_s,  [3,1,2]),[NTv,1,1]);
thetaElec_s = repmat(permute(mixCnst.thetaElec_s, [3,1,2]),[NTv,1,1]);
gDegen_s    = repmat(permute(mixCnst.gDegen_s,    [3,1,2]),[NTv,1,1]);
bElectron   = repmat(mixCnst.bElectron.'                  ,[NTv,1]);
nAtoms_s    = repmat(mixCnst.nAtoms_s.'                   ,[NTv,1]);
hForm_s     = repmat(mixCnst.hForm_s.'                    ,[NTv,1]);
R_s         = repmat(mixCnst.R_s.'                        ,[NTv,1]);
Tv_mat      = repmat(Tv,                                   [1,mixCnst.N_spec]);

[hv,cpv] = getMixture_hv_cpv(y_s,Tv_mat,Tv_mat,Tv_mat,R_s,...
    thetaVib_s,thetaElec_s,gDegen_s,bElectron,options,mixCnst.AThermFuncs_s,nAtoms_s,hForm_s); % vibrational enthalpy and heat capacity
cptr = getMixture_cptr(y_s,R_s,nAtoms_s,bElectron,options);
hForm = (y_s.*hForm_s)*ones(mixCnst.N_spec,1);                                  % mixture formation enthalpy
Cpv = cpv * intel.Tv_e/intel.h_e;
htr = h-hv-hForm;                                                               % mixture sensible trans-rot enthalpy
T   = htr./cptr;                                                                % trans-rot temperature
cp  = cptr+cpv;                                                                 % total heat capacity
rho = getMixture_rho(y_s,p,T,Tv,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options); % mixture density
Mm  = getMixture_Mm_R(y_s,T,Tv,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron);      % mixture molar mass
[~,~,kappav] = getTransport_mu_kappa(y_s,T,Tv,rho,p,Mm,options,mixCnst,T,Tv,Tv,cp,'2T'); % vibrational thermal conductivity
av1 = intel.Tv_e/(intel.h_e*intel.rho_e*intel.mu_e) * kappav.*rho;              % computing av1

end % props4ODEtauv
