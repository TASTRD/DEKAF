function y = solve_ODEEuler(f,t,y0,varargin)
% solve_ODEEuler uses the Euler method to evaluate an ODE of the type:
% dy/dt = f(t,y)
%
% Usage:
%   (1)
%       y = solve_ODEEuler(f,t,y0)
%
%   (2)
%       y = solve_ODEEuler(...,'ymin',ymin)
%       |--> sets a lower bound on the values that y can have
%
%   (3)
%       y = solve_ODEEuler(...,'ymax',ymax)
%       |--> sets an upper bound on the values that y can have
%
% Inputs and outputs:
%   f(t,y)  - annonymous function
%   t       - vector of t for which the ODE must be evaluated (length Nt)
%   y0      - initial value of y (length Neq)
%   y       - solution after solving the ODE (size Nt x Neq)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% checking inputs
[~,bymin,idx_ymin] = find_flagAndRemove('ymin',varargin);
[~,bymax,idx_ymax] = find_flagAndRemove('ymax',varargin);

Nt = length(t);                                 % number of t points
Neq = length(y0);                               % number of equations
y = zeros(Neq,Nt);                              % allocating
y(:,1) = y0(:);                                 % populating first position
for ii=1:Nt-1                                   % looping t
    dt = t(ii+1)-t(ii);                             % step in t
    k1 = dt*f(t(ii),y(:,ii));
    y(:,ii+1) = y(:,ii) + k1;  % evaluating function
    if bymin
        ymin = varargin{idx_ymin+1}(:);
        y(y(:,ii+1)<ymin,ii+1) = ymin(y(:,ii+1)<ymin); % setting lower bound
    end
    if bymax
        ymax = varargin{idx_ymax+1}(:);
        y(y(:,ii+1)>ymax,ii+1) = ymax(y(:,ii+1)>ymax); % setting lower bound
    end
end
y = y.';                                        % making it Nt x Neq

end % solve_ODEEuler