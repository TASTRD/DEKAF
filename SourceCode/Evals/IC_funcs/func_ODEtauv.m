function dy = func_ODEtauv(eta,y,intel,options,flds)
% func_ODEtauv defines the system of ordinary differential equations to be
% solved to get the initial guess for tauv.
%
% The equation to be solved is:
%   Cp2*f*dtauv/deta - Cp1*theta*df/deta*tauv + d/deta(av1*dtauv/deta) = 0
%
% Usage:
%   (1)
%       dy = func_ODEtauv(eta,y,intel,options,flds)
%
% Inputs and outputs:
%   y
%       variable vector, composed of:
%       (1) -   tauv
%       (2) -   av1*dtauv_deta
%   dy
%       solution vector composed of:
%       (1) -   dtauv_deta
%       (2) -   d/deta(av1*dtauv_deta)
%
%   eta
%       self-similar non-dimensional coordinate
%   intel
%       classic DEKAF intel structure that must contain (at least):
%           g, f, df_deta
%               profiles generated with build_IC
%           eta
%               coordinate corrersponding to the profiles generated with build_IC
%           mixCnst
%               structure containing all the necessary species properties
%           U_e, T_e, p_e, h_e, rho_e, mu_e
%               values of the different flow variables at the BL edge
%   options
%       classic DEKAF options structure
%   flds
%       structure containing the values of the different field variables
%       (H, df_deta, f, and y_s) at each eta position (also stored in
%       flds.eta)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% computing properties
[f,df_deta,Cpv,av1] = props4ODEtauv(eta,y(1),intel,options,flds);

% Evaluating ODE
dy = ones(2,1);
dy(1) = y(2)./av1;
dy(2) = - Cpv.*f.*y(2)./av1 + Cpv.*intel.theta.*df_deta.*y(1);

end % func_ODEtauv