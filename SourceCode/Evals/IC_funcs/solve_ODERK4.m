function y = solve_ODERK4(f,t,y0,varargin)
% solve_ODERK4 uses the fourth-order Runge-Kutta method to evaluate an ODE
% of the type:  dy/dt = f(t,y)
%
% Usage:
%   (1)
%       y = solve_ODERK4(f,t,y0)
%
%   (2)
%       y = solve_ODERK4(...,'ymin',ymin)
%       |--> sets a lower bound on the values that y can have
%
%   (3)
%       y = solve_ODERK4(...,'ymax',ymax)
%       |--> sets an upper bound on the values that y can have
%
% Inputs and outputs:
%   f(t,y)  - annonymous function
%   t       - vector of t for which the ODE must be evaluated (length Nt)
%   y0      - initial value of y (length Neq)
%   y       - solution after solving the ODE (size Nt x Neq)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% checking inputs
[~,bymin,idx_ymin] = find_flagAndRemove('ymin',varargin);
[~,bymax,idx_ymax] = find_flagAndRemove('ymax',varargin);

Nt = length(t);                                 % number of t points
Neq = length(y0);                               % number of equations
y = zeros(Neq,Nt);                              % allocating
y(:,1) = y0(:);                                 % populating first position
for ii=1:Nt-1                                   % looping t
    dt = t(ii+1)-t(ii);                             % step in t
    k1 = dt*f(t(ii)     ,y(:,ii));                  % evaluating RK coefficients
    k2 = dt*f(t(ii)+dt/2,y(:,ii)+k1/2);
    k3 = dt*f(t(ii)+dt/2,y(:,ii)+k2/2);
    k4 = dt*f(t(ii)     ,y(:,ii)+k3);
    y(:,ii+1) = y(:,ii) + 1/6 * (k1+2*k2+2*k3+k4);  % evaluating function
    if bymin
        ymin = varargin{idx_ymin+1}(:);
        y(y(:,ii+1)<ymin,ii+1) = ymin(y(:,ii+1)<ymin); % setting lower bound
    end
    if bymax
        ymax = varargin{idx_ymax+1}(:);
        y(y(:,ii+1)>ymax,ii+1) = ymax(y(:,ii+1)>ymax); % setting lower bound
    end
end
y = y.';                                        % making it Nt x Neq

end % solve_ODERK4