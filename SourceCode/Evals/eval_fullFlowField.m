function err = eval_fullFlowField(intel,options)
% eval_fullFlowField(intel,options) evaluates the system equations on
% the full flow field and plots the residuals as a function of eta and xi
% for each of the equations.
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

% extracting and modifying necessary options
N = options.N;
N_x = options.mapOpts.N_x;
switch options.flow
    case {'CPG','TPG','TPGD','CNE'};    N_spec = options.N_spec;
    case 'LTEED';                       N_spec = options.N_elem;
    otherwise;                          error(['The chosen flow assumption ''',options.flow,''' is not supported']);
end
options.globalEval = true;

% repeating edge quantities to make them N x N_x matrices
eta_mat = repmat(intel.eta,[1,N_x]);
xi_mat = repmat(intel.xi,[N,1]);
intel4globalEval = intel;
intel4globalEval.xi = xi_mat;
intel4globalEval.beta = repmat(intel.beta,[N,1]);
intel4globalEval.Theta = repmat(intel.Theta,[N,1]);
intel4globalEval.theta = repmat(intel.theta,[N,1]);
intel4globalEval.Ecu   = repmat(intel.Ecu,[N,1]);

% Evaluating equation
[a_forc,b_forc,~] = eval_forcing(intel4globalEval,[],options);
eq_vec = a_forc + b_forc;

% separating outputted vector into the contributions of the different
% equations
err.momXeq  = eq_vec(1:N,:); % The x-momentum equation is the 1st N positions of the eq. vector
if options.Cooke
    err.momZeq  = eq_vec(2*N+1:3*N,:); % The z-momentum equation is between the 3rd & 4th N positions of the eq. vector
    err.enereq   = eq_vec(3*N+1:4*N,:); % The energy equation is between the 4th & 5th N positions of the eq. vector
    if sum(strcmp(options.flow,{'TPGD','CNE','LTEED'}))
        for s=1:N_spec-1
            err.(['speceq',num2str(s)]) = eq_vec((3+s)*N+1:(4+s)*N,:); % The species equations are between the 6+2(s-1)th and the 7+2(s-1)th positions of the eq. vector
        end
        err.concCond = eq_vec(end-2*N+1:end-N,:); % The concentration condition is in the second-last N positions
    end
else
    err.enereq   = eq_vec(2*N+1:3*N,:); % The energy equation is between the 4th & 5th N positions of the eq. vector
    if sum(strcmp(options.flow,{'TPGD','CNE','LTEED'}))
        for s=1:N_spec-1
            err.(['speceq',num2str(s)]) = eq_vec((2+s)*N+1:(3+s)*N,:); % The species equations are between the 5+2(s-1)th and the 6+2(s-1)th positions of the eq. vector
        end
        err.concCond = eq_vec(end-2*N+1:end-N,:); % The concentration condition is in the second-last N positions
    end
end

% Defining size of the subplots
err_fields = fieldnames(err);
N_err = length(err_fields);
[sub_I,sub_J] = optimalPltSize(N_err);

% plotting
figure
for ii=1:N_err
    err2plot = err.(err_fields{ii});                    % extracting field from structure
    if isfield(intel.norms_ref,err_fields{ii})          % searching in norms_ref for a reference
        ref_norm = intel.norms_ref.(err_fields{ii});
        disp(err_fields{ii});
    else                                                % otherwise we set the reference to 1
        ref_norm = 1;
    end

    err2plot(isinf(err2plot)) = eps*1e3;
    err2plot(isnan(err2plot)) = eps*1e3;
    err2plot_mean = log10(abs(mean(mean(err2plot(2:end-1,:))/ref_norm)));
    err2plot = log10(abs(err2plot/ref_norm));           % we normalize the error with respect to the reference
    disp(['Max ', err_fields{ii}, ': ', num2str(max(max(err2plot(2:end-1,:))))]);
    disp(['Mean ', err_fields{ii}, ': ', num2str(err2plot_mean)]);
    err2plot(err2plot<-30) = -30;                       % we cut off whatever is lower than -30
    subplot(sub_I,sub_J,ii)
    surf(xi_mat(2:end-1,:),eta_mat(2:end-1,:),err2plot(2:end-1,:))
    xlabel('\xi'); ylabel('\eta'); zlabel(['log(',err_fields{ii},')']);
    ylim([0,6]);
end

end % eval_fullFlowField
