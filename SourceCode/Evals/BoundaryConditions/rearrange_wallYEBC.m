function [yE_w,options] = rearrange_wallYEBC(intel,options,y_Ee)
% rearrange_wallYEBC rearranges the options associated to the wall
% elemental-mass-fraction boundary condition, and makes the apropriate
% definitions in options.YE_bc. 
%
% At the exit of this function the 'yEwProfile' BC will be substituted for
% 'yEWall'.
%
% Usage:
%   (1)
%       [yE_w,options] = rearrange_wallYEBC(intel,options,y_Ee)
%
% Inputs and outputs:
%   intel
%       intel structure with the required fields
%   options
%       options structure with the required options
%   y_Ee
%       edge elemental mass fractions (in matrix form)
%   ys_w
%       wall elemental mass fractions (in cell form)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0


switch options.wallYE_BC
    case 'zeroYEgradient'                   % zero elemental-mass-fraction gradient
        options.YE_bc = num2cell(zeros(1,intel.mixCnst.N_elem));    % placeholder
        yE_w = NaN;                                                 % placeholder
    case 'yEWall'                           % elemental mass fraction at the wall
        options = standardvalue(options,'YE_bc',num2cell(y_Ee(1,:)),'No value was passed for the wall mass fractions, so the edge values were taken');
        yE_w = cellfun(@(cll)repmat(cll,[length(options.x_bc),1]),options.YE_bc,'UniformOutput',false);
    case 'yEwProfile'                       % elemental mass fraction profile at the wall
        neededvalue(options,'YE_bc','options.YE_bc must be passed for options.wallYE_BC=''yswProfile''');
        yE_w = options.YE_bc;
        options.wallYE_BC = 'yEWall';           % from here on, we don't distinguish between ysWall, and yswProfile
        if ~options.marchYesNo
            warning(['An options.wallYE_BC = ''yEwProfile'' was chosen for the QSS solver, so the first value (at x = ',num2str(options.x_bc(1)),' m) was taken']); pause(1);
        end
    otherwise                               % otherwise break
        error(['the chosen wallYE_BC ''',options.wallYE_BC,''' is not supported']);
end

end % rearrange_wallYEBC