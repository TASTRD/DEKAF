function options = rearrange_wallBlowingBC(options,rho_e,mu_e,U_e,p_e,R_e,T_e,gam_e,M_e,xi_e,xPhys_e,varargin)
% rearrange_wallBlowingBC rearranges the options associated to the wall
% boundary condition, and makes the apropriate definitions in options.F_bc
%
% Usage:
%   (1)
%       options = rearrange_wallBlowingBC(options,rho_e,mu_e,U_e,p_e,R_e,...
%                                               T_e,gam_e,M_e,xi_e,xPhys_e)
%       |--> how it should be called if there is a single xi_e where it
%       should all be evaluated (like in the SS case). This usage will not
%       work for marching
%   (2)
%       options = rearrange_wallBlowingBC(...,'cone',r0)
%       |--> makes the transformation of V correctly for a conical
%       coordinate system
%
% Inputs and outputs:
%   options     - options structure with the required options
%   rho_e       - edge density at the first streamwise position
%   mu_e        - edge dynamic viscosity at the first streamwise position
%   U_e         - edge streamwise velocity at the first streamwise position
%   p_e         - edge pressure at the first streamwise position
%   R_e         - edge gas constant at the first streamwise position
%   T_e         - edge temperature at the first streamwise position
%   gam_e       - edge specific heat capacity ratio at the first streamwise
%               position
%   M_e         - edge Mach number at the first streamwise position
%   xi_e        - vector of xi corresponding to the positions where the
%               BL computation is to be made
%   xPhys_e     - vector of dimensional (physical) x corresponding to the
%               positions where the BL computation is to be made
%   r0          - base radius of the cone at each xi_e position
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[r0,~] = parse_optional_input(varargin,'cone',1); % looking to see if the user passed r0, and fixing it to 1 otherwise (no modification of the boundary condition)

switch options.wallBlowing_BC
    case {'Vwall','mwall'}
        x_bc = 0;
    case 'VwProfile'
        x_bc = options.x_bc; % extracting coordinate paired with the profile
        options.wallBlowing_BC = 'Vwall'; % from now on, we don't distinguish between Vwall and VwProfile
    case 'mwProfile'
        x_bc = options.x_bc; % extracting coordinate paired with the profile
        options.wallBlowing_BC = 'mwall'; % from now on, we don't distinguish between mwall and mwProfile
    otherwise
        error(['something went really wrong. Unrecognized wallBlowing_BC ''',options.wallBlowing_BC,'''']);
end

options.trackerVwall = interp1OrRepmat(x_bc,options.trackerVwall,xPhys_e,options.meth_edge_interp); % interpolating onto the grid points

switch options.wallBlowing_BC                                   % depending on the wallBlowing BC
    case 'Vwall'                                                    % at this point it can only be Vwall
        % Estimation of the wall density
        switch options.thermal_BC                                       % depending on the thermal wall-BC
            case 'H_F'                                                      % no matter what heat-flux is chosen, we will assume adiabatic for the estimation of rho_w
                T_w1 = (1-(gam_e-1)/2 * M_e^2)*T_e;                             % estimated wall temperature at the first position
            case 'Twall'                                                    % isothermal
                T_w1 = options.trackerTwall(1);
            otherwise                                                       % if it is neither, break
                error(['the chosen thermal_BC ''',options.thermal_BC,''' is not supported in the determination of rho_w']);
        end
        rho_w1 = p_e/(R_e*T_w1);                                        % initial estimated wall density
        V_w1 = options.trackerVwall(1);                                 % initial wall-blown velocity
        m_w1 = rho_w1*V_w1;                                             % initial wall-blown mass flow
    case 'mwall'                                                    % ... or mWall
        m_w1 = options.trackerVwall(1);                                 % initial wall-blown mass flow
    otherwise                                                       % ... otherwise break
        error(['the chosen wallBlowing_BC ''',options.wallBlowing_BC,''' is not supported']);
end

options.F_bc = getBlowing_fw(m_w1,xi_e(1),rho_e,U_e,mu_e,r0); % obtaining f boundary condition


end % rearrange_wallBlowingBC