function [T_w,Tv_w,g_lim,options] = rearrange_thermalBC(intel,options,ys_w,y_se,TTrans_e,TVib_e,p_e,H_e,h_e,cp_e,Pr_e,x_e)
% rearrange_thermalBC rearranges the options associated to the wall
% mass-fraction boundary condition, and makes the apropriate definitions in
% options.Ys_bc.
%
% Usage:
%   (1)
%       [T_w,Tv_w,g_lim,options] = rearrange_thermalBC(intel,options,ys_w,y_se,TTrans_e,TVib_e,p_e,H_e,h_e,cp_e,Pr_e,x_e)
%
% Inputs and outputs:
%   intel
%       intel structure with the required fields
%   options
%       options structure with the required options, including:
%           x_bc
%               position vector for the boundary-condition profiles (N_xbc x 1)
%           trackerTwall
%               wall-temperature boundary-condition profile (N_xbc x 1)
%   ys_w
%       cell of vectors with the wall mass fractions {size N_spec x (N_xbc x 1)}
%   y_se
%       edge mass fractions (size Nx x 1)
%   TTrans_e
%       edge translational temperature (size Nx x 1)
%   TVib_e
%       edge vibrational temperature (size Nx x 1)
%   p_e
%       edge static pressure (size Nx x 1)
%   H_e
%       edge total enthalpy (size Nx x 1)
%   h_e
%       edge static enthalpy (size Nx x 1)
%   cp_e
%       edge heat capacity at constant pressure (size Nx x 1)
%   Pr_e
%       edge Prandtl number (size Nx x 1)
%   x_e
%       position vector paired with the edge vectors (size Nx x 1)
%   g_lim
%       limit to impose on the non-dimensional total enthalpy (size 1 x 1)
%   T_w
%       vector of wall temperatures (size N_xbc x 1)
%   Tv_w
%       vector of wall vibrational temperatures (size N_xbc x 1)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

x_bc = options.x_bc(:);                                                     % position vector paired with the boundary conditions
mixCnst = intel.mixCnst;                                                    % mixture constants

% Interpolating
TTrans_e    = interp1OrRepmat(x_e,TTrans_e  ,x_bc,options.meth_edge_interp);
TVib_e      = interp1OrRepmat(x_e,TVib_e    ,x_bc,options.meth_edge_interp);
H_e         = interp1OrRepmat(x_e,H_e       ,x_bc,options.meth_edge_interp);
h_e         = interp1OrRepmat(x_e,h_e       ,x_bc,options.meth_edge_interp);
cp_e        = interp1OrRepmat(x_e,cp_e      ,x_bc,options.meth_edge_interp);
Pr_e        = interp1OrRepmat(x_e,Pr_e      ,x_bc,options.meth_edge_interp);
p_e         = interp1OrRepmat(x_e,p_e       ,x_bc,options.meth_edge_interp);

if ismember(options.thermal_BC,{'Twall','TwallProfile'}) || (strcmp(options.thermal_BC,'ablation') && ~options.marchYesNo) % we have a condition on the wall enthalpy, and that could be smaller than H_e
    switch options.thermal_BC
        case 'Twall'                                                        % we will copy the tracker_Twall to make a vector out of it
            T_w = options.trackerTwall;
            T_w = repmat(T_w,size(x_bc));
        case 'TwallProfile'                                                 % we will interpolate the provided trackerTwall to the physical xPhys_e
            T_w = options.trackerTwall(:);
            options.thermal_BC = 'Twall';                                       % from here on, we don't distinguish between Twall, and TwallProfile
            if ~options.marchYesNo
                warning(['An options.thermal_BC = ''TwallProfile'' was chosen for the QSS solver, so the first value (at x = ',num2str(x_bc(1)),' m) was taken']); pause(1);
            end
        case 'ablation'                                                     % we will use as the initial wall temperature the maximum that can be stood by the surface material
%             T_w = mixCnst.gasSurfReac_struct.Tw_max;
            T_w = NaN(size(x_bc));
            % NOTE: consider that this option in the conditional fork will
            % only be accessed if the ablation BC is chosen for the QSS
            % solver (beginning of the domain). For marching positions, the
            % solver will use the full ablation BC
        otherwise
        error(['the chosen thermal_BC ''',options.thermal_BC,''' is not supported']);
    end
    switch options.numberOfTemp
        case '1T'
            Tv_w = T_w;
        case '2T'
            switch options.thermalVib_BC
                case 'frozen'
                    Tv_w = TVib_e;
                    Tv_w = repmat(Tv_w,size(x_bc));
                case 'thermalEquil'                                                 % nothing to pass, temperature equal to the
                    Tv_w = T_w;
                case 'Twall'                                                        % we will copy the tracker_Twall to make a vector out of it
                    Tv_w = options.trackerTvwall;
                    Tv_w = repmat(Tv_w,size(x_bc));
                case 'TwallProfile'                                                 % we will interpolate the provided trackerTwall to the physical xPhys_e
                    Tv_w = options.trackerTvwall;
                    if ~options.marchYesNo
                        warning(['An options.thermalVib_BC = ''',options.thermalVib_BC,''' was chosen for the QSS solver, so the first value (at x = ',num2str(x_bc(1)),' m) was taken']); pause(1);
                    end
                    options.thermalVib_BC = 'Twall';                                    % from here on, we don't distinguish between Twall, and TwallProfile
                case 'ablation'                                                     % we will use as the initial wall temperature the maximum that can be stood by the surface material
                    Tv_w = NaN(size(x_bc));
                    % NOTE: consider that this option in the conditional fork will
                    % only be accessed if the ablation BC is chosen for the QSS
                    % solver (beginning of the domain). For marching positions, the
                    % solver will use the full ablation BC
                otherwise
                error(['the chosen thermalVib_BC ''',options.thermalVib_BC,''' is not supported']);
            end
        otherwise
            error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
    end
    switch options.flow
        case 'CPG'
            H_w = cp_e.*T_w;
            hv_w = H_w;
        case {'LTE','LTEED','TPGD','TPG','CNE'}
            switch options.flow
                case {'LTE','LTEED'}
                    [X_sw,spec_list_table] = getEquilibrium_X_tables(p_e,T_w,mixCnst.LTEtableData,options); % estimating wall mole fractions from tables
                    idx_spec = cellfun(@(cll)find(strcmp(spec_list_table,cll)),mixCnst.spec_list);          % rearranging species if necessary
                    y_sw = getEquilibrium_ys(X_sw(:,idx_spec),mixCnst.Mm_s);                                % wall mass fractions
                case {'TPGD','TPG','CNE'}
                    switch options.wallYs_BC
                        case {'nonCatalytic','ablation'};   y_sw = repmat(y_se(1,:),size(x_bc));    % we guess that the mass fraction is equal to the freestream
                        case 'ysWall';                      y_sw = cell1D2matrix2D(ys_w);           % we obtain it from the BC value
                        otherwise;                          error(['the chosen wallYs_BC=''',options.wallYs_BC,''' is not supported']);
                    end
                    % IMPORTANT: the wall enthalpy is computed here assuming the edge
                    % mass fractions. In chemical non-equilibrium this will not be the
                    % case, and if the cp of the mixture at the wall is significantly
                    % lower than in the freestream, the wall enthalpy could go down
                    % below the limit established by g_lim.
                otherwise
                    error(['The chosen flow assumption ''',options.flow,''' is not supported']);
            end
            [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,Tw_mat,Tvw_mat] = reshape_inputs4enthalpy(mixCnst.R_s,...
                mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,T_w,Tv_w);
            [H_w,~,~,~,~,~,~,hv_w] = getMixture_h_cp(y_sw,Tw_mat,Tw_mat,Tvw_mat,Tvw_mat,Tvw_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);
        otherwise
            error(['The chosen flow assumption ''',options.flow,''' is not supported']);
    end
    g_w = H_w./H_e; % we initially fix the value of G at the wall assuming the same cp as in the freestream
    options.G_bc = g_w(1); % we only care about the first one, the rest will be updated through update_g_isoThermal and eval_thermo
    options.tauv_bc = Tv_w(1)./TTrans_e(1);
    options.gv_bc = hv_w(1)./h_e(1); % used only for the initial guess
    if min([T_w(:);TTrans_e(:)])<400 && options.perturbRelaxation>0.5 && strcmp(options.numberOfTemp,'2T')
        warning('The flow''s temperature is very low, and in 2-T models, it may lead to convergence problems. Consider making options.perturbRelaxation=0.5');
        pause(2);
    end
    % If the maximum wall temperature is higher than the adiabatic, we will
    % make the wall adiabatic
    if strcmp(options.thermal_BC,'ablation') && H_w > sqrt(Pr_e)*H_e
        options.thermal_BC = 'H_F';
        options.G_bc = 0;
    else
        options.trackerTwall = T_w;
        options.trackerTvwall = Tv_w;
    end
elseif strcmp(options.thermal_BC,'gwall')
    H_w = options.G_bc.*H_e; % exact
    T_w = NaN*H_w; % placeholder
    Tv_w = NaN*H_w; % placeholder
else
    H_w = sqrt(Pr_e).*H_e;  % recovery factor
    T_w = NaN*H_w; % placeholder
    Tv_w = NaN*H_w; % placeholder
end
% HARDCODE WARNING: we fix the limit enthalpy to 50% of the minimum
g_lim = 0.5*min([1;H_w(:)./H_e(:)]);

end % rearrange_thermalBC