function [ys_w,options] = rearrange_wallYsBC(intel,options,y_se)
% rearrange_wallYsBC rearranges the options associated to the wall
% mass-fraction boundary condition, and makes the apropriate definitions in
% options.Ys_bc.
%
% At the exit of this function the 'yswProfile' BC will be substituted for
% 'ysWall'.
%
% Usage:
%   (1)
%       [ys_w,options] = rearrange_wallYsBC(intel,options,y_se)
%
% Inputs and outputs:
%   intel
%       intel structure with the required fields
%   options
%       options structure with the required options
%   y_se
%       edge mass fractions
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0


switch options.wallYs_BC
    case {'nonCatalytic','ablation'}        % zero mass-fraction gradient
        options.Ys_bc = num2cell(zeros(1,intel.mixCnst.N_spec));    % placeholder
        ys_w = NaN;                                                 % placeholder
    case 'catalyticGamma'
        neededvalue(options,'Ys_bc',['Error: the chosen wallYs_BC ''',options.wallYs_BC,''' requires a value of the probability of catalytic recombination in options.Ys_bc.']);
        ys_w = NaN;                                                 % placeholder
    case 'catalyticK'
        neededvalue(options,'Ys_bc',['Error: the chosen wallYs_BC ''',options.wallYs_BC,''' requires a value of the catalytic-recombination velocity [m/s] in options.Ys_bc.']);
        ys_w = NaN;                                                 % placeholder
    case 'ysWall'                           % mass fraction at the wall
        options = standardvalue(options,'Ys_bc',num2cell(y_se(1,:)),'No value was passed for the wall mass fractions, so the edge values were taken');
        ys_w = cellfun(@(cll)repmat(cll,[length(options.x_bc),1]),options.Ys_bc,'UniformOutput',false);
    case 'yswProfile'                       % mass fraction profile at the wall
        neededvalue(options,'Ys_bc','options.Ys_bc must be passed for options.wallYs_BC=''yswProfile''');
        ys_w = options.Ys_bc;
        options.wallYs_BC = 'ysWall';           % from here on, we don't distinguish between ysWall, and yswProfile
        if ~options.marchYesNo
            warning(['An options.wallYs_BC = ''yswProfile'' was chosen for the QSS solver, so the first value (at x = ',num2str(options.x_bc(1)),' m) was taken']); pause(1);
        end
    case 'fullCatalytic'                    % full catalytic condition
        error('fullCatalytic not supported yet');
    otherwise                               % otherwise break
        error(['the chosen wallYs_BC ''',options.wallYs_BC,''' is not supported']);
end

end % rearrange_wallYsBC