function [mat,forcing] = eval_thermalEquilibriumBC(mat,forcing,intel,options,varargin)
% eval_thermalEquilibriumBC is a function to evaluate the thermal
% equilibrium BC on the system matrix and forcing vector.
%
% It is meant to liberate gv at the wall.
%
% Usage:
%   (1)
%       [mat,forcing] = eval_thermalEquilibriumBC(mat,forcing,intel,options);
%       |--> for non-marching cases
%
%   (2)
%       [mat,forcing] = eval_thermalEquilibriumBC(mat,forcing,intel,options,intelp);
%       |--> for marching cases (require intelp structure)
%
% Inputs and outputs:
%   mat     - system matrix
%   forcing - system forcing vector
%   intel   - DEKAF flowfield structure
%   options - DEKAF options structure
%
% See also: eval_ablationEnergyBC, eval_ablationConcentrationBC,
% eval_ablationMassBC, build_bc
%
% Author: Fernando Miro Miro
% Date: December 2018
% GNU Lesser General Public License 3.0

% extracting variables of interest
tau = intel.tau;
tauv = intel.tauv;
Bv_g = intel.Bv_g;
Bv_tauv = intel.Bv_tauv;
if ismember(options.flow,{'CNE','TPGD'})    % for multi-species flow assumptions
    Bv_ys = intel.Bv_ys;
end

% Determining where the BC is to be applied
Ntot = size(mat,1);                                             % total number of points
N = options.N;                                                  % number of wall-normal points
if options.Cooke                                                % if the cooke system is to be solved (with k also)
    if ismember(options.flow,{'TPGD','CNE'})                        % for multispecies
        idxgv = (5+intel.mixCnst.N_spec)*N;                             % gv will be the "5+N_spec"-th variable
    else                                                            % for non-multispecies
        idxgv = 5*N;                                                    % gv will be the 5th variable
    end
else                                                            % if the cooke system is not to be solved (no k)
    if ismember(options.flow,{'TPGD','CNE'})                            % for multispecies
        idxgv = (4+intel.mixCnst.N_spec)*N;                                 % gv will be the "4+N_spec"-th variable
    else                                                                % for non-multispecies
        idxgv = 4*N;                                                        % gv will be the 4th variable
    end
end

% computing forcing term
a = tauv(end) - tau(end);                                       % temperature difference at the wall
forcing(idxgv) = -a;                                            % building forcing term

% computing matrix terms
A = zeros(1,Ntot);                                              % allocating
ivc = 1;                                                        % index counting variables (df_deta)
ivc = ivc+1;                                                    % increasing var. counter (f)
if options.Cooke                                                % for Cooke
    ivc=ivc+1;                                                      % increasing var. counter (k)
end
ivc=ivc+1;                                                      % increasing var. counter (g)
A(1,ivc*N) = -Bv_g(end);                                        % term multiplying g
% DEVELOPERS NOTE: the equation is evaluated at the wall, which is why
% we do ivc*N - we want every variable at the wall (Nth position)
if ismember(options.flow,{'TPGD','CNE'})                        % for multispecies
    for s=1:intel.mixCnst.N_spec                                    % looping species
        ivc=ivc+1;                                                      % increasing var. counter (ys)
        A(1,ivc*N) = Bv_ys{s};                                          % term multiplying ysh
    end
end
ivc=ivc+1;                                                      % increasing var. counter (tauv)
A(1,ivc*N) = Bv_tauv(end);                                        % term multiplying tauv

mat(idxgv,:) = A;                                           % populating system matrix
