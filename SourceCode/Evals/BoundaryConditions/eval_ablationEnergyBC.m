function [mat,forcing] = eval_ablationEnergyBC(mat,forcing,intel,options,varargin)
% eval_ablationEenergyBC is a function to evaluate the ablation energy
% conservation BC on the system matrix and forcing vector.
%
% It is meant to liberate g at the wall.
%
% Usage:
%   (1)
%       [mat,forcing] = eval_ablationEnergyBC(mat,forcing,intel,options);
%       |--> for non-marching cases
%
%   (2)
%       [mat,forcing] = eval_ablationMassBC(mat,forcing,intel,options,intelp);
%       |--> for marching cases (require intelp structure)
%
% Inputs and outputs:
%   mat     - system matrix
%   forcing - system forcing vector
%   intel   - DEKAF flowfield structure
%   options - DEKAF options structure
%
% DEVELOPERS NOTE: expressions obtained from using the mini-automatic
% derivation tool in Derivation/BLEquations/derivation_perturbation_eqs_CNEablation_General.mac
%
% See also: eval_ablationMassBC, eval_ablationConcentrationBC, build_bc
%
% Author: Fernando Miro Miro
% Date: December 2018
% GNU Lesser General Public License 3.0

% extracting variables of interest
xi = intel.xi;
dg_deta = intel.dg_deta;
a1 = intel.a1;
D1 = intel.D1;
dys_deta = intel.dys_deta;
as1 = intel.as1;
OmegaAbl = intel.OmegaAbl;                      % radiation energy source term
dOmegaAbl_dg = intel.dOmegaAbl_dg;
dOmegaAbl_ddfdeta = intel.dOmegaAbl_ddfdeta;
dOmegaAbl_dyl = intel.dOmegaAbl_dyl;
switch options.numberOfTemp
    case '1T'
        % nothing more needed
    case '2T'                                   % for two-temperatures
        dtauv_deta = intel.dtauv_deta;
        av1 = intel.av1;
        dOmegaAbl_dtauv = intel.dOmegaAbl_dtauv;
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

% Determining where the BC is to be applied
Ntot = size(mat,1);                                             % total number of points
N = options.N;                                                  % number of wall-normal points
if options.Cooke                                                % if the cooke system is to be solved (with k also)
    idxg = 4*N;                                                     % g will be the 4th variable
else                                                            % if the cooke system is not to be solved (no k)
    idxg = 3*N;                                                     % g will be the 3rd variable
end

% computing forcing term
a = a1(end)*dg_deta(end);                                      % Self-similar forcing term
if ismember(options.flow,{'TPGD','CNE'})                            % for multispecies assumptions
    for s=1:intel.mixCnst.N_spec                                        % looping species
        a = a - as1{s}(end)*dys_deta{s}(end);
    end
end
switch options.numberOfTemp
    case '1T'                                                   % for 1-T models
        % nothing to add
    case '2T'                                                   % for 2-T models
        a = a + av1(end)*dtauv_deta(end);                           % conduction of vibrational energy
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end
b = (2*xi)^(1/2) * (-OmegaAbl(end));                        % streamwise-varying term
c = 0;                                                      % and the term corresponding to moving to the lhs the FD terms of the previous stations
forcing(idxg) = -(a + b + c);                                   % building forcing term

% computing matrix terms
A = zeros(1,Ntot);                                              % allocating
B = zeros(1,Ntot);
Cc = zeros(1,Ntot);

ivc = 1;                                                        % index counting variables (df_deta)
ivc = ivc+1;                                                    % increasing var. counter (f)
if options.Cooke                                                % for Cooke
    ivc=ivc+1;                                                      % increasing var. counter (k)
end
ivc=ivc+1;                                                      % increasing var. counter (g)
A(1,(ivc-1)*N+1:ivc*N) = a1(end)*D1(N,:);                       % term multiplying g
% DEVELOPERS NOTE: the equation is evaluated at the wall, which is why
% we do ivc*N - we want every variable at the wall (Nth position)
if ismember(options.flow,{'TPGD','CNE'})                        % for multispecies
    for s=1:intel.mixCnst.N_spec                                    % looping species
        ivc=ivc+1;                                                      % increasing var. counter (ys)
        A(1,(ivc-1)*N+1:ivc*N) = A(1,(ivc-1)*N+1:ivc*N) -  as1{s}(end) *D1(N,:); % term multiplying ysh
    end
end
switch options.numberOfTemp
    case '1T'
        % nothing to add
    case '2T'                                                       % for two-temperatures
        ivc=ivc+1;                                                      % increasing var. counter (tauv)
        A(1,(ivc-1)*N+1:ivc*N) = av1(end)*D1(N,:);                      % term multiplying tauv
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

ivc = 1;                                                        % index counting variables (df_deta)
B(1,ivc*N) = -dOmegaAbl_ddfdeta(end)*(2*xi)^(1/2);              % term multiplying dfh_deta
ivc = ivc+1;                                                    % increasing var. counter (f)
if options.Cooke                                                % for Cooke
    ivc=ivc+1;                                                      % increasing var. counter (k)
end
ivc=ivc+1;                                                      % increasing var. counter (g)
B(1,ivc*N) =  -dOmegaAbl_dg(end)*(2*xi)^(1/2);                  % term multiplying gh
if ismember(options.flow,{'TPGD','CNE'})                        % for multispecies
    for s=1:intel.mixCnst.N_spec                                    % looping species
        ivc=ivc+1;                                                      % increasing var. counter (ys)
        B(1,ivc*N) = -dOmegaAbl_dyl{s}(end)*(2*xi)^(1/2);               % term multiplying ysh
    end
end
switch options.numberOfTemp
    case '1T'
        % nothing to add
    case '2T'                                                       % for two-temperatures
        ivc=ivc+1;                                                      % increasing var. counter (tauv)
        B(1,ivc*N) =  - dOmegaAbl_dtauv(end)*(2*xi)^(1/2);              % term multiplying tauvh
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end
mat(idxg,:) = A+B+Cc;                                           % populating system matrix
