function [mat,forcing] = eval_ablationMassBC(mat,forcing,intel,options,varargin)
% eval_ablationMassBC is a function to evaluate the ablation total mass
% conservation BC on the system matrix and forcing vector.
%
% It is meant to liberate f at the wall (allowing for non-zero mass injection)
%
% Usage:
%   (1)
%       [mat,forcing] = eval_ablationMassBC(mat,forcing,intel,options);
%       |--> for non-marching cases
%
%   (2)
%       [mat,forcing] = eval_ablationMassBC(mat,forcing,intel,options,intelp);
%       |--> for marching cases (require intelp structure)
%
% Inputs and outputs:
%   mat     - system matrix
%   forcing - system forcing vector
%   intel   - DEKAF flowfield structure
%   options - DEKAF options structure
%
% DEVELOPERS NOTE: expressions obtained from using the mini-automatic
% derivation tool in Derivation/BLEquations/derivation_perturbation_eqs_CNEablation_General.mac
%
% See also: eval_ablationEnergyBC, eval_ablationConcentrationBC, build_bc
%
% Author: Fernando Miro Miro
% Date: December 2018
% GNU Lesser General Public License 3.0

% extracting variables of interest
f = intel.f;
if options.marchYesNo                                           % if we march
    Mabl = intel.Mabl;
    dMabl_dg = intel.dMabl_dg;
    dMabl_ddfdeta = intel.dMabl_ddfdeta;
    dMabl_dyl = intel.dMabl_dyl;
    switch options.numberOfTemp
        case '1T'
            % nothing more needed
        case '2T'                                   % for two-temperatures
            dMabl_dtauv = intel.dMabl_dtauv;                % we need the variation of the source term with tauv also
        otherwise
            error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
    end

    xi = intel.xi;
    if isempty(varargin)                                            % checking thatt the number of inputs is correct
        error('Errror: intelp is required');
    else
        intelp = varargin{1};
    end
    df_dxi = intel.df_dxi;                                          % streamwise variation of df_dxi at this station
    % obtaining finite-difference stencil
    ih = 1:size(intelp.f,2);                                         % index to be used when evaluating the c forcing vector
    FDc = intel.FDc_xi.';
end

% Determining where the BC is to be applied
Ntot = size(mat,1);                                             % total number of points
N = options.N;                                                  % number of wall-normal points
idxV = 2*N;
% we want to liberate f at the wall - f is always the second variable, and N
% is the position of the wall

% computing forcing term
a_fw = f(end);                                                  % Self-similar forcing term
if options.marchYesNo                                           % if we march
    b_fw = 2*xi*df_dxi(end) + sqrt(2*xi)*Mabl(end);                 % we obtain the streamwise-varying term
    c_fw = 2*xi * intelp.fh(end,ih) * FDc(ih+1);                    % and the term corresponding to moving to the lhs the FD terms of the previous stations
else                                                            % if we don't march
    b_fw = 0;                                                       % both b and c are zero
    c_fw = 0;
end
forcing(idxV) = -(a_fw + b_fw + c_fw);                          % building forcing term

% computing matrix terms
A_fw = zeros(1,Ntot);                                           % allocating
B_fw = zeros(1,Ntot);
Cc_fw = zeros(1,Ntot);
A_fw(1,idxV) = 1;                                               % Self-similar forcing term
if options.marchYesNo                                           % if we march
    ivc = 1;                                                        % index counting variables (df_deta)
    B_fw(1,ivc*N) = sqrt(2*xi) * dMabl_ddfdeta(end);                % term multiplying dfh_deta
    % DEVELOPERS NOTE: the equation is evaluated at the wall, which is why
    % we do ivc*N - we want every variable at the wall (Nth position)
    ivc = ivc+1;                                                    % increasing var. counter (f)
    Cc_fw(1,ivc*N) = 2*xi*FDc(1);                                   % term multiplying f (equivalent to d/dxi derivative)
    if options.Cooke                                                % for Cooke
        ivc=ivc+1;                                                      % increasing var. counter (k)
    end
    ivc=ivc+1;                                                      % increasing var. counter (g)
    B_fw(1,ivc*N) = sqrt(2*xi) * dMabl_dg(end);                     % term multiplying gh
    if ismember(options.flow,{'TPGD','CNE'})                        % for multispecies
        for l=1:intel.mixCnst.N_spec                                    % looping species
            ivc=ivc+1;                                                      % increasing var. counter (yl)
            B_fw(1,ivc*N) = sqrt(2*xi) * dMabl_dyl{l}(end);                 % term multiplying yhl
        end
    end
    switch options.numberOfTemp
        case '1T'
            % nothing to add
        case '2T'                                                           % for two-temperatures
            ivc=ivc+1;                                                          % increasing var. counter (tauv)
            B_fw(1,ivc*N) = sqrt(2*xi) * dMabl_dtauv(end);                        % term multiplying tauvh
        otherwise
            error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
    end
end
mat(idxV,:) = A_fw+B_fw+Cc_fw;                                  % populating system matrix
