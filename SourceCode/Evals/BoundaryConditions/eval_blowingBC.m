function [mat,forcing] = eval_blowingBC(mat,forcing,intel,options,varargin)
% eval_blowingBC is a function to evaluate the blowing BC on the system
% matrix and forcing vector
%
% Usage:
%   (1)
%       [mat,forcing] = eval_blowingBC(mat,forcing,intel,options);
%       |--> for non-marching cases
%
%   (2)
%       [mat,forcing] = eval_blowingBC(mat,forcing,intel,options,intelp);
%       |--> for marching cases (require intelp structure)
%
% Inputs and outputs:
%   mat     - system matrix
%   forcing - system forcing vector
%   intel   - DEKAF flowfield structure
%   options - DEKAF options structure
%
% Author: Fernando Miro Miro
% Date: November 2018
% GNU Lesser General Public License 3.0

% extracting variables of interest
f_w = intel.f(end);
Afw = intel.Afw;
xi = intel.xi;

if options.marchYesNo
    if isempty(varargin)                                            % checking thatt the number of inputs is correct
        error('Errror: intelp is required');
    else
        intelp = varargin{1};
    end
    df_dxi_w = intel.df_dxi(end);                                   % streamwise variation of df_dxi at this station
    % obtaining finite-difference stencil
    ih = 1:size(intelp.f,2);                                         % index to be used when evaluating the c forcing vector
    FDc = intel.FDc_xi.';
end

idx_fw = 2*options.N;
% we want to liberate f at the wall - f is always the second variable, and N
% is the position of the wall

% computing forcing term
a_fw = f_w + sqrt(2*xi)*Afw;                                    % Self-similar forcing term
if options.marchYesNo                                           % if we march
    b_fw = 2*xi * df_dxi_w;                                         % we obtain the streamwise-varying term
    c_fw = 2*xi * intelp.fh(end,ih) * FDc(ih+1);                    % and the term corresponding to moving to the lhs the FD terms of the previous stations
else                                                            % if we don't march
    b_fw = 0;                                                       % both b and c are zero
    c_fw = 0;
end
forcing(idx_fw) = -(a_fw + b_fw + c_fw);                        % building forcing term

% computing matrix terms
Ntot = size(mat,1);                                             % total number of points
A_fw = zeros(1,Ntot);                                           % allocating
B_fw = zeros(1,Ntot);
Cc_fw = zeros(1,Ntot);
A_fw(1,idx_fw) = 1;                                             % Self-similar forcing term
if options.marchYesNo                                           % if we march
    Cc_fw(1,idx_fw) = 2*xi*FDc(1);                                  % we need also the term coming from the streamwise derivative
end
mat(idx_fw,:) = A_fw+B_fw+Cc_fw;                                % populating system matrix
