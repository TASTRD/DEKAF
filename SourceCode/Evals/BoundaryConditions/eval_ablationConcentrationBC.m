function [mat,forcing] = eval_ablationConcentrationBC(mat,forcing,intel,options,varargin)
% eval_ablationConcentrationBC is a function to evaluate the ablation species
% concentration conservation BC on the system matrix and forcing vector.
%
% It is meant to liberate ys at the wall for all species except for the
% last
%
% Usage:
%   (1)
%       [mat,forcing] = eval_ablationConcentrationBC(mat,forcing,intel,options);
%       |--> for non-marching cases
%
%   (2)
%       [mat,forcing] = eval_ablationConcentrationBC(mat,forcing,intel,options,intelp);
%       |--> for marching cases (require intelp structure)
%
% Inputs and outputs:
%   mat     - system matrix
%   forcing - system forcing vector
%   intel   - DEKAF flowfield structure
%   options - DEKAF options structure
%
% DEVELOPERS NOTE: expressions obtained from using the mini-automatic
% derivation tool in Derivation/BLEquations/derivation_perturbation_eqs_CNEablation_General.mac
%
% See also: eval_ablationEnergyBC, eval_ablationMassBC
%
% Author: Fernando Miro Miro
% Date: December 2018
% GNU Lesser General Public License 3.0

% extracting variables of interest
f = intel.f;
ys = intel.ys;
dys_deta = intel.dys_deta;
zetas = intel.zetas;
dzetas_deta = intel.dzetas_deta;
D1 = intel.D1;
if options.bPairDiff                            % for spec-spec diffusion
    Csl = intel.Csl;
else                                            % for spec-mix diffusion
    Cs = intel.Cs;
end
if options.marchYesNo                                           % if we march
    Mabl_s = intel.Mabl_s;
    dMabls_dg = intel.dMabls_dg;
    dMabls_ddfdeta = intel.dMabls_ddfdeta;
    dMabls_dyl = intel.dMabls_dyl;
    switch options.numberOfTemp
        case '1T'
            % nothing more needed
        case '2T'                                   % for two-temperatures
            dMabls_dtauv = intel.dMabls_dtauv;              % we need the variation of the source terms with tauv also
        otherwise
            error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
    end

    xi = intel.xi;
    if isempty(varargin)                                            % checking that the number of inputs is correct
        error('Errror: intelp is required');
    else
        intelp = varargin{1};
    end
    df_dxi = intel.df_dxi;                                          % streamwise variation of df_dxi at this station
    % obtaining finite-difference stencil
    ih = 1:size(intelp.f,2);                                         % index to be used when evaluating the c forcing vector
    FDc = intel.FDc_xi.';
end

% Determining where the BC is to be applied
for s=1:intel.mixCnst.N_spec-1                                  % looping species on which to apply the BC
    % IMPORTANT!! We only have to apply the condition on N_spec-1 species,
    % because the last one will be determined from the concentration
    % condition sum(ys)=1, which is already in the equation set
    Ntot = size(mat,1);                                             % total number of points
    N = options.N;                                                  % number of wall-normal points
    idxV = (3+options.Cooke+s)*N;                                   % position of species s
    % we want to liberate ys at the wall - ys will be the "3+s"-th (if
    % there is no k) or the "4+s"-th (if there is k) and N is the position
    % of the wall

    % computing forcing term
    a = f(end)*ys{s}(end);                                          % Self-similar forcing term
    if options.bPairDiff                                            % for spec-spec diffusion
        for l=1:intel.mixCnst.N_spec                                    % looping species
            a = a + Csl{s,l}(end)*dys_deta{l}(end)*zetas{l}(end)+Csl{s,l}(end)*dzetas_deta{l}(end)*ys{l}(end);
        end
    else                                                            % for spec-mix diffusion
        a = a + Cs{s}(end)*dys_deta{s}(end)*zetas{s}(end)+Cs{s}(end)*dzetas_deta{s}(end)*ys{s}(end);
    end
    if options.marchYesNo                                           % if we march
        b = 2*xi*df_dxi(end)*ys{s}(end) + sqrt(2*xi)*Mabl_s{s}(end);     % we obtain the streamwise-varying term
        c = 2*xi*ys{s}(end)* intelp.fh(end,ih) * FDc(ih+1);             % and the term corresponding to moving to the lhs the FD terms of the previous stations
    else                                                            % if we don't march
        b = 0;                                                          % both b and c are zero
        c = 0;
    end
    forcing(idxV) = -(a + b + c);                                   % building forcing term

    % computing matrix terms
    A = zeros(1,Ntot);                                              % allocating
    B = zeros(1,Ntot);
    Cc = zeros(1,Ntot);

    ivc = 1;                                                        % index counting variables (df_deta)
    ivc = ivc+1;                                                    % increasing var. counter (f)
    A(1,ivc*N) = ys{s}(end);                                        % term multiplying fh
    % DEVELOPERS NOTE: the equation is evaluated at the wall, which is why
    % we do ivc*N - we want every variable at the wall (Nth position)
    if options.Cooke                                                % for Cooke
        ivc=ivc+1;                                                      % increasing var. counter (k)
    end
    ivc=ivc+1;                                                      % increasing var. counter (g)
    if ismember(options.flow,{'TPGD','CNE'})                        % for multispecies
        for l=1:intel.mixCnst.N_spec                                    % looping species
            ivc=ivc+1;                                                      % increasing var. counter (yl)
            if options.bPairDiff                                            % spec-spec diffusion
                A(1,(ivc-1)*N+1:ivc*N) = (Csl{s,l}(end)*zetas{l}(end))*D1(end,:); % term multiplying dyhl_deta
                A(1,ivc*N) = A(1,ivc*N) + Csl{s,l}(end)*dzetas_deta{l}(end);      % term multiplying  yhl
            elseif l==s                                                     % spec-mix diffusion
                A(1,(ivc-1)*N+1:ivc*N) = (Cs{s}(end)*zetas{s}(end))*D1(end,:);    % term multiplying dyhs_deta
                A(1,ivc*N) = A(1,ivc*N) + Cs{s}(end)*dzetas_deta{s}(end);         % term multiplying  yhs
            end
            if l==s
                A(1,ivc*N)           = A(1,ivc*N) + f(end);                  % term multiplying yhs
            end
        end
    end

    if options.marchYesNo                                           % if we march
        ivc = 1;                                                        % index counting variables (df_deta)
        B(1,ivc*N) = sqrt(2*xi) * dMabls_ddfdeta{s}(end);               % term multiplying dfh_deta
        ivc = ivc+1;                                                    % increasing var. counter (f)
        Cc(1,ivc*N) = 2*xi*ys{s}(end)*FDc(1);                           % term multiplying f (equivalent to d/dxi derivative)
        if options.Cooke                                                % for Cooke
            ivc=ivc+1;                                                      % increasing var. counter (k)
        end
        ivc=ivc+1;                                                      % increasing var. counter (g)
        B(1,ivc*N) = sqrt(2*xi) * dMabls_dg{s}(end);                    % term multiplying gh
        if ismember(options.flow,{'TPGD','CNE'})                        % for multispecies
            for l=1:intel.mixCnst.N_spec                                    % looping species
                ivc=ivc+1;                                                      % increasing var. counter (yl)
                B(1,ivc*N) = sqrt(2*xi) * dMabls_dyl{s,l}(end);                 % term multiplying yhl
                if s==l
                    B(1,ivc*N) = B(1,ivc*N) + 2*xi*df_dxi(end);                     % term multiplying yhs
                end
            end
        end
        switch options.numberOfTemp
            case '1T'
                % nothing to add
            case '2T'                                                           % for two-temperatures
                ivc=ivc+1;                                                          % increasing var. counter (tauv)
                B(1,ivc*N) = sqrt(2*xi) * dMabls_dtauv{s}(end);                       % term multiplying tauvh
            otherwise
                error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
        end
    end
    mat(idxV,:) = A+B+Cc;                                           % populating system matrix
end
