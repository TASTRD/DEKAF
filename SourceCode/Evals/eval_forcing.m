function [a_forc,b_forc,c_forc] = eval_forcing(varargin)
% EVAL_FORCING(intel,intelp,options) computes the forcing vector in the
% linearised matrix system, by evaluating the flow equations on the
% base-flow quantities.
%
% The forcing term will also be divided into the three contributions
% made for the matrices as done in build_mat.m
%  - a without marching terms (varying in xi)
%  - b: the one with marching terms
%  - c: the one containing the terms resulting from the evaluation of the
%  xi derivatives of the fluctuating variables
%
% The function can be evaluated also on the complete boundary-layer
% xi-varying flowfield. For this, options.globalEval must be set to true.
% With this option, the c vector will be simply a column of zeros.
%
% For a detailed description on the inputs and available options see also
% setDefaults, ListOfVariablesExplained, build_mat, eval_fullFlowField
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

if nargin==3 % we must extract everything from intel
    intel = varargin{1};
    intelp = varargin{2};
    options = varargin{3};

    % constants
    if options.marchYesNo
        xi      = intel.xi;
    else
        xi = 0; % place holder
    end

    %% Extract variables from structures
    % options and constants
    theta   = intel.theta;
    Theta   = intel.Theta;
    beta    = intel.beta;
    if ismember(options.flow,{'TPGD','CNE'}) % setting up booleans for the multi-species case
        MultiSpec = true;
        N_spec  = options.N_spec;
        MultiElem = false;
        N_elem = 0;
        if options.bVariableIdxBath
            [~,idx_bath] = max(cell1D2matrix2D(intel.ys),[],2);
            idx_bath = reshape(idx_bath,size(intel.f));
        else
            idx_bath = intel.mixCnst.idx_bath * ones(size(intel.f));
        end
    elseif ismember(options.flow,{'LTEED'})
        MultiSpec = false;
        N_spec = 0;
        MultiElem = true;
        N_elem = intel.mixCnst.N_elem;
        idx_bath = intel.mixCnst.idx_bathElement;
    else
        MultiSpec = false;
        N_spec = 0;
        MultiElem = false;
        N_elem = 0;
    end

    % flow properties
    a0 = intel.a0;
    a1 = intel.a1;
    da1_deta = intel.da1_deta;
    a2 = intel.a2;
    da2_deta = intel.da2_deta;
    C = intel.C;
    dC_deta = intel.dC_deta;
    jj = intel.j;  % we avoid using j, since matlab understands it as the complex unity

    if MultiSpec
        zetas = intel.zetas;
        dzetas_deta = intel.dzetas_deta;
        dzetas_deta2 = intel.dzetas_deta2;
        as1 = intel.as1;
        das1_deta = intel.das1_deta;
        if options.bPairDiff    % we will have asl2 and Csl
            asl2 = intel.asl2;
            dasl2_deta = intel.dasl2_deta;
            Csl = intel.Csl;
            dCsl_deta = intel.dCsl_deta;
        else                    % we will have as2 and Cs
            as2 = intel.as2;
            das2_deta = intel.das2_deta;
            Cs = intel.Cs;
            dCs_deta = intel.dCs_deta;
        end
        Omegas = intel.Omegas;
        %dOmegas_ddfdeta = intel.dOmegas_ddfdeta;
        %dOmegas_dg = intel.dOmegas_dg; % derivatives of the source term with the enthalpy (obtained using chain rule a million times)
        %dOmegas_dyl = intel.dOmegas_dyl; % jacobian of the source term with the species concentrations
    elseif MultiElem
        CYEF        = intel.CYEF;
        agE         = intel.agE;
        auE         = intel.auE;
        aYE         = intel.aYE;
        dCYEF_deta  = intel.dCYEF_deta;
        dagE_deta   = intel.dagE_deta;
        dauE_deta   = intel.dauE_deta;
        daYE_deta   = intel.daYE_deta;
    end

    f          = intel.f;
    df_deta    = intel.df_deta;
    df_deta2   = intel.df_deta2;
    df_deta3   = intel.df_deta3;
    g          = intel.g;
    dg_deta    = intel.dg_deta;
    dg_deta2   = intel.dg_deta2;

    dk_deta    = intel.dk_deta;
    dk_deta2   = intel.dk_deta2;

    if MultiSpec
        ys         = intel.ys; 
        dys_deta   = intel.dys_deta;
        dys_deta2  = intel.dys_deta2; 
    elseif MultiElem 
        yE          = intel.yE;
        dyE_deta    = intel.dyE_deta;
        dyE_deta2   = intel.dyE_deta2;
    end

    if options.marchYesNo
        df_dxi      = intel.df_dxi;
        dg_dxi      = intel.dg_dxi;
        df_detadxi  = intel.df_detadxi;
        dk_dxi      = intel.dk_dxi;
        if MultiSpec
            dys_dxi     = intel.dys_dxi;
        elseif MultiElem        % we define ys to yE, in order to share some common code
            dyE_dxi     = intel.dyE_dxi;
        end
    end

    if strcmp(options.numberOfTemp,'2T')
        bElectron = intel.mixCnst.bElectron;
        Ecu = intel.Ecu;
        Ecp = intel.Ecp;

        av1 = intel.av1;
        dav1_deta = intel.dav1_deta;
        av2 = intel.av2;
        dav2_deta = intel.dav2_deta;
        Cpv = intel.Cpv;
        OmegaV = intel.OmegaV;

        tauv          = intel.tauv;
        dtauv_deta    = intel.dtauv_deta;
        dtauv_deta2   = intel.dtauv_deta2;
        if options.marchYesNo
            dtauv_dxi      = intel.dtauv_dxi;
        end
        if MultiSpec
            if options.bPairDiff    % we will have asl2 and Csl
                avsl2 = intel.avsl2;
            else                    % we will have as2 and Cs
                avs2 = intel.avs2;
            end
        end
    end
    if ismember(options.modelTurbulence,{'SSTkomega'})
        KT          = intel.KT;
        dKT_deta    = intel.dKT_deta;
        dKT_deta2   = intel.dKT_deta2;
        WT          = intel.WT;
        dWT_deta    = intel.dWT_deta;
        dWT_deta2   = intel.dWT_deta2;
        AK          = intel.AK;
        AW          = intel.AW;
        BW          = intel.BW;
        ThetaK      = intel.ThetaK;
        ThetaW      = intel.ThetaW;
        aK0         = intel.aK0;
        aK1         = intel.aK1;
        aK2         = intel.aK2;
        daK2_deta   = intel.daK2_deta;
        aW0         = intel.aW0;
        aW1         = intel.aW1;
        aW2         = intel.aW2;
        daW2_deta   = intel.daW2_deta;
        if options.marchYesNo
            dKT_dxi = intel.dKT_dxi;
            dWT_dxi = intel.dWT_dxi;
        end
    end
else                                % They were all input separately
    error('separate inputs are not ready');
end
N = options.N;
N_xi = size(xi,2);
oo = zeros(N,N_xi);

if ~options.globalEval && options.marchYesNo % This is only necessary if we are evaluating the rhs in the matrix system. If we
    % are evaluating globally (on the entire field), then we don't need the c terms.
    % previous xi locations to be used in the evaluation of the xi derivatives
    % (depending on the order of the FD method in the xi direction)
    %ih = 2:options.ox+1;
    ih = 1:size(intelp.f,2);
    FDc = intelp.FDc; % extracting FD stencil

    dfh_dxi     = intelp.fh(:,ih)       * FDc(ih+1);
    dfh_detadxi = intelp.dfh_deta(:,ih) * FDc(ih+1);
    dkh_dxi     = intelp.kh(:,ih)       * FDc(ih+1);
    dgh_dxi     = intelp.gh(:,ih)       * FDc(ih+1);
    if MultiSpec
        for s=N_spec:-1:1
            dysh_dxi{s} = intelp.ysh{s}(:,ih) * FDc(ih+1);
        end
    elseif MultiElem         % we define ys to yE, in order to share some common code
        for s=N_elem:-1:1
            dyEh_dxi{s} = intelp.yEh{s}(:,ih) * FDc(ih+1);
        end
    end
    if strcmp(options.numberOfTemp,'2T')
        dtauvh_dxi  = intelp.tauvh(:,ih)  * FDc(ih+1);
    end
    if ismember(options.modelTurbulence,{'SSTkomega'})
        dKTh_dxi = intelp.KTh(:,ih) * FDc(ih+1);
        dWTh_dxi = intelp.WTh(:,ih) * FDc(ih+1);
    end
end

% We express the forcing terms as they appear in the base-state equation.
% When using them as a forcing vector in the fluctuation variable system,
% they must have opposite sign to what is expressed here.
a_1 = df_deta2.*dC_deta+df_deta3.*C+beta.*jj+df_deta2.*f-beta.*df_deta.^2 ;
if options.marchYesNo
    b_1 = 2*xi.*df_deta2.*df_dxi-2*xi.*df_deta.*df_detadxi;
    if ~options.globalEval
        c_1 = 2*xi.*(df_deta2.*dfh_dxi - df_deta.*dfh_detadxi);
    end
end
if options.Cooke
    a_3 = dk_deta.*dC_deta+dk_deta2.*C+dk_deta.*f ;
    if options.marchYesNo
        b_3 = 2*xi.*df_dxi.*dk_deta-2*xi.*df_deta.*dk_dxi;
        if ~options.globalEval
            c_3 = 2*xi.*(dk_deta.*dfh_dxi - df_deta.*dkh_dxi);
        end
    end
end

a_4 = -df_deta.*g.*Theta+dg_deta.*f+a0.*dk_deta.^2+a1.*dg_deta2+da1_deta.*dg_deta+a2.*df_deta.*df_deta3+a2.*df_deta2.^2+da2_deta.*df_deta.*df_deta2;
if options.marchYesNo
    b_4 = 2*xi.*df_dxi.*dg_deta-2*xi.*df_deta.*dg_dxi;
    if ~options.globalEval
        c_4 = 2*xi.*(dg_deta.*dfh_dxi - df_deta.*dgh_dxi);
    end
end


if strcmp(options.numberOfTemp,'2T') % two-temperature model, we need the vibrational energy equation
    a_v = (-Cpv.*df_deta.*tauv.*theta)+Cpv.*dtauv_deta.*f+av1.*dtauv_deta2+dav1_deta.*dtauv_deta;
    if options.marchYesNo
        b_v = xi.*((-2.*Cpv.*df_deta.*dtauv_dxi)+2.*Cpv.*df_dxi.*dtauv_deta+2.*OmegaV); % term multiplying xi
        if ~options.globalEval
%             c_v =  (2.*Cpv.*dfh_dxi.*dtauv_deta-2.*Cpv.*df_deta.*dtauvh_dxi).*xi;
            c_v = (-2.*Cpv.*df_deta.*dtauvh_dxi+2.*Cpv.*dfh_dxi.*dtauv_deta).*xi;
        end
    end
    a_4 = a_4 + av2.*dtauv_deta2+dav2_deta.*dtauv_deta; % we must also add the vibrational-specific terms to the energy equation
end
%{%
if MultiSpec
    a_s = zeros(N_spec*N,N_xi); % forcing vectors for the N_spec species equations
    if options.marchYesNo
        b_s = zeros(N_spec*N,N_xi);
        c_s = zeros(N_spec*N,N_xi);
    end
    if options.bPairDiff % spec-spec diffusion
         for s=1:N_spec % looping species
            a_4 = a_4 - as1{s}.*dys_deta2{s}-das1_deta{s}.*dys_deta{s}; % energy equation
            for l=1:N_spec % looping species
                % adding terms with double index (s,l)
                a_4 = a_4 + (asl2{s,l}.*dys_deta2{l}+dasl2_deta{s,l}.*dys_deta{l}).*zetas{l}+(asl2{s,l}.*dzetas_deta2{l}+dasl2_deta{s,l}.*dzetas_deta{l}).*ys{l}+2.*asl2{s,l}.*dys_deta{l}.*dzetas_deta{l};
            end
            idx_s = (s-1)*N+1 : s*N;
            a_s(idx_s,:) = dys_deta{s}.*f;
            if options.marchYesNo
                b_s(idx_s,:) = 2*xi.*Omegas{s}-2*xi.*df_deta.*dys_dxi{s}+2*xi.*df_dxi.*dys_deta{s};
                if ~options.globalEval
                    c_s(idx_s,:) = 2*xi.*(dys_deta{s}.*dfh_dxi - df_deta .*dysh_dxi{s});
                end
            end
            for l=1:N_spec % looping species
                a_s(idx_s,:) = a_s(idx_s,:) + (dys_deta{l}.*zetas{l}+dzetas_deta{l}.*ys{l}).*dCsl_deta{s,l}+Csl{s,l}.*dys_deta2{l}.*zetas{l}+Csl{s,l}.*dzetas_deta2{l}.*ys{l}+2.*Csl{s,l}.*dys_deta{l}.*dzetas_deta{l};
            end
        end
        if strcmp(options.numberOfTemp,'2T')
            for s=1:N_spec % looping species
                a_v = a_v + ((-Ecu.*bElectron(s).*beta.*df_deta.*jj.*ys{s})-Ecp.*bElectron(s).*dys_deta{s}.*f.*jj).*zetas{s}-Ecp.*bElectron(s).*dzetas_deta{s}.*f.*jj.*ys{s};
                for l=1:N_spec % looping species
                    a_v = a_v + avsl2{s,l}.*dtauv_deta.*dys_deta{l}.*zetas{l}+avsl2{s,l}.*dtauv_deta.*dzetas_deta{l}.*ys{l};
                end
            end
        end
    else % spec-mix diffusion
        for s=1:N_spec % including diffusion terms into the energy equation
            a_4 = a_4 + (as2{s}.*dys_deta2{s}+das2_deta{s}.*dys_deta{s}).*zetas{s}+(as2{s}.*dzetas_deta2{s}+das2_deta{s}.*dzetas_deta{s}).*ys{s}+2.*as2{s}.*dys_deta{s}.*dzetas_deta{s}-as1{s}.*dys_deta2{s}-das1_deta{s}.*dys_deta{s};
            idx_s = (s-1)*N+1 : s*N;
            a_s(idx_s,:) = (dys_deta{s}.*zetas{s}+dzetas_deta{s}.*ys{s}).*dCs_deta{s}+Cs{s}.*dys_deta2{s}.*zetas{s}+Cs{s}.*dzetas_deta2{s}.*ys{s}+dys_deta{s}.*f+2.*Cs{s}.*dys_deta{s}.*dzetas_deta{s};
            if options.marchYesNo
                b_s(idx_s,:) = 2*xi.*Omegas{s}-2*xi.*df_deta.*dys_dxi{s}+2*xi.*df_dxi.*dys_deta{s};

                if ~options.globalEval
                    c_s(idx_s,:) = 2*xi.*(dys_deta{s}.*dfh_dxi - df_deta    .*dysh_dxi{s});
                end
            end
            if strcmp(options.numberOfTemp,'2T') % we add the species-specific terms for the vibrational equation
                a_v = a_v + ((-Ecu.*bElectron(s).*beta.*df_deta.*jj.*ys{s})-Ecp.*bElectron(s).*dys_deta{s}.*f.*jj+avs2{s}.*dtauv_deta.*dys_deta{s}).*zetas{s}+(avs2{s}.*dtauv_deta.*dzetas_deta{s}-Ecp.*bElectron(s).*dzetas_deta{s}.*f.*jj).*ys{s};
            end
        end
    end
    if options.marchYesNo && ~options.globalEval && strcmp(options.numberOfTemp,'2T')
        for s=1:N_spec
            c_v = c_v + (2.*Ecp.*bElectron(s).*df_deta.*dysh_dxi{s}-2.*Ecp.*bElectron(s).*dfh_dxi.*dys_deta{s}).*jj.*xi.*zetas{s}-2.*Ecp.*bElectron(s).*dfh_dxi.*dzetas_deta{s}.*jj.*xi.*ys{s};
        end
    end

    % Building vectors corresponding to the CoNcentration Condition on the
    % species mass fractions and gradients (sum(Ys) = 1)
    a_cnc = zeros(N,N_xi);
    if options.marchYesNo
        b_cnc = zeros(N,N_xi);
        c_cnc = zeros(N,N_xi);
    end
    for s=1:N_spec
        a_cnc(1:N,:) = a_cnc(1:N,:) + ys{s};
    end
    a_cnc(1:N,:) = a_cnc(1:N,:) - ones(N,N_xi);
elseif MultiElem
    a_s = zeros(N_elem*N,N_xi); % forcing vectors for the N_elem-1 elemental equations
    if options.marchYesNo
        b_s = zeros(N_elem*N,N_xi);
        c_s = zeros(N_elem*N,N_xi);
    end
    for E=1:N_elem % looping elements
        a_4 = a_4 + aYE{E}.*dyE_deta2{E}+daYE_deta{E}.*dyE_deta{E};
        idx_E = (E-1)*N+1 : E*N; % positions spanning element E
        a_s(idx_E,:) = dyE_deta{E}.*f + agE{E}.*dg_deta2+dagE_deta{E}.*dg_deta-auE{E}.*df_deta.*df_deta3-auE{E}.*df_deta2.^2-dauE_deta{E}.*df_deta.*df_deta2;
        for F=1:N_elem % looping elements
            a_s(idx_E,:) = a_s(idx_E,:) + CYEF{E,F}.*dyE_deta2{F}+dCYEF_deta{E,F}.*dyE_deta{F};
        end
        if options.marchYesNo
            b_s(idx_E,:) = (2.*df_dxi.*dyE_deta{E}-2.*df_deta.*dyE_dxi{E}).*xi; % term multiplying xi
            if ~options.globalEval
                c_s(idx_E,:) = 2*xi.*(dyE_deta{E}.*dfh_dxi - df_deta.*dyEh_dxi{E});
            end
        end
    end

    % Building vectors corresponding to the CoNcentration Condition on the
    % elemental mass fractions and gradients (sum(yE) = 1)
    a_cnc = zeros(N,N_xi);
    if options.marchYesNo
        b_cnc = zeros(N,N_xi);
        c_cnc = zeros(N,N_xi);
    end
    for E=1:N_elem
        a_cnc(1:N,:) = a_cnc(1:N,:) + yE{E};
    end
    a_cnc(1:N,:) = a_cnc(1:N,:) - ones(N,N_xi);
    N_spec = N_elem;
end
if ismember(options.modelTurbulence,{'SSTkomega'})
    a_T1 = dKT_deta.*f+aK0.*dk_deta.^2+aK1.*df_deta2.^2-KT.*ThetaK.*df_deta+dKT_deta.*daK2_deta+aK2.*dKT_deta2;
    a_T2 = dWT_deta.*f+aW0.*dk_deta.^2+aW1.*df_deta2.^2-ThetaW.*WT.*df_deta+dWT_deta.*daW2_deta+aW2.*dWT_deta2+BW.*dKT_deta.*dWT_deta;
    if options.marchYesNo
        b_T1 = 2.*xi.*(dKT_deta.*df_dxi-dKT_dxi.*df_deta-AK.*KT.*WT);
        b_T2 = 2.*xi.*(dWT_deta.*df_dxi-dWT_dxi.*df_deta-AW.*WT.^2);
        if ~options.globalEval
            c_T1 = 2.*xi.*(dKT_deta.*dfh_dxi-dKTh_dxi.*df_deta);
            c_T2 = 2.*xi.*(dWT_deta.*dfh_dxi-dWTh_dxi.*df_deta);
        end
    end
end
%}
% Assembling
if options.Cooke
    a_forc = [a_1 ; oo ; a_3 ; a_4 ];
    if options.marchYesNo
        b_forc = [b_1 ; oo ; b_3 ; b_4 ];
        if ~options.globalEval
            c_forc = [c_1 ; oo ; c_3 ; c_4 ];
        end
    end
    N_var = 4;
else
    a_forc = [a_1 ; oo ; a_4 ];
    if options.marchYesNo
        b_forc = [b_1 ; oo ; b_4];
        if ~options.globalEval
            c_forc = [c_1 ; oo ; c_4];
        end
    end
    N_var = 3;
end
if ismember(options.modelTurbulence,{'SSTkomega'})
    a_forc = [a_forc ; a_T1 ; a_T2];
    if options.marchYesNo
        b_forc = [b_forc ; b_T1 ; b_T2];
        if ~options.globalEval
            c_forc = [c_forc ; c_T1 ; c_T2];
        end
    end
    N_var = N_var + 2;
end

if MultiSpec||MultiElem % appending the multi-species forcing term
    idxBath_rows = (N_var+idx_bath-1)*N + repmat((1:N).',[1,N_xi]);
    idx_cols = repmat(1:N_xi,[N,1]);
    idx2D = sub2ind([(N_var+N_spec)*N,N_xi],idxBath_rows,idx_cols);
    a_forc = [a_forc ; a_s];
    a_forc(idx2D) = a_cnc;
    if options.marchYesNo
        b_forc = [b_forc ; b_s];
        b_forc(idx2D) = b_cnc;
        if ~options.globalEval
            c_forc = [c_forc ; c_s];
            c_forc(idx2D) = c_cnc;
        end
    end
end

if strcmp(options.numberOfTemp,'2T')
    a_forc = [a_forc ; a_v];
    if options.marchYesNo
        b_forc = [b_forc ; b_v];
        if ~options.globalEval
            c_forc = [c_forc ; c_v];
        end
    end
end

if ~options.marchYesNo
    b_forc = zeros(size(a_forc));
    c_forc = zeros(size(a_forc));
elseif options.globalEval
    c_forc = zeros(size(a_forc));
end