function [conv,intel] = eval_norm(intel,options)
% EVAL_NORM(intel,options) makes an assessment of the convergence of the
% numerical method, by calculating the norm of a series of quantities
% specified by the user through options.variables4conv. It needs also an
% additional fields NindVariables4conv with the number of indices that the
% particular variable has (species quantities could have several indices:
% ys{1}, ys{2}, etc.).
%
% it puts the results into the field norms in intel, which will have
% subfieldnames corresponding to the name of the particular variable.
%
% The function also allows to check on the total residual of the equations
% themselves, called as:
%
% momXeq, momZeq, enereq and speceq
%
% The evaluation of equations requires a reference value of the equation
% norm, such that its relative convergence can be properly assessed. This
% reference value is stored in norms_ref, another structure inside intel
% with the same fieldnames as norms. Field quantities don't require a
% reference, since the convergence is assessed with respect to the norm
% infinity of the field at the previous iteration, normalized with respect
% to the field's maximum value.
%
% the norms structure inside intel must also have another structure "old"
% containing the profiles from the previous iteration in the same way that
% they are now saved in intel.
%
% defaults are defined in setDefaults
%
% See also: setDefaults, variableDatabase
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

% extracting fields to be considered for convergence structure
Fields4conv = options.Fields4conv; % variables to study convergence
NindFields4conv = options.NindFields4conv; % number of indices each variable has (like ys{1}, ys{2} etc.)
it_run = intel.it; % current iteration in the eta looping
NFields = length(Fields4conv); % number fo fields to be used for the evaluation of the convergence
NFieldsTot = sum(NindFields4conv); % total number of fields
norms = intel.norms;
norms_ref = intel.norms_ref; % reference norms for the equation convergence study

% obtaining those fields representing equations
eqBool = ~cellfun(@isempty,regexp(Fields4conv,'eq$','match')); % vector of boolean with the locations that match eq at the end
% REGEXP NOTE: we are looking for the locations in vars4conv where the
% variable name has eq at the end, identifying an equation.

% defining how the error should be evaluated
errType = 'diff_weight';

% initialising where all the norms will be listed and connecting varnames
% and eqnames with all_normDiff. This way we can track down where each
% variable goes in all_normDiff, and viceversa.
all_normDiff = ones(NFieldsTot,1); % we initialize it with ones to be sure that we don't exit on the first iteration (having all norms=0)
allnormsIn_vars = zeros(NFieldsTot,1); % what variable all coefficients in all_normDiff correspond to
for ii=1:length(NindFields4conv)%NFieldsTot
    current_pos_abs = sum(NindFields4conv(1:(ii-1))); % how many positions before this one are already occupied in all_normDiff
    allnormsIn_vars(current_pos_abs+1 : current_pos_abs+NindFields4conv(ii)) = ii; % this will tell us to what variable in varnames each position in all_normDiff corresponds
end

% initial check on the variables to make sure that there isn't any that has
% very low values, thus giving a fake bad numerical convergence. This comes
% from the fact that convergence is studied on the norm of the difference
% between the fields, normalized with the maximum value of the field. If
% this maximum value is close to zero, the normalization will render a very
% high value which is not representative of the system's overall
% convergence.
consider_normDiff = ones(NFieldsTot,1);
for ii=1:length(Fields4conv)
    posField = sum(NindFields4conv(1:(ii-1))); % where the current field is placed inside all_normDiff
    if NindFields4conv(ii)==1 % it only has one index
        if ~eqBool(ii) && (norm(intel.(Fields4conv{ii}),inf) < options.variableNormMinimum)
            consider_normDiff(posField+1) = false;
        end
    else % it has more than one index
        for s=1:NindFields4conv(ii)
            if ~eqBool(ii) && (norm(intel.(Fields4conv{ii}){s},inf) < options.variableNormMinimum)
                consider_normDiff(posField+s) = false;
            end
        end
    end
end

%% Obtaining norms
for ii=1:NFields % looping fields of interest
    posField = sum(NindFields4conv(1:(ii-1))); % where the current field is placed inside all_normDiff
    if eqBool(ii) % we have an equation
        if ~isfield(norms_ref,Fields4conv{ii}) % if the chosen norm has not been saved
            if NindFields4conv(ii)==1 % there is only one index for the equation (like for momXeq)
                % Dodge a division by zero when calculating `norms.(Fields4conv{ii})(it_run)`
                % ...just in case our initial guess is exact
                if norms.res.(Fields4conv{ii}) == 0
                    norms_ref.(Fields4conv{ii}) = 1;
                else
                    norms_ref.(Fields4conv{ii}) = norms.res.(Fields4conv{ii});
                end
                norms.(Fields4conv{ii})(2) = 1;
                all_normDiff(posField+1) = 1;
            else % we have more than one index for the equation (like for speceq)
                for s=1:NindFields4conv(ii)
                    % Again, dodge a division by zero as stated above
                    if norms.res.(Fields4conv{ii}){s} == 0
                        norms_ref.(Fields4conv{ii}){s} = 1;
                    else
                        norms_ref.(Fields4conv{ii}){s} = norms.res.(Fields4conv{ii}){s};
                    end
                    norms.(Fields4conv{ii}){s}(2) = 1;
                    all_normDiff(posField+s) = 1;
                end
            end
        else % we store the relative residual
            if NindFields4conv(ii)==1 % there is only one index for the equation (like for momXeq)
                norms.(Fields4conv{ii})(it_run) = norms.res.(Fields4conv{ii}) / norms_ref.(Fields4conv{ii});
                all_normDiff(posField+1) = norms.(Fields4conv{ii})(it_run);
            else % we have more than one index for the equation (like for speceq)
                for s=1:NindFields4conv(ii)
                    norms.(Fields4conv{ii}){s}(it_run) = norms.res.(Fields4conv{ii}){s} / norms_ref.(Fields4conv{ii}){s};
                    all_normDiff(posField+s) = norms.(Fields4conv{ii}){s}(it_run);
                end
            end
        end
    else % we have a variable
        if it_run~=2 % then we can compute the difference
            if NindFields4conv(ii)==1 % there is only one index for the equation (like for momXeq)
                norms.(Fields4conv{ii})(it_run) = calcError(intel.(Fields4conv{ii}),norms.old.(Fields4conv{ii}),errType);
                all_normDiff(posField+1) = norms.(Fields4conv{ii})(it_run);
            else % we have more than one index
                for s=1:NindFields4conv(ii)
                    norms.(Fields4conv{ii}){s}(it_run) = calcError(intel.(Fields4conv{ii}){s},norms.old.(Fields4conv{ii}){s},errType);
                    all_normDiff(posField+s) = norms.(Fields4conv{ii}){s}(it_run);
                end
            end
        end
        norms.old.(Fields4conv{ii}) = intel.(Fields4conv{ii}); % storing for the next run
    end
end

% Eliminating small values and obtaining maximum of all norms - convergence
% criterion
all_normDiff(~consider_normDiff) = 0; % by making it zero we avoid it being "artificially" the driver of the convergence criterion
[conv,id] = max(all_normDiff);

if options.display && ~options.quiet
    if options.perturbRelaxation==0;    str_perturbRelaxation = [' (dynamic relax. fact. ',num2str(intel.dynamicRelaxFact),')']; % dynamic relaxation factor, which we want to mention
    else;                               str_perturbRelaxation ='';
    end
    disp(['--- Iteration ',num2str(it_run-1),...
        ', with error ',num2str(conv,'%.2e'),...
        ', due to ',Fields4conv{allnormsIn_vars(id)},str_perturbRelaxation]);
end

% saving for the next run
intel.norms = norms;
intel.norms_ref = norms_ref;

end % eval_norm.m
