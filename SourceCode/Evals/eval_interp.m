function [intel,newgrid] = eval_interp(intel,options)
% note: intel.y from eval_dimVars is dimensional, but does not correspond
% to grid spacing and differentiation matrices that the Malik mapping can
% produce on its own, therefore the mapping of the eta-nodes has to be
% adapted to correspond to a Malik-mapping in the y-coordinate (through
% rho)
% Author(s):    Ethan Beyak
%               Koen Groot

newgrid = struct;

if isfield(options,'mapit') && options.mapit == 1
    % reuse y_i and y_max from the self-similar solution
    y_i   = intel.y_i; % calculated in eval_dimVars
    y_max = intel.y(1);
    disp('--------------------------------------------------------------------------')
    disp(['--- Interpolation y_i and y_max values [m]: ',num2str(y_i),' and: ',num2str(y_max)])
end

if isfield(options,'y_max')
    % if y_max is set externally, use that for the new grid
    newgrid.y_max = options.y_max;
else
    % otherwise reuse the calculated y_max
    newgrid.y_max = y_max;
end
if isfield(options,'y_i')
    newgrid.y_i = options.y_i;
else
    newgrid.y_i = y_i;
end

% build Malik-mapping for y-coordinates
[newgrid.y,D1y,D2y,eta_star] =  build_mapping_vars(newgrid.y_max,newgrid.y_i,options.N,options.chebydist);
newgrid.D1 = D1y;
newgrid.D2 = D2y;

interp.f        = chebinterp(intel.f,eta_star);
interp.u        = chebinterp(intel.u,eta_star);
interp.du_dy    = chebinterp(intel.du_dy,eta_star);

figure
plot(D1y*intel.u,newgrid.y,'o'); hold on,
plot(intel.du_dy,intel.y,'x')

% figure
% plot(D1y*intel.v,newgrid.y,'o'); hold on,
% plot(intel.dv_dy,intel.y,'x')
%
figure
plot(D1y*intel.w,newgrid.y,'o'); hold on,
plot(intel.dw_dy,intel.y,'x')

% eta_star is eta_star2
% newgrid.y is eta_2
% eta_1 is implicitly contained in the functions from dimVars (u, f, v, etc etc)
% interpolate all solution factos that come out of dimVars
% good check: differentiate u,v,w with D1y, D2y

end % eval_interp.m
