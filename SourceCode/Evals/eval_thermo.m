function [intel,options] = eval_thermo(intel,options)
% EVAL_THERMO(intel,options) obtains the coefficients necessary for the evaluation
% of the boundary-layer equations
%
% IMPORTANT: if the composition at the wall changes and a fixed dimensional
% value of the temperature at the wall was chosen by the user, then this
% function will also update the g, dg_deta and dg_deta2 vectors.
%
% Intel should contain:
% --> H_e, h_e, T_e, mu_e, rho_e, U_e, N_spec, mixCnst, D1, D2, g, dg_deta.
%   dg_deta2, df_deta, df_deta2, ys or ys_e (depending on options.flow),
%   p_e, and the necessary coefficients to evaluate the properties
%   according to the chosen options.
%
% Options required for this function:
% --> N, flow, T_tol, T_itMax, modelTransport, cstPr, modelDiffusion,
% bPairDiff.
%
% the options structure is also outputted, updating the value of
% options.G_bc if options.thermal_BC was chosen together with a
% chemically-reacting flow assumption.
%
% For a detailed description of the available options and their defaults
% and the variables used see also listOfVariablesExplained, setDefaults
%
% DEVELOPERS NOTE: expandBaseFlow is no longer supported, which is why
% there are a bunch of temperature derivatives that are commented out
%
% Other documentation: update_g_isoThermalWall
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Managing inputs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Extracting required quantities
if options.marchYesNo   % checking what xi location we're at
    i_xi = intel.i_xi;
else                    % if we are not marching, we fix this to one
    i_xi = 1;
end
%   extracting edge values and options
H_e         = intel.H_e;
h_e         = intel.h_e;
T_e         = intel.T_e;
mu_e        = intel.mu_e;
rho_e       = intel.rho_e;
U_e         = intel.U_e;
W_0         = intel.W_0;
Ecu         = intel.Ecu;
Ecw         = intel.Ecw;
mixCnst     = intel.mixCnst;
N_spec      = mixCnst.N_spec;
N_eta       = options.N;
D1          = intel.D1;
D2          = intel.D2;
%   extracting flow fields
g           = intel.g;
dg_deta     = intel.dg_deta;
dg_deta2    = intel.dg_deta2;
df_deta     = intel.df_deta;
df_deta2    = intel.df_deta2;
df_deta3    = intel.df_deta3;
switch options.numberOfTemp
    case '1T'
        Tv_e    = intel.T_e;
    case '2T'
        Tv_e    = intel.Tv_e;
        tauv    = intel.tauv;
    otherwise
        error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
end
switch options.modelTurbulence
    case {'none','SmithCebeci1967'}         % no additional arrangements needed
    case 'SSTkomega'
        wT_e = intel.wT_e;                        % reference turbulent vorticity
        kT_e = intel.kT_e;                    % reference turbulent kinetic energy
        kT        = intel.KT        * kT_e;
        dkT_deta  = intel.dKT_deta  * kT_e;
        wT        = intel.WT        * wT_e;
        dwT_deta  = intel.dWT_deta  * wT_e;
        dwT_deta2 = intel.dWT_deta2 * wT_e;
    otherwise
        error(['the chosen modelTurbulence ''',options.modelTurbulence,''' is not supported']);
end
switch options.flow     % depending on the flow assumption we will get ys from different places
    case {'CPG','TPG'}      % for CPG and TPG they are fixed and equal to the edge fractions
        ys = cell(N_spec,1);
        for s=1:N_spec
            ys{s}       = ones(N_eta,1) * intel.ys_e{s};
        end
        XY_s = cell1D2matrix2D(ys);
    case 'LTE'              % for LTE they are obtained from the equilibrium conditions
        XY_s = [];               % placeholder
    case 'LTEED'            % for LTEED they are obtained from the equilibrium conditions, but getAll_properties needs the appropriate inputs
        y_E = cell1D2matrix2D(intel.yE);                        % elemental mass fractions
        MM  = getMixture_Mm_R(y_E,[],[],mixCnst.Mm_E,[],[]);    % mixture atomic weight
        XY_s = getSpecies_X(y_E,MM,mixCnst.Mm_E);                % this is actually the elemental mole fractions (needed for getAll_properties)
    case {'TPGD','CNE'}     % for multi-species assumptions they come from the system mass fractions
        ys = intel.ys;
        XY_s = cell1D2matrix2D(ys);
    otherwise
        error(['the ''',options.flow,''' flow assumption is not a valid option.']);
end
% Pressure
p_e = intel.p_e;
p = ones(N_eta,1) * p_e;   % vector of pressure (constant in the boundary layer)

% Setting useful booleans
bMultiSpec = ismember(options.flow,{'TPGD','CNE'});
bMultiElem = ismember(options.flow,{'LTEED'});

% obtaining static enthalpy
h = getEnthalpy_semitotal2static(H_e*g,H_e*dg_deta,H_e*dg_deta2,U_e*df_deta,U_e*df_deta2,U_e*df_deta3);

switch options.numberOfTemp
    case '1T'
        N_T = 1;
        Tv = NaN*h; % placeholder
    case '2T'
        N_T = 2;
        Tv = tauv*Tv_e; % vibrational enthalpy
    otherwise
        error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Making arrangement and computing properties
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if isfield(intel,'Tguess');  Tguess = intel.Tguess;     % used for the NR iteration to get T from h
else;                        Tguess = [];               % if left empty, a CPG value will be taken
end
if isfield(intel,'XsGuessLTE'); XsGuessLTE = intel.XsGuessLTE;  % used for the NR iteration to solve the equilibrium system
else;                           XsGuessLTE = [];                % if left empty, tabled values are taken
end
switch options.flow                                     % Extra CPG inputs to getAll_properties
    case 'CPG';                                 cp_e = intel.cp_e;   R_e = intel.R_e;
    case {'TPG','TPGD','CNE','LTE','LTEED'};    cp_e = NaN;         R_e = NaN;      % placeholders
    otherwise;                          error(['the chosen flow assumption ''',options.flow,''' is not supported']);
end
switch options.modelDiffusion                           % Extra diffusion-model dependent inputs
    case 'cstSc';       mixCnst.Sc_e = intel.Sc_e;
    case 'cstLe';       mixCnst.Le_e = intel.Le_e;
end

[rho , mu , kappa , TTrans , TRot , TVib , TElec , Tel , h , cp , ~ , Mm , ~ , y_s , X_s , h_s , D_s , nTfrac_s , source_s , dsources_dT , dsources_dym , ...
    kappav , ~ , cptr , cpv , htr_s , hv_s , sourcev , dsourcev_dT , dsourcev_dym , dcpv_dTv , cpv_s , y_E , A_E , C_EF , dys_dyE , dXs_dyE] = ...
            getAll_properties(mixCnst,intel.eta,p,XY_s,h,Tv,options,cp_e,R_e,'passing_h',Tguess,'correctTBC',i_xi,'noOptionCheck','guessXsLTE',XsGuessLTE);
% NOTE: for LTEED we're actually passing X_E here /\

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Computing additional properties related to boundary conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Surface chemistry
if options.marchYesNo && (ismember(options.thermal_BC,    {'ablation'}) || ... % all ablative BCs need the wall source terms
                          ismember(options.wallBlowing_BC,{'ablation'}) || ...
                          ismember(options.wallYs_BC,     {'ablation'}))
    protectedvalue(options,'flow','CNE');
    protectedvalue(options,'EoS','idealGas');
    sourceGSs       = getSurfaceSpecies_sourceGS(         TTrans(end),Tel(end),p(end),y_s(end,:),mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.pAtm,mixCnst.gasSurfReac_struct,options);
    dsourceGSs_dT   = getSurfaceSpeciesDer_dsourceGSs_dT( TTrans(end),Tel(end),p(end),y_s(end,:),mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.pAtm,mixCnst.gasSurfReac_struct,options);
    dsourceGSs_dym  = getSurfaceSpeciesDer_dsourceGSs_dym(TTrans(end),Tel(end),p(end),y_s(end,:),mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.gasSurfReac_struct,options);
elseif options.marchYesNo && ismember(options.wallYs_BC,{'catalyticGamma','catalyticK'})
    options.flow = protectedvalue(options,'flow','CNE');
    options.EoS  = protectedvalue(options,'EoS','idealGas');
    sourceGSs       = getSurface_catalytic_sourceGS(        p(end),y_s(end,:),mixCnst.gasSurfReac_struct.nuCat_prod,mixCnst.gasSurfReac_struct.nuCat_reac,mixCnst.Mm_s,mixCnst.bElectron,TTrans(end),Tel(end),mixCnst.R0,                     options.wallYs_BC,options.Ys_bc);
    dsourceGSs_dT   = getSurfaceDer_catalytic_dsourceGS_dT( p(end),y_s(end,:),mixCnst.gasSurfReac_struct.nuCat_prod,mixCnst.gasSurfReac_struct.nuCat_reac,mixCnst.Mm_s,mixCnst.bElectron,TTrans(end),Tel(end),mixCnst.R0,options.numberOfTemp,options.wallYs_BC,options.Ys_bc);
    dsourceGSs_dym  = getSurfaceDer_catalytic_dsourceGS_dym(p(end),y_s(end,:),mixCnst.gasSurfReac_struct.nuCat_prod,mixCnst.gasSurfReac_struct.nuCat_reac,mixCnst.Mm_s,mixCnst.bElectron,TTrans(end),Tel(end),mixCnst.R0,                     options.wallYs_BC,options.Ys_bc);
else
    sourceGSs       = zeros(N_eta,N_spec);
    dsourceGSs_dT   = zeros(N_eta,N_spec,N_T);
    dsourceGSs_dym  = zeros(N_eta,N_spec,N_spec);
end
                                                            % Obtaining total source term (and derivatives)
sourceGS        = sourceGSs * ones(N_spec,1);                       % (eta,s)   --> (eta,1)
dsourceGS_dT    = permute(sum(dsourceGSs_dT,2),[1,3,2]);            % (eta,s,T) --> (eta,T)
dsourceGS_dym   = sum(dsourceGSs_dym,3);                            % (eta,m,s) --> (eta,m)
if ismember(options.thermal_BC,{'ablation'})          % only the thermal ablative BC needs the radiative source term
    [sourceAbl,dsourceAbl_dT,dsourceAbl_dym] = getSurface_energySource(TTrans(end),TRot(end),TVib(end),TElec(end),Tel(end),sourceGSs,dsourceGSs_dT,dsourceGSs_dym,mixCnst.R_s,...
                                            mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,...
                                            mixCnst.sigmaStefBoltz,mixCnst.gasSurfReac_struct.epsilonRadSurf,options);
end

% For Conical BLs - computing transverse curvature term
switch options.coordsys
    case 'cartesian2D'
        t = ones(N_eta,1); % there is no transverse curvature to take care of
    case {'cone','cylindricalExternal','cylindricalInternal'}
        if options.transverseCurvature % for marching, we will have this term
            % obtaining dimensional y
            [~,~,r,r0] = getDimensional_xy(D1,rho,U_e,intel.xi,intel.x,options.coordsys,intel.cylCoordFuncs);
            t           = r./r0;                    % transverse curvature term
        else % for SS BLs, it must be deleted because it is a function of xi
            t = ones(N_eta,1);
        end
    otherwise
        error(['the chosen coordsys ''',options.coordsys,''' is not supported.']);
end

% If the user specifies a blowing BC, it must be updated, according to the
% value of rho at the wall
if ismember(options.wallBlowing_BC,{'Vwall','mwall'})
    switch options.wallBlowing_BC                                           % depending on the boundary condition
        case 'Vwall'                                                            % if it is the velocity
            V_w = options.trackerVwall(i_xi);                                       % we extract it
            rho_w = rho(end);                                                       % density at the wall
            m_w = V_w*rho_w;                                                        % mass-flow at the wall
        case 'mwall'                                                            % if it is the massflow
            m_w = options.trackerVwall(i_xi);                                       % we extract it
        otherwise                                                               % otherwise break
            error(['the chosen wallBlowing_BC ''',options.wallBlowing_BC,''' is not supported']);
    end
    switch options.coordsys
        case 'cartesian2D'
            intel.Afw = m_w/(rho_e*mu_e*U_e);
        case {'cone','cylindricalExternal','cylindricalInternal'}
            xPhys = intel.xPhys;                                                % transformed x
            r0 = intel.cylCoordFuncs.rc(xPhys);                                 % conical radius
            intel.Afw = m_w/(r0*rho_e*mu_e*U_e);
        otherwise
            error(['the chosen coordsys ''',options.coordsys,''' is not supported']);
    end
    % This parameter will be needed for the evaluation of the BC in
    % eval_blowingBC
end

% Turbulence
switch options.modelTurbulence
    case 'none'                                 % nothing to add
    case 'SmithCebeci1967'                      % computing eddy viscosity and Prandtl number
        [muT,PrT] = getTurbulent_mu_Pr(options.modelTurbulence,rho,mu,df_deta,df_deta2,g,intel.eta,D1,rho_e,U_e,intel.xi,intel.x,options.coordsys,intel.cylCoordFuncs);
    case 'SSTkomega'                            % computing eddy viscosity and Prandtl number
        wT_w = getWall_turbulentViscosity(D1,rho,mu,df_deta2,U_e,intel.xi,intel.x,options.coordsys,intel.cylCoordFuncs);
        [wT,dwT_deta,dwT_deta2] = correctBoundaries(wT,wT_w,intel.eta,dwT_deta,dwT_deta2,'onlyPosEnd','fix_a',4);
        intel.WT        = wT./wT_e;
        intel.dWT_deta  = dwT_deta./wT_e;
        intel.dWT_deta2 = dwT_deta2./wT_e;
        [muT,PrT,betaK,betaW,sigmaK,sigmaW,alphaW,gammaT] = getTurbulent_mu_Pr(options.modelTurbulence,rho,mu,df_deta,df_deta2,g,intel.eta,D1,rho_e,U_e,intel.xi,intel.x,options.coordsys,intel.cylCoordFuncs,kT,wT,dkT_deta,dwT_deta);
    otherwise
        error(['The chosen turbulence model ''',options.modelTurbulence,''' is not supported']);
end

% Obtaining scaling parameter for the source terms due to the Probstein-Elliot transformation
sourceScaleVol = 1;                                                         % default value of the scaling parameter for volumetric source terms
sourceScaleSur = 1;                                                         % default value of the scaling parameter for surface source terms
if options.marchYesNo
    switch options.coordsys
        case 'cartesian2D'                                                  % nothing tot add
        case {'cone','cylindricalExternal','cylindricalInternal'}           % we must scale the source term due to the Probstein-Elliot transformation
            xPhys = intel.xPhys;                                                % transformed x
            r0 = intel.cylCoordFuncs.rc(xPhys);                                 % conical radius
            sourceScaleVol = 1/r0^2;                                            % volumetric source-term scaling
            sourceScaleSur = 1/(t.*r0);                                         % surface source-term scaling (this is actually 1/r)
        otherwise;              error(['the chosen coordsys ''',options.coordsys,''' is not supported']);
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Non-dimensionalizing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch options.numberOfTemp
    case '1T'
        if bMultiSpec||bMultiElem
            if options.bPairDiff
                [intel.C,intel.a1,intel.a2,intel.a0,intel.j,C_sl,a_s1,a_sl2,zeta_s] = ...
                    physical2comp_props(rho,mu,cp,      kappa,          Ecu,Ecw,rho_e,mu_e,h_e,Tv_e,t,D_s,h_s,          Mm,mixCnst.Mm_s,nTfrac_s,true,y_s);
            else
                [intel.C,intel.a1,intel.a2,intel.a0,intel.j,C_s,a_s1,a_s2,zeta_s] = ...
                    physical2comp_props(rho,mu,cp,      kappa,          Ecu,Ecw,rho_e,mu_e,h_e,Tv_e,t,D_s, h_s,          Mm,mixCnst.Mm_s,nTfrac_s,false);
            end
            [Omega_s,dOmegas_dg,dOmegas_ddfdeta,dOmegas_dym] = ...
                            physical2comp_source(source_s,dsources_dT,dsources_dym,TTrans,Tel,p,y_s,cp,h_s,df_deta,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,H_e,U_e,rho_e,mu_e,options);
        else
            [intel.C,intel.a1,intel.a2,intel.a0,intel.j] = ...
                    physical2comp_props(rho,mu,cp,      kappa,          Ecu,Ecw,rho_e,mu_e,h_e,Tv_e,t);
        end
        cp4Source = cp;           % heat capacity used for the expansion about the baseflow of the source terms
    case '2T'
        % remember that cp is now actually cptr (translational-rotational)
        if bMultiSpec
            if options.bPairDiff
                [intel.C,intel.a1,intel.a2,intel.a0,intel.j,C_sl,a_s1,a_sl2,zeta_s,intel.av1,intel.av2,intel.Cpv,av_sl2] = ...
                    physical2comp_props(rho,mu,{cptr,cpv},{kappa,kappav}, Ecu,Ecw,rho_e,mu_e,h_e,Tv_e,t,D_s,{htr_s,hv_s},Mm,mixCnst.Mm_s,nTfrac_s,true,y_s,cpv_s);
            else
                [intel.C,intel.a1,intel.a2,intel.a0,intel.j,C_s,a_s1,a_s2,zeta_s,intel.av1,intel.av2,intel.Cpv,av_s2] = ...
                    physical2comp_props(rho,mu,{cptr,cpv},{kappa,kappav}, Ecu,Ecw,rho_e,mu_e,h_e,Tv_e,t,D_s,{htr_s,hv_s},Mm,mixCnst.Mm_s,nTfrac_s,false,   cpv_s);
            end
            [Omega_s,dOmegas_dg,dOmegas_ddfdeta,dOmegas_dym,dOmegas_dtauv] = ...
                            physical2comp_source(source_s,dsources_dT,dsources_dym,TTrans,Tel,p,y_s,cptr,h_s,df_deta,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,H_e,U_e,rho_e,mu_e,options,cpv,Tv_e);
        else
            [intel.C,intel.a1,intel.a2,intel.a0,intel.j,intel.av1,intel.av2,intel.Cpv] = ...
                    physical2comp_props(rho,mu,{cptr,cpv},{kappa,kappav}, Ecu,Ecw,rho_e,mu_e,h_e,Tv_e,t);
        end
        [OmegaV,dOmegaV_dg,dOmegaV_ddfdeta,dOmegaV_dym,dOmegaV_dtauv] = ...
                            physical2comp_sourcev(sourcev,dsourcev_dT,dsourcev_dym,TTrans,Tel,p,y_s,cptr,cpv,h_s,df_deta,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,H_e,h_e,Tv_e,U_e,rho_e,mu_e,options);
        [intel.dCpv_dg,intel.dCpv_ddfdeta,dCpv_dys,intel.dCpv_dtauv] = physical2comp_cpv(dcpv_dTv,cpv_s,H_e,h_e,U_e,Tv_e,cptr,cpv,df_deta,h_s,options);
        cp4Source = cptr;           % heat capacity used for the expansion about the baseflow of the source terms
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end
if ismember(options.wallYs_BC,{'ablation','catalyticGamma','catalyticK'}) && options.marchYesNo           % Surface chemistry models for mass fractions
    [Mabl_s,dMabls_dg,dMabls_ddfdeta,dMabls_dym,dMabls_dtauv] = physical2comp_sourceGSs(sourceGSs,dsourceGSs_dT,dsourceGSs_dym,cp4Source(end),h_s(end,:),df_deta(end),H_e,U_e,rho_e,mu_e,options,cpv(end),Tv_e);
end
if ismember(options.wallBlowing_BC,{'ablation'}) && options.marchYesNo      % Surface chemistry models for wall blowing
    [Mabl  ,dMabl_dg ,dMabl_ddfdeta ,dMabl_dym ,dMabl_dtauv ] = physical2comp_sourceGS(sourceGS,  dsourceGS_dT, dsourceGS_dym, cp4Source(end),h_s(end,:),df_deta(end),H_e,U_e,rho_e,mu_e,options,cpv(end),Tv_e);
end
if ismember(options.thermal_BC,{'ablation'})                                % Surface chemistry models for energy
    [OmegaAbl,dOmegaAbl_dg,dOmegaAbl_ddfdeta,dOmegaAbl_dym,dOmegaAbl_dtauv] = physical2comp_sourceEnergyAbl(sourceAbl,dsourceAbl_dT,dsourceAbl_dym,cp4Source(end),h_s(end,:),df_deta(end),H_e,U_e,rho_e,mu_e,options,cpv(end),Tv_e);
end
if strcmp(options.numberOfTemp,'2T') && ismember(options.thermalVib_BC,{'thermalEquil','ablation'})
    [intel.Bv_tauv,intel.Bv_g,intel.Bv_ys] = physical2comp_thermalEquil(cpv(end),cptr(end),h_s(end,:),H_e,Tv_e);
end
if bMultiElem
    if options.bPairDiff                                        % spec-spec diffusion
        if options.molarDiffusion;  CXYs_inputs = {C_sl,dXs_dyE};   % molar diffusion
        else;                       CXYs_inputs = {C_sl,dys_dyE};   % mass diffusion
        end
    else                                                        % spec-mix diffusion
        if options.molarDiffusion;  CXYs_inputs = {C_s, dXs_dyE};   % molar diffusion
        else;                       CXYs_inputs = {C_s, dys_dyE};   % mass diffusion
        end
    end
    [agE,auE,aYE,CYEF] = physical2comp_elementalDiffusion(rho,A_E,C_EF,a_s1,CXYs_inputs{:},dys_dyE,cp,h_s,U_e,H_e,rho_e,mu_e,options.bPairDiff);
end

% Modifying non-dimensional parameteres due to turbulence
switch options.modelTurbulence
    case 'none' % nothing to add
    case 'SmithCebeci1967' % computing eddy viscosity and Prandtl number
        [intel.C,intel.a1] = physical2comp_turbulence(intel.C,intel.a1,muT,PrT,mu,cp,kappa,options);
    case 'SSTkomega'
        [intel.AK,intel.AW,intel.BW,intel.aK0,intel.aK1,intel.aK2,intel.aW0,intel.aW1,intel.aW2] = physical2comp_SSTkomega(rho,mu,muT,wT,betaK,betaW,sigmaK,sigmaW,alphaW,gammaT,rho_e,mu_e,U_e,W_0,kT_e,wT_e,t,sourceScaleVol);
        [intel.C,intel.a1] = physical2comp_turbulence(intel.C,intel.a1,muT,PrT,mu,cp,kappa,options);
        intel.CT = muT.*rho./(mu_e*rho_e);
    otherwise
        error(['The chosen turbulence model ''',options.modelTurbulence,''' is not supported']);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%  Computing gradients and populating intel
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
intel.dj_deta    = D1*intel.j;
intel.dC_deta    = D1*intel.C;
intel.da1_deta   = D1*intel.a1;
intel.da2_deta   = D1*intel.a2;
intel.tau        = TTrans/T_e;
intel.dtau_deta  = D1*intel.tau;
intel.dtau_deta2 = D2*intel.tau;
if strcmp(options.thermal_BC,'Twall')                       % updating g vector in case the boundary condition required it
    g_new = (h + 0.5*(U_e.*df_deta).^2) / H_e;
%     [intel.g,intel.dg_deta,intel.dg_deta2] = correctBoundaries(intel.g,g_new,intel.eta,intel.dg_deta,intel.dg_deta2);
    intel.g        = g_new;
    intel.dg_deta  = D1*g_new;
    intel.dg_deta2 = D2*g_new;
    options.G_bc = intel.g(end);
end
if bMultiSpec
    for s=1:N_spec % defining species properties
        intel.zetas{s}           = zeta_s(:,s);
        intel.dzetas_deta{s}     = D1 * zeta_s(:,s);
        intel.dzetas_deta2{s}    = D2 * zeta_s(:,s);
        intel.as1{s}             = a_s1(:,s);
        intel.das1_deta{s}       = D1 * a_s1(:,s);
        intel.Omegas{s}          = Omega_s(:,s) * sourceScaleVol;
        if options.bPairDiff % the diffusion theory accounts for species-species diffusion (N_spec^2 diff. coefficients)
            for l=1:N_spec % looping second species index
                intel.Csl{s,l}           = C_sl(:,s,l);
                intel.asl2{s,l}          = a_sl2(:,s,l);
                intel.dCsl_deta{s,l}     = D1 * C_sl(:,s,l);
                intel.dasl2_deta{s,l}    = D1 * a_sl2(:,s,l);
            end
        else                % the diffusion theory accounts for species-mixture diffusion (N_spec diff. coefficients)
            intel.Cs{s}              = C_s(:,s);
            intel.dCs_deta{s}        = D1 * C_s(:,s);
            intel.as2{s}             = a_s2(:,s);
            intel.das2_deta{s}       = D1 * a_s2(:,s);
        end
        intel.dOmegas_dg{s}      = dOmegas_dg(:,s) * sourceScaleVol;
        intel.dOmegas_ddfdeta{s} = dOmegas_ddfdeta(:,s) * sourceScaleVol;
        for l=1:N_spec
            intel.dOmegas_dyl{s,l}   = dOmegas_dym(:,s,l) * sourceScaleVol;
        end
    end
elseif bMultiElem
    for E=mixCnst.N_elem:-1:1 % defining elemental properties
        intel.agE{E}       = agE(:,E);
        intel.auE{E}       = auE(:,E);
        intel.aYE{E}       = aYE(:,E);
        intel.dagE_deta{E} = D1 * agE(:,E);
        intel.dauE_deta{E} = D1 * auE(:,E);
        intel.daYE_deta{E} = D1 * aYE(:,E);
        for F=mixCnst.N_elem:-1:1
            intel.CYEF{E,F}       = CYEF(:,E,F);
            intel.dCYEF_deta{E,F} = D1 * CYEF(:,E,F);
        end
    end
end
intel.t = t;
switch options.modelTurbulence
    case {'none','SmithCebeci1967'} % nothing to add
    case 'SSTkomega'
        intel.daK2_deta = D1*intel.aK2;
        intel.daW2_deta = D1*intel.aW2;
    otherwise
        error(['The chosen turbulence model ''',options.modelTurbulence,''' is not supported']);
end
%%% Additional two-temperature fields
if strcmp(options.numberOfTemp,'2T')
    options.tauv_bc   = TVib(end);
    intel.tauv        = TVib/Tv_e;
    intel.dtauv_deta  = D1*intel.tauv;
    intel.dtauv_deta2 = D2*intel.tauv;
    intel.dav1_deta   = D1*intel.av1;
    intel.dav2_deta   = D1*intel.av2;
    intel.OmegaV = OmegaV * sourceScaleVol;
    intel.dOmegaV_dg = dOmegaV_dg * sourceScaleVol;
    intel.dOmegaV_dtauv = dOmegaV_dtauv * sourceScaleVol;
    intel.dOmegaV_ddfdeta = dOmegaV_ddfdeta * sourceScaleVol;
    if bMultiSpec
        for s=1:N_spec % defining species properties
            intel.dCpv_dys{s}= dCpv_dys(:,s);
            intel.dOmegas_dtauv{s}= dOmegas_dtauv(:,s) * sourceScaleVol;
            intel.dOmegaV_dys{s} = dOmegaV_dym(:,s) * sourceScaleVol;
            if options.bPairDiff % the diffusion theory accounts for species-species diffusion (N_spec^2 diff. coefficients)
                for l=1:N_spec % looping second species index
                    intel.avsl2{s,l} = av_sl2(:,s,l);
                end
            else
                intel.avs2{s} = av_s2(:,s);
            end
        end
    end
end

%%% Additional gas-surface interaction fields
if ismember(options.wallYs_BC,{'ablation','catalyticGamma','catalyticK'}) && options.marchYesNo % Surface chemistry models for mass fractions
    for s=1:N_spec % looping species;
        intel.Mabl_s{s} = Mabl_s(:,s) * sourceScaleSur;
        intel.dMabls_dg{s} = dMabls_dg(:,s) * sourceScaleSur;
        intel.dMabls_ddfdeta{s} = dMabls_ddfdeta(:,s) * sourceScaleSur;
        intel.dMabls_dtauv{s} = dMabls_dtauv(:,s) * sourceScaleSur;
        for l=1:N_spec % looping species
            intel.dMabls_dyl{s,l} = dMabls_dym(:,s,l * sourceScaleSur);
        end
    end
end
if ismember(options.wallBlowing_BC,{'ablation'}) && options.marchYesNo      % Surface chemistry models for wall blowing
    intel.Mabl = Mabl * sourceScaleSur;
    intel.dMabl_dg = dMabl_dg * sourceScaleSur;
    intel.dMabl_ddfdeta = dMabl_ddfdeta * sourceScaleSur;
    intel.dMabl_dtauv = dMabl_dtauv * sourceScaleSur;
    for l=1:N_spec % looping species
        intel.dMabl_dyl{l} = dMabl_dym(:,l) * sourceScaleSur;
    end
end
if ismember(options.thermal_BC,{'ablation'})                                % Surface chemistry models for energy
    intel.OmegaAbl = OmegaAbl * sourceScaleSur;
    intel.dOmegaAbl_dg = dOmegaAbl_dg * sourceScaleSur;
    intel.dOmegaAbl_ddfdeta = dOmegaAbl_ddfdeta * sourceScaleSur;
    intel.dOmegaAbl_dtauv = dOmegaAbl_dtauv * sourceScaleSur;
    for l=1:N_spec % looping species
        intel.dOmegaAbl_dyl{l} = dOmegaAbl_dym(:,l) * sourceScaleSur;
    end
end

% species derivatives if not in system equations
if ismember(options.flow,{'LTE','LTEED','TPG','CPG'}) % for non-multispecies mixtures we want to save the mass fractions also
    intel.ys = matrix2D2cell1D(y_s);
    for s=1:N_spec
        intel.dys_deta{s} = D1 * y_s(:,s);
        intel.dys_deta2{s} = D2 * y_s(:,s);
    end
end
if ~ismember(options.flow,{'LTEED'})
    intel.yE = matrix2D2cell1D(y_E);
    for E=1:mixCnst.N_elem
        intel.dyE_deta{E}  = D1 * y_E(:,E);
        intel.dyE_deta2{E} = D2 * y_E(:,E);
    end
end
intel.Tguess = TTrans;
intel.XsGuessLTE = X_s;
end % eval_thermo.m
