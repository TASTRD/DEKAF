function [urot,wrot,phi] = eval_rotationVelVec(u,w,options,Lambda_sweep)
%EVAL_ROTATIONVELVEC Rotate the components of the velocity vector from one
%frame to another, specified by options.rotateVelocityVector
%
% Usage
%   [urot,wrot] = eval_rotationVelVec(u,w,options,Lambda_sweep)
%   [urot,wrot,phi] = eval_rotationVelVec(u,w,options,Lambda_sweep)
%
% In
%   u            - global frame horizontal velocity,     N_y x N_x
%   w            - global frame spanwise velocity,       N_y x N_x
%   options      - necessary fields
%                   .rotateVelocityVector (specify rotation for angle phi)
%   Lambda_sweep - sweep angle (streamline: unused)        1 x 1
%
% Out
%   urot         - rotated u by angle phi,               N_y x N_x
%   wrot         - rotated w by angle phi,               N_y x N_x
%   phi          - angle of rotation, [streamline: 1 x N_x, sweep: 1x1]
%
% See also setDefaults, eval_dimVars
%
% Author(s): Ethan Beyak
%            Koen Groot
%
% GNU Lesser General Public License 3.0

N_y = size(u,1);

switch options.rotateVelocityVector
    case 'streamline'
        phi = atan2(w(1,:),u(1,:));
        phi_2D = repmat(phi,[N_y,1]);
    case 'cst_sweep'
        phi = deg2rad(Lambda_sweep);
        phi_2D = phi;
    case 'rhow_IP'
        error('rho*w inflection point option is currently unsupported!');
    case 'w_IP'
        error('w inflection point option is currently unsupported!');
    otherwise
        error(['option for velocity vector rotation',options.rotateVelocityVector,' is currently unsupported... Aborting!']);
end

% perform the rotation
urot = + u.*cos(phi_2D) + w.*sin(phi_2D);
wrot = - u.*sin(phi_2D) + w.*cos(phi_2D);

end % eval_rotationVelVec
