function [a_HO,b_HO,c_HO] = eval_highOrderTerms(intel,options)
% eval_highOrderTerms(intel,options) evaluates the part of the forcing
% vector due to the higher order fluctuation terms.
%
% see also: DEKAF_QSS, DEKAF_BL, setDefaults, listOfVariablesExplained,
% eval_thermo
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

%%% DEVELOPERS NOTE: there's a lot of definitions commented out. That's
% because they are not used for the higher-order terms.

error('the highOrderTerms option is not supported in this version.');

%%% Extracting everything from intel and options
% options and constants
N       = options.N;
N_spec  = options.N_spec;
beta    = intel.beta;
% flow properties
%a0 = intel.a0;
%da0_deta = intel.da0_deta;
%da0_dg = intel.da0_dg;
%dda0_dg_deta = intel.dda0_dg_deta;
%da0_ddfdeta = intel.da0_ddfdeta;
%dda0_ddfdeta_deta = intel.dda0_ddfdeta_deta;

%a1 = intel.a1;
%da1_deta = intel.da1_deta;
da1_dg = intel.da1_dg;
dda1_dg_deta = intel.dda1_dg_deta;
da1_ddfdeta = intel.da1_ddfdeta;
dda1_ddfdeta_deta = intel.dda1_ddfdeta_deta;

a2 = intel.a2;
da2_deta = intel.da2_deta;
da2_dg = intel.da2_dg;
dda2_dg_deta = intel.dda2_dg_deta;
da2_ddfdeta = intel.da2_ddfdeta;
dda2_ddfdeta_deta = intel.dda2_ddfdeta_deta;

%C = intel.C;
%dC_deta = intel.dC_deta;
dC_dg = intel.dC_dg;
ddC_dg_deta = intel.ddC_dg_deta;
dC_ddfdeta = intel.dC_ddfdeta;
ddC_ddfdeta_deta = intel.ddC_ddfdeta_deta;

%jj = intel.jj;  % we avoid using j, since matlab understands it as the complex unity
%djj_deta = intel.dj_deta;
%djj_dg = intel.dj_dg;
%ddjj_dg_deta = intel.ddj_dg_deta;
%djj_ddfdeta = intel.dj_ddfdeta;
%ddjj_ddfdeta_deta = intel.ddj_ddfdeta_deta;

if options.MultiSpec
    %zetas = intel.zetas;
    %dzetas_deta = intel.dzetas_deta;
    %dzetas_deta2 = intel.dzetas_deta2;
    %gs = intel.gs;
    %dgs_deta = intel.dgs_deta;
end

%f          = intel.f;
df_deta    = intel.df_deta;
df_deta2   = intel.df_deta2;
df_deta3   = intel.df_deta3;
fh          = intel.fh;
dfh_deta    = intel.dfh_deta;
dfh_deta2   = intel.dfh_deta2;
dfh_deta3   = intel.dfh_deta3;

gh          = intel.gh;
dgh_deta    = intel.dgh_deta;
dgh_deta2   = intel.dgh_deta2;

dkh_deta    = intel.dkh_deta;
dkh_deta2   = intel.dkh_deta2;

if options.MultiSpec
    %ysh         = intel.ysh;
    dysh_deta   = intel.dysh_deta;
    %dysh_deta2  = intel.dysh_deta2;
end

if options.marchYesNo
    dfh_dxi      = zeros(N,1); % we neglect variations of the hatted variables along xi
    dfh_detadxi  = zeros(N,1);
    dgh_dxi      = zeros(N,1);
    dkh_dxi      = zeros(N,1);
    %df_dxi       = intel.df_dxi;
    %dg_dxi      = intel.dg_dxi;
    %df_detadxi  = intel.df_detadxi;
    %dk_dxi      = intel.dk_dxi;
    if options.MultiSpec
        %dys_dxi     = intel.dys_dxi;
        dysh_dxi    = zeros(N,1); % we neglect variations of the hatted variables along xi
    end
end

%%% populating vectors
a_HO1 = dfh_deta2.*gh.*ddC_dg_deta+dfh_deta.*dfh_deta2.*ddC_ddfdeta_deta+(dfh_deta3.*gh+dfh_deta2.*dgh_deta).*dC_dg+(dfh_deta.*dfh_deta3+dfh_deta2.^2).*dC_ddfdeta+dfh_deta2.*fh-beta.*dfh_deta.^2;
a_HO3 = dkh_deta.*gh.*ddC_dg_deta+dfh_deta.*dkh_deta.*ddC_ddfdeta_deta+(dkh_deta2.*gh+dgh_deta.*dkh_deta).*dC_dg+(dfh_deta.*dkh_deta2+dfh_deta2.*dkh_deta).*dC_ddfdeta+dkh_deta.*fh;
a_HO4 = (da1_dg.*dgh_deta2+dda1_dg_deta.*dgh_deta+da2_dg.*dfh_deta.*df_deta3+(2.*da2_dg.*dfh_deta2+dda2_dg_deta.*dfh_deta).*df_deta2+(da2_dg.*dfh_deta3+dda2_dg_deta.*dfh_deta2).*df_deta+da2_dg.*dfh_deta.*dfh_deta3+da2_dg.*dfh_deta2.^2+dda2_dg_deta.*dfh_deta.*dfh_deta2).*gh+dgh_deta.*fh+da1_ddfdeta.*dfh_deta.*dgh_deta2+da1_dg.*dgh_deta.^2+(da2_dg.*dfh_deta.*df_deta2+da2_dg.*dfh_deta2.*df_deta+(da2_dg.*dfh_deta+da1_ddfdeta).*dfh_deta2+dda1_ddfdeta_deta.*dfh_deta).*dgh_deta+da2_ddfdeta.*dfh_deta.^2.*df_deta3+(3.*da2_ddfdeta.*dfh_deta.*dfh_deta2+dda2_ddfdeta_deta.*dfh_deta.^2).*df_deta2+(da2_ddfdeta.*dfh_deta.*dfh_deta3+da2_ddfdeta.*dfh_deta2.^2+dda2_ddfdeta_deta.*dfh_deta.*dfh_deta2).*df_deta+(da2_ddfdeta.*dfh_deta.^2+a2.*dfh_deta).*dfh_deta3+(2.*da2_ddfdeta.*dfh_deta+a2).*dfh_deta2.^2+(dda2_ddfdeta_deta.*dfh_deta.^2+da2_deta.*dfh_deta).*dfh_deta2;
if options.marchYesNo
    b_HO1 = 2.*dfh_deta2.*dfh_dxi-2.*dfh_deta.*dfh_detadxi;
    b_HO3 = 2.*dfh_dxi.*dkh_deta-2.*dfh_deta.*dkh_dxi;
    b_HO4 = 2.*dfh_dxi.*dgh_deta-2.*dfh_deta.*dgh_dxi;
end
if options.MultiSpec
    a_HOs = zeros(N_spec*N,1);
    b_HOs = zeros(N_spec*N,1);
    for s=1:options.N_spec-1 % we only have N_spec-1 species equations
        a_HOs((s-1)*N+1:s*N) = dysh_deta{s}.*fh;
        if options.marchYesNo
            b_HOs((s-1)*N+1:s*N) = 2.*dfh_dxi.*dysh_deta{s}-2.*dfh_deta.*dysh_dxi{s};
        end
    end
    % the concentration condition will have no higher-order terms, so we
    % simply let this be zero.
end

%%% Assembling
oo = zeros(N,1);

if options.Cooke
    a_HO = [a_HO1 ; oo ; a_HO3 ; a_HO4];
    if options.marchYesNo
        b_HO = [b_HO1 ; oo ; b_HO3 ; b_HO4];
    else
        b_HO = [ oo   ; oo ; oo ;  oo  ];
    end
    c_HO = [ oo   ; oo ; oo ;  oo  ];
else
    a_HO = [a_HO1 ; oo ; a_HO4];
    if options.marchYesNo
        b_HO = [b_HO1 ; oo ; b_HO4];
    else
        b_HO = [ oo   ; oo ;  oo  ];
    end
    c_HO = [ oo   ; oo ;  oo  ];
end

if options.MultiSpec
    a_HO = [a_HO ; a_HOs];
    b_HO = [b_HO ; b_HOs];
    c_HO = [c_HO ; zeros(N_spec*N,1)];
end
