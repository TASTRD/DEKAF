function [intel,options] = DEKAF_QSS(intel,options)
% DEKAF_QSS Solve the quasi-self-similar compressible boundary-layer system
% of ODEs, using the Chebyshev pseudo-spectral collocation method.
%
% intel = DEKAF_QSS(intel,options);
%
% Notes:
% For an explanation of inputs, see listOfVariablesExplained.m and
% setDefaults.m
%
% Children and Related Functions:
% See also build_edge_values.m, build_IC.m, eval_thermo.m,
%          build_mat_totalEnthalpy.m, update_solution.m
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
%            Koen Groot
%            Alexander Moyes
%
% GNU Lesser General Public License 3.0

% Displaying DEKAF logo
if options.displayHeader
    displayTestcaseInfo(intel,options);
end

% Importing solution or obtaining initial guess
if ischar(options.importSolution)
    intel = build_importedSolution(intel,options);
else
    disp('--- Building initial guess...');
    intel = build_IC(intel,options);
end

% Initializing options
conv = 1;
intel.norms = struct; % initialising norm structures
intel = standardvalue(intel,'norms_ref',struct);
intel.it = 1;
intel.i_xi = 1; % we are computing self-similar, so it's always the first xi
all_conv(intel.it) = conv;
disp('--- Running...')
tmr = tic; % starting timer
[intel,options] = eval_thermo(intel,options); % initializing thermodynamic properties
while conv>options.tol && intel.it<options.it_max

    % plotting transient BL profiles
    if options.plot_transient && sum(intel.it==options.it_plot)
        plotProfiles(intel,options);
    end

    % creating convergence GIF
    if options.gifConv && floor(intel.it/options.gifStep) == intel.it/options.gifStep
        intel = plot_gifConvergence(intel,options,conv);
    end

    % storing previous convergence in intel
    intel.conv = conv;

    % defining LHS matrix and RHS forcing term vector
    intel = build_mat([],intel,[],options);
    intel = build_BC(intel,options);

    % solving
    intel.sol_hat = intel.mat \ intel.forcing;

    % extracting, rebuilding derivatives and coefficients
    [intel,options]       = update_solution(intel,options);
    intel.it    = intel.it+1;
    [conv,intel]= eval_norm(intel,options);

    all_conv(intel.it) = conv;
    % checking for NaNs
    if isnan(conv)
        error('the solution NaNed');
    end
    % Check if the residual has saturated at the same order of
    % magnitude for several iterations
    res_sat_flag = false;
    if length(all_conv) >= options.it_res_sat(1)
        res_sat_flag = check_res_saturation(all_conv,options.it_res_sat(1),options.it_res_sat_diff_val(1));
    end
    if res_sat_flag
        fprintf('--- Residuals have become numerically saturated over the last %d iterations.\n',options.it_res_sat(1))
        fprintf('--- Exitting the self-similar iteration process!\n');
        break
    end
end

if options.plotRes
    plotProfiles(intel,options);
    plot_convergence(intel,options); % plotting norms, residuals, etc.
end

if intel.it==options.it_max
    disp(['--- Concluded at the maximum iteration (',num2str(options.it_max),')']);
end

% Re-dimensionalise state variables
if options.dimoutput
    intel = eval_dimVars(intel,options);
end

disp('--- Done');

intel   = orderfields(intel);
options = orderfields(options);

runtime = toc(tmr);
disp(['--- Runtime of ',num2str(runtime,4),' seconds,    QSS update error:    ',num2str(conv,'%.2e')]);

end % DEKAF_QSS.m
