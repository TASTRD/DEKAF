function [rVis,deltaStar,varargout] = DEKAF_nozzleViscousCorrection(xInv,rInv,deltaStar0,p_e0,T_e,M_e,T_w,mixture,flow,modelTransport,geomTol,varargin)
% DEKAF_nozzleViscousCorrection is a DEKAF wrap-around function performing
% a viscous correction of a nozzle geometry, estimated with inviscid
% theory, using DEKAF's marching BL solver.
%
% Usage:
%   (1)
%       [rVis,deltaStar,conv_vec] = DEKAF_nozzleViscousCorrection(xInv,rInv,delta0,...
%                                   p_e0,T_e,M_e,T_w,mixture,flow,modelTransport,geomTol)
%
%   (2)     (IN DEVELOPMENT)
%       [...] = DEKAF_nozzleViscousCorrection(...,'EoS',modelEoS)
%       |---> Allows to manually choose the equation of state to be used.
%       Default: 'idealGas'
%
%   (3)     (IN DEVELOPMENT)
%       [...] = DEKAF_nozzleViscousCorrection(...,'modelTurbulence',modelTurbulence)
%       |---> Allows to manually choose the turbulence modeling to be used.
%       Default: 'none'
%
%   (4)
%       [...,var1,var2,...] = DEKAF_nozzleViscousCorrection(...,'additionalOutVars',{'var1','var2',...})
%       |---> Allows to pass a cell of strings with the names of additional
%       variables to be outputted by the function. A list of the variable
%       names employed in DEKAF can be found in <a href="matlab:help listOfVariablesExplained">listOfVariablesExplained</a>.
%       For instance, if we want to retrieve the velocity and temperature
%       profiles along the boundary layer, we must call the function with:
%       [...,u,T] = DEKAF_nozzleViscousCorrection(...,'additionalOutVars',{'u','T'})
%
%   (5)
%       [...] = DEKAF_nozzleViscousCorrection(...,'additionalOptions',{'opt1',val1,'opt2',val2,...})
%       |---> Allows to fix additional options for DEKAF beyond what is
%       established by this function. The additional options must be passed
%       in successive pairs in a cell, where the first item of each pair is
%       a string identifying the name of the option, and the second is the
%       value to be assigned to that option. The list of options fixed by
%       this function is specified below.
%       For instance, if we want to run DEKAF with options.N=200 and
%       options.L=20, we must call the function with:
%       [...,] = DEKAF_nozzleViscousCorrection(...,'additionalOptions',{'N',200,'L',20})
%
%   (6)
%       [...] = DEKAF_nozzleViscousCorrection(...,'guessDeltaStarFromExternalBL',scaleFact)
%       |--> allows to get the initial guess for the value of the
%       displacement thickness (deltaStar0) from an external BL simulation.
%       The BL size of external flows is typically much smaller than that
%       of internal flows, an enlargement scale factor (scaleFact) must
%       also be provided. A value of around 2 is recommended.
%       This usage obviously bypasses whatever is passed through deltaStar0
%
%   (7)
%       [...] = DEKAF_nozzleViscousCorrection(...,'keepMesh')
%       |--> does not modify the nozzle mesh. If this flag is not passed,
%       the mesh is modified in order to retain always the computational
%       domain in which DEKAF was run. Regardless of whether this flag is
%       passed or not, the solution is always reinterpolated back to the
%       original mesh, so the outputs are always size N_x x 1
%
%   (8)
%       [...] = DEKAF_nozzleViscousCorrection(...,'runOnOriginalMesh')
%       |--> runs DEKAF on exactly the same mesh points that were passed in
%       the inputs. This flag inutilizes the 'keepMesh' flag
%
%   (9)
%       [...] = DEKAF_nozzleViscousCorrection(...,'noPlot')
%       |--> does not plot the several steps in the convergence of the
%       nozzle
%
%   (10)
%       [...] = DEKAF_nozzleViscousCorrection(...,'fitType',fitType)
%       |--> allows to pass an integer identifying the type of fitting to
%       be performed. The supported values are:
%           0  ---> no fitting
%           1  ---> as done in <a href="matlab:help fit_deltaStar1">fit_deltaStar1</a>
%           2  ---> as done in <a href="matlab:help fit_deltaStar2">fit_deltaStar2</a>
%           3  ---> as done in <a href="matlab:help fit_deltaStar3">fit_deltaStar3</a>
%           4  ---> as done in <a href="matlab:help fit_deltaStar4">fit_deltaStar4</a>
%       default: 4
%
%   (11)
%       [...] = DEKAF_nozzleViscousCorrection(...,'itMax',itMax)
%       |--> allows to specify the maximum number of iterations to be
%       performed (by default 10)
%
%   (12)
%       [...] = DEKAF_nozzleViscousCorrection(...,'relaxFact',relaxFact)
%       |--> allows to specify the relaxation factor on the
%       displacement-thickness correction (by default 0.5)
%
% Inputs:
%   xInv, rInv      [m]         size (N_x x 1)
%       inviscid nozzle geometry using the coordinate system along the
%       nozzle axis direction, and radial (normal) direction to it.
%   deltaStar0      [m]         size (N_x x 1)
%       initial guess of the boundary-layer displacement thickness.
%   p_e0            [Pa]        size (1 x 1)
%       boundary-layer-edge static pressure at the first streamwise
%       location xInv(1).
%   T_e             [K]         size (N_x x 1)
%       boundary-layer-edge static temperature.
%   M_e             [-]         size (N_x x 1)
%       boundary-layer-edge frozen Mach number.
%   T_w             [K]         size (1 x 1)
%       isothermal wall temperature.
%   mixture
%       string identifying the gas mixture to be used. All supported
%       mixtures can be found in <a href="matlab:help getMixture_constants">getMixture_constants</a>.
%   flow
%       string identifying the flow assumption to be used. All supported
%       flows are detailed in <a href="matlab:help setDefaults">setDefaults</a>, but the most interesting ones
%       for nozzles are:
%           'CPG'   - calorically perfect gas (cp=cst)
%           'TPG'   - thermally perfect gas (cp=cp(T))
%       more elaborate chemical or thermal non-equilibrium effects may
%       require the inviscid edge conditions to satisfy certain
%       constraints. For them, contact a developer.
%   modelTransport
%       string identifying the transport model to be employed. All
%       supported transport models are detailed in <a href="matlab:help setDefaults">setDefaults</a>
%   geomTol         [-]         size (1 x 1)
%       tolerance to be employed on the geometry's correction (delta)
%   modelEoS
%       string identifying the model to be employed for the equation of
%       state. All supported models for the EoS are detailed in <a href="matlab:help setDefaults">setDefaults</a>
%       Default: 'idealGas'
%   modelTurbulence
%       string identifying the model to be employed for turbulence. All
%       supported turbulence models are detailed in <a href="matlab:help setDefaults">setDefaults</a>
%       Default: 'none'
%
% Outputs:
%   rVis            [m]         size (N_x x 1)
%       viscous nozzle radius, paired with xInv (equivalent to rInv but
%       after the viscous modification).
%   deltaStar       [m]         size (N_x x 1)
%       boundary-layer displacement thickness obtained after the iterative
%       process.
%   conv_vec        [m]         size (N_it x 1)
%       maximum modification of the geometry at each iteration. It
%       corresponds to the vector of convergence values. The simulation
%       stops when the last value in conv_vec is lower than geomTol.
%
% All options passed to DEKAF are identically set as in <a href="matlab:help setDefaults">setDefaults</a>,
% except for:
%       L --------------------> 10
%       eta_i ----------------> 3
%       tol ------------------> 1e-12
%       thermal_BC -----------> 'Twall'
%       G_bc -----------------> T_w
%       cstPr ----------------> false
%       BLproperties ---------> true                    *{1}
%       bGICM4delta99 --------> false
%       coordsys -------------> 'cylindricalInternal'   *{1}
%       transverseCurvature --> true                    *{1}
%       plotRes --------------> false
%       MarchingGlobalConv ---> false
%       mapOpts.N_x ----------> 300
%       xSpacing -------------> 'linear'
%       mapOpts.x_start ------> 1e-6
%       mapOpts.x_end --------> s_e(end)                *{1}
%   *{1} NOT to be modified - required for correct setup of the problem
%
% The explanation of what each of these options actually corresponds to can
% be found in <a href="matlab:help setDefaults">setDefaults</a>
%
% Author: Fernando Miro Miro
% Date: March 2020
% GNU Lesser General Public License 3.0

% -------------------------------------------------------------------------
% DEVELOPER'S NOTES
% 1.  DEKAF refers to the surface coordinates as x, whereas this function
%   assigns x to the position along the nozzle axis, and s to the surface
%   coordinates. Therefore there are several definitions like intel.x_e = s_e;
% -------------------------------------------------------------------------

% Checking optional inputs
[modelEoS,          varargin] = parse_optional_input(varargin,'modelEoS','idealGas');
[modelTurbulence,   varargin] = parse_optional_input(varargin,'modelTurbulence','none');
[additionalOutVars, varargin] = parse_optional_input(varargin,'additionalOutVars',{});
[additionalOptions, varargin] = parse_optional_input(varargin,'additionalOptions',{});
[itMax,             varargin] = parse_optional_input(varargin,'itMax',10);
[fitType,           varargin] = parse_optional_input(varargin,'fitType',4);
[relaxFact,         varargin] = parse_optional_input(varargin,'relaxFact',0.5);
[scaleFact,         varargin] = parse_optional_input(varargin,'guessDeltaStarFromExternalBL',NaN);
bGuessDelta = ~isnan(scaleFact);
[varargin,bKeepMesh]     = find_flagAndRemove('keepMesh',           varargin);
[varargin,bOriginalMesh] = find_flagAndRemove('runOnOriginalMesh',  varargin);
[~       ,bNoPlot]       = find_flagAndRemove('noPlot',             varargin);
interpMeth = 'spline'; % HARDCODE ALERT!!

% populating options structure
options.flow            = flow;
options.mixture         = mixture;
options.modelEoS        = modelEoS;
options.modelTurbulence = modelTurbulence;
options.modelTransport  = modelTransport;

% Establishing protected values
protectedvalue(options,'flow',{'CPG','TPG'});
protectedvalue(options,'modelEoS',{'idealGas'});
protectedvalue(options,'modelTurbulence',{'none'});

% populating intel
xInv = xInv(:);
rInv = rInv(:);
M_e = M_e(:);
T_e = T_e(:);

% populating other options              % basic options
options.L                   = 10;           % Domain size (in non-dimensional eta length)
options.eta_i               = 3;            % Mapping parameter, eta_crit
options.tol                 = 1e-12;        % Convergence tolerance
options.thermal_BC          = 'Twall';      % Wall bc (isothermal)
options.G_bc                = T_w;          % value of the wall temperature
options.cstPr               = false;        % we want a variable Pr
options.BLproperties        = true;         % we need delta99 to be outputted
options.bGICM4delta99       = false;        % .. but we don't want it to take forever
% curvature terms
options.coordsys            = 'cylindricalInternal';
options.transverseCurvature = true;
% display options
options.plotRes             = false;        % we don't want the residual plot
options.MarchingGlobalConv  = false;        % we don't want the global residual plot
options.display             = false;        % do not display the full convergence info at each streamwise station
% marching mapping options
if bOriginalMesh % we pass a customized x, corresponding to s
    options.xSpacing            = 'custom_x';     % mapping in the marching direction
else % we build a linear spacing in x, where we will run DEKAF
    options.mapOpts.N_x         = 300;          % default streamwise resolution
    options.xSpacing            = 'linear';     % mapping in the marching direction
end
options.mapOpts.x_start     = 1e-6;         % initial s position [m]
% additional options
options = populateUserFixedOptions(options,additionalOptions);

% initial guess for deltaStar0 if necessary
if bGuessDelta                                                  % if the user chose to guess deltaStar0
    s_e = [options.mapOpts.x_start;cumsum(sqrt(diff(xInv).^2 + diff(rInv).^2))]; % surface distance
    if bOriginalMesh                                                % we pass a customized x, corresponding to s
        options.mapOpts.x_custom = s_e;                                 % vector of s positions on which to run DEKAF [m]
    else                                                            % we build a linear spacing in x, where we will run DEKAF
        options.mapOpts.x_end = s_e(end);                               % initial s position [m]
    end
    intel.x_e       = s_e;                                          % streamwise surface coordinate paired with the *_e profiles
    intel.rc        = rInv;                                         % nozzle radius (initial guess)
    intel.xcAxis    = xInv;                                         % nozzle axis location
    intel.M_e       = M_e;                                          % Mach number at the first boundary-layer edge pos.
    intel.p_e0      = p_e0;                                         % Pressure at the first boundary-layer edge pos.
    intel.T_e       = T_e;                                          % Temperature at boundary-layer edge
    options.coordsys = 'cylindricalExternal';                       % changing coordinate system to solve the external field
    options.transverseCurvature = false;                            % ... without transverse curvature, otherwise it'll break
    [intelOut,~] = DEKAF(intel,options,'marching');                 % Running DEKAF
    sOut = intelOut.x(1,:).';                                       % surface domain used by DEKAF
    if bKeepMesh                                                    % we want to keep the same original mesh and interpolate onto it
        deltaStar = scaleFact*interp1(sOut,intelOut.deltastar(:),s_e,'spline'); % obtaining displacement thickness
        x = xInv;
        switch fitType
            case 0      % no fit
            case 1;     deltaStar = fit_deltaStar1(x,deltaStar,4);
            case 2;     deltaStar = fit_deltaStar2(x,deltaStar,4);
            case 3;     deltaStar = fit_deltaStar3(x,deltaStar,4);
            case 4;     deltaStar = fit_deltaStar4(x,deltaStar);
            otherwise;  error(['Non-supported fitType (',num2str(fitType),')']);
        end
        r = rInv + deltaStar;                                           % updating r with the obtained deltaStar
        s_e = [options.mapOpts.x_start;cumsum(sqrt(diff(x).^2 + diff(r).^2))]; % surface distance
    else                                                            % we want to update the mesh with the computational domain in DEKAF
        deltaStar = scaleFact*intelOut.deltastar(:);                    % obtaining displacement thickness
        s_e = sOut;                                                     % location along the surface (DEKAF domain)
        x = intelOut.cylCoordFuncs.xcAxis(s_e);                         % obtaining x and r corresponding to the s_e points
        switch fitType
            case 0      % no fit
            case 1;     deltaStar = fit_deltaStar1(x,deltaStar,4);
            case 2;     deltaStar = fit_deltaStar2(x,deltaStar,4);
            case 3;     deltaStar = fit_deltaStar3(x,deltaStar,4);
            case 4;     deltaStar = fit_deltaStar4(x,deltaStar);
            otherwise;  error(['Non-supported fitType (',num2str(fitType),')']);
        end
        r = intelOut.cylCoordFuncs.rc(s_e) + deltaStar;
    end
    options.coordsys = 'cylindricalInternal';                       % setting options back to their standard value
    options.transverseCurvature = true;
else                                                            % if the user has provided deltaStar0
    deltaStar = deltaStar0;                                         % ... we use it
    r = rInv + deltaStar;  
    x = xInv;
    s_e = [options.mapOpts.x_start;cumsum(sqrt(diff(x).^2 + diff(r).^2))]; % surface distance
end

% looping
conv = 1; it=0;                         % initializing
if ~bNoPlot                             % plotting if necessary
    figID = figure;
    plot(xInv,rInv,'k--','displayname','inviscid'); hold on; daspect([1,1,1]); 
    plot(x,r,'k-', 'displayname','it = 0');
end
while conv>geomTol && it<itMax                                  % looping until we reach the desired tolerance on the geometry
    it=it+1;                                                        % increasing counter
    if bOriginalMesh                                                % we pass a customized x, corresponding to s
        options.mapOpts.x_custom = s_e;                                 % vector of s positions on which to run DEKAF [m]
    else                                                            % we build a linear spacing in x, where we will run DEKAF
        options.mapOpts.x_end = s_e(end);                               % initial s position [m]
    end
    intel.rc        = r;                                            % nozzle radius (initial guess)
    intel.xcAxis    = x;                                            % nozzle axis location
    intel.M_e       = interp1(xInv,M_e,x,interpMeth);               % Mach number at the first boundary-layer edge pos.
    intel.p_e0      = p_e0;                                         % Pressure at the first boundary-layer edge pos.
    intel.T_e       = interp1(xInv,T_e,x,interpMeth);               % Temperature at boundary-layer edge
    intel.x_e       = s_e;                                          % streamwise surface coordinate paired with the *_e profiles
    [intelOut,optionsOut] = DEKAF(intel,options,'marching');        % Running DEKAF
    sOut = intelOut.x(1,:).';                                       % surface domain used by DEKAF
    if optionsOut.mapOpts.separation_i_xi==optionsOut.mapOpts.N_x   % no need to restrict domain
        idxCut = optionsOut.mapOpts.N_x;
    else                                                            % domain must be reduced
        idxCut = optionsOut.mapOpts.separation_i_xi-1;
    end
    options.mapOpts.N_x = idxCut;                                   % defining new number of points
    idx2keep  = s_e<=sOut(idxCut);                                  % indices in s_e to keep (before it all broke)
    rOld = r(idx2keep);
    if bKeepMesh                                                    % we want to keep the same original mesh and interpolate onto it
        alpha     = intelOut.cylCoordFuncs.alphac(s_e(idx2keep));       % surface inclination angle
        deltaStar = interp1(sOut,intelOut.deltastar(:),s_e(idx2keep),interpMeth).*cosd(alpha); % obtaining displacement thickness
        x         = xInv(idx2keep);                                     % cutting x to keep only those we need
        switch fitType
            case 0      % no fit
            case 1;     deltaStar = fit_deltaStar1(x,deltaStar,4);
            case 2;     deltaStar = fit_deltaStar2(x,deltaStar,4);
            case 3;     deltaStar = fit_deltaStar3(x,deltaStar,4);
            case 4;     deltaStar = fit_deltaStar4(x,deltaStar);
            otherwise;  error(['Non-supported fitType (',num2str(fitType),')']);
        end
        delta_r   = rInv(idx2keep) + deltaStar - rOld;                  % correction of the geometry
        r         = r(idx2keep) + relaxFact*delta_r;                    % correcting geometry
        s_e       = [options.mapOpts.x_start;cumsum(sqrt(diff(x).^2 + diff(r).^2))]; % surface distance
    else                                                            % we want to update the mesh with the computational domain in DEKAF
        s_e       = sOut;                                               % location along the surface
        alpha     = intelOut.cylCoordFuncs.alphac(s_e);                 % surface inclination angle
        deltaStar = intelOut.deltastar(idx2keep).'.*cosd(alpha);        % obtaining displacement thickness
        x   = intelOut.cylCoordFuncs.xcAxis(s_e);                       % previous geometry
        switch fitType
            case 0      % no fit
            case 1;     deltaStar = fit_deltaStar1(x,deltaStar,4);
            case 2;     deltaStar = fit_deltaStar2(x,deltaStar,4);
            case 3;     deltaStar = fit_deltaStar3(x,deltaStar,4);
            case 4;     deltaStar = fit_deltaStar4(x,deltaStar);
            otherwise;  error(['Non-supported fitType (',num2str(fitType),')']);
        end
        delta_r = interp1(xInv,rInv,x,interpMeth) + deltaStar - r;      % modification of the geometry
        r       = intelOut.cylCoordFuncs.rc(s_e) + relaxFact*delta_r;   % previous geometry with a correction
    end
    conv      = max(abs(delta_r));                                  % computing convergence
    disp(['--------------------------------------------------------------------------',newline,...
          '-- deltaStar converged to ',num2str(conv),'m at it=',num2str(it),newline,...
          '--------------------------------------------------------------------------',newline]);
    if ~bNoPlot                                                     % plotting if necessary
        figure(figID);
        plot(x,r,'Color',two_colors(it,itMax,'Rbw2'),'displayname',['it = ',num2str(it)]); daspect([1,1,1]);
        xlabel('x [m]'); ylabel('r [m]'); lgnd=legend();set(lgnd,'location','northwest'); pause(1);
    end
    conv_vec(it) = conv; %#ok<AGROW>
end
if bKeepMesh;   rVis = r;                                           % we are already in xInv
else;           rVis = interp1(x,r,xInv,interpMeth);                % We must go back to xInv
end

% preparing additional variables to output
varargout{1} = conv_vec;
for ii=length(additionalOutVars):-1:1
    varargout{ii+1} = intelOut.(additionalOutVars{ii});
end

end % DEKAF_nozzleViscousCorrection