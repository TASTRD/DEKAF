function intel = extract_inviscid4DEKAFBL(intel,inviscid,options)
% extract_inviscid4DEKAFBL extracts all the important inviscid edge
% profiles so that they can be used by DEKAF's boundary-layer solver.
%
% Examples:
%   intel = extract_inviscid4DEKAFBL(intel,inviscid,options)
%
% we want to keep the velocity profile and the temperature. All the others
% will be computed by build_edge_values
%
% See also: setDefaults
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

T0 = inviscid.T0;
u0 = inviscid.u0;
rho0 = inviscid.rho0;
h0 = inviscid.h0;

N_x = options.inviscidMap.N_x;
intel.shock_angle = inviscid.shock_angle;

intel.U_e   = real(inviscid.u * u0);
intel.y_se  = real(inviscid.y_s);
intel.p_e   = real(inviscid.p * rho0 * u0^2);
intel.h_e   = real(inviscid.h * h0);
intel.rho_e = real(1./inviscid.u * rho0);  % non-dim. continuity --> rho*u = 1

switch options.numberOfTemp
    case '1T'
        intel.TTrans_e  = real(inviscid.T * T0);
        intel.TRot_e    = intel.TTrans_e;
        intel.TVib_e    = intel.TTrans_e;
        intel.TElec_e   = intel.TTrans_e;
        intel.Tel_e     = intel.TTrans_e;
    case '2T'
        intel.hv_e      = real(inviscid.hv * h0);
        intel.TTrans_e  = real(inviscid.T * T0);
        intel.TRot_e    = intel.TTrans_e;
        intel.TVib_e    = real(inviscid.Tv * T0);
        intel.TElec_e   = intel.TVib_e;
        intel.Tel_e     = intel.TVib_e;
    otherwise
        error(['options.numberOfTemp of ''',options.numberOfTemp,''' is currently not supported!']);
end

% x vector and derivation matrix (used to rebuild the edge conditions
[dX_dx,~] = xMappings(options.xInviscidSpacing,inviscid.x,inviscid.inviscidMap);  % obtaining derivatives of the x mapping
dX = inviscid.dX;
D1 = FDdif(N_x,dX,['backward',num2str(options.inviscid_ox)]);

intel.Dxe = diag(dX_dx) * D1;
intel.x_e = inviscid.x(:); % this is a column vector

end