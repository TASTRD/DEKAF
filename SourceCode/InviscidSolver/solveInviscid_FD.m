function inviscid = solveInviscid_FD(inviscid,options)
% solveInviscid_FD solves the system of equations corresponding to the
% inviscid flowfield using a FD method.
%
% Usage:
%   (1)
%       inviscid = solveInviscid_FD(inviscid,options)
%
% Inputs and outputs:
%   inviscid
%       structure containing all the flowfield quantities needed to solve
%       the invisicid region
%   options
%       classic DEKAF options structure
%
% See also: DEKAF_inviscid, solveInviscid_ode45
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% main loop
N_x = options.inviscidMap.N_x;
Ndisp = 10; disp_idx = round(linspace(1,N_x,Ndisp+1)); disp_idx(1) = []; idsp=0; % display variables
for it = 2:N_x
    % computing properties
    inviscid = eval_inviscidThermo(inviscid,options,it);

    % computing matrices
    [mat,forc] = build_inviscidMat(inviscid,options,it);

    % solving system
    sol = mat\forc;

    % updating solution
    inviscid = update_inviscidSolution(inviscid,options,sol,it);

    % checking for NaNs
    if nnz(isnan(sol));             error('the solution NaNed');
    elseif ismember(it,disp_idx);   idsp = idsp+1; disp(['--- ',num2str(idsp*100/Ndisp),'% completed, x = ',num2str(inviscid.x(it)),' m']);
    end
end

% Final evaluation
inviscid = eval_inviscidThermo(inviscid,options,N_x+1);

end % solveInviscid_FD