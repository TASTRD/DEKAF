function [intel,options] = DEKAF_inviscid(intel,options)
% DEKAF_inviscid solves the inviscid 1-D problem at the boundary-layer edge
% for a flat plate.
%
% Examples:
%   [intel,options] = DEKAF_inviscid(intel,options)
%
% See also: setDefaults
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

tic;
% checking that the user did not ask for CPG
if ~ismember(options.flow,{'CNE'}) && ~(ismember(options.flow,{'TPGD','TPG'}) && ismember(options.numberOfTemp,{'2T'}))
    error('This is a thermo-chemical non-equilibrium 1-D solver. It therefore requires either a CNE flow assumption, or a TPG/TPGD flow assumption with options.numberOfTemp = ''2T''.');
end
options.EoS = protectedvalue(options,'EoS','idealGas');

% Reading intel and preparing inviscid structure
[inviscid,intel] = build_inviscid_props(intel,options);

% Displaying message
displayInviscidLogo(inviscid,options);

switch options.inviscidMethod
    case 'ode45';   inviscid = solveInviscid_ode45(inviscid,options);
    case 'FD';      inviscid = solveInviscid_FD(inviscid,options);
    otherwise
        error(['the chosen value of options.inviscidMethod ''',options.inviscidMethod,''' is not supported']);
end

% extracting only the variables that we want for build_edge_values
intel = extract_inviscid4DEKAFBL(intel,inviscid,options);

runtime = toc;
disp(['--- Runtime of ',num2str(runtime),' seconds']);
displayEpilog('Computed with');
