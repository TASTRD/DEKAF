function inviscid = init_inviscid_flds(U_e2,h_e2,TTrans_e2,rho_e2,p_e2,y_se2,mixCnst,beta,wedgeCone_angle,options,varargin)
%INIT_INVISCID_FLDS Initialize the fields of the inviscid struct that is
%used in DEKAF's inviscid solver.
%
% Usage:
% (1) For one-temperature gas mixtures
%   inviscid = ...
%     init_inviscid_flds(U_e2,h_e2,TTrans_e2,rho_e2,p_e2,y_se2,mixCnst,beta,...
%                                                   wedgeCone_angle,options);
%
% (2) For two-temperature gas mixtures
%   inviscid = ...
%     init_inviscid_flds(U_e2,h_e2,TTrans_e2,rho_e2,p_e2,y_se2,mixCnst,beta,...
%                                       wedgeCone_angle,options,TVib_e2,hv_e2);
%
% See also build_invisicid_props
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
% GNU Lesser General Public License 3.0

%% Extract necessary quantities
switch options.numberOfTemp
    case '1T' % Nothing special to do here...
    case '2T' % Translational+Rotational & Vibrational+Electronic+Electron
        if isempty(varargin)
            error('Missing inputs for two-temperature case! See Usage (2).')
        end
        TVib_e2 = varargin{1};
        hv_e2   = varargin{2};
    otherwise
        error(['options.numberOfTemp of ''',options.numberOfTemp,''' is not currently supported!']);
end

N_x     = options.inviscidMap.N_x;
N_spec  = mixCnst.N_spec;

%% Mapping properties
[inviscid.x,inviscid.dX] = xMappings(options.xInviscidSpacing,'get_x',options.inviscidMap);
inviscid.inviscidMap = options.inviscidMap;

%% Post-shock properties
% Nondimensionalize quantities w.r.t first post-shock value
% These vectors represent the initial guess for the Newton-Raphson scheme
% in the inviscid solver.
inviscid.u0     = U_e2;
inviscid.h0     = h_e2;
inviscid.T0     = TTrans_e2;
inviscid.rho0   = rho_e2;
inviscid.y_s0   = y_se2;
inviscid.Ec     = U_e2^2 / h_e2;
inviscid.mom0 = 1 + p_e2 / (rho_e2*U_e2^2);

% allocating computational vectors
inviscid.u  = repmat( U_e2 / inviscid.u0, [N_x,1] );
inviscid.h  = repmat( h_e2 / inviscid.h0, [N_x,1] );
inviscid.T  = repmat( TTrans_e2 / inviscid.T0, [N_x,1] );
inviscid.y_s= repmat( y_se2 , [N_x,1] );
inviscid.p          = ones(N_x,1);
inviscid.Omega_s    = ones(N_x,N_spec);

switch options.numberOfTemp
    case '1T'
        % nothing else needed
    case '2T' % Translational+Rotational & Vibrational+Electronic+Electron
        inviscid.Tv0    = TVib_e2;
        inviscid.Tv     = repmat( TVib_e2 / inviscid.T0, [N_x,1] );         % not a value of 1 downstream
        inviscid.hv0    = hv_e2;
        inviscid.hv     = repmat( hv_e2 / inviscid.h0, [N_x,1] );
        inviscid.dp_dhv  = ones(N_x,1);
        inviscid.Omegav  = ones(N_x,1);
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported.']);
end

%% Miscellaneous properties
inviscid.L          = options.inviscidMap.x_end;
inviscid.mixCnst    = mixCnst;
inviscid.shock_angle= beta;
inviscid.wedgeCone_angle= wedgeCone_angle;

end % init_inviscid_flds
