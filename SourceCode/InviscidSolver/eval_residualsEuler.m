function [eqs,eq_names] = eval_residualsEuler(intel,options,varargin)
% eval_residualsInviscid evaluates the residuals of the 1-D Euler
% non-equilibrium equations.
%
% Usage:
%   (1)  --> basic
%       [eqs,eq_names] = eval_residualsEuler(intel,options)
%
%   (2)  --> passing vectors
%       [eqs,eq_names] = eval_residualsEuler('passVecs',options,x,U,p,TTrans,TVib,TRot,TElec,Tel,...
%                                               y_s,drho_dx,dU_dx,dp_dx,dys_dx,dTv_dx,dT_dx,mixCnst)
%
% Inputs:
%   intel, options
%       classic DEKAF structures
%
% Outpus:
%   eq_res
%       cell with the residuals of each equation as a function of the
%       streamwise location.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if ischar(intel) && strcmp(intel,'passVecs')
    bPassVecs = true;
    ic=1;
    x           = varargin{ic}; ic=ic+1;
    U           = varargin{ic}; ic=ic+1;
    p           = varargin{ic}; ic=ic+1;
    TTrans      = varargin{ic}; ic=ic+1;
    TVib        = varargin{ic}; ic=ic+1;
    TRot        = varargin{ic}; ic=ic+1;
    TElec       = varargin{ic}; ic=ic+1;
    Tel         = varargin{ic}; ic=ic+1;
    y_s         = varargin{ic}; ic=ic+1;
    drho_dx     = varargin{ic}; ic=ic+1;
    dU_dx       = varargin{ic}; ic=ic+1;
    dp_dx       = varargin{ic}; ic=ic+1;
    dys_dx      = varargin{ic}; ic=ic+1;
    dTv_dx      = varargin{ic}; ic=ic+1;
    dT_dx       = varargin{ic}; ic=ic+1;
    mixCnst     = varargin{ic}; %ic=ic+1;
else
    bPassVecs = false;
    x      = intel.x_e(:);
    Dx     = intel.Dxe;
    U      = intel.U_e(:);
    p      = intel.p_e(:);
    TTrans = intel.TTrans_e(:);
    TVib   = intel.TVib_e(:);
    TRot   = intel.TRot_e(:);
    TElec  = intel.TElec_e(:);
    Tel    = intel.Tel_e(:);
    y_s    = intel.y_se;
    mixCnst = intel.mixCnst;
end
N_spec = mixCnst.N_spec;

rho = getMixture_rho(y_s,p,TTrans,Tel,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron);

[h,~,~,h_s,~,~,~,hv,cptr,cpv,~,hv_s] = getMixture_h_cp_noMat(y_s,TTrans,TRot,TVib,TElec,Tel,mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,options);

if ismember(options.flow,{'CNE'})
    [source_s,~,~,source_sr] = getSpecies_source(TTrans,TRot,TVib,TElec,Tel,y_s,rho,mixCnst.Mm_s,mixCnst.nu_reac,mixCnst.nu_prod,mixCnst.A_r,mixCnst.nT_r,mixCnst.theta_r,mixCnst.qf2T_r,mixCnst.qb2T_r,mixCnst.Keq_struc,options);
else
    source_s = zeros(size(y_s));
    source_sr = zeros([size(y_s),0]);
end
switch options.numberOfTemp
    case '1T'
        sourcev = NaN(size(h));
    case '2T'
sourcev = getMixture_sourcev(TTrans,TVib,TElec,Tel,y_s,p,rho,source_s,mixCnst.Mm_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.R_s,mixCnst.aVib_sl,mixCnst.bVib_sl,mixCnst.sigmaVib_s,mixCnst.bMol,mixCnst.bElectron, ...
                            mixCnst.R0,mixCnst.nAvogadro,mixCnst.pAtm,source_sr,mixCnst.hIon_s,mixCnst.kBoltz,mixCnst.qEl,mixCnst.epsilon0,mixCnst.consts_Omega11,mixCnst.bPos,mixCnst.bIonelim,mixCnst.bIonHeavyim,mixCnst.bDissel,mixCnst.bIonAssoc,options);
    otherwise
        error(['The chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

% computing derivatives
if bPassVecs
    dh_dx  = cptr.*dT_dx + cpv.*dTv_dx + (h_s .*dys_dx)*ones(N_spec,1);
    dhv_dx =               cpv.*dTv_dx + (hv_s.*dys_dx)*ones(N_spec,1);
else
    for s=1:N_spec
    dys_dx(:,s)  = Dx*y_s(:,s);
    end
    dh_dx   = Dx*h;
    dhv_dx  = cpv.*(Dx*TVib) + (hv_s.*dys_dx)*ones(N_spec,1);
%     dhv_dx  = Dx*hv;
    drho_dx = Dx*rho;
    dU_dx   = Dx*U;
    dp_dx   = Dx*p;
end
                        
cont0 = (rho(1).*U(1))/x(end);
mom0  = (p(1) + 0.5*rho(1).*U(1)^2)/x(end);
enr0  = (h(1) + 0.5*U(1).^2)/x(end);
spc0  = cont0;
venr0 = max([sourcev;rho(:).*U(:).*hv(:)/x(end)]);

ic=0;
ic=ic+1; eqs{ic} = (drho_dx(:).*U(:) + rho(:).*dU_dx(:))/cont0;       % continuity equation
eq_names{ic} = 'continuity';
ic=ic+1; eqs{ic} = (dp_dx(:) + rho(:).*U(:).*(dU_dx(:)))/mom0;        % momentum equation
eq_names{ic} = 'momentum';
ic=ic+1; eqs{ic} = (dh_dx(:) + U(:).*dU_dx(:))/enr0;                  % energy equation
eq_names{ic} = 'total energy';
for s=1:N_spec
ic=ic+1; eqs{ic} = (rho(:).*U(:).*dys_dx(:,s) - source_s(:,s))/spc0;  % species equation
eq_names{ic} = ['species ',mixCnst.spec_list{s}];
end
ic=ic+1; eqs{ic} = (rho(:).*U(:).*dhv_dx - sourcev)/venr0;            % vib-elec-el energy equation
eq_names{ic} = 'vib-elec-el energy';

end % eval_residualsInviscid