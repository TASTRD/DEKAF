function [TTrans_e2,TRot_e2,TVib_e2,TElec_e2,Tel_e2,...
    y_se2,p_e2,U_e2,beta,intel] = build_post_shock(intel,options,wedgeCone_angle)
%BUILD_POST_SHOCK_CONDITIONS Construct local variables for post shock
% conditions depending on whether the supplied data is for pre-shock or
% post-shock.
%
% Usage:
%   1. [TTrans_e2,TRot_e2,TVib_e2,TElec_e2,Tel_e2,...
%       y_se2,p_e2,U_e2,beta,intel] = build_post_shock(intel,options,wedgeCone_angle)
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% Assemble pre-shock values
[TTrans_e1,TRot_e1,TVib_e1,TElec_e1,Tel_e1,p_e1,M_e1,U_e1,gam_e1,y_se1,R_e1,rho_e1,h_e1,X_Ee1,intel] ...
    = init_shock_vars(intel,options); % for LTE and LTEED y_se1 is actually X_Ee1

% Save preshock values
intel.M_inf     = M_e1;
intel.U_inf     = U_e1;
intel.rho_inf   = rho_e1;
intel.p_inf     = p_e1;
intel.T_inf     = TTrans_e1;
intel.h_inf     = h_e1;
intel.ys_inf    = matrix2D2cell1D(y_se1); % for LTE and LTEED y_se1 is actually X_Ee1

% Depending on the flow assumption we must provide different things
switch options.noseFlow
    case {'CPG','TPG','TPGD','CNE'};    XY_se1 = y_se1;
    case {'LTE','LTEED'};               XY_se1 = X_Ee1;
    otherwise;                          error(['The chosen flow assumption ''',options.flow,''' is not supported']);
end

% Jumping shock
[U_e2,TTrans_e2,TRot_e2,TVib_e2,TElec_e2,Tel_e2,p_e2,y_se2,beta] = eval_shockJump(U_e1,TTrans_e1,TRot_e1,TVib_e1,TElec_e1,Tel_e1,p_e1,XY_se1,gam_e1,R_e1,wedgeCone_angle,intel.mixCnst,options);

end % build_post_shock
