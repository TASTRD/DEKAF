function [A,b] = build_inviscidMat(inviscid,options,it)
% build_inviscidMat builds the system matrices to solve the 1D inviscid
% problem
%
% Examples:
%
%   [A,b] = build_inviscidMat(inviscid,options)
%
% It effectively solves the species conservation, the mixture continuity,
% the x momentum, the total energy and the vibrational energy Euler
% equations, using as state quantities:
%
%       y_s         mass fractions                  [N_x x N_spec]
%       rho         mixture density                 [N_x x 1]
%       u           streamwise velocity             [N_x x 1]
%       h           mixture total static enthalpy   [N_x x 1]
%       hv          mixture vibrational enthalpy    [N_x x 1]
%
% The computational matrix will have the form:
%
%   _                                                                                                                  _     _        _
%  |                                                                                                                    |   |          |
%  |   spec(1),ys(1)    . . .    spec(1),ys(Ns-1)      spec(1),ys(Ns)           0            0               0          |   |  ys(1)   |
%  |        .           .               .                      .                .            .               .          |   |    .     |
%  |        .             .             .                      .                .            .               .          |   |    .     |
%  |        .               .           .                      .                .            .               .          |   |    .     |
%  |                                                                                                                    |   |          |
%  |  spec(Ns-1),ys(1)  . . .   spec(Ns-1),ys(Ns-1)   spec(Ns-1),ys(Ns)         0             0               0         |   | ys(Ns-1) |
%  |                                                                                                                    |   |          |
%  |        1           . . .           1                     1                 0             0               0         |   |  ys(Ns)  |
%  |                                                                                                                    |   |          |
%  |     mom,ys(1)      . . .       mom,ys(Ns-1)          mom,ys(Ns)         momX,u         momX,h         momX,hv      |   |    u     |
%  |                                                                                                                    |   |          |
%  |        0           . . .           0                     0               enr,u         enr,h             0         |   |    h     |
%  |                                                                                                                    |   |          |
%  |        0           . . .           0                     0                 0             0            vibEnr,hv    |   |    hv    |
%  |_                                                                                                                  _|   |_        _|
%
% FIXME: outdated - wrong location of some of the blanks
%
% See also: setDefaults
%
% Author: Fernando Miro Miro
%         Ethan Beyak
% GNU Lesser General Public License 3.0

% note on indexing: "it" corresponds to position i+1 in the marching.
% we will flip the stencil so that we have first the term multiplying the
% oldest term (it-ox) and finally the term multiplying it

% options and others
N_spec = inviscid.mixCnst.N_spec;
icv = 0; % initialize
ox0 = options.inviscid_ox;
dX = inviscid.dX;
if it<=ox0
    ox = it-1;
    idx_ox = 1:it-1; % index of the positions in the variable vector to be used for the RHS (pos. i, i-1, etc.)
else
    ox = ox0;
    idx_ox = it-ox:it-1;
end
idx_FD = 1:ox;

% Fields
Ec = inviscid.Ec;
y_s = inviscid.y_s;
u = inviscid.u;
h = inviscid.h;
mom0 = inviscid.mom0;
p = inviscid.p;
Omega_s = inviscid.Omega_s;

idx_u = N_spec+icv+1; icv = icv+1;
idx_h = N_spec+icv+1; icv = icv+1;
if strcmp(options.numberOfTemp,'2T') % additional fields for 2-T
    hv = inviscid.hv;
    Omegav = inviscid.Omegav;
    idx_hv = N_spec+icv+1; icv = icv+1;
end

% obtaining FD stencil
L = inviscid.L;                                                                        % final position of the domain (used to non-dimensionalize the x derivatives)
[dX_dx,~] = xMappings(options.xInviscidSpacing,inviscid.x(it),inviscid.inviscidMap);   % obtaining derivatives of the x mapping
[FD,scaling] = FDstencil(ox);
FDc = dX_dx * flip(FD)/(scaling*dX) * L;

% building matrices
A = zeros(N_spec+icv,N_spec+icv);                                       % initializing

A(1:N_spec-1,1:N_spec-1) = eye(N_spec-1) * FDc(end);                        % diagonal of the species equations

A(N_spec,1:N_spec) = 1;                                                     % concentration condition
A(idx_u,idx_u) = 1;

A(idx_h,idx_u) = Ec*u(it-1)*FDc(end);                                       % terms multiplying u in the energy equation
A(idx_h,idx_h) = FDc(end);                                                  % terms multiplying h in the energy equation

if strcmp(options.numberOfTemp,'2T')                                        % additional fields for 2-T
    A(idx_hv,idx_hv) = FDc(end);                                                % terms multiplying hv in the vibrational energy equation
end

% building forcing term
b = zeros(N_spec+icv,1);                                                  % initializing
b(1:N_spec-1) = Omega_s(it-1,1:N_spec-1).' - (FDc(idx_FD) * y_s(idx_ox,1:N_spec-1)).'; % species conservation
b(N_spec) = 1;                                                              % concentration condition
b(idx_u) = mom0 - p(it-1); % momentum
b(idx_h) = -FDc(idx_FD) * h(idx_ox) - Ec*u(it-1) * FDc(idx_FD)*u(idx_ox);   % energy
if strcmp(options.numberOfTemp,'2T')                                        % additional fields for 2-T
    b(idx_hv) = Omegav(it-1) - FDc(idx_FD) * hv(idx_ox);                        % vibrational energy
end

end % build_inviscidMat