function [matBC,forcBC] = build_inviscidBC(mat,forc,inviscid,options)
% build_inviscidBC imposes the necessary boundary conditions for the
% inviscid problem.
%
% Examples:
%   [matBC,forcBC] = build_inviscidBC(mat,forc,inviscid,options);
%
% See also: setDefaults
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_spec = inviscid.mixCnst.N_spec;
N_x = options.N_x;
shockPos = inviscid.shockPos;

% Mass fractions
for s=1:N_spec
    idx_s = (s-1)*N_x + shockPos;
    mat(idx_s,:) = 0;
    mat(idx_s,idx_s) = 1;
    forc(idx_s) = 0;
end
N_var = 0; % initializing counter

% density
% idx_rho = (N_spec+N_var)*N_x + shockPos; N_var = N_var+1;
% mat(idx_rho,:) = 0;
% mat(idx_rho,idx_rho) = 1;
% forc(idx_rho) = 0;

% velocity
idx_u = (N_spec+N_var)*N_x + shockPos; N_var = N_var+1;
mat(idx_u,:) = 0;
mat(idx_u,idx_u) = 1;
forc(idx_u) = 0;

% total enthalpy
idx_h = (N_spec+N_var)*N_x + shockPos; N_var = N_var+1;
mat(idx_h,:) = 0;
mat(idx_h,idx_h) = 1;
forc(idx_h) = 0;

if strcmp(options.numberOfTemp,'2T')
    % vibrational enthalpy
    idx_hv = (N_spec+N_var)*N_x + shockPos; %N_var = N_var+1;
    mat(idx_hv,:) = 0;
    mat(idx_hv,idx_hv) = 1;
    forc(idx_hv) = 0;
end

matBC = mat;
forcBC = forc;