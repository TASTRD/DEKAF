function check_inviscidInputs(intel,options)
%CHECK_INVISCIDINPUTS Error checking for the building-inviscid-properties
% script
%
% Usage:
%   check_inviscidInputs(intel,options);
%
% Author(s): Ethan Beyak
% GNU Lesser General Public License 3.0

% Check to make sure _e variables do NOT exist
if isfield(intel,'T_e')
    error('intel.T_e is defined at the first iteration. intel.T_e0 must be defined!');
elseif isfield(intel,'p_e')
    error('intel.p_e is defined at the first iteration. intel.p_e0 must be defined!');
elseif isfield(intel,'Re1_e')
    error('intel.Re1_e is defined at the first iteration. intel.Re1_e0 must be defined!');
elseif isfield(intel,'ys_e')
    error('intel.ys_e is defined at the first iteration. intel.ys_e0 must be defined!');
end

% T_e0 is always imposed. If not, fail.
if ~isfield(intel,'T_e0')
    error('the input intel.T_e0 was not defined and is necessary!');
end
switch options.numberOfTemp
    case '2T' % Translational+Rotational & Vibrational+Electronic+Electron
        if ~isfield(intel,'Tv_e0')
            error('the input intel.Tv_e0 was not defined and is necessary!');
        end
end

% Check that both p_e0 and Re1_e0 exist. If so, fail.
if isfield(intel,'p_e0') && isfield(intel,'Re1_e0')
    error('both inputs intel.p_e0 and intel.Re1_e0 are defined! This condition is not supported!');
end

% Check that both M_e0 and U_e0 exist. If so, fail.
if isfield(intel,'U_e0') && isfield(intel,'M_e0')
    error('both inputs intel.U_e0 and intel.M_e0 are defined! This condition is not supported!');
end

% If user is supplying pre-shock conditions, check necessary fields.
if options.shockJump
    switch options.coordsys
        case 'cartesian2D'
            if ~isfield(intel,'wedge_angle')
                error('the input intel.wedge_angle was not defined and is necessary for pre-shock conditions!');
            end
        case 'cone'
            if ~isfield(intel,'cone_angle')
                error('the input intel.cone_angle was not defined and is necessary for pre-shock conditions!');
            end
        otherwise
            error(['the choosen coordsys ''',options.coordsys,''' is not supported']);
    end

end


end % check_inviscidInputs
