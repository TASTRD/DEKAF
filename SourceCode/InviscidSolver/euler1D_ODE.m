function dydx = euler1D_ODE(x,y,mixCnst,options)
% euler1D_ODE is the ordinary differential equation corresponding to the
% ODE of non-equilibrium 1D Euler.
%
% Usage:
%   (1)
%       dydx = euler1D_ODE(x,y,mixCnst,options)
%
% Inputs and outputs:
%   x
%       independent variable (streamwise position) [m]
%   y, dy
%       dependent variable vector and its derivative wrt x. Variables:
%           Tv      --->    vib-elec-el temperature [K] x 1 (only 2T)
%           y_s     --->    mass fractions [-]          x N_spec
%           T       --->    temperature [K]             x 1
%           u       --->    velocity [m/s]              x 1
%           rho     --->    density [kg/m^3]            x 1
%   mixCnst
%       structure containing the mixture constants
%   options
%       structure containing the system options
%
% IMPORTANT: inputs and outputs are columns
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% Extracting inputs
y = y.';                                                                    % making input into a row
N_spec = mixCnst.N_spec;
N_reac = length(mixCnst.A_r);
ic=1;
switch options.numberOfTemp
    case '1T'
    case '2T';  Tv = y(ic); ic=ic+1;
    otherwise;  error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
end
y_s = y(ic:ic+N_spec-1); ic=ic+N_spec;
T   = y(ic); ic=ic+1;
u   = y(ic); ic=ic+1;
rho = y(ic); %ic=ic+1;

% Computing properties
switch options.numberOfTemp
    case '1T'                                                               % one-temperature models (no Tv)
        dydx  = zeros(1,N_spec+3);                                              % allocating output
        Tv = T;                                                                 % vibrational temperature equal to trans-rot
[~,cp,~,h_s,~,~,~,~,~,cpv] = getMixture_h_cp_noMat(y_s,T,T,T,T,T,mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s, ...
                                                                              mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,options,mixCnst.AThermFuncs_s);
    case '2T'                                                               % two-temperature models
        dydx  = zeros(1,N_spec+4);                                              % allocating output
[~,~,~,h_s,~,~,~,~,cp,cpv] = getMixture_h_cp_noMat(y_s,T,T,Tv,Tv,Tv,mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s, ...
                                                                              mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,options,mixCnst.AThermFuncs_s);
    otherwise
        error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
end
p = getMixture_p(y_s,rho,T,Tv,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options);
if strcmp(options.flow,'CNE') && ~isempty(mixCnst.A_r)
    [source_s,~,~,source_sr] = getSpecies_source(T,T,Tv,Tv,Tv,y_s,rho,mixCnst.Mm_s,mixCnst.nu_reac,mixCnst.nu_prod,mixCnst.A_r,mixCnst.nT_r,mixCnst.theta_r,mixCnst.qf2T_r,mixCnst.qb2T_r,mixCnst.Keq_struc,options);
else
    source_s = zeros(1,N_spec);     source_sr = zeros(1,N_spec,N_reac);
end
[dp_drho,dp_dys,dp_dT,dp_dTv]   = getMixtureDer_dp_dall(rho,T,Tv,y_s,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron);

% differential equation
ic=1; % initializing index counter
switch options.numberOfTemp
    case '1T'                                                               % one-temperature models (no Tv)
        fact_dTvdx_Ener = 0;    fact_dTvdx_MomX = 0;                            % factors multiplying dTv/dx (empty)
    case '2T'                                                               % two-temperature models
        sourcev = getMixture_sourcev(T,Tv,Tv,Tv,y_s,p,rho,source_s,mixCnst.Mm_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,...
                                    mixCnst.R_s,mixCnst.aVib_sl,mixCnst.bVib_sl,mixCnst.sigmaVib_s,mixCnst.bMol,mixCnst.bElectron,mixCnst.R0,mixCnst.nAvogadro,mixCnst.pAtm,...
                                    source_sr,mixCnst.hIon_s,mixCnst.kBoltz,mixCnst.qEl,mixCnst.epsilon0,mixCnst.consts_Omega11,mixCnst.bPos,mixCnst.bIonelim,mixCnst.bIonHeavyim,...
                                    mixCnst.bDissel,mixCnst.bIonAssoc,options);
        % Vibrational energy equation --> dTv/dx
        denomVib = rho.*u.*cpv;                                                 % Vibrational energy equation denominator
        dydx(ic) = 1./denomVib .* (sourcev); ic=ic+1;
        fact_dTvdx_Ener = cpv-u.*dp_dTv./(rho.*u - rho./u.*dp_drho);            % factors multiplying dTv/dx
        fact_dTvdx_MomX = dp_dTv;
    otherwise
        error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
end
% Species concentration equation --> dys/dx
denomSpec = rho.*u;                                                         % Species concentration equation denominator
dydx(ic:ic+N_spec-2) = 1./denomSpec .* (source_s(1:N_spec-1)); ic=ic+N_spec-1;
dydx(ic) = -dydx(ic-N_spec+1:ic-1) * ones(N_spec-1,1); ic=ic+1;             % Concentration condition
%           dys/dx
% Energy equation --> dT/dx
denomEner = cp - u.*dp_dT./(rho.*u - rho./u.*dp_drho);                      % Energy equation denominator
dydx(ic) = -1./denomEner .* ((dydx(ic-N_spec:ic-1) .* (h_s-u.*dp_dys./(rho.*u - rho./u.*dp_drho)))*ones(N_spec,1) + dydx(1) .* fact_dTvdx_Ener); ic=ic+1; % Energy equation
%                             dys/dx                                                                                dTv/dx    (=0 for 1T)
% X-momentum equation --> du/dx
denomMomX = rho.*u - rho./u.*dp_drho;                                       % X-momentum equation denominator
dydx(ic) = -1./(denomMomX) .* (dydx(ic-1).*dp_dT + (dydx(ic-N_spec-1:ic-2).*dp_dys)*ones(N_spec,1) + dydx(1) .* fact_dTvdx_MomX); ic=ic+1;
%                               dT/dx               dys/dx                                            dTv/dx    (=0 for 1T)
% Continuity equation --> drho/dx
denomCont = u;                                                              % Continuity equation denominator
dydx(ic) = -1./denomCont .* dydx(ic-1).*rho;
%                            du/dx

dydx=dydx.'; % making into a column

end % euler1D_ODE