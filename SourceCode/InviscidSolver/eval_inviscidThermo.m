function inviscid = eval_inviscidThermo(inviscid,options,it)
%EVAL_INVISCIDTHERMO Evaluate thermal properties necessary for the inviscid
%solver.
%
% Important Inputs and Outputs of inviscid and options:
%
% inviscid
%   y_s                     N_x x N_spec    (normalized)
%   rho                     N_x x 1         (non-dim)
%   u                       N_x x 1         (non-dim)
%   h                       N_x x 1         (non-dim)
%   T                       N_x x 1         (non-dim)
%   hv *2                   N_x x 1         (non-dim)
%   Tv *2                   N_x x 1         (non-dim)
%   mixCnst                 structure
%     *2 numberOfTemp = '2T'
%
% Developers note: D stands for dimensional quantities
%
% See also: setDefaults
%
% Author: Fernando Miro Miro
%         Ethan Beyak
% GNU Lesser General Public License 3.0

% Extract mixture constants
mixCnst = inviscid.mixCnst;
fldnmsMix = fieldnames(mixCnst);
for ii=1:length(fldnmsMix)
    eval([fldnmsMix{ii},' = mixCnst.',fldnmsMix{ii},';']);
end

% sizes and options
N_spec = mixCnst.N_spec;

% extracting variables and making them dimensional
rho0    = inviscid.rho0;
u0      = inviscid.u0;
h0      = inviscid.h0;
T0      = inviscid.T0;
L       = inviscid.L;

y_s     = inviscid.y_s(it-1,:);  % non-dimensional quantities
u       = inviscid.u(it-1);
h       = inviscid.h(it-1);

y_sD    = y_s;        % redimensionalizing
rhoD    = 1./u*rho0;  % non-dim. continuity --> rho*u = 1
hD      = h*h0;

if strcmp(options.numberOfTemp,'2T')
    hv0 = inviscid.hv0;
    Tv0 = inviscid.Tv0;
    hv = inviscid.hv(it-1);
    hvD = hv*h0;
end

% Partition the temperatures depending on the thermal model chosen
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,...
    hForm_s_mat,~] = reshape_inputs4enthalpy(R_s,nAtoms_s,thetaVib_s,thetaElec_s,...
    gDegen_s,bElectron,hForm_s,hD); % hD is only passed so that reshape_inputs has the target N_x size
switch options.numberOfTemp
    case '1T' % All grouped with translational temperature
        %%% Newton-Raphson on the h, to obtain T
        if ~isfield(inviscid,'Tguess')
            Tn0      = hD/h0*T0;    % Initial guess for temperature T (CPG relation)
        else
            Tn0 = inviscid.Tguess;
        end
        Tn = get_NRTfromh(hD,y_sD,Tn0,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,...
            thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);

        % Assign temperatures
        TTransD = Tn;
        TRotD = Tn;
        TVibD = Tn;
        TElecD = Tn;
        TelD = Tn;
    case '2T' % Translational+Rotational & Vibrational+Electronic+Electron
        %%% Newton-Raphson on the hv, to obtain Tv
        if ~isfield(inviscid,'Tvguess')
            Tvn0      = hvD/hv0*Tv0;    % Initial guess for temperature Tv (CPG relation)
        else
            Tvn0 = inviscid.Tvguess;
        end
        [Tn,Tvn] = get_NRTTvfromhhv(hD,hvD,y_sD,Tvn0,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,...
                            thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options);

        % Assign temperatures
        TTransD = Tn;
        TRotD = Tn;
        TVibD = Tvn;
        TElecD = Tvn;
        TelD = Tvn;
    otherwise
        error(['options.numberOfTemp = ''',options.numberOfTemp,''' is not currently supported!']);
end

% pressure & derivatives
pD           = getMixture_p(y_sD,rhoD,TTransD,TelD,R0,Mm_s,bElectron,mixCnst.EoS_params,options);

% Species mass source term
if strcmp(options.flow,'CNE') && ~isempty(A_r)
    source_sD  = getSpecies_source(TTransD,TRotD,TVibD,TElecD,TelD,y_sD,...
                    rhoD,Mm_s,nu_reac,nu_prod,A_r,nT_r,theta_r,qf2T_r,qb2T_r,Keq_struc,options);
else
    source_sD = zeros(1,N_spec);
end

% Vibrational source term
if strcmp(options.numberOfTemp,'2T')
    sourcevD = getMixture_sourcev(TTransD,TVibD,TElecD,TelD,y_sD,pD,rhoD,source_sD,Mm_s,thetaVib_s,thetaElec_s,gDegen_s,R_s,...
                                                                aVib_sl,bVib_sl,sigmaVib_s,bMol,bElectron,R0,nAvogadro,pAtm,options);
%     sourcevD = 0;
end

% Saving non-dimensional quantities into inviscid
inviscid.p(it-1,1)              = pD / (rho0*u0^2);
inviscid.Omega_s(it-1,:)        = source_sD * L./(u0*rho0);
% inviscid.Omega_s(it-1,:)        = 0;
inviscid.T(it-1)                = TTransD / T0;              % we also save the non-dimensional temperature, to output it at the end to build_edge_values
inviscid.Tguess                 = TTransD;
switch options.numberOfTemp
    case '2T'
%         inviscid.dp_dhv(it-1)      = dp_dhvD * h0 / (rho0*u0^2);
        inviscid.Omegav(it-1)      = sourcevD * L/(rho0*u0*h0);
        inviscid.Tv(it-1)          = TVibD / T0;
        inviscid.Tvguess           = TVibD;
end

end % eval_inviscidThermo
