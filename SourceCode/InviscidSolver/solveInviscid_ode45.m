function inviscid = solveInviscid_ode45(inviscid,options)
% solveInviscid_ode45 solves the system of equations corresponding to the
% inviscid flowfield using matlab's built-in ode45 function.
%
% Usage:
%   (1)
%       inviscid = solveInviscid_ode45(inviscid,options)
%
% Inputs and outputs:
%   inviscid
%       structure containing all the flowfield quantities needed to solve
%       the invisicid region
%   options
%       classic DEKAF options structure
%
% See also: DEKAF_inviscid, solveInviscid_FD
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% extracting from inviscid
mixCnst = inviscid.mixCnst;
N_spec  = mixCnst.N_spec;
x       = inviscid.x;
rho0    = inviscid.rho0;
u0      = inviscid.u0;
T0      = inviscid.T0;
y_s0    = inviscid.y_s0;
switch options.numberOfTemp
    case '1T';  y0 =                [y_s0 , T0 , u0 , rho0].';  NT = 1;
    case '2T';  y0 = [inviscid.Tv0 , y_s0 , T0 , u0 , rho0].';  NT = 2;
    otherwise;  error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
end


% solving and extracting
[~,y_out] = ode45(@(x,y)euler1D_ODE(x,y,mixCnst,options),x,y0);

ic=NT; % index counter initialization
y_s = y_out(:,ic:ic+N_spec-1); ic=ic+N_spec;
T   = y_out(:,ic); ic=ic+1;
u   = y_out(:,ic); ic=ic+1;
rho = y_out(:,ic); %ic=ic+1;
switch options.numberOfTemp
    case '1T';  Tv = T;
    case '2T';  Tv = y_out(:,1);
    otherwise;  error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
end

% repopulating inviscid to return the same format as solveInviscid_FD does
p = getMixture_p(y_s,rho,T,Tv,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options);
[h,~,~,~,~,~,~,hv] = getMixture_h_cp_noMat(y_s,T,T,Tv,Tv,Tv,mixCnst.R_s,mixCnst.nAtoms_s,...
                    mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,options,mixCnst.AThermFuncs_s);
inviscid.u   = u /  inviscid.u0;
inviscid.p   = p / (inviscid.rho0*inviscid.u0^2);
inviscid.T   = T /  inviscid.T0;
inviscid.h   = h /  inviscid.h0;
inviscid.y_s = y_s;
switch options.numberOfTemp
    case '1T'
    case '2T';  inviscid.hv = hv / inviscid.h0;
                inviscid.Tv = Tv / inviscid.T0;
    otherwise;  error(['the chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
end

end % solveInviscid_ode45