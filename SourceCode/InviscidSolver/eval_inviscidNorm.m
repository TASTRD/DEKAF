function [conv,cause,inviscid] = eval_inviscidNorm(inviscid,options,it,update,forcBC)
% eval_inviscidNorm evaluates the norm of the solution vector and the
% variable vector for the inviscid solver, and computes the convergence
% criterion.
%
% Examples:
%   [conv,cause,inviscid] = eval_inviscidNorm(inviscid,options,it,conv,update,forcBC)
%
% See also: setDefaults
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_x = options.N_x;
N_spec = options.N_spec;

if it==1                                                            % initial round, nothing to check
    conv = 1;
    % obtaining the reference of the residual for the equations at the first iteration
    N_var = 1; % density and streamwise velocity
    switch options.numberOfTemp                                     % obtaining number of temperatures
        case '1T'
            N_T = 1;
        case '2T'
            N_T = 2;
        otherwise
            error(['the selected thermal model ''',options.numberOfTemp,''' is not supported']);
    end
    inviscid.refEq = ones(N_x*(N_spec+N_T+N_var),1);                % allocating equations reference values
    for ii=1:N_spec + N_T+N_var                                     % looping equations
        idx_ii = (ii-1)*N_x+1:ii*N_x;                                   % position of the equation ii in forcBC
        inviscid.refEq(idx_ii) = norm(forcBC(idx_ii),Inf)*ones(N_x,1);  % reference value for each equation
    end
    % lower cap on the initial residual (if smaller than one, we set it to
    % one to avoid ridiculously small initial residuals to mess up the
    % convergence)
    inviscid.refEq(inviscid.refEq<1) = 1;
    cause = ' - ';
else
    conv = norm([forcBC./inviscid.refEq ; update],Inf); % computing residual
    % looking for the culprit
    idxEq = find(abs(forcBC./inviscid.refEq)==conv,1,'first');
    idxVar = find(abs(update)==conv,1,'first');
    N_var = 1; % initializing counter
%     idx_rho = (N_spec+N_var)*N_x; N_var = N_var+1;
    idx_u = (N_spec+N_var)*N_x; N_var = N_var+1;
    idx_h = (N_spec+N_var)*N_x; N_var = N_var+1;
    if strcmp(options.numberOfTemp,'2T')
        idx_hv = (N_spec+N_var)*N_x; %N_var = N_var+1;
    else
        idx_hv = 0; % place holder
    end
    if ~isempty(idxEq)                                              % if any equations coincided
        if idxEq<=N_spec*N_x                                            % Species equations
            cause = ['the ',num2str(ceil(idxEq/N_x)),' spec eq.'];
%         elseif idxEq<=idx_rho                                           % Mass equation
%             cause = 'the mass eq.';
        elseif idxEq<=idx_u                                             % x-momentum equation
            cause = 'the x-mom eq.';
        elseif idxEq<=idx_h                                             % Energy equation
            cause = 'the energy eq.';
        elseif idxEq<=idx_hv                                            % Vibrational energy equation
            cause = 'the vib. energy eq.';
        end
    elseif ~isempty(idxVar)                                         % if any variables coincided
        if idxVar<=N_spec*N_x                                           % Species mass frac.
            cause = ['y_{',inviscid.mixCnst.spec_list{ceil(idxVar/N_x)},'}'];
%         elseif idxVar<=idx_rho                                          % Density
%             cause = 'rho';
        elseif idxVar<=idx_u                                            % Velocity
            cause = 'u';
        elseif idxVar<=idx_h                                            % Enthalpy
            cause = 'h';
        elseif idxVar<=idx_hv                                           % Vibrational enthalpy
            cause = 'hv';
        end
    else
        cause = 'unknown';
    end
end