function [inviscid,intel] = build_inviscid_props(intel,options)
%BUILD_INVISCID_PROPS Create the inviscid struct of properties for the edge
% flow solver at the beginning of the x streamwise domain.
%
% Usage:
%   inviscid = build_inviscid_props(intel,options);
%
% Outputs
%   inviscid
%       .u          N_x x 1     nondimensionalized by   .u0     1 x 1
%       .h          N_x x 1                             .h0     1 x 1
%       .T          N_x x 1                             .T0     1 x 1
%       .rho        N_x x 1                             .rho0   1 x 1
%       .y_s        N_x x N_spec                        .y_s0   1 x N_spec
%       .hv         N_x x 1                             .h      1 x 1
%       .mixCnst    (struct)
%       .x          N_x x 1                             .L      1 x 1
%       .D_x        N_x x N_x
%
%   intel
%       removed fields for M_e0 and Re1_e0, if existed in the first place.
%
% Note that this function supports shock-jump relations. If desired, set...
%   options.shockJump = true;
%   intel.wedge_angle = <your_velocity_deflection_angle> (for coordsys = 'cartesian2D')
%   intel.cone_angle  = <your_velocity_deflection_angle> (for coordsys = 'cone')
%
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
% GNU Lesser General Public License 3.0

% Check if all necessary intel inputs exist
check_inviscidInputs(intel,options);

% Extract mixture constants
mixCnst = intel.mixCnst;

%% Pre-shock calculations
% ...or if the user specified post-shock conditions at the get-go, then
% just assemble the right local variables for the succeeding calculations.

if options.shockJump % user-specified pre-shock values
    switch options.coordsys
        case 'cartesian2D';     wedgeCone_angle = intel.wedge_angle;
        case 'cone';            wedgeCone_angle = intel.cone_angle;
        otherwise;              error(['the chosen coordsys ''',options.coordsys,''' is not supported for shock jumps']);
    end
    [TTrans_e2,TRot_e2,TVib_e2,TElec_e2,Tel_e2,...
        y_se2,p_e2,U_e2,beta,intel] = build_post_shock(intel,options,wedgeCone_angle);
else % user-specified post-shock values from the get-go
    [TTrans_e2,TRot_e2,TVib_e2,TElec_e2,Tel_e2,p_e2,~,U_e2,~,y_se2,~,~,~,intel] ...
        = init_shock_vars(intel,options);
    beta = 0; % no shock angle
    wedgeCone_angle = 0;
end

%% Additional post-shock calculations

% Reshape for enthalpy calculation
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
    bElectron_mat,hForm_s_mat,TTranse2_mat,TRote2_mat,TVibe2_mat,TElece2_mat,Tele2_mat] = ...
    reshape_inputs4enthalpy(mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,...
        mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,...
        TTrans_e2,TRot_e2,TVib_e2,TElec_e2,Tel_e2);

% Compute species enthalpies and heat capacities at constant pressure
[h_e2,~,~,~,~,~,~,hv_e2] = getMixture_h_cp(y_se2,...
    TTranse2_mat,TRote2_mat,TVibe2_mat,TElece2_mat,Tele2_mat,R_s_mat,nAtoms_s_mat,...
    thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);

% Mass density
rho_e2 = getMixture_rho(y_se2,p_e2,TTrans_e2,Tel_e2,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options);

%% Store in inviscid
switch options.numberOfTemp
    case '1T'
        inviscid = ...
            init_inviscid_flds(U_e2,h_e2,TTrans_e2,rho_e2,p_e2,y_se2,mixCnst,beta,wedgeCone_angle,options);
    case '2T' % Translational+Rotational & Vibrational+Electronic+Electron
        inviscid = ...
            init_inviscid_flds(U_e2,h_e2,TTrans_e2,rho_e2,p_e2,y_se2,mixCnst,beta,wedgeCone_angle,options,TVib_e2,hv_e2);
    otherwise
        error(['options.numberOfTemp of ''',options.numberOfTemp,''' is not currently supported!']);
end

end % build_inviscid_props
