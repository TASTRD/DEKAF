function [TTrans_e1,TRot_e1,TVib_e1,TElec_e1,Tel_e1,p_e1,M_e1,U_e1,gam_e1,y_se1,R_e1,rho_e1,h_e1,X_Ee1,intel] ...
    = init_shock_vars(intel,options)
%INIT_SHOCK_VARS Initialize the variables for the shock relations for
%either pre-shock or post-shock conditions
%
% Usage:
%   1. preshock
% [TTrans_e1,TRot_e1,TVib_e1,TElec_e1,Tel_e1,p_e1,M_e1,U_e1,gam_e1,y_se1,R_e1,rho_e1,h_e1,X_Ee1,intel] ...
%   = init_shock_vars(intel,options);
%
%   2. postshock
% [TTrans_e2,TRot_e2,TVib_e2,TElec_e2,Tel_e2,p_e2,M_e2,U_e2,gam_e2,y_se2,R_e2,rho_e2,h_e2,X_Ee2,intel] ...
%   = init_shock_vars(intel,options);
%
% Notes:
%   In this function, the local variables with the suffix *_e1 do NOT
%   necessarily refer to the pre-shock conditions, as is the nomenclature
%   in the higher-level subfunctions. The *_e1 refers to either pre- or
%   post-, as the operations below are identical for both cases.
%
% Summary:
% 1. Initialize temperatures, mass fractions, velocity, and Mach.
% 2. If Re1 is defined, iterate on rho and p until converged to eps.
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% Note that a postscript of 1 indicates pre-shock conditions
% and a 2 indicates post-shock conditions
% Extract temperatures
switch options.numberOfTemp
    case '1T' % All grouped with translational temperature
        TTrans_e1   = intel.T_e0;
        TRot_e1     = intel.T_e0;
        TVib_e1     = intel.T_e0;
        TElec_e1    = intel.T_e0;
        Tel_e1      = intel.T_e0;
    case '2T' % Translational+Rotational & Vibrational+Electronic+Electron
        TTrans_e1   = intel.T_e0;
        TRot_e1     = intel.T_e0;
        TVib_e1     = intel.Tv_e0;
        TElec_e1    = intel.Tv_e0;
        Tel_e1      = intel.Tv_e0;
    otherwise
        error(['options.numberOfTemp = ''',options.numberOfTemp,''' is not currently supported!']);
end

% retrieving mixture constants
mixCnst = intel.mixCnst;

% Define default mass fraction edge composition
[~,y_se1] = getDefault_composition(intel,options.mixture,true,'ys_e0');
X_Ee1 = getElement_XE_from_ys(y_se1,mixCnst.Mm_s,mixCnst.elemStoich_mat,options.bChargeNeutrality,mixCnst.elem_list); % elemental mole fraction at the edge

% Get the mixture enthalpy
[R_s_mat    ,nAtoms_s_mat    ,thetaVib_s_mat    ,thetaElec_s_mat    ,gDegen_s_mat    ,bElectron_mat    ,hForm_s_mat    ,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat] = reshape_inputs4enthalpy(...
 mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,TTrans_e1 ,TRot_e1 ,TVib_e1 ,TElec_e1 ,Tel_e1);
h_e1 = getMixture_h_cp(y_se1,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);

% Get the frozen ratio of specific heats
gam_e1 = getMixture_gam(TTrans_e1,TRot_e1,TVib_e1,TElec_e1,Tel_e1,...
    y_se1,mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,...
    mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,options,mixCnst.AThermFuncs_s);

% Get the mixture gas constant
[Mm_e1,R_e1] = getMixture_Mm_R(y_se1,TTrans_e1,Tel_e1,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron);

% Extract pressure and Mach
if isfield(intel,'M_e0') && ~isfield(intel,'U_e0')
    M_e1 = intel.M_e0;
    U_e1 = M_e1 * sqrt(gam_e1*R_e1*TTrans_e1);
    intel = rmfield(intel,'M_e0');
elseif ~isfield(intel,'M_e0') && isfield(intel,'U_e0')
    U_e1 = intel.U_e0;
    M_e1 = U_e1 / sqrt(gam_e1*R_e1*TTrans_e1);
end

if isfield(intel,'Re1_e0') && ~isfield(intel,'p_e0')

    % HARDCODE WARNING
    % Preparing initial guess for the iteration process to compute the accurate
    % edge properties (depending weakly on p)
    pe_dum = 101325;        % initial guess for the pressure (Pa)
    convPRho = 1;           % initializing convergence criterion on p & rho
    tolPRho = eps;          % tolerance for the iteration on p & rho
    itPRhoMax = 50;         % maximum iteration count for p & rho
    itPRho = 1;             % initializing
    while convPRho>tolPRho && itPRho<=itPRhoMax

        % Guess the density through the ideal gas relation.
        rhoe_dum = getMixture_rho(y_se1,pe_dum,TTrans_e1,Tel_e1,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options);

        % Calculate dynamic viscosity
        mu_e1 = getTransport_mu_kappa(y_se1,TTrans_e1,Tel_e1,rhoe_dum,pe_dum,...
            Mm_e1,options,mixCnst);

        % We may readily compute rho_e1.
        rho_e1 = mu_e1 * intel.Re1_e0 / U_e1;

        % Constant pressure downstream, so p_e0 == p_e for all x
        p_e1 = getMixture_p(y_se1,rho_e1,TTrans_e1,Tel_e1,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options);

        % convergence criterion for the looping on the p & rho
        convPRho = norm((p_e1 - pe_dum)/pe_dum,Inf);
        pe_dum = p_e1;
        dispif(['p-rho it. ',num2str(itPRho),' --> convPRho = ',num2str(convPRho)],options.display);
        itPRho = itPRho+1;

    end % while

    % Remove Re1_e0 so that the code doesn't break with the hierarching while loop
    intel = rmfield(intel,'Re1_e0');

elseif isfield(intel,'p_e0')
    p_e1 = intel.p_e0;
    rho_e1 = getMixture_rho(y_se1,p_e1,TTrans_e1,Tel_e1,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options);
elseif isfield(intel,'rho_e0')
    rho_e1 = intel.rho_e0;
    p_e1 = getMixture_p(y_se1,rho_e1,TTrans_e1,Tel_e1,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options);
end

end % init_shock_vars
