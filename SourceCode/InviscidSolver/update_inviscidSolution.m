function inviscid = update_inviscidSolution(inviscid,options,sol,it)
% update_inviscidSolution updates the inviscid solutions within the
% solving loop.
%
% Examples:
%   intel = update_inviscidSolution(inviscid,options,update);
%
% See also: setDefaults
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_spec = inviscid.mixCnst.N_spec;
idx_s = 1:N_spec;
idx_u = N_spec+1;
idx_h = N_spec+2;
if strcmp(options.numberOfTemp,'2T')
    idx_hv = N_spec+3;
end
inviscid.u(it) = sol(idx_u);
inviscid.h(it) = sol(idx_h);
if strcmp(options.numberOfTemp,'2T')
    inviscid.hv(it) = sol(idx_hv);
end

% enforcing conditions on the mass fractions
if options.bChargeNeutrality               % enforcing charge-neutrality and concentration conditions
    y_s = enforce_chargeNeutralityCond(sol(idx_s).',inviscid.mixCnst.bElectron,inviscid.mixCnst.bPos,...
        inviscid.mixCnst.Mm_s,inviscid.mixCnst.R0,'ys_lim',...
        options.ys_lim,'massFrac');
    inviscid.y_s(it,:) = enforce_concCond(y_s,inviscid.mixCnst.idx_bath);
else                                % enforcing only concentration condition
    inviscid.y_s(it,:) = enforce_concCond(sol(idx_s).',inviscid.mixCnst.idx_bath,'ys_lim',options.ys_lim);
end