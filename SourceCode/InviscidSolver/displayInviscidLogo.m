function [] = displayInviscidLogo(inviscid,options)
% displayInviscidLogo displays the initial message for the inviscid solver
%
% Examples:
%   displayInviscidLogo(inviscid,options)
%
% See also: setDefaults
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0


% preparing strings
ys_string = sprintf('%0.3f ',inviscid.y_s0);
% displaying message
disp('--------------------------------------------------------------------------');
disp('                                                                          ');
disp('                                 ( (                                      ');
disp('                                  ) )                                     ');
disp('                                ........                                  ');
disp('                                |      |]                                 ');
disp('                                \      /                                  ');
disp('                           dekaf.`----''                                  ');
disp('                                                                          ');
disp('--------------------------------------------------------------------------');
disp('-------------    DEKAF EXPLICIT MARCHING INVISCID SOLVER     -------------');
disp('--------------------------------------------------------------------------');
disp('------------------ Authors: E. Beyak, & F. Miro Miro ---------------------');
displayEpilog();
if options.shockJump
disp(['--- After a shock caused by a ',num2str(inviscid.wedgeCone_angle),' [deg] ',options.shockType,', the conditions are:']);
end
if isfield(inviscid,'M0')
disp(['--- Initial edge Mach: ',num2str(inviscid.M0),' [-]']);
end
if isfield(inviscid,'u0')
disp(['--- Initial edge velocity: ',num2str(inviscid.u0),' [m/s]']);
end
if isfield(inviscid,'rho0')
disp(['--- Initial edge density: ',num2str(inviscid.rho0),' [kg/m^3]']);
end
if isfield(inviscid,'T0')
disp(['--- Initial edge temperature: ',num2str(inviscid.T0),' [K]']);
end
if isfield(inviscid,'Tv0')
disp(['--- Initial edge vibrational temperature: ',num2str(inviscid.Tv0),' [K]']);
end
if isfield(inviscid,'h0')
disp(['--- Initial edge enthalpy: ',num2str(inviscid.h0/1e6),' [MJ/kg]']);
end
if isfield(inviscid,'hv0')
disp(['--- Initial edge vibrational enthalpy: ',num2str(inviscid.hv0/1e6),' [MJ/kg]']);
end
if isfield(inviscid,'Re1_0')
disp(['--- Initial edge unit Reynolds number: ',num2str(inviscid.Re10),' [1/m]']);
end
if isfield(inviscid,'p0')
disp(['--- Initial edge pressure: ',num2str(inviscid.p0),' [Pa]']);
end
disp(['--- Mixture: ',options.mixture]);
disp(['--- Species: ',strjoin(inviscid.mixCnst.spec_list,', ')]);
disp(['--- Initial edge mass fractions: ',ys_string,'[-]']);
disp(['--- Thermal model: ',options.modelThermal]);
disp(['--- Number of temperatures: ',options.numberOfTemp]);
disp(['--- Mapping: ',options.xInviscidSpacing]);
disp(['--- x range: 0 --> ',num2str(options.inviscidMap.x_end),' [m]']);
disp(['--- Number of x positions: ',num2str(options.inviscidMap.N_x)]);
disp('--------------------------------------------------------------------------');