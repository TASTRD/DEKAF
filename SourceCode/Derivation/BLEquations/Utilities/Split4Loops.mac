/* Split4Loops(eq,var_loops) splits a linearised equation into blocks           */
/* corresponding to the parts of it that will have to be evaluated in different */
/* loops in the final implementation.                                           */
/*                                                                              */
/* To identify these loops, we must pass a series of variables that will        */
/* require loops.                                                               */
/*                                                                              */
/* For an equation like                                                         */
/*      eq: a_sl*f + a_s*g + b_s*f + a*df_dx + b                                */
/* where a_sl is a double-indexed variable that therefore requires it be summed */
/* over s and l (two loops), a_s & b_s are single-indexed variables that        */
/* require they be summed over s (one loop) and a & b do not require any loops. */
/*                                                                              */
/*  -in---> Split4Loops(eq,[[a_sl],[a_s,b_s]])                                  */
/*  -out--> [a*df_dx + b , a_s*g + b_s*f , a_sl*f]                              */
/*                                                                              */
/* Author: Fernando Miró Miró                                                   */
/* GNU Lesser General Public License 3.0                                        */

disp("------------------------------------------------------------------------")$
disp("----------------------- Loading Split4Loops.mac ------------------------")$
disp("------------------------------------------------------------------------")$
disp("------- This splits an equation into blocks corresponding to the -------")$
disp("------- parts of it that need to be evaluated in different loops. ------")$

Split4Loops(eq,var_loop):=

block([NLoops,eq_split,eqRed,eqLoop],

NLoops: length(var_loop),
eq_split: [],                   /* initializing */
eqRed: eq,                      /* initializing reduced equation (where we will eliminate the terms we've already covered */

for iLoop:1 thru NLoops do(                         /* looping loop blocks */
    eqLoop: eqRed,                                      /* storing what the equation looked like before the variable loop */
    for jj:1 thru length(var_loop[iLoop]) do(           /* looping variables in each loop block */
        eqRed: ratsubst(0,var_loop[iLoop][jj],eqRed)    /* removing from eqRed the terms we've already looped */
    ),
    eq_split: append(eq_split,[expand(eqLoop-eqRed)])   /* appending to the split list */
),
eq_split:append(eq_split,[eqRed]),      /* appending final set of terms */
eq_split:reverse(eq_split),             /* reversing order */

return(eq_split)

)$


/* Function test */
eq: a_sl*a_s*f + a_s*b_s*g + b_s*f + a*df_dx + b;
Split4Loops(eq,[[a_sl],[a_s,b_s]]);

disp("------------------------------------------------------------------------")$
disp("----------------------- Split4Loops.mac loaded -------------------------")$
