grep -rl -r '\*'            MaximaOutStrings.txt | xargs sed -i 's/\*/\.\*/g'
grep -rl -r '\/'            MaximaOutStrings.txt | xargs sed -i 's/\//\.\//g'
grep -rl -r '\^'            MaximaOutStrings.txt | xargs sed -i 's/\^/\.\^/g'
grep -rl -r '\"'            MaximaOutStrings.txt | xargs sed -i 's/\"/ /g'
