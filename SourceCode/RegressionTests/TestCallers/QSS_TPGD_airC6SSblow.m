function [intel,options,marching] = QSS_TPGD_airC6SSblow()
% QSS_TPGD_airC6SSblow sets up intel and options for the regression test
%
% Test details:
%   - Quasi-self-similar solver
%   - freestream Mach 25, 269.4 K and 117.2 Pa, and wall 1900 K
%   - Chemically-frozen shock relations, yet in vibrational equilibrium for
%   a 10-deg wedge
%   - Self-similar blowing of CO2 with fw=-0.1
%   - TPGD flow assumption
%   - airC6Mortensen mixture
%   - CE_122 transport model
%   - FOCE_Ramshaw diffusion model
%   - Wright2005Bellemans2015 collision data
%   - convergence tolerance: 1e-12
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 25; % [-]
intel.T_e = 269.4; % [K]
intel.p_e = 117.2; % [Pa]
frc = 4e-4; % fraction of CO2 in the mixture
intel.ys_e = {frc,1e-10,1e-10,1e-10,0.767-frc-3e-10,0.233}; % in the freestream 0.1% CO2, 76.6% N2 and 23.3% O2, the rest 1e-8%
intel.wedge_angle = 10; % [deg]

options.modelCollisionNeutral = 'Wright2005Bellemans2015';
options.mixture = 'airC6Mortensen';
options.thermal_BC = 'Twall';
options.G_bc = 1900; % [K]
options.wallBlowing_BC = 'SS';
options.F_bc = 1; % [m/s]
options.wallYs_BC = 'ysWall';
options.Ys_bc = {1-5*frc,frc,frc,frc,frc,frc}; % (CO2,N,O,NO,N2,O2) [-]
options.flow = 'TPGD';
options.modelTransport = 'CE_122';
options.tol = 1e-12;
options.shockJump = true;
options.plotRes = false;

marching = {}; % flag for DEKAF

options.vars4test = {'df_deta','g','ys'};
end % QSS_TPGD_airC6SSblow
