function [intel,options,marching] = BL_CPG_Ar_nozzle()
% BL_CPG_Ar_nozzle sets up intel and options for the regression test
%
% Test details:
%   - Boundary-layer marching solver
%   - Edge conditions from a nozzle case (Mach 1-10)
%   - CPG flow assumption
%   - Ar mixture
%   - Isothermal wall 296K
%   - Sutherland transport model with constant Prandlt number
%   - marching with a tanh spacing
%   - convergence tolerance: 1e-11
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

%% Thermophysical properties
Tw = 296; % [K]
Pr  = 0.70;     % Prandtl number [-]
mixture = 'Ar'; % pure argon mixture

%% Nozzle
load('CFDpp_ArNozzle_viscousProfiles.mat','s_e','rc','xAxis','M_e','T_e','p_e'); % loading nozzle info

options.N                       = 100;                  % Number of points
options.L                       = 6;                    % Domain size (in non-dimensional eta length)
options.eta_i                   = 2;                    % Mapping parameter, eta_crit
options.tol                     = 1e-12;                % Convergence tolerance
options.it_max                  = 40;                   % maximum number of iterations

% flow options
options.thermal_BC              = 'Twall';              % Wall bc (isothermal)
options.G_bc                    = Tw;                   % value of the wall temperature
options.flow                    = 'CPG';                % flow assumption
options.mixture                 = mixture;              % mixture
options.modelTransport          = 'Sutherland';         % transport model

options.specify_cp              = false;
options.specify_gam             = false;
options.cstPr                   = true;
options.Pr                      = Pr;

options.BLproperties = false;

% display options
options.plotRes = false;                        % we don't want the residual plot
options.MarchingGlobalConv = false;             % we don't want the global residual plot
options.quiet   = true;                         % do not display the full convergence info at each streamwise station

% flow conditions
intel.M_e               = M_e;                  % Mach number at the first boundary-layer edge pos.     [-]
intel.p_e0              = p_e(1);               % Pressure at the boundary-layer edge                   [Pa]
intel.T_e               = T_e;                  % Temperature at boundary-layer edge                    [K]
intel.x_e               = s_e;                  % streamwise surface coordinate                         [m]
intel.rc                = rc;                   % nozzle radius                                         [m]
intel.xcAxis            = xAxis;                % nozzle axis location                                  [m]

% marching mapping options
N_x = 200;
options.xSpacing            = 'custom_x';       % mapping in the marching direction
options.mapOpts.x_custom    = linspace(1e-8,s_e(end),N_x);

% curvature terms
options.coordsys = 'cylindricalInternal';
options.transverseCurvature = true;

marching = {'marching'}; % flag for DEKAF

%%% Exceptions for the regression test
ic=1;
% options.regressionExceptions(ic).matlabVersion  = 'R2018a';
% options.regressionExceptions(ic).OS             = 'Ubuntu 18.04.3 LTS';
% options.regressionExceptions(ic).processor      = 'Intel(R) Xeon(R) CPU E3-1505M v5 @ 2.80GHz';
% options.regressionExceptions(ic).tolFactor      = 10; ic=ic+1;
%%% end of exceptions

options.vars4test = {'u','v','T','y'};
end % BL_CPG_Ar_nozzle