function [intel,options,marching] = BL_CNE_oxygen2Arrhenius()
% BL_CNE_oxygen2Arrhenius sets up intel and options for the regression test
%
% Test details:
%   - Boundary-layer marching solver
%   - Mach 10, edge 800 K and Re1 5.5e6 1/m, and wall 1000K
%   - Edge profile coming from DEKAF's non-equilibrium 1-D inviscid solver
%   - CNE flow assumption
%   - oxygen2Bortner mixture
%   - Chapman-Enskog transport model, with constant Schmidt number (0.7)
%   - Wright2005 collision data
%   - marching with a tanh spacing
%   - convergence tolerance:
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 10*ones(1,10);     % [-]
intel.T_e = 800*ones(1,10);    % [K]
intel.Re1_e0 = 5.5e6;          % [1/m]
intel.Sc_e = 0.7;
intel.x_e = linspace(1e-6,0.1,10); % [m]

options.modelCollisionNeutral = 'Wright2005';
options.modelDiffusion = 'cstSc';
options.molarDiffusion = false;
options.thermal_BC = 'Twall';
options.G_bc = 5000; % [K]
options.mixture = 'oxygen2Bortner';
options.flow = 'CNE';
options.modelTransport = 'CE_12';
options.tol = 1e-11;
options.inviscidFlowfieldTreatment = 'constant';
options.plotRes = false;
options.MarchingGlobalConv = false;

options.mapOpts.x_start = 1e-6; % [m]
options.mapOpts.x_end = 0.1; % [m]
options.mapOpts.N_x = 50;
options.xSpacing = 'linear';

options.dimoutput = true;

marching = {'marching'}; % flag for DEKAF

options.vars4test = {'u','T','ys'};
end % BL_CNE_oxygen2Arrhenius