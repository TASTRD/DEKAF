function [intel,options,marching] = BL_CNE_air5StuckertCone()
% BL_CNE_air5StuckertCone sets up intel and options for the regression test
%
% Test details:
%   - Boundary-layer marching solver
%   - freestream Mach 25, 252.6 K and 2.008e-4 atm, and wall 1200K
%   - Frozen edge profile condition (no inviscid 1D solver)
%   - Chemically-frozen shock relations (yet in vibrational equilibrium)
%   for a 10-deg cone.
%   - CNE flow assumption
%   - air5Stuckert91 mixture
%   - Brokaw58 transport model
%   - FOCE_StefanMaxwell diffusion model
%   - Stuckert91 collision data
%   - marching with a tanh spacing
%   - convergence tolerance: 1e-10
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 25;                 % [-]
intel.T_e = 252.6;              % [K]
intel.p_e = 2.008e-4 * 101325; % [Pa]
intel.cone_angle = 10;          % [deg]

options.modelCollisionNeutral = 'Stuckert91';
options.mixture = 'air5Stuckert91';
options.flow = 'CNE';
options.coordsys = 'cone';
options.modelTransport = 'CE_122';
options.modelDiffusion = 'FOCE_StefanMaxwell';
options.molarDiffusion = true;
options.thermal_BC = 'Twall';
options.G_bc = 1200; % [K]
options.tol = 1e-10;
options.shockJump = true;
options.inviscidFlowfieldTreatment = 'constant';
options.plotRes = false;
options.MarchingGlobalConv = false;

options.mapOpts.x_start = 1e-4; % [m]
options.mapOpts.x_end = 7.68; % [m]
options.mapOpts.N_x = 50;
options.xSpacing = 'tanh';

options.dimoutput = true;

marching = {'marching'}; % flag for DEKAF

options.vars4test = {'u','v','T','ys'};
end % BL_CNE_air5StuckertCone