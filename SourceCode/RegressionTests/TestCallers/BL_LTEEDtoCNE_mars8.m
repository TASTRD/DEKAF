function [intel,options,marching] = BL_LTEEDtoCNE_mars8()
% BL_LTEEDtoCNE_mars8 sets up intel and options for the regression test
%
% Test details:
%   - Boundary-layer marching solver
%   - freestream Mach 20, 250 K and 0.001 kg/m^3, and wall 1600K
%   - shock relations in thermochemical equilibrium for a 30-deg wedge.
%   - CNE flow assumption, with an LTEED nose
%   - mars8Mortensen
%   - BlottnerEucken transport model
%   - FOCE_Ramshaw diffusion model
%   - marching with a tanh spacing
%   - convergence tolerance: 1e-10
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 20;     % [-]
intel.T_e = 250;    % [K]
intel.rho_e = 0.001;% [kg/m^3]
intel.wedge_angle = 30; % [deg]

options.L = 20;
options.mixture = 'mars8Mortensen';
options.flow = 'CNE';
options.noseFlow = 'LTEED';
options.modelTransport = 'BlottnerEucken';
options.modelDiffusion = 'FOCE_Ramshaw';
options.molarDiffusion = true;
options.thermal_BC = 'Twall';
options.G_bc = 1600; % [K]
options.wallYs_BC = 'nonCatalytic';
options.tol = 1e-10;
options.shockJump = true;
options.inviscidFlowfieldTreatment = 'constant';

options.plotRes = false;
options.MarchingGlobalConv = false;

options.mapOpts.x_start = 0.05; % [m]
options.mapOpts.x_end = 1; % [m]
options.mapOpts.N_x = 20;
options.xSpacing = 'tanh';

options.dimoutput = true;

marching = {'marching'}; % flag for DEKAF

%%% Exceptions for the regression test
% ic=1;
% options.regressionExceptions(ic).matlabVersion  = 'R2018a';
% options.regressionExceptions(ic).OS             = 'Ubuntu 18.04.3 LTS';
% options.regressionExceptions(ic).processor      = 'Intel(R) Xeon(R) CPU E3-1505M v5 @ 2.80GHz';
% options.regressionExceptions(ic).tolFactor      = 10; ic=ic+1;
%%% end of exceptions

options.vars4test = {'u','v','T','ys','yE','y'};
end % BL_LTEEDtoCNE_mars8