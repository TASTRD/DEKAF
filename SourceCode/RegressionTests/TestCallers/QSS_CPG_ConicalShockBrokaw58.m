function [intel,options,marching] = QSS_CPG_ConicalShockBrokaw58()
% QSS_CPG_ConicalShockBrokaw58 sets up intel and options for the regression test
%
% Test details:
%   - Quasi-self-similar solver
%   - freestream Mach 30, 300 K and 100 Pa, and adiabatic wall
%   - Perfect-gas shock relations for a 7-deg cone
%   - TPG flow assumption
%   - air2 mixture
%   - Brokaw58 transport model
%   - convergence tolerance:
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 30; % [-]
intel.T_e = 300; % [K]
intel.p_e = 100; % [Pa]
intel.x = 0.5; % [m]
intel.cone_angle = 7; % [deg]

options.mixture = 'air2';
options.flow = 'CPG';
options.modelTransport = 'Brokaw58';
options.tol = 1e-15;
options.thermal_BC = 'adiab';
options.shockJump = true;
options.coordsys = 'cone';
options.dimoutput = true;
options.dimXQSS = true;
options.plotRes = false;

marching = {}; % flag for DEKAF

%%% Exceptions for the regression test
ic=1;
options.regressionExceptions(ic).matlabVersion  = 'R2018a';
options.regressionExceptions(ic).OS             = 'Ubuntu 18.04.3 LTS';
options.regressionExceptions(ic).processor      = 'Intel(R) Xeon(R) CPU E3-1505M v5 @ 2.80GHz';
options.regressionExceptions(ic).tolFactor      = 100; ic=ic+1;
%%% end of exceptions

options.vars4test = {'u','v','T','dT_dy'};
end % QSS_CPG_ConicalShockBrokaw58
