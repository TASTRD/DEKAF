function [intel,options,marching] = BL_TNE_air2_vibMortensen()
% BL_TNE_air2_vibMortensen sets up intel and options for the regression test
%
% Test details:
%   - Boundary-layer marching solver
%   - Mach 5, edge 1500 K and 20 kPa, and wall 300K
%   - 2-T TPG flow assumption
%   - air2 mixture
%   - Blottner-Eucken transport model
%   - marching with a tanh spacing
%   - convergence tolerance: 1e-11
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

T_e     = 1500;      % K
R_e     = 288.1935;  % J/kg-K (corresponds to the chosen ys_e 22%O2 - 78%N2)
gam     = 1.4;       % vibrationally frozen edge
U_e = 5*sqrt(T_e*R_e*gam);

intel.U_e = U_e*ones(1,10);     % [-]
intel.T_e = T_e*ones(1,10);     % [K]
intel.Tv_e = T_e*ones(1,10);    % [K]
intel.p_e0 = 20e3;              % [Pa]
intel.x_e = linspace(0,1e3,10); % [m]

options.thermal_BC = 'Twall';
options.G_bc = 300; % [K]
options.thermalVib_BC = 'Twall';
options.tauv_bc = 300; % [K]
options.mixture = 'air2';
options.flow = 'TPG';
options.modelTransport = 'BlottnerEucken';
options.referenceVibRelax = 'Mortensen2013';
options.numberOfTemp = '2T';
options.perturbRelaxation = 0.5;
options.it_max = 500;                       % maximum iteration count
options.it_res_sat = options.it_max;        % residual saturation limit
options.tol = 1e-11;
options.inviscidFlowfieldTreatment = 'constant';
options.plotRes = false;
options.MarchingGlobalConv = false;

options.mapOpts.x_start = 1e-6; % [m]
options.mapOpts.x_end = 0.7; % [m]
options.mapOpts.N_x = 50;
options.xSpacing = 'linear';

marching = {'marching'}; % flag for DEKAF

%%% Exceptions for the regression test
ic=1;
options.regressionExceptions(ic).matlabVersion  = 'R2018a';
options.regressionExceptions(ic).OS             = 'Ubuntu 18.04.3 LTS';
options.regressionExceptions(ic).processor      = 'Intel(R) Xeon(R) CPU E3-1505M v5 @ 2.80GHz';
options.regressionExceptions(ic).tolFactor      = 10; ic=ic+1;
%%% end of exceptions

options.vars4test = {'u','T','Tv','ys'};
end % BL_TNE_air2_vibMortensen