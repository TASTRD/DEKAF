function [intel,options,marching] = QSS_LTE_air11CE123()
% QSS_LTE_air11CE123 sets up intel and options for the regression test
%
% Test details:
%   - Quasi-self-similar solver
%   - Mach 30, edge 300 K and Re1 5.5e6 1/m, and wall 5000K
%   - LTE flow assumption
%   - air11mutation mixture
%   - Chapman-Enskog transport model, with Ramshaw ambipolar diffusion model
%   - Wright2005 and Mason67Devoto73 collision data
%   - convergence tolerance:
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 30;     % [-]
intel.T_e = 300;    % [K]
intel.Re1_e = 5.5e6;% [1/m]
intel.x = 2; % [m]

options.modelCollisionNeutral = 'Wright2005';
options.modelCollisionChargeCharge = 'Mason67Devoto73';
options.modelDiffusion = 'FOCE_RamshawAmbipolar';
options.molarDiffusion = true;
options.mixture = 'air11mutation';
options.flow = 'LTE';
options.thermal_BC = 'Twall';
options.G_bc = 5000; % [K]
options.modelTransport = 'CE_123';
options.tol = 1e-11;
options.it_max = 300;
options.dimoutput = true;
options.dimXQSS = true;
options.plotRes = false;
options.silenceWarnings = true;

marching = {}; % flag for DEKAF

%%% Exceptions for the regression test
ic=1;
options.regressionExceptions(ic).matlabVersion  = 'R2018a';
options.regressionExceptions(ic).OS             = 'Ubuntu 18.04.3 LTS';
options.regressionExceptions(ic).processor      = 'Intel(R) Xeon(R) CPU E3-1505M v5 @ 2.80GHz';
options.regressionExceptions(ic).tolFactor      = 200; ic=ic+1;
%%% end of exceptions

options.vars4test = {'u','T','ys'};
end % QSS_LTE_air11CE123
