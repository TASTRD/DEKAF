function [intel,options,marching] = QSS_LTEED_mars8()
% QSS_LTEED_mars8 sets up intel and options for the regression test
%
% Test details:
%   - Quasi-self-similar solver
%   - Mach 10, edge 1000 K and 10kPa, and adiabatic wall
%   - LTEED flow assumption
%   - mars8Mortensen mixture
%   - Chapman-Enskog transport model, with Stefan-Maxwell diffusion model
%   - mixture with 32.2% C 3.14% N 64.6% O (molar - default for mars8)
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 10;      % [-]
intel.T_e = 1000;   % [K]
intel.p_e = 1e4;    % [Pa]
intel.x   = 1; % [m]

% options.passElementalFractions = true;
options.modelDiffusion = 'FOCE_Ramshaw';
options.molarDiffusion = true;
options.mixture = 'mars8Mortensen';
options.flow = 'LTEED';
options.thermal_BC = 'adiab';
options.modelTransport = 'CE_123';
options.tol = 1e-11;
options.it_max = 300;
options.dimoutput = true;
options.dimXQSS = true;
options.plotRes = false;

marching = {}; % flag for DEKAF

%%% Exceptions for the regression test
% ic=1;
% options.regressionExceptions(ic).matlabVersion  = 'R2018a';
% options.regressionExceptions(ic).OS             = 'Ubuntu 18.04.3 LTS';
% options.regressionExceptions(ic).processor      = 'Intel(R) Xeon(R) CPU E3-1505M v5 @ 2.80GHz';
% options.regressionExceptions(ic).tolFactor      = 200; ic=ic+1;
%%% end of exceptions

options.vars4test = {'u','T','ys','yE','y'};
end % QSS_LTEED_mars8
