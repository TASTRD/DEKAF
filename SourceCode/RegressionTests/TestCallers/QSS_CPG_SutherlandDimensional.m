function [intel,options,marching] = QSS_CPG_SutherlandDimensional()
% QSS_CPG_SutherlandDimensional sets up intel and options for the regression test
%
% Test details:
%   - Quasi-self-similar solver
%   - edge Mach 0.8, 300 K and 101325 Pa, and adiabatic wall
%   - CPG flow assumption
%   - air2 mixture
%   - Sutherland's transport model, specifying gamma, cp
%   - computing dimensional quantitites for a fixed Reynolds number
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 0.8; % [-]
intel.T_e = 300; % [K]
intel.p_e = 101325; % [Pa]
intel.gam = 1.4; % [-]
intel.cp = 1004.5; % [J/kg-K]

options.cstPr = false;
options.specify_cp = true;
options.specify_gam = true;
options.mixture = 'air2';
options.thermal_BC = 'adiab';
options.flow = 'CPG';
options.modelTransport = 'Sutherland';
options.tol = 1e-13;
options.plotRes = false;

options.dimoutput = true;
options.fixReQSS = true;
intel.Re = 100:100:3000;

marching = {}; % flag for DEKAF

options.vars4test = {'u','v','y','T'};
end % QSS_CPG_SutherlandDimensional
