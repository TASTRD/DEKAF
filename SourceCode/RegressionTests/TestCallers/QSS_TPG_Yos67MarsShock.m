function [intel,options,marching] = QSS_TPG_Yos67MarsShock()
% QSS_TPG_Yos67MarsShock sets up intel and options for the regression test
%
% Test details:
%   - Quasi-self-similar solver
%   - freestream 10000m/s, 200 K and 0.1 kg/m^3, and wall 1000 K
%   - Chemically-frozen shock relations, yet in vibrational equilibrium for
%   a 10-deg wedge
%   - TPG flow assumption
%   - mars5Park94 mixture
%   - Yos 64 transport model
%   - mars5dplr collision data
%   - convergence tolerance:
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.U_e = 10000; % [m/s]
intel.T_e = 200; % [K]
intel.rho_e = 0.1; % [kg/m^3]
intel.ys_e = {0.05,0.05,0.01,0.3,0.59}; % [-]
intel.wedge_angle = 10; % [deg]

options.modelCollisionNeutral = 'mars5dplr';
options.mixture = 'mars5Park94';
options.thermal_BC = 'Twall';
options.G_bc = 1000; % [K]
options.flow = 'TPG';
options.modelTransport = 'Yos67';
options.tol = 1e-12;
options.shockJump = true;
options.plotRes = false;

marching = {}; % flag for DEKAF

options.vars4test = {'df_deta','g'};
end % QSS_TPG_Yos67MarsShock
