function [intel,options,marching] = BL_CPG_NACA0012Swept()
%BL_CPG_AIRFOIL
%
% Test details:
%   - Boundary-layer marching solver
%   - Edge-profile data from incompressible solver for NACA0012 at 3 deg AoA
%   - Edge conditions for DEKAF created internally by convert_airfoil
%   - CPG flow assumption
%   - Air mixture
%   - Sutherland's transport model, specifying gamma, cp and a constant Pr
%   - marching with a tanh spacing in xi
%   - convergence tolerance: eps
%
% See also: regressionTest
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

% Core parameters for the test
alpha_v_u   = 3;                % angle of attack [deg]
Lambda_w_uv = 45;               % sweep [deg]
chord       = 1;                % chord [m]
M_inf_Q     = 0.5;              % Mach based off of resultant freestream velocity
T_inf       = 298;              % [K]
p_inf       = 101325;           % [Pa]
Pr          = 0.72;             % constant Prandtl number               [-]
R           = 287.058;          % gas constant                          [J/kg-K]
gam         = 1.4;              % constant gamma                        [-]
cp          = gam*R/(gam-1);    % heat capacity at constant pressure    [J/kg-K]
N_x         = 200;              % deresolved for regression test

% Loading case data
filenameCp  = ['naca0012_Cp_@',num2str(alpha_v_u),'.txt'];
start_row   = 4;
formatSpec  = '%10f%9f%f%[^\n\r]';
fileID      = fopen(filenameCp,'r');
dataArray   = textscan(fileID, formatSpec, ...
    'Delimiter', '', 'WhiteSpace', '', 'TextType', 'string', ...
    'HeaderLines' ,start_row-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
fclose(fileID);

% Populating airfoil structure
airfoil.xc_data         = dataArray{1};
airfoil.Cp_UV_data      = dataArray{3};
airfoil.xc_geom_data    = dataArray{1};
airfoil.yc_geom_data    = dataArray{2};
airfoil.chord_U         = chord;            % [m]

options.airfoil.data_orientation    = 'ccw_from_top_TE';
options.airfoil.M_inf_UV_data       = 0;
options.airfoil.s_hyperinterp_fac   = 1;        % let's keep it relatively coarse for the regression
options.airfoil.debug               = 0;        % interactive debugging: off = 0, on = 1
options.airfoil.side                = 'bottom'; % side of airfoil for DEKAF

intel.M_inf_Q     = M_inf_Q;        % -
intel.Lambda_w_uv = Lambda_w_uv;    % [deg]
intel.alpha_v_u   = alpha_v_u;      % [deg]
intel.T_inf       = T_inf;          % [K]
intel.p_inf       = p_inf;          % [Pa]

% Plotting for convert_airfoil
options.airfoil.plot_Cp     = 0;
options.airfoil.plot_R_mrch = 0;
options.airfoil.plot_beta   = 0;
options.airfoil.plot_xi     = 0;

% Inform DEKAF we are running an airfoil case through intel and options.
intel.airfoil                       = airfoil;
options.inviscidFlowfieldTreatment  = 'airfoil';

% Marching mapping options
options.xiSpacing       = 'tanh';   % mapping in the marching direction
options.mapOpts.N_x     = N_x;      % number of streamwise points

% essential DEKAF inputs
options.N                       = 40;                   % number of wall-normal points
options.L                       = 100;                  % wall-normal domain size, in nondimensional eta units
options.eta_i                   = 6;                    % mapping parameter, eta_crit
options.thermal_BC              = 'adiab';              % wall bc
options.flow                    = 'CPG';                % flow assumption
options.mixture                 = 'Air';                % mixture
options.modelTransport          = 'Sutherland';         % transport model
options.mu_ref                  = 1.716e-05;            % Sutherland's reference mu             [kg/m-s]
options.T_ref                   = 273.15;               % Sutherland's reference temperature    [K]
options.Su_mu                   = 110.6;                % Sutherland's constant for mu          [K]
options.cstPr                   = true;                 % constant Prandtl number
options.Pr                      = Pr;
options.specify_cp              = true;                 % constant cp
intel.cp                        = cp;
options.specify_gam             = true;                 % constant gamma
intel.gam                       = gam;
options.tol                     = eps;                  % convergence tolerance
options.it_max                  = 100;                  % maximum number of iterations
options.it_res_sat              = 8;                    % set number of iterations to determine convergence saturation
options.N_thread                = 1;                    % parfor access to # of physical cores
options.plot_transient          = false;                % to plot the inter-iteration profiles
options.plotRes                 = false;                % to plot the residual convergence
options.MarchingGlobalConv      = false;                % global convergence plot
options.dimoutput               = true;                 % to output fields in dimensional form
options.display                 = 0;                    % throttle display of messages
options.quiet                   = true;                 % do not display the full convergence info at each streamwise station
options.BLproperties            = true;                 % calculate boundary-layer properties, like delta99 and shape-factor
options.rotateVelocityVector    = 'streamline';         % output streamline-rotated velocities as u_s and w_s in intel

marching = {'marching'}; % flags for DEKAF

%%% Exceptions for the regression test
% Commented out for now, 2020-03-26.
% ic=1;
% options.regressionExceptions(ic).matlabVersion  = 'R2018a';
% options.regressionExceptions(ic).OS             = 'Ubuntu 18.04.3 LTS';
% options.regressionExceptions(ic).processor      = 'Intel(R) Xeon(R) CPU E3-1505M v5 @ 2.80GHz';
% options.regressionExceptions(ic).tolFactor      = 2e4; ic=ic+1;
%%% end of exceptions

options.vars4test = {'u_s','v','w_s','T'};

end % BL_CPG_NACA0012Swept
