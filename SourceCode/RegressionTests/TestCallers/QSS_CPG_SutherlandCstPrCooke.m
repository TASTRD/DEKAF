function [intel,options,marching] = QSS_CPG_SutherlandCstPrCooke()
% QSS_CPG_SutherlandCstPrCooke sets up intel and options for the regression test
%
% Test details:
%   - Quasi-self-similar solver
%   - edge Mach 5, 400 K and 4000 Pa, and adiabatic wall
%   - CPG flow assumption
%   - air2 mixture
%   - Sutherland's transport model, specifying gamma, cp and a constant Pr
%   - convergence tolerance:
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 5; % [-]
intel.T_e = 400; % [K]
intel.p_e = 4000; % [Pa]
intel.gam = 1.4; % [-]
intel.cp = 1004.5; % [J/kg-K]
intel.Lambda_sweep = 15; % [deg]

options.cstPr = true;
options.Pr = 0.7; % [-]
options.specify_cp = true;
options.specify_gam = true;
options.mixture = 'air2';
options.thermal_BC = 'adiab';
options.flow = 'CPG';
options.modelTransport = 'Sutherland';
options.tol = 1e-13;
options.plotRes = false;

marching = {}; % flag for DEKAF

options.vars4test = {'df_deta','k','g'};
end % QSS_CPG_SutherlandCstPrCooke
