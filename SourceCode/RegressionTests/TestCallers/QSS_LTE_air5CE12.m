function [intel,options,marching] = QSS_LTE_air5CE12()
% QSS_LTE_air5CE12 sets up intel and options for the regression test
%
% Test details:
%   - Quasi-self-similar solver
%   - freestream Mach 25, 200 K and Re1 5.5e6 1/m, and adiabatic wall
%   - Equilibrium shock jump relations for a 15-deg wedge
%   - LTE flow assumption
%   - air5mutation mixture
%   - Chapman-Enskog transport model, with Ramshaw mass diffusion model
%   - Stuckert91 collision data
%   - convergence tolerance:
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 25;     % [-]
intel.T_e = 200;    % [K]
intel.Re1_e = 5.5e6;% [1/m]
intel.x = 1.5; % [m]
intel.wedge_angle = 15; % [deg]

options.modelCollisionNeutral = 'Stuckert91';
options.modelDiffusion = 'FOCE_Ramshaw';
options.molarDiffusion = false;
options.mixture = 'air5mutation';
options.flow = 'LTE';
options.thermal_BC = 'adiab';
options.modelTransport = 'CE_12';
options.tol = 1e-12;
options.shockJump = 'true';
options.dimoutput = true;
options.dimXQSS = true;
options.plotRes = false;

marching = {}; % flag for DEKAF

%%% Exceptions for the regression test
ic=1;
options.regressionExceptions(ic).matlabVersion  = 'all';
options.regressionExceptions(ic).OS             = 'all';
options.regressionExceptions(ic).processor      = 'all';
options.regressionExceptions(ic).tolFactor      = 30; ic=ic+1;
%%% end of exceptions

options.vars4test = {'u','T','ys'};
end % QSS_LTE_air5CE12
