function [intel,options,marching] = QSS_TPG_BlottnerEucken()
% QSS_TPG_BlottnerEucken sets up intel and options for the regression test
%
% Test details:
%   - Quasi-self-similar solver
%   - edge Mach 8, 300 K and 10000 Pa, and wall 300K
%   - Perfect-gas shock relations for a 7-deg cone
%   - TPG flow assumption
%   - air5mutation mixture
%   - BlottnerEucken transport model
%   - convergence tolerance:
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 8; % [-]
intel.T_e = 300; % [K]
intel.p_e = 10000; % [Pa]

options.mixture = 'air5mutation';
options.flow = 'TPG';
options.modelTransport = 'BlottnerEucken';
options.thermal_BC = 'Twall';
options.G_bc = 300; % [K]
options.tol = 1e-13;
options.plotRes = false;

marching = {}; % flag for DEKAF

options.vars4test = {'df_deta','g'};
end % QSS_TPG_BlottnerEucken
