function [intel,options,marching] = QSS_realGas_customPoly()
% QSS_realGas_customPoly sets up intel and options for the regression test
%
% Test details:
%   - Quasi-self-similar solver
%   - freestream Mach 3, 1000 K and 10 MPa, and isothermal wall (300K)
%   - TPG flow assumption
%   - N2 mixture
%   - customPoly7 thermal model for high pressures
%   - vanDerWaals equation of state
%   - CE_122 transport model
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e   = 3;    % [-]
intel.T_e   = 1000; % [K]
intel.p_e   = 10e6; % [Pa]
intel.x     = 0.1;  % [m]

options.mixture = 'N2';
options.flow = 'TPG';
options.modelTransport = 'CE_122';
options.tol = 1e-13;
options.thermal_BC = 'Twall';
options.G_bc = 300; % [K]
options.EoS = 'vanDerWaals';
options.modelThermal = 'customPoly7';
options.customPoly.ATherm_s{1} = [  3.49984375e00   3.53100528e00   2.65957610e00  ;
                                    5.42919935e-06 -1.23660988e-04  2.04347856e-03 ;
                                    0.0            -5.02999433e-07 -9.74374625e-07 ;
                                    0.0             2.43530612e-09  2.25657401e-10 ;
                                    0.0            -1.40881235e-12 -2.04980840e-14 ;
                                   -1.04373668e03  -1.04697628e03  -8.27207854e02  ;
                                    3.10284436e00   2.96747038e00   7.44514386e00 ];
options.customPoly.Tlims_s{1} = [50.0 250.0 975.0 3000.0];
options.dimoutput = true;
options.dimXQSS = true;
options.plotRes = false;
options.tol = 1e-12;
options.bLinearInterpThermalPoly = false; % so that it doesn't interpolate the coefficients

marching = {}; % flag for DEKAF

options.vars4test = {'u','v','T','rho','y'};
end % QSS_realGas_customPoly
