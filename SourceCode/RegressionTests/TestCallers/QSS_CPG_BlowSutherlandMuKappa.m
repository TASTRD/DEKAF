function [intel,options,marching] = QSS_CPG_BlowSutherlandMuKappa()
% QSS_CPG_BlowSutherlandMuKappa sets up intel and options for the regression test
%
% Test details:
%   - Quasi-self-similar solver
%   - edge Mach 4.5, 61.61 K and 0.01899 kg/m^3, and isothermal wall (311K)
%   - CPG flow assumption
%   - air2 mixture
%   - Sutherland's transport model, specifying gamma, cp but with a varying
%   Pr (Sutherland on both mu and kappa)
%   - heated wall (at 311K)
%   - convergence tolerance: 1e-13
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 4.5; % [-]
intel.T_e = 61.61; % [K]
intel.rho_e = 1.899e-2; % [Pa]
intel.gam = 1.4; % [-]
intel.cp = 1004.5; % [J/kg-K]

options.cstPr = false;
options.specify_cp = true;
options.specify_gam = true;
options.mixture = 'air2';
options.thermal_BC = 'Twall';
options.G_bc = 311;
options.wallBlowing_BC = 'SS';
options.F_bc = -0.4;
options.flow = 'CPG';
options.modelTransport = 'Sutherland';
options.tol = 1e-13;
options.plotRes = false;

marching = {}; % flag for DEKAF

options.vars4test = {'df_deta','df_deta2','g','dg_deta'};
end % QSS_CPG_BlowSutherlandMuKappa
