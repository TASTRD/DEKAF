function [intel,options,marching] = BL_CNE_airC6BlowingWedge()
% BL_CNE_airC6BlowingWedge sets up intel and options for the regression test
%
% Test details:
%   - Boundary-layer marching solver
%   - freestream Mach 25, 250 K and 0.001 kg/m^3, and wall 4000K
%   - Blowing of carbon mixture with constant Vw = 1 m/s
%   - Frozen edge profile condition (no inviscid 1D solver)
%   - Chemically-frozen shock relations (yet in vibrational equilibrium)
%   for a 10-deg wedge.
%   - CNE flow assumption
%   - airC6Mortensen mixture (CO2,N,O,NO,N2,O2)
%   - CE122 transport model
%   - FOCE_Ramshaw diffusion model
%   - Wright2005Bellemans2015 collision data
%   - marching with a tanh spacing
%   - convergence tolerance: 1e-10
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 25;     % [-]
intel.T_e = 250;    % [K]
intel.rho_e = 0.001;% [kg/m^3]
intel.wedge_angle = 10; % [deg]
frc = 4e-4; % fraction of CO2 in the mixture
trc = 1e-10; % fraction of traces
intel.ys_e = {frc,trc,trc,trc,0.767-frc-3*trc,0.233}; % (CO2,N,O,NO,N2,O2)

options.modelCollisionNeutral = 'Wright2005Bellemans2015';
options.mixture = 'airC6Mortensen';
% options.mixture = 'airC11Mortensen';
options.flow = 'CNE';
options.modelTransport = 'CE_122';
options.modelDiffusion = 'FOCE_Ramshaw';
options.molarDiffusion = true;
options.thermal_BC = 'Twall';
options.G_bc = 4000; % [K]
% options.wallBlowing_BC = 'SS';
% options.F_bc = -0.1; % [-]
options.wallBlowing_BC = 'Vwall';
options.F_bc = 1; % [m/s]
options.wallYs_BC = 'ysWall';
options.Ys_bc = {1-5*trc,trc,trc,trc,trc,trc}; % (CO2,N,O,NO,N2,O2) [-]
% options.Ys_bc = {0.01,0.8-5*trc,0.05,0.1,0.01,0.03,trc,trc,trc,trc,trc}; % {C3,CO2,C2,CO,CN,C,N,O,NO,N2,O2} [-]
options.tol = 1e-10;
options.shockJump = true;
options.inviscidFlowfieldTreatment = 'constant';
options.plotRes = false;
options.MarchingGlobalConv = false;

options.mapOpts.x_start = 1e-4; % [m]
options.mapOpts.x_end = 1; % [m]
options.mapOpts.N_x = 50;
options.xSpacing = 'tanh';

options.dimoutput = true;

marching = {'marching'}; % flag for DEKAF

%%% Exceptions for the regression test
ic=1;
options.regressionExceptions(ic).matlabVersion  = 'R2018a';
options.regressionExceptions(ic).OS             = 'Ubuntu 18.04.3 LTS';
options.regressionExceptions(ic).processor      = 'Intel(R) Xeon(R) CPU E3-1505M v5 @ 2.80GHz';
options.regressionExceptions(ic).tolFactor      = 10; ic=ic+1;
%%% end of exceptions

options.vars4test = {'u','v','T','ys'};
end % BL_CNE_airC6BlowingCone