function [intel,options,marching] = QSS_TPGD_AirHe_SuthWilke()
% QSS_TPGD_AirHe_SuthWilke sets up intel and options for the regression test
%
% Test details:
%   - Quasi-self-similar blowing solver
%   - freestream Mach 6, 70 K and 1000 Pa, and wall 300 K
%   - Chemically-frozen shock relations, yet in vibrational equilibrium for
%   a 7-deg cone
%   - Self-similar blowing of He with fw=-0.2
%   - TPGD flow assumption
%   - Air-He mixture
%   - SutherlandWilke transport model
%   - cstSc diffusion model
%   - convergence tolerance: 1e-12
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 6; % [-]
intel.T_e = 70; % [K]
intel.p_e = 1000; % [Pa]
intel.Sc_e = 0.71; % [-]
frc = 1e-5; % fraction of air in the injected gas
intel.cone_angle = 7; % [deg]

options.coordsys = 'cone';
options.mixture = 'Air-He';
options.thermal_BC = 'Twall';
options.G_bc = 300; % [K]
options.wallBlowing_BC = 'SS';
options.F_bc = -0.1; % [-]
options.wallYs_BC = 'ysWall';
options.Ys_bc = {frc,1-frc}; % (Air,He) [-]
options.flow = 'TPGD';
options.modelTransport = 'SutherlandWilke';
options.modelDiffusion = 'cstSc';
options.tol = 1e-12;
options.shockJump = true;
options.plotRes = false;
options.dimoutput = true;
options.dimXQSS = true;
intel.x = 0.5; % [m]

marching = {}; % flag for DEKAF

options.vars4test = {'u','v','T','ys','y'};
end % QSS_TPGD_AirHe_SuthWilke
