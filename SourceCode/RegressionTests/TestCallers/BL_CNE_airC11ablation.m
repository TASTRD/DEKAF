function [intel,options,marching] = BL_CNE_airC11ablation()
% BL_CNE_airC11ablation sets up intel and options for the regression test
%
% Test details:
%   - Boundary-layer marching solver
%   - freestream Mach 25, 269.4 K and 117.2 Pa, and wall 1900K
%   - isothermal ablating wall
%   - Frozen edge profile condition (no inviscid 1D solver)
%   - Chemically-frozen shock relations (yet in vibrational equilibrium)
%   for a 10-deg wedge.
%   - CNE flow assumption
%   - airC11Mortensen mixture (CO2,N,O,NO,N2,O2)
%   - BlottnerEucken transport model
%   - FOCE_Ramshaw diffusion model
%   - marching with a tanh spacing
%   - convergence tolerance: 1e-10
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 25;     % [-]
intel.T_e = 269.4;    % [K]
intel.p_e = 117.2;% [Pa]
intel.wedge_angle = 10; % [deg]
trc = 1e-10; % fraction of traces
intel.ys_e = {trc,trc,trc,trc,trc,trc,trc,trc,trc,0.767-8*trc,0.233};  % {C3,CO2,C2,CO,CN,C,N,O,NO,N2,O2}

options.mixture = 'airC11Mortensen';
options.flow = 'CNE';
options.modelTransport = 'BlottnerEucken';
options.modelDiffusion = 'FOCE_Ramshaw';
options.molarDiffusion = true;
options.thermal_BC = 'Twall';
options.G_bc = 1900; % [K]
options.wallBlowing_BC = 'ablation';
options.wallYs_BC = 'ablation';
options.tol = 1e-10;
options.shockJump = true;
options.inviscidFlowfieldTreatment = 'constant';
options.plotRes = false;
options.MarchingGlobalConv = false;

options.mapOpts.x_start = 1e-8; % [m]
options.mapOpts.x_end = 0.25; % [m]
options.mapOpts.N_x = 50;
options.xSpacing = 'tanh';

options.dimoutput = true;

marching = {'marching'}; % flag for DEKAF

options.vars4test = {'u','v','T','ys'};
end % BL_CNE_airC11ablation