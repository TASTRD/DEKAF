function [intel,options,marching] = BL_TCNE_air5Inviscid()
% BL_TCNE_air5Inviscid sets up intel and options for the regression test
%
% Test details:
%   - Boundary-layer marching solver
%   - freestream Mach 35 300 K and 0.001 kg/m^3, and wall 4000K
%   - Edge profile coming from DEKAF's non-equilibrium 1-D inviscid solver
%   - Chemically-frozen shock relations (yet in vibrational equilibrium)
%   for a 20-deg wedge.
%   - CNE flow assumption with two temperatures
%   - air5mutation mixture
%   - Chapman-Enskog transport model
%   - Wright2005 collision data
%   - marching with a log spacing
%   - convergence tolerance:
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e0 = 35;     % [-]
intel.T_e0 = 300;    % [K]
intel.Tv_e0 = intel.T_e0;    % [K]
intel.rho_e0 = 0.001;% [kg/m^3]
intel.wedge_angle = 20; % [deg]

options.modelCollisionNeutral = 'Wright2005';
options.mixture = 'air5mutation';
options.flow = 'CNE';
options.numberOfTemp = '2T';
options.modelTransport = 'CE_12';
options.molarDiffusion = false;
options.thermal_BC = 'Twall';
options.G_bc = 4000; % [K]
options.thermalVib_BC = 'Twall';
options.tauv_bc = 4000; % [K]
options.tol = 1e-10;
options.shockJump = true;
options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';
options.inviscidMethod = 'ode45';
options.xInviscidSpacing = 'tanh';
options.inviscidMap.N_x = 2000;
options.inviscid_ox = 6;
options.plotRes = false;
options.MarchingGlobalConv = false;
options.plotInviscid = false;
options.perturbRelaxation = 0.5;

options.mapOpts.x_start = 1e-7; % [m]
options.mapOpts.x_end = 0.01; % [m]
options.mapOpts.N_x = 50;
options.xSpacing = 'tanh';

options.dimoutput = true;

marching = {'marching'}; % flag for DEKAF

%%% Exceptions for the regression test
ic=1;
options.regressionExceptions(ic).matlabVersion  = 'R2018a';
options.regressionExceptions(ic).OS             = 'Ubuntu 18.04.3 LTS';
options.regressionExceptions(ic).processor      = 'Intel(R) Xeon(R) CPU E3-1505M v5 @ 2.80GHz';
options.regressionExceptions(ic).tolFactor      = 3000; ic=ic+1;
%%% end of exceptions

options.vars4test = {'u','T','ys'};
end % BL_CNE_air5Inviscid