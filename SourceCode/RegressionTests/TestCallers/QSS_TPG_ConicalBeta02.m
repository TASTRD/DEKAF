function [intel,options,marching] = QSS_TPG_ConicalBeta02()
% QSS_TPG_ConicalBeta02 sets up intel and options for the regression test
%
% Test details:
%   - Quasi-self-similar solver
%   - freestream Mach 5, 300 K and 101325 Pa, and isothermal wall (400K)
%   - pressure-gradient parameter beta=0.2
%   - TPG flow assumption
%   - air2 mixture
%   - CE_122 transport model
%   - convergence tolerance:
%
% See also: regressionTest
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

intel.M_e = 5; % [-]
intel.T_e = 300; % [K]
intel.p_e = 100; % [Pa]
intel.x = 0.5; % [m]
intel.cone_angle = 10; % [deg]

options.mixture = 'air2';
options.flow = 'TPG';
options.modelTransport = 'CE_122';
options.tol = 1e-13;
options.thermal_BC = 'Twall';
options.G_bc = 400; % [K]
intel.beta = 0.05;
options.shockJump = false;
options.coordsys = 'cone';
options.dimoutput = true;
options.dimXQSS = true;
options.plotRes = false;

marching = {}; % flag for DEKAF

options.vars4test = {'u','v','T'};
end % QSS_TPG_ConicalBeta02
