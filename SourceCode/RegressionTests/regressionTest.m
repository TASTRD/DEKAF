function [] = regressionTest(varargin)
% regressionTest is a function running a series of test scripts conforming
% DEKAF's regression test.
%
% The goal is to make sure that new developments do not lead to unwanted
% errors in other blocks of the code.
%
% Usage:
%   (1)
%       regressionTest()
%
%   (2)
%       regressionTest(path)
%       |---> allows to fix the path where DEKAF is installed. For instance
%       regressionTest(getenv('DEKAF_DIRECTORY')) will analyze the default
%       DEKAF location.
%       By default, the regression test will look for the location of the
%       DEKAF (main) function.
%
%   (3)
%       regressionTest(...,'chooseTests',cases)
%       |---> allows to pass a cell of strings with the identifiers of the
%       different cases to be run. Supported cases listed below.
%       ...or alternatively, pass in a vector of indices corresponding to the
%       cases of interest.
%       e.g., if `cases` = [1 2], then regression testing will run ...
%       'QSS_CPG_SutherlandCstPrCooke' and 'QSS_CPG_BlowSutherlandMuKappa'
%
%   (4)
%       regressionTest(...,'saveMode')
%       |---> creates reference files with the data to be compared against
%       in future regression tests.
%       IMPORTANT! This option should only be used when there is absolute
%       certainty that the reference files are being overwritten for a good
%       reason (like previous errors in the computations). Wrongly using
%       these option could lead to untraceable future errors and even to
%       false failures.
%       Also, the execution in save mode must be done in
%       SourceCode/RegressionTests/, so that the reference data files can
%       be saved in the appropriate folder.
%
%   (5)
%       regressionTest(...,'fastMode')
%       |---> runs the regression test in fast mode (only 5 streamwise
%       stations)
%
%   (6)
%       regressionTest(...,'noGitInfo')
%       |---> does not look for the git information.
%
% See also: setDefaults,
%       1.    QSS_CPG_SutherlandCstPrCooke,
%       2.    QSS_CPG_SutherlandDimensional,
%       3.    QSS_CPG_BlowSutherlandMuKappa,
%       4.    QSS_TPG_BlottnerEucken,
%       5.    QSS_CPG_ConicalShockBrokaw58,
%       6.    QSS_TPG_ConicalBeta02,
%       7.    QSS_TPG_Yos67MarsShock,
%       8.    QSS_TPGD_AirHe_SuthWilke,
%       9.    QSS_TPGD_airC6SSblow,
%      10.    QSS_LTE_air11CE123,
%      11.    QSS_LTE_air5CE12,
%      12.    QSS_LTEED_mars8,
%      13.    QSS_realGas_customPoly,
%      14.    BL_CNE_oxygen2Arrhenius,
%      15.    BL_CNE_air5Inviscid,
%      16.    BL_LTEEDtoCNE_mars8,
%      17.    BL_TNE_air2_vibMortensen,
%      18.    BL_TCNE_air5Inviscid,
%      19.    BL_TCNE_air11Inviscid,
%      20.    BL_CNE_air5StuckertCone,
%      21.    BL_CNE_airC6BlowingWedge,
%      22.    BL_CNE_airC11ablation,
%      23.    BL_CPG_NACA0012Swept,
%      24.    BL_CPG_Ar_nozzle
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

errmsg_ID = 'regressionTest';
dateNow = datetime; % obtaining date
datestr = [num2str(dateNow.Year),'-',num2str(dateNow.Month,'%02d'),'-',num2str(dateNow.Day,'%02d') ...
    ,'_',num2str(dateNow.Hour,'%02d'),num2str(dateNow.Minute,'%02d')];
logPath = ['regressionTest_',datestr,'.log'];              % building name of the log file
diary(logPath);                                                             % opening log file

% list of tests
cases = {...
    'QSS_CPG_SutherlandCstPrCooke' ...
    ,'QSS_CPG_SutherlandDimensional' ...
    ,'QSS_CPG_BlowSutherlandMuKappa' ...
    ,'QSS_TPG_BlottnerEucken' ...
    ,'QSS_CPG_ConicalShockBrokaw58' ...
    ,'QSS_TPG_ConicalBeta02' ...
    ,'QSS_TPG_Yos67MarsShock' ...
    ,'QSS_TPGD_AirHe_SuthWilke' ...
    ,'QSS_TPGD_airC6SSblow' ...
    ,'QSS_LTE_air11CE123' ...
    ,'QSS_LTE_air5CE12' ...
    ,'QSS_LTEED_mars8' ...
    ,'QSS_realGas_customPoly' ...
    ,'BL_CNE_oxygen2Arrhenius' ...
    ,'BL_CNE_air5Inviscid' ...
    ,'BL_LTEEDtoCNE_mars8' ...
    ,'BL_TNE_air2_vibMortensen' ...
    ,'BL_TCNE_air5Inviscid' ...
    ,'BL_TCNE_air11Inviscid' ...
    ,'BL_CNE_air5StuckertCone' ...
    ,'BL_CNE_airC6BlowingWedge' ...
    ,'BL_CNE_airC11ablation' ...
    ,'BL_CPG_NACA0012Swept' ...
    ,'BL_CPG_Ar_nozzle' ...
    };
try                                                                         % starting full regression test here
    disp('--------------------------------------------------------------------------');
    disp('--------------          DEKAF REGRESSION TEST V1.0          --------------');

    % Checking inputs
    [varargin,saveMode] = find_flagAndRemove('saveMode',varargin);          % looking for the save flag
    [varargin,fastMode] = find_flagAndRemove('fastMode',varargin);          % looking for the fast flag
    [varargin,noGitInfo] = find_flagAndRemove('noGitInfo',varargin);        % looking for the noGitInfo flag
    [cases2check,varargin] = parse_optional_input(varargin,'chooseTests',cases); % looking for the test-choosing flag
    if isempty(varargin)                                                    % if no other input was given (path)
        inputGitInfo = {};                                                      % we don't provide anything to getGitInfo
    else                                                                    % otherwise
        inputGitInfo = varargin;                                                % the user passed the path
    end
    if fastMode && saveMode
        error('The fastMode and saveMode are not compatible - reference files must always be generated with the slow mode (no fastMode flag)');
    end
    % Allow the user to input a cell of indices for the tests rather than the full string names
    if isnumeric(cases2check)
        % `cases2check` is initially passed in as numeric indices that are related to `cases`
        cases2check = cases(cases2check);
        % ...cases2check is now a cell of strings for the regression cases of interest.
    end
    missingCases = ~ismember(cases2check,cases);                            % looking for any cases that are not in the list of tests
    if nnz(~missingCases)~=length(cases2check)                              % if there are any, break
        error(['some of the chosen cases are missing: ',sprintf('\n - %s',cases2check{missingCases}),...
            sprintf('\nPlease choose from amongst one of the available test cases:'),sprintf('\n - %s',cases{:})]);
    end
    datasubdir = 'ReferenceData/';
    if saveMode                                                             % if saveMode was chosen, make a check with the user to make sure he knows what he's doing
        disp('--------------------------------------------------------------------------');
        disp('----------                   ¡¡¡ WARNING !!!                   -----------');
        disp('--------------------------------------------------------------------------');
        disp('--   You have chosen the regression test''s save-mode. This will ---------');
        disp('-- overwrite all previously saved reference files. You should only do ----');
        disp('-- this if you are overwriting a previous result which is provenly -------');
        disp('-- wrong or inaccurate. --------------------------------------------------');
        disp('--------------------------------------------------------------------------');
        reply = input('-- Do you wish to continue (true/false)? ');
        if ~reply
            saveMode = false;                                                       % if he regrets, don't save
        end
        if ~exist(datasubdir,'dir')
            error(['It appears ',datasubdir,' doesn''t exist. You are likely in the wrong directory.\n',...
                'Consider cd $DEKAF_DIRECTORY/SourceCode/RegressionTests'],errmsg_ID);
        end
    end

    % obtaining git and system info
    if noGitInfo
        gitInfo = getSystemInfo();
        gitInfo.branch = '';
        gitInfo.hash = '';
        gitInfo.url = '';
    else
        gitInfo = getGitInfo(inputGitInfo{:});
        if isempty(fieldnames(gitInfo))                                         % if we don't find the git data, break
            errMsg = sprintf(['getGitInfo could not find any active git repository. This normally happens because the root DEKAF directory has ',...
                'not been added to matlab''s working path.\nInstead of doing addpath(genpath([getenv(''DEKAF_DIRECTORY''),''/SourceCode''])) simply ',...
                'do addpath(genpath(getenv(''DEKAF_DIRECTORY''))).']);
            error(errMsg);
        end
    end

    % running dekaf in loop
    N_cases = length(cases2check);
    bSuccess = cell(N_cases,1);                                             % allocating and initializing
    testVars = cell(N_cases,1);
    testTime = zeros(N_cases,1);
    failures = struct;
    iF = 0;
    for ii=1:N_cases                                                        % looping cases
        keepVars('cases2check','ii','gitInfo','saveMode','fastMode','bSuccess','testVars','N_cases','failures','iF','datasubdir','testTime'); % keeping variables of interest, deleting others
        clc;                                                                    % clearing screen (to make it easier to track errors)
        disp('--------------------------------------------------------------------------');
        disp(['-- Running case ',num2str(ii),'/',num2str(N_cases)]);
        disp(['-----> ',cases2check{ii}]);

        filepath = ['regressionData_',cases2check{ii},'.mat'];                  % path to the saved file

        % Running DEKAF
        tic;
        [intel,options,marching] = feval(cases2check{ii});                      % obtaining case intel and options
        if fastMode
            options.iXi_stop = 5;
        end
        [intel,options] = DEKAF(intel,options,marching{:});                     % running DEKAF
        testTime(ii) = toc;

        % Obtaining regression-test tolerance
        regTol = getRegressionTol(gitInfo,options);

        % Set the x indices up to separation (inclusive)
        if options.marchYesNo;  ixs = options.mapOpts.separation_i_xi;
        else;                   ixs = size(intel.f,2);
        end
        
        % Setting indices to be used for comparison
        if fastMode;    idxComp = 1:min(5,size(intel.f,2));
        else;           idxComp = 1:size(intel.f,2);
        end

        % Saving
        if saveMode                                                             % saving data-set (if chosen)
            for jj=1:length(options.vars4test)
                eval([options.vars4test{jj},' = intel.',options.vars4test{jj},';']); % E.g.: df_deta = intel.df_deta;
            end
            save([datasubdir,filepath],'gitInfo',options.vars4test{:});
        end

        % Checking against saved data
        ref = load(filepath);
        bCells = cellfun(@(cll)iscell(intel.(cll)),options.vars4test);          % checking if the variables in vars4test are cells in intel
        lgthCells = cellfun(@(cll)length(intel.(cll)),options.vars4test(bCells)); % length of the cell variables
        N_flds = sum(lgthCells) + nnz(~bCells);                                 % total number of fields (length of cells plus the number of non-cells)
        bSuccess{ii} = false(N_flds,1);                                         % allocating success boolean and test variable name cell
        testVars{ii} = cell(N_flds,1);
        ic = 0;                                                                 % initializing index counter
        for jj=1:length(options.vars4test)                                      % looping variables to be tested
            if iscell(intel.(options.vars4test{jj}))                                % if we have cell variables (like ys{s})
                Ncll = length(intel.(options.vars4test{jj}));                           % number of positions in the cell
                for s = 1:Ncll                                                          % looping cell locations
                    ic = ic+1;                                                              % increasing index counter
                    allCells = cat(3,intel.(options.vars4test{jj}){:});                     % concatenating all fields in the cell to get the maximum for the normalization
                    refQt = max(max(max(allCells(:,idxComp,:))));                           % obtaining reference quantity
                    err = norm((intel.(options.vars4test{jj}){s}(:,idxComp) - ref.(options.vars4test{jj}){s}(:,idxComp))/refQt,inf); % computing error
                    if err>regTol                                                           % if we have a higher error than the tolerance
                        bSuccess{ii}(ic) = false;                                               % failure
                        iF = iF+1;                                                              % increase failure counter
                        failures(iF).ref = ref.(options.vars4test{jj}){s}(:,idxComp);           % saving reference field to plot
                        failures(iF).new = intel.(options.vars4test{jj}){s}(:,idxComp);         % saving new field to plot
                        failures(iF).var = [options.vars4test{jj},'(',num2str(s),')'];          % saving variable name
                        failures(iF).case = cases2check{ii};                                    % saving case name
                        failures(iF).err = err;                                                 % saving value of the error
                        failures(iF).tol = regTol;                                              % saving value of the regression tolerance
                        failures(iF).ixs = ixs;                                                 % saving x index of separation/final station
                    else                                                                    % otherwise
                        bSuccess{ii}(ic) = true;                                                % success
                    end
                    testVars{ii}{ic} = [options.vars4test{jj},'(',num2str(s),')'];          % storing variable name
                end                                                                     % closing cell loop
            else                                                                    % if we have non-cell variables (like u or T)
                ic = ic+1;                                                              % increasing index counter
                refQt = max(max(abs(ref.(options.vars4test{jj})(:,idxComp))));          % computing reference quantity
                err = norm((intel.(options.vars4test{jj})(:,idxComp) - ref.(options.vars4test{jj})(:,idxComp))/refQt,inf); % computing error
                if err>regTol                                                           % if we have a higher error than the tolerance
                    bSuccess{ii}(ic) = false;                                               % failure
                    iF = iF+1;                                                              % increase failure counter
                    failures(iF).ref = ref.(options.vars4test{jj})(:,idxComp);              % saving reference field to plot
                    failures(iF).new = intel.(options.vars4test{jj})(:,idxComp);            % saving new field to plot
                    failures(iF).var = options.vars4test{jj};                               % saving variable name
                    failures(iF).case = cases2check{ii};                                    % saving case name
                    failures(iF).err = err;                                                 % saving value of the error
                    failures(iF).tol = regTol;                                              % saving value of the regression tolerance
                    failures(iF).ixs = ixs;                                                 % saving x index of separation/final station
                else                                                                    % otherwise
                    bSuccess{ii}(ic) = true;                                                % success
                end
                testVars{ii}{ic} = options.vars4test{jj};                               % storing variable name
            end
        end                                                                     % closing variable loop
    end                                                                     % closing case loop

    regression_unitTests();                                         % running unit tests
    writeSummary(cases2check,testVars,bSuccess,failures,gitInfo,testTime);

catch ME                                                            % if we find an error, we want to write it to file and close the diary

    regression_unitTests();                                         % running unit tests
    fprintf(1, 'ERROR:\n%s\n', ME.message);
    str4error = sprintf('ERROR:\n%s\n', ME.message);                    % string for error display
    for ii=1:length(ME.stack)                                           % looping embedded functions causing the error
        fprintf(1, 'File: %s\nLine: %d\n',ME.stack(ii).name,ME.stack(ii).line); % printing position of the error
        str4error = [str4error,sprintf('File: %s\n',hlink(ME.stack(ii).name,ME.stack(ii).name,ME.stack(ii).line))]; %#ok<AGROW>
    end
    writeSummary(cases2check,testVars,bSuccess,failures,gitInfo,testTime);% we write the summary of the cases that did finish
    diary off;                                                          % closing diary
    error(str4error);                                                   % displaying error message
end

end % regressionTest
