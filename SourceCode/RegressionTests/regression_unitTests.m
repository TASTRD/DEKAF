function [] = regression_unitTests()
% regression_unitTests runs all unit tests and prints the summary.
%
% Usage:
%   (1)
%       regression_unitTests()
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% sorting paths
funcPath = which_unique('regression_unitTests');                        % path to this same function
parts = regexp(funcPath,'SourceCode','split');                          % parts before and after the SourceCode folder
unitPath = [parts{1},'SourceCode/UnitTests'];                           % path to where the unit tests are
sep = getOSPathSeparator();
foldersInUnit = regexp(genpath(unitPath),sep,'split');                  % all folders contained in the UnitPath folder
foldersInUnit(cellfun(@isempty,foldersInUnit)) = [];                    % removing empty members of the cell

% looping folders and running tests
bPassAll = []; % allocating
testsAll = {};
for iF = length(foldersInUnit):-1:1                                     % looping folders
    structWhat = what(foldersInUnit{iF});                                   % looking at what's in the folders
    tests2run_iF = structWhat.m;                                            % extracting matlab files
    for iT = length(tests2run_iF):-1:1                                      % looping tests
        bPass_iFiT = feval(tests2run_iF{iT}(1:end-2));                          % evaluating without the extension.m
        bPassAll = [bPassAll,bPass_iFiT];                                       % appending boolean results
        tests_iFiT = cellfun(@(cll)[foldersInUnit{iF},'/',tests2run_iF{iT},' subtest_',num2str(cll)],num2cell((1:length(bPass_iFiT)).'),'UniformOutput',false);
        testsAll = [testsAll;tests_iFiT];                                       % appending test names
    end
end

% Printing summary
strPassed = {'Failed    X','Passed   -/'};
Ncol = 10;
NcolTest = max(cellfun(@length,testsAll))+3;                % maximum length of any tests
gitInfo = getGitInfo();
disp('--------------------------------------------------------------------------');
disp('-----------        DEKAF UNIT-REGRESSION TEST SUMMARY         ------------');
disp('--------------------------------------------------------------------------');
disp(['-- ',pad_str('Date: ',Ncol),datestr(datetime('now'))]);
disp(['-- ',pad_str('Branch: ',Ncol),gitInfo.branch]);
disp(['-- ',pad_str('SHA: ',Ncol),gitInfo.hash]);
disp(['-- ',pad_str('url: ',Ncol),gitInfo.url]);
disp('--------------------------------------------------------------------------');
for iT=1:length(testsAll) % looping tests
    disp(['-- ',pad_str([testsAll{iT},' '],NcolTest,'pad_strWith','-'),'--> ',strPassed{bPassAll(iT)+1}]);
end
disp('--------------------------------------------------------------------------');
if any(~bPassAll);      disp('-- Some unit tests failed');
else;                   disp('-- All unit tests passed');
end
disp('--------------------------------------------------------------------------');
end % regression_unitTests