function regTol = getRegressionTol(sysInfo,options)
% getRegressionTol returns the tolerance to be used for the regression
% test.
%
% It is computed based on the tolerance provided to DEKAF through
% options.tol, and whatever relaxation may be necessary on such tolerance
% as a consequence of the system in which the test is being run.
%
% Usage:
%   (1)
%       regTol = getRegressionTol(sysInfo,options)
%
% Inputs and outputs:
%   sysInfo
%       structure containing the information of the system in which the
%       test is being run, as provided by getSystemInfo
%   options
%       classic DEKAF options structure, containing at least:
%          .tol
%               tolerance determining the convergence of the solution
%          .regressionExceptions
%               structured cell array containing as many entries as
%               exceptions are necessary for the considered test. It must
%               have the following fields:
%                   .matlabVersion, .OS, .processor
%                       Identical to how they would be provided by
%                       getSystemInfo. Alternatively 'all' can be used to
%                       denote all possible values of one of the fields.
%                   .tolFactor
%                       factor to be applied on the tolerance
%
% If no match is found, then the tolFactor is taken as 1.
%
% Author: Fernando Miro Miro
% Date: October 2019
%
% GNU Lesser General Public License 3.0

tolFactor = 1;                                                              % default value

if isfield(options,'regressionExceptions')                                  % if there are exceptions to check
    matlabVersions  = {options.regressionExceptions.matlabVersion};             % extracting values of the different fields of the exceptions
    OSs             = {options.regressionExceptions.OS};
    processors      = {options.regressionExceptions.processor};
    bidx_matVer = ismember(matlabVersions,  {sysInfo.matlabVersion, 'all'});    % searching exceptions matching the current system info or the wild card 'all'
    bidx_OS     = ismember(OSs,             {sysInfo.OS,            'all'});
    bidx_proc   = ismember(processors,      {sysInfo.processor,     'all'});
    idxExcep = find(bidx_matVer & bidx_OS & bidx_proc);                         % exception matching all three system info data
    if ~isempty(idxExcep)                                                       % if there is such an exception
        tolFactor = options.regressionExceptions(idxExcep).tolFactor;               % we take its tolFactor
    end
end

regTol = tolFactor * options.tol;                                           % computing regression tolerance

end % getRegressionTol