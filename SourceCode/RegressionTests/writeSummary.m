function [] = writeSummary(cases2check,testVars,bSuccess,failures,gitInfo,testTime)
% writeSummary displays the summary of passed and failed regression tests
% to screen, and produces plots showing the non-passed tests' agreement.
%
% Usage:
%   (1)
%       writeSummary(cases2check,testVars,bSuccess,failures,gitInfo,testTime)
%
% Inputs and outputs:
%   cases2check
%       cell of strings with the names of all the cases to be checked
%   testVars
%       cell of cells containing the names of the different variables to
%       consider for each regression test in cases2check
%   bSuccess
%       cell of boolean vectors, storing whether each of the test variables
%       in testVars passed the test or not
%   failures
%       structured cell array with the reference and new profiles of all
%       the failed cases, as well as the error found in them
%   gitInfo
%       structure with information on the git branch, revision, etc
%   testTime
%       vector with the time taken to conduct the various tests
%
% Author: Fernando Miro Miro
% Date: December 2018
% GNU Lesser General Public License 3.0

% writing summary
N_cases = length(cases2check);
strPassed = {'Failed    X','Passed   -/'};
Ncol = 16;
disp('--------------------------------------------------------------------------');
disp('-----------       DEKAF GLOBAL REGRESSION TEST SUMMARY        ------------');
disp('--------------------------------------------------------------------------');
disp(['-- ',pad_str('Date: ',Ncol),datestr(datetime('now'))]);
disp(['-- ',pad_str('Branch: ',Ncol),gitInfo.branch]);
disp(['-- ',pad_str('SHA: ',Ncol),gitInfo.hash]);
disp(['-- ',pad_str('url: ',Ncol),gitInfo.url]);
disp(['-- ',pad_str('DEKAF version: ',Ncol),gitInfo.DEKAFversion]);
disp(['-- ',pad_str('Matlab version: ',Ncol),gitInfo.matlabVersion]);
disp(['-- ',pad_str('OS: ',Ncol),gitInfo.OS]);
disp(['-- ',pad_str('Processor: ',Ncol),gitInfo.processor]);
for ii=1:N_cases % looping cases
    disp('--------------------------------------------------------------------------');
    disp(['-- Case ',cases2check{ii},' (',num2str(testTime(ii)),'s)']);
    disp( '--    Variable       |     Passed/Failed');
    disp( '---------------------|---------------------------');
    for jj=1:length(testVars{ii})
        disp(['--    ',pad_str(testVars{ii}{jj},15),'|     ',strPassed{int16(bSuccess{ii}(jj))+1}]);
    end
end
disp('--------------------------------------------------------------------------');

% plotting failed cases
if sum(cellfun(@sum,bSuccess))<sum(cellfun(@length,bSuccess))       % if there are any failures, we plot them
    disp('-- Failed cases:');
    NFail = length(failures);                                           % number of failures
    Nmax = 6;                                                           % we don't want more than 6 plots per figure
    Nprf = 10;                                                          % profiles to be plotted
    if NFail>Nmax                                                       % if there are too many failures
        N_i = 2; N_j = 3;                                                   % we set plot size to the maximum - 2 x 3
    else                                                                % otherwise
        [N_i,N_j] = optimalPltSize(NFail);                                 % we get the optimal plot size
    end
    ic = 0;                                                             % initializing index counter
    for iF = 1:NFail                                                    % looping failures
        disp(['-- ',failures(iF).case,' - ',failures(iF).var,', error ',num2str(failures(iF).err),' > tol = ',num2str(failures(iF).tol)]);
        if ic>=Nmax                                                         % if we reached the maximum subplots
            suptitle('Failed cases');
            legend('reference','new');                                          % we add legend to the previous figure
            ic = 1;                                                             % we reinitialize the counter
            figure;                                                             % and start a new figure
        elseif ic==0                                                        % if we our counter is zero
            ic = 1;                                                             % we initialize the counter
            figure;                                                             % we start a new figure
        else                                                                % otherwise
            ic = ic+1;                                                          % we increase the counter
        end
        subplot(N_i,N_j,ic);                                                % choosing subplot
        sz1 = size(failures(iF).ref,1);                                     % obtaining field size in first rank: y extent
        sz2 = failures(iF).ixs;                                             % field size in second rank: x index of separation/final station
        if sz2>2                                                            % if there is more than one column
            idx_plt = round(linspace(1,sz2,Nprf+1));                            % we choose only Nprf columns
            idx_plt(1) = [];
        else                                                                % otherwise
            idx_plt = 1;                                                        % we choose the only column
        end
        for iprf = 1:length(idx_plt)                                        % looping profiles to be plotted (columns)
            plot(real(failures(iF).ref(:,idx_plt(iprf))),sz1:-1:1,'k-'); hold on      % plotting reference case
            plot(real(failures(iF).new(:,idx_plt(iprf))),sz1:-1:1,'r--'); hold on     % plotting new failed case
        end
        title(strrep([failures(iF).case,' - ',failures(iF).var,', error ',num2str(failures(iF).err)],'_','\_')); % adding title
        xlabel(strrep(failures(iF).var,'_','\_')); ylabel('index');         % adding axis labels
    end                                                                 % closing failure loop
    legend('reference','new');                                          % adding legend
    suptitle('Failed cases');
    disp('--------------------------------------------------------------------------');
    disp(leftStr('If the test failures are known to be associated to matlab''s rounding differences associated to matlab version, OS, or processor, an exception can be added to the regression test in SourceCode/RegressionTests/TestCallers/. An example can be found in test QSS_LTE_air5CE12. The matlabVersion, OS, and processor strings must coincide with those provided by getSystemInfo().',dispNstr()));
    disp('--------------------------------------------------------------------------');
end

% printing out test time
disp(['-- Total test time: ',num2str(sum(testTime)),' s']);                    % displaying test time in seconds
diary off;                                                          % closing diary

end % writeSummary
