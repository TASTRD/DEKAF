function [intel,options] = changeMixture(intel,options,mixture,spec_groups,varargin)
% changeMixture changes the gas mixture in a DEKAF structure set.
%
% It rearranges species-specific fields, and obtains the gas properties for
% the new mixture. The concentration condition is also enforced on the
% mass-fractions such that the sum of them is always equal to 1. Similarly,
% if the target mixture is ionized and options.ambipolar is true, the
% charge neutrality condition is enforced.
%
% Usage:
%   (1)
%       [intel,options] = changeMixture(intel,options,mixture,spec_groups)
%
%   (2)
%       [intel,options] = changeMixture(...,'plot')
%       |--> prepares a plot with the initial and final composition
%       profiles
%
%   (3)
%       [intel,options] = changeMixture(...,'complementaryMixture',intel2,options2)
%       |--> allows to pass a second complementary set of structures, so
%       that the missing mass fractions are obtained from it.
%       See example 3.
%
% inputs and outputs:
%   intel
%       classic DEKAF data structure. The fields that are modified are:
%           .ys, .ys_e, .ys_inf
%               species mass fractions in the boundary layer, in the
%               boundary-layer edge, and in the pre-shock region.
%           .dys_dy, .dys_dy2, .dys_dx, .dys_dx2, .dys_dxdy
%               spatial gradients of the mass fractions in the boundary
%               layer
%           .rhos
%               species partial densities in the boundary layer
%           .drhos_dy, .drhos_dy2, .drhos_dx, .drhos_dx2, .drhos_dxdy
%               spatial gradients of the species partial densities in the
%               boundary layer
%           .mixCnst
%               structure with all the mixture constants needed to compute
%               the various properties
%   options
%       classic DEKAF options structure. The fields that are modified are:
%           .mixture
%               string identifying the gas mixture
%   mixture
%       string identifying the new gas mixture
%   spec_groups
%       cell of cells containing the strings of the various species that
%       should be grouped into each of the new species.
%
% Examples:
%   (1)
%   If the original mixture was 'air11_Park91' with spec_list:
%       {'N','O','N2','O2','NO','N+','O+','N2+','O2+','NO+','e-'}
%   and the target mixture is 'air5_Park90' with spec_list:
%       {'N','O','NO','N2','O2'}
%   then spec_groups should include a cell of cells similar to:
%       {{'N','N+','e-'} , {'O','O+'} , {'NO','NO+'} , {'N2','N2+'} , {'O2','O2+'}}
%   With it, all ion species will be paired with their corresponding
%   neutral, with electrons being grouped together with the atomic nitrogen
%   species. In other words, with such definitions one would have species
%   mass fractions rearranged as:
%           ys{N}  = ys{N}  + ys{N+} + ys{e-}
%           ys{O}  = ys{O}  + ys{O+}
%           ys{NO} = ys{NO} + ys{NO+}
%           ys{N2} = ys{N2} + ys{N2+}
%           ys{O2} = ys{O2} + ys{O2+}
%
%   (2)
%   If the original mixture was 'air5_Park90' with spec_list:
%       {'N','O','NO','N2','O2'}
%   and the target mixture was 'air11_Park91' with spec_list:
%       {'N','O','N2','O2','NO','N+','O+','N2+','O2+','NO+','e-'}
%   then spec_groups should include a cell of cells similar to:
%       {{'N'},{'O'},{'N2'},{'O2'},{'NO'},{},{},{},{},{},{}}
%   With it, the mass fractions of all ion species (corresponding to an
%   empty cell in spec_groups) are defined equal to the trace value in
%   options.ys_lim. In other words, with such definitions one would have
%   species mass fractions rearranged as:
%           ys{N}   = ys{N}
%           ys{O}   = ys{O}
%           ys{N2}  = ys{N2}
%           ys{O2}  = ys{O2}
%           ys{NO}  = ys{NO}
%           ys{N+}  = options.ys_lim
%           ys{O+}  = options.ys_lim
%           ys{N2+} = options.ys_lim
%           ys{O2+} = options.ys_lim
%           ys{NO+} = options.ys_lim
%           ys{e-}  = options.ys_lim
%
%   (3)
%   If the original mixture was 'air5_Park90' with spec_list:
%       {'N','O','NO','N2','O2'}
%   and the target mixture was 'air11_Park91' with spec_list:
%       {'N','O','N2','O2','NO','N+','O+','N2+','O2+','NO+','e-'}
%   then, if spec_groups is someething like this:
%       {{'N'},{'O'},{'N2'},{'O2'},{'NO'},{'comp_N+_@_N'},{'comp_O+_@_O'},...
%           {'comp_N2+_@_N2'},{'comp_O2+_@_O2'},{'comp_NO+_@_NO'},{'comp_e-_@_N'}}
%   With it, the mass fractions of all ion species, which have the "comp_"
%   identifier, are obtained from the complementary fields (see usage 3).
%   The species followed by "_@_" is the species from the original mixture
%   (ari5_Park90) that will loose mass to account for the imported species.
%   In other words:
%           ys{N}   = ys{N}  - ysComp{N+} - ysComp{e-}
%           ys{O}   = ys{O}  - ysComp{O+}
%           ys{N2}  = ys{N2} - ysComp{N2+}
%           ys{O2}  = ys{O2} - ysComp{O2+}
%           ys{NO}  = ys{NO} - ysComp{NO+}
%           ys{N+}  = ysComp{N+}
%           ys{O+}  = ysComp{O+}
%           ys{N2+} = ysComp{N2+}
%           ys{O2+} = ysComp{O2+}
%           ys{NO+} = ysComp{NO+}
%           ys{e-}  = ysComp{e-}
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% spec_groups = {{'N'},{'O'},{'N2'},{'O2'},{'NO'},{'comp_N+_@_N'},{'comp_O+_@_O'},{'comp_N2+_@_N2'},{'comp_O2+_@_O2'},{'comp_NO+_@_NO'},{'comp_e-_@_N'}};

[varargin,bPlot] = find_flagAndRemove('plot',varargin);
[~,~,idx] = find_flagAndRemove('complementaryMixture',varargin);
if ~isempty(idx)
    intelComp   = varargin{idx+1};
    optionsComp = varargin{idx+2};
    spec_listComp = intelComp.mixCnst.spec_list;
end

options.EoS = protectedvalue(options,'EoS','idealGas');

%%%% PlOTTING original mixture
if bPlot
    [~,idx] = min(abs(1-intel.x(1,:)));
    StyleS    = repmat({'-','-.','--',':'},[1,10]);
    figure
    subplot(1,2,1);
    for s=1:length(intel.ys)
        plot(intel.ys{s}(:,idx),intel.eta,StyleS{s},'Color',two_colors(s,length(intel.ys),'Rbw2'),'displayname',intel.mixCnst.spec_list{s}); hold on;
    end
    xlabel('y_s [-]'); ylabel('\eta [-]'); legend(); ylim([0,6]); title('Original mixture');
end

%%% Analyzing inputs and allocating
mixtureOriginal   = options.mixture;                                        % extracting original mixture and species list
spec_listOriginal = intel.mixCnst.spec_list;
[mixCnst,options] = getAll_constants(mixture,options);                      % obtaining new mixCnst
N_spec = length(mixCnst.spec_list);                                         % number of species in the target mixture
if N_spec ~= length(spec_groups)                                            % grouping condition
    error(['the length of the cell of cells spec_groups (',num2str(length(spec_groups)),') is not equal to the number of species (',num2str(N_spec),') in the chosen mixture ''',num2str(mixture),'''']);
end
bYsInf = isfield(intel,'ys_inf');
[Ny,Nx] = size(intel.ys{1});                                                % dimensions
y_s     = zeros(Ny*Nx,  N_spec);                                            % intializing in matrix form
y_se    = zeros(Nx,     N_spec);
y_sinf  = zeros(1,      N_spec);

%%% Substituting species as requested
for s=1:N_spec                                                              % looping new species
    if isempty(spec_groups{s})                                                  % if there aren't any
        y_s(:,s)    = options.ys_lim;                                               % we make the mass-fraction equal to the limiting value
        y_se(:,s)   = options.ys_lim;
        if bYsInf
            y_sinf(:,s) = options.ys_lim;
        end
    elseif any(~cellfun(@isempty,regexp(spec_groups{s},'^comp\_','once')))      % if we must use the complementary structure
        for l=1:length(spec_groups{s})                                              % looping species in the group
            if ~isempty(regexp(spec_groups{s}{l},'^comp\_','once'))                     % we must take it from the complementary structure
                parts = regexp(spec_groups{s}{l},'(comp\_)|(\_\@\_)','split');              % splitting string
                specComp  = parts{2};                                                       % species from the complementary structure to take
                specMinus = parts{3};                                                       % species to subtract the mass fraction from
                % adding mass fraction of ll (comp. struct.) to s (original struct.)
                ll = find(strcmp(spec_listComp,specComp));                                  % who should we add to
                if isempty(ll);     error(['the chosen species ''',specComp,''' in group ',num2str(s),' is not included in the list of species in the complementary mixture ''',optionsComp.mixture,''' --> {',strjoin(spec_listComp,','),'}']);
                else;               [y_s,y_se,y_sinf] = addSpecies(s,ll,y_s,y_se,y_sinf,intelComp,bYsInf); % we add it
                end
                % subtracting mass fraction of ll (comp. struct.) from ss (original struct.)
                ss = find(strcmp(spec_listComp,specMinus));                                 % who should we subtract from
                if isempty(ss);     error(['the chosen species ''',specMinus,''' in group ',num2str(s),' is not included in the list of species in the original mixture ''',mixtureOriginal,''' --> {',strjoin(spec_listOriginal,','),'}']);
                else;               [y_s,y_se,y_sinf] = addSpecies(ss,ll,y_s,y_se,y_sinf,intelComp,bYsInf,'substract'); % we substract it
                end
            end
        end
    else                                                                        % if there are
        for l=1:length(spec_groups{s})                                              % looping species in the group
            ll = find(strcmp(spec_listOriginal,spec_groups{s}{l}));                     % finding the group member in the original species list
            if isempty(ll);     error(['the chosen species ''',spec_groups{s}{l},''' in group ',num2str(s),' is not included in the list of species in the original mixture ''',mixtureOriginal,''' --> {',strjoin(spec_listOriginal,','),'}']);
            else;               [y_s,y_se,y_sinf] = addSpecies(s,ll,y_s,y_se,y_sinf,intel,bYsInf);  % we add it
            end
        end
    end
end

%%% Ensuring that the concentration and ambipolar conditions are satisfied if needed
y_s     = enforce_concCond(y_s,     mixCnst.idx_bath,'ys_lim',options.ys_lim); % enforcing concentration condition
y_se    = enforce_concCond(y_se,    mixCnst.idx_bath,'ys_lim',options.ys_lim);
if bYsInf
    y_sinf  = enforce_concCond(y_sinf,  mixCnst.idx_bath,'ys_lim',options.ys_lim);
end
if options.bChargeNeutrality                                                % for mixtures where charge neutrality must be imposed
    y_s = enforce_chargeNeutralityCond(y_s,mixCnst.bElectron,mixCnst.bPos,mixCnst.Mm_s,mixCnst.R0,'massFrac','ys_lim',options.ys_lim); % enforcing charge-neutrality condition
end

%%% Computing species densities and others
p = reshape(repmat(intel.p_e,[Ny,1]),[Nx*Ny,1]);                            % (x,1) --> (yx,1)
switch options.numberOfTemp                                                 % recomputing mixture density
    case '1T';      rho = getMixture_rho(y_s,p,intel.T(:),intel.T(:),mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron);
    case '2T';      rho = getMixture_rho(y_s,p,intel.T(:),intel.Tv(:),mixCnst.R0,Mm_s,mixCnst.bElectron);
    otherwise;      error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end
rho_s   = y_s .* repmat(rho,[1,N_spec]);                                    % species partial densities
y_s     = reshape(y_s,[Ny,Nx,N_spec]);                                      % (yx,s) --> (y,x,s)
rho_s   = reshape(rho_s,[Ny,Nx,N_spec]);                                    % (yx,s) --> (y,x,s)
for s=N_spec:-1:1                                                           % looping species
                                                                                % saving 0th order derivatives
    ys_e{s}   = y_se(:,s);
    if bYsInf
        ys_inf{s} = y_sinf(:,s);
    end
    ys{s}     = y_s(:,:,s);
    rhos{s}   = rho_s(:,:,s);
    for ix=1:Nx                                                                 % looping streamwise positions
                                                                                    % Compute y first and second derivatives
        drhos_dy{s}   = intel.D1_y(:,:,ix)*rho_s(:,ix,s);
        dys_dy{s}     = intel.D1_y(:,:,ix)*y_s(:,ix,s);
        drhos_dy2{s}  = intel.D2_y(:,:,ix)*rho_s(:,ix,s);
        dys_dy2{s}    = intel.D2_y(:,:,ix)*y_s(:,ix,s);
    end
                                                                                    % First order in x
        drhos_dx{s}   =   D1_x_func(rho_s(:,:,s) ,intel.D1,intel.D1_xi,intel.deta_dx,intel.dxi_dx);
        dys_dx{s}     =   D1_x_func(y_s(:,:,s)   ,intel.D1,intel.D1_xi,intel.deta_dx,intel.dxi_dx);
                                                                                    % Mixed partials
        drhos_dxdy{s} =   D2_xy_func(rho_s(:,:,s),intel.D1,intel.D1_xi,intel.D2,intel.deta_dx,intel.dxi_dx,intel.deta_dy);
        dys_dxdy{s}   =   D2_xy_func(y_s(:,:,s)  ,intel.D1,intel.D1_xi,intel.D2,intel.deta_dx,intel.dxi_dx,intel.deta_dy);
                                                                                    % Second order in x
        drhos_dx2{s}  =   D2_x_func(rho_s(:,:,s) ,intel.D1,intel.D1_xi,intel.D2,intel.D2_xi,intel.deta_dx,intel.dxi_dx);
        dys_dx2{s}    =   D2_x_func(y_s(:,:,s)   ,intel.D1,intel.D1_xi,intel.D2,intel.D2_xi,intel.deta_dx,intel.dxi_dx);
end

%%% Preparing output
intel.ys            = ys;
intel.ys_e          = ys_e;
intel.ys_inf        = ys_inf;
intel.dys_dy        = dys_dy;
intel.dys_dy2       = dys_dy2;
intel.dys_dx        = dys_dx;
intel.dys_dxdy      = dys_dxdy;
intel.dys_dx2       = dys_dx2;
intel.rhos          = rhos;
intel.drhos_dy      = drhos_dy;
intel.drhos_dy2     = drhos_dy2;
intel.drhos_dx      = drhos_dx;
intel.drhos_dxdy    = drhos_dxdy;
intel.drhos_dx2     = drhos_dx2;
intel.mixCnst       = mixCnst;                                              % including mixCnst and mixture in outputs
options.mixture     = mixture;

%%%% PLOTTING final mixture
if bPlot
    [~,idx] = min(abs(1-intel.x(1,:)));
    subplot(1,2,2);
    for s=1:length(intel.ys)
        plot(intel.ys{s}(:,idx),intel.eta,StyleS{s},'Color',two_colors(s,length(intel.ys),'Rbw2'),'displayname',intel.mixCnst.spec_list{s}); hold on;
    end
    xlabel('y_s [-]'); ylabel('\eta [-]'); legend(); ylim([0,6]); title('Final mixture');
end

end % changeMixture


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [y_s,y_se,y_sinf] = addSpecies(s,ll,y_s,y_se,y_sinf,intel,bYsInf,varargin)
% addSpecies is an auxiliary function to changeMixture.
%
% Usage:
%   (1)
%       [y_s,y_se,y_sinf] = addSpecies(s,ll,y_s,y_se,y_sinf,intel,bYsInf)
%
%   (2)
%       [...] = addSpecies(...,'substract')
%       |--> substracts the chosen species mass fraction, rather than
%       adding it.
%
% Inputs and outputs:
%   s
%       identifier of the species from y_s, y_se, y_sinf, onto which ll
%       must be added
%   ll
%       identifier of the species from intel that must be added onto s
%   y_s
%       mass fractions in the boundary layer
%   y_se
%       mass fractions in the boundary-layer edge
%   y_sinf
%       mass fractions in the freestream
%   intel
%       structure with the fields where species ll must be taken
%   bYsInf
%       boolean determining if the freestream mass fraction is provided or
%       not.
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,bMinus] = find_flagAndRemove('substract',varargin);

if bMinus                                       % we subtract
    y_s(:,s)    = y_s(:,s) - intel.ys{ll}(:);
    y_se(:,s)   = y_se(:,s) - intel.ys_e{ll}(:);
    if bYsInf
        y_sinf(:,s) = y_sinf(:,s) - intel.ys_inf{ll};
    end
else                                            % we add
    y_s(:,s)    = y_s(:,s) + intel.ys{ll}(:);
    y_se(:,s)   = y_se(:,s) + intel.ys_e{ll}(:);
    if bYsInf
        y_sinf(:,s) = y_sinf(:,s) + intel.ys_inf{ll};
    end
end

end % addSpecies