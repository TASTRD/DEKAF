% build_LTETables is a script to generate, test and save equilibrium
% composition tables for a chosen mixture.
%
% The temperature range is standardly chosen to be between 5 and 30,000K,
% distributing the points with a chebyshev distribution with the
% intermediate point T_i in 5000K.
%
% The pressure range is chosen between 1 and 10,000,000Pa, with a
% logarithmic spacing.
%
% Author: Fernando Miro Miro

%%%% IMPORTANT: don't worry if you get NaN's during the evaluation. The
% numerical method sometimes doesn't converge for lower temperatures. The
% final table to be stored will have it correctly, setting all NaNs to the
% last non-NaN (aN?) value.

%%%%% INPUTS

% Paths etc
clear;
opts = struct;

% Defining parameters

% mars5
mixture = 'mars5Mortensen';
% mutdatapath = '';
X_e = [0.3333,0.6667]; % First entry, Oxygen elemental mole fraction; Second entry, nitrogen elemental mole fraction.
elem_list = {'C','O'};
extraName = '';

% mars8
% mixture = 'mars8Mortensen';
% % mutdatapath = '';
% X_e = [0.32,0.03,0.65]; % First entry, Oxygen elemental mole fraction; Second entry, nitrogen elemental mole fraction.
% elem_list = {'C','N','O'};
% extraName = '';

% air11
% mixture = 'air11mutation';
% % mutdatapath = 'mutationTables_T200-10-20000_P400-100-20000_air11_withExtraStuff.dat';
% % First entry, ratio of elements' mole fractions; Second entry, ratio of ions to electrons.
% X_e = [0.21,0.79]; % First entry, Oxygen elemental mole fraction; Second entry, nitrogen elemental mole fraction.
% XEq = [X_e(1)/X_e(2),1];
% elem_list = {'O','N'};
% extraName = '';

% air5
% mutdatapath = 'mutationTables_T200-10-20000_P400-100-20000_air5_withExtraStuff.dat';
% mixture = 'air5mutation';
% % mixture = 'air5Park85';
% X_e = [0.21,0.79]; % First entry, Oxygen elemental mole fraction; Second entry, nitrogen elemental mole fraction.
% XEq = X_e(1)/X_e(2); % First entry, ratio of elements' mole fractions; Second entry, ratio of ions to electrons.
% elem_list = {'O','N'};
% % opts.modelEquilibrium = 'Park85';
% % extraName = '_Park85';
% extraName = '';

% oxygen2
% mutdatapath = '';
% mixture = 'oxygen2Bortner';
% X_e = 1; % Oxygen elemental mole fraction
% XEq = []; % First entry, ratio of elements' mole fractions; Second entry, ratio of ions to electrons.
% elem_list = {'O'};
% extraName = '';

% T_min   = 5;        % [K]
T_min   = 10;        % [K]
T_max   = 30e3;     % [K]
% T_max   = 20e3;     % [K]
T_i     = 10e3;     % [K]
N_T     = 1000;     % [-]

p_min   = 1;        % [Pa]
p_max   = 10e6;     % [Pa]
N_p     = 57;       % [-]
% p_min   = 20000;      % [Pa]
% p_max   = 20000;    % [Pa]
% N_p     = 1;       % [-]

%%%%% END OF INPUTS

mixtureMut = regexp(mixture,'^([a-z]|[A-Z])+[0-9]+','match');
mixtureMut(strcmp(mixtureMut,'oxygen2')) = {'O2'};

% Obtaining required species and chemistry properties
opts.mixture = mixture; % terribly redundant, but it's the way we have it
opts.flow = 'LTE';
[mixCnst,opts] = getAll_constants(mixture,opts);
mixCnst.XEq = X_e;
spec_list = mixCnst.spec_list;

N_X = length(mixCnst.XEq);      % number of atomic ratios to be imposed
N_spec = mixCnst.N_spec;        % number of species

% Building temperature and pressure vectors
[T_vec,~,~,~] = build_mapping_vars(T_max,T_i,N_T,true,T_min);           % building vector and derivation matrices
% T_vec = linspace(T_max,T_min,N_T).';
p_vec       = logspace(log10(p_max),log10(p_min),N_p);                  % we want logaritmic spacing for pressure

% Calculating equilibrium constants
pAtm_vec = mixCnst.Keq_struc.pAtm * ones(size(T_vec));                  % repeating the reference pressure to have the same size as T_vec
T_r = getReaction_T(ones(size(mixCnst.reac_list_eq)),T_vec);
lnKeq_r = getReaction_lnKeq(T_r,T_vec,T_vec,T_vec,T_vec,T_vec,mixCnst.delta_nuEq,mixCnst.Keq_struc,opts);     % computing equilibrium constants
R0 = mixCnst.R0;                                                        % universal gas constant
lnKpeq = getEquilibrium_lnKpeq(lnKeq_r,R0,T_vec,sum(mixCnst.delta_nuEq,1)'); % obtaining Kp constants (in Pa^delta_nu)

% Double-loop method
opts.display = true;
opts.pTEqVecs = false; % we are providing a mesh of p-T points
X_mat = getEquilibrium_X(p_vec,T_vec,lnKpeq,mixCnst.XEq,mixCnst,'doubleLoop',opts);
X_mat = permute(reshape(permute(X_mat,[2,1]),[N_spec,N_T,N_p]),[2,3,1]);

%% Fixing nan'd entries to the last non-nan value
for ip = 1:N_p % looping pressure
    bnan = isnan(X_mat(:,ip,1));     % Grab the logical array according to the nans
    if sum(bnan)
        lastNotNan = find(bnan,1,'first') - 1;  % last position that wasn't a NaN
        N_nans = sum(bnan);                     % number of NaN'd positions
        X_mat(lastNotNan+1:end,ip,:) = repmat(X_mat(lastNotNan,ip,:),[N_nans,1,1]);
    end
end

% Savename
savename = getLTE_tablePath(mixture,X_e,elem_list,N_T,T_min,T_max,N_p,p_min,p_max,'extraName',extraName);

%% Computing equilibrium static enthalpies
X_2D = reshape(X_mat,[N_T*N_p,N_spec]);         % (T,p,s) --> (Tp,s)
y_s = getEquilibrium_ys(X_2D,mixCnst.Mm_s);     % computing mass fraction from mole fractions
ys_3D = reshape(y_s,[N_T,N_p,N_spec]);          % (Tp,s) --> (T,p,s)
% Preparing enthalpy-specific properties
T = repmat(T_vec,[N_p,1]);
p = reshape(repmat(p_vec(:).',[N_T,1]),[N_T*N_p,1]);
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,Tn_mat] = ...
                reshape_inputs4enthalpy(mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,T);

[h_1D,cp_1D,~,~,~,cpMod_s]  = getMixture_h_cp(y_s,Tn_mat,Tn_mat,Tn_mat,Tn_mat,Tn_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,opts);
rho = getMixture_rho(y_s,p,T,T,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron);
s_1D = getMixture_s(T,T,T,T,T,rho,y_s,mixCnst.R_s,mixCnst.nAtoms_s,...
                      mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,mixCnst.Mm_s,...
                      mixCnst.nAvogadro,mixCnst.hPlanck,mixCnst.kBoltz,mixCnst.L_s,mixCnst.sigma_s,mixCnst.thetaRot_s,opts);
h_mat = reshape(h_1D,[N_T,N_p]);                                                    % (Tp,1) --> (T,p)
cp_mat = reshape(cp_1D,[N_T,N_p]);                                                  % (Tp,1) --> (T,p)
s_mat = reshape(s_1D,[N_T,N_p]);                                                    % (Tp,1) --> (T,p)

%% Building an equispaced enthalpy and entropy vector, to get T=T(h,p) and T=T(s,p)
N_h = 10*N_T;                                           % HARDCODE ALERT we make it 10 times larger than T_vec
h_vec = linspace(min(h_mat(:)),max(h_mat(:)),N_h).';
s_vec = linspace(min(s_mat(:)),max(s_mat(:)),N_h).';
T_hp = zeros(N_h,N_p);                                  % allocating temperature as a function of enthalpy and pressure
T_sp = zeros(N_h,N_p);                                  % allocating temperature as a function of entropy and pressure
for ip=1:N_p                                            % looping pressure
    T_hp(:,ip) = interp1(h_mat(:,ip),T_vec,h_vec,'spline'); % interpolating temperature on enthalpy for a given pressure
    T_sp(:,ip) = interp1(s_mat(:,ip),T_vec,s_vec,'spline'); % interpolating temperature on entropy for a given pressure
end

%% loading mutation data
% mutationComparison = false;
%{%
% reacID = 1:3; % air5
% N_reac = 5; % air5
reacID = 1; % oxygen2
N_reac = 1; % oxygen2
mutationComparison = true;
% [T_mut,p_mut,h_mut,cp_mut,s_mut,ys_mut,Xs_mut,gs_mut,kf_mut,kb_mut] = mppequilWrap([T_min,100,T_max],[100,100,100000],[0,1,10,16,12],[2,0,12],mixtureMut{1},'-r',[0,1],N_reac);
[T_mut,p_mut,h_mut,cp_mut,s_mut,ys_mut,Xs_mut,gs_mut] = mppequilWrap([T_min,200,T_max],[100,100,100000],[0,1,10,16,12],[2,0,12],mixtureMut{1});
% [kf_mut,kb_mut] = mppequilWrap([T_min,100,T_max],p_min,[],[],mixtureMut{1},'-r',[0,1],N_reac);

T_mut  = unique(T_mut);
p_mut  = unique(p_mut);
NT_mut = length(T_mut);
Np_mut = length(p_mut);
ys_mut = permute(cell2mat(permute(ys_mut(:),[3,2,1])),[2,1,3]);
Xs_mut = permute(cell2mat(permute(Xs_mut(:),[3,2,1])),[2,1,3]);
gs_mut = permute(cell2mat(permute(gs_mut(:),[3,2,1])),[2,1,3]);
h_mut = permute(h_mut,[2,1,3]);
cp_mut = permute(cp_mut,[2,1,3]);
s_mut = permute(s_mut,[2,1,3]);

% kf_mut = cell2mat(permute(kf_mut(:),[3,2,1]));
% kb_mut = cell2mat(permute(kb_mut(:),[3,2,1]));
% lnkf_mut = log(kf_mut(:,reacID));
% lnkb_mut = log(kb_mut(:,reacID));
% lnkeq_mut = lnkf_mut - lnkb_mut;
%}

%% plotting results
clear plots legends % mole fractions
figure
for jj = 1:N_p
    clr = color_rainbow(jj,max(N_p,2));
    for ii=1:N_spec
    plots(jj) = plot(T_vec,X_mat(:,jj,ii),'Color',clr);
    hold on;
    if mutationComparison && (p_vec(jj)>=min(0.999999*p_mut) && p_vec(jj)<=max(1.000001*p_mut))
        if length(p_mut)>1
    Xs_p = interp1(p_mut,Xs_mut(:,:,ii),p_vec(jj),'spline','extrap'); % interpolating mutation mole fractions onto the pressure being plotted
        else
    Xs_p = Xs_mut(:,:,ii); % interpolating mutation mole fractions onto the pressure being plotted
        end
    plots(N_p+1) = plot(T_mut,Xs_p,'s','Color',clr);
    legends{N_p+1} = 'mutation++';
    end
    end
    legends{jj} = ['p = ',num2str(round(p_vec(jj))),' Pa'];
end
legend(plots,legends);
ylabel('X_s [-]');
xlabel('T [K]');
title(mixture);
%{%
clear plots legends % mass fractions
figure
for jj = 1:N_p
    clr = color_rainbow(jj,max(N_p,2));
    for ii=1:N_spec
    plots(jj) = plot(T_vec,ys_3D(:,jj,ii),'Color',clr);
    hold on;
    if mutationComparison && (p_vec(jj)>=min(0.999999*p_mut) && p_vec(jj)<=max(1.000001*p_mut))
        if length(p_mut)>1
    ys_p = interp1(p_mut,ys_mut(:,:,ii),p_vec(jj),'spline','extrap'); % interpolating mutation mass fractions onto the pressure being plotted
        else
    ys_p = ys_mut(:,:,ii);
        end
    plots(N_p+1) = plot(T_mut,ys_p,'s','Color',clr);
    legends{N_p+1} = 'mutation++';
    end
    end
    legends{jj} = ['p = ',num2str(round(p_vec(jj))),' Pa'];
end
legend(plots,legends);
ylabel('y_s [-]');
xlabel('T [K]');
title(mixture);

clear plots legends % enthalpy
figure
for jj = 1:N_p
    clr = color_rainbow(jj,max(N_p,2));
    plots(jj) = plot(T_vec,h_mat(:,jj),'Color',clr);
    hold on;
    if mutationComparison && (p_vec(jj)>=min(0.999999*p_mut) && p_vec(jj)<=max(1.000001*p_mut))
        if length(p_mut)>1
    h_p = interp1(p_mut,h_mut,p_vec(jj),'spline','extrap'); % interpolating mutation mass fractions onto the pressure being plotted
        else
    h_p = h_mut;
        end
    plots(N_p+1) = plot(T_mut,h_p,'s','Color',clr);
    legends{N_p+1} = 'mutation++';
    end
    legends{jj} = ['p = ',num2str(round(p_vec(jj))),' Pa'];
end
legend(plots,legends);
ylabel('h [J/kg]');
xlabel('T [K]');
title(mixture);

clear plots legends % entropy
figure
for jj = 1:N_p
    clr = color_rainbow(jj,max(N_p,2));
    plots(jj) = plot(T_vec,s_mat(:,jj),'Color',clr);
    hold on;
    if mutationComparison && (p_vec(jj)>=min(0.999999*p_mut) && p_vec(jj)<=max(1.000001*p_mut))
        if length(p_mut)>1
    s_p = interp1(p_mut,s_mut,p_vec(jj),'spline','extrap'); % interpolating mutation mass fractions onto the pressure being plotted
        else
    s_p = s_mut;
        end
    plots(N_p+1) = plot(T_mut,s_p,'s','Color',clr);
    legends{N_p+1} = 'mutation++';
    end
    legends{jj} = ['p = ',num2str(round(p_vec(jj))),' Pa'];
end
legend(plots,legends);
ylabel('s [J/kg-K]');
xlabel('T [K]');
title(mixture);

clear plots legends % heat capacity
figure
for jj = 1:N_p
    clr = color_rainbow(jj,max(N_p,2));
    plots(jj) = plot(T_vec,cp_mat(:,jj),'Color',clr);
    hold on;
    if mutationComparison && (p_vec(jj)>=min(0.999999*p_mut) && p_vec(jj)<=max(1.000001*p_mut))
        if length(p_mut)>1
    cp_p = interp1(p_mut,cp_mut,p_vec(jj),'spline','extrap'); % interpolating mutation mass fractions onto the pressure being plotted
        else
    cp_p = cp_mut;
        end
    plots(N_p+1) = plot(T_mut,cp_p,'s','Color',clr);
    legends{N_p+1} = 'mutation++';
    end
    legends{jj} = ['p = ',num2str(round(p_vec(jj))),' Pa'];
end
legend(plots,legends);
ylabel('cp [J/kg-K]');
xlabel('T [K]');
title(mixture);
%}

%% Equilibrium constant
if mutationComparison
%%%% OLD COMPARISON
%{
figure
subplot(2,2,1)
plot(T_vec,lnKeq_r,'r-',T_mut,lnkeq_mut,'k--');
ylim([min(min(lnkeq_mut)),Inf]);
ylim([-300,20]); xlim([200,1500]);
xlabel('T [K]');
% ylabel('ln(K_{eq}) [mol,m,s,K]');
grid minor
subplot(2,2,2)
plot(T_vec,lnKeq_r,'r-',T_mut,lnkeq_mut,'k--');
ylim([min(min(lnkeq_mut)),Inf]);
ylim([-20,20]); xlim([1500,20000]);
xlabel('T [K]');
ylabel('ln(K_{eq}) [mol,m,s,K]');
grid minor

% Derivative of lnKeqr with T
dlnKeqr_dT = getReactionDer_dlnKeqr_dT(y_s(1:length(T_vec),:),T_vec,mixCnst.delta_nuEq,mixCnst.Keq_struc,opts); % y_s does not appear in the expressions
dlnkeqmut_dT = diff(lnkeq_mut)./diff(repmat(T_mut,[1,size(lnkeq_mut,2)]));
subplot(2,2,3)
plot(T_vec,dlnKeqr_dT,'r-',T_mut(1:end-1),dlnkeqmut_dT,'k--',T_mut(2:end),dlnkeqmut_dT,'k--');
ylim([-Inf,0.15]); xlim([200,1500]);
xlabel('T [K]');
grid minor
subplot(2,2,4)
plot(T_vec,dlnKeqr_dT,'r-',T_mut(1:end-1),dlnkeqmut_dT,'k--',T_mut(2:end),dlnkeqmut_dT,'k--');
ylim([-Inf,0.1]); xlim([1500,Inf]);
xlabel('T [K]');
ylabel('\partial ln(K_{eq}) / \partial T [mol,m,s,K]');
grid minor
%}
%%%% UNTIL HERE
%%%% VERIFICATION, NOT NEEDED
%{
% Second derivative of lnKeqr with T
dlnKeqr_dT2 = getReactionDer2_dlnKeqr_dT2(T_vec,mixCnst.delta_nuEq,mixCnst.Keq_struc,opts);
dlnKeqr_dT2_app = diff(dlnKeqr_dT)./repmat(diff(T_vec),[1,size(dlnKeqr_dT,2)]);
subplot(3,2,5)
plot(T_vec,dlnKeqr_dT2,'r-',T_vec(1:end-1),dlnKeqr_dT2_app,'b-.',T_vec(2:end),dlnKeqr_dT2_app,'b-.');
% ylim([-1e-4,0]); xlim([1500,Inf]);
xlabel('T [K]');
ylabel('\partial^2 ln(K_{eq}) / \partial T^2 [mol,m,s,K]');
grid minor

% Third derivative of lnKeqr with T
dlnKeqr_dT3 = getReactionDer3_dlnKeqr_dT3(T_vec,mixCnst.delta_nuEq,mixCnst.Keq_struc,opts);
dlnKeqr_dT3_app = diff(dlnKeqr_dT2)./repmat(diff(T_vec),[1,size(dlnKeqr_dT2,2)]);
subplot(3,2,6)
plot(T_vec,dlnKeqr_dT3,'r-',T_vec(1:end-1),dlnKeqr_dT3_app,'b-.',T_vec(2:end),dlnKeqr_dT3_app,'b-.');
% ylim([-1e-4,0]); xlim([1500,Inf]);
xlabel('T [K]');
ylabel('\partial^3 ln(K_{eq}) / \partial T^3 [mol,m,s,K]');
grid minor
%}
%%%% UNTIL HERE
end

%% VERIFICATION BLOCK (to be removed)
%%%%% TEST ON THE SLIGHT DISCREPANCY WITH MUTATION
%{
if mutationComparison
lnkf_r = getReaction_lnkf(T_r,mixCnst.A_r,mixCnst.nT_r,mixCnst.theta_r); % forward rates
lnkf_r = lnkf_r(:,reacID);
lnkb_r = lnkf_r - lnKeq_r;
%{
deltah = linspace(-0.00001,0.00001,21);
% deltah = [-flip(deltah),0,deltah];
figure
clear plots legends;
for ii=11%1:length(deltah)
    opts.deltah = deltah(ii);
    lnKeq_r = getReaction_lnKeq(T_r,T_vec,T_vec,T_vec,T_vec,T_vec,mixCnst.delta_nuEq,mixCnst.Keq_struc,opts);
    lnkeq_interp = interp1(T_vec,lnKeq_r,T_mut,'spline','extrap');
    % figure
%     plots(ii) = plot(T_mut,exp(lnkeq_interp(:,1)-lnkeq_mut(:,1)),'Color',color_rainbow(ii,length(deltah)));
    % legends{ii} = ['\Delta Q_s = ',num2str(deltah(ii))];
    % legends{ii} = ['\Delta N_A = ',num2str(deltah(ii))];
    legends{ii} = ['\Delta h_{f,s}^o = ',num2str(deltah(ii))];
    hold on
    % ylim([0,1]);
    ylabel('K_{eq}^{mut} / K_{eq}^{DEKAF}'); xlabel('T [K]');
end
%}
%{%
figure
subplot(1,3,1)
plot(T_vec,lnKeq_r,'r-',T_mut,lnkeq_mut,'k--');
ylim([min(min(lnkeq_mut)),Inf]);
ylim([-100,20]);
xlabel('T [K]');
ylabel('ln(K_{eq}) [mol,m,s,K]');
grid minor

subplot(1,3,2)
plot(T_vec,lnkb_r,'r-',T_mut,lnkb_mut,'k--');
ylim([-5,20]);
xlabel('T [K]');
ylabel('ln(k_{br}) [mol,m,s,K]');
grid minor

subplot(1,3,3)
plot(T_vec,lnkf_r,'r-',T_mut,lnkf_mut,'k--');
ylim([-100,Inf]);
xlabel('T [K]');
ylabel('ln(k_{fr}) [mol,m,s,K]');
grid minor
end


% comparing the jumps in gibbs free energy
%{
gibbs_s = getSpecies_gibbs(T_vec,4000*ones(size(T_vec)),mixCnst.R_s,mixCnst.Mm_s,mixCnst.thetaRot_s,mixCnst.L_s,mixCnst.sigma_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.kBoltz,mixCnst.Keq_struc.hPlanck,mixCnst.nAvogadro,mixCnst.hForm_s);
delta_g(:,1) = 2*gibbs_s(:,1) - gibbs_s(:,4); % N2 diss.
delta_g(:,2) = 2*gibbs_s(:,2) - gibbs_s(:,5); % O2 diss.
delta_g(:,3) = gibbs_s(:,1) + gibbs_s(:,2) - gibbs_s(:,3); % NO diss.

g_mut = reshape(gs_mut(p_mut==4000,:,:),[NT_mut,N_spec]);
delta_g_mut(:,1) = 2*g_mut(:,1) - g_mut(:,4); % N2 diss.
delta_g_mut(:,2) = 2*g_mut(:,2) - g_mut(:,5); % O2 diss.
delta_g_mut(:,3) = g_mut(:,1) + g_mut(:,2) - g_mut(:,3); % NO diss.

delta_g_interp = interp1(T_vec,delta_g,T_mut,'spline','extrap');
% Rotating the gibbs free energy plot to see what is causing the wiggles
%{
T_norm = T_mut/max(T_mut);
delta_g_interp_norm = delta_g_interp./max(max(abs(delta_g_interp)));
delta_g_mut_norm = delta_g_mut./max(max(abs(delta_g_mut)));
polycoeff = polyfit(T_norm,delta_g_interp_norm(:,1),1); % getting slope of plot
alf = atand(polycoeff(1)); % rotation angle
g_rot = delta_g_interp_norm(:,1)*cosd(alf) - T_norm*sind(alf);
T_rot = T_norm*cosd(alf) + delta_g_interp_norm(:,1)*sind(alf);

g_rot_mut = delta_g_mut_norm(:,1)*cosd(alf) - T_norm*sind(alf);
T_rot_mut = T_norm*cosd(alf) + delta_g_mut_norm(:,1)*sind(alf);
figure
plot(T_rot,g_rot,'r-',T_rot_mut,g_rot_mut,'k--');
ylabel('curvenormal \Delta_r g_s - T [?¿]'); xlabel('curvewise \Delta_r g_s - T [?¿]');
% ylabel('\Delta_r g_s'); xlabel('T [K]');
%}
figure
plot(T_mut,(delta_g_interp-delta_g_mut)./mean(mean(g_mut)));
ylabel('\Delta_r g_s^{DEKAF} - \Delta_r g_s^{mut} [normalized with |g_s^{mut}|]'); xlabel('T [K]');
end
%}

%}
%%%%%%% UNTIL HERE

%%%%%% COMPARING VISCOSITY, HEAT CAPACITY AND THERMAL CONDUCTIVITY TO MUTATION
%{
if mutationComparison
% Choosing a single p and computing mass fractions and temperature derivatives
[~,ip] = min(abs(p_vec-p_mut(round(Np_mut/2))));
X_s = reshape(X_mat(:,ip,:),[N_T,N_spec]);
T = T_vec;
p1 = p_vec(ip);
y_s = getEquilibrium_ys(X_s,mixCnst.Mm_s);                                  % computing mass fraction from mole fractions

rho             = getMixture_rho(y_s,p1*ones(size(T)),T,T,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron);  % computing mixture density and other properties ...
dlnKeqr_dT      = getReactionDer_dlnKeqr_dT(y_s,T,mixCnst.delta_nuEq,mixCnst.Keq_struc,opts);
dlnKpeq_dT      = getEquilibriumDer_dlnKpeq_dT(dlnKeqr_dT,T,sum(mixCnst.delta_nuEq,1)');

mixture_short = regexp(mixture,'^[a-zA-Z]+\d+','match'); mixture_short = mixture_short{1};
switch mixture_short
    case 'air5'
[dXs_dT,~]      = getEquilibriumDer_X_air5(1,X_s,p1*ones(size(T)),mixCnst.XEq,mixCnst.spec_list,mixCnst.reac_list_eq,dlnKpeq_dT);
    case 'air11'
[dXs_dT,~]      = getEquilibriumDer_X_air11(1,X_s,p1*ones(size(T)),mixCnst.XEq(1),mixCnst.spec_list,mixCnst.reac_list_eq,dlnKpeq_dT);
    otherwise
        error(['The chosen mixture ''',mixture_short,'''does not have a supported getEquilibriumDer_X_* function']);
end
dys_dT          = getEquilibriumDer_dys_dq(X_s,dXs_dT,mixCnst.Mm_s);              % .. to obtain the mass fraction derivatives with T and p
dys_dT_approx = diff(y_s,1)./diff(repmat(T_vec,[1,N_spec]));
dys_dT_approx(end+1,:) = dys_dT_approx(end,:);
dXs_dT_approx = diff(X_s,1)./diff(repmat(T_vec,[1,N_spec]));
dXs_dT_approx(end+1,:) = dXs_dT_approx(end,:);

% Comparing viscosity and thermal conductivity
Sc_e = 0.5;
% Preparing enthalpy-specific properties
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,Tn_mat] = ...
                reshape_inputs4enthalpy(mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,T);
[hh,~,~,h_s,~,cpMod_s,cvMod_s] = getMixture_h_cp(y_s,Tn_mat,Tn_mat,Tn_mat,Tn_mat,Tn_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,opts);
[mu,mu_s]      = getMixture_mu_BlottnerWilke(y_s,T,T,mixCnst.Mm_s,mixCnst.Amu_s,mixCnst.Bmu_s,mixCnst.Cmu_s,mixCnst.R0,mixCnst.bElectron);
kappa   = getMixture_kappa_EuckenWilke(y_s,mu_s,T,T,cvMod_s.cvTrans_s,cvMod_s.cvRot_s,cvMod_s.cvVib_s,cvMod_s.cvElec_s,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron);
mu_mat = repmat(mu,[1,N_spec]); % same diffusion coefficient for all species depends only on mixture mu and rho
rho_mat = repmat(rho,[1,N_spec]);
D_s = mu_mat./(rho_mat * Sc_e);
kappa_reac = getEquilibrium_kappareac('spec-mix',h_s,rho,D_s,dXs_dT);
kappa_tot = kappa + kappa_reac;

cp_reac = getEquilibrium_cpreac(h_s,dys_dT);
cp_tot = cp_mat(:,ip) + cp_reac;
cp_approx = diff(hh)./diff(T_vec); cp_approx(end+1) = cp_approx(end);

mu_mat = reshape(data(:,3),[NT_mut,Np_mut]);
kappa_mat = reshape(data(:,4),[NT_mut,Np_mut]);
cp_matt = reshape(data(:,7),[NT_mut,Np_mut]);
mu_mat = permute(mu_mat,[2,1]);
kappa_mat = permute(kappa_mat,[2,1]);
cp_matt = permute(cp_matt,[2,1]);
mu_mut = interp1(p_mut,mu_mat,p1,'spline','extrap');
kappa_mut = interp1(p_mut,kappa_mat,p1,'spline','extrap');
cp_mut = interp1(p_mut,cp_matt,p1,'spline','extrap');

%% PLOTTING
figure
plot(T,dXs_dT);
hold on; grid minor;
plot(T,dXs_dT_approx,'k--');
xlabel('T [K]'); ylabel('dX_s/dT [K^{1}]');
figure
plot(T,dys_dT);
hold on; grid minor;
plot(T,dys_dT_approx,'k--');
xlabel('T [K]'); ylabel('dy_s/dT [K^{1}]');

figure
subplot(2,2,1)
plot(T,mu,T_mut,mu_mut); xlim([0,10e3]);
xlabel('T [K]'); ylabel('\mu [kg/m-s]'); grid minor;
subplot(2,2,2)
plot(T,kappa_tot,T_mut,kappa_mut); xlim([0,10e3]);
xlabel('T [K]'); ylabel('\kappa_{eq} [W/K-m]'); grid minor;
subplot(2,2,3)
plot(T,cp_tot,T_mut,cp_mut); hold on; grid minor;
plot(T,cp_approx,'k--'); xlim([0,10e3]);
xlabel('T [K]'); ylabel('cp_{eq} [J/kg-K]');
legend('DEKAF Blottner+Eucken+cstSc','mutation++','DEKAF approx');
subplot(2,2,4)
plot(T,D_s(:,1)); xlim([0,10e3]);
xlabel('T [K]'); ylabel('D_s [m^2/s]'); grid minor;
end
%}
%%%%%% UNTIL HERE

%% Reshaping
save(savename,'X_mat','h_mat','s_mat','T_vec','T_i','p_vec','h_vec','s_vec','T_hp','T_sp','spec_list');
