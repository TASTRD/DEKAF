% Sample launch script to compute the compressible self-similar boundary
% layer profile using VESTA's self-similar BL solver
%
% Author: Fernando Miro Miro
% Date: December 2015

clear;
close all;
addpath(genpath('/home/miromiro/workspace/VESTA/'));
addpath(genpath('/home/miromiro/workspace/DEKAF/SourceCode/'));
M_vec = [0.01,0.25:0.25:30];
%T_vec = [150,300,600,900,1200];
gw_min = 0.2;
N_Tw = 15;
N_M = length(M_vec);
styleS = {'-','-.','--',':'};
styleS = [styleS,styleS,styleS,styleS,styleS];

g_mat = zeros(10001,N_M,N_Tw);
dg_deta_mat = zeros(10001,N_M,N_Tw);
dg_deta2_mat = zeros(10001,N_M,N_Tw);
for ii=1:N_M
    clear plots1 legends1 plots2 legends2 plots3 legends3
    M_e = M_vec(ii);
    T_e = 300;
    gam = 1.4;
    Ec = (gam-1)*M_e^2;
    % The first run is going to be adiabatic, and then we will ramp down
    % the G_bc until we reach the equivalent to gw of 0.2
    options.H_F = true;
    options.G_bc = 0;
    for jj=1:N_Tw
        options.tol = 1e-15;
        options.L = 40;
        options.N = 10001;
        options.s_new = 1.0;
        if jj~=1
        options.H_F= false;
        options.G_bc = Gw_vecs(jj);
        end
        options.beta = 0;
        options.import_sol = true;
        options.FileName = 'M12T300p4000gw0.0067114fw0.mat';
        options.fluid = 'air';
        %options.M_e = 5;
        options.M_e = M_e;
        options.T_e = T_e;
        options.p_e = 4000;
        options.save = false;
        compr_profile = CBL_profile (options);
        % for the first iteration (for this Mach) we generate the vector of
        % Gwalls
        if jj==1
            Gw_vecs = linspace(compr_profile.g(1),gw_min/(1+Ec/2),N_Tw);
        end
        %% Rebuilding g profile (DEKAF nomenclature)

        g_mat(:,ii,jj) = compr_profile.g*(1+Ec/2) - compr_profile.f_p.^2*Ec/2;
        dg_deta_mat(:,ii,jj) = compr_profile.g_p*(1+Ec/2) - compr_profile.f_p.*compr_profile.f_pp * Ec;
        dg_deta2_mat(:,ii,jj) = compr_profile.g_pp*(1+Ec/2) - compr_profile.f_pp.^2 * Ec - compr_profile.f_p.*compr_profile.f_ppp*Ec;
        eta = linspace(0,options.L,options.N);

        rainbowVec = color_rainbow(jj,N_Tw);
        figure(103);
        subplot(11,11,ii);
        title(['M = ',num2str(M_vec(ii))]);
        plots3(jj) = plot(dg_deta2_mat(:,ii,jj),eta,styleS{jj},'LineWidth',2,'Color',rainbowVec);
        %legends3{jj} = ['T_e = ',num2str(T_vec(jj))];
        legends3{jj} = ['g_w = ',num2str(compr_profile.g(1))];
        if ii==30
        legend(plots3,legends3);
        end
        xlabel('d2g/deta2');
        hold on
        ylim([0,7]);

        figure(102);
        subplot(11,11,ii);
        title(['M = ',num2str(M_vec(ii))]);
        plots2(jj) = plot(dg_deta_mat(:,ii,jj),eta,styleS{jj},'LineWidth',2,'Color',rainbowVec);
        %legends2{jj} = ['T_e = ',num2str(T_vec(jj))];
        legends2{jj} = ['g_w = ',num2str(compr_profile.g(1))];
        if ii==30
        legend(plots2,legends2);
        end
        xlabel('dg/deta');
        hold on
        ylim([0,7]);

        figure(101);
        subplot(11,11,ii);
        title(['M = ',num2str(M_vec(ii))]);
        plots1(jj) = plot(g_mat(:,ii,jj),eta,styleS{jj},'LineWidth',2,'Color',rainbowVec);
        %legends1{jj} = ['T_e = ',num2str(T_vec(jj))];
        legends1{jj} = ['g_w = ',num2str(compr_profile.g(1))];
        if ii==30
        legend(plots1,legends1);
        end
        xlabel('g');
        hold on
        ylim([0,7]);

        pause(0.2);
    end
    save('VeryColdWallProfiles_VESTA.mat','eta','g_mat','dg_deta_mat','dg_deta2_mat');
end

%% keeping only the middle temperature (it doesn't seem to modify the profiles too much) and saving
M_mat = repmat(M_vec',[1,N_Tw]);
gw_mat = reshape(g_mat(1,:,:),[N_M,N_Tw]);
save('VeryColdWallProfiles_VESTA.mat','eta','g_mat','dg_deta_mat','dg_deta2_mat','M_mat','gw_mat');