% Sample launch script to compute the compressible self-similar boundary
% layer profile using VESTA's self-similar BL solver.
%
% It generates BLs with beta=1 (stagnation line)
%
% Author: Fernando Miro Miro
% Date: September 2018

% clear;
% close all;
%addpath(genpath([getenv('VESTA_DIRECTORY'),'/VESTA_code']));
%addpath(genpath([getenv('DEKAF_DIRECTORY'),'_stable/']));
savename = 'VeryColdWallProfiles_VESTA_beta1.mat';
M_vec = [0.3:0.02:0.44,0.45:0.01:0.91,0.912:0.002:0.94,0.941:0.001:1];
M_vecSave = 0.3:0.02:1; % those that we actually want to save
%T_vec = [150,300,600,900,1200];
gw_min = 0.2;
dgw = 0.001;
N_TwSave = 50;
N_M = length(M_vec);
N_MSave = length(M_vecSave);
[Ni,Nj] = optimalPltSize(N_MSave);
styleS = {'-','-.','--',':'};
styleS = repmat(styleS,[1,ceil(N_TwSave/4)]);

g_mat = zeros(10001,N_MSave,N_TwSave);
dg_deta_mat = zeros(10001,N_MSave,N_TwSave);
dg_deta2_mat = zeros(10001,N_MSave,N_TwSave);
df_deta_mat = zeros(10001,N_MSave,N_TwSave);
df_deta2_mat = zeros(10001,N_MSave,N_TwSave);
df_deta3_mat = zeros(10001,N_MSave,N_TwSave);
% Restart options
load(savename);
Mstart = 0.46;
idxM1 = find(M_vec==Mstart);
for ii=idxM1:N_M
    clear plots1 legends1 plots2 legends2 plots3 legends3 plots4 legends4 plots5 legends5 plots6 legends6
    M_e = M_vec(ii);
    T_e = 300;
    gam = 1.4;
    Ec = (gam-1)*M_e^2;
    % The first run is going to be adiabatic, and then we will ramp down
    % the G_bc until we reach the equivalent to gw of 0.2
    options.H_F = true;
    options.G_bc = 0;
    N_Tw = Inf; % initial value
    jj=0;
    while jj<N_Tw
        jj=jj+1;
        options.tol = 1e-15;
        options.L = 40;
        options.N = 10001;
        options.s_new = 1.0;
        if jj~=1
        options.H_F= false;
        options.G_bc = Gw_vecs(jj);
        end
        options.beta = 1;
        options.import_sol = true;
        if ii==1 && jj==1 % very first guess
            options.FileName = 'M0.3T300p4000gwp0fw0.mat';
        elseif ii~=1 && jj==1 % adiabatic first guess (previous adiabatic case)
            options.FileName = ['M',num2str(M_vec(ii-1)),'T',num2str(T_e),'p4000gwp0fw0.mat'];
        elseif jj==2 % for second (we must use adiabatic as first guess)
            options.FileName = ['M',num2str(M_e),'T',num2str(T_e),'p4000gwp0fw0.mat'];
        else % for all other cases
            options.FileName = ['M',num2str(M_e),'T',num2str(T_e),'p4000gw',num2str(Gw_vecs(jj-1)),'fw0.mat'];
        end
        options.fluid = 'air';
        %options.M_e = 5;
        options.M_e = M_e;
        options.T_e = T_e;
        options.p_e = 4000;
        options.save = true;
        options.autoname = true;
        compr_profile = CBL_profile (options);
        if ~(jj<=2) % we delete all non-adiabatic files
            delete(options.FileName); % we want the adiabatic ones to remain, because they will be used later
        end
        % for the first iteration (for this Mach) we generate the vector of
        % Gwalls
        if jj==1
            Gw_vecs = (compr_profile.g(1):-dgw:gw_min/(1+Ec/2));
            N_Tw = length(Gw_vecs);
            Gw_vecsSave = Gw_vecs(round(linspace(1,N_Tw,N_TwSave)));
        end
        %% Rebuilding g profile (DEKAF nomenclature)

        idxM = find(abs(M_vecSave-M_vec(ii))<1e-5); % position of the iith M in M_vecSave
        idxGw = find(abs(Gw_vecsSave-Gw_vecs(jj))<1e-5); % position of the jjth Gwall in Gwall_vecSave
        if ~isempty(idxM) && ~isempty(idxGw)
            g_mat(:,idxM,idxGw) = compr_profile.g*(1+Ec/2) - compr_profile.f_p.^2*Ec/2;
            dg_deta_mat(:,idxM,idxGw) = compr_profile.g_p*(1+Ec/2) - compr_profile.f_p.*compr_profile.f_pp * Ec;
            dg_deta2_mat(:,idxM,idxGw) = compr_profile.g_pp*(1+Ec/2) - compr_profile.f_pp.^2 * Ec - compr_profile.f_p.*compr_profile.f_ppp*Ec;
            df_deta_mat(:,idxM,idxGw) = compr_profile.f_p;
            df_deta2_mat(:,idxM,idxGw) = compr_profile.f_pp;
            df_deta3_mat(:,idxM,idxGw) = compr_profile.f_ppp;
            eta = linspace(0,options.L,options.N);
            %{
            rainbowVec = color_rainbow(idxGw,N_TwSave);
            figure(103);
            subplot(Ni,Nj,idxM);
            title(['M = ',num2str(M_vec(ii))]);
            plots3(idxGw) = plot(dg_deta2_mat(:,idxM,idxGw),eta,styleS{idxGw},'LineWidth',2,'Color',rainbowVec);
            %legends3{idxGw} = ['T_e = ',num2str(T_vec(idxGw))];
            legends3{idxGw} = ['g_w = ',num2str(compr_profile.g(1))];
            if idxM==N_MSave
                legend(plots3,legends3);
            end
            xlabel('d2g/deta2');
            hold on
            ylim([0,7]);

            figure(102);
            subplot(Ni,Nj,idxM);
            title(['M = ',num2str(M_vec(ii))]);
            plots2(idxGw) = plot(dg_deta_mat(:,idxM,idxGw),eta,styleS{idxGw},'LineWidth',2,'Color',rainbowVec);
            %legends2{idxGw} = ['T_e = ',num2str(T_vec(idxGw))];
            legends2{idxGw} = ['g_w = ',num2str(compr_profile.g(1))];
            if idxM==N_MSave
                legend(plots2,legends2);
            end
            xlabel('dg/deta');
            hold on
            ylim([0,7]);

            figure(101);
            subplot(Ni,Nj,idxM);
            title(['M = ',num2str(M_vec(ii))]);
            plots1(idxGw) = plot(g_mat(:,idxM,idxGw),eta,styleS{idxGw},'LineWidth',2,'Color',rainbowVec);
            %legends1{idxGw} = ['T_e = ',num2str(T_vec(idxGw))];
            legends1{idxGw} = ['g_w = ',num2str(compr_profile.g(1))];
            if idxM==N_MSave
                legend(plots1,legends1);
            end
            xlabel('g');
            hold on
            ylim([0,7]);

            figure(106);
            subplot(Ni,Nj,idxM);
            title(['M = ',num2str(M_vec(ii))]);
            plots6(idxGw) = plot(df_deta3_mat(:,idxM,idxGw),eta,styleS{idxGw},'LineWidth',2,'Color',rainbowVec);
            %legends6{idxGw} = ['T_e = ',num2str(T_vec(idxGw))];
            legends6{idxGw} = ['g_w = ',num2str(compr_profile.g(1))];
            if idxM==N_MSave
                legend(plots6,legends6);
            end
            xlabel('d3f/deta3');
            hold on
            ylim([0,7]);

            figure(105);
            subplot(Ni,Nj,idxM);
            title(['M = ',num2str(M_vec(ii))]);
            plots5(idxGw) = plot(df_deta2_mat(:,idxM,idxGw),eta,styleS{idxGw},'LineWidth',2,'Color',rainbowVec);
            %legends5{idxGw} = ['T_e = ',num2str(T_vec(idxGw))];
            legends5{idxGw} = ['g_w = ',num2str(compr_profile.g(1))];
            if idxM==N_MSave
                legend(plots5,legends5);
            end
            xlabel('d2f/deta2');
            hold on
            ylim([0,7]);

            figure(104);
            subplot(Ni,Nj,idxM);
            title(['M = ',num2str(M_vec(ii))]);
            plots4(idxGw) = plot(df_deta_mat(:,idxM,idxGw),eta,styleS{idxGw},'LineWidth',2,'Color',rainbowVec);
            %legends4{idxGw} = ['T_e = ',num2str(T_vec(idxGw))];
            legends4{idxGw} = ['g_w = ',num2str(compr_profile.g(1))];
            if idxM==N_MSave
                legend(plots4,legends4);
            end
            xlabel('df/deta');
            hold on
            ylim([0,7]);
            %}
        end
    end
    delete(['M',num2str(M_e),'T',num2str(T_e),'p4000gw',num2str(Gw_vecs(end)),'fw0.mat']);
    M_mat = repmat(M_vecSave',[1,N_TwSave]);
    gw_mat = reshape(g_mat(1,:,:),[N_MSave,N_TwSave]);
    save(savename,'eta','g_mat','dg_deta_mat','dg_deta2_mat','df_deta_mat','df_deta2_mat','df_deta3_mat','M_mat','gw_mat');
end

%% keeping only the middle temperature (it doesn't seem to modify the profiles too much) and saving
M_mat = repmat(M_vecSave',[1,N_TwSave]);
gw_mat = reshape(g_mat(1,:,:),[N_MSave,N_TwSave]);
save(savename,'eta','g_mat','dg_deta_mat','dg_deta2_mat','df_deta_mat','df_deta2_mat','df_deta3_mat','M_mat','gw_mat');
