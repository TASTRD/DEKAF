function mixCnst_out = compareAndPopulate_mixCnst(mixCnst_old,mixCnst_new,varargin)
% compareAndPopulate_mixCnst is a function that compares an old and a new
% mixCnst structure and pops warnings whenever there are mismatches.
%
% Usage:
%   (1)
%       mixCnst_out = compareAndPopulate_mixCnst(mixCnst_old,mixCnst_new)
%
% Inputs:
%   mixCnst_old
%       mixCnst structure obtained with an older version of DEKAF
%   mixCnst_new
%       mixCnst structure obtained with a newer version of DEKAF
%
% Outputs:
%   mixCnst_out
%       mixCnst structure after the necessary modifications/comparisons
%  
% Author(s): Fernando Miro Miro
% 
% GNU Lesser General Public License 3.0

[recursiveName,~] = parse_optional_input(varargin,'recursiveName','');

mixCnst_out = struct;                                                       % allocating
flds = unique([fieldnames(mixCnst_new);fieldnames(mixCnst_old)]);           % fieldnames of the new mixCnst structure
for ii=1:length(flds)                                                       % looping new fields
    if ~isfield(mixCnst_old,flds{ii})                                           % if the field does not exist in the old structure
        warning(['field ''',recursiveName,flds{ii},''' was not found in the older mixCnst structure, so it was populated with defaults.']);
        mixCnst_out.(flds{ii}) = mixCnst_new.(flds{ii});                            % we populate it with the new value
    elseif ~isfield(mixCnst_new,flds{ii})                                       % if the field does not exist in the new structure
        mixCnst_out.(flds{ii}) = mixCnst_old.(flds{ii});                            % we populate it with the new value
    else                                                                        % if the field exists in both, we compare them
        if isstruct(mixCnst_new.(flds{ii}))                                         % if the field is a structure
            mixCnst_out.(flds{ii}) = compareAndPopulate_mixCnst(mixCnst_old.(flds{ii}),mixCnst_new.(flds{ii}),'recursiveName',[flds{ii},'.']); % recursive call
        end
        mixCnst_out.(flds{ii}) = mixCnst_old.(flds{ii});                            % we keep the old value
    end
end                                                                         % close field loop


end % compareAndPopulate_mixCnst