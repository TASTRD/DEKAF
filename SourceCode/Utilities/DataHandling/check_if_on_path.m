function on_path = check_if_on_path(folder)
%CHECK_IF_ON_PATH Given a folder, determine if it is on the MATLAB path.
%
% Author: Ethan Beyak
% Date: 24 Aug 2018
% GNU Lesser General Public License 3.0

% using intrinsics: path, pathsep
path_cell = regexp(path, pathsep, 'split');
if ispc     % Windows is not case-sensitive
  on_path = any(strcmpi(folder, path_cell));
else
  on_path = any(strcmp(folder, path_cell));
end

end
