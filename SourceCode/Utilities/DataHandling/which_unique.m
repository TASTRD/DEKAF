function pathOut = which_unique(fileName)
% which_unique does the same as matlab's 'which' function, only that it pops
% an error if a unique path is not found (0 or >1 paths).
%
% Usage:
%   (1)
%       pathOut = which_unique(fileName)
%
% See also: which
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

pathCell = which(fileName,'-All');
if length(pathCell)>1                                                           % checking that it is not multiply defined
    error(['multiple paths to ',fileName,' where found:',sprintf('\n     %s',pathCell{:}),...
        sprintf('\n'),'Try removing other DEKAF versions from the matlab path or restarting matlab.']); %#ok<SPRINTFN>
elseif isempty(pathCell)                                                      % checking if it is empty (not found)
    error(['the ',fileName,' file could not be found. Make sure the paths are correctly added']);
else
    pathOut = pathCell{1};
end
