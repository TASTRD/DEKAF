function [var_out,bool,idx_match] = find_flagAndRemove(str,var_in)
% find_flagAndRemove looks for a specific string flag in an array of
% inputs, and returns, either the same array and false, or the array
% without the specified string and true.
%
% Examples:
%   (1)     [var_out,bool] = find_flagAndRemove(str,var_in);
%
%   (2)     [var_out,bool,idx] = find_flagAndRemove(str,var_in);
%       |--> returns also the position that the boolean was occupying in
%       varargin
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

bStrgs = cellfun(@ischar,var_in);                                           % identifying characters in var_in
bmatch = strcmp(var_in(bStrgs),str);                                        % we look for str amongst the chars
if nnz(bmatch)                                                              % it means that the user passed the flag str
    bool = true;                                                                % set boolean to true
    idx = 1:length(var_in);                                                     % position index
    idx(~bStrgs) = [];                                                          % eliminating non-strings
    idx_match = idx(bmatch);                                                    % keeping the one that matched str
    var_out = {var_in{1:idx_match-1},var_in{idx_match+1:end}};                  % eliminating str entry
else                                                                        % the user didn't pass the flag str
    bool = false;                                                               % set boolean to false
    var_out = var_in;                                                           % make outputs equal to inputs
    idx_match = [];
end