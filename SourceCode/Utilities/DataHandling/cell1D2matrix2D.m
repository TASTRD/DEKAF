function mat2D = cell1D2matrix2D(cell1D)
% cell1D2matrix2D(cell1D) converts a one-dimensional cell with vectors of
% equal length to a two-dimensional matrix with these vectors arranged in
% columns.
%
% See also: matrix2D2cell1D
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_cell = length(cell1D);         % Number of cell positions
N_vec = length(cell1D{1}(:));       % number of positions in each column vector in the cell
mat2D = zeros(N_vec,N_cell);     % allocating
for ii=1:N_cell                  % looping cell
    mat2D(:,ii) = cell1D{ii}(:); % populating matrix
end             % Note: the (:) ensures the output to be arranged in columns