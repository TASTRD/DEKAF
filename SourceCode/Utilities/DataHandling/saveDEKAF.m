function [] = saveDEKAF(path,varargin)
% saveDEKAF does the same as matlab's save function, only that it appends
% also the information of the git revision and branch that is currently
% being used.
%
% Examples:
%   saveDEKAF(path,'var1','var2',...)
%
% See also: getGitInfo
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

gitInfo = getGitInfo();                                                     % obtaining revision info
gitInfo.matlabVersion = version;                                            % saving matlab version
gitInfo.date = datetime;                                                    % saving date and time
gitInfo.computer = computer;                                                % saving computer info
wkspc_info = dbstack;                                                       % obtaining workspace info (arranged from most to least global)
wkspc_names = {wkspc_info(:).name};                                         % keeping only the workspace names
idx_save = ~cellfun(@isempty,regexp(wkspc_names,'saveVESTA','match'));      % index in wkspc_names where saveVESTA is
if idx_save+1>length(wkspc_names)                                           % if the saveVESTA index is the last one (called from the base)
    wkspc_save = 'base';                                                        % we will save in the base
else                                                                        % otherwise
    wkspc_save = 'caller';                                                      % we save in caller (the function that called saveVESTA)
end
evalin(wkspc_save,['save(''',path,''',''',strjoin(varargin,''','''),''');']); % saving all chosen variables from the workspace at level N-1
save(path,'-append','gitInfo');                                             % appending to the saved file, the gitInfo