function [Xout,cll,iXcll] = parse_optional_input(cll,X,varargin)
% parse_optional_input checks for set of varargin inputs if a particular
% flag was activated. If it was, it returns the input right after it, and
% if it doesn't it either breaks, or sets a default value.
%
%   Examples
%       (1)     Xout = parse_optional_input(cll,X)
%           |--> breaks if the flag was not found
%       (2)     Xout = parse_optional_input(cll,X,X0)
%           |--> sets a default if the flag was not found
%       (3)     [Xout,cll] = parse_optional_input(...)
%           |--> returns the inputted cell without the searched flag
%       (3)     [Xout,cll,iXcll] = parse_optional_input(...)
%           |--> returns the index in the cell where the following flag is
%
%   Inputs & outputs:
%       -   cll         cell of inputs coming from varargin
%       -   X           string identifying the searched flag
%       -   X0          default value
%       -   Xout        value to output
%       -   iXcll       position within cll where the searched flag was
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% checking inputs and setting booleans
if isempty(varargin)
    bBreak = true;
else
    bBreak = false;
    X0 = varargin{1};
end

% searching for X flag in cll
bcllstr = cellfun(@isstr,cll);                                  % identifying the cells that are actually strings
cllstr = cll;                                                   % initializing cell with only strings
cllstr(~bcllstr) = {' '};                                       % replacing non-strings with empty spaces
bXcll = ~cellfun(@isempty,regexp(cllstr,['^',X,'$'],'once'));   % the ^ and $ signs force to match the whole word
if sum(bXcll)                                                   % if the flag was found
    iXcll = find(bXcll);                                            % position of flag in cell
    Xout = cll{iXcll+1};                                            % reading variable from cell
    cll(iXcll:iXcll+1) = [];                                        % clearing out the read option
else                                                            % otherwise
    if bBreak                                                       % if the user didn't set a default for the value - break
        error(['the flag ',X,' was not found amongst the inputs']);
    else                                                        % if they did then
        Xout = X0;                                                  % setting default
        iXcll = [];
    end
end
