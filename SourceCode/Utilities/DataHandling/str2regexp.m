function str_out = str2regexp(str_in)
% str2regexp converts a string such that it can be used as an argument in a
% regular expression.
%
% It essentially escapes all the regexp metacharacters:
%    \ [ ] ( ) | ^ $ * + - ? ! _ / "
%
% Usage:
%   (1)
%       str_out = str2regexp(str_in)
%
% Examples:
%   (1)
%       str2regexp('/home/miromiro/workspace/DEKAF/whatever_folder-here')
%           ans = '\\/home\\/miromiro\\/workspace\\/DEKAF\\/whatever\\_folder\\-here'
%
% Author: Fernando Miro Miro
% Date: December 2019
% GNU Lesser General Public License 3.0

str2escape = {'\','[',']','(',')','|','^','$','*','+','-','?','!','_','/','"'};
% note that the escape symbol \ must be substituted first, otherwise it will
% also be escaped itself
str_out = str_in;
for ii=1:length(str2escape)
    str_out = strrep(str_out,str2escape{ii},['\',str2escape{ii}]);
end

end % str2regexp