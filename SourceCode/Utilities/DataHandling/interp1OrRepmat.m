function varargout = interp1OrRepmat(varargin)
% interp1OrRepmat interpolates or uses repmat if the original vector is of
% length 1
%
% Usage: the same as interp1
%
% See also: interp1
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if length(varargin{1})==1       % we use repmat
    switch length(varargin)
        case 1                      % V is in position 1
            V  = varargin{1};
            Xq = varargin{2};
        otherwise                   % V is in position 2
            V  = varargin{2};
            Xq = varargin{3};
    end
    varargout{1} = repmat(V,size(Xq));
else                            % we use interp1
    [varargout{1:nargout}] = interp1(varargin{:});
end

end % interp1OrRepmat