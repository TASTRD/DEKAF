function options = assess_inputs(intel, options,varargin)
%ASSESS_INPUTS Check intel and options input structures for invalid field values
% Throw warnings if any deprecated options have been specified
%
% Usage:
%   (1)
%       options = assess_inputs(intel, options)
%
%   (2)
%       [...] = assess_inputs(...,'onlyRenamedVars')
%       |--> only emits warnings of renamed variables, not of
%       user-specified variables that are now internal
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

[~,bOnlyRenamedVars] = find_flagAndRemove('onlyRenamedVars',varargin);

% Check for deprecated values -- options
if (isfield(options,'ynodesrequested') && options.ynodesrequested)
    warning('options.ynodesrequested has been deprecated and should be removed from the options struct!');
end

if ~bOnlyRenamedVars && isfield(options,'Cooke')
    warning('options.Cooke has been deprecated. Whether the w-momentum equation is included in the equation system or not depends on the value of intel.Lambda_sweep');
end

if isfield(options,'bAmbipolar')
    warning('options.bAmbipolar has been deprecated, since it actually referred to charge neutrality. The new option is called ''bChargeNeutrality'' and it was defined equal to what was passed in bAmbipolar');
    options.bChargeNeutrality = options.bAmbipolar;
    options = rmfield(options,'bAmbipolar');
end

if isfield(options,'bVaryingEdgeFSC')
    warning('options.bVaryingEdgeFSC has been deprecated, since it actually referred to QSS (quasi-self-similarity) rather than FSC (Falkner-Skan-Cooke). The new option is called ''bVaryingEdgeQSS'' and it was defined equal to what was passed in bVaryingEdgeFSC');
    options.bVaryingEdgeQSS = options.bVaryingEdgeFSC;
    options = rmfield(options,'bVaryingEdgeFSC');
end

if isfield(options,'fixReFSC')
    warning('options.fixReFSC has been deprecated, since it actually referred to QSS (quasi-self-similarity) rather than FSC (Falkner-Skan-Cooke). The new option is called ''fixReQSS'' and it was defined equal to what was passed in fixReFSC');
    options.fixReQSS = options.fixReFSC;
    options = rmfield(options,'fixReFSC');
end

if isfield(options,'dimXFSC')
    warning('options.dimXFSC has been deprecated, since it actually referred to QSS (quasi-self-similarity) rather than FSC (Falkner-Skan-Cooke). The new option is called ''dimXQSS'' and it was defined equal to what was passed in dimXFSC');
    options.dimXQSS = options.dimXFSC;
    options = rmfield(options,'dimXFSC');
end

if isfield(options,'bInviscidSolver') && options.bInviscidSolver
    warning('options.bInviscidSolver has been deprecated. It is now unified into inviscidFlowfieldTreatment. The functionalities previously requiring options.bInviscidSolver=true, are now obtained with options.inviscidFlowfieldTreatment = ''1DNonEquilibriumEuler''.');
    options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';
    options = rmfield(options,'bInviscidSolver');
end

if isfield(options,'runAirfoil') && options.runAirfoil
    warning('options.runAirfoil has been deprecated. It is now unified into inviscidFlowfieldTreatment. The functionalities previously requiring options.runAirfoil=true, are now obtained with options.inviscidFlowfieldTreatment = ''airfoil''.');
    options.inviscidFlowfieldTreatment = 'airfoil';
    options = rmfield(options,'runAirfoil');
end

% Checking mixtures
if isfield(options,'mixture')
    switch options.mixture
        case 'oxygen2mutation'
            warning('The chosen mixture ''oxygen2mutation'' is a deprecated name. It is now called oxygen2Park01. Mixture name renamed.');
            options.mixture = 'oxygen2Park01';
        case 'air5mutation'
            warning('The chosen mixture ''air5mutation'' is a deprecated name. It is now called air5Park01. Mixture name renamed.');
            options.mixture = 'air5Park01';
        case 'air11mutation'
            warning('The chosen mixture ''air11mutation'' is a deprecated name. It is now called air11Park01. Mixture name renamed.');
            options.mixture = 'air11Park01';
        case 'air11Park91'
            warning('The chosen mixture ''air11Park91'' is a deprecated name. It is now called air11Park93. Mixture name renamed.');
            options.mixture = 'air11Park93';
    end
end
            

end % assess_inputs
