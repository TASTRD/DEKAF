function DEKAFname = get_DEKAFMatName(flow,mixture,modelTransport,M,wedge_angle,thermalBC,G_bc,N_eta,varargin)
% get_DEKAFMatName returns a string with the name to the DEKAF mat file
% depending on the flow conditions and models.
%
% Usage:
%   (1)
%       DEKAFname = get_DEKAFMatName(flow,mixture,modelTransport,M,...
%                                             wedge_angle,thermalBC,G_bc,N)
%
%   (2)
%       DEKAFname = get_DEKAFMatName(...,additionalInfo)
%
%   (3)
%       DEKAFname = get_DEKAFMatName(...,'noFolder')
%       |--> does not save in a specific folder with the flow assumption
%       name. E.g.: 'DEKAF_TPG_blabla.mat' instead of 'TPG/DEKAF_TPG_blabla.mat'
%
%   (4)
%       DEKAFname = get_DEKAFMatName(...,'fullMixture')
%       |--> uses the full mixture name, instead of the abbreviation
%
% Inputs and outputs:
%   flow            string identifying the flow assumption (CPG/TPG/etc)
%   mixture         string identifying the mixture (air5mutation/air11Park93/etc)
%   modelTransport  string identifying the transport model
%   M               pre-shock Mach number
%   wedge_angle     wedge angle
%   thermalBC       string identifying the thermal wall BC used (adiab/Twall/etc)
%   G_bc            value of the thermal boundary value (T/Heat flux/etc)
%   N_eta           number of wall-normal points
%   additionalInfo  string containing whatever additional identifier may be
%                   necessary
%
% Author: Fernando Miro Miro
% Date: June 2018

[varargin,noFolder] = find_flagAndRemove('noFolder',varargin);
[varargin,fullMixture] = find_flagAndRemove('fullMixture',varargin);
[varargin,~,idx] = find_flagAndRemove('options',varargin);
if isempty(idx)
    coordsysStr = 'wedge';
else
    options = varargin{idx};
    varargin(idx) = [];
    if strcmp(options.coordsys,'cartesian2D')
        coordsysStr = 'wedge';
    else
        coordsysStr = options.coordsys;
    end
end

if isempty(varargin)
    additionalInfo = '';
else
    additionalInfo = ['_',varargin{1}];
end

if fullMixture
    mixtureStr = {mixture};
else
    mixtureStr = regexp(mixture,'^([a-z]|[A-Z]|\-|\_)+[0-9]*','match');
end

if noFolder
    folder = '';
else
    folder = [flow,'/'];
end

DEKAFname = ['mat_DEKAF/',folder,'DEKAF_',flow,'_',mixtureStr{1},'_',...
            strjoin(regexp(modelTransport,'_','split'),''),'_M',num2str(M),'_',coordsysStr,...
            num2str(wedge_angle),'_',thermalBC,num2str(G_bc),'_Neta',num2str(N_eta),additionalInfo,'.mat'];