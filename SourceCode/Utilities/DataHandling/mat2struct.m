function struct_out = mat2struct(mat_in,fld_names,varargin)
% mat2struct converts a matrix into a structure where all the columns of
% the original matrix are put into separate fields of the structure.
%
% Usage:
%   (1)
%       struct_out = mat2struct(mat_in,fld_names)
%
%   (2)
%       struct_out = mat2struct(mat_in,fld_names,struct_in)
%       |--> appends the fields coming from the matrix onto an existing
%       structure.
%
% See also: struct2mat
%
% Author: Fernando Miro Miro
% Date: June 2019
% GNU Lesser General Public License 3.0

if isempty(varargin);       struct_in = struct;
else;                       struct_in = varargin{1};
end

flds_in             = fieldnames(struct_in);                    % fields in the original structure
flds2overwrite      = flds_in(ismember(flds_in,fld_names));     % fields that need be overwritten (both in struct_in and fld_names)
flds2keep           = flds_in(~ismember(flds_in,fld_names));    % fields that we want to keep and append to mat_in
struct_in           = rmfield(struct_in,flds2overwrite);        % removing fields that must be overwritten from struct_in
allflds             = [fld_names(:);flds2keep(:)].';            % all the fields that will be in the final structure
cell_fromStructIn   = struct2cell(struct_in).';                 % making a cell out of the input structure
[N1,N2]             = size(mat_in);                             % size of matrix
cell_fromMat        = mat2cell(mat_in,N1,ones([1,N2]));         % rearranging from matrix to cell
struct_out          = cell2struct([cell_fromMat,cell_fromStructIn],allflds,2); % making into structure

end % mat2struct
