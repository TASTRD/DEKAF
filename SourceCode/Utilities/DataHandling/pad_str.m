function str_out = pad_str(str_in,N,varargin)
% PAD adds trailing or leading blanks to a string to make it of a specified
% length
%
%   Examples:
%       (1)     str = 'aaa'
%               pad_str(str,10)
%               'aaa       '
%
%       (2)     str = 'aaa'
%               pad_str(str,10,'right')
%               'aaa       '
%
%       (3)     str = 'aaa'
%               pad_str(str,10,'left')
%               '       aaa'
%
%       (4)     str = 'aaa'
%               pad_str(str,10,'left','pad_strWith','-')
%               '-------aaa'
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[filling,varargin] = parse_optional_input(varargin,'pad_strWith',' ');

if isempty(varargin)
    flag = 'right';
else
    flag = varargin{1};
end

N_in = length(str_in);                          % number of characters in the incoming string
if N_in>N                                       % if the input string is already larger than N, we simply output it as it is
    str_out = str_in;
else                                            % otherwise
    str_out = repmat(filling(1),[1,N]);                 % allocate outward string with blanks
    switch flag
        case 'left'                                     % if we want leading blanks
            str_out(end-N_in+1:end) = str_in;
        case 'right'                                    % if we want trailing blanks
            str_out(1:N_in) = str_in;
        otherwise                                       % otherwise break
            error(['the input flag ''',flag,''' is not supported']);
    end
end
