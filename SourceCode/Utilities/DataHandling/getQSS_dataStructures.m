function [intelQSS,optionsQSS] = getQSS_dataStructures(intel,options)
%getQSS_dataStructures Properly construct the self-similar intel struct by
% manipulating the edge quantities in intel
%
% Usage:
%   (1)
%       [intelQSS,optionsQSS] = getQSS_dataStructures(intel,options)
%
% Essentially it:
%   1st) modifies the options that are differernt between the QSS and BL
%   solvers:
%       .dimoutput ----> false
%       .marchYesNo ---> false
%   2nd) reduces the size of BL-edge vectors from N_x to 1
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
% GNU Lesser General Public License 3.0

optionsQSS = options;
optionsQSS.dimoutput = false;                   % we don't want to evaluate the dimensional profiles
optionsQSS.marchYesNo = false;                  % there is obviously no marching for QSS

N_x = options.mapOpts.N_x;

flds_all = fieldnames(intel);                                               % may have *_e0, or *_e fields
for ii=1:length(flds_all)                       % looping all fields in intel
    if isnumeric(intel.(flds_all{ii}))              % numerical value
        if length(intel.(flds_all{ii}))==N_x && ~ismember(flds_all{ii},{'eta','D1','D2'})
            intelQSS.(flds_all{ii}) = intel.(flds_all{ii})(1);  % take first
        else
            intelQSS.(flds_all{ii}) = intel.(flds_all{ii});     % take all
        end
    elseif iscell(intel.(flds_all{ii}))             % cell
        Ncll = length(intel.(flds_all{ii}));            % number of cell positions
        for ic=Ncll:-1:1                                % looping cell positions
            if length(intel.(flds_all{ii}){ic})==N_x;   intelQSS.(flds_all{ii}){ic} = intel.(flds_all{ii}){ic}(1);  % take first
            else;                                       intelQSS.(flds_all{ii}){ic} = intel.(flds_all{ii}){ic};     % take all
            end
        end
    else                                            % whatever else
        intelQSS.(flds_all{ii}) = intel.(flds_all{ii}); % take all
    end
end

end % getQSS_dataStructures