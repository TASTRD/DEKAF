function mat_out = struct2mat(struct_in,varargin)
% struct2mat converts a structure into a matrix where all the fields of the
% original structure are stacked in columns.
%
% Usage:
%   (1)
%       mat_out = struct2mat(struct_in)
%
%   (2)
%       mat_out = struct2mat(struct_in,flds)
%       |--> allows to pass a list with the fields of the structure to
%       include in the matrix
%
% See also: mat2struct
%
% Author: Fernando Miro Miro
% Date: June 2019
% GNU Lesser General Public License 3.0

if isempty(varargin);       flds = fieldnames(struct_in);
else;                       flds = varargin{1};
end

flds_all = fieldnames(struct_in);                               % all fields in struct_in
flds2rm = flds_all(~ismember(flds_all,flds));                   % removing the fields that are not in flds
struct_red = rmfield(struct_in,flds2rm);                        % creating a reduced structure without these fields
mat_out = cell2mat(transpose(struct2cell(struct_red)));         % creating matrix

end % struct2mat