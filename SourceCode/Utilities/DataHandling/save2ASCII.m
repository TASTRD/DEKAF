function [] = save2ASCII(path,intel,varargin)
% save2ASCII writes a series of fields of the intel structure into separate
% files.
%
% Usage:
%   (1)
%       save2ASCII(path,intel)
%       |--> saves everything inside intel into separate files in a folder
%       in path.
%       IMPORTANT! Path to a folder must finish with a /
%
%   (2)
%       save2ASCII(...,'varlist',{var1,var2})
%       |--> saves a specific series of variables (identified with the
%       strings in var1, var2, ...) inside intel into separate files in a
%       folder in path.
%
%   (3)
%       save2ASCII(...,'prefix',prefix)
%       |--> allows to pass a prefix for the name of the file to be saved.
%       For instance if prefix='Var_', files will be named 'Var_u',
%       'Var_T', etc instead of 'u', 'T', etc.
%
%   (4)
%       save2ASCII(...,'passGitInfo',gitInfo)
%       |--> allows to pass the gitInfo structure, instead of obtaining it
%       from intel
%
% Author: Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

% Checking options
[varargin,~,idx] = find_flagAndRemove('varlist',varargin);
if isempty(idx);    fldnms = fieldnames(intel);                             % default list of fields to save (all)
else;               fldnms = varargin{idx};                                 % getting additional input
end
[varargin,~,idx] = find_flagAndRemove('prefix',varargin);
if isempty(idx);    prefix = '';                                            % default variable-name prefix
else;               prefix = varargin{idx};                                 % getting additional input
end
[varargin,~,idx] = find_flagAndRemove('passGitInfo',varargin);
if isempty(idx);    gitInfo = intel.gitInfo;                                % default gitInfo structure
else;               gitInfo = varargin{idx};                                % getting additional input
end

% Preparing strings and paths
str_git = ['Solution obtained with DEKAF, on the ',gitInfo.branch,' branch, SHA=',gitInfo.hash,', with matlab ',gitInfo.matlabVersion,' on an operating system ',gitInfo.OS,' working on a processor ',gitInfo.processor];

if ~exist(path,'dir') % creating folder if necessary
    mkdir(path);
end

% Writing
for ii = 1:length(fldnms)                                                   % looping field names
    path_ii = [path,'/',prefix,fldnms{ii}];                                     % building full file path
    data = intel.(fldnms{ii});                                                  % extracting data from intel
    if isstruct(data)                                                           % the requested field is a structure
        save2ASCII(path_ii,data,'prefix',[fldnms{ii},'_'],'passGitInfo',gitInfo);   % recursive call
    elseif isnumeric(data) || islogical(data)                                   % the requested field is numeric or logical
        if ndims(data)>3                                                              % more than 3D is not supported
            error(['field ''',fldnms{ii},''' has more than three dimensions, which is not supported']);
        else
            [~,Nx,Nz] = size(data);                                                     % size of the field to write
            for iz=1:Nz                                                                 % loooping third dimension
                if Nz==1                                                                    % non-3D variables
                    fID = fopen([path_ii,'.dat'],'w');                                          % opening file for writing for non-3D variables
                    data_iz = data;                                                             % data to write for non-3D variables
                    fprintf(fID,['## Variable: ',prefix,fldnms{ii},'\n',str_git,'\n']);      % header
                else                                                                        % 3D variables
                    fID = fopen([path_ii,'_3DsliceN',num2str(iz),'.dat'],'w');                  % opening file for writing for
                    data_iz = data(:,:,iz);                                                     % data to write for 3D variables
                    fprintf(fID,['## Variable: ',prefix,fldnms{ii},'(:,:,',num2str(iz),')\n',str_git,'\n']); % header
                end
                fprintf(fID,[repmat('%0.16e \t',[1,Nx]),'\n'],full(data_iz.'));             % writing data
                fclose(fID);                                                                % closing file
            end
        end
    elseif ischar(data)                                                         % the requested data is a character
        fID = fopen([path_ii,'.dat'],'w');                                          % opening file for writing
        fprintf(fID,['## Variable: ',prefix,fldnms{ii},'\n',str_git,'\n']);      % header
        fprintf(fID,'%s \n',data);                                                  % writing data
        fclose(fID);                                                                % closing file
    elseif iscell(data)                                                         % the requested field is a cell
        if ~isempty(data) && iscell(data{1})                                        % for embedded cells -- break
            warning(['File ',prefix,fldnms{ii},' was not generated because it corresponds to an embedded cell (cell in a cell), which is not supported']);
        else                                                                        % if not embedded
            Ncll = length(data(:));                                                     % number of cell positions
            if ~isempty(data) && isnumeric(data{1})                                                       % cell of numerical data (separate files for each cell position)
                for jc=1:Ncll                                                               % looping cell positions
                    [~,Nx,Nz] = size(data{jc});                                                 % size of the field to write
                    fID = fopen([path_ii,num2str(jc),'.dat'],'w');                              % opening file for writing
                    fprintf(fID,['## Variable: ',prefix,fldnms{ii},'{',num2str(jc),'}\n',str_git,'\n']);% header
                    if Nz>1                                                                     % more than 2-D not supported
                        error(['entry ',num2str(jc),' in the cell in field ''',fldnms{ii},''' has more than two dimensions, which is not supported']);
                    else                                                                        % 1-D or 2-D is fine
                        fprintf(fID,[repmat('%0.16e \t',[1,Nx]),'\n'],full(data{jc}.'));             % writing data
                    end
                    fclose(fID);                                                                % closing file
                end
            elseif isempty(data) || ischar(data{1})                                                      % cell of characters
                fID = fopen([path_ii,'.dat'],'w');                                          % opening file for writing
                fprintf(fID,['## Variable: ',prefix,fldnms{ii},'\n',str_git,'\n']);         % header
                for jc=1:Ncll                                                               % looping cell positions
                    fprintf(fID,'%s \n',data{jc});                                              % writing data
                end
                fclose(fID);                                                                % closing file
            end
        end
    else
        warning(['Field ''',fldnms{ii},''' in intel was ignored, since it is neither a structure, nor numeric, nor a string, nor a cell.']);
    end
end

end % save2ASCII