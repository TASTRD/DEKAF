function [out_list] = makeList(str,varargin)
% makeList(str,i_0,i_end) makes a list of N_i = i_end-i_0+1 strings
% containing str+'#', where # is a number ranging from i_0 to i_end.
%
% makeList(str,strRepi,i_0,i_end) allows to specify a string strRepi in
% str to be replaced by the counter i.
%
% makeList(str,i_0,i_end,j_0,j_end) makes a list of N_i x N_j strings
% containing str+'#o#', where the first # is a number ranging from i_0 to
% i_end, and the second # is a number ranging from j_0 to j_end.
%
% makeList(str,strRepi,i_0,i_end,strRepj,j_0,j_end) allows to specify two
% strings strRepi and strRepj in str to be replaced by the counters i and
% j.
%
% makeList(str,strRepi,i_0,i_end,strRepj,j_0,j_end,strRepk,k_0,k_end)
% allows to specify three strings strRepi, strRepj and strRekj in str to be
% replaced by the counters i, j and k.
%
% makeList(...,'di',di) allows to fix the spacing di for the i counter
%
% makeList(...,'dj',dj) allows to fix the spacing dj for the j counter
%
% makeList(...,'dk',dk) allows to fix the spacing dk for the k counter
%
% Examples:
%
%   (1)
%       makeList('D_s',1,3)
%           ans =
%               {'D_s1','D_s2','D_s3'}
%
%   (2)
%       makeList('D_s[#]','#',1,3)
%           ans =
%               {'D_s[1]','D_s[2]','D_s[3]'}
%   (3)
%       makeList('D_sl',1,2,1,2)
%           ans =
%               {'D_sl1o1','D_sl1o2','D_sl2o1','D_sl2o2'}
%
%   (4)
%       makeList('D_sl[#,$]','#',1,2,'$',1,3)
%           ans =
%               {'D_sl[1,1]','D_sl[1,2]','D_sl[1,3]','D_sl[2,1]','D_sl[2,2]','D_sl[2,3]'}
%
%   (5)
%       makeList('D_sl[#,$]','#',1,2,'$',10,30,'dj',10)
%           ans =
%               {'D_sl[1,10]','D_sl[1,20]','D_sl[1,30]','D_sl[2,10]','D_sl[2,20]','D_sl[2,30]'}
%
%   (6)
%       makeList('D_sl[#,$,@]','#',1,2,'$',1,3,'@',1,2)
%           ans =
%               {'D_sl[1,1,1]', 'D_sl[1,1,2]', 'D_sl[1,2,1]', 'D_sl[1,2,2]', 'D_sl[1,3,1]', 'D_sl[1,3,2]', ...
%                'D_sl[2,1,1]', 'D_sl[2,1,2]', 'D_sl[2,2,1]', 'D_sl[2,2,2]', 'D_sl[2,3,1]', 'D_sl[2,3,2]'}
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[di,varargin] = parse_optional_input(varargin,'di',1);
[dj,varargin] = parse_optional_input(varargin,'dj',1);
[dk,varargin] = parse_optional_input(varargin,'dk',1);

out_list = {};
switch length(varargin)
    case 2 % simple 1-D case
        i_0     = varargin{1};
        i_end   = varargin{2};
        for i=i_0:di:i_end
            out_list = [out_list,[str,num2str(i)]];
        end
    case 3 % 1-D case with indices to substitute
        strRepi = varargin{1};
        i_0     = varargin{2};
        i_end   = varargin{3};
        for i=i_0:di:i_end
            out_list = [out_list,strrep(str,strRepi,num2str(i))];
        end
    case 4 % simple 2-D case
        i_0     = varargin{1};
        i_end   = varargin{2};
        j_0     = varargin{3};
        j_end   = varargin{4};
        for i=i_0:di:i_end
            for j=j_0:dj:j_end
                out_list = [out_list,[str,num2str(i),'o',num2str(j)]];
            end
        end
    case 6 % 2-D case with indices to substitute
        strRepi = varargin{1};
        i_0     = varargin{2};
        i_end   = varargin{3};
        strRepj = varargin{4};
        j_0     = varargin{5};
        j_end   = varargin{6};
        for i=i_0:di:i_end
            for j=j_0:dj:j_end
                out_list = [out_list,strrep(strrep(str,strRepi,num2str(i)),strRepj,num2str(j))]; %#ok<*AGROW>
            end
        end
    case 9 % 3-D casee with indices to substitute
        strRepi = varargin{1};
        i_0     = varargin{2};
        i_end   = varargin{3};
        strRepj = varargin{4};
        j_0     = varargin{5};
        j_end   = varargin{6};
        strRepk = varargin{7};
        k_0     = varargin{8};
        k_end   = varargin{9};
        for i=i_0:di:i_end
            for j=j_0:dj:j_end
                for k=k_0:dk:k_end
                    out_list = [out_list,strrep(strrep(strrep(str,strRepi,num2str(i)),strRepj,num2str(j)),strRepk,num2str(k))]; %#ok<*AGROW>
                end
            end
        end
    otherwise
        error('wrong number of inputs');
end