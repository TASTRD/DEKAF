function matlabVersion = getMatlabVersion()
% getMatlabVersion returns a string with the current matlab version.
%
% Usage:
%   (1)
%       matlabVersion = getMatlabVersion()
%
% See also: getSystemInfo, getOSInfo, cpuinfo
%
% Author: Fernando Miro Miro
% Date: October 2019
%
% GNU Lesser General Public License 3.0

verInfo = ver;                                      % version information of all packages
releases = unique({verInfo.Release});               % unique list of releases
releases = regexprep(releases,'^\((.*)\)$','$1');   % removing the surrounding brackets

if length(releases)>1                               % there is more than one release
    error(['more than one matlab release was found when executing ''ver'': ',newline,'  - ',strjoin(releases,[newline,'  - '])]);
else                                                % there is a unique release
    matlabVersion = releases{1};
end

end % getMatlabVersion