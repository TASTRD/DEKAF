function txtOut = regexprep_nested(txtIn,expIn,replaceIn,varargin)
% regexprep_nested evaluates a regular-expression substitution, similarly
% to regexprep, but respecting the nested curly brackets {}
%
% Usage: the same as regexprep
%
% IMPORTANT! wild-card arguments must include the non-greedy identifier,
% otherwise it will make a mess.
%
% The function works recursively, calling itself for each nested curly
% bracket instance
%
% Examples:
%   (1)
%       txtIn = 'This \new{test \new{yes \old{also yes} even this} without avoiding} anything';
%       expIn = '\\new\{(.*?)\}';
%       replaceIn = '$1';
%       regexprep_nested(txtIn,expIn,replaceIn)
%       ans =
%           'This test yes \old{also yes} even this without avoiding anything'
%       regexprep(txtIn,expIn,replaceIn)
%       ans =
%           'This test \new{yes \old{also yes even this} without avoiding} anything'
%
% Author: Fernando Miro Miro
% Date: August 2019


[varargin,~,idxRec] = find_flagAndRemove('recursLevel',varargin);
if isempty(idxRec);     recLvl = 0;                                         % initial recursive level (0)
else;                   recLvl = varargin{idxRec};      varargin(idxRec) = [];
end

lvl = getLvl(txtIn);                                                        % obtaining levels
lvlJump = [diff(lvl);0];                                                    % level jumps
idx_lvl0Jump = find((lvl==0 & lvlJump==1)|(lvl==0 & [0;lvlJump(1:end-1)]==-1)); % indices of the locations where the index jumps from 0 to 1 or from 1 to 0
Njumps = length(idx_lvl0Jump)/2;                                            % number of jumps
for ij=Njumps:-1:1                                                          % looping jumps
    idx_jump_ij = idx_lvl0Jump(2*(ij-1)+1):idx_lvl0Jump(2*ij);                  % positions limitting the jump
    txtReplace{ij} = regexprep_nested(txtIn(idx_jump_ij(2:end-1)),expIn,replaceIn,varargin{:},'recursLevel',recLvl+1); % recursive call to the "jumping" part of the string
    txtIn = [txtIn(1:idx_jump_ij(1)) , '##jump#',num2str(ij),'#' , txtIn(idx_jump_ij(end):end)]; % replacing jump for '##jump#123#
end

txt0 = regexprep(txtIn,expIn,replaceIn,varargin{:});                        % replacing after having removed the nested content by ##jump#123#
for ij=Njumps:-1:1                                                          % looping jumps
    txt0 = strrep(txt0 , ['##jump#',num2str(ij),'#'] , txtReplace{ij});         % replacing back the recursive calls' results
end
txtOut = txt0;

end % regexprep_nested
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function lvl = getLvl(txtIn)
% getLvl returns the bracket {} level at each string of a string.
%
% Usage:
%   (1)
%       lvl = getLvl(txtIn)
%
% Function embedded to regexprep_nested
%
% Author: Fernando Miro Miro
% Date: August 2019

Nchar = length(txtIn);                  % number of characters
idxOpen  = regexp(txtIn,'\{','start');  % parenthesis openings
idxClose = regexp(txtIn,'\}','start');  % parenthesis closings

lvl = zeros(Nchar,1);                   % initializing level vector
for ii=1:length(idxOpen)                % looping openings/closings
    idx2sum  = idxOpen(ii)+1:Nchar;         % which characters increase their recursion level
    idx2subt = idxClose(ii) :Nchar;         % which characters decrease their recursion level
    lvl(idx2sum)  = lvl(idx2sum) +1;        % increasing level
    lvl(idx2subt) = lvl(idx2subt)-1;        % decreasing level
end

end % getLvl