function OSInfo = getOSInfo()
% getOSInfo returns a string with the current operative system information.
%
% Usage:
%   (1)
%       OSInfo = getOSInfo()
%
% See also: getSystemInfo, getMatlabVersion, cpuinfo
%
% Author: Fernando Miro Miro
% Date: October 2019
%
% GNU Lesser General Public License 3.0

if ispc                                                                         % windows
    [~,releaseInfo] = system('systeminfo');                                         % obtaining the full operative system information
    OSInfo = regexprep(releaseInfo,'^(.*)OS\ Name\:\ *([^\n]*)\n(.*)$','$2');       % keeping everything between 'OS Name: ...'
    % regexp clarification - [^\n]* identifies a string of characters not
    % containing a line break (\n). This allows us to ensure that we break
    % the match at the first \n
elseif ismac                                                                    % mac
    error('this function is not ready for mac');
elseif isunix                                                                   % linux
    releaseInfo = fileread('/etc/os-release');                                      % obtaining the full operative system information
    OSInfo = regexprep(releaseInfo,'^(.*)PRETTY\_NAME\=\"([^\"]*)\"(.*)$','$2');    % keeping everything between 'PRETTY_NAME="..."'
    % regexp clarification - [^\"]* identifies a string of characters not
    % containing ". This allows us to ensure that we find the first "" pair
else                                                                            % other - not supported
    error('the operating system is neither windows, nor mac, nor linux - not supported');
end

end % getOSInfo