function [msg, caught_msg, err_msg] = parse_exception(ME)
%PARSE_EXCEPTION Given an exception from a try/catch block, parse this
% MException object into a simple message for the user/developer.
%
% Usage:
%    msg = parse_exception(ME);
%    [msg, caught_msg, err_msg] = parse_exception(ME);
%
% In:
%     ME: MException object
%
% Out:
%    msg: Total message relaying all relevant information about ME
%    caught_msg: substring of msg detailing the stack and line information
%    err_msg: substring of msg detailing the exact error message
%
% Example:
%     try
%         nonexistant_function();
%     catch ME
%         warning(parse_exception(ME));
%     end
%
% Author: Ethan Beyak
% Date: August 2019
% GNU Lesser General Public License 3.0

caught_msg = 'Error caught in ';
leftspace = repmat(' ',1,length(['Warning: ', caught_msg]));
caught_msg = [caught_msg, ME.stack(1).name, ':', num2str(ME.stack(1).line), '\n'];

Nstacks = length(ME.stack);
for ii = 2:Nstacks
    caught_msg = [caught_msg, leftspace, ...
        ME.stack(ii).name, ':', num2str(ME.stack(ii).line), '\n']; %#ok<AGROW>
end

err_msg = ['Error message is ', ME.message];

msg = [caught_msg, err_msg];

end % parse_exception
