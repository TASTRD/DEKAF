function options = populateUserFixedOptions(options,additionalOptions)
% populateUserFixedOptions populates the options structure with whatever
% options the user chose to fix.
%
% Usage:
%   (1)
%       options = populateUserFixedOptions(options,{'opt1',val1,'opt2',val2,...})
%
% Example:
%   For instance, if we want to fix options.N=200 and options.L=20:
%       options = populateUserFixedOptions(options,{'N',200,'L',20})
%
% See also: DEKAF_nozzleViscousCorrection
%
% Author: Fernando Miro Miro
% Date: March 2020
% GNU Lesser General Public License 3.0

if length(additionalOptions)/2 ~= round(length(additionalOptions)/2)
    error('The length of the cell with the additionalOptions must be an even number (see documentation).');
end

for ii=1:length(additionalOptions)/2                        % looping option name - option value pairs
    ii1 = 2*(ii-1)+1;                                           % first element of each pair
    ii2 = 2*(ii-1)+2;                                           % second element of each pair
    nameParts = regexp(additionalOptions{ii1},'\.','split');    % splitting at the points, in case there are embedded structures
    switch length(nameParts)                                    % different assignments depending on the number of embedded structures
        case 1;     options.(nameParts{1})                                                              = additionalOptions{ii2};
        case 2;     options.(nameParts{1}).(nameParts{2})                                               = additionalOptions{ii2};
        case 3;     options.(nameParts{1}).(nameParts{2}).(nameParts{3})                                = additionalOptions{ii2};
        case 4;     options.(nameParts{1}).(nameParts{2}).(nameParts{3}).(nameParts{4})                 = additionalOptions{ii2};
        case 5;     options.(nameParts{1}).(nameParts{2}).(nameParts{3}).(nameParts{4}).(nameParts{5})  = additionalOptions{ii2};
        otherwise;  error(['The number of embedded structures (',num2str(length(nameParts)),') is not supported']);
    end
end


end % populateUserFixedOptions