function intel = interpolateAllVariables(x1,x2,vars2interp,meth_edge_interp,intel)
% interpolateAllVariables interpolates all variables inside a structure,
% and populate intel with them.
%
% Usage:
%   (1)
%       intel = interpolateAllVariables(x1,x2,vars2interp,meth_edge_interp,intel)
%
% Inputs and outputs:
%   x1
%       original x vector, paired with all the fields in vars2interp
%   x2
%       goal x vector, onto which the fields must be interpolated
%   vars2interp
%       structure with the various fields to interpolate from x1 to x2
%   meth_edge_interp
%       method to be used in the interpolation (all those in matlab's
%       interp1 function are accepted.
%   intel
%       structure that will be populated with the fields in vars2interp,
%       after interpolating from x1 to x2.
%
% Author: Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

fldnms = fieldnames(vars2interp);                                           % fields to be interpolated
for ii=1:length(fldnms)                                                     % looping fields to be interpolated
    if iscell(vars2interp.(fldnms{ii}))                                         % the field is a cell
        intel.(fldnms{ii}) = cell(size(vars2interp.(fldnms{ii})));                  % allocating cell
        for s=length(vars2interp.(fldnms{ii})(:)):-1:1                              % looping cell positions
            intel.(fldnms{ii}){s} = interp1OrRepmat(x1,vars2interp.(fldnms{ii}){s},x2,meth_edge_interp); % interpolating
        end
    else                                                                        % the field is not a cell
        intel.(fldnms{ii}) = interp1OrRepmat(x1,vars2interp.(fldnms{ii}),x2,meth_edge_interp); % interpolating
    end
end


end % interpolateAllVariables