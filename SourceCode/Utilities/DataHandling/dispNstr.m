function Nstr = dispNstr()
% dispNstr returns the number of strings to be used in all DEKAF messages
%
% Usage:
%   (1)
%       Nstr = dispNstr()
%
% Author: Fernando Miro Miro
% Date: September 2019
% GNU Lesser General Public License 3.0

% 74 is chosen because it is the length of the original DEKAF mug ASCII art
Nstr = 74;

end % dispNstr