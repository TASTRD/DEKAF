function [] = authorshipCheking(varargin)
% authorshipChecking looks through the various matlab functions and
% identifies the author(s) of all of them, providing development weights
% for each of them.
%
% Usage:
%   (1)
%       authorshipCheking()
%       |---> Performs the check on the SourceCode/ folder, ignoring
%       SourceCode/External, and dwith the default list of authors.
%
%   (2)
%       authorshipCheking('extraAuthors',authors_cell)
%       |---> alows to pass a cell of strings with the names of the various
%       authors to be added onto the default lis of authors.
%
%   (3)
%       authorshipCheking('ignore',pathIgnore_cell)
%       |---> alows to pass a cell of strings with the paths to be ignored
%       in the function search. By default it only ignores
%       'SourceCode/External'.
%
%   (4)
%       authorshipCheking('dispMax',dispMax)
%       |---> alows to customize the number of files with most lines that
%       are displayed (by default 15)
%
% By default, the list of authors checked is:
%   - Miro Miro
%   - Beyak
%   - Groot
%   - Moyes
% Additional authors can be considered through usage 2.
%
% Author: Fernando Miro Miro
% Date: December 2019
% GNU Lesser General Public License 3.0

% Determining root path
fullPath = which_unique('DEKAF.m');                  % full path to the location of the main DEKAF function
pathParts = regexp(fullPath,'\/','split');          % splitting folders
rootPath = strjoin(pathParts(1:end-1),'/');         % path to the root SourceCode folder

% Checking optional inputs
[extraAuthors,      varargin] = parse_optional_input(varargin,'extraAuthors',{});
[pathIgnore_cell,   varargin] = parse_optional_input(varargin,'ignore',{[rootPath,'/External/']});
[dispMax,           ~       ] = parse_optional_input(varargin,'dispMax',15);

% List of authors
authors_list = {'Miro Miro',...
                'Beyak',...
                'Groot',...
                'Moyes'};
authors_list = [authors_list,extraAuthors];                                 % appending extra authors fixed by the user
authors_4regexp = cellfun(@str2regexp,authors_list,'UniformOutput',false);  % transforming names to regexp-friendly form

% Obtaining full lists of files
list_files = what_recursive(rootPath);                                      % all files
for ii=1:length(pathIgnore_cell)                                            % looping paths to ignore
    idx2rm = ~cellfun(@isempty,regexp(list_files,['^',str2regexp(pathIgnore_cell{ii})],'once')); % locating instances starting with the paths to ignore
    list_files(idx2rm) = [];                                                    % removing them from the list
end
idx_mFiles = ~cellfun(@isempty,regexp(list_files,'\.m$','once'));           % locating all m files
list_mFiles = list_files(idx_mFiles);                                       % keeping only the m files

% looping files and determining their length and author(s)
N_auths = length(authors_list);                                             % number of authors
N_files = length(list_mFiles);                                              % number of lines
author_mat = zeros(N_files,N_auths);                                        % allocating author matrix (who authors what)
L_files = zeros(N_files,1);                                                 % allocatinig number of lines per file
Lc_files =  L_files;                                                        % allocating number of lines of code per file
for iF=1:N_files                                                            % looping files
    fID = fopen(list_mFiles{iF},'r');                                           % opening file
    allText_iF = textscan(fID,'%s','delimiter','\n');                           % reading full text
    fclose(fID);                                                                % closing file
    idx_comment = ~cellfun(@isempty,regexp(allText_iF{1},'^\%','once'));        % lines starting with a comment
    idx_blank = cellfun(@isempty,allText_iF{1});                                % blank lines
    L_files(iF) = length(allText_iF{1});                                        % storing number of lines
    Lc_files(iF) = L_files(iF) - nnz(idx_comment) - nnz(idx_blank);             % storing number of code lines (without commments and blanks)
    idx_lAuthor = find(~cellfun(@isempty,regexp(allText_iF{1},'^\%\ *Author(\(s\))?\ *\:','once'))); % locating the "Author" line
    N_lAuthor = length(idx_lAuthor);                                            % number of author lines
    if N_lAuthor==0                                                             % no information on who created it
    else                                                                        % one or more author lines
        for iA=1:N_lAuthor                                                          % looping author lines
            bAuthor = ~cellfun(@(cll)isempty(regexp(allText_iF{1}{idx_lAuthor(iA)},cll,'once')),authors_4regexp); % finding authors that appear in the author line
            author_mat(iF,:) = author_mat(iF,:) + bAuthor;                              % increasing matrix count
            it=0;                                                                       % initializing while variable
            while nnz(bAuthor)~=0                                                       % as long as we find authors
                it=it+1;                                                                    % increasing count
                bAuthor = ~cellfun(@(cll)isempty(regexp(allText_iF{1}{idx_lAuthor(iA)+it},cll,'once')),authors_4regexp); % finding authors that appear in the next line
                author_mat(iF,:) = author_mat(iF,:) + bAuthor;                              % increasing matrix count
            end
        end
    end
end

noAuthor_vec = all(author_mat==0,2);                                        % author-less functions

% Computing statistics
N_coauthors = sum(author_mat,2);                                            % number of coauthors per file
N_coauthors(N_coauthors==0) = 1;                                            % ensuring that there are no 0/0 situations
authorship_frac = author_mat./repmat(N_coauthors,[1,N_auths]);              % authorship fraction for each author/file
authorship_frac = [authorship_frac,noAuthor_vec];                           % appending column for "null author"
author_lines  = (authorship_frac .* repmat(L_files, [1,N_auths+1])).' * ones(N_files,1); % number of lines per author
author_linesC = (authorship_frac .* repmat(Lc_files,[1,N_auths+1])).' * ones(N_files,1); % number of lines of code per author
fraction_lines  = 100* author_lines /sum(author_lines);                     % fraction of code by each author
fraction_linesC = 100* author_linesC/sum(author_linesC);                    % fraction of code by each author
[~,idx_longestFile] = sort(L_files,'descend');                              % sorting the list of files from longest to shortest

% Writing summary:
authors_list = [authors_list,{'Not identified'}];
disp('-------------------------------------------------------------------');
disp('--------------------- DEKAF Authorship summary --------------------');
disp(['-------------------- Date: ',datestr(datetime('now')),' -------------------']);
disp('---                                                             ---');
disp('--- Author                   | Lines           Code lines       ---');
disp('-----------------------------+-------------------------------------');
for iA=1:N_auths+1
disp(['--- ',pad_str(authors_list{iA},25),'| ',...
             pad_str(num2str(author_lines(iA) ,'%0.0f'),6,'left'),pad_str(['(',num2str(fraction_lines(iA) ,'%0.2f'),'%)'],9,'left'),' '...
             pad_str(num2str(author_linesC(iA),'%0.0f'),6,'left'),pad_str(['(',num2str(fraction_linesC(iA),'%0.2f'),'%)'],10,'left'),...
             ' ---']);
end
disp('---                                                             ---');
itDisp = 0; iF0=0;                                                          % initializing
disp(['--- ',pad_str(['The ',num2str(dispMax),' non-authored files with the most lines are:'],59),' ---']);
while itDisp<dispMax                                                        % as long as we don't reach the maximum number of displays
    iF0=iF0+1;                                                                  % increasing file counter
    iF = idx_longestFile(iF0);                                                  % putting them in descending order
    if noAuthor_vec(iF)                                                         % if the file had no author
        disp(['--- ',pad_str(num2str(L_files(iF),'%0.0f'),6,'left'),' -> ',list_mFiles{iF}]); % we display it
        itDisp=itDisp+1;                                                        % increasing display counter
    end
end
disp('-------------------------------------------------------------------');


end % authorshipCheking
