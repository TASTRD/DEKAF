function [list_files,list_files_local] = what_recursive(path)
% whatRecursive makes a recursive search within a path of all the
% non-folder contents
%
% Usage:
%   (1)
%       list_files = what_recursive(path)
%       |--> returns a cell of strings with the complete path of the
%       various files
%
%   (2)
%       [list_files,list_files_local] = what_recursive(path)
%       |--> returns also the local file names
%
% Author: Fernando Miro Miro
% Date: November 2019
% GNU Lesser General Public License 3.0

fldrInfo            = dir(path);                                            % content of the folder
list_files_local          = {fldrInfo.name}.';                              % names of the content
bidxDir             = [fldrInfo.isdir].';                                   % positions of directories
bidxDot             = ~cellfun(@isempty,regexp(list_files_local,'^(\.|\.\.)$'));  % positions of . and ..
list_files_local(bidxDot) = [];                                             % removing . and ..
bidxDir(bidxDot)    = [];
idxDir              = find(bidxDir);                                        % numerical values of the directories' positions

list_files = cellfun(@(cll)[path,'/',cll],list_files_local,'UniformOutput',false); % making local path into global path

for ii=length(idxDir):-1:1                                                  % looping folders
    [list_files_ii,list_files_local_ii] = what_recursive(list_files{idxDir(ii)}); % recursive call
    list_files       = [list_files(      1:idxDir(ii)-1) ; list_files_ii       ; list_files(      idxDir(ii)+1:end)]; % extending list
    list_files_local = [list_files_local(1:idxDir(ii)-1) ; list_files_local_ii ; list_files_local(idxDir(ii)+1:end)];
end

end % what_recursive