function [] = saveLight(savepath,intel,options,varargin)
% saveLight saves only the minimum number of variables necessary to fully
% characterize the system:
%   -   state variables (u, T, ys, etc.)
%   -   wall-normal and streamwise directions (y, x, eta, xi)
%   -   spatial derivatives of the previous (du_dy,du_dy2,etc.)
%   -   spatial derivation matrices (D1, D2, D1_y, etc.)
%   -   flow properties like mixCnst
%   -   options structure (in case you need smth)
%
%   Examples:
%       (1)     saveLight(savepath,intel,options)
%
%       (2)     saveLight(savepath,intel,options,extra_vars)
%       |--> saves also all variables included in the list extra_vars
%
%       (3)     saveLight(...,'noDerivs')
%       |--> saves only the zeroth-order derivatives.
%
%       (4)     saveLight(...,'onlyYDerivs')
%       |--> saves only y derivatives (overwritten by noDerivs)
%
%       (5)     saveLight(...,'removeImag')
%       |--> removes imaginary part of the variables (sometimes appears
%       during the iteration process)
%
%       (6)     saveLight(...,'onlyRhos')
%       |--> saves only the species partial densities, and not the mass
%       fractions
%
%       (7)     saveLight(...,'reducedX',idx_x)
%       |--> allows to reduce the x points that are actually saved to file
%
%       (8)     saveLight(...,'noXDerivMat')
%       |--> does not save the derivation matrices and the other variables
%       to be able to compute x derivatives externally
%
%       (9)     saveLight(...,'BLprops')
%       |--> saves also boundary-layer (integral) properties
%
%       (10)    saveLight(...,'noChemistry')
%       |--> does not save the thermochemical nonequilibrium quantities
%       (i.e., ys, rhos)
%
% For detailed explanation on the options and their defaults see also
% setDefaults
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[varargin,noDerivs]     = find_flagAndRemove('noDerivs',varargin);      % no-derivatives flag
[varargin,onlyRhos]     = find_flagAndRemove('onlyRhos',varargin);      % only rho_s flag
[varargin,onlyYDerivs]  = find_flagAndRemove('onlyYDerivs',varargin);   % only y derivatives flag
[varargin,removeImag]   = find_flagAndRemove('removeImag',varargin);    % removes imaginary part of the vectors
[varargin,noXDerivMat]  = find_flagAndRemove('noXDerivMat',varargin);   % does not save the x derivation matrices and others
[varargin,BLprops]      = find_flagAndRemove('BLprops',varargin);       % add in BL integral properties
[varargin,noChemistry]  = find_flagAndRemove('noChemistry',varargin);   % remove all chemistry quantities
[varargin,reducedX,idx] = find_flagAndRemove('reducedX',varargin);      % reduced x domain flag
if ~isempty(idx)
    idx_x = varargin{idx};
    varargin(idx) = [];
else
    if isfield(intel,'u')
    idx_x = 1:size(intel.u,2); % if no idx was specified, we take all the columns in u (x positions)
    elseif isfield(intel,'f')
    idx_x = 1:size(intel.f,2); % if no idx was specified, we take all the columns in f (x positions)
    else
        error('neither u nor f where found as inputs, so idx could not be specified');
    end
end

if isempty(varargin)
    extra_vars = {};
else
    extra_vars = varargin{1};
end

%%% building variable lists by type
vars_needed = {'gitInfo','mixCnst','eta','xi','cylCoordFuncs'};                 % variables that are always needed (if they exist)
vars_edge = {'U_e','T_e','h_e','H_e','p_e','rho_e','mu_e','M_e','gam_e','cp_e','R_e','Re1_e','y_i','lBlasius'}; % edge variables that vary over x
vars_edge_s = {'ys_e','ys_inf','yE_e','yE_inf'};                                % edge variables that vary over x and have a species index
vars_edge_0 = {'U_e0','T_e0','p_e0','rho_e0','M_e0','Re1_e0','Sc_e','Le_e',...  % edge variables that do not vary
                'Pr_e','mu_ref','Su_mu','T_ref','beta','W_0','cone_angle','wedge_angle','shock_angle', 'Lambda_sweep', ...
                'U_inf','T_inf','h_inf','p_inf','rho_inf','M_inf'};
vars_dim = {'x','y','u','v','w','T','Tv','rho'};                                % flow dimensional variables
vars_dim_s = {'ys','rhos'};                                                     % flow dimensional variables with a species index
vars_bl_props = {'delta99', 'eta99', 'glBlasius', 'llBlasius', 'deltastar', ...
                 'thetastar', 'deltaestar', 'deltahstar', 'shapefactor'};
vars_dim_diffy = {'du_dy','du_dy2','dv_dy','dv_dy2','dw_dy','dw_dy2','dT_dy','dT_dy2','dTv_dy','dTv_dy2'};              % y derivatives of flow dimensional variables
vars_dim_diffx = {'du_dx','du_dxdy','du_dx2','dv_dx','dv_dxdy','dv_dx2','dw_dx','dw_dxdy','dw_dx2',...                  % x derivatives of flow dimensional variables
                    'dT_dx','dT_dxdy','dT_dx2','dTv_dx','dTv_dxdy','dTv_dx2'};
vars_dim_s_diffy = {'dys_dy','dys_dy2','dyE_dy','dyE_dy2','drhos_dy','drhos_dy2'};                                      % y derivatives of flow dimensional variables with a species index
vars_dim_s_diffx = {'dys_dx','dys_dxdy','dys_dx2','dyE_dx','dyE_dxdy','dyE_dx2','drhos_dx','drhos_dxdy','drhos_dx2'};   % x derivatives of flow dimensional variables with a species index
vars_nondim = {'f','df_deta','df_deta2','df_deta3','g','dg_deta','dg_deta2','gv','dgv_deta','dgv_deta2',...             % flow non-dimensional variables
                    'k','dk_deta','dk_deta2','ys','dys_deta','dys_deta2','yE','dyE_deta','dyE_deta2','j','tau','dtau_deta','dtau_deta2'};
vars_3D = {'D1_y','D2_y'};
vars_2D = {'D1','D2'};

vars4xDeriv = {'D1','D2','D1_xi','D2_xi','deta_dx','deta_dy','dxi_dx'};

%%% choosing which lists to add according to the booleans
vars2save = [vars_needed,vars_edge,vars_edge_s,vars_edge_0];            % always wanted
if options.dimoutput || options.marchYesNo                              % dimensional variables
    vars2save = [vars2save,vars_dim,vars_dim_s];
    if ~noDerivs                                                            % derivatives of dimensional variables
        vars2save = [vars2save,vars_dim_diffy,vars_dim_s_diffy,vars_3D];
        if ~onlyYDerivs                                                         % x derivatives of dimensional variables
            vars2save = [vars2save,vars_dim_diffx,vars_dim_s_diffx];
        end
    end
    if ~noXDerivMat                                                         % requirements for x derivation matrices to be retrievable
        vars2save = [vars2save,vars4xDeriv];
    end
else                                                                    % non-dimensional variables
    vars2save = [vars2save,vars_nondim,vars_2D];
end
if BLprops
    vars2save = [vars2save, vars_bl_props];                             % boundary-layer (integral) values
end
vars2save = unique([vars2save,extra_vars]);                             % appending extra variables chosen by the user

%%% Removing certain lists according to the chosen booleans
if onlyRhos
    idx_ys = ~cellfun(@isempty,regexp(vars2save,'^ys|^dys','once'));
    vars2save(idx_ys) = [];
end
if noChemistry
    idx_chemistry = ~cellfun(@isempty,regexp(vars2save,'^d?(y|rho)s','once'));
    vars2save(idx_chemistry) = [];
end

%%% Preparing string for the command to be used on the variables (real() or
% nothing)
if removeImag           % we will do real(...)
    strCommand0 = 'real(';  % opening command
    strCommand1 = ')';      % closing command
else                    % we will do ... (nothing)
    strCommand0 = '';       % opening command
    strCommand1 = '';       % closing command
end

%%% Warning message if reduced x and passing D_x matrices
if reducedX && ismember('D1_xi',vars2save) && length(idx_x) < size(intel.u,2)
    warning('A reduced x range was chosen to save the profiles whilst requiring to save streamwise derivation matrices (D1_xi). Reducing the x range will result in wrong derivation matrices');
end

%%% extracting from intel
bSave = false(size(vars2save));                             % initializing save boolean
for ii=1:length(vars2save)                                  % looping variables
    if isfield(intel,vars2save{ii})                             % if field exists
        bSave(ii) = true;                                           % we save it
        if iscell(intel.(vars2save{ii}))                            % if it is a cell (species quantity)
            sizes = size(intel.(vars2save{ii}){1});                     % obtaining dimensions
            for s=1:length(intel.(vars2save{ii}))                       % looping cell
                if length(sizes)==2 && sizes(2)~=1                          % matrix/vector varying over x
                    eval([vars2save{ii},'{s} = ',strCommand0,'intel.',vars2save{ii},'{s}(:,idx_x)',strCommand1,';']); % E.g.: ys{s} = real(intel.ys{s}(:,idx_x)); or ys_e{s} = intel.ys_e{s}(:,idx_x);
                elseif length(sizes)==2 && sizes(2)==1                      % matrix/vector not varying over x
                    eval([vars2save{ii},'{s} = ',strCommand0,'intel.',vars2save{ii},'{s}',strCommand1,';']); % E.g.: ys_e0{s} = real(intel.ys_e0{s}); or ys_e{s} = intel.ys_e{s};
                else                                                        % what else could it be?
                    error(['the sizes of ''',vars2save{ii},''' are: ',sprintf('%d x ',sizes),'. No option satisfies this combination']);
                end
            end
        elseif isstruct(intel.(vars2save{ii}))                      % if it is a structure
            eval([vars2save{ii},' = intel.',vars2save{ii},';']);        % E.g.: mixCnst = intel.mixCnst;
        elseif isnumeric(intel.(vars2save{ii}))                     % if it is a numerical matrix/vector/value
            sizes = size(intel.(vars2save{ii}));                        % obtaining dimensions
            if length(sizes)==3                                         % 3D matrix with x varying along the third dim.
                eval([vars2save{ii},' = ',strCommand0,'intel.',vars2save{ii},'(:,:,idx_x)',strCommand1,';']); % E.g.: D1_y = intel.D1_y(:,:,idx_x);
            elseif length(sizes)==2 && ismember(vars2save{ii},vars_2D)  % 2D matrix of N x N
                eval([vars2save{ii},' = ',strCommand0,'intel.',vars2save{ii},strCommand1,';']);        % E.g.: D1 = real(intel.D1);
            elseif length(sizes)==2 && sizes(2)~=1                      % matrix/vector varying over x
                eval([vars2save{ii},' = ',strCommand0,'intel.',vars2save{ii},'(:,idx_x)',strCommand1,';']); % E.g.: u = intel.u(:,idx_x);
            elseif length(sizes)==2 && sizes(2)==1                      % matrix/vector not varying over x
                eval([vars2save{ii},' = ',strCommand0,'intel.',vars2save{ii},strCommand1,';']);        % E.g.: f = real(intel.f);
            else                                                        % what else could it be?
                error(['the sizes of ''',vars2save{ii},''' are: ',sprintf('%d x ',sizes),'. No option satisfies this combination']);
            end
        end
    end
end
vars2save = vars2save(bSave); % removing the fields that do not appear in our intel
[~,idx_sorted] = sort(lower(vars2save)); % sorting them without case dependency
vars2save = vars2save(idx_sorted);

save(savepath,vars2save{:},'options');
