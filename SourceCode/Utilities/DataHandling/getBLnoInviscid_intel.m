function [intelBL,optionsBL] = getBLnoInviscid_intel(intel,intelQSS,options,optionsQSS)
% getBLnoInviscid_intel Properly construct the intel struct for the
% marching solver, whenever the user does not request for the inviscid 1D
% solver.
%
% Usage:
%   (1)
%       [intelBL,optionsBL] = getBLnoInviscid_intel(intel,intelQSS,options,optionsQSS)
%
% In that case, all *_e quantities coming from the QSS simulation must be
% repeated for the entire x range
%
% Since intelQSS comes from a concluded QSS run, we have all edge
% properties at our reach. If options.shockJump=true, these properties will
% be the post-shock ones. We will always fix the same edge properties:
%   - M_e
%   - p_e0
%   - T_e
%   - ys_e
%
% Sc_e and Le_e are also defined from intel, in case it is necessary for
% the diffusion assumption.
%
% The options structure is also modified to toggle the shockJump boolean to
% false. The edge properties coming from the QSS simulation are already
% post-shock, so we don't want to make a double shock jump.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

flds_all = fieldnames(intel);                                               % fieldnames in the original intel structure
flds_e = flds_all(~cellfun(@isempty,regexp(flds_all,'^[\w]+\_(e|e0)$','match'))); % finding all *_e or *_e0 fields
intelBL         = rmfield(intel,flds_e);                                    % removing them from intel
if optionsQSS.fixXeRangePostshock                                           % if the user wants to fix the x_e range to be used in the post-shock region
    neededvalue(intel,'x_e','''fixXeRangePostshock'' was chosen as true, yet x_e was not passed in intel.'); % make sure it is there
    intelBL.x_e = intel.x_e;                                                    % we read it from intel
else                                                                        % if he doesn't
    intelBL.x_e     = 0;                                                        % dummy x
end
intelBL.M_e     = intelQSS.M_e * ones(size(intelBL.x_e));                   % adding the fields from intelQSS
intelBL.T_e     = intelQSS.T_e * ones(size(intelBL.x_e));
intelBL.ys_e    = cellfun(@(cll)cll * ones(size(intelBL.x_e)),intelQSS.ys_e,'UniformOutput',false);
if length(intelBL.x_e)>1                                                    % depending on the length of x_e
    intelBL.p_e0    = intelQSS.p_e;                                             % pressure will be fixed through p_e0
else                                                                        % or
    intelBL.p_e     = intelQSS.p_e;                                             % through p_e
end
if isfield(intelQSS,'Tv_e')
    intelBL.Tv_e = intelQSS.Tv_e * ones(size(intelBL.x_e));
end
optionsBL = options;
optionsBL.shockJump = false;                                                % QSS properties are already post-shock
if isfield(intelQSS,'shock_angle')
    intelBL.shock_angle = intelQSS.shock_angle;
end

end % getBLnoInviscid_intel