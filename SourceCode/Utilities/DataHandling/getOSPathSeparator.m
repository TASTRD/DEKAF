function sep = getOSPathSeparator()
%getOSPathSeparator Retrieve the path separator based on the machine's OS.
%
% Example:
%   sep = getOSPathSeparator();
%   PATHdirs = regexp(getenv('PATH'),sep,'split');
%
% Author(s): Ethan Beyak, Fernando Miro Miro
% GNU Lesser General Public License 3.0

if ispc
    sep = '\;'; % for windows, the separation is given by ;
else
    sep = '\:'; % for non-windows (mac and unix), the separation is given by :
end

end % getOSPathSeparator
