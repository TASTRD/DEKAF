function cllOut = alphabet(varargin)
% ALPHABET returns a cell of strings with the letters of the alphabet in
% lowercase.
%
% Examples:
%   (1)
%       ALPHABET()
%
%       ans = {'a','b','c','d','e','f','g','h','i','j','k','l','m',...
%              'n','o','p','q','r','s','t','u','v','w','x','y','z'}
%
%   (2)
%       ALPHABET('capital')
%
%       ans = {'A','B','C','D','E','F','G','H','I','J','K','L','M',...
%              'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}
%
% Author: Fernando Miro Miro
% Date: November 2018
% GNU Lesser General Public License 3.0

[~,bCapital] = find_flagAndRemove('capital',varargin);

if bCapital
cll1 = {'A','B','C','D','E','F','G','H','I','J','K','L','M',...
          'N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
else
cll1 = {'a','b','c','d','e','f','g','h','i','j','k','l','m',...
          'n','o','p','q','r','s','t','u','v','w','x','y','z'}; % one letter
end

cllOut = cll1;
for ii=1:length(cllOut) % looping and appending two-letter entries
    cllOut = [cllOut,cellfun(@(str)[cll1{ii},str],cll1,'UniformOutput',false)];
end