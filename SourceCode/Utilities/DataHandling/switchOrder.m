function var_out = switchOrder(var_in,dim,list_in,list_out)
% switchOrder changes the order of a matrix in a particular dimension, such
% that it matches the order of a given list.
%
% Examples:
%   (1)
%       var_in = [1,2,3,4,5,6];
%       list_in = {'apple','banana','carrot','daisy','elephant','flamingo'}
%       list_out = {'banana','daisy','elephant','apple','banana'}
%       dim = 2;
%   var_out = switchOrder(var_in,dim,list_in,list_out)
%       ---> [2,4,5,1,2]
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

idx_out = cellfun(@(cll)find(ismember(list_in,cll)),list_out);              % positions of the elements in list_out in list_in
N_dim = length(size(var_in));                                               % number of dimensions
sizes(1:N_dim) = size(var_in);                                              % all sizes
idx_dim = 1:N_dim;
var_in = permute(var_in,[dim,idx_dim(idx_dim~=dim)]);                       % putting dim in the first dimension
var_in = reshape(var_in,[sizes(dim),prod(sizes(idx_dim~=dim))]);            % making the matrix 2D by compressing all other dimensions into the first
var_out = var_in(idx_out,:);                                                % swapping order
var_out = reshape(var_out,[length(list_out),sizes(idx_dim~=dim)]);          % reshaping back to the original number of dimensions
var_out = permute(var_out,[2:dim,1,dim+1:N_dim]);                           % permuting the dimension back to its right place