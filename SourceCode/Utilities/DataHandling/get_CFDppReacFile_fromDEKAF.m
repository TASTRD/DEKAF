function [] = get_CFDppReacFile_fromDEKAF(mixCnst,options,varargin)
% get_CFDppReacFile_fromDEKAF creates a .reac file to be used in CFD++,
% with the same reaction-rate constants as in DEKAF.
%
% Usage:
%   (1)
%       options.mixture = air2
%       get_CFDppReacFile_fromDEKAF(mixCnst,options)
%       |
%       |--> saves in './DEKAF_air2.rea'
%
%   (2)
%       get_CFDppReacFile_fromDEKAF(mixCnst,options,'savepath','filename.rea')
%       |
%       |--> saves in 'filename.rea'
%
% Inputs and outputs:
%   mixCnst
%       structure with all the mixture constants of interest as returned by
%       getAll_constants
%   options
%       structure with all DEKAF options as returned by setDefaults
%
% See also: getAll_constants, setDefaults
%
% Author(s): Fernando Miro Miro
% Date: November 2018
% GNU Lesser General Public License 3.0

% checking options
[~,~,idx] = find_flagAndRemove('savepath',varargin);
if isempty(idx)
    savepath = ['./DEKAF_',options.mixture,'.rea'];
end

% Extracting variables from mixCnst
nu_reac = mixCnst.nu_reac;
nu_prod = mixCnst.nu_prod;
A_r     = mixCnst.A_r;
theta_r = mixCnst.theta_r;
nT_r    = mixCnst.nT_r;
spec_list = mixCnst.spec_list;
[N_spec,N_reac] = size(nu_reac);

% Unit changes
A_r = A_r .* (1e3).^(sum(nu_reac,1)-1); % [(m^3/mol)^(sum(nu_reac)-1)] --> [(m^3/kmol)^(sum(nu_reac)-1)]
Ea_r = mixCnst.R0 * theta_r*1e3;        % [K] --> [J/kmol]

% preparing header
gitInfo = getGitInfo(); % getting git information
str_header = ['#',newline,'Version ',DEKAFversion(),newline,'#',newline...
    '#------------------------------------------------------------------------------------',newline,...
    '#---                                 DESCRIPTION                                  ---',newline,...
    '#------------------------------------------------------------------------------------',newline,...
    '# Reaction file created automatically by get_CFDppReacFile_fromDEKAF',newline,...
    '#',newline,'# DEKAF mixture: ',options.mixture,newline,...
    '#',newline,'# The format is:',newline,...
    '#   1) number of reactants',newline,...
    '#   2) number of products',newline,...
    '#   3) reaction',newline,...
    '#   4) modified reaction',newline,...
    '#   5) rates',newline,...
    '#       5.1) A_r  - reaction rate preexponential constant              (m^3/kmol)^(sum(nu_reac)-1) * K^(1/nT_r)',newline,...
    '#       5.2) nT_r - reaction rate exponential constant                 -',newline,...
    '#       5.3) Ea_r - reaction activation energy                         J/kmol',newline,...
    '#       5.4) q_r  - reaction exponent for the trans-rot temperature    -',newline,...
    '#       5.5) qv_r - reaction exponent for the vib-elec-el temperature  -',newline,...
    '#       5.6-5.9) unknown, copied from previous *.rea file',newline,...
    '#   6) efficiencies in groups of ',num2str(N_spec),' (one per species - not applicable since third-body reactions are expressed explicitly)',newline,...
    '#',newline,...
    '# DEKAF url:',   gitInfo.url,    newline,...
    '# DEKAF branch:',gitInfo.branch, newline,...
    '# DEKAF hash:',  gitInfo.hash,   newline,...
    '#------------------------------------------------------------------------------------',newline,...
    ];
% preparing species-list string
str_specList = ['#',newline,'# Number of species:',newline,num2str(N_spec),newline,...
    '# List of species:',newline,sprintf('%s\n',spec_list{:})];
% preparing reaction-list string
str_reacList{1} = ['#',newline,'# Number of reactions:',newline,num2str(N_reac),newline,...
    '# List of reactions:',newline,'#',newline];
for r = N_reac:-1:1 % looping reactions
    N_reactants = nnz(nu_reac(:,r));
    N_products  = nnz(nu_prod(:,r));
    cell_reac = {}; % allocating cell with the reactant coefficients
    cell_prod = {}; % allocating cell with the product coefficients
    ir = 1; ip = 1; % initializing counters
    for s=1:N_spec % looping species
        if nu_reac(s,r)~=0 % if the species participates of the reactant
            cell_reac{ir} = [num2str(nu_reac(s,r)),'.0 ',spec_list{s}]; % adding species with its stoichiometry to the cell
            ir = ir+1; % increasing reactant cell counter
        end
        if nu_prod(s,r)~=0 % if the species participates of the product
            cell_prod{ip} = [num2str(nu_prod(s,r)),'.0 ',spec_list{s}]; % adding species with its stoichiometry to the cell
            ip = ip+1; % increasing product cell counter
        end
    end
    str_reacList{r+1} = [...
        num2str(N_reactants),' reactants',newline,...
        num2str(N_products) ,' products', newline,...
        strjoin(cell_reac,' + '),' <-> ',strjoin(cell_prod,' + '),newline,...
        strjoin(cell_reac,' + '),' <-> ',strjoin(cell_prod,' + '),newline,...
        sprintf('%0.3e %0.2f %0.6e',[A_r(r),nT_r(r),Ea_r(r)]),' 0.5 0.5 1.0 0.0 0.0',newline,... % the additional numbers are the factors for the two-temperature case FIXME and some unknown coefficients
        sprintf('%0.1f ',zeros(size(spec_list))),newline...                                      % third-body efficiencies (not applicable when decomposing third-body reactions into N_spec different ones)
        ];
end

% Writing to file
fID = fopen(savepath,'w');
fwrite(fID,[str_header,str_specList,str_reacList{:}]);
fclose(fID);

