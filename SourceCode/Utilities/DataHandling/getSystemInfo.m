function sysData = getSystemInfo()
% getSystemInfo returns the most relevant system information, including:
%   - Matlab version
%   - Operative system
%   - Processor
%
% Usage:
%   (1)
%       sysData = getSystemInfo()
%
% Inputs and outputs:
%   sysData
%       structure containing the relevant system data in string format:
%          .matlabVersion
%          .OS
%          .processor
%
% See also: getMatlabVersion, getOSInfo, cpuinfo
%
% Author: Fernando Miro Miro
% Date: October 2019
%
% GNU Lesser General Public License 3.0

sysData.matlabVersion   = getMatlabVersion();
sysData.OS              = getOSInfo();
processorData           = cpuinfo();
sysData.processor       = processorData.Name;

end % getSystemInfo