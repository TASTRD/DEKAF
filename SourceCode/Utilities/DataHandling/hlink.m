function hstr = hlink(funcName,varargin)
% HLINK returns the string to be used in disp(...) to have a hyperlink to a
% matlab function.
%
% Examples:
%   (1)
%       hstr = hlink('DEKAF')
%       --> '<a href="matlab- opentoline(which(''DEKAF.m'',1)">DEKAF</a>'
%
%   (2)
%       hstr = hlink('DEKAF','here')
%       --> '<a href="matlab- opentoline(which(''DEKAF.m'',1)">here</a>'
%       |
%       |--> allows to specify the text with which the hyperlink will be
%       displayed
%
%   (3)
%       hstr = hlink('DEKAF','here',25)
%       --> '<a href="matlab- opentoline(which(''DEKAF.m'',25)">here</a>'
%       |
%       |--> allows to specify the line in the function to which the
%       hyperlink will direct
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

switch length(varargin)
    case 0
        dispName = funcName; % defaults
        lineNum = 1;
    case 1
        dispName = varargin{1};
        lineNum = 1; % defaults
    case 2
        dispName = varargin{1};
        lineNum = varargin{2};
    otherwise
        error('Wrong number of inputs');
end

hstr = ['<a href="matlab: opentoline(''',which([funcName,'.m']),''',',num2str(lineNum),')">',dispName,'</a>'];

end % hlink