function strOut = leftStr(strIn,Nstr)
% leftStr returns a left-aligned string within a given number of characters.
%
% Usage:
%   (1)
%       strOut = leftStr(strIn,Nstr)
%
% Examples:
%   (1)
%       strIn = 'This is a trial string. We make it very long and with one hyphen-dominated long word to split.';
%       Nstr = 25;
%       leftStr(strIn,Nstr)
%           ans =
%             '- This is a trial  ------
%              - string. We make it  ---
%              - very long and with  ---
%              - one hyphen- -----------
%              - dominated long word  --
%              - to split. -------------'
%
% See also: centerStr
%
% Author: Fernando Miro Miro
% Date: October 2019

NstrIn = length(strIn);                                             % length of inputted string

if NstrIn>Nstr-4                                                    % for strings that are too long, we have to split them
    idxSpc = regexp(strIn,'\-|\ ','start');                             % finding spaces and hyphens to split at
    idxSplitRel = find(idxSpc<Nstr-4,1,'last');                         % relative positions of where we want to split - the last accepted splitting point more than Nstr-4
    if isempty(idxSplitRel);    idxSplit = Nstr-4;                      % if there is no accepted splitting point - split at Nstr-4
    else;                       idxSplit = idxSpc(idxSplitRel);         % if there is, split there
    end
    strOut = [leftStr(strIn(1:idxSplit),Nstr) , newline , leftStr(strIn(idxSplit+1:end),Nstr)]; % calling recursively the two halves
else                                                                % for strings that aren't too long
    N2 = Nstr-NstrIn-3;                                                 % number of hyphens after
    strOut = ['- ',strIn,' ',repmat('-',[1,N2])];                       % preparing output
end


end % leftStr