function [] = anyNaNs_DKF(structIn,varargin)
% anyNaNs_DKF checks if any of the fields in a structure contain NaNs.
%
% It works recursively, so it also searches in structures embedded within
% the passed structure.
%
% Usage:
%   (1)
%       anyNaNs_DKF(structIn)
%
%   (2)
%       anyNaNs_DKF(structIn,prevString)
%       |--> allows to pass a string to be displayed before the structure
%       field name. This is used for the recursive call.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if isempty(varargin);       prevString = '';
else;                       prevString = varargin{1};
end

fldnms = fieldnames(structIn);                                                  % structure field names

for ii=1:length(fldnms)                                                         % looping field names
    if isnumeric(structIn.(fldnms{ii}))                                             % vector - check for NaNs
        if any(isnan(structIn.(fldnms{ii})(:)))
            disp([prevString,fldnms{ii},' has ',num2str(nnz(isnan(structIn.(fldnms{ii})(:)))),' NaNs']);
        end
    elseif isstruct(structIn.(fldnms{ii}))                                          % embedded structure - recursive call
        anyNaNs(structIn.(fldnms{ii}),[prevString,fldnms{ii},'.']);
    elseif iscell(structIn.(fldnms{ii}))                                            % cell - loop it and check for NaNs
        cll = structIn.(fldnms{ii})(:);                                                 % extracting cell
        for jj=1:length(cll)                                                            % looping cell
            if ~isnumeric(cll{jj})
                disp([prevString,fldnms{ii},'{',num2str(jj),'}  is not a structure nor numeric, so it cannot be searched']);
            elseif isnumeric(cll{jj}) && any(isnan(cll{jj}(:)))
                disp([prevString,fldnms{ii},'{',num2str(jj),'} has ',num2str(nnz(isnan(cll{jj}(:)))),' NaNs']);
            end
        end
    else                                                                            % anything else - no check
        disp([prevString,fldnms{ii},' is not a structure nor numeric, so it cannot be searched']);
    end
end

end % anyNaNs_DKF
