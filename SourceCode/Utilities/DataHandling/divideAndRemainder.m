function [out] = divideAndRemainder(N1,N2)
% divideAndRemainder(N1,N2) returns a vector of interval limits, such that
% they have a maximium length of N2, going from 1 to N1.
%
% Examples:
%   (1)
%       divideAndRemainder(13,3)
%           Ans =
%                   [3,6,9,12,13]
%
%   (2)
%       divideAndRemainder(133,20)
%           Ans =
%                   [20,40,60,80,100,120,133]
%
%   (3)
%       divideAndRemainder(133,140)
%           Ans =
%                   133
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if N2>N1;                       out = N1; % one unique interval
elseif floor(N1/N2) == N1/N2;   out = N2:N2:floor(N1/N2)*N2; % various identical intervals
else;                           out = [N2:N2:floor(N1/N2)*N2,N1]; % various intervals
end


end % divideAndRemainder