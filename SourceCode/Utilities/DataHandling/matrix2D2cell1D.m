function cell1D = matrix2D2cell1D(mat2D)
% matrix2D2cell1D(mat2D) converts a two-dimensional matrix with vectors
% arranged in columns into a one-dimensional cell with these vectors in the
% different cell positions.
%
% See also: cell1D2matrix2D
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,N_cell] = size(mat2D);       % size
cell1D = cell(N_cell,1);        % allocating
for ii=1:N_cell                 % looping cell
    cell1D{ii} = mat2D(:,ii);   % populating matrix
end