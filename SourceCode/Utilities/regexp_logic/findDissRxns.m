function [idxDiss,idxTB] = findDissRxns(rxns)
%FINDDISSRXNS Find the indices of the dissociation reactions from a matrix
% of stoichiometric coefficients, as well as the indices corresponding to
% reactions with third-bodies.
%
% Example:
%   rxns = {...
%               'O + N   <->  O+ + e- + N',...
%               'O2 + M  <->  O2+ + e- + M',...
%               'O + N   <->  NO+ + e-',...
%               'NO + M  <->  N + O + M',...
%               'O2 + M  <->  2O + M',...
%               'O2 + O  <->  3O',...
%               'CO2 + M <->  O2 + C + M',...
%               'C2 + N  <->  2C + N',...
%               'N2 + e- <->  2N + e-',...
%               'C3 + M  <->  C2 + C + M',...
%               };
%   [idxDiss,idxTB] = findDissRxns(rxns)
%   idxDiss = [...
%               0;
%               0;
%               0;
%               1;
%               1;
%               1;
%               1;
%               1;
%               1;
%               1;
%              ];
%
%   idxTB   = [...
%               0;
%               1;
%               0;
%               1;
%               1;
%               1;
%               0;
%               0;
%               1;
%              ];
%
% Notes:
% A dissociation reaction is a chemical reaction where a compound breaks
% apart into two or more parts. For example, AB -> A + B
%
% IMPORTANT: three-way dissociation is not considered, since it is
% extraordinarily rare (see Scoggins Thesis)
%
% findDissRxns is a subfunction of identifyReac_dissel.m
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

idxDiss = false(length(rxns),1);
idxTB   = idxDiss;

for ii=1:length(rxns)
    % Split the reaction into reactants and products
    rxn = regexp(rxns{ii},'<->','split');
    reac = rxn{1}; prod = rxn{2};
    reacs = split_reacSide(reac);                                   % splitting individual products
    prods = split_reacSide(prod);
    reacs = strrep(reacs,' ','');                                   % removing blank spaces
    prods = strrep(prods,' ','');

    % Check reactions that have M's in them.
    bM_reacs = strcmp(reacs,'M');                                   % boolean vector with the positions of M in the reactants
    bM_prods = strcmp(prods,'M');                                   % boolean vector with the positions of M in the products
    idxTB(ii) = any(bM_reacs) & any(bM_prods);                      % if we have M on both sides it's a third-body reaction (with variable third body M)

    % removing M from lists
    reacs(bM_reacs) = [];
    prods(bM_prods) = [];
    for iR = 1:length(reacs)                                        % looping remaining reactants
        diss_mol = reacs{iR};                                           % choosing potential dissociation molecule
        % new arrangement working for whatever molecule
        atom_list = getAtoms_molecule(diss_mol);                         % extracting all atoms in a molecule
        [comb_list1,comb_list2] = getAtoms_combinations(atom_list);         % possible combinations of atoms after dissociation
        for jj=1:length(comb_list1)                                         % looping combinations
            if ismember(comb_list1{jj},prods) && ismember(comb_list2{jj},prods) ... % if both dissociation combinations are in the product list
                    || strcmp(comb_list1{jj},comb_list2{jj}) && ismember(['2',comb_list2{jj}],prods) % or if it is a double-product
                idxDiss(ii) = true;                                                 % it is a dissociation reaction
            end
        end
    end
end

end % findDissRxns