function [idxIoniz,idxTB] = findIonizImpactRxns(rxns)
% findIonizImpactRxns Find the indices of the ionization reactions by
% impact from a series of reaction strings, as well as the indices
% corresponding to reactions with third-bodies.
%
% Example:
%   rxns = {...
%               'O + N   <->  O+ + e- + N',...
%               'O2 + M  <->  O2+ + e- + M',...
%               'O + N   <->  NO+ + e-',...
%               'NO + M  <->  N + O + M',...
%               'O2 + e- <->  O2+ + 2e-',...
%               'O2+ + N <->  O2++ + e- + N',...
%               'CO2 + M <->  O2 + C + M',...
%               'N2 + N  <->  N2+ + N + e-',...
%               'N2 + e- <->  2N + e-',...
%               'C3 + M  <->  C2 + C + M',...
%               'NO+ + M <->  NO++ + e- + M',...
%               };
%   [idxIoniz,idxTB] = findIonizImpactRxns(rxns)
%   idxIoniz = [...
%               1;
%               1;
%               0;
%               0;
%               1;
%               1;
%               0;
%               1;
%               0;
%               0;
%               1;
%              ];
%
%   idxTB   = [...
%               0;
%               1;
%               0;
%               1;
%               0;
%               0;
%               1;
%               0;
%               0;
%               1;
%               1;
%              ];
%
% Notes:
% An ionization reaction is a chemical reaction where a species becomes
% ionized and looses one electron. For example, A -> A+ + e-
%
% IMPORTANT: second ionization reactions, must be expressed with the second
% ions with two + signs: A+ -> A++ + e-
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

idxIoniz = false(length(rxns),1);
idxTB   = idxIoniz;

for ii=1:length(rxns)
    % Split the reaction into reactants and products
    rxn = regexp(rxns{ii},'<->','split');
    reac = rxn{1}; prod = rxn{2};
    reacs = split_reacSide(reac);                                   % splitting individual products
    prods = split_reacSide(prod);
    reacs = strrep(reacs,' ','');                                   % removing blank spaces
    prods = strrep(prods,' ','');

    % Check reactions that have M's in them.
    bM_reacs = strcmp(reacs,'M');                                   % boolean vector with the positions of M in the reactants
    bM_prods = strcmp(prods,'M');                                   % boolean vector with the positions of M in the products
    idxTB(ii) = any(bM_reacs) & any(bM_prods);                      % if we have M on both sides it's a third-body reaction (with variable third body M)

    % removing M from lists
    reacs(bM_reacs) = [];
    prods(bM_prods) = [];
    for iR = 1:length(reacs)                                        % looping remaining reactants
        ioniz_spec = reacs{iR};                                         % choosing potential ionizing species
        if ismember([ioniz_spec,'+'],prods) && ismember('e-',prods)     % if the ion of the potential ionizing species and an electron are present in the product list
            idxIoniz(ii) = true;                                            % it is an ionization reaction
        end
    end
end

end % findIonizImpactRxns