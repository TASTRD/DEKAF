function bCationNeut = getPair_bCationNeut(bPos,bNeg)
%GETBOOL_BCATIONNEUT Calculate the boolean of cation-neutral interactions
% given the negatively-charged boolean and the positively-charged boolean
%
% Usage:
%   bCationNeut = getPair_bCationNeut(bPos,bNeg);
%
% Inputs and Outputs
%   bPos            boolean array. an entry is true if positively-charged
%                   N_spec x 1
%   bNeg            boolean array. an entry is true if negatively-charged
%                   N_spec x 1
%
%   bCharged        boolean matrix. the (i,j)th entry is true if the (s,l)
%                   interaction of spec_list involve a neutral species and
%                   a cation
%                   N_spec x N_spec
%
% Examples
%   spec_list = {'O+','O2+','e-','N','O'};
%   bCationNeut = getPair_bCationNeut(bPos,bNeg)
%
%   bCationNeut =
%
%       5×5 logical array
%
%       0   0   0   1   1
%       0   0   0   1   1
%       0   0   0   0   0
%       0   0   0   0   0
%       0   0   0   0   0
%
%

% Related functions:
% See also getSpecies_bPosNeg
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

bNeut = ~bPos & ~bNeg;

bCationNeut = logical(bPos' .* bNeut);

end % getPair_bCationNeut
