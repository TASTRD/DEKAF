function mol = getMolecule_atoms(atoms)
% getMolecule_atoms returns the molecule that would be the combination of a
% series of atoms.
%
% Examples:
%   (1)
%       getMolecule_atoms({'O','O','O','O','C','C','C','N'})
%       ans = 'C3NO4'
%
% Author: Fernando Miro Miro
% Date: May 2019
% GNU Lesser General Public License 3.0

atoms = sort(atoms(:).');                   % organizing alphabetically and making into row cell
atomsUnique = unique(atoms);                % removing repetitions
nAtoms = cellfun(@(cll)num2str(nnz(ismember(atoms,{cll}))),atomsUnique,'UniformOutput',false); % making a list of strings with the number of appearances of each atom
nAtoms(strcmp(nAtoms,'1')) = {''};          % removing ones
molCell2D = [atomsUnique ; nAtoms];         % appending vertically
mol = [molCell2D{:}];                       % appending everything

end % getMolecule_atoms