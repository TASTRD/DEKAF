function ZCharge = getSpecies_ZCharge(bPos,bNeg)
%GETSPECIES_ZCHARGE Calculate the elementary charge value of the species
%
% Usage:
%   (1)
%       ZCharge = getSpecies_ZCharge(bPos,bNeg)
%
% Inputs and outputs:
%   bPos        [N_spec x 1]
%   bNeg        [N_spec x 1]
%   ZCharge     [N_spec x 1]
%
% note, this function currently only works for SINGLY-IONIZED species
% for mutliply-ionized species, spec_list will have to be passed in, and
% the number post + or - will need to be regexp'd out. If no number, set to
% one
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
% GNU Lesser General Public License 3.0

ZCharge = bPos - bNeg;

end % getSpecies_ZCharge
