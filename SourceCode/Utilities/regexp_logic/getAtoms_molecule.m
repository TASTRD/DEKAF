function atom_list = getAtoms_molecule(spec_list)
% getAtoms_molecule returns a list of all atoms contained in a molecular
% species.
%
% Examples:
%   (1)
%       getAtoms_molecule('CO2')
%       ans = {'C','O','O'}
%
%   (2)
%       getAtoms_molecule({'CO2','Ca3C2'})
%       ans = {{'C','O','O'} , {'Ca','Ca','Ca','C','C'}}
%
% Author: Fernando Miro Miro
% Date: May 2019
% GNU Lesser General Public License 3.0


bSingle = ~iscell(spec_list);                                   % single-species boolean
if bSingle
    spec_list = {spec_list};                                        % making into cell if necessary
end
atom_list = cell(size(spec_list));                              % allocating
for s=1:length(spec_list)                                       % looping species
    parts = regexp(spec_list{s},'([0-9]+)|([A-Z][a-z]?)|(e\-)','match');  % separating into numbers (one or more) or Captial letter plus lowercase letter (zero or one) or e-
    % NOTE: the e- option is simply kept in order to make the function usable when called within identifyReac_ionassoc
    bNum = ~cellfun(@isempty,regexp(parts,'[0-9]+','once'));        % identifying numbers in the molecule
    atom_list{s} = {};                                              % initializing
    for ii=1:length(parts)                                          % looping parts of the molecule
        if ~bNum(ii)                                                    % part not numeric
            atom_list{s} = [atom_list{s},parts(ii)];                        % we simply append the atom
        else                                                            % part is numeric
            for jj=1:str2double(parts{ii})-1                                % we loop up to as many positions as necessary
                % NOTE: one less than the number in the molecule's expression, because the previous position already had the atom once
                atom_list{s} = [atom_list{s},parts(ii-1)];                      % we append the previous atom
            end
        end
    end
end
if bSingle                                                      % if a single species was passed
    atom_list = atom_list{1};                                       % we output a cell instead of a cell of cells
end

end % getAtoms_molecule