function bDissel = identifyReac_dissel(rxns,spec_list)
%identifyReac_dissel Construct a boolean vector identifying which
% reactions in a cell of reactions describe dissociation with third-body
% of free electrons.
%
% Input:
%   rxns        cell of strings where each entry represents a chemical
%               reaction. The reactants are supplied to the left of the
%               ascii equilibrium double arrow '<->'; the products are
%               to the right of the double arrow
%
%               it is assumed that the lack of a coefficient on a
%               chemical species is equivalent to a coefficient of one.
%
%               user input requires separation of chemical reactants,
%               products, and the double arrow with whitespace to
%               distinguish distinct parts of the syntax
%
%   spec_list   cell of strings where each entry represents a species
%               considered in the chemical reactions supplied
%               the order of the columns in the output matrices
%               nu_reac, nu_prod are ordered according to that
%               prescribed in spec_list
%
% Output:
%   bDissel     vector of booleans of length(rxns). The ith element is true
%               if and only if the reaction described by rxns{i} is an
%               dissociation reaction with a third-body of a free electron.
%
% Example:
%   rxns = {...
%               'O + N   <-> O+ + e- + N',...
%               'O2 + M  <-> O2+ + e- + M',...
%               'O + N   <-> NO+ + e-',...
%               'NO + M  <-> N + O + M',...
%               'O2 + M  <-> 2O + M',...
%               'CO2 + M <-> O2 + C + M',...
%               'C2 + N  <-> 2C + N',...
%               'N2 + e- <-> 2N + e-',...
%               'C3 + M  <-> C2 + C + M',...
%               };
%   spec_list = {'N','O','NO','N2','O2','C3','C2','C','CO2',...
%                'N+','O+','NO+','N2+','O2+','e-'};
%   bDissel = identifyReac_dissel(rxns,spec_list)
%           [ 0;                                            % O + N   <->  O+ + e- + N
%             0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0   % O2 + M  <->  O2+ + e- + M
%             0;                                            % O + N   <->  NO+ + e-
%             0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 1   % NO + M  <->  N + O + M
%             0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 1   % O2 + M  <->  2O + M
%             0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 1   % CO2 + M <->  O2 + C + M
%             0;                                            % C2 + N  <->  2C + N
%             1;                                            % N2 + e- <->  2N + e-
%             0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 1   % C3 + M  <->  C2 + C + M
%           ]
%
% See also identifyReac_ionelim.m, identifyReac_ionassoc.m, findDissRxns,
%          GenerateStoichiometricMatrices.m
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% Identify the positions of the dissociation reactions.
[idxDiss,idxTB] = findDissRxns(rxns);

% Find 'e-' in the list of species.
bElectron = ismember(spec_list, 'e-');

% Initialize
N_spec = length(spec_list);
N_reac = length(rxns);
bDissel = false(N_reac + nnz(idxTB)*(N_spec-1),1); % Subtract one from N_spec to not double-count
jj = 0;

for ii = 1:length(rxns)
    if idxTB(ii)                                % it has a third body
        bDissel(jj+1:jj+N_spec) = idxDiss(ii) & bElectron; % it will be true for the electron species and dissociation reactions, false otherwise
        jj = jj + N_spec;                           % increase reaction index counter by N_spec
    else                                        % the reaction does not have a third body
        if idxDiss(ii) && ...                       % it is a stand-alone dissociation reaction (like 'C2 + N  <->  2C + N')
           ~isempty(regexp(rxns{ii},'e\-','match')) % ... and contains an electron somewhere (colliding)
            bDissel(jj+1) = true;                       % it is a dissociation-by-electron-impact reaction
        end
        jj = jj + 1;                                % increase reaction index counter by 1.
    end
end

end % identifyReac_dissel.m