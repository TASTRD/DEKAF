function [bIonelim,bIonHeavyim,bDissel,bIonAssoc] = identifyReac_allioniz(rxns,spec_list)
% identifyReac_all identifies all the different types of reactions.
%
% Usage:
%   (1)
%       [bIonelim,bIonHeavyim,bDissel,bIonAssoc] = identifyReac_all(rxns,spec_list)
%
% Inputs:
%   rxns        cell of strings where each entry represents a chemical
%               reaction. The reactants are supplied to the left of the
%               ascii equilibrium double arrow '<->'; the products are
%               to the right of the double arrow
%
%               it is assumed that the lack of a coefficient on a
%               chemical species is equivalent to a coefficient of one.
%
%               user input requires separation of chemical reactants,
%               products, and the double arrow with whitespace to
%               distinguish distinct parts of the syntax
%
%   spec_list   cell of strings where each entry represents a species
%               considered in the chemical reactions supplied
%               the order of the columns in the output matrices
%               nu_reac, nu_prod are ordered according to that
%               prescribed in spec_list
%
% Output:
%   bIonelim    vector of booleans of length(rxns). The ith element is true
%               if and only if the reaction described by rxns{i} is an
%               ionization electron impact reaction.
%
%   bIonHeavyim    vector of booleans of length(rxns). The ith element is true
%               if and only if the reaction described by rxns{i} is an
%               ionization by heavy-particle impact.
%
%   bDissel     vector of booleans of length(rxns). The ith element is true
%               if and only if the reaction described by rxns{i} is an
%               dissociation reaction with a third-body of a free electron.
%
%   bIonAssoc   vector of booleans of length(rxns). The ith element is true
%               if and only if the reaction described by rxns{i} is an
%               associative ionization reaction.
%
% See also identifyReac_ionassoc, identifyReac_ionelim,
% identifyReac_dissel, identifyReac_ionHeavyim,
% GenerateStoichiometricMatrices, findIonizRxns
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

bIonHeavyim = identifyReac_ionHeavyim(rxns,spec_list);
bIonelim    = identifyReac_ionelim(rxns,spec_list);
bIonAssoc   = identifyReac_ionassoc(rxns,spec_list);
bDissel     = identifyReac_dissel(rxns,spec_list);

end % identifyReac_all