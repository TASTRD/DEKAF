function [nu_reac,nu_prod,rxns_tb] = generateStoichiometricMatrices(reactions,spec_list)
%GENERATESTOICHIOMETRICMATRICES Generate stoichiometric matrices given a
%   list of chemical reactions and a corresponding list of species
%
% [nu_reac,nu_prod,rxns_tb] = generateStoichiometricMatrices(reactions,spec_list);
%
% Inputs:
%   reactions   -   cell of strings where each entry represents a chemical
%                   reaction. The reactants are supplied to the left of the
%                   ascii equilibrium double arrow '<->'; the products are
%                   to the right of the double arrow
%
%                   the number of distinct species supplied in reactions
%                   is denoted N_spec
%                   the number of distinct chemical reactions supplied in
%                   reactions is denoted N_reac
%
%                   it is assumed that the lack of a coefficient on a
%                   chemical species is equivalent to a coefficient of one.
%
%                   user input requires separation of chemical reactants,
%                   products, and the double arrow with whitespace to
%                   distinguish distinct parts of the syntax
%
%                   a third-body (e.g., in a dissociation reaction) is
%                   denoted with a capital 'M'. this function will handle
%                   this string input specially, so that M matches any of
%                   the species listed in spec_list. in other words,
%                   an input reaction with M on both sides of the
%                   double arrow will be replaced by length(spec_list)
%                   strings representing the corresponding third-body
%                   reactions, ordered identically to that of spec_list.
%                   the string 'M' must be placed as the final reactant
%                   and product in the reaction
%
%   spec_list   -   cell of strings where each entry represents a species
%                   considered in the chemical reactions supplied
%                   the order of the columns in the output matrices
%                   nu_reac, nu_prod are ordered according to that
%                   prescribed in spec_list
%
% Outputs:
%   nu_reac     -   matrix of reactant stoichimetric coefficients
%                   corresponding to chemical reactions supplied by the
%                   user. matrix is size (N_reac x N_spec)
%
%   nu_prod     -   matrix of product stoichimetric coefficients
%                   corresponding to chemical reactions supplied by the
%                   user. matrix is size (N_reac x N_spec)
%
%   rxns_tb     -   cell array of strings whose entries will be the
%                   reactions inputted by the user with third-body M's
%                   replaced by their literal strings from spec_list.
%
% Examples:
% a)
%   reactions = {...
%               'O + N   <->  O+ + e- + N',...
%               'O2 + M  <->  2O + M',...
%               'NO + O  <->  N + O2',...
%               'O + N2  <->  N + NO',...
%               };
%   spec_list = {'N','O','NO','N2','O2','O+','e-'};
%   [nu_reac,nu_prod] = generateStoichiometricMatrices(reactions,spec_list)
%       nu_reac =
%       [1, 1, 0, 0, 0, 0, 0;
%        1, 0, 0, 0, 1, 0, 0;
%        0, 1, 0, 0, 1, 0, 0;
%        0, 0, 1, 0, 1, 0, 0;
%        0, 0, 0, 1, 1, 0, 0;
%        0, 0, 0, 0, 2, 0, 0;
%        0, 0, 0, 0, 1, 1, 0;
%        0, 0, 0, 0, 1, 0, 1;
%        0, 1, 1, 0, 0, 0, 0;
%        0, 1, 0, 1, 0, 0, 0;];
%
%       nu_prod =
%       [1, 0, 0, 0, 0, 1, 1;
%        1, 2, 0, 0, 0, 0, 0;
%        0, 3, 0, 0, 0, 0, 0;
%        0, 2, 1, 0, 0, 0, 0;
%        0, 2, 0, 1, 0, 0, 0;
%        0, 2, 0, 0, 1, 0, 0;
%        0, 2, 0, 0, 0, 1, 0;
%        0, 2, 0, 0, 0, 0, 1;
%        1, 0, 0, 0, 1, 0, 0;
%        1, 0, 1, 0, 0, 0, 0;];
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

% account for third-body reactions
jj = 1;
for ii=1:length(reactions)
    rxn = regexp(reactions{ii},'<->','split');
    if length(rxn)>2; error('Incorrect input format for ''reactions'', see help generateStoichiometricMatrices'); end
    reac = rxn{1}; prod = rxn{2};
    % Check if third-body 'M' is surrounded by white space and/or terminating
    exprTb  = '\s+M\s*$?';
    bReacTb = ~isempty(regexp(reac,exprTb,'once'));
    bProdTb = ~isempty(regexp(prod,exprTb,'once'));
    if bReacTb && ~bProdTb; error('Third-body ''M'' appeared on only one side of the reaction!'); end
    if bReacTb && bProdTb % if reaction contains a third-body
        % replace it with N_spec reactions, where M is replaced by spec_list{kk}
        for kk=1:length(spec_list)
            rxns_tb{jj} = regexprep(reactions{ii},exprTb,[' ',spec_list{kk},' ']); %#ok<AGROW>
            jj=jj+1;
        end
    else
        rxns_tb{jj} = reactions{ii};
        jj = jj+1;
    end
end

N_reac = length(rxns_tb);
N_spec = length(spec_list);

nu_reac = zeros([N_reac, N_spec]);
nu_prod = zeros([N_reac, N_spec]);

for ii=1:N_reac
    rxn = regexp(rxns_tb{ii},'<->','split');
    reac = rxn{1}; prod = rxn{2};
    nu_reac(ii,:) = rxn2coeffs(reac,spec_list);
    nu_prod(ii,:) = rxn2coeffs(prod,spec_list);
end

end % generateStoichiometricMatrices.m

function nu_row = rxn2coeffs(rxn,spec_list)
%RXN2COEFFS Generate a row of the stoichiometric coefficients for one side
% of a chemical reaction
%
% GNU Lesser General Public License 3.0

% Extract species
% Regular expression: match letters followed by any number of +'s or -'s
spec_rxn = regexp(rxn,'\w+[\+-]*','match');

% Regular expression: positive look ahead
% If followed by \w characters, match the integer coefficient in front
mNums = regexp(spec_rxn,'([1-9]+)(?=\w+)','match');
mLogical = cellfun(@isempty,mNums);
mNums(mLogical)={{'1'}};
mCoeff = str2double([mNums{:}]);

% Match coefficients with order designated in spec_list
nu_row = zeros([1,length(spec_list)]);

% Escape any + characters to accomodate regular expression syntax
spec_list = regexprep(spec_list,'+','\\\+');

% Remove coefficients on spec_rxn for ease of comparison to spec_list
% Regular expression: positive look ahead
% If followed by \w characters, replace the integer coefficient in front
% with the empty string
spec_rxn = regexprep(spec_rxn,'([1-9]+)(?=\w+)','');

for ii=1:length(spec_list)
    % Find indices where a given species in spec_list is found in spec_rxn
    % Regular expression: match species name exactly
    mMatch = ~cellfun(@isempty,regexp(spec_rxn,['^',spec_list{ii},'$'],'match'));
    if nnz(mMatch)
        nu_row(1,ii) = sum(mCoeff(mMatch)); % accounts for repeated species
    end
end

end % rxn2coeffs.m
