function bIonAssoc = identifyReac_ionassoc(rxns,spec_list)
% identifyReac_ionassoc Construct a boolean vector identifying which
% reactions in a cell of reactions describe associative ionization.
%
% Input:
%   rxns        cell of strings where each entry represents a chemical
%               reaction. The reactants are supplied to the left of the
%               ascii equilibrium double arrow '<->'; the products are
%               to the right of the double arrow
%
%               it is assumed that the lack of a coefficient on a
%               chemical species is equivalent to a coefficient of one.
%
%               user input requires separation of chemical reactants,
%               products, and the double arrow with whitespace to
%               distinguish distinct parts of the syntax
%
%   spec_list   cell of strings where each entry represents a species
%               considered in the chemical reactions supplied
%               the order of the columns in the output matrices
%               nu_reac, nu_prod are ordered according to that
%               prescribed in spec_list
%
% Output:
%   bIonAssoc   vector of booleans of length(rxns). The ith element is true
%               if and only if the reaction described by rxns{i} is an
%               associative ionization reaction.
%
% Notes:
%   An associative ionization reaction, also known as Penning ionization,
% is defined as the interaction between two neutral species A and B,
% resulting in an ionized molecule AB+ and an electron.
%
% The reaction contains zero electrons in the reactants, and one electron
% in the products. The neutral species of the reactants combine and form a
% form a cation in the products.
%
% Example:
%   rxns = {...
%               'O + N   <->  O+ + e- + N',...
%               'O2 + e- <->  O2+ + e- + e-',...
%               'O + N   <->  NO+ + e-',...
%               'NO + e- <->  N + O + e-',...
%               'NO + M  <->  NO+ + e- + M',...
%               'N2 + e- <->  N2+ + 2e-',...
%               'O2+ + M <->  O2++ + e- + M',...
%               'N2 + O  <->  NO2+ + e-',...
%               'O2 + O  <->  O3+ + e-',...
%               };
%   spec_list = {'N','O','NO','N2','O2',...
%                'N+','O+','NO+','N2+','O2+','e-'};
%   bIonAssoc = identifyReac_ionassoc(rxns)
%   bIonAssoc = [...
%               0;
%               0;
%               1;
%               0;
%               0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0;
%               0;
%               0; 0; 0; 0; 0; 0; 0; 0; 0; 0; 0;
%               1;
%               1;
%               ];
%
% IMPORTANT: this function cannot detect associative second ionization.
% Eg.: A+ + B <-> AB++ + e-
%
% See also identifyReac_ionelim.m, GenerateStoichiometricMatrices.m
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% Initialize vector of booleans
bIonAssoc = [];
for ii=1:length(rxns)
    % Assume input rxns is correct format as specified in the description
    % Split the reaction by the delimiter, <->
    rxn = regexp(rxns{ii},'<->','split');
    reacs = split_reacSide(rxn{1});                             % splitting individual reactants and products
    prods = split_reacSide(rxn{2});
    reacAtoms = getAtoms_molecule(reacs);                       % splitting molecules in reactants into atoms
    targetMol = [getMolecule_atoms([reacAtoms{:}].'),'+'];      % the product must contain this molecule (appending of all reactants and positively charged)
    if ismember('M',reacs) && ismember('M',prods)               % third-body reaction
        bIonAssoc = [bIonAssoc;false(length(spec_list),1)];
    elseif ismember(targetMol,prods) && ismember('e-',prods)    % we have the associated ion and an electron in the products
        bIonAssoc = [bIonAssoc;true];                               % this is what we look for
    else                                                        % anything else
        bIonAssoc = [bIonAssoc;false];                              % is not what we look for
    end
end

end % identifyReac_ionassoc.m
