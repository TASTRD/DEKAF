function [bPos,bNeg] = getSpecies_bPosNeg(spec_list)
%GETBOOL_BPOSNEG Generate booleans for positive and negative species
% in a list of species
%
% Usage
% [bPos,bNeg] = getSpecies_bPosNeg(spec_list);
%
% Inputs and Outputs
%   spec_list       cell array of strings whose entries represent the
%                   list of species in the mixture, length N_spec
%
%   bPos            boolean array. an entry is true if positively-charged
%                   N_spec x 1
%   bNeg            boolean array. an entry is true if negatively-charged
%                   N_spec x 1
%
% Example
%   spec_list = {'O+','O2+','e-','N','O'};
%   [bPos,bNeg] = getSpecies_bPosNeg(spec_list);
%
%   bPos =
%
%       5×1 logical array
%
%       1
%       1
%       0
%       0
%       0
%
%
%   bNeg =
%
%       5×1 logical array
%
%       0
%       0
%       1
%       0
%       0
%
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

bPos = ~cellfun(@isempty,regexp(spec_list,'+','once'))';
bNeg = ~cellfun(@isempty,regexp(spec_list,'-','once'))';

end % getSpecies_bPosNeg
