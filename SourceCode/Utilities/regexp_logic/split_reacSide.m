function spec_list = split_reacSide(reac_side)
% split_reacSide splits one of the sides of a reaction into it's separate
% components.
%
% Examples:
%   (1)
%       split_reacSide('N + O + e-')
%       ans = {'N','O','e-'}
%
%   (2)
%       split_reacSide('3N + 2O2+ + e-')
%       ans = {'N','N','N','O2+','O2+','e-'}
%
% Author: Fernando Miro Miro
% Date: May 2019
% GNU Lesser General Public License 3.0

parts = regexp(reac_side,'[\ ]+\+[\ ]+','split');       % splitting at the + signs with spaces before and after
parts = regexprep(parts,'\ ','');                       % removing blanks
spec_list = {};                                         % initializing output
for ii=1:length(parts)                                  % looping parts of the reaction side
    num_str = regexp(parts{ii},'^[0-9]+','match');          % finding leading number
    rest_str = regexprep(parts{ii},'^([0-9]*)(.*)$','$2');  % removing leading number
    if isempty(num_str)                                     % there was no leading number
        spec_list = [spec_list,rest_str];                       % we simply append the species name
    else                                                    % there was a leading number
        num = str2double(num_str);                              % making string into double
        spec_list = [spec_list,repmat({rest_str},[1,num])];     % we append the species name repeated as many times as number
    end

end

end % split_reacSide