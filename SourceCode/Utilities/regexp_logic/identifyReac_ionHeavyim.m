function bIonHeavyim = identifyReac_ionHeavyim(rxns,spec_list)
% identifyReac_ionHeavyim Construct a boolean vector identifying which
% reactions in a cell of reactions describe ionization by heavy-particle
% impact.
%
% Input:
%   rxns        cell of strings where each entry represents a chemical
%               reaction. The reactants are supplied to the left of the
%               ascii equilibrium double arrow '<->'; the products are
%               to the right of the double arrow
%
%               it is assumed that the lack of a coefficient on a
%               chemical species is equivalent to a coefficient of one.
%
%               user input requires separation of chemical reactants,
%               products, and the double arrow with whitespace to
%               distinguish distinct parts of the syntax
%
%   spec_list   cell of strings where each entry represents a species
%               considered in the chemical reactions supplied
%               the order of the columns in the output matrices
%               nu_reac, nu_prod are ordered according to that
%               prescribed in spec_list
%
% Output:
%   bIonHeavyim    vector of booleans of length(rxns). The ith element is true
%               if and only if the reaction described by rxns{i} is an
%               ionization by heavy-particle impact.
%
% Notes:
%   An ionization heavy impact reaction is defined as an ionization
% method in which energetic heavy particles interact with solid or gas phase
% atoms or molecules to produce ions. The reaction contains no electrons in
% the reactants, and one electron in the products.
%
% Example:
%   rxns = {...
%               'O + N   <->  O+ + e- + N',...
%               'O2 + e- <->  O2+ + e- + e-',...
%               'O + N   <->  NO+ + e-',...
%               'NO + e- <->  N + O + e-',...
%               'NO + M  <->  NO+ + e- + M',...
%               'N2 + e- <->  N2+ + 2e-',...
%               'O2+ + M <->  O2++ + e- + M',...
%               };
%   spec_list = {'N','O','NO','N2','O2',...
%                'N+','O+','NO+','N2+','O2+','e-'};
%   bIonHeavyim = identifyReac_ionHeavyim(rxns,spec_list)
%   bIonHeavyim = [...
%               1;
%               0;
%               0;
%               0;
%               1; 1; 1; 1; 1; 1; 1; 1; 1; 1; 0
%               0;
%               1; 1; 1; 1; 1; 1; 1; 1; 1; 1; 0
%               ];
%
% See also identifyReac_ionassoc.m, GenerateStoichiometricMatrices.m,
% findIonizRxns
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% looking for ionization reactions
[idxIoniz,idxTB] = findIonizImpactRxns(rxns);

% Find 'e-' in the list of species.
bElectron = ismember(spec_list, 'e-');

% Initialize
N_spec = length(spec_list);
N_reac = length(rxns);
bIonHeavyim = false(N_reac + nnz(idxTB)*(N_spec-1),1); % Subtract one from N_spec to not double-count
jj = 0;

for ii = 1:length(rxns)
    if idxTB(ii)                                % it has a third body
        bIonHeavyim(jj+1:jj+N_spec) = idxIoniz(ii) & ~bElectron; % it will be true for the electron species and ionization reactions, false otherwise
        jj = jj + N_spec;                           % increase reaction index counter by N_spec
    else                                        % the reaction does not have a third body
        rxn = regexp(rxns{ii},'<->','split');       % separating into reactants and products
        if idxIoniz(ii) && ...                      % it is a stand-alone ionization reaction (like 'C2 + e-  <->  C2+ + 2e-')
           isempty(regexp(rxn{1},'e\-','match'))   % ... and does not contains an electron in the reactants (colliding) (like 'C2 + N  <->  C2+ + N + e-')
            bIonHeavyim(jj+1) = true;                       % it is a dissociation-by-electron-impact reaction
        end
        jj = jj + 1;                                % increase reaction index counter by 1.
    end
end

end % identifyReac_ionHeavyim.m
