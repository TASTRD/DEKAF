function [comb_list1,comb_list2] = getAtoms_combinations(atom_list)
% getAtoms_combinations returns all possible molecules as the result of
% combinations of a list of atoms.
%
% Only combinations resulting in the division of the list of atoms into two
% groups are considered. Three-way divisions of molecules are excluded,
% since such a dissociation is extraordinarily rare (Scoggins Thesis)
%
% Examples:
%   (1)
%       [comb_list1,comb_list2] = getAtoms_combinations({'O','O','C','N'})
%           comb_list1 = {'C',  'CN','CNO','CO','CO2','N',  'NO','NO2','O',  'O2'}
%           comb_list2 = {'NO2','O2','O',  'NO','N',  'CO2','CO','C',  'CNO','CN'}
%
%   (2)
%       [comb_list1,comb_list2] = getAtoms_combinations({'Ca','O','Ca','He'})
%           comb_list1 = {'Ca',   'Ca2','Ca2He','Ca2O','CaHe','CaHeO','CaO' ,'He',  'HeO','O'}
%           comb_list2 = {'CaHeO','HeO','O',    'He',  'CaO', 'Ca',   'CaHe','Ca2O','Ca2','Ca2He'}
%
% Author: Fernando Miro Miro
% Date: May 2019
% GNU Lesser General Public License 3.0

atom_list = sort(atom_list);                                        % sorting into alphabetical order
Natoms =  length(atom_list);                                        % number of atoms

ic=1;                                                               % index counter
comb_all1 = {};         comb_all2 = {};                             % allocating for eventual atomic species
for ii=1:Natoms-1                                                   % looping all possible number of atoms in a group
    idx_comb_ii = nchoosek(1:Natoms,ii);                                % index of all combinations with ii elements
    for jj=1:size(idx_comb_ii,1)                                        % looping combinations
        bidx_notComb = ~ismember(1:Natoms,idx_comb_ii(jj,:));               % indices of those atoms that are not in the loop combination
        comb_all1{ic} = getMolecule_atoms(atom_list(idx_comb_ii(jj,:)));    % assembling atoms in the combination
        comb_all2{ic} = getMolecule_atoms(atom_list(bidx_notComb));         % assembling atoms that are not in the combination
        ic=ic+1;                                                            % increasing index counter
    end
end

[comb_list1,idx_keep] = unique(comb_all1);                          % removing duplicities
comb_list2 = comb_all2(idx_keep);

end % getAtoms_combinations