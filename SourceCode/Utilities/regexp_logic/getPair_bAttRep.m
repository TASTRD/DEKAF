function [bAtt,bRep,bCharged] = getPair_bAttRep(spec_list)
%GETBOOL_BATTREP Create lists of attracted and repulsed pairs of
% species from a list of species in a mixture
%
% Usage
%   [bAtt,bRep,bCharged] = getPair_bAttRep(spec_list);
%
% Inputs and Outputs
%   spec_list       cell array of strings whose entries represent the
%                   list of species in the mixture
%
%   bAtt            cell array of strings whose entries are the attractive
%                   interactions (by way of electrostatic forces)
%   bRep            cell array of strings whose entries are the repulsive
%                   interactions (by way of electrostatic forces)
%   bCharged        boolean matrix. the (i,j)th entry is true if the (s,l)
%                   interaction of spec_list involve both electronically-
%                   charged species
%                   N_spec x N_spec
%
% Examples
%
%   spec_list = {'O+','O2+','e-','N','O'};
%   [bAtt,bRep,bCharged] = getPair_bAttRep(spec_list);
%
%   bAtt = {'O+ - e-','e- - O+','O2+ - e-','e- - O2+'};
%   bRep = {'O+ - O+','O2+ - O2+','e- - e-',...
%               'O+ - O2+','O2+ - O+'};
%   bCharged =
%
%       5×5 logical array
%
%       1   1   1   0   0
%       1   1   1   0   0
%       1   1   1   0   0
%       0   0   0   0   0
%       0   0   0   0   0
%
% Related functions:
% See also getPair_Collision_constants.m
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

N_spec = length(spec_list);

[bPos,bNeg] = getSpecies_bPosNeg(spec_list);

bCharged = getPair_bCharged(bPos,bNeg);

space = repmat({32}, [N_spec,N_spec]); % 32: ascii character code for space
spec_list_rows  = repmat(spec_list,[N_spec,1]);
spec_list_cols  = repmat(spec_list',[1,N_spec]);
pairs           = strcat(spec_list_rows,' -',space,spec_list_cols);

bRep = [{pairs{bPos,bPos}},{pairs{bNeg,bNeg}}];
bAtt = [{pairs{bPos,bNeg}},{pairs{bNeg,bPos}}];

end % getPair_bAttRep.m
