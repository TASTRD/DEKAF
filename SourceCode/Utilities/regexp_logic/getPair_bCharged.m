function bCharged = getPair_bCharged(bPos,bNeg)
%GETPAIR_BCHARGED Calculate the boolean of charge-charge interactions
% given the negatively-charged boolean and the positively-charged boolean
%
% Usage:
%   bCharged = getPair_bCharged(bPos,bNeg);
%
% Examples
%
%   spec_list = {'O+','O2+','e-','N','O'};
%   [bPos,bNeg] = getSpecies_bPosNeg(spec_list);
%   bCharged = getPair_bCharged(bPos,bNeg)
%   bCharged =
%
%       5×5 logical array
%
%       1   1   1   0   0
%       1   1   1   0   0
%       1   1   1   0   0
%       0   0   0   0   0
%       0   0   0   0   0
%
% Related functions:
% See also getSpecies_bPosNeg
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

bCharged1D  = bPos + bNeg;
bCharged    = logical(bCharged1D * bCharged1D');

end % getPair_bCharged
