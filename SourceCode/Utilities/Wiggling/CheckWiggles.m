function wiggliness = CheckWiggles(intel,fieldstr,coordstr,plotornot,varargin)
%CHECKWIGGLES(intel,fieldstr,coordstr,tol) checks wiggles in the freestream
% fieldstr is the string describing the field name you want to check the
%          freestream off
% coordstr is the string describing the coordinate field in intel
% tol indicates the width of the field of view w.r.t. the freestream value
%
% Author(s): Koen Groot
% GNU Lesser General Public License 3.0

% using the 2 one-but-outermost values to measure potential wiggliness
wiggliness = (intel.(fieldstr)(2)-intel.(fieldstr)(3))/max(abs(intel.(fieldstr)));

if plotornot
    disp(['--- Zig-zag pattern amplitude expected with amplitude: ',...
        num2str(wiggliness,'%.2e')])
end

if isempty(varargin)
    if wiggliness > eps
        tol = wiggliness;
    else
        tol = eps;
    end
else
    tol = varargin{1};
end

if plotornot
    figure

    plot(intel.(fieldstr),intel.(coordstr))

    if diff(intel.(fieldstr)(1)+[-tol tol]) == 0
    axis([intel.(fieldstr)(1)*(1+[-tol tol]) 0 intel.(coordstr)(1)])
    else
    axis([intel.(fieldstr)(1)+[-tol tol] 0 intel.(coordstr)(1)])
    end
end

end % CheckWiggles.m
