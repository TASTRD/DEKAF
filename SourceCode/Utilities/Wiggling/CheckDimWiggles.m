function CheckDimWiggles(intel,options)
%CheckNonDimWiggles(intel,options) Checks all non-dimensional variables for
% wiggles larger than the

if options.Cooke
fieldnames = {'u','du_dy','du_dy2','v',...
    'w','dw_dy','dw_dy2',...
    'h','dh_dy','dh_dy2',...
    'T','rho','mu','htot'};
else
fieldnames = {'u','du_dy','du_dy2','v',...
    'h','dh_dy','dh_dy2',...
    'T','rho','mu','htot'};
end
% note: f itself is excluded from the wiggle test

for i = 1:length(fieldnames)
    wiggleamplitude = CheckWiggles(intel,fieldnames{i},'y',0);
    if wiggleamplitude>options.tol
        warning(['!!! The variable ',fieldnames{i},' contains supertolerance (',num2str(wiggleamplitude,'%.2e'),') wiggles, please apply restraint'])
    end
end
end

