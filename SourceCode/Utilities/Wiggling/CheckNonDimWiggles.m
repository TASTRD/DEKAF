function CheckNonDimWiggles(intel,options)
%CheckNonDimWiggles(intel,options) Checks all non-dimensional variables for
% wiggles larger than the

if options.Cooke
fieldnames = {'df_deta','df_deta2','df_deta3',...
    'k','dk_deta','dk_deta2','g','dg_deta','dg_deta2',...
    'C','j','a1','dj_dg'};
else
fieldnames = {'df_deta','df_deta2','df_deta3',...
    'g','dg_deta','dg_deta2',...
    'C','j','a1','dj_dg'};
end
% note: f itself is excluded from the wiggle test

for i = 1:length(fieldnames)
    wiggleamplitude = CheckWiggles(intel,fieldnames{i},'eta',0);
    if wiggleamplitude>options.tol
        warning(['!!! The variable ',fieldnames{i},' contains supertolerance (',num2str(wiggleamplitude,'%.2e'),') wiggles'])
    end
end

end % CheckNonDimWiggles.m

