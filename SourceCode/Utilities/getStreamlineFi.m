function fi = getStreamlineFi(U_e, W_0, marchYesNo)
%GETSTREAMLINEFI Build the relative angle of the streamlines with respect
% to the x direction (aka, fi)

% U_e will equal zero for non-shearing stagnation flows
% We need to handle this case manually, as atand2() breaks down for its
% denominator equal to zero.
if all(U_e ~= 0)
    fi = atan2d(W_0,U_e);
else
    idx_stag = U_e == 0;
    fi(idx_stag) = sign(W_0)*90;
    if marchYesNo
        fi(~idx_stag) = atan2d(W_0,U_e(~idx_stag));
    end
end

end % getStreamlineFi