function tau_TransEl = getMixture_tauTransEl(y_s,TTrans,Tel,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11)
% getMixture_tauTransEl returns the relaxation time of exchanges between
% the heavy-particle and the electron translational energy.
%
% Usage:
%   (1)
%       tau_TransEl = getMixture_tauTransEl(y_s,TTrans,Tel,rho,Mm_s,R0,nAvogadro,bPos,...
%                                           bElectron,epsilon0,kBoltz,qEl,consts_Omega11)
%
% Inputs and outputs:
%   y_s                 [-]
%       species mass fraction (size N_eta x N_spec)
%   TTrans              [K]
%       translational temperature of heavy species (size N_eta x 1)
%   Tel                 [K]
%       translational temperature of electron (size N_eta x 1)
%   rho                 [kg/m^3]
%       mixture density (size N_eta x 1)
%   Mm_s                [kg/mol]
%       species molar mass (size N_spec x 1)
%   R0           [J/mol-K]
%       Universal gas constant (size 1 x 1)
%   nAvogadro           [-]
%       Avogadro's number (size 1 x 1)
%   bPos                [-]
%       positively-charged-species boolean (size N_spec x 1)
%   bElectron           [-]
%       electron boolen (size N_spec x 1)
%   epsilon0            [C^2/N-m^2 ]
%       permittivity of vacuum (size 1 x 1)
%   kBoltz              [m^2-kg/K-s^2]
%       Boltzmann constant (size 1 x 1)
%   qEl                 [C]
%       elementary charge (size 1 x 1)
%   consts_Omega11      [-]
%       struct with fields .A, .B, .C, .D, .E, .F for the curve fit for
%       collision integral (1,1). Each field has size N_spec x N_spec
%   tau_TransEl       [s]
%       relaxation time of exchanges between the heavy-particle and the
%       electron translational energy
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

lnOmega11_sl = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega11,bPos);
n_s = getSpecies_n(rho,y_s,Mm_s,nAvogadro); % species number density

% obtaining heavy/electron properties
Omega11_Hel = exp(lnOmega11_sl(:,~bElectron,bElectron)); % collisions between heavy particles and electrons
Mm_el       = Mm_s(bElectron);          % molar mass of electrons
Mm_H        = Mm_s(~bElectron);         % molar mass of heavy particles
n_H         = n_s(:,~bElectron);        % number density of heavy particles

% reshaping
N_heavy = nnz(~bElectron);              % number of heavy particles
N_eta   = length(Tel);                  % number of computational points
Mm_H    = repmat(Mm_H.',[N_eta,1]);     % (H,1) --> (eta,H)
Tel     = repmat(Tel,[1,N_heavy]);      % (eta,1) --> (eta,H)

% computing relaxation time
tau_TransElInv = (8/3 * Mm_el./Mm_H .* n_H .* sqrt(8*R0*Tel/(pi*Mm_el)) .* Omega11_Hel)*ones(N_heavy,1);
tau_TransEl = tau_TransElInv.^(-1);

end % getMixture_tauTransEl