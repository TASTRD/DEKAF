function Qel = getMixture_Qel(y_s,TTrans,Tel,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,source_sr,thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc)
% getMixture_Qel computes the electron energy source term in a
% two-temperature system
%
% Usage:
%   (1)
%       Qel = getMixture_Qel(y_s,TTrans,Tel,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,...
%                   consts_Omega11,source_sr,thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc)
%
% Inputs and outputs:
%   y_s                 [-]
%       species mass fraction (size N_eta x N_spec)
%   TTrans              [K]
%       translational temperature of heavy species (size N_eta x 1)
%   Tel                 [K]
%       translational temperature of electron (size N_eta x 1)
%   rho                 [kg/m^3]
%       mixture density (size N_eta x 1)
%   Mm_s                [kg/mol]
%       species molar mass (size N_spec x 1)
%   R0           [J/mol-K]
%       Universal gas constant (size 1 x 1)
%   nAvogadro           [-]
%       Avogadro's number (size 1 x 1)
%   bPos                [-]
%       positively-charged-species boolean (size N_spec x 1)
%   bElectron           [-]
%       electron boolen (size N_spec x 1)
%   epsilon0            [C^2/N-m^2 ]
%       permittivity of vacuum (size 1 x 1)
%   kBoltz              [m^2-kg/K-s^2]
%       Boltzmann constant (size 1 x 1)
%   qEl                 [C]
%       elementary charge (size 1 x 1)
%   consts_Omega11      [-]
%       struct with fields .A, .B, .C, .D, .E, .F for the curve fit for
%       collision integral (1,1). Each field has size N_spec x N_spec
%   source_sr           [kg/s]
%       mass source term of each species s due to each reaction r
%       (size N_eta x N_spec x N_reac)
%   thetaVib_s          [K]
%       activation temperature for the vibrational energy mode m of each
%       species s (size N_spec x N_m)
%   hIon_s              [J/kg]
%       ionization enthalpy of each species s (size N_spec x 1)
%   bMol                [-]
%       molecular-species boolean (size N_spec x 1)
%   bIonelim            [-]
%       boolean for reactions in the category "ionization by electron
%       impact" (size N_reac x 1)
%   bIonHeavyim         [-]
%       boolean for reactions in the category "ionization by heavy-particle
%       impact" (size N_reac x 1)
%   bDissel             [-]
%       boolean for reactions in the category "dissociation by electron
%       impact" (size N_reac x 1)
%   bIonAssoc           [-]
%       boolean for reactions in the category "ionization by association"
%       (size N_reac x 1)
%   Qel                 [J/kg-s]
%       electron energy source term in a two-temperature system
%       (size N_eta x 1)
%
% See also: listOfVariablesExplained, getMixture_sourcev
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

% obtaining numbers of reactions, species, etc.
[N_eta,~]       = size(y_s);                    % number of species and computational points
N_ion           = nnz(bPos);                    % number of ion species
N_mol           = nnz(bMol);                    % number of molecular species
N_hpiaii        = nnz(bIonHeavyim|bIonAssoc);   % number of heavy-particle-impact- or associative-ionization reactions
N_eii           = nnz(bIonelim);                % number of electron-impact-ionization reactions
N_eid           = nnz(bDissel);                 % number of electron-impact-dissociation reactions

% auxiliary variables
rho_el = y_s(:,bElectron) .* rho; % electron partial density
Mm_el = Mm_s(bElectron); % electron molar mass
hDiss_s = getSpecies_hVib(Tel,R0./Mm_s,thetaVib_s,'notRepmatted'); % species dissociation-activation enthalpy (equal to vibrational enthalpy)

% mass source terms of each group of reactions
source_hpiaii   = reshape(source_sr(:,bElectron,bIonHeavyim|bIonAssoc),[N_eta,N_hpiaii]);
source_eii      = source_sr(:,:,bIonelim);
source_eid      = source_sr(:,:,bDissel);

% energy source terms of each contribution
Qel_hpiaii  = 3/2*R0*Tel./Mm_el .* (source_hpiaii*ones(N_hpiaii,1));

hIon_s2D        = reshape(repmat(hIon_s(bPos).',[N_eta,1,N_eii]) , [N_eta,N_eii*N_ion]);
source_eii2D    = reshape(source_eii(:,bPos,:)                   , [N_eta,N_eii*N_ion]);
Qel_eii     = -(source_eii2D.*hIon_s2D) * ones(N_eii*N_ion,1);

hDiss_s2D       = reshape(repmat(hDiss_s(:,bMol),[1,1,N_eid]) , [N_eta,N_eid*N_mol]);
source_eii2D    = reshape(source_eid(:,bMol,:)                , [N_eta,N_eid*N_mol]);
Qel_eid     = -(source_eii2D.*hDiss_s2D) * ones(N_eid*N_mol,1);

tau_TransEl = getMixture_tauTransEl(y_s,TTrans,Tel,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11);
Qel_TransEl = 3*rho_el*R0/Mm_el.*(TTrans - Tel)./tau_TransEl;

% summing all contributions
Qel = Qel_hpiaii + Qel_eii + Qel_eid + Qel_TransEl;

end % getMixture_Qel