function [sourcev,tauVibMW_s,tauVibP_s,hVibEq_s] = getMixture_sourcev(TTrans,TVib,TElec,Tel,y_s,p,rho,source_s,Mm_s,thetaVib_s,thetaElec_s,gDegen_s,R_s,...
                                                                aVib_sl,bVib_sl,sigmaVib_s,bMol,bElectron,R0,nAvogadro,pAtm,source_sr,hIon_s,kBoltz,qEl,...
                                                                epsilon0,consts_Omega11,bPos,bIonelim,bIonHeavyim,bDissel,bIonAssoc,options,varargin)
% getMixture_sourcev computes the vib-elec-el energy source term
%
% Usage:
%   (1)
%       sourcev = getMixture_sourcev(TTrans,TVib,TElec,Tel,y_s,p,rho,source_s,Mm_s,thetaVib_s,thetaElec_s,gDegen_s,R_s,...
%                                     aVib_sl,bVib_sl,sigmaVib_s,bMol,bElectron,R0,nAvogadro,pAtm,source_sr,hIon_s,kBoltz,qEl,...
%                                     epsilon0,consts_Omega11,bPos,bIonelim,bIonHeavyim,bDissel,bIonAssoc,options)
%   (2)
%       sourcev = getMixture_sourcev(...,'noQFormv')
%       |---> doese not include the chemical formation vibrational source
%       term
%
% Inputs:
%   TTrans          N_eta x 1
%   TVib            N_eta x 1
%   TElec           N_eta x 1
%   Tel             N_eta x 1
%   y_s             N_eta x N_spec
%   p               N_eta x 1
%   rho             N_eta x 1
%   source_s        N_eta x 1
%   Mm_s            N_spec x 1
%   thetaVib_s      N_spec x Nvib
%   thetaElec_s     N_spec x N_stat
%   gDegen_s        N_spec x N_stat
%   R_s             N_spec x 1
%   aVib_sl         N_spec x N_spec (species s relaxing due to species l)
%   bVib_sl         N_spec x N_spec (species s relaxing due to species l)
%   sigmaVib_s      N_spec x 1
%   bMol            N_spec x 1
%   bElectron       N_spec x 1
%   R0              1 x 1
%   nAvogadro       1 x 1
%   pAtm            1 x 1
%
% See also: listOfVariablesExplained, getMixture_QTransRot2Vib, getMixture_QFormv
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,bNoQForm] = find_flagAndRemove('noQFormv',varargin);

% Vibrational relaxation
[QTransRot2Vib,tauVibMW_s,tauVibP_s,hVibEq_s] = getMixture_QTransRot2Vib(TTrans,TVib,Tel,y_s,p,rho,Mm_s,thetaVib_s,R_s,...
                            aVib_sl,bVib_sl,sigmaVib_s,bMol,bElectron,R0,nAvogadro,pAtm,options);

% Enthalpies due to the formation of species
if bNoQForm;    QFormv = 0;
else;           QFormv = getMixture_QFormv(TVib,TElec,Tel,source_s,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options);
end

% Electron source term
if any(bElectron);  Qel = getMixture_Qel(y_s,TTrans,Tel,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,...
                                consts_Omega11,source_sr,thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc);
else;               Qel = 0;
end


% assembling source term
sourcev = QTransRot2Vib + QFormv + Qel;