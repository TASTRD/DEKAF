function [aVib_sl,bVib_sl,sigmaVib_s] = getPair_Vibrational_constants(spec_list,bMol,bElectron,Mm_s,thetaVib_s,nAvogadro,options)
% getPair_Vibrational_constants returns the constants driving the
% vibrational relaxation of the different pairs of molecule species.
%
% Usage:
%   (1)
%       [aVib_sl,bVib_sl,sigmaVib_s] = getPair_Vibrational_constants(spec_list,...
%                           bMol,bElectron,Mm_s,thetaVib_s,nAvogadro,options)
%
% options.referenceVibRelax must contain a string identifying what
% reference to use to obtain the vibrational relaxation constants.
% Supported values are:
%       'Park93'        - tabulated, the same as in mutation
%       'Mortensen2013' - uses original expressions proposed by Millikan
%                       and White, based on Mm_s, and thetaVib_s
%
% Inputs:
%   spec_list
%       cell of strings containing the different species in the mixture
%       (N_spec x 1)
%   bMol
%       positive boolean: true for molecule species, false for atomic or
%       electron species. (N_spec x 1)
%   bElectron
%       positive boolean: true for electron species, false for other
%       species. (N_spec x 1)
%   Mm_s
%       species molar mass [kg/mol] (N_spec x 1)
%   thetaVib_s
%       species vibrational activation temperature [K] (N_spec x N_vib)
%   nAvogadro
%       Avogadro's number (1 x 1)
%   options
%       classic DEKAF options structure
%
% Outputs:
%   aVib_sl
%       Millikan & White's a constant for the vibrational relaxation times
%       (N_spec x N_spec)
%   bVib_sl
%       Millikan & White's b constant for the vibrational relaxation times
%       (N_spec x N_spec)
%   sigmaVib_s
%       Park's sigma cross-section for the vibrational relaxation times
%       (N_spec x 1)
%
% References:
%   Millikan, R. C., & White, D. R. (1963). Systematics of vibrational
% relaxation. The Journal of Chemical Physics, 39(12), 3209–3213.
% https://doi.org/10.1063/1.1734182
%   Park, C. (1993). Review of chemical-kinetic problems of future NASA
% missions. I - Earth entries. Journal of Thermophysics and Heat Transfer,
% 7(3), 385--398. https://doi.org/10.2514/3.496
%   Park, C., Howe, J. T., Jaffe, R. L., & Candler, G. V. (1994). Review of
% chemical-kinetic problems of future NASA missions. II - Mars entries.
% Journal of Thermophysics and Heat Transfer, 8(1), 9–23.
% https://doi.org/10.2514/3.496
%
% See also: setDefaults
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_spec      = length(spec_list);
N_mol       = nnz(bMol);
idx_mol     = find(bMol); % indices of the molecular species
N_heavy     = nnz(~bElectron);
idx_heav     = find(~bElectron); % indices of the heavy species

mu_sl = nAvogadro*getPair_redMass(Mm_s,nAvogadro) * 1e3; % computing reduced mass
% mu_sl = getPair_redMass(Mm_s,nAvogadro) * 1e3; % computing reduced mass

aVib_sl = zeros(N_spec,N_spec); % initializing
bVib_sl = zeros(N_spec,N_spec);
sigmaVib_s = getSpecies_sigmaVib(spec_list,bMol,options);

for m=1:N_mol % looping molecular species
    s = idx_mol(m);
    for lh=1:N_heavy % looping species
        l = idx_heav(lh);
        pair = [spec_list{s},' - ',spec_list{l}];
        switch options.referenceVibRelax
            case 'Park93'
                switch pair
                    %%%% N2 RELAXATION
                    case 'N2 - N' % Park 1993
                        aVib_sl(s,l) = 180;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0262;           % [K^(-1/3)]
                    case 'N2 - N+' % Park 1993
                        aVib_sl(s,l) = 180;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0262;           % [K^(-1/3)]
                    case 'N2 - O' % Park 1993
                        aVib_sl(s,l) = 72.4;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.015;            % [K^(-1/3)]
                    case 'N2 - O+' % Park 1993
                        aVib_sl(s,l) = 72.4;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.015;            % [K^(-1/3)]
                    case 'N2 - N2' % Park 1993
                        aVib_sl(s,l) = 221;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0290;           % [K^(-1/3)]
                    case 'N2 - N2+' % Park 1993
                        aVib_sl(s,l) = 221;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0290;           % [K^(-1/3)]
                    case 'N2 - O2' % Park 1993
                        aVib_sl(s,l) = 229;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0295;           % [K^(-1/3)]
                    case 'N2 - O2+' % Park 1993
                        aVib_sl(s,l) = 229;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0295;           % [K^(-1/3)]
                    case 'N2 - NO' % Park 1993
                        aVib_sl(s,l) = 225;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0293;           % [K^(-1/3)]
                    case 'N2 - NO+' % Park 1993
                        aVib_sl(s,l) = 225;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0293;           % [K^(-1/3)]
                    case 'N2 - C' % Park 1994
                        aVib_sl(s,l) = 72.4;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.015;            % [K^(-1/3)]
                    case 'N2 - CO' % Park 1994
                        aVib_sl(s,l) = 221;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.029;            % [K^(-1/3)]
                    case 'N2 - CO2' % Park 1994
                        aVib_sl(s,l) = 245;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0305;           % [K^(-1/3)]

                        %%%% N2+ RELAXATION
                    case 'N2+ - N' % mutation precomputed values (??)
                        aVib_sl(s,l) = 170;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0262;           % [K^(-1/3)]
                    case 'N2+ - N+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 170;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0262;           % [K^(-1/3)]
                    case 'N2+ - O' % mutation precomputed values (??)
                        aVib_sl(s,l) = 178;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0268;           % [K^(-1/3)]
                    case 'N2+ - O+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 178;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0268;           % [K^(-1/3)]
                    case 'N2+ - N2' % mutation precomputed values (??)
                        aVib_sl(s,l) = 209;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0290;           % [K^(-1/3)]
                    case 'N2+ - N2+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 209;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0290;           % [K^(-1/3)]
                    case 'N2+ - O2' % mutation precomputed values (??)
                        aVib_sl(s,l) = 216;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0295;           % [K^(-1/3)]
                    case 'N2+ - O2+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 216;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0295;           % [K^(-1/3)]
                    case 'N2+ - NO' % mutation precomputed values (??)
                        aVib_sl(s,l) = 212;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0293;           % [K^(-1/3)]
                    case 'N2+ - NO+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 212;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0293;           % [K^(-1/3)]

                        %%%% O2 RELAXATION
                    case 'O2 - N' % Park 1993
                        aVib_sl(s,l) = 72.4;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.015;            % [K^(-1/3)]
                    case 'O2 - N+' % Park 1993
                        aVib_sl(s,l) = 72.4;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.015;            % [K^(-1/3)]
                    case 'O2 - O' % Park 1993
                        aVib_sl(s,l) = 47.7;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.059;            % [K^(-1/3)]
                    case 'O2 - O+' % Park 1993
                        aVib_sl(s,l) = 47.7;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.059;            % [K^(-1/3)]
                    case 'O2 - N2' % Park 1993
                        aVib_sl(s,l) = 134;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0295;           % [K^(-1/3)]
                    case 'O2 - N2+' % Park 1993
                        aVib_sl(s,l) = 134;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0295;           % [K^(-1/3)]
                    case 'O2 - O2' % Park 1993
                        aVib_sl(s,l) = 138;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.030;            % [K^(-1/3)]
                    case 'O2 - O2+' % Park 1993
                        aVib_sl(s,l) = 138;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.030;            % [K^(-1/3)]
                    case 'O2 - NO' % Park 1993
                        aVib_sl(s,l) = 136;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0298;           % [K^(-1/3)]
                    case 'O2 - NO+' % Park 1993
                        aVib_sl(s,l) = 136;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0298;           % [K^(-1/3)]

                        %%%% O2+ RELAXATION
                    case 'O2+ - N' % mutation precomputed values (??)
                        aVib_sl(s,l) = 148;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0265;           % [K^(-1/3)]
                    case 'O2+ - N+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 148;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0265;           % [K^(-1/3)]
                    case 'O2+ - O' % mutation precomputed values (??)
                        aVib_sl(s,l) = 155;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0271;           % [K^(-1/3)]
                    case 'O2+ - O+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 155;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0271;           % [K^(-1/3)]
                    case 'O2+ - N2' % mutation precomputed values (??)
                        aVib_sl(s,l) = 184;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0295;           % [K^(-1/3)]
                    case 'O2+ - N2+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 184;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0295;           % [K^(-1/3)]
                    case 'O2+ - O2' % mutation precomputed values (??)
                        aVib_sl(s,l) = 190;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.030;            % [K^(-1/3)]
                    case 'O2+ - O2+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 190;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.030;            % [K^(-1/3)]
                    case 'O2+ - NO' % mutation precomputed values (??)
                        aVib_sl(s,l) = 187;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0298;           % [K^(-1/3)]
                    case 'O2+ - NO+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 187;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0298;           % [K^(-1/3)]

                        %%%% NO RELAXATION (all relaxers assumed to be the same as for NO-NO)
                    case 'NO - N' % Park 1993
                        aVib_sl(s,l) = 49.5;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.042;            % [K^(-1/3)]
                    case 'NO - N+' % Park 1993
                        aVib_sl(s,l) = 49.5;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.042;            % [K^(-1/3)]
                    case 'NO - O' % Park 1993
                        aVib_sl(s,l) = 49.5;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.042;            % [K^(-1/3)]
                    case 'NO - O+' % Park 1993
                        aVib_sl(s,l) = 49.5;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.042;            % [K^(-1/3)]
                    case 'NO - N2' % Park 1993
                        aVib_sl(s,l) = 49.5;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.042;            % [K^(-1/3)]
                    case 'NO - N2+' % Park 1993
                        aVib_sl(s,l) = 49.5;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.042;            % [K^(-1/3)]
                    case 'NO - O2' % Park 1993
                        aVib_sl(s,l) = 49.5;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.042;            % [K^(-1/3)]
                    case 'NO - O2+' % Park 1993
                        aVib_sl(s,l) = 49.5;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.042;            % [K^(-1/3)]
                    case 'NO - NO' % Park 1993
                        aVib_sl(s,l) = 49.5;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.042;            % [K^(-1/3)]
                    case 'NO - NO+' % Park 1993
                        aVib_sl(s,l) = 49.5;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.042;            % [K^(-1/3)]

                        %%%% NO+ RELAXATION
                    case 'NO+ - N' % mutation precomputed values (??)
                        aVib_sl(s,l) = 188;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0264;           % [K^(-1/3)]
                    case 'NO+ - N+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 188;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0264;           % [K^(-1/3)]
                    case 'NO+ - O' % mutation precomputed values (??)
                        aVib_sl(s,l) = 197;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0270;           % [K^(-1/3)]
                    case 'NO+ - O+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 197;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0270;           % [K^(-1/3)]
                    case 'NO+ - N2' % mutation precomputed values (??)
                        aVib_sl(s,l) = 231;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0293;           % [K^(-1/3)]
                    case 'NO+ - N2+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 231;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0293;           % [K^(-1/3)]
                    case 'NO+ - O2' % mutation precomputed values (??)
                        aVib_sl(s,l) = 239;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0298;           % [K^(-1/3)]
                    case 'NO+ - O2+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 239;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0298;           % [K^(-1/3)]
                    case 'NO+ - NO' % mutation precomputed values (??)
                        aVib_sl(s,l) = 236;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0295;           % [K^(-1/3)]
                    case 'NO+ - NO+' % mutation precomputed values (??)
                        aVib_sl(s,l) = 236;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0295;           % [K^(-1/3)]

                        %%%% CO RELAXATION
                    case 'CO - Ar' % Park 1994
                        aVib_sl(s,l) = 215;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.0302;           % [K^(-1/3)]
                    case 'CO - N' % Park 1994
                        aVib_sl(s,l) = 47.7;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.05;             % [K^(-1/3)]
                    case 'CO - O' % Park 1994
                        aVib_sl(s,l) = 47.7;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.05;             % [K^(-1/3)]
                    case 'CO - C' % Park 1994
                        aVib_sl(s,l) = 47.7;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.05;             % [K^(-1/3)]
                    case 'CO - N2' % Park 1994
                        aVib_sl(s,l) = 198;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.029;            % [K^(-1/3)]
                    case 'CO - CO' % Park 1994
                        aVib_sl(s,l) = 198;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.029;            % [K^(-1/3)]
                    case 'CO - CO2' % Park 1994
                        aVib_sl(s,l) = 218;              % [K^(1/3)]
                        bVib_sl(s,l) = 0.305;            % [K^(-1/3)]

                        %%%% CO2 RELAXATION
                    case 'CO2 - Ar' % Park 1994
                        aVib_sl(s,l) = 50.3;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.0321;           % [K^(-1/3)]
                    case 'CO2 - N' % Park 1994
                        aVib_sl(s,l) = 35.8;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.0271;           % [K^(-1/3)]
                    case 'CO2 - O' % Park 1994
                        aVib_sl(s,l) = 37.6;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.0278;           % [K^(-1/3)]
                    case 'CO2 - C' % Park 1994
                        aVib_sl(s,l) = 33.7;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.0263;           % [K^(-1/3)]
                    case 'CO2 - N2' % Park 1994
                        aVib_sl(s,l) = 45.4;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.0305;           % [K^(-1/3)]
                    case 'CO2 - CO' % Park 1994
                        aVib_sl(s,l) = 45.4;             % [K^(1/3)]
                        bVib_sl(s,l) = 0.0305;           % [K^(-1/3)]
                    case 'CO2 - CO2' % Park 1994
                        aVib_sl(s,l) = 36.5;             % [K^(1/3)]
                        bVib_sl(s,l) = -0.0193;          % [K^(-1/3)]
                    otherwise
                        error(['the chosen pair ''',pair,''' is not supported. Change options.referenceVibRelax=''Mortensen2013'' to obtain vibrational rerlaxation constants from the mass fractions and vibrational activation temperatures.']);
                end
            case 'Mortensen2013' % Eq. 2.25 - 2.27 of Mortensen's thesis
                aVib_sl(s,l) = 1.16e-3 * sqrt(mu_sl(s,l)) * thetaVib_s(s,1)^(4/3);
                bVib_sl(s,l) = 0.015 * mu_sl(s,l)^(1/4);
            otherwise
                error(['Error the chosen reference for the vibrational relaxation constants ''',options.referenceVibRelax,''' is not supported']);
        end
    end
end

end % getPair_Vibrational_constants