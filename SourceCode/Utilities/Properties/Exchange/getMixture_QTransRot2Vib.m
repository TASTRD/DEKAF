function [QTransRot2Vib,tauVibMW_s,tauVibP_s,hVibEq_s] = getMixture_QTransRot2Vib(TTrans,TVib,Tel,y_s,p,rho,Mm_s,thetaVib_s,R_s,...
                            aVib_sl,bVib_sl,sigmaVib_s,bMol,bElectron,R0,nAvogadro,pAtm,options)
% getMixture_QTransRot2Vib computes the vibrational energy source term due
% to the relaxation of energy from the translational-rotational modes to
% the vibrational using Millikan & White's correlations.
%
% Usage:
%   (1)
%       QTransRot2Vib = getMixture_QTransRot2Vib(TTrans,TVib,Tel,y_s,p,rho,Mm_s,thetaVib_s,...
%                   R_s,aVib_sl,bVib_sl,sigmaVib_s,bMol,bElectron,R0,nAvogadro,pAtm,options)
%
% Inputs:
%   TTrans          N_eta x 1
%   TVib            N_eta x 1
%   Tel             N_eta x 1
%   y_s             N_eta x N_spec
%   p               N_eta x 1
%   rho             N_eta x 1
%   Mm_s            N_spec x 1
%   thetaVib_s      N_spec x Nvib
%   R_s             N_spec x 1
%   aVib_sl         N_spec x N_spec (species s relaxing due to species l)
%   bVib_sl         N_spec x N_spec (species s relaxing due to species l)
%   sigmaVib_s      N_spec x 1
%   bMol            N_spec x 1
%   bElectron       N_spec x 1
%   R0              1 x 1
%   nAvogadro       1 x 1
%   pAtm            1 x 1
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% analyzing inputs
[N_eta,N_spec] = size(y_s);
Nvib = size(thetaVib_s,2);

% reshaping and repmatting
TTrans_mat = repmat(TTrans,[1,N_spec]);                                     % (eta,1) --> (eta,s)
TVib_mat = repmat(TVib,[1,N_spec]);                                         % (eta,1) --> (eta,s)
R_s_mat         = repmat(R_s.',[N_eta,1]);                                  % (eta,1) --> (eta,s)
thetaVib_s_mat  = repmat(permute(thetaVib_s,[3,1,2]),[N_eta,1,1]);          % (s,v) --> (eta,s,v)

% computing necessary terms
rho_s       = repmat(rho,[1,N_spec]).*y_s;                                  % species densities
hVib_s      = getSpecies_hVib(TVib_mat,R_s_mat,thetaVib_s_mat);             % species vibrational enthalpy
hVibEq_s    = getSpecies_hVib(TTrans_mat,R_s_mat,thetaVib_s_mat);           % species equilibrium vibrational enthalpy
tauVibMW_s  = getSpecies_tauVibMW(TTrans,y_s,p,Mm_s,aVib_sl,bVib_sl,bElectron,pAtm); % Millikan & White's relaxation time
if options.bParkCorrectionTauVib
    tauVibP_s   = getSpecies_tauVibP(TTrans,Tel,y_s,p,Mm_s,bElectron,sigmaVib_s,R0,nAvogadro); % Park's correction for high temperatures
else
    tauVibP_s = zeros(N_eta,N_spec);
end

% Computing source term
addend = rho_s.*(hVibEq_s-hVib_s)./(tauVibMW_s+tauVibP_s);
addend(:,~bMol) = 0;                                                        % vibrational energy is exclusive to molecular species
QTransRot2Vib = addend*ones(N_spec,1);