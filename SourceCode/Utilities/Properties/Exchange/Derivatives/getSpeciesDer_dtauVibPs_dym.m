function dtauVibPs_dym = getSpeciesDer_dtauVibPs_dym(TTrans,Tel,y_s,p,Mm_s,bElectron,sigmaVib_s,R0,nAvogadro)
% getSpeciesDer_dtauVibPs_dym computes the derivative wrt each of the
% system's temperatures of the correction of the species vibrational
% relaxation time proposed by Park for high temperatures.
%
% Usage:
%   (1)
%       dtauVibPs_dym = getSpeciesDer_dtauVibPs_dym(TTrans,Tel,y_s,p,Mm_s,bElectron,...
%                                          sigmaVib_s,R0,nAvogadro)
%
% Inputs & outputs:
%   TTrans          N_eta x 1
%   Tel             N_eta x 1
%   y_s             N_eta x N_spec
%   p               N_eta x 1
%   Mm_s            N_spec x 1
%   bElectron       N_spec x 1
%   sigmaVib_s      N_spec x 1
%   R0              1 x 1
%   nAvogadro       1 x 1
%   dtauVibPs_dym   N_eta x N_spec(m) x N_spec(s)
%
% FIXME: THIS FUNCTION IS INVALID FOR NON-IDEAL EQUATIONS OF STATE
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,N_spec] = size(y_s);

% computing intermediate variables
tauVibP_s = getSpecies_tauVibP(TTrans,Tel,y_s,p,Mm_s,bElectron,sigmaVib_s,R0,nAvogadro); % (eta,s)
rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);                                % (eta,1)
drho_dym = getMixtureDer_drho_dym(p,y_s,R0,Mm_s,bElectron,TTrans,Tel);                   % (eta,m)
n_s = getSpecies_n(rho,y_s,Mm_s,nAvogadro);                                              % (eta,s)
dns_dym = getSpeciesDer_dns_dym(y_s,drho_dym,rho,nAvogadro,Mm_s);                        % (eta,m,s)

% reshaping and repmatting
tauVibP_s_3D = repmat(permute(tauVibP_s,[1,3,2]),[1,N_spec,1]);             % (eta,s) --> (eta,m,s)
n_s_3D = repmat(permute(n_s,[1,3,2]),[1,N_spec,1]);                         % (eta,s) --> (eta,m,s)

dtauVibPs_dym = -tauVibP_s_3D./n_s_3D .* dns_dym; % there is some not obvious algebra involved. See notes

end % getSpeciesDer_dtauVibPs_dym