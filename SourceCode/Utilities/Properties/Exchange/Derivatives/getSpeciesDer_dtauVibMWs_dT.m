function dtauVibMWs_dT = getSpeciesDer_dtauVibMWs_dT(TTrans,y_s,p,Mm_s,aVib_sl,bVib_sl,bElectron,pAtm,numberOfTemp)
% getSpeciesDer_dtauVibMWs_dT computes the derivative wrt temperature of
% the vibrational relaxation time of the molecular species using Millikan &
% White's correlation.
%
% Usage:
%   (1)
%       dtauVibMWs_dT = getSpeciesDer_dtauVibMWs_dT(TTrans,y_s,p,Mm_s,aVib_sl,...
%                                           bVib_sl,bElectron,pAtm,numberOfTemp)
%
% Inputs & Outputs:
%   TTrans          N_eta x 1
%   y_s             N_eta x N_spec
%   p               N_eta x 1
%   Mm_s            N_spec x 1
%   aVib_sl         N_spec x N_spec (species s relaxing due to species l)
%   bVib_sl         N_spec x N_spec (species s relaxing due to species l)
%   bElectron       N_spec x 1
%   pAtm            1 x 1
%   numberOfTemp    string identifying the number of temperatures in the model
%   dtauVibMWs_dT   N_eta x N_spec x N_T
%
% See also: listOfVariablesExplained, setDefaults
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% analyzing inputs, reshaping and repmatting
[N_eta,N_spec]  = size(y_s);
N_heavy         = nnz(~bElectron);
idx_heavy       = find(~bElectron);
TTrans_3D       = repmat(TTrans,[1,N_spec,N_spec]);
p_3D            = repmat(p,[1,N_spec,N_spec]);
aVib_sl         = repmat(reshape(aVib_sl,[1,N_spec,N_spec]),[N_eta,1,1]);
bVib_sl         = repmat(reshape(bVib_sl,[1,N_spec,N_spec]),[N_eta,1,1]);

% computing required terms
Mm      = getMixture_Mm_R(y_s,TTrans,TTrans,Mm_s,1,bElectron);              % dummies are passed for Tel & R0(not needed for Mm)
X_s     = getSpecies_X(y_s,Mm,Mm_s);
tau_sl  = pAtm./p_3D .* exp(aVib_sl.*(TTrans_3D.^(-1/3) - bVib_sl) - 18.42);
dtausl_dT  = - pAtm./p_3D .* aVib_sl/3.*TTrans.^(-4/3) .* exp(aVib_sl.*(TTrans_3D.^(-1/3) - bVib_sl) - 18.42);

% taking heavy species only
X_H     = X_s(:,~bElectron);                                                % mole fraction of heavy particles
X_s3D   = repmat(reshape(X_s,[N_eta,1,N_spec]),[1,N_spec,1]);               % (eta,l) --> (eta,s,l)
X_allH2D = repmat(X_H*ones(N_heavy,1),[1,N_spec]);                          % sum of the mole frac. of all heavies
tau_sH  = tau_sl(:,:,~bElectron);
dtausH_dT  = dtausl_dT(:,:,~bElectron);                                           % removing electrons

% computing relaxation time
addend1 = zeros(N_eta,N_spec); % allocating
addend2 = zeros(N_eta,N_spec);
for ll=1:N_heavy % looping species
    l = idx_heavy(ll);
    addend1 = addend1 + X_s3D(:,:,l)./tau_sH(:,:,l); % suming relaxation time of each relaxing pair
    addend2 = addend2 + X_s3D(:,:,l)./tau_sH(:,:,l).^2.*dtausH_dT(:,:,l); % suming relaxation time of each relaxing pair
end
dtauVibMWs_dT = X_allH2D./addend1.^2 .* addend2;

switch numberOfTemp
    case '2T'
        dtauVibMWs_dT(:,:,2) = 0; % the derivative wrt the vibrational temperature is zero
    otherwise
        error(['the chosen number of temperatures ''',numberOfTemp,''' is not supported']);
end

end % getSpeciesDer_dtauVibMWs_dT