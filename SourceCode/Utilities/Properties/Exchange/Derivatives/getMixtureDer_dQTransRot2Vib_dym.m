function dQTransRot2Vib_dym = getMixtureDer_dQTransRot2Vib_dym(TTrans,TVib,Tel,y_s,p,rho,pAtm,R0,...
                                    nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,...
                                    R_s,bElectron,thetaVib_s,options)
% getMixtureDer_dQTransRot2Vib_dym computes the derivatives of the species
% vibrational source term due to the relaxation of energy from the
% translational-rotational modes to the vibrational, with respect to the
% different mass fractions
%
% Usage:
%   (1)
%       dQTransRot2Vib_dym = getMixtureDer_dQTransRot2Vib_dym(TTrans,TVib,Tel,y_s,p,rho,pAtm,...
%                                   R0,nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,bMol,...
%                                   Mm_s,R_s,bElectron,thetaVib_s,options)
%
% Inputs and Outputs:
%   TTrans              N_eta x 1
%   TVib                N_eta x 1
%   Tel                 N_eta x 1
%   y_s                 N_eta x N_spec
%   p                   N_eta x 1
%   rho                 N_eta x 1
%   pAtm                1 x 1
%   R0                  1 x 1
%   nAvogadro           1 x 1
%   aVib_sl             N_spec x N_spec
%   bVib_sl             N_spec x N_spec
%   sigmaVib_s          N_spec x 1
%   bMol                N_spec x 1
%   Mm_s                N_spec x 1
%   R_s                 N_spec x 1
%   bElectron           N_spec x 1
%   thetaVib_s          N_spec x Nvib
%   dQTransRot2Vib_dym  N_eta x N_spec
%
% See also: getMixtureDer_dQTransRot2Vib_dT, listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);
N_mol = nnz(bMol);

% computing enthalpies and derivatives (heat capacities)
TTrans_mat = repmat(TTrans,[1,N_spec]);                                     % (eta,1) --> (eta,s)
TVib_mat = repmat(TVib,[1,N_spec]);                                         % (eta,1) --> (eta,s)
R_s_mat         = repmat(R_s.',[N_eta,1]);                                  % (eta,1) --> (eta,s)
thetaVib_s_mat  = repmat(permute(thetaVib_s,[3,1,2]),[N_eta,1,1]);          % (s,v) --> (eta,s,v)
hVib_s      = getSpecies_hVib(TVib_mat,R_s_mat,thetaVib_s_mat);             % species vibrational enthalpy
hVibEq_s  = getSpecies_hVib(TTrans_mat,R_s_mat,thetaVib_s_mat);             % species equilibrium vibrational enthalpy

% Computing other variables and derivatives
drho_dym = getMixtureDer_drho_dym(p,y_s,R0,Mm_s,bElectron,TTrans,Tel); % (eta,m)
tauVibMW_s  = getSpecies_tauVibMW(TTrans,y_s,p,Mm_s,aVib_sl,bVib_sl,bElectron,pAtm);       % (eta,s)
if options.bParkCorrectionTauVib
tauVibP_s   = getSpecies_tauVibP(TTrans,Tel,y_s,p,Mm_s,bElectron,sigmaVib_s,R0,nAvogadro); % (eta,s)
else
    tauVibP_s = zeros(N_eta,N_spec);
end
tauVib_s = tauVibMW_s + tauVibP_s;
dtauVibMWs_dym = getSpeciesDer_dtauVibMWs_dym(TTrans,y_s,p,Mm_s,aVib_sl,bVib_sl,bElectron,pAtm); % (eta,m,s)
if options.bParkCorrectionTauVib
    dtauVibPs_dym = getSpeciesDer_dtauVibPs_dym(TTrans,Tel,y_s,p,Mm_s,bElectron,sigmaVib_s,R0,nAvogadro); % (eta,m,s)
else
    dtauVibPs_dym = zeros(N_eta,N_spec,N_spec);
end
dtauVibs_dym = dtauVibMWs_dym + dtauVibPs_dym;

% Reshaping and repmatting for loop (eta,s,m)
dtauVibs_dym = permute(dtauVibs_dym,[1,3,2]);                               % (eta,m,s) --> (eta,s,m)
rho_2D = repmat(rho,[1,N_mol]);                                             % (eta,1) --> (eta,s)

% retaining only molecular species
dtauVibs_dym = dtauVibs_dym(:,bMol,:);
% NOTE: tauVib_s is only non-zero for molecular species, however, each of
% this tauVib_s depends on ALL y_m, therefore, dtauVib_dym for a
% non-molecular m species is NOT ZERO.
tauVib_s = tauVib_s(:,bMol);
hVibEq_s = hVibEq_s(:,bMol);
hVib_s = hVib_s(:,bMol);
y_s = y_s(:,bMol);

% Computing derivative of the source term
addend1 = (y_s.*(hVibEq_s - hVib_s)./tauVib_s)*ones(N_mol,1);
dQTransRot2Vib_dym = zeros(N_eta,N_spec);
for m = 1:N_spec % looping derivand species
    addend2 = -rho_2D.*y_s.*(hVibEq_s - hVib_s)./tauVib_s.^2.*dtauVibs_dym(:,:,m);
    dQTransRot2Vib_dym(:,m) = drho_dym(:,m).*addend1 + addend2*ones(N_mol,1);
end
addend3 = rho_2D.*(hVibEq_s - hVib_s)./tauVib_s;                            % these subindices are actually m, not s
dQTransRot2Vib_dym(:,bMol) = dQTransRot2Vib_dym(:,bMol) + addend3;                      % adding additional term for the molecular species