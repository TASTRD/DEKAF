function dtauTransEl_dT = getMixtureDer_dtauTransEl_dT(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,numberOfTemp)
% getMixtureDer_dtauTransEl_dT computes the temperature derivatives of the
% relaxation time of energy transfer between the translational mode of
% heavy particles and electrons.
%
% Usage:
%   (1)
%       dtauTransEl_dT = getMixtureDer_dtauTransEl_dT(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,...
%                               bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,numberOfTemp)
%
% Inputs and outputs:
%   y_s                 [-]
%       species mass fraction (size N_eta x N_spec)
%   TTrans              [K]
%       translational temperature of heavy species (size N_eta x 1)
%   Tel                 [K]
%       translational temperature of electron (size N_eta x 1)
%   p                   [Pa]
%       mixture pressure (size N_eta x 1)
%   rho                 [kg/m^3]
%       mixture density (size N_eta x 1)
%   Mm_s                [kg/mol]
%       species molar mass (size N_spec x 1)
%   R0           [J/mol-K]
%       Universal gas constant (size 1 x 1)
%   nAvogadro           [-]
%       Avogadro's number (size 1 x 1)
%   bPos                [-]
%       positively-charged-species boolean (size N_spec x 1)
%   bElectron           [-]
%       electron boolen (size N_spec x 1)
%   epsilon0            [C^2/N-m^2 ]
%       permittivity of vacuum (size 1 x 1)
%   kBoltz              [m^2-kg/K-s^2]
%       Boltzmann constant (size 1 x 1)
%   qEl                 [C]
%       elementary charge (size 1 x 1)
%   consts_Omegaij      [-]
%       struct with fields .A, .B, .C, .D, .E, .F for the collision
%       integral curve fit. Each field has size N_spec x N_spec
%   numberOfTemp
%       string identifying the number of temperatures considered in the
%       system.
%   dtauTransEl_dT      [s/K]
%       temperature derivatives of the relaxation time of energy transfer
%       between the translational mode of heavy particles and electrons
%       (size N_eta x N_T).
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

% Obtaining sizes
[N_eta,N_spec] = size(y_s);
N_heavy = nnz(~bElectron);
switch numberOfTemp
    case '1T'
        N_T         = 1;        idxEl = NaN;
        drho_dT     = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,TTrans);            % (eta,T)
    case '2T'
        N_T         = 2;        idxEl = 2;
        drho_dT     = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,[TTrans,Tel],'2T'); % (eta,T)
    otherwise
        error(['the chosen numberOfTemp=''',numberOfTemp,''' is not supported']);
end
% idxEl keeps track of what temperature is the electron one, which has a different expression for the derivative

% Computing auxiliary quantities
tau_TransEl      = getMixture_tauTransEl(y_s,TTrans,Tel,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11);                   % (eta,1)
n_s              = getSpecies_n(rho,y_s,Mm_s,nAvogadro);                                                                                            % (eta,s)
dns_dT           = getSpeciesDer_dns_dT(y_s,drho_dT,nAvogadro,Mm_s);                                                                                % (eta,s,T)

lnOmega11_sl     = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega11,bPos);                          % (eta,s,l)
dlnOmega11_dT    = getPairDer_dlnOmegaij_dT(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,numberOfTemp); % (eta,s,l,T)
lnOmega11_sel    = lnOmega11_sl(:,:,bElectron);                                                                                                     % (eta,s)
dlnOmega11sel_dT = permute(dlnOmega11_dT(:,:,bElectron,:),[1,2,4,3]);                                                                               % (eta,s,T)
Omega11_sel      = exp(lnOmega11_sel);                                                                                                              % (eta,s)
dOmega11sel_dT   = repmat(Omega11_sel,[1,1,N_T]) .* dlnOmega11sel_dT; % dA/dq = A*dlnA/dq                                                             (eta,s,T)

% repmatting
Tel2D = repmat(Tel,[1,N_spec]);                 % (eta,1) --> (eta,s)
Mm_el = Mm_s(bElectron);                        % electron molar mass
Mm_H2D = repmat(Mm_s(~bElectron).',[N_eta,1]);  % heavy-particle molar mass

% assembling
for iT=N_T:-1:1
    bracket = dns_dT(:,:,iT).*Omega11_sel + n_s.*dOmega11sel_dT(:,:,iT);   % bracket appearing in the expression of the
    if iT==idxEl                                                            % for the electron temperature
        bracket = bracket + n_s.*Omega11_sel./(2*Tel2D);                        % including addend accounting for d/dTel of the Tel in sqrt
    end
    bracketH = bracket(:,~bElectron);                                       % keeping only heavy species
    dtauTransEl_dT(:,iT) = -tau_TransEl.^2.*sqrt(8*R0*Tel/(pi*Mm_el)) .* ((8/3*Mm_el./Mm_H2D .* bracketH)*ones(N_heavy,1));
end

end % getMixtureDer_dtauTransEl_dT