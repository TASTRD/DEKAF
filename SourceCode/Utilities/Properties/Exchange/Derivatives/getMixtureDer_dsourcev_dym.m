function dsourcev_dym = getMixtureDer_dsourcev_dym(TTrans,TVib,TElec,Tel,y_s,p,rho,dsources_dym,pAtm,R0,nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,R_s,...
                                        bElectron,thetaVib_s,thetaElec_s,gDegen_s,bPos,epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dym,hIon_s,...
                                        bIonelim,bIonHeavyim,bDissel,bIonAssoc,options,varargin)
% getMixtureDer_dsourcev_dym computes the derivatives of the species
% vib-elec-el source term with respect to the different temperatures
%
% Usage:
%   (1)
%       dsourcev_dym = getMixtureDer_dsourcev_dym(TTrans,TVib,TElec,Tel,y_s,p,rho,dsources_dym,pAtm,R0,nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,R_s,...
%                                         bElectron,thetaVib_s,thetaElec_s,gDegen_s,bPos,epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dym,hIon_s,...
%                                         bIonelim,bIonHeavyim,bDissel,bIonAssoc,options)
%   (2)
%       dsourcev_dym = getMixtureDer_dsourcev_dym(...,'noQFormv')
%       |---> doese not include the chemical formation vibrational source
%       term
%
% Inputs and Outputs:
%   TTrans              N_eta x 1
%   TVib                N_eta x 1
%   TElec               N_eta x 1
%   Tel                 N_eta x 1
%   y_s                 N_eta x N_spec
%   p                   N_eta x 1
%   rho                 N_eta x 1
%   dsources_dym        N_eta x N_spec(m) x N_spec(s)
%   pAtm                1 x 1
%   R0                  1 x 1
%   nAvogadro           1 x 1
%   aVib_sl             N_spec x N_spec
%   bVib_sl             N_spec x N_spec
%   sigmaVib_s          N_spec x 1
%   bMol                N_spec x 1
%   Mm_s                N_spec x 1
%   R_s                 N_spec x 1
%   bElectron           N_spec x 1
%   thetaVib_s          N_spec x Nvib
%   thetaElec_s         N_spec x N_stat
%   gDegen_s            N_spec x N_stat
%   bPos                N_spec x 1
%   epsilon0            1 x 1
%   qEl                 1 x 1
%   consts_Omega11      N_spec x N_spec
%   dsourcesr_dym       N_eta x N_spec(m) x N_spec(s) x N_reac
%   hIon_s              N_spec x 1
%   bIonelim            N_reac x 1
%   bIonHeavyim         N_reac x 1
%   bDissel             N_reac x 1
%   bIonAssoc           N_reac x 1
%   options             structure
%   dsourcev_dym        N_eta x N_spec
%
% See also: getMixtureDer_dsourcev_dT, listOfVariablesExplained,
% getMixtureDer_dQTransRot2Vib_dym, getMixtureDer_dQFormv_dym
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,bNoQForm] = find_flagAndRemove('noQFormv',varargin);

dQTransRot2Vib_dym = getMixtureDer_dQTransRot2Vib_dym(TTrans,TVib,Tel,y_s,p,rho,pAtm,R0,...
                                    nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,...
                                    R_s,bElectron,thetaVib_s,options);

if bNoQForm;    dQFormv_dym = 0;
else;           dQFormv_dym = getMixtureDer_dQFormv_dym(TVib,TElec,Tel,dsources_dym,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options);
end

if any(bElectron);  dQel_dym = getMixtureDer_dQel_dym(y_s,TTrans,Tel,rho,p,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dym,thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc);
else;               dQel_dym = 0;
end

dsourcev_dym = dQTransRot2Vib_dym + dQFormv_dym + dQel_dym;