function dQTransRot2Vib_dT = getMixtureDer_dQTransRot2Vib_dT(TTrans,TVib,Tel,y_s,p,rho,pAtm,R0,...
                                nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,R_s,...
                                bElectron,thetaVib_s,numberOfTemp,options)
% getMixtureDer_dQTransRot2Vib_dT computes the derivatives of the species
% vibrational source term due to the relaxation of energy from the
% translational-rotational modes to the vibrational, with respect to the
% different temperatures
%
% Usage:
%   (1)
%       dQTransRot2Vib_dT = getMixtureDer_dQTransRot2Vib_dT(TTrans,TVib,Tel,y_s,p,rho,...
%                           pAtm,R0,nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,...
%                           bMol,Mm_s,R_s,bElectron,thetaVib_s,numberOfTemp,options)
%
% Inputs and Outputs:
%   TTrans              N_eta x 1
%   TVib                N_eta x 1
%   Tel                 N_eta x 1
%   y_s                 N_eta x N_spec
%   p                   N_eta x 1
%   rho                 N_eta x 1
%   pAtm                1 x 1
%   R0                  1 x 1
%   nAvogadro           1 x 1
%   aVib_sl             N_spec x N_spec
%   bVib_sl             N_spec x N_spec
%   sigmaVib_s          N_spec x 1
%   bMol                N_spec x 1
%   Mm_s                N_spec x 1
%   R_s                 N_spec x 1
%   bElectron           N_spec x 1
%   thetaVib_s          N_spec x Nvib
%   numberOfTemp        string identifying the number of temperatures in the model
%   dQTransRot2Vib_dT         N_eta x N_T
%
% See also: getMixtureDer_dQTransRot2Vib_dym, listOfVariablesExplained,
% setDefaults
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);
N_mol = nnz(bMol);

% computing enthalpies and derivatives (heat capacities)
TTrans_mat = repmat(TTrans,[1,N_spec]);                                     % (eta,1) --> (eta,s)
TVib_mat = repmat(TVib,[1,N_spec]);                                         % (eta,1) --> (eta,s)
R_s_mat         = repmat(R_s.',[N_eta,1]);                                  % (eta,1) --> (eta,s)
thetaVib_s_mat  = repmat(permute(thetaVib_s,[3,1,2]),[N_eta,1,1]);          % (s,v) --> (eta,s,v)
[hVib_s,cvVib_s]      = getSpecies_hVib(TVib_mat,R_s_mat,thetaVib_s_mat);   % species vibrational enthalpy
[hVibEq_s,cvVibEq_s]  = getSpecies_hVib(TTrans_mat,R_s_mat,thetaVib_s_mat); % species equilibrium vibrational enthalpy

switch numberOfTemp
    case '2T'
        TGroup = [TTrans,TVib];
        dhVibEqs_dT   = cat(3, cvVibEq_s,  zeros(N_eta,N_spec));      % (eta,s,iT)
        dhVibs_dT     = cat(3, zeros(N_eta,N_spec),    cvVib_s);      % (eta,s,iT)
    otherwise
        error(['the selected thermal model ''',numberOfTemp,''' is not supported']);
end
N_T = size(TGroup,2);

% Computing other variables and derivatives
drho_dT = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,TGroup,numberOfTemp);                   % (eta,iT)
tauVibMW_s  = getSpecies_tauVibMW(TTrans,y_s,p,Mm_s,aVib_sl,bVib_sl,bElectron,pAtm);            % (eta,s)
if options.bParkCorrectionTauVib
    tauVibP_s   = getSpecies_tauVibP(TTrans,Tel,y_s,p,Mm_s,bElectron,sigmaVib_s,R0,nAvogadro); % (eta,s)
else
    tauVibP_s = zeros(N_eta,N_spec);
end
tauVib_s = tauVibMW_s + tauVibP_s;
dtauVibMWs_dT = getSpeciesDer_dtauVibMWs_dT(TTrans,y_s,p,Mm_s,aVib_sl,bVib_sl,bElectron,pAtm,numberOfTemp); % (eta,s,iT)
if options.bParkCorrectionTauVib
    dtauVibPs_dT = getSpeciesDer_dtauVibPs_dT(TTrans,Tel,y_s,p,Mm_s,bElectron,sigmaVib_s,R0,nAvogadro,numberOfTemp); % (eta,s,iT)
else
    dtauVibPs_dT = zeros(N_eta,N_spec,N_T);
end
dtauVibs_dT = dtauVibMWs_dT + dtauVibPs_dT;

% Reshaping and repmatting for loop (eta,s,iT)
drho_dT_3D = repmat(permute(drho_dT,[1,3,2]),[1,N_mol,1]); % (eta,iT) --> (eta,s,iT)
rho_2D = repmat(rho,[1,N_mol]); % (eta,1) --> (eta,s)

% retaining only molecular species
tauVib_s    = tauVib_s(:,bMol);
dtauVibs_dT = dtauVibs_dT(:,bMol,:);
hVibEq_s    = hVibEq_s(:,bMol);
hVib_s      = hVib_s(:,bMol);
dhVibEqs_dT = dhVibEqs_dT(:,bMol,:);
dhVibs_dT   = dhVibs_dT(:,bMol,:);
y_s         = y_s(:,bMol);

% Computing derivative of the source term
dQTransRot2Vib_dT = zeros(N_eta,N_T);
for iT = 1:N_T
    addend1 = drho_dT_3D(:,:,iT).*y_s.*(hVibEq_s-hVib_s)./tauVib_s;
    addend2 = rho_2D.*y_s.*(dhVibEqs_dT(:,:,iT) - dhVibs_dT(:,:,iT))./tauVib_s;
    addend3 = -rho_2D.*y_s.*(hVibEq_s-hVib_s)./tauVib_s.^2 .* dtauVibs_dT(:,:,iT);
    dQTransRot2Vib_dT(:,iT) = (addend1 + addend2 + addend3)*ones(N_mol,1);
end