function dtauVibPs_dT = getSpeciesDer_dtauVibPs_dT(TTrans,Tel,y_s,p,Mm_s,bElectron,sigmaVib_s,R0,nAvogadro,numberOfTemp)
% getSpeciesDer_dtauVibPs_dT computes the derivative wrt each of the
% system's temperatures of the correction of the species vibrational
% relaxation time proposed by Park for high temperatures.
%
% Usage:
%   (1)
%       dtauVibPs_dT = getSpeciesDer_dtauVibPs_dT(TTrans,Tel,y_s,p,Mm_s,bElectron,...
%                                          sigmaVib_s,R0,nAvogadro,numberOfTemp)
%
% Inputs & outputs:
%   TTrans          N_eta x 1
%   Tel             N_eta x 1
%   y_s             N_eta x N_spec
%   p               N_eta x 1
%   Mm_s            N_spec x 1
%   bElectron       N_spec x 1
%   sigmaVib_s      N_spec x 1
%   R0              1 x 1
%   nAvogadro       1 x 1
%   numberOfTemp    string identifying the number of temperatures in the model
%   dtauVibPs_dT    N_eta x N_spec x N_T
%
% FIXME: THIS FUNCTION IS INVALID FOR NON-IDEAL EQUATIONS OF STATE
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);

switch numberOfTemp
    case '2T'
        TGroup = [TTrans,Tel];
        TTransInv_only = [1./TTrans,zeros(N_eta,1)]; % term used in computing the derivatives
    otherwise
        error(['the chosen number of temperatures ''',numberOfTemp,''' is not supported']);
end

N_T = size(TGroup,2);

% computing intermediate variables
tauVibP_s = getSpecies_tauVibP(TTrans,Tel,y_s,p,Mm_s,bElectron,sigmaVib_s,R0,nAvogadro); % (eta,s)
rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);                                % (eta,1)
drho_dT = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,TGroup,numberOfTemp);            % (eta,iT)
n_s = getSpecies_n(rho,y_s,Mm_s,nAvogadro);                                              % (eta,s)
dns_dT = getSpeciesDer_dns_dT(y_s,drho_dT,nAvogadro,Mm_s);                               % (eta,s,iT)
sqrt_term = sqrt(8*R0./(pi*Mm_s));                                                       % (s,1)

% reshaping and repmatting
TTrans_3D = repmat(TTrans,[1,N_spec,N_T]);                                  % (eta,1) --> (eta,s,iT)
TTransInvOnly_3D = repmat(permute(TTransInv_only,[1,3,2]),[1,N_spec,1]);    % (eta,iT) --> (eta,s,iT)
sqrt_term_3D = repmat(sqrt_term.',[N_eta,1,N_T]);                           % (s,1) --> (eta,s,iT)
sigmaVib_s_3D = repmat(sigmaVib_s.',[N_eta,1,N_T]);                         % (s,1) --> (eta,s,iT)
tauVibP_s_3D = repmat(tauVibP_s,[1,1,N_T]);                                 % (eta,s) --> (eta,s,iT)
n_s_3D = repmat(n_s,[1,1,N_T]);                                             % (eta,s) --> (eta,s,iT)

sigma_s     = sigmaVib_s_3D.*(50e3./TTrans_3D).^2;                          % (eta,s,iT)
dsigmas_dT  = -2*sigmaVib_s_3D.*(50e3)^2 .* TTransInvOnly_3D.^3;            % (eta,s,iT)

addend1 = dns_dT .* sigma_s .* sqrt_term_3D .* sqrt(TTrans_3D);
addend2 = n_s_3D/2 .* sigma_s .* sqrt_term_3D .* sqrt(TTransInvOnly_3D);    % only non-zero for d()/dTTrans
addend3 = n_s_3D .* dsigmas_dT .* sqrt_term_3D .* sqrt(TTrans_3D);          % only non-zero for d()/dTTrans
dtauVibPs_dT = -tauVibP_s_3D.^2 .* (addend1 + addend2 + addend3);

end % getSpeciesDer_dtauVibPs_dT