function dsourcev_dT = getMixtureDer_dsourcev_dT(TTrans,TVib,TElec,Tel,y_s,p,rho,source_s,dsources_dT,pAtm,R0,nAvogadro,...
                            aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,R_s,bElectron,thetaVib_s,thetaElec_s,gDegen_s,bPos,...
                            epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dT,source_sr,hIon_s,bIonelim,bIonHeavyim,...
                            bDissel,bIonAssoc,numberOfTemp,options,varargin)
% getMixtureDer_dsourcev_dT computes the derivatives of the species
% vib-elec-el source term with respect to the different temperatures
%
% Usage:
%   (1)
%       dsourcev_dT = getMixtureDer_dsourcev_dT(TTrans,TVib,TElec,Tel,y_s,p,rho,source_s,dsources_dT,pAtm,R0,nAvogadro,...
%                             aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,R_s,bElectron,thetaVib_s,thetaElec_s,gDegen_s,bPos,...
%                             epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dT,source_sr,hIon_s,bIonelim,bIonHeavyim,...
%                             bDissel,bIonAssoc,numberOfTemp,options)
%   (2)
%       dsourcev_dT = getMixtureDer_dsourcev_dT(...,'noQFormv')
%       |---> doese not include the chemical formation vibrational source
%       term
%
% Inputs and Outputs:
%   TTrans              N_eta x 1
%   TVib                N_eta x 1
%   TElec               N_eta x 1
%   Tel                 N_eta x 1
%   y_s                 N_eta x N_spec
%   p                   N_eta x 1
%   rho                 N_eta x 1
%   source_s            N_eta x N_spec
%   dsources_dT         N_eta x N_spec x N_T
%   pAtm                1 x 1
%   R0                  1 x 1
%   nAvogadro           1 x 1
%   aVib_sl             N_spec x N_spec
%   bVib_sl             N_spec x N_spec
%   sigmaVib_s          N_spec x 1
%   bMol                N_spec x 1
%   Mm_s                N_spec x 1
%   R_s                 N_spec x 1
%   bElectron           N_spec x 1
%   thetaVib_s          N_spec x Nvib
%   thetaElec_s         N_spec x N_stat
%   gDegen_s            N_spec x N_stat
%   bPos                N_spec x 1
%   epsilon0            1 x 1
%   qEl                 1 x 1
%   consts_Omega11      N_spec x N_spec
%   dsourcesr_dT        N_eta x N_spec x N_T x N_reac
%   source_sr           N_eta x N_spec x N_reac
%   hIon_s              N_spec x 1
%   bIonelim            N_reac x 1
%   bIonHeavyim         N_reac x 1
%   bDissel             N_reac x 1
%   bIonAssoc           N_reac x 1
%   numberOfTemp        string identifying the number of temperatures in the model
%   options             structure
%   dsourcev_dT         N_eta x N_T
%
% See also: getMixtureDer_dsourcev_dym, listOfVariablesExplained,
% setDefaults, getMixtureDer_dQTransRot2Vib_dT, getMixtureDer_dQFormv_dT
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,bNoQForm] = find_flagAndRemove('noQFormv',varargin);

dQTransRot2Vib_dT = getMixtureDer_dQTransRot2Vib_dT(TTrans,TVib,Tel,y_s,p,rho,pAtm,R0,...
                                nAvogadro,aVib_sl,bVib_sl,sigmaVib_s,bMol,Mm_s,R_s,...
                                bElectron,thetaVib_s,numberOfTemp,options);

if bNoQForm;    dQFormv_dT = 0;
else;           dQFormv_dT = getMixtureDer_dQFormv_dT(TVib,TElec,Tel,source_s,dsources_dT,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options);
end

if any(bElectron);  dQel_dT = getMixtureDer_dQel_dT(y_s,TTrans,Tel,rho,p,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dT,source_sr,thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc,numberOfTemp);
else;               dQel_dT = 0;
end

dsourcev_dT = dQTransRot2Vib_dT + dQFormv_dT + dQel_dT;