function dtauVibMWs_dym = getSpeciesDer_dtauVibMWs_dym(TTrans,y_s,p,Mm_s,aVib_sl,bVib_sl,bElectron,pAtm)
% getSpeciesDer_dtauVibMWs_dym computes the derivative wrt the species mass
% fraction of the vibrational relaxation time of the molecular species
% using Millikan & White's correlation.
%
% Usage:
%   (1)
%       dtauVibMWs_dym = getSpeciesDer_dtauVibMWs_dym(TTrans,y_s,p,Mm_s,...
%                                           aVib_sl,bVib_sl,bElectron,pAtm)
%
% Inputs & Outputs:
%   TTrans          N_eta x 1
%   y_s             N_eta x N_spec
%   p               N_eta x 1
%   Mm_s            N_spec x 1
%   aVib_sl         N_spec x N_spec (species s relaxing due to species l)
%   bVib_sl         N_spec x N_spec (species s relaxing due to species l)
%   bElectron       N_spec x 1
%   pAtm            1 x 1
%   dtauVibMWs_dym  N_eta x N_spec(m) x N_spec(s)
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% analyzing inputs, reshaping and repmatting
[N_eta,N_spec]  = size(y_s);
N_heavy         = nnz(~bElectron);
idx_heavy       = find(~bElectron);
TTrans_3D       = repmat(TTrans,[1,N_spec,N_spec]);
p_3D            = repmat(p,[1,N_spec,N_spec]);
aVib_sl         = repmat(reshape(aVib_sl,[1,N_spec,N_spec]),[N_eta,1,1]);
bVib_sl         = repmat(reshape(bVib_sl,[1,N_spec,N_spec]),[N_eta,1,1]);

% computing required terms
Mm      = getMixture_Mm_R(y_s,TTrans,TTrans,Mm_s,1,bElectron);              % dummies are passed for Tel & R0(not needed for Mm)
X_s     = getSpecies_X(y_s,Mm,Mm_s);
dXs_dym = getSpeciesDer_dXs_dym(y_s,Mm_s);                                  % (eta,m,s)
X_H     = X_s(:,~bElectron);                                                % mole fraction of heavy particles
dXH_dym = dXs_dym(:,:,~bElectron);                                          % (eta,m,l) with l in heavy

tau_sl = pAtm./p_3D .* exp(aVib_sl.*(TTrans_3D.^(-1/3) - bVib_sl) - 18.42); % exponential term (eta,s,l)
tau_sH  = tau_sl(:,:,~bElectron);                                           % removing electrons

% reshaping sum over ls
X_s4D = repmat(permute(X_s,[1,3,4,2]),[1,N_spec,N_spec,1]);                 % (eta,l) --> (eta,m,s,l)
tausH_4D = repmat(permute(tau_sH,[1,4,2,3]),[1,N_spec,1,1]);                % (eta,s,l) --> (eta,m,s,l)
dXs_dym_4D = repmat(permute(dXs_dym,[1,2,4,3]),[1,1,N_spec,1]);             % (eta,m,l) --> (eta,m,s,l)
X_allH2D = repmat(X_H*ones(N_heavy,1),[1,N_spec]);                          % sum of the mole frac. of all heavies

% computing necessary sums
addend1 = zeros(N_eta,N_spec,N_spec); % allocating (eta,m,s)
addend3 = zeros(N_eta,N_spec,N_spec);
dXallH_dym = zeros(N_eta,N_spec); % (eta,m)
for ll=1:N_heavy % looping species
    l = idx_heavy(ll);
    dXallH_dym = dXallH_dym + dXH_dym(:,:,l);
    addend1 = addend1 + X_s4D(:,:,:,l)./tausH_4D(:,:,:,l); % suming relaxation time of each relaxing pair
    addend3 = addend3 + dXs_dym_4D(:,:,:,l)./tausH_4D(:,:,:,l);
end

% assembling derivative
dtauVibMWs_dym = dXallH_dym./addend1 - X_allH2D./addend1.^2 .* addend3;

end % getSpeciesDer_dtauVibMWs_dym