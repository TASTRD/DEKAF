function dQel_dym = getMixtureDer_dQel_dym(y_s,TTrans,Tel,rho,p,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dym,thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc)
% getMixtureDer_dQel_dym computes the concentration derivative of the
% energy source term due to electrons in a two-temperature system.
%
% Usage:
%   (1)
%       dQel_dym = getMixtureDer_dQel_dym(y_s,TTrans,Tel,rho,p,Mm_s,R0,nAvogadro,bPos,...
%                           bElectron,epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dym,...
%                           thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc)
%
% Inputs and outputs:
%   y_s                 [-]
%       species mass fraction (size N_eta x N_spec)
%   TTrans              [K]
%       translational temperature of heavy species (size N_eta x 1)
%   Tel                 [K]
%       translational temperature of electron (size N_eta x 1)
%   rho                 [kg/m^3]
%       mixture density (size N_eta x 1)
%   p                   [Pa]
%       mixture pressure (size N_eta x 1)
%   Mm_s                [kg/mol]
%       species molar mass (size N_spec x 1)
%   R0           [J/mol-K]
%       Universal gas constant (size 1 x 1)
%   nAvogadro           [-]
%       Avogadro's number (size 1 x 1)
%   bPos                [-]
%       positively-charged-species boolean (size N_spec x 1)
%   bElectron           [-]
%       electron boolen (size N_spec x 1)
%   epsilon0            [C^2/N-m^2 ]
%       permittivity of vacuum (size 1 x 1)
%   kBoltz              [m^2-kg/K-s^2]
%       Boltzmann constant (size 1 x 1)
%   qEl                 [C]
%       elementary charge (size 1 x 1)
%   consts_Omega11      [-]
%       struct with fields .A, .B, .C, .D, .E, .F for the curve fit for
%       collision integral (1,1). Each field has size N_spec x N_spec
%   dsourcesr_dym       [kg/s]
%       concentration derivative of the mass source term of each species s
%       due to each reaction r (size N_eta x N_spec(m) x N_spec(s) x N_reac)
%   thetaVib_s          [K]
%       activation temperature for the vibrational energy mode m of each
%       species s (size N_spec x N_m)
%   hIon_s              [J/kg]
%       ionization enthalpy of each species s (size N_spec x 1)
%   bMol                [-]
%       molecular-species boolean (size N_spec x 1)
%   bIonelim            [-]
%       boolean for reactions in the category "ionization by electron
%       impact" (size N_reac x 1)
%   bIonHeavyim         [-]
%       boolean for reactions in the category "ionization by heavy-particle
%       impact" (size N_reac x 1)
%   bDissel             [-]
%       boolean for reactions in the category "dissociation by electron
%       impact" (size N_reac x 1)
%   bIonAssoc           [-]
%       boolean for reactions in the category "ionization by association"
%       (size N_reac x 1)
%   dQel_dym        [J/kg-s]
%       concentration derivative of the energy source term due to electrons
%       in a two-temperature system. (size N_eta x N_spec)
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

% Obtaining sizes
[N_eta,N_spec] = size(y_s);
N_ion           = nnz(bPos);                    % number of ion species
N_mol           = nnz(bMol);                    % number of molecular species
N_hpiaii        = nnz(bIonHeavyim|bIonAssoc);   % number of heavy-particle-impact- or associative-ionization reactions
N_eii           = nnz(bIonelim);                % number of electron-impact-ionization reactions
N_eid           = nnz(bDissel);                 % number of electron-impact-dissociation reactions

% Common auxiliary quantities
Mm_el = Mm_s(bElectron);

%%%%%%%%% First contribution dQel1_dym --> heavy-particle-impact and association ionization
dsource_dym_hpiaii_el = permute(dsourcesr_dym(:,:,bElectron,bIonHeavyim|bIonAssoc),[1,4,2,3]);  % (eta,m,s,r) --> (eta,hpiaii,m)
for m=N_spec:-1:1
    dQel1_dym(:,m) = 3/2*R0/Mm_el*Tel .* (dsource_dym_hpiaii_el(:,:,m)*ones(N_hpiaii,1));       % (eta,m)
end

%%%%%%%%% Second contribution dQel2_dym --> electron-impact ionization
dsource_dym_eii_ion = permute(reshape(dsourcesr_dym(:,:,bPos,bIonelim),[N_eta,N_spec,N_ion*N_eii]),[1,3,2]); % (eta,m,s,r) --> (eta,m,eiiIon) --> (eta,eiiIon,m)
hIon_s2Dsr          = repmat(hIon_s(bPos),    [1,N_eii]);                                                    % (s,1) --> (Ion,eii)
hIon_s2D            = repmat(hIon_s2Dsr(:).', [N_eta,1]);                                                    % (s,r) --> (1,eiiIon) --> (eta,eiiIon)
for m=N_spec:-1:1
    dQel2_dym(:,m) = -(dsource_dym_eii_ion(:,:,m).*hIon_s2D)*ones(N_ion*N_eii,1);                            % (eta,m)
end

%%%%%%%%% Third contribution dQel3_dym --> electron-impact dissociation
dsource_dym_eid_mol = permute(reshape(dsourcesr_dym(:,:,bMol,bDissel),[N_eta,N_spec,N_mol*N_eid]),[1,3,2]); % (eta,m,s,r) --> (eta,m,eidMol) --> (eta,eidMol,m)
hDiss_s             = getSpecies_hVib(Tel,R0./Mm_s(bMol),thetaVib_s(bMol,:),'notRepmatted');                % (eta,Mol)
hDiss_s2D           = reshape(repmat(hDiss_s,[1,1,N_eid]),[N_eta,N_mol*N_eid]);                             % (eta,Mol) --> (eta,Mol,eid) --> (eta,eidMol)
for m=N_spec:-1:1
    dQel3_dym(:,m) = -(dsource_dym_eid_mol(:,:,m).*hDiss_s2D)*ones(N_mol*N_eid,1);                          % (eta,m)
end

%%%%%%%%% Fourth contribution dQel4_dym --> translational heavy-electron energy relaxation
tau_TransEl     = getMixture_tauTransEl(y_s,TTrans,Tel,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11);            % (eta,1)
dtauTransEl_dym = getMixtureDer_dtauTransEl_dym(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11);  % (eta,m)
drho_dym        = getMixtureDer_drho_dym(p,y_s,R0,Mm_s,bElectron,TTrans,Tel);                                                               % (eta,m)
drhos_dym       = getSpeciesDer_drhol_dym(rho,drho_dym,y_s);                                                                                % (eta,m,s)
drhoel_dym      = drhos_dym(:,:,bElectron);                                                                                                 % (eta,m)
rho_el          = y_s(:,bElectron).*rho;                                                                                                    % (eta,1)

tau_TransEl2D   = repmat(tau_TransEl,[1,N_spec]); % (eta,1) --> (eta,m)
rho_el2D        = repmat(rho_el,     [1,N_spec]); % (eta,1) --> (eta,m)
TTrans2D        = repmat(TTrans,     [1,N_spec]); % (eta,1) --> (eta,m)
Tel2D           = repmat(Tel,        [1,N_spec]); % (eta,1) --> (eta,m)

dQel4_dym = 3*R0/Mm_el*(TTrans2D-Tel2D) .* (drhoel_dym./tau_TransEl2D - rho_el2D./tau_TransEl2D.^2 .* dtauTransEl_dym); % (eta,m)

%%%%%%%%% Assembling
dQel_dym = dQel1_dym + dQel2_dym + dQel3_dym + dQel4_dym; % (eta,m)

end % getMixtureDer_dQel_dym