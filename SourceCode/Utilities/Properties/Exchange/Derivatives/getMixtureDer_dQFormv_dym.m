function dQFormv_dym = getMixtureDer_dQFormv_dym(TVib,TElec,Tel,dsources_dym,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options)
% getMixtureDer_dQFormv_dym computes temperature derivatives of the
% mixture's vibrational-electronic-electron enthalpy source term due to the
% formation of species.
%
% Usage:
%   (1)
%       dQFormv_dym = getMixtureDer_dQFormv_dym(TVib,TElec,Tel,dsources_dym,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options)
%
% Inputs and outputs:
%   TVib            (N_eta x 1)
%   TElec           (N_eta x 1)
%   Tel             (N_eta x 1)
%   source_s        (N_eta x N_spec)
%   dsources_dym    (N_eta x N_spec(m) x N_spec(s)
%   R_s             (N_spec x 1)
%   thetaVib_s      (N_spec x Nvib)
%   thetaElec_s     (N_spec x N_stat)
%   gDegen_s        (N_spec x N_stat)
%   bElectron       (N_spec x 1)
%   dQFormv_dym     (N_eta x N_spec)
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

protectedvalue(options,'modelThermal',{'RRHO','RRHO-1LA'});

% reshaping
[N_eta,N_spec,~] = size(dsources_dym);

TVib = repmat(TVib,[1,N_spec]);
TElec = repmat(TElec,[1,N_spec]);
Tel = repmat(Tel,[1,N_spec]);
R_s = repmat(R_s.',[N_eta,1]);
thetaVib_s = repmat(permute(thetaVib_s,[3,1,2]),[N_eta,1,1]);
thetaElec_s = repmat(permute(thetaElec_s,[3,1,2]),[N_eta,1,1]);
gDegen_s = repmat(permute(gDegen_s,[3,1,2]),[N_eta,1,1]);
bElectron = repmat(bElectron.',[N_eta,1]);

% Computing species vib-elec-el heat capacity
hv_s = getSpecies_hv_cpv(TVib,TElec,Tel,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options);

% computing source term
dsources_dym = permute(dsources_dym,[1,3,2]); % (eta,m,s) --> (eta,s,m)
for m=N_spec:-1:1
    dQFormv_dym(:,m) = (hv_s.*dsources_dym(:,:,m))*ones(N_spec,1);
end

end % getMixtureDer_dQFormv_dym