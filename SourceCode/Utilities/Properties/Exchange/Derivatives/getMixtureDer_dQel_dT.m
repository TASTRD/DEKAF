function dQel_dT = getMixtureDer_dQel_dT(y_s,TTrans,Tel,rho,p,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dT,source_sr,thetaVib_s,hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc,numberOfTemp)
% getMixtureDer_dQel_dT computes the temperature derivatives of the energy
% source term due to electrons in a two-temperature system.
%
% Usage:
%   (1)
%       dQel_dT = getMixtureDer_dQel_dT(y_s,TTrans,Tel,rho,p,Mm_s,R0,nAvogadro,bPos,bElectron,...
%                           epsilon0,kBoltz,qEl,consts_Omega11,dsourcesr_dT,source_sr,thetaVib_s,...
%                           hIon_s,bMol,bIonelim,bIonHeavyim,bDissel,bIonAssoc,numberOfTemp)
%
% Inputs and outputs:
%   y_s                 [-]
%       species mass fraction (size N_eta x N_spec)
%   TTrans              [K]
%       translational temperature of heavy species (size N_eta x 1)
%   Tel                 [K]
%       translational temperature of electron (size N_eta x 1)
%   rho                 [kg/m^3]
%       mixture density (size N_eta x 1)
%   p                   [Pa]
%       mixture pressure (size N_eta x 1)
%   Mm_s                [kg/mol]
%       species molar mass (size N_spec x 1)
%   R0           [J/mol-K]
%       Universal gas constant (size 1 x 1)
%   nAvogadro           [-]
%       Avogadro's number (size 1 x 1)
%   bPos                [-]
%       positively-charged-species boolean (size N_spec x 1)
%   bElectron           [-]
%       electron boolen (size N_spec x 1)
%   epsilon0            [C^2/N-m^2 ]
%       permittivity of vacuum (size 1 x 1)
%   kBoltz              [m^2-kg/K-s^2]
%       Boltzmann constant (size 1 x 1)
%   qEl                 [C]
%       elementary charge (size 1 x 1)
%   consts_Omega11      [-]
%       struct with fields .A, .B, .C, .D, .E, .F for the curve fit for
%       collision integral (1,1). Each field has size N_spec x N_spec
%   source_sr           [kg/s]
%       mass source term of each species s due to each reaction r
%       (size N_eta x N_spec x N_reac)
%   dsourcesr_dT        [kg/s-K]
%       temperature derivatives of the mass source term of each species s
%       due to each reaction r (size N_eta x N_spec x N_T x N_reac)
%   thetaVib_s          [K]
%       activation temperature for the vibrational energy mode m of each
%       species s (size N_spec x N_m)
%   hIon_s              [J/kg]
%       ionization enthalpy of each species s (size N_spec x 1)
%   bMol                [-]
%       molecular-species boolean (size N_spec x 1)
%   bIonelim            [-]
%       boolean for reactions in the category "ionization by electron
%       impact" (size N_reac x 1)
%   bIonHeavyim         [-]
%       boolean for reactions in the category "ionization by heavy-particle
%       impact" (size N_reac x 1)
%   bDissel             [-]
%       boolean for reactions in the category "dissociation by electron
%       impact" (size N_reac x 1)
%   bIonAssoc           [-]
%       boolean for reactions in the category "ionization by association"
%       (size N_reac x 1)
%   numberOfTemp
%       string identifying the number of temperatures that are used to
%       describe the thermal state of the gas
%   dQel_dT        [J/kg-s-K]
%       temperature derivatives of the energy source term due to electrons
%       in a two-temperature system. (size N_eta x N_spec)
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

% Obtaining sizes
[N_eta,~]       = size(y_s);
N_ion           = nnz(bPos);                    % number of ion species
N_mol           = nnz(bMol);                    % number of molecular species
N_hpiaii        = nnz(bIonHeavyim|bIonAssoc);   % number of heavy-particle-impact- or associative-ionization reactions
N_eii           = nnz(bIonelim);                % number of electron-impact-ionization reactions
N_eid           = nnz(bDissel);                 % number of electron-impact-dissociation reactions
switch numberOfTemp
    case '2T'
        N_T         = 2;        idxEl = 2;
        plusMinus_T = [1,-1];                   % (T,1) used in dQel4_dT
        drho_dT     = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,[TTrans,Tel],'2T'); % (eta,T)
    otherwise
        error(['the chosen numberOfTemp=''',numberOfTemp,''' is not supported']);
end

% Common auxiliary quantities
Mm_el = Mm_s(bElectron);

%%%%%%%%% First contribution dQel1_dT --> heavy-particle-impact and association ionization
dsource_dT_hpiaii_el = permute(dsourcesr_dT(:,bElectron,:,bIonHeavyim|bIonAssoc),[1,4,3,2]);    % (eta,s,T,r) --> (eta,1,T,hpiaii) --> (eta,hpiaii,T)
source_hpiaii_el = permute(source_sr(:,bElectron,bIonHeavyim|bIonAssoc),[1,3,2]);               % (eta,s,r) ----> (eta,1,hpiaii) ----> (eta,hpiaii)
for iT=N_T:-1:1
    dQel1_dT(:,iT) = 3/2*R0/Mm_el*Tel .* (dsource_dT_hpiaii_el(:,:,iT)*ones(N_hpiaii,1));       % (eta,T)
    if iT==idxEl                                                                                % adding term exclusive to Tel derivative
        dQel1_dT(:,iT) = dQel1_dT(:,iT) + 3/2*R0/Mm_el.*(source_hpiaii_el*ones(N_hpiaii,1));    % (eta,T)
    end
end

%%%%%%%%% Second contribution dQel2_dT --> electron-impact ionization
dsource_dT_eii_ion4D = permute(dsourcesr_dT(:,bPos,:,bIonelim),[1,3,2,4]);                      % (eta,s,T,r) ------> (eta,T,Ion,eii)
dsource_dT_eii_ion   = permute(reshape(dsource_dT_eii_ion4D,[N_eta,N_T,N_ion*N_eii]),[1,3,2]);  % (eta,T,Ion,eii) --> (eta,T,eiiIon) --> (eta,eiiIon,T)
hIon_s2Dsr           = repmat(hIon_s(bPos),    [1,N_eii]);                                      % (s,1) ------------> (Ion,eii)
hIon_s2D             = repmat(hIon_s2Dsr(:).', [N_eta,1]);                                      % (s,r) ------------> (1,eiiIon) ------> (eta,eiiIon)
for iT=N_T:-1:1
    dQel2_dT(:,iT) = -(dsource_dT_eii_ion(:,:,iT).*hIon_s2D)*ones(N_ion*N_eii,1);               % (eta,T)
end

%%%%%%%%% Third contribution dQel3_dT --> electron-impact dissociation
hDiss_s              = getSpecies_hVib(Tel,R0./Mm_s(bMol),thetaVib_s(bMol,:),'notRepmatted');       % (eta,Mol)
dhDisss_dT_short     = getSpeciesDer_dhVibs_dT(Tel,R0./Mm_s(bMol),thetaVib_s(bMol,:),numberOfTemp); % (eta,Mol,T)

source_eidMol        = reshape(source_sr(:,bMol,bDissel),   [N_eta,N_mol*N_eid]);                   % (eta,s,r) --------> (eta,eidMol)
hDiss_s2D            = reshape(repmat(hDiss_s,[1,1,N_eid]), [N_eta,N_mol*N_eid]);                   % (eta,Mol) --------> (eta,Mol,eid) --> (eta,eidMol)
dsource_dT_eid_mol4D = permute(        dsourcesr_dT(:,bMol,:,bDissel),                [1,3,2,4]);   % (eta,s,T,r) ------> (eta,T,Mol,eid)
dsource_dT_eid_mol   = permute(reshape(dsource_dT_eid_mol4D,[N_eta,N_T,N_mol*N_eid]), [1,3,2]);     % (eta,T,Mol,eid) --> (eta,T,eidMol) ---> (eta,eidMol,T)
dhDiss_dT4D          = permute(repmat(dhDisss_dT_short,[1,1,1,N_eid]),                [1,3,2,4]);   % (eta,Mol,T) ------> (eta,Mol,T,eid) --> (eta,T,Mol,eid)
dhDiss_dT            = permute(reshape(dhDiss_dT4D,         [N_eta,N_T,N_mol*N_eid]), [1,3,2]);     % (eta,T,Mol,eid) --> (eta,T,eidMol) ---> (eta,eidMol,T)

for iT=N_T:-1:1
    dQel3_dT(:,iT) = -(dsource_dT_eid_mol(:,:,iT).*hDiss_s2D + source_eidMol.*dhDiss_dT(:,:,iT))*ones(N_mol*N_eid,1); % (eta,T)
end

%%%%%%%%% Fourth contribution dQel4_dT --> translational heavy-electron energy relaxation
tau_TransEl     = getMixture_tauTransEl(y_s,TTrans,Tel,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11);                        % (eta,1)
dtauTransEl_dT  = getMixtureDer_dtauTransEl_dT(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11,numberOfTemp);  % (eta,T)
drhos_dT        = getSpeciesDer_drhol_dT(drho_dT,y_s);                                                                                                  % (eta,s,T)
drhoel_dT       = permute(drhos_dT(:,bElectron,:),[1,3,2]);                                                                                             % (eta,T)
rho_el          = y_s(:,bElectron).*rho;                                                                                                                % (eta,1)

tau_TransEl2D   = repmat(tau_TransEl,[1,N_T]); % (eta,1) --> (eta,T)
rho_el2D        = repmat(rho_el,     [1,N_T]); % (eta,1) --> (eta,T)
TTrans2D        = repmat(TTrans,     [1,N_T]); % (eta,1) --> (eta,T)
Tel2D           = repmat(Tel,        [1,N_T]); % (eta,1) --> (eta,T)
plusMinus_T2D   = repmat(plusMinus_T,[N_eta,1]); % (T,1) --> (eta,T)

dQel4_dT = 3*R0/Mm_el*((TTrans2D-Tel2D).*drhoel_dT./tau_TransEl2D ...
                     - (TTrans2D-Tel2D).*rho_el2D ./tau_TransEl2D.^2 .* dtauTransEl_dT ...
                     + plusMinus_T2D.*   rho_el2D ./tau_TransEl2D) ;            % (eta,T)

%%%%%%%%% Assembling
dQel_dT = dQel1_dT + dQel2_dT + dQel3_dT + dQel4_dT; % (eta,T)

end % getMixtureDer_dQel_dT