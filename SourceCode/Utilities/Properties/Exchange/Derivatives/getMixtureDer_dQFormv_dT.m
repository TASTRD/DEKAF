function dQFormv_dT = getMixtureDer_dQFormv_dT(TVib,TElec,Tel,source_s,dsources_dT,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options)
% getMixtureDer_dQFormv_dT computes temperature derivatives of the
% mixture's vibrational-electronic-electron enthalpy source term due to the
% formation of species.
%
% Usage:
%   (1)
%       dQFormv_dT = getMixtureDer_dQFormv_dT(TVib,TElec,Tel,source_s,dsources_dT,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options)
%
% Inputs and outputs:
%   TVib            (N_eta x 1)
%   TElec           (N_eta x 1)
%   Tel             (N_eta x 1)
%   source_s        (N_eta x N_spec)
%   dsources_dT     (N_eta x N_spec x N_T)
%   R_s             (N_spec x 1)
%   thetaVib_s      (N_spec x Nvib)
%   thetaElec_s     (N_spec x N_stat)
%   gDegen_s        (N_spec x N_stat)
%   bElectron       (N_spec x 1)
%   dQFormv_dT      (N_eta x N_T)
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

protectedvalue(options,'modelThermal',{'RRHO','RRHO-1LA'});

% reshaping
[N_eta,N_spec,N_T] = size(dsources_dT);

TVib = repmat(TVib,[1,N_spec]);
TElec = repmat(TElec,[1,N_spec]);
Tel = repmat(Tel,[1,N_spec]);
R_s = repmat(R_s.',[N_eta,1]);
thetaVib_s = repmat(permute(thetaVib_s,[3,1,2]),[N_eta,1,1]);
thetaElec_s = repmat(permute(thetaElec_s,[3,1,2]),[N_eta,1,1]);
gDegen_s = repmat(permute(gDegen_s,[3,1,2]),[N_eta,1,1]);
bElectron = repmat(bElectron.',[N_eta,1]);

% Computing species vib-elec-el heat capacity
[hv_s,dhvs_dTv] = getSpecies_hv_cpv(TVib,TElec,Tel,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options);

switch options.numberOfTemp
    case '2T'
        dhvs_dT = cat(3,zeros(N_eta,N_spec),dhvs_dTv);
    otherwise
        error(['the selected thermal model ''',options.numberOfTemp,''' is not supported']);
end

% computing source term
for iT=N_T:-1:1
    dQFormv_dT(:,iT) = (dhvs_dT(:,:,iT).*source_s + hv_s.*dsources_dT(:,:,iT))*ones(N_spec,1);
end

end % getMixtureDer_dQFormv_dT