function dtauTransEl_dym = getMixtureDer_dtauTransEl_dym(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11)
% getMixtureDer_dtauTransEl_dym computes the concentration derivatives of
% the relaxation time of energy transfer between the translational mode of
% heavy particles and electrons.
%
% Usage:
%   (1)
%       dtauTransEl_dym = getMixtureDer_dtauTransEl_dym(y_s,TTrans,Tel,p,rho,Mm_s,R0,...
%                           nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11)
%
% Inputs and outputs:
%   y_s                 [-]
%       species mass fraction (size N_eta x N_spec)
%   TTrans              [K]
%       translational temperature of heavy species (size N_eta x 1)
%   Tel                 [K]
%       translational temperature of electron (size N_eta x 1)
%   p                   [Pa]
%       mixture pressure (size N_eta x 1)
%   rho                 [kg/m^3]
%       mixture density (size N_eta x 1)
%   Mm_s                [kg/mol]
%       species molar mass (size N_spec x 1)
%   R0           [J/mol-K]
%       Universal gas constant (size 1 x 1)
%   nAvogadro           [-]
%       Avogadro's number (size 1 x 1)
%   bPos                [-]
%       positively-charged-species boolean (size N_spec x 1)
%   bElectron           [-]
%       electron boolen (size N_spec x 1)
%   epsilon0            [C^2/N-m^2 ]
%       permittivity of vacuum (size 1 x 1)
%   kBoltz              [m^2-kg/K-s^2]
%       Boltzmann constant (size 1 x 1)
%   qEl                 [C]
%       elementary charge (size 1 x 1)
%   consts_Omegaij      [-]
%       struct with fields .A, .B, .C, .D, .E, .F for the collision
%       integral curve fit. Each field has size N_spec x N_spec
%   dtauTransEl_dym     [s]
%       concentration derivatives of the relaxation time of energy transfer
%       between the translational mode of heavy particles and electrons
%       (size N_eta x N_spec).
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

% Obtaining sizes
[N_eta,N_spec] = size(y_s);
N_heavy = nnz(~bElectron);

% Computing auxiliary quantities
tau_TransEl       = getMixture_tauTransEl(y_s,TTrans,Tel,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11);       % (eta,1)
n_s               = getSpecies_n(rho,y_s,Mm_s,nAvogadro);                                                                                % (eta,s)
drho_dym          = getMixtureDer_drho_dym(p,y_s,R0,Mm_s,bElectron,TTrans,Tel);                                                          % (eta,m)
dns_dym           = getSpeciesDer_dns_dym(y_s,drho_dym,rho,nAvogadro,Mm_s);                                                              % (eta,m,s)

lnOmega11_sl      = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega11,bPos);              % (eta,s,l)
dlnOmega11sl_dym  = getPairDer_dlnOmegaij_dym(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omega11); % (eta,s,l,m)
lnOmega11_sel     = lnOmega11_sl(:,:,bElectron);                                                                                         % (eta,s)
dlnOmega11sel_dym = permute(dlnOmega11sl_dym(:,:,bElectron,:),[1,2,4,3]);                                                                % (eta,s,m)
Omega11_sel       = exp(lnOmega11_sel);                                                                                                  % (eta,s)
dOmega11_sel_dym  = repmat(Omega11_sel,[1,1,N_spec]) .* dlnOmega11sel_dym; % dA/dq = A*dlnA/dq                                             (eta,s,m)

% repmatting
Mm_el = Mm_s(bElectron);                        % electron molar mass
Mm_H2D = repmat(Mm_s(~bElectron).',[N_eta,1]);  % heavy-particle molar mass
dns_dym_3D  = permute(dns_dym,[1,3,2]);         % (eta,m,s) --> (eta,s,m)

% assembling
for m=N_spec:-1:1
    bracket = dns_dym_3D(:,:,m).*Omega11_sel + n_s.*dOmega11_sel_dym(:,:,m);    % bracket appearing in the expression of the
    bracketH = bracket(:,~bElectron);                                           % keeping only heavy species
    dtauTransEl_dym(:,m) = -tau_TransEl.^2.*sqrt(8*R0*Tel/(pi*Mm_el)) .* ((8/3*Mm_el./Mm_H2D .* bracketH)*ones(N_heavy,1));
end

end % getMixtureDer_dtauTransEl_dym