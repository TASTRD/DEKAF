function sigmaVib_s = getSpecies_sigmaVib(spec_list,bMol,options)
% getSpecies_sigmaVib returns the limiting cross section for vibrational
% excitation at 50 000K.
%
% Usage:
%   (1)
%       sigmaVib_s = getSpecies_sigmaVib(spec_list,bMol,options)
%
% Inputs and outputs:
%   spec_list
%       cell of strings with the names of each species
%   bMol
%       molecular-species boolean (size N_spec x 1)
%   options
%       options structure, which must contain
%           .referenceVibRelax
%               reference to be used as the source for sigmaVib_s.
%               Supported values are:
%                   'Park93'        - Refs. [1] and [2]
%                   'Mortensen2013' - Ref. [3]
%   sigmaVib_s
%       vector of size N_spec x 1 with the limiting cross section for
%       vibrational excitation at 50 000K
%
% References:
%   [1] Park, C. (1993). Review of chemical-kinetic problems of future NASA
%   missions. I - Earth entries. Journal of Thermophysics and Heat
%   Transfer, 7(3), 385–398. https://doi.org/10.2514/3.496
%   [2] Park, C., Howe, J. T., Jaffe, R. L., & Candler, G. V. (1994).
%   Review of chemical-kinetic problems of future NASA missions. II - Mars
%   entries. Journal of Thermophysics and Heat Transfer, 8(1), 9–23.
%   https://doi.org/10.2514/3.496
%   [3] Mortensen, C. (2013). Effects of Thermochemical Nonequilibrium on
%   Hypersonic Boundary-Layer Instability in the Presence of Surface
%   Ablation or Isolated Two-Dimensional Roughness. UCLA.
%
% Author: Fernando Miro Miro
% Date: April 2019
% GNU Lesser General Public License 3.0

sigmaVib_s = NaN*ones(size(bMol)); % allocating

for s=1:length(spec_list)
    if bMol(s)
        switch options.referenceVibRelax
            case 'Park93'
                switch spec_list{s}
                    case  {'N2','N2+','O2','O2+','NO','NO+'}
                        sigmaVib_s(s) = 3e-21; % [m^2]
                    case 'CO'
                        sigmaVib_s(s) = 3e-22; % [m^2]
                    case 'CO2'
                        sigmaVib_s(s) = 1e-20; % [m^2]
                    case {'C2','C3','CN','Air'} % actually from Mortensen, but left here for completeness
                        sigmaVib_s(s) = 1e-21; % [m^2]
                    otherwise
                        error(['the chosen molecular species ''',spec_list{s},''' is not supported']);
                end
            case 'Mortensen2013' % Eq. 2.30 of his thesis
                sigmaVib_s(s) = 1e-21; % [m^2]
            otherwise
                error(['Error the chosen reference for the vibrational relaxation constants ''',options.referenceVibRelax,''' is not supported']);
        end
    end
end

end % sigmaVib_s