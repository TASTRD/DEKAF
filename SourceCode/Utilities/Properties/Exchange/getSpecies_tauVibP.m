function tauVibP_s = getSpecies_tauVibP(TTrans,Tel,y_s,p,Mm_s,bElectron,sigmaVib_s,R0,nAvogadro)
% getSpecies_tauVibP computes the correction of the species vibrational
% relaxation time proposed by Park for high temperatures.
%
% Usage:
%   (1)
%       tauVibP_s = getSpecies_tauVibP(TTrans,Tel,y_s,p,Mm_s,bElectron,...
%                                               sigmaVib_s,R0,nAvogadro)
%
% Inputs & outputs:
%   TTrans      N_eta x 1
%   Tel         N_eta x 1
%   y_s         N_eta x N_spec
%   p           N_eta x 1
%   Mm_s        N_spec x 1
%   bElectron   N_spec x 1
%   sigmaVib_s  N_spec x 1
%   R0          1 x 1
%   nAvogadro   1 x 1
%   tauVibP_s   N_eta x N_spec
%
% FIXME: THIS FUNCTION IS INVALID FOR NON-IDEAL EQUATIONS OF STATE
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% computing intermediate
rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);
n_s = getSpecies_n(rho,y_s,Mm_s,nAvogadro);

% reshaping and repmatting
[N_eta,N_spec] = size(y_s);
TTrans = repmat(TTrans,[1,N_spec]);
Mm_s = repmat(Mm_s.',[N_eta,1]);
sigmaVib_s = repmat(sigmaVib_s.',[N_eta,1]);

sigmav_s = sigmaVib_s.*(50e3./TTrans).^2;

tauVibP_s = 1./(n_s.*sqrt(8.*R0.*TTrans./(pi*Mm_s)).*sigmav_s);