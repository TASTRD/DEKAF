function [Mm_s,Amu_s,Bmu_s,Cmu_s,Akappa_s,Bkappa_s,Ckappa_s,Dkappa_s,Ekappa_s,muRef_s,TmuRef_s,Smu_s,kappaRef_s,TkappaRef_s,Skappa_s,R_s,...
    nAtoms_s,thetaVib_s,pCrit_s,TCrit_s,Keq_struc,thetaRot_s,L_s,sigma_s,thetaElec_s,gDegen_s,bElectron,hForm_s,HForm298K_s,hIon_s,bPos,bMol,...
    gasSurfReac_struct,AThermFuncs_s] = ...
    getSpecies_constants(spec_listIn,R0,hPlanck,cLight,kBoltz,nAvogadro,Keq_struc,gasSurfReac_struct,options)
% getSpecies_constants Calculate physical constants for a list of species.
%
% Usage:
%   (1)
%       [Mm_s,Amu_s,Bmu_s,Cmu_s,Akappa_s,Bkappa_s,Ckappa_s,Dkappa_s,Ekappa_s,muRef_s,TmuRef_s,Smu_s,kappaRef_s,TkappaRef_s,Skappa_s,R_s,...
%     nAtoms_s,thetaVib_s,pCrit_s,TCrit_s,Keq_struc,thetaRot_s,L_s,sigma_s,thetaElec_s,gDegen_s,bElectron,hForm_s,HForm298K_s,hIon_s,bPos,bMol,...
%     gasSurfReac_struct,AThermFuncs_s] = ...
%     getSpecies_constants(spec_listIn,R0,hPlanck,cLight,kBoltz,nAvogadro,Keq_struc,gasSurfReac_struct,options)
%
% Inputs:
%   spec_list       -   cell array of strings whose entries are the species
%   R0              -   universal gas constant      [J/(kg*K)]
%   hPlanck         -   planck's constant           [J*s]
%   cLight          -   speed of light              [m/s]
%   kBoltz          -   boltzmann's constant        [m^2*kg/K*s^2]
%   nAvogadro       -   Avogadro's constant         [part/mole]
%   Keq_struc       -   struct used for getReaction_Keq.m
%                       its fields are not used in this function
%   gasSurfReac_struct  structure containing those variables needed for the
%                       gas-surface interaction modeling.
%   options         -   structure, this function requires: elecStates
%
% Outputs:
%   Mm_s
%       species molar mass constants. Vector of size (N_spec x 1)
%   Amu_s, Bmu_s, Cmu_s
%       Coefficients in the Blottner curve fit for the dynamic viscosity
%       [-]. vectors of size (N_spec x 1)
%   Akappa_s,...,Ekappa_s
%       Same story but for the thermal conductivity. Also vectors of size (N_spec x 1)
%   muRef_s, TmuRef_s, Smu_s
%       coefficients in Sutherland's viscosity law for each species (N_spec
%       x 1)
%   kappaRef_s, TkappaRef_s, Skappa_s
%       Same story but for the thermal conductivity (N_spec x 1)
%   R_s
%       species gas constant. vector of size (N_spec x 1)
%   nAtoms_s
%       # of atoms for each species. vector of size (N_spec x 1)
%   thetaVib_s
%       species vibrational activation temperature (N_spec x 1)
%   pCrit_s
%       species critical pressure (N_spec x 1)
%   TCrit_s
%       species critical temperature (N_spec x 1)
%   Keq_struc
%       structure containing all necessary fields for the computing of the
%       Keq_struc (depends on options.modelEquilibrium)
%   thetaRot_s
%       molecule's rotational activation temperature (N_spec x 1)
%   L_s
%       molecule's linearity factor (N_spec x 1)
%   sigma_s
%       molecule's steric factor (N_spec x 1)
%   Mm_s
%       molecule's moment of inertia (N_spec x 1)
%   thetaElec_s
%       activation temperature of the different electronic states
%       (N_spec x N_stat)
%   gDegen_s
%       degeneracies of the different electronic states (N_spec x N_stat)
%   bElectron
%       electron boolean true for electron species and false for all others
%       (N_spec x 1)
%   hForm_s
%       formation enthalpy of each species (N_spec x 1)
%   HForm298K_s
%       formation enthalpy per mol at 298K (N_spec x 1)
%   hIon_s
%       ionization enthalpy per kg (N_spec x 1)
%   bPos
%       positive boolean: true for positively-charged species, false if
%       not. (N_spec x 1)
%   bMol
%       positive boolean: true for molecule species, false for atomic or
%       electron species. (N_spec x 1)
%   gasSurfReac_struct
%       structure containing those variables needed for the gas-surface
%       interaction modeling. It additionally receives parameters for the
%       sublimation reactions:
%           alfaSubl_s
%               species sublimation probabilities (N_spec x 1)
%           PVapP_s
%               species vapor pressure P coefficient (N_spec x 1)
%           QVapP_s
%               species vapor pressure Q coefficient (N_spec x 1)
%   AThermFuncs_s
%       structured cell array of size N_spec x 1, with the anonymous
%       functions for each species. Each cell-array position contains
%       N_coef anonymous functions:
%           .A1, .A2, etc
%
% Curve fit coefficients for Blottner's fit for species e-, N+, O+, N2+ &
% O2+, are obtained from Gupta & Yos's 1990 paper.
%
% Sutherland law coefficients for the species are obtained from the CFD++
% database, except for the 'Air' species. 'Air' values are modified to be
% consistent with the values inside the function, getDefault_Sutherland.
%
% Heats of formation at 0K are obtained from http://cccbdb.nist.gov/hf0k.asp
%
% References:
%   (1)  Park, Chul, Nonequilibrium Hypersonic Aerothermodynamics,
%     John Wiley and Sons, 1990, Table 1.4, pages 27-32
%   (2)  Blottner et al., 1971, Chemically Reacting Viscous Flow Program for
%     Multi-Component Gas Mixtures, Tech. rep., SC-RR-70-754
%   (3)  Gupta and Yos 1990, A review of reaction rates and thermodynamic
%     and transport properties for an 11-species air model for chemical
%     and thermal non-equilibrium calculations to 30000K
%   (4)  Alkandry, Boyd & Martin, 2013, Comparison of Models for Mixture
%     Transport Properties for Numerical Simulations of Ablative
%     Heat-Shields. AIAA paper 2013-0303
%   (5)  Mortensen, C. (2013). Effects of Thermochemical Nonequilibrium on
%     Hypersonic Boundary-Layer Instability in the Presence of Surface
%     Ablation or Isolated Two-Dimensional Roughness. UCLA.
%   (6)  Gnoffo, P. A., Gupta, R. N., & Shinn, J. L. (1989). Conservation
%     Equations and Physical Models for Hypersonic Air Flows in Thermal and
%     Chemical Nonequilibrium.
%   (7)   Thompson, R. A., Lee, K.-P., & Gupta, R. N. (1990). Computer
%      Codes For The Evaluation Of Thermodynamic Properties, Transport
%      Properties, And Equilibrium Constants Of An 11-Species Air Model.
%
% DEVELOPERS NOTE: useful regexp expressions to isolate the electronic
% levels' degeneracies from mutation's csv files are:
%   (1) for the degeneracies
%       \"\ energy\=\"([0-9]+)\.[0-9]\"\ \/\>\n[\ ]+\<level\ degeneracy\=\"
%   (2) for the electronic energies
%       \"\ \/\>\n[\ ]+\<level\ degeneracy\=\"([0-9]+)\"\ energy\=\"
% These can be used in the ctrl+R option of gedit (at least in ubuntu 18.04)
%
% Children and related functions:
%   See also listOfVariablesExplained, setDefaults
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% option defaults
options = standardvalue(options,'inputBlottner',false);

% appending any "ghost" species that might be needed for the evaluation of
% the formation enthalpies
[spec_list,idx_ghostSpec] = get_additionalGhostSpecies(spec_listIn);
idx_notGhost = ~ismember((1:length(spec_list)).',idx_ghostSpec);
N_spec      = length(spec_list);

% Checking for multi-vibrational species
N_vibMax = 4;                                                               % maximum number of vibrational frequencies of any species
multiVib_specs = {'CO2','C3'};                                                   % list of multi-vibrational species
if nnz(cellfun(@(cll)ismember(cll,multiVib_specs),spec_list))               % if there are any multi-vibrational species
    thetaVib_s  = zeros(N_spec,N_vibMax);                                       % we make thetaVib_s a matrix
else % other
    thetaVib_s  = zeros(N_spec,1);
end

thetaRot_s  = zeros(N_spec,1);
Mm_s        = zeros(N_spec,1);
Amu_s       = zeros(N_spec,1);
Bmu_s       = zeros(N_spec,1);
Cmu_s       = zeros(N_spec,1);
Akappa_s    = zeros(N_spec,1);
Bkappa_s    = zeros(N_spec,1);
Ckappa_s    = zeros(N_spec,1);
Dkappa_s    = zeros(N_spec,1);
Ekappa_s    = zeros(N_spec,1);
muRef_s     = NaN*ones(N_spec,1);
TmuRef_s    = NaN*ones(N_spec,1);
Smu_s       = NaN*ones(N_spec,1);
kappaRef_s  = NaN*ones(N_spec,1);
TkappaRef_s = NaN*ones(N_spec,1);
Skappa_s    = NaN*ones(N_spec,1);
L_s         = zeros(N_spec,1);
sigma_s     = zeros(N_spec,1);
nAtoms_s    = zeros(N_spec,1); % number of atoms
HForm298K_s = zeros(N_spec,1);
HIon_s      = NaN*ones(N_spec,1); % ionization energy
pCrit_s     = zeros(N_spec,1);
TCrit_s     = zeros(N_spec,1);
formReac_s  = cell(N_spec,1);  % formation reaction
omega_s     = zeros(N_spec,1); % frequencies of the electronic states
gDegen_s    = zeros(N_spec,1); % partition functions of the electronic states
bMol        = false(N_spec,1); % boolean matrix for molecules (true for molecular species, false for others)
alfaSubl_s  = zeros(N_spec,1); % sublimation probabilities
PVapP_s     = zeros(N_spec,1); % vapor pressure P coefficient
QVapP_s     = zeros(N_spec,1); % vapor pressure Q coefficient
AThermNASA7_s = cell(N_spec,1); % coefficients in the NASA7 fits
TlimsNASA7_s = cell(N_spec,1);  % temperature ranges in the NASA7 fits

calPerCm_2_JPerM = 418.4; % conversion factors
eV_2_JPerMol = 9.65e4;

% Use logic to determine species booleans
% todo: add functionality to determine bMol via regexp
% FIXME: This equals sign currently assumes that there are NO anions in the
% mixture, setting bElectron = bNeg;
[bPos,bElectron] = getSpecies_bPosNeg(spec_list);

for s=1:N_spec
    switch spec_list{s}
        case 'Air'
            Mm_s(s)     = getElement_Mm('Air'); % [kg/mol]
            Amu_s(s)    = NaN; % SI
            Bmu_s(s)    = NaN; % SI
            Cmu_s(s)    = NaN; % SI
            Akappa_s(s) = NaN; % SI
            Bkappa_s(s) = NaN; % SI
            Ckappa_s(s) = NaN; % SI
            Dkappa_s(s) = NaN; % SI
            Ekappa_s(s) = NaN; % SI
            muRef_s(s)      = 1.716e-5; % [kg/m-s]
            TmuRef_s(s)     = 273.15;   % [K]
            Smu_s(s)        = 110.6;    % [K]
            kappaRef_s(s)   = 0.0241;   % [W/m-K]
            TkappaRef_s(s)  = 273.15;   % [K]
            Skappa_s(s)     = 194;      % [K]
            L_s(s)      = 2; % [-]
            sigma_s(s)  = 1; % [-]
            nAtoms_s(s) = 2; % [-]
            pCrit_s(s)  = 3.77e6; % [Pa]
            TCrit_s(s)  = 132.164; % [K]
            thetaRot_s(s) = 0; % [K] (irrelevant - neglecting rotational energy)
            thetaVib_s(s,1) = 0; % [K] (irrelevant - negelecting have no vibrational energy)
            HForm298K_s(s)  = 0; % [J/mol] (298K)
            formReac_s{s} = ' <-> ';
            bMol(s) = true;
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'mutation'
                    N_stat = 1;
                    omega_s(s,1:N_stat) = 0.0; % [1/m]
                    gDegen_s(s,1:N_stat) = 1; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'Ar'
            Mm_s(s)     = getElement_Mm('Ar'); % [kg/mol]
            Amu_s(s)    = NaN; % SI
            Bmu_s(s)    = NaN; % SI
            Cmu_s(s)    = NaN; % SI
            Akappa_s(s) = NaN; % SI
            Bkappa_s(s) = NaN; % SI
            Ckappa_s(s) = NaN; % SI
            Dkappa_s(s) = NaN; % SI
            Ekappa_s(s) = NaN; % SI
            muRef_s(s)      = 2.117e-5; % [kg/m-s]
            TmuRef_s(s)     = 273.16;   % [K]
            Smu_s(s)        = 146.3;    % [K]
            kappaRef_s(s)   = 0.0164;   % [W/m-K]
            TkappaRef_s(s)  = 273.16;   % [K]
            Skappa_s(s)     = 146.3;    % [K]
            L_s(s)      = 0; % [-]
            sigma_s(s)  = 1; % [-]
            nAtoms_s(s) = 1; % [-]
            pCrit_s(s)  = 4.898e6; % [Pa]
            TCrit_s(s)  = 150.8; % [K]
            thetaRot_s(s) = 0; % [K] (irrelevant - neglecting rotational energy)
            thetaVib_s(s,1) = 0; % [K] (irrelevant - negelecting have no vibrational energy)
            HForm298K_s(s)  = 0; % [J/mol] (298K)
            formReac_s{s} = ' <-> ';
            bMol(s) = false;
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'mutation'
                    N_stat = 31;
                    omega_s(s,1:N_stat) = [0.0, 93144.1, 93751.0, 94554.1, 95400.2, 104102.5,   ...
                                     105463.2, 105617.7, 106087.7, 106238.0, 107054.8, 107132.2,...
                                     107290.2, 107496.9, 108723.1, 111668.3, 111818.5, 112139.4,...
                                     112750.7, 113020.9, 113426.5, 113469.0, 113643.8, 113717.1,...
                                     114148.2, 114641.5, 114805.6, 114822.5, 114862.2, 114975.5,...
                                     115367.4] * 1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [1, 5, 3, 1, 3, 3, 7, 5, 3, 5, 1, 3, 5, 3, 1, 1,...
                                            3, 5, 9, 7, 5, 5, 3, 7, 3, 5, 5, 7, 1, 3, 3]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'He'
            Mm_s(s)     = getElement_Mm('He'); % [kg/mol]
            Amu_s(s)    = NaN; % SI
            Bmu_s(s)    = NaN; % SI
            Cmu_s(s)    = NaN; % SI
            Akappa_s(s) = NaN; % SI
            Bkappa_s(s) = NaN; % SI
            Ckappa_s(s) = NaN; % SI
            Dkappa_s(s) = NaN; % SI
            Ekappa_s(s) = NaN; % SI
            muRef_s(s)      = 1.865e-5; % [kg/m-s]
            TmuRef_s(s)     = 273.16;   % [K]
            Smu_s(s)        = 72.9;     % [K]
            kappaRef_s(s)   = 0.1428;   % [W/m-K]
            TkappaRef_s(s)  = 273.16;   % [K]
            Skappa_s(s)     = 187.4;    % [K]
            L_s(s)      = 0; % [-]
            sigma_s(s)  = 1; % [-]
            nAtoms_s(s) = 1; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            thetaRot_s(s) = 0; % [K] (irrelevant - neglecting rotational energy)
            thetaVib_s(s,1) = 0; % [K] (irrelevant - negelecting have no vibrational energy)
            HForm298K_s(s)  = 0; % [J/mol] (298K)
            formReac_s{s} = ' <-> ';
            bMol(s) = false;
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'mutation'
                    N_stat = 1;
                    omega_s(s,1:N_stat) = 0.0; % [1/m]
                    gDegen_s(s,1:N_stat) = 1; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'Ne'
            Mm_s(s)     = getElement_Mm('Ne'); % [kg/mol]
            Amu_s(s)    = NaN; % SI
            Bmu_s(s)    = NaN; % SI
            Cmu_s(s)    = NaN; % SI
            Akappa_s(s) = NaN; % SI
            Bkappa_s(s) = NaN; % SI
            Ckappa_s(s) = NaN; % SI
            Dkappa_s(s) = NaN; % SI
            Ekappa_s(s) = NaN; % SI
            muRef_s(s)      = 2.975e-5; % [kg/m-s]
            TmuRef_s(s)     = 273.16;   % [K]
            Smu_s(s)        = 64.1;     % [K]
            kappaRef_s(s)   = 0.0465;   % [W/m-K]
            TkappaRef_s(s)  = 273.16;   % [K]
            Skappa_s(s)     = 88.1;    % [K]
            L_s(s)      = 0; % [-]
            sigma_s(s)  = 1; % [-]
            nAtoms_s(s) = 1; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            thetaRot_s(s) = 0; % [K] (irrelevant - neglecting rotational energy)
            thetaVib_s(s,1) = 0; % [K] (irrelevant - negelecting have no vibrational energy)
            HForm298K_s(s)  = 0; % [J/mol] (298K)
            formReac_s{s} = ' <-> ';
            bMol(s) = false;
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'mutation'
                    N_stat = 1;
                    omega_s(s,1:N_stat) = 0.0; % [1/m]
                    gDegen_s(s,1:N_stat) = 1; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'N'
            Mm_s(s)     = getElement_Mm('N'); % [kg/mol]
            if strcmp(options.modelTransport,'GuptaWilke')
            Amu_s(s)    = 0.0120; % SI
            Bmu_s(s)    = 0.5930; % SI
            Cmu_s(s)    = log(0.1) -12.3805; % SI
            else
            Amu_s(s)    = 0.011572; % SI
            Bmu_s(s)    = 0.6031679; % SI
            Cmu_s(s)    = log(0.1) -12.4327495; % SI
            end
            Akappa_s(s) = 0; % SI
            Bkappa_s(s) = 0; % SI
            Ckappa_s(s) = 0.01619; % SI
            Dkappa_s(s) = 0.55022; % SI
            Ekappa_s(s) = -12.92190+log(calPerCm_2_JPerM); % SI
            muRef_s(s)      = 1.84691e-5; % [kg/m-s]
            TmuRef_s(s)     = 273.16;   % [K]
            Smu_s(s)        = 163.5;    % [K]
            kappaRef_s(s)   = 0.041131; % [W/m-K]
            TkappaRef_s(s)  = 273.16;   % [K]
            Skappa_s(s)     = 207.4;    % [K]
            L_s(s)      = 0; % [-]
            sigma_s(s)  = 1; % [-] (irrelevant for atoms)
            nAtoms_s(s) = 1; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            thetaRot_s(s) = 0; % [K] (irrelevant - neglecting atom's rotational energy)
            thetaVib_s(s,1) = 0; % [K] (irrelevant - atoms have no vibrational energy)
            HForm298K_s(s)  = 472440; % [J/mol] mutation (298K)
%             hForm0K_s(s)  = 470800; % [J/mol] website (0K)
            HIon_s(s) = 14.53 * eV_2_JPerMol; % Gnoffo 1989 Table I
            formReac_s{s} = 'N2 <-> 2N';
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'Park90'
                    N_stat = 22;
                    omega_s(s,1:N_stat) = [0 19228 28840 83337 86193 95276 96793 103862 ...
                        104857 104902 107082 110021 110315 110486 111363 ...
                        112851 112929 114298 115107 115631 115991 116248]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [4 10 6 12 6 36 18 18 60 30 54 18 90 126 54 90 288 648 882 1152 1458 1800]; % [-]
                case 'mutation'
                    N_stat = 46;
                    omega_s(s,1:N_stat) = [0.0 19228.0 28842.0 83333.0 86197.0 88132.0 93585.0 94843.0 ...
                        95513.0 96755.0 96835.0 97795.0 99666.0 103691.0 104199.0 104627.0 104723.0  ...
                        104852.0 105014.0 105135.0 106482.0 106829.0 107014.0 107224.0 107450.0 107619.0  ...
                        109885.0 110079.0 110329.0 110442.0 110636.0 111063.0 111240.0 111499.0 111886.0  ...
                        112313.0 112668.0 112821.0 112910.0 113354.0 114120.0 114209.0 114241.0 114547.0  ...
                        115015.0 115467.0] * 1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [4 10 6 12 6 12 2 20 12 4 10 6 10 ...
                        12 6 6 28 26 20 10 2 20 12 10 4 6 12 6 90 126  ...
                        24 2 38 4 10 6 18 60 126 32 18 90 180 20 108 18]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = false;
            % NASA-7 polynomials    % From Ref. 7
            AThermNASA7_s{s} = [ 0.25031E+01  0.24820E+01  0.27480E+01 -0.12280E+01  0.15520E+02 ;    % From Ref. 7
                                -0.21800E-04  0.69258E-04 -0.39090E-03  0.19268E-02 -0.38858E-02 ;
                                 0.54205E-07 -0.63065E-07  0.13380E-06 -0.24370E-06  0.32288E-06 ;
                                -0.56476E-10  0.18387E-10 -0.11910E-10  0.12193E-10 -0.96053E-11 ;
                                 0.20999E-13 -0.11747E-14  0.33690E-15 -0.19918E-15  0.95472E-16 ;
                                 0.56130E+05  0.56130E+05  0.56130E+05  0.56130E+05  0.56130E+05 ;
                                 0.41676E+01  0.42618E+01  0.28720E+01  0.28469E+02 -0188120E+02];
            TlimsNASA7_s{s} = [300,1000,6000,15000,25000,30000];
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'O'
            Mm_s(s)     = getElement_Mm('O'); % [kg/mol]
            if strcmp(options.modelTransport,'GuptaWilke')
            Amu_s(s)    = 0.0205; % SI
            Bmu_s(s)    = 0.4257; % SI
            Cmu_s(s)    = log(0.1) -11.5803; % SI
            else
            Amu_s(s)    = 0.0203144; % SI
            Bmu_s(s)    = 0.4294404; % SI
            Cmu_s(s)    = log(0.1) -11.6031403; % SI
            end
            Akappa_s(s) = 0; % SI
            Bkappa_s(s) = 0; % SI
            Ckappa_s(s) = 0.03310; % SI
            Dkappa_s(s) = 0.22834; % SI
            Ekappa_s(s) = -11.58116+log(calPerCm_2_JPerM); % SI
            muRef_s(s)      = 2.21761e-5; % [kg/m-s]
            TmuRef_s(s)     = 273.16;   % [K]
            Smu_s(s)        = 165.7;    % [K]
            kappaRef_s(s)   = 0.045558; % [W/m-K]
            TkappaRef_s(s)  = 273.16;   % [K]
            Skappa_s(s)     = 157.27;   % [K]
            L_s(s)      = 0; % [-]
            sigma_s(s)  = 1; % [-] (irrelevant for atoms)
            nAtoms_s(s) = 1; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            thetaVib_s(s,1) = 0; % [K] (irrelevant - atoms have no vibrational energy)
            thetaRot_s(s) = 0; % [K] (irrelevant - neglecting atom's rotational energy)
            HForm298K_s(s)  = 249229; % [J/mol] mutation (298K)
%             hForm0K_s(s)  = 246800; % [J/mol] website (0K)
            HIon_s(s) = 13.614 * eV_2_JPerMol; % Gnoffo 1989 Table I
            formReac_s{s} = 'O2 <-> 2O';
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'Park90'
                    N_stat = 19;
                    omega_s(s,1:N_stat) = [78 15868 33792 73768 76795 86629 88631 95757 ...
                        97445 99313 102227 102881 103869 105394 106639 107583  ...
                        108117 108478 108734]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [9 5 1 5 3 15 9 8 40 24 8 96 24 168 288 392 512 648 800]; % [-]
                case 'mutation'
                    N_stat = 40;
                    omega_s(s,1:N_stat) = [0.0 15889.0 33794.0 73768.0 76792.0 86624.0 ...
                        88641.0 95480.0 96222.0 97513.0 97593.0 99207.0 99771.0 ...
                        101223.0 102191.0 102513.0 102755.0 102917.0 102997.0 103078.0 ...
                        103723.0 103965.0 105094.0 105256.0 105498.0 105554.0 105901.0 ...
                        105982.0 106627.0 106708.0 106869.0 107514.0 107595.0 107700.0 ...
                        108175.0 108224.0 108555.0 108595.0 108829.0 108853.0 ]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [9 5 1 5 3 15 9 5 3 25 15 15 9 15 5 3 5 25 15 56 ...
                        15 9 5 3 49 56 15 9 5 3 168 5 3 96 8 40 8 40 3 40]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = false;
            % NASA-7 polynomials
            AThermNASA7_s{s} = [ 0.28236E+01  0.25421E+01  0.25460E+01 -0.97871E-02  0.16428E+02 ;    % From Ref. 7
                                -0.89478E-03 -0.27551E-04 -0.59520E-04  0.12450E-02 -0.39313E-02 ;
                                 0.83060E-06 -0.31028E-08  0.27010E-07 -0.16154E-06  0.29840E-06 ;
                                -0.16837E-09  0.45511E-11 -0.27980E-11  0.80380E-11 -0.81613E-11 ;
                                -0.73205E-13 -0.43681E-15  0.93800E-16 -0.12624E-15  0.75004E-16 ;
                                 0.29150E+05  0.29150E+05  0.29150E+05  0.29150E+05  0.29150E+05 ;
                                 0.35027E+01  0.49203E+01  0.50490E+01  0.21711E+02 -0.94358E+02];
            TlimsNASA7_s{s} = [300,1000,6000,15000,25000,30000];
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'NO'
            Mm_s(s)     = getElement_Mm('N') + getElement_Mm('O'); % [kg/mol]
            if strcmp(options.modelTransport,'GuptaWilke')
            Amu_s(s)    = 0.0452; % SI
            Bmu_s(s)    = -0.0609; % SI
            Cmu_s(s)    = log(0.1) -9.4596; % SI
            else
            Amu_s(s)    = 0.0436378; % SI
            Bmu_s(s)    = -0.0335511; % SI
            Cmu_s(s)    = log(0.1) -9.5767430; % SI
            end
            Akappa_s(s) = 0.02792; % SI
            Bkappa_s(s) = -0.87133; % SI
            Ckappa_s(s) = 10.17967; % SI
            Dkappa_s(s) = -52.03466; % SI
            Ekappa_s(s) = 88.67060+log(calPerCm_2_JPerM); % SI
            muRef_s(s)      = 1.774e-5; % [kg/m-s]
            TmuRef_s(s)     = 273.16;   % [K]
            Smu_s(s)        = 147.7;    % [K]
            kappaRef_s(s)   = 0.02353;  % [W/m-K]
            TkappaRef_s(s)  = 273.16;   % [K]
            Skappa_s(s)     = 279.6;    % [K]
            L_s(s)      = 2; % [-]
            sigma_s(s)  = 1; % [-]
            nAtoms_s(s) = 2; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            HForm298K_s(s)  = 91089; % [J/mol] mutation (298K)
%             hForm0K_s(s)  = 90500; % [J/mol] website (0K)
            HIon_s(s) = 9.5 * eV_2_JPerMol; % Gnoffo 1989 Table I
            formReac_s{s} = 'O2 + N2 <-> 2NO';
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'Park90'
                    rBond_s     = 1.1508e-10; % [m]
                    redMass_s   = getElement_Mm('N')*getElement_Mm('O')/(getElement_Mm('N')+getElement_Mm('O')); % [kg]
                    I_s = redMass_s/nAvogadro .* rBond_s.^2; % moment of inertia
                    thetaRot_s(s) = hPlanck^2/(8*pi^2*kBoltz*I_s); % [K]
                    omega_0vc_s = 1904.2e2; % [1/m]
                    N_stat = 15;
                    thetaVib_s(s,1) = hPlanck * cLight * omega_0vc_s ./ kBoltz; % vibrational activation temperature (K)
                    omega_s(s,1:N_stat) = [0 38807 43966 45932 47950 52186 53085 53637 60364 ...
                        60629 61800 62473 62913 63040 64078]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [4 8 2 4 4 4 2 4 4 2 4 2 2 4 4]; % [-]
                case 'mutation'
                    thetaRot_s(s) = 2.464; % [K]
                    thetaVib_s(s,1) = 2759.293; % [K]
                    N_stat = 2;
                    omega_s(s,1:N_stat) = [0.0 38000.0 ]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [4   8       ]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = true;
            % NASA-7 polynomials
            AThermNASA7_s{s} = [ 0.35887E+01  0.32047E+01  0.38543E+01  0.43309E+01  0.23507E+01    % From Ref. 7
                                -0.12479E-02  0.12705E-02  0.23409E-03 -0.58086E-04  0.58643E-03
                                 0.39786E-05 -0.46603E-06 -0.21354E-07  0.28059E-07 -0.31316E-07
                                -0.28651E-08  0.75007E-10  0.16689E-11 -0.15694E-11  0.60495E-12
                                 0.63015E-12 -0.42314E-14 -0.49070E-16  0.24104E-16 -0.40557E-17
                                 0.97640E+04  0.97640E+04  0.97640E+04  0.97640E+04  0.97640E+04
                                 0.51497E+01  0.66867E+01  0.31541E+01  0.10735E+00  0.14026E+02];
            TlimsNASA7_s{s} = [300,1000,6000,15000,25000,30000];
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'N2'
            Mm_s(s)     = 2*getElement_Mm('N'); % [kg/mol]
            if strcmp(options.modelTransport,'GuptaWilke')
                Amu_s(s)    = 0.0203; % SI
                Bmu_s(s)    = 0.4329; % SI
                Cmu_s(s)    = log(0.1) -11.8153; % SI
            else
                Amu_s(s)    = 0.0268142; % SI
                Bmu_s(s)    = 0.3177838; % SI
                Cmu_s(s)    = log(0.1) -11.3155513; % SI
            end
            Akappa_s(s) = 0.03607; % SI
            Bkappa_s(s) = -1.07503; % SI
            Ckappa_s(s) = 11.95029; % SI
            Dkappa_s(s) = -57.90063; % SI
            Ekappa_s(s) = 93.21782+log(calPerCm_2_JPerM); % SI
            muRef_s(s)      = 1.656e-5; % [kg/m-s]
            TmuRef_s(s)     = 273.16;   % [K]
            Smu_s(s)        = 104.7;    % [K]
            kappaRef_s(s)   = 0.02407;  % [W/m-K]
            TkappaRef_s(s)  = 273.16;   % [K]
            Skappa_s(s)     = 178.1;    % [K]
            L_s(s)      = 2; % [-]
            sigma_s(s)  = 2; % [-]
            nAtoms_s(s) = 2; % [-]
            pCrit_s(s)  = 3.394e6; % [Pa]
            TCrit_s(s)  = 126.2; % [K]
            HForm298K_s(s)  = 0; % [J/mol] mutation (298K)
            HIon_s(s) = 15.51 * eV_2_JPerMol; % Gnoffo 1989 Table I
            formReac_s{s} = ' <-> '; % reference substance
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'Park90'
                    rBond_s     = 1.0977e-10; % [m]
                    redMass_s   = getElement_Mm('N')^2/(2*getElement_Mm('N')); % [kg]
                    I_s = redMass_s/nAvogadro .* rBond_s.^2; % moment of inertia [kg-m^2]
                    thetaRot_s(s) = hPlanck^2/(8*pi^2*kBoltz*I_s); % [K]
                    omega_0vc_s = 2358.6e2; % [1/m]
                    N_stat = 22;
                    thetaVib_s(s,1) = hPlanck * cLight * omega_0vc_s ./ kBoltz; % vibrational activation temperature (K)
                    omega_s(s,1:N_stat) = [0 50204 59619 59808 66272 68153 69283 72097 76436 ...
                        87900 89137 0 0 0 0 0 0 0 0 0 0 0]*1e2;
                    gDegen_s(s,1:N_stat) = [1 3 6 6 3 1 2 2 5 6 6 0 0 0 0 0 0 0 0 0 0 0]; % [-]
                case 'mutation'
                    thetaRot_s(s) = 2.886; % [K]
                    thetaVib_s(s,1) = 3408.464; % [K]
                    N_stat = 8;
                    omega_s(s,1:N_stat) = [0.0 50203.5 59619.2 59808.0 66272.5 68152.7 69283.0 ...
                        72097.6 ]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [1 3 6 6 3 1 2 2 ]; % [-]
                case 'Bitter2015'
                    thetaRot_s(s) = 2.886; % [K] (actually taken from mutation)
                    thetaVib_s(s,1) = 3390; % [K]
                    N_stat = 1;
                    omega_s(s,1:N_stat) = 0; % [1/m]
                    gDegen_s(s,1:N_stat) = 1; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = true;
            % NASA-7 polynomials
            AThermNASA7_s{s} = [  0.36748E+01  0.32125E+01  0.31811E+01  0.96377E+01 -0.51681E+01 ;    % From Ref. 7
                                 -0.12081E-02  0.10137E-02  0.89745E-03 -0.25728E-02  0.23337E-02 ;
                                  0.23240E-05 -0.30467E-06 -0.20216E-06  0.33020E-06 -0.12953E-06 ;
                                 -0.63218E-09  0.41091E-10  0.18266E-10 -0.14315E-10  0.27872E-11 ;
                                 -0.22577E-12 -0.20170E-14 -0.50334E-15  0.20333E-15 -0.21360E-16 ;
                                 -0.10430E+04 -0.10430E+04 -0.10430E+04 -0.10430E+04 -0.10430E+04 ;
                                  0.23580E+01  0.43661E+01  0.46264E+01 -0.37587E+02  0.66217E+02  ];
            TlimsNASA7_s{s} = [300,1000,6000,15000,25000,30000];
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'O2'
            Mm_s(s)     = 2*getElement_Mm('O'); % [kg/mol]
            if strcmp(options.modelTransport,'GuptaWilke')
            Amu_s(s)    = 0.0484; % SI
            Bmu_s(s)    = -0.1455; % SI
            Cmu_s(s)    = log(0.1) -8.9231; % SI
            else
            Amu_s(s)    = 0.0449290; % SI
            Bmu_s(s)    = -0.0826158; % SI
            Cmu_s(s)    = log(0.1) -9.2019475; % SI
            end
            Akappa_s(s) = 0.07987; % SI
            Bkappa_s(s) = -2.58428; % SI
            Ckappa_s(s) = 31.25959; % SI
            Dkappa_s(s) = -166.76267; % SI
            Ekappa_s(s) = 321.69820+log(calPerCm_2_JPerM); % SI
            muRef_s(s)      = 1.919e-5; % [kg/m-s]
            TmuRef_s(s)     = 273.16;   % [K]
            Smu_s(s)        = 125.0;    % [K]
            kappaRef_s(s)   = 0.02449;  % [W/m-K]
            TkappaRef_s(s)  = 273.16;   % [K]
            Skappa_s(s)     = 268.8;    % [K]
            L_s(s)      = 2; % [-]
            sigma_s(s)  = 2; % [-]
            nAtoms_s(s) = 2; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            HForm298K_s(s)  = 0; % [J/mol] mutation (298K)
            HIon_s(s) = 12.5 * eV_2_JPerMol; % Gnoffo 1989 Table I
            formReac_s{s} = ' <-> '; % reference substance
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'Park90'
                    rBond_s     = 1.2075e-10; % [m]
                    redMass_s   = getElement_Mm('O')^2/(2*getElement_Mm('O')); % [kg]
                    I_s = redMass_s/nAvogadro .* rBond_s.^2; % moment of inertia [kg-m^2]
                    thetaRot_s(s) = hPlanck^2/(8*pi^2*kBoltz*I_s); % [K]
                    omega_0vc_s = 1580.2e2; % [1/m]
                    N_stat = 22;
                    thetaVib_s(s,1) = hPlanck * cLight * omega_0vc_s ./ kBoltz; % vibrational activation temperature (K)
                    omega_s(s,1:N_stat) = [0 7918 13195 33057 34690 35398 39279 49793 54031 ...
                        55524 57041 67841 69123 72842 74866 75260 76091 79883 82166 0 0 0]*1e2;
                    gDegen_s(s,1:N_stat) = [3 2 1 1 6 3 10 3 3 6 6 8 8 2 2 3 1 3 1 0 0 0]; % [-]
                case 'mutation'
                    thetaRot_s(s) = 2.086; % [K]
                    thetaVib_s(s,1) = 2276.979; % [K]
                    N_stat = 7;
                    omega_s(s,1:N_stat) = [0.0 7917.5 13195.1 33055.7 34690.9 35393.1 49792.1 ]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [3 2 1 1 6 3 3 ]; % [-]
                case 'Bitter2015'
                    thetaRot_s(s) = 2.086; % [K] (actually taken from mutation)
                    thetaVib_s(s,1) = 2270; % [K]
                    N_stat = 1;
                    omega_s(s,1:N_stat) = 0; % [1/m]
                    gDegen_s(s,1:N_stat) = 1; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = true;
            % NASA-7 polynomials
            AThermNASA7_s{s} =  [0.36146E+01  0.35949E+01  0.38599E+01  0.34867E+01  0.39620E+01 ;    % From Ref. 7
                                -0.18598E-02  0.75213E-03  0.32510E-03  0.52384E-03  0.39446E-03 ;
                                 0.70814E-05 -0.18732E-06 -0.92131E-08 -0.39123E-07 -0.29506E-07 ;
                                -0.68070E-08  0.27913E-10 -0.78684E-12  0.10094E-11  0.73975E-12 ;
                                 0.21628E-11 -0.15774E-14  0.29426E-16 -0.88718E-17 -0.64209E-17 ;
                                -0.10440E+04 -0.10440E+04 -0.10440E+04 -0.10440E+04 -0.10440E+04 ;
                                 0.43628E+01  0.38353E+01  0.23789E+01  0.48179E+01  0.13985E+01 ];
            TlimsNASA7_s{s} = [300,1000,6000,15000,25000,30000];
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'N+'
            Mm_s(s)     = getElement_Mm('N')-getElement_Mm('e-'); % [kg/mol]
            Amu_s(s)    = 0; % SI
            Bmu_s(s)    = 2.5; % SI
            Cmu_s(s)    = log(0.1) -32.0453; % SI
            Akappa_s(s) = 0; % SI
            Bkappa_s(s) = 0; % SI
            Ckappa_s(s) = 0.03088; % SI
            Dkappa_s(s) = 2.06339; % SI
            Ekappa_s(s) = -31.51368+log(calPerCm_2_JPerM); % SI
            muRef_s(s)     = 1.077e-8;
            TmuRef_s(s)    = 273.16;
            Smu_s(s)       = 111.1246;
            kappaRef_s(s)  = 2.418e-5;
            TkappaRef_s(s) = 273.16;
            Skappa_s(s)    = 108.496 ; % SI
            L_s(s)      = 0; % [-]
            sigma_s(s)  = 1; % [-] (irrelevant for atoms)
            nAtoms_s(s) = 1; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            thetaRot_s(s) = 0; % [K] (irrelevant - neglecting atom's rotational energy)
            thetaVib_s(s,1) = 0; % [K] (irrelevant - atoms have no vibrational energy)
            HForm298K_s(s)  = 1881903; % [J/mol] mutation (298K)
            HIon_s(s) = 14.53 * eV_2_JPerMol; % Gnoffo 1989 Table I
            formReac_s{s} = 'N2 <-> 2N+ + 2e-'; % reference substance
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'Park90'
                    N_stat = 7;
                    omega_s(s,1:N_stat) = [89 15316 32686 92246 109219 144189 149058 ]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [9 5 1 15 9 5 12 ]; % [-]
                case 'mutation'
                    N_stat = 9;
                    omega_s(s,1:N_stat) = [0, 48.7, 130.8, 15316.2, 32688.8, 46784.6, 92237.2, 92250.3, 92251.8 ] * 1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [1 3 5 5 1 5 7 5 3 ]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = false;
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'O+'
            Mm_s(s)     = getElement_Mm('O')-getElement_Mm('e-'); % [kg/mol]
            Amu_s(s)    = 0; % SI
            Bmu_s(s)    = 2.5; % SI
            Cmu_s(s)    = log(0.1) -32.3606; % SI
            Akappa_s(s) = -0.4013; % SI
            Bkappa_s(s) = 1.32468; % SI
            Ckappa_s(s) = -16.22091; % SI
            Dkappa_s(s) = 89.96782; % SI
            Ekappa_s(s) = -208.57442+log(calPerCm_2_JPerM); % SI
            muRef_s(s)     = 1.316e-8;
            TmuRef_s(s)    = 273.16;
            Smu_s(s)       = 42.8076;
            kappaRef_s(s)  = 2.720e-5;
            TkappaRef_s(s) = 273.16;
            Skappa_s(s)    = 20.8068 ; % SI
            L_s(s)      = 0; % [-]
            sigma_s(s)  = 1; % [-] (irrelevant for atoms)
            nAtoms_s(s) = 1; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            thetaRot_s(s) = 0; % [K] (irrelevant - neglecting atom's rotational energy)
            thetaVib_s(s,1) = 0; % [K] (irrelevant - atoms have no vibrational energy)
            HForm298K_s(s)  = 1568841; % [J/mol] mutation (298K)
            HIon_s(s) = 13.614 * eV_2_JPerMol; % Gnoffo 1989 Table I
            formReac_s{s} = 'O2 <-> 2O+ + 2e-'; % reference substance
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'Park90'
                    N_stat = 6;
                    omega_s(s,1:N_stat) = [0 26816 40467 119933 164791 186604 ]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [4 10 6 12 10 18 ]; % [-]
                case 'mutation'
                    N_stat = 5;
                    omega_s(s,1:N_stat) = [0, 26808.4, 26829.4, 40466.9, 40468.4 ]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [4 6 4 4 2 ]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = false;
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'NO+'
            Mm_s(s)     = getElement_Mm('N') + getElement_Mm('O') - getElement_Mm('e-'); % [kg/mol]
            if strcmp(options.modelTransport,'GuptaWilke')
            Amu_s(s)    = 0; % SI
            Bmu_s(s)    = 2.5; % SI
            Cmu_s(s)    = log(0.1) -32.0453; % SI
            else
            Amu_s(s)    = 0.3020141; % SI
            Bmu_s(s)    = -3.5039791; % SI
            Cmu_s(s)    = log(0.1) -3.7355157; % SI
            end
            Akappa_s(s) = -0.06836; % SI
            Bkappa_s(s) = 2.57829; % SI
            Ckappa_s(s) = -35.72737; % SI
            Dkappa_s(s) = 219.09215; % SI
            Ekappa_s(s) = -519.00261+log(calPerCm_2_JPerM); % SI
            muRef_s(s)     = 1.535e-8;
            TmuRef_s(s)    = 273.16;
            Smu_s(s)       = 128.59;
            kappaRef_s(s)  = 1.394e-5;
            TkappaRef_s(s) = 273.16;
            Skappa_s(s)    = 223.175 ; % SI
            L_s(s)      = 2; % [-]
            sigma_s(s)  = 1; % [-]
            nAtoms_s(s) = 2; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            HForm298K_s(s)  = 990653; % [J/mol] mutation (298K)
            HIon_s(s) = 9.5 * eV_2_JPerMol; % Gnoffo 1989 Table I
            formReac_s{s} = 'N2 + O2 <-> 2NO+ + 2e-'; % reference substance
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'Park90'
                    rBond_s     = 1.0632e-10; % [m]
                    redMass_s   = getElement_Mm('N')*getElement_Mm('O')/(getElement_Mm('N')+getElement_Mm('O')); % [kg]
                    I_s = redMass_s/nAvogadro .* rBond_s.^2; % moment of inertia [kg-m^2]
                    thetaRot_s(s) = hPlanck^2/(8*pi^2*kBoltz*I_s); % [K]
                    omega_0vc_s = 2376.4e2; % [1/m]
                    N_stat = 8;
                    thetaVib_s(s,1) = hPlanck * cLight * omega_0vc_s ./ kBoltz; % vibrational activation temperature (K)
                    omega_s(s,1:N_stat) = [0 52190 59240 61880 67720 69540 71450 73472 ]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [1 3 6 6 3 1 2 2  ]; % [-]
                case 'mutation'
                    thetaRot_s(s) = 2.892; % [K]
                    thetaVib_s(s,1) = 3473.491; % [K]
                    N_stat = 8;
                    omega_s(s,1:N_stat) = [0 52190 59225 61883 67745 69542 71452 73474 ]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [1 3 6 6 3 1 2 2   ]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = true;
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'N2+'
            Mm_s(s)     = 2*getElement_Mm('N') - getElement_Mm('e-'); % [kg/mol]
            Amu_s(s)    = 0; % SI
            Bmu_s(s)    = 2.5; % SI
            Cmu_s(s)    = log(0.1) -32.0827; % SI
            Akappa_s(s) = 0; % SI
            Bkappa_s(s) = -0.03723; % SI
            Ckappa_s(s) = 0.84192; % SI
            Dkappa_s(s) = -3.59040; % SI
            Ekappa_s(s) = -18.65620+log(calPerCm_2_JPerM); % SI
            muRef_s(s)     = 1.576e-8;
            TmuRef_s(s)    = 273.16;
            Smu_s(s)       = 91.3302;
            kappaRef_s(s)  = 2.600e-5;
            TkappaRef_s(s) = 273.16;
            Skappa_s(s)    = 218.54 ; % SI
            L_s(s)      = 2; % [-]
            sigma_s(s)  = 2; % [-]
            nAtoms_s(s) = 2; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            HForm298K_s(s)  = 1509509; % [J/mol] mutation (298K)
            HIon_s(s) = 15.51 * eV_2_JPerMol; % Gnoffo 1989 Table I
            formReac_s{s} = 'N2 <-> N2+ + e-'; % reference substance
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'Park90'
                    rBond_s     = 1.1164e-10; % [m]
                    redMass_s   = getElement_Mm('N')^2/(2*getElement_Mm('N')); % [kg]
                    I_s = redMass_s/nAvogadro .* rBond_s.^2; % moment of inertia [kg-m^2]
                    thetaRot_s(s) = hPlanck^2/(8*pi^2*kBoltz*I_s); % [K]
                    omega_0vc_s = 2207e2; % [1/m]
                    thetaVib_s(s,1) = hPlanck * cLight * omega_0vc_s ./ kBoltz; % vibrational activation temperature (K)
                    N_stat = 5;
                    omega_s(s,1:N_stat) = [0 9167 25461 52318 64608 ]*1e2;
                    gDegen_s(s,1:N_stat) = [2 4 4 4 4 ]; % [-]
                case 'mutation'
                    thetaRot_s(s) = 2.818; % [K]
                    thetaVib_s(s,1) = 3253.157; % [K]
                    N_stat = 9;
                    omega_s(s,1:N_stat) = [0 9167 25461 25500 41600 46000 52815 53000 57000 ]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [2 4 2 4 8 8 4 4 4 ]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = true;
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'O2+'
            Mm_s(s)     = 2*getElement_Mm('O') - getElement_Mm('e-'); % [kg/mol]
            Amu_s(s)    = 0; % SI
            Bmu_s(s)    = 2.5; % SI
            Cmu_s(s)    = log(0.1) -32.0148; % SI
            Akappa_s(s) = -0.08373; % SI
            Bkappa_s(s) = 2.75459; % SI
            Ckappa_s(s) = -33.74529; % SI
            Dkappa_s(s) = 185.13274; % SI
            Ekappa_s(s) = -401.50753+log(calPerCm_2_JPerM); % SI
            muRef_s(s)     = 1.541e-8;
            TmuRef_s(s)    = 273.16;
            Smu_s(s)       = 145.659;
            kappaRef_s(s)  = 2.328e-5;
            TkappaRef_s(s) = 273.16;
            Skappa_s(s)    = 310.956 ; % SI
            L_s(s)      = 2; % [-]
            sigma_s(s)  = 2; % [-]
            nAtoms_s(s) = 2; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            HForm298K_s(s)  = 1171413; % [J/mol] mutation (298K)
            HIon_s(s) = 12.5 * eV_2_JPerMol; % Gnoffo 1989 Table I
            formReac_s{s} = 'O2 <-> O2+ + e-'; % reference substance
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'Park90'
                    rBond_s     = 1.1164e-10; % [m]
                    redMass_s   = getElement_Mm('O')^2/(2*getElement_Mm('O')); % [kg]
                    I_s = redMass_s/nAvogadro .* rBond_s.^2; % moment of inertia [kg-m^2]
                    thetaRot_s(s) = hPlanck^2/(8*pi^2*kBoltz*I_s); % [K]
                    omega_0vc_s = 1904.7e2; % [1/m]
                    thetaVib_s(s,1) = hPlanck * cLight * omega_0vc_s ./ kBoltz; % vibrational activation temperature (K)
                    N_stat = 4;
                    omega_s(s,1:N_stat) = [0 32964 40669 49553 ]*1e2;
                    gDegen_s(s,1:N_stat) = [4 8 4 4 ]; % [-]
                case 'mutation'
                    thetaRot_s(s) = 2.475; % [K]
                    thetaVib_s(s,1) = 2887.139; % [K]
                    N_stat = 5;
                    omega_s(s,1:N_stat) = [0 32913 40572 40600 43300 ]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [4 8 4 4 6 ]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = true;
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'e-'
            Mm_s(s)     = getElement_Mm('e-'); % [kg/mol]
            Amu_s(s)    = 0; % SI
            Bmu_s(s)    = 2.5; % SI
            Cmu_s(s)    = log(0.1) -37.44725; % SI
            Akappa_s(s) = 0; % SI
            Bkappa_s(s) = 0; % SI
            Ckappa_s(s) = 0.00032; % SI
            Dkappa_s(s) = 2.49375; % SI
            Ekappa_s(s) = -27.89805+log(calPerCm_2_JPerM); % SI
            muRef_s(s)     = 9.015e-11;
            TmuRef_s(s)    = 273.16;
            Smu_s(s)       = 2.5527e-4;
            kappaRef_s(s)  = 5.152e-3;
            TkappaRef_s(s) = 273.16;
            Skappa_s(s)    = 2.3059e-2 ; % SI
            L_s(s)      = 0; % [-] (irrelevant for atoms/electrons)
            sigma_s(s)  = 1; % [-] (irrelevant for atoms/electrons)
            nAtoms_s(s) = 1; % [-] (not physically true, but okay since this variable is just a flag for incorporating rotational energy, which is neglected for electrons)
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            HForm298K_s(s)  = 0; % [J/mol] mutation (298K)
            formReac_s{s} = ' <-> '; % reference substance
            thetaRot_s(s) = 0; % [K] (irrelevant - neglecting electron's rotational energy)
            thetaVib_s(s,1) = 0; % [K] (irrelevant - electrons have no vibrational energy)
            N_stat = 0;
            omega_s(s,1:N_stat) = zeros(1,N_stat); % [1/m] (electrons have no electronic energy)
            gDegen_s(s,1:N_stat) = zeros(1,N_stat); % [-]
            bMol(s) = false;
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'CO2'
            Mm_s(s)     = getElement_Mm('C') + 2*getElement_Mm('O'); % [kg/mol]
            Amu_s(s)    = -0.0195274; % SI % From Mortensen Thesis
            Bmu_s(s)    = 1.0478180; % SI
            Cmu_s(s)    = log(0.1) -14.3221200; % SI
            Akappa_s(s) = NaN; % SI
            Bkappa_s(s) = NaN; % SI
            Ckappa_s(s) = NaN; % SI
            Dkappa_s(s) = NaN; % SI
            Ekappa_s(s) = NaN; % SI
            muRef_s(s)      = 1.38e-5;  % [kg/m-s]
            TmuRef_s(s)     = 273.16;   % [K]
            Smu_s(s)        = 253.0;    % [K]
            kappaRef_s(s)   = 0.0146;   % [W/m-K]
            TkappaRef_s(s)  = 273.16;   % [K]
            Skappa_s(s)     = 939.8;    % [K]
            L_s(s)      = 2; % [-]
            sigma_s(s)  = 2; % [-]
            nAtoms_s(s) = 3; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            HForm298K_s(s)  = -393472; % [J/mol] mutation (298K)
            formReac_s{s} = 'Cgr + O2 <-> CO2';
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'mutation'
                    thetaRot_s(s) = 0.563; % [K]
                    thetaVib_s(s,:) = [932.109,932.109,1914.081,3373.804]; % [K]
                    N_stat = 5;
                    omega_s(s,1:N_stat) = [0.0 30000,33000,36000,45000]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [1, 3,    6,    3,    2]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = true;
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'C'
            Mm_s(s)     = getElement_Mm('C'); % [kg/mol]
            Amu_s(s)    =  -0.0001000; % SI % From Mortensen Thesis
            Bmu_s(s)    =   0.7928000; % SI
            Cmu_s(s)    = log(0.1) -13.4154000; % SI
            Akappa_s(s) = NaN; % SI
            Bkappa_s(s) = NaN; % SI
            Ckappa_s(s) = NaN; % SI
            Dkappa_s(s) = NaN; % SI
            Ekappa_s(s) = NaN; % SI
            L_s(s)      = 0; % [-]      = 0; % [-]
            sigma_s(s)  = 1; % [-] (irrelevant for atoms)
            nAtoms_s(s) = 1; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            HForm298K_s(s)  = 716680; % [J/mol] mutation (298K)
            formReac_s{s} = 'Cgr <-> C';
            thetaRot_s(s) = 0; % [K]
            thetaVib_s(s,1) = 0; % [K]
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'mutation'
                    N_stat = 15;
                    omega_s(s,1:N_stat) = [0.0,16.4,43.4,10192.6,21648.0,33735.2,64086.9,64091.0,64089.9,75254.0,75255.3,75256.1,97878.0,105798.7,119878.0]*1e2;   % [1/m]
                    gDegen_s(s,1:N_stat)= [1,  3,   5,   5,      1,      5,      7,      5,      3,      3,      5,      1,      5,      3,       3];       % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = false;
            % Sublimation parameters
            alfaSubl_s(s) = 0.14;   % [-]
            PVapP_s(s) = -85715;    % [K]
            QVapP_s(s) = 18.69;     % [-]
        case 'CO'
            Mm_s(s)     = getElement_Mm('C') + getElement_Mm('O'); % [kg/mol]
            Amu_s(s)    =  -0.0195274; % SI
            Bmu_s(s)    =   1.0132950; % SI
            Cmu_s(s)    = log(0.1) -13.9787300; % SI
            Akappa_s(s) = NaN; % SI
            Bkappa_s(s) = NaN; % SI
            Ckappa_s(s) = NaN; % SI
            Dkappa_s(s) = NaN; % SI
            Ekappa_s(s) = NaN; % SI
            L_s(s)      = 2; % [-]
            sigma_s(s)  = 1; % [-]
            nAtoms_s(s) = 2; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            HForm298K_s(s)  = -110530; % [J/mol] mutation (298K)
            formReac_s{s} = '2Cgr + O2 <-> 2CO';
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'mutation'
                    thetaRot_s(s) = 2.782; % [K]
                    thetaVib_s(s,1) = 3083.452; % [K]
                    N_stat = 10;
                    omega_s(s,1:N_stat) = [0.0,48686.79,55825.65,61123.9,64230.24,65075.77,65084.4,65928.0,80000.0,81000.0]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [1,        6,       3,      6,       3,       2,      1,      2,     10,      5]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = true;
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        case 'C3'
            Mm_s(s)     = 3*getElement_Mm('C'); % [kg/mol]
            Amu_s(s)    =  -0.0147000; % SI
            Bmu_s(s)    =   0.8811000; % SI
            Cmu_s(s)    = log(0.1) -13.5051000; % SI
            Akappa_s(s) = NaN; % SI
            Bkappa_s(s) = NaN; % SI
            Ckappa_s(s) = NaN; % SI
            Dkappa_s(s) = NaN; % SI
            Ekappa_s(s) = NaN; % SI
            L_s(s)      = 2; % [-]
            sigma_s(s)  = 2; % [-]
            nAtoms_s(s) = 3; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            HForm298K_s(s)  = 823630; % [J/mol] mutation (298K)
            formReac_s{s} = '3Cgr <-> C3';
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'mutation'
                    thetaRot_s(s) = 0.607; % [K]
                    thetaVib_s(s,:) = [212.582, 212.582, 1723.379 , 2990.460]; % [K]
                    N_stat = 10;
                    omega_s(s,1:N_stat) = [0.0, 14000.0, 21500.0, 23800.0, 24675.5, 29100.0, 32800.0, 32900.0, 33700.0, 40500.0]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [ 1,       6,       6,        3,      2,       6,       3,        1,      2,       2]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = true;
            % Sublimation parameters
            alfaSubl_s(s) = 0.03;   % [-]
            PVapP_s(s) = -93227;    % [K]
            QVapP_s(s) = 23.93;     % [-]
        case 'C2'
            Mm_s(s)     = 2*getElement_Mm('C'); % [kg/mol]
            Amu_s(s)    =  -0.0031000; % SI
            Bmu_s(s)    =   0.6920000; % SI
            Cmu_s(s)    = log(0.1) -12.6127000; % SI
            Akappa_s(s) = NaN; % SI
            Bkappa_s(s) = NaN; % SI
            Ckappa_s(s) = NaN; % SI
            Dkappa_s(s) = NaN; % SI
            Ekappa_s(s) = NaN; % SI
            L_s(s)      = 2; % [-]
            sigma_s(s)  = 2; % [-]
            nAtoms_s(s) = 2; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            HForm298K_s(s)  = 828374; % [J/mol] mutation (298K)
            formReac_s{s} = '2Cgr <-> C2';
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'mutation'
                    thetaRot_s(s) = 2.579; % [K]
                    thetaVib_s(s,1) = 2603.551; % [K]
                    N_stat = 27;
                    omega_s(s,1:N_stat) = [0.0, 716.2, 6434.3, 8391.3, 9227.4, 15180.0, 18740.0, 20022.5, 22500.0, 30475.0, 34261.9, 34875.0, 35175.0,...
                        38300.0, 39365.0, 40796.7, 41000.0, 42600.0, 43100.0, 43239.8, 43450.0, 44750.0, 51850.0, 52850.0, 55034.9, 55400.0, 63100.0]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [ 1,     6,      3,      2,      3,       2,       1,       6,      10,       5,       2,       6,       1,...
                              6,       2,       6,       6,       6,       2,       1,       3,       6,       1,       2,       1,       2,       2]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = true;
            % Sublimation parameters
            alfaSubl_s(s) = 0.26;   % [-]
            PVapP_s(s) = -98363;    % [K]
            QVapP_s(s) = 22.20;     % [-]
        case 'CN'
            Mm_s(s)     = getElement_Mm('C') + getElement_Mm('N'); % [kg/mol]
            Amu_s(s)    =  -0.0025000; % SI
            Bmu_s(s)    =   0.6810000; % SI
            Cmu_s(s)    = log(0.1) -12.4914000; % SI
            Akappa_s(s) = NaN; % SI
            Bkappa_s(s) = NaN; % SI
            Ckappa_s(s) = NaN; % SI
            Dkappa_s(s) = NaN; % SI
            Ekappa_s(s) = NaN; % SI
            L_s(s)      = 2; % [-]
            sigma_s(s)  = 1; % [-]
            nAtoms_s(s) = 2; % [-]
            pCrit_s(s)  = NaN; % [Pa]
            TCrit_s(s)  = NaN; % [K]
            HForm298K_s(s)  = 439970; % [J/mol] mutation (298K)
            formReac_s{s} = '2Cgr + N2 <-> 2CN';
            switch options.modelEnergyModes % depending on where we get our electronic levels from, we will have more or less
                case 'mutation'
                    thetaRot_s(s) = 2.761; % [K]
                    thetaVib_s(s,1) = 2992.786; % [K]
                    N_stat = 18;
                    omega_s(s,1:N_stat) = [0.0, 9240.0, 25752.0, 32400.0, 43000.0, 47700.0, 53800.0, 54486.0, 59151.2, 60095.5, 61155.0, 62400.0, 63700.0, 64224.0, 64000.0, 69500.0, 70200.0, 70700.0]*1e2; % [1/m]
                    gDegen_s(s,1:N_stat) = [ 2,      4,       2,       4,       8,       8,       4,       4,       2,       4,       4,       4,       4,       4,       2,       8,       2,       4]; % [-]
                otherwise
                    error(['the source ''',options.modelEnergyModes,''' for the electronic energy levels is not available for ',spec_list{s}]);
            end
            bMol(s) = true;
            % Sublimation parameters
            alfaSubl_s(s) = 0;          % no sublimation
            PVapP_s(s) = 0;
            QVapP_s(s) = 0;
        otherwise
            error(['species ''',spec_list{s},''' not identified']);
    end
end

% Overwriting the viscosity curve fits if the user chooses to
if options.inputBlottner && (isfield(options.inputCustomConstants,'Amu_s') || ...
        isfield(options.inputCustomConstants,'Bmu_s') || isfield(options.inputCustomConstants,'Cmu_s'))
    Amu_s = options.inputCustomConstants.Amu_s;
    Bmu_s = options.inputCustomConstants.Bmu_s;
    Cmu_s = options.inputCustomConstants.Cmu_s;
end

% Calculating derivate properties
R_s = R0./Mm_s;
thetaElec_s = hPlanck * cLight * omega_s ./ kBoltz; % electronic activation temperatures

% Obtaining annonymous functions for the thermal-coefficient polynomial fits
switch options.modelThermal                                         % depending on the thermal model
    case {'RRHO','RRHO-1LA'}                                            % RRHO-based
        AThermFuncs_s = struct;                                             % dummy function
        HForm0K_s = getSpecies_HForm0K(formReac_s,spec_list,R_s,Mm_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,HForm298K_s,options); % formation enthalpy
        hForm_s = HForm0K_s./Mm_s;                                          % passing from J/mol to J/kg
        if ismember('Air',spec_list)                                        % assembling formation enthalpy for mono-species mixture 'Air'
            hForm_s(strcmp(spec_list,'Air')) = 0.7671*hForm_s(strcmp(spec_list,'N2')) + 0.2329*hForm_s(strcmp(spec_list,'O2'));
        end
        if options.enthalpyOffset                                           % changing origin of enthalpies such that there are no negative enthalpies
            hOffSet = min(hForm_s(~bElectron & idx_notGhost)); 
            hForm_s = hForm_s - hOffSet;
        end
    case {'NASA7','customPoly7'}                                        % polynomial-fit-based
        switch options.modelThermal                                         % depending on the fit
            case 'NASA7'                                                        % NASA-7
                ATherm_s = AThermNASA7_s;                                           % obtaining coefficients
                Tlims_s = TlimsNASA7_s;                                             % obtaining temperature-range limits
            case 'customPoly7'                                                  % customized 7-coefficient polynomial fit
                ATherm_s = options.customPoly.ATherm_s;                             % obtaining coefficients
                Tlims_s  = options.customPoly.Tlims_s;                              % obtaining temperature-range limits
            otherwise                                                           % break
                error(['The chosen modelThermal ''',options.modelThermal,''' is not supported']);
        end
        if options.enthalpyOffset
            hOffSet = min(cellfun(@(cll)cll(6,1),ATherm_s).*R_s);
            for s=N_spec:-1:1
                ATherm_s{s}(6,:) = ATherm_s{s}(6,:)-hOffSet/R_s(s);
            end
        end
        [ATherm_s,Tlims_s] = apply_clipoffThermalCoefs(ATherm_s,Tlims_s);   % clipping off at the extremes of the temperature range
        AThermFuncs_s = populate_thermalPolyCoefs(ATherm_s,Tlims_s,options.bLinearInterpThermalPoly); % populating anonymous functions
        for s=N_spec:-1:1                                                   % looping species
            hForm_s(s,1) = R_s(s)*AThermFuncs_s(s).A6(0);                         % obtaining formation enthalpies at 0K
        end
    otherwise
        error(['The chosen modelThermal ''',options.modelThermal,''' is not supported']);
end

% Neglecting energies if necessary
if options.neglect_hRot;        thetaRot_s(:) = 0;
end
if options.neglect_hVib;        thetaVib_s(:) = 0;
end
if options.neglect_hElec;       thetaElec_s(:) = 0;
                                gDegen_s(:) = 0;
end
hIon_s = HIon_s./Mm_s;

% Removing ghost species
Mm_s            = Mm_s(idx_notGhost);
Amu_s           = Amu_s(idx_notGhost);
Bmu_s           = Bmu_s(idx_notGhost);
Cmu_s           = Cmu_s(idx_notGhost);
Akappa_s        = Akappa_s(idx_notGhost);
Bkappa_s        = Bkappa_s(idx_notGhost);
Ckappa_s        = Ckappa_s(idx_notGhost);
Dkappa_s        = Dkappa_s(idx_notGhost);
Ekappa_s        = Ekappa_s(idx_notGhost);
muRef_s         = muRef_s(idx_notGhost);
TmuRef_s        = TmuRef_s(idx_notGhost);
Smu_s           = Smu_s(idx_notGhost);
kappaRef_s      = kappaRef_s(idx_notGhost);
TkappaRef_s     = TkappaRef_s(idx_notGhost);
Skappa_s        = Skappa_s(idx_notGhost);
R_s             = R_s(idx_notGhost);
nAtoms_s        = nAtoms_s(idx_notGhost);
thetaVib_s      = thetaVib_s(idx_notGhost,:);
thetaRot_s      = thetaRot_s(idx_notGhost);
L_s             = L_s(idx_notGhost);
sigma_s         = sigma_s(idx_notGhost);
thetaElec_s     = thetaElec_s(idx_notGhost,:);
gDegen_s        = gDegen_s(idx_notGhost,:);
bElectron       = bElectron(idx_notGhost);
hForm_s         = hForm_s(idx_notGhost);
HForm298K_s     = HForm298K_s(idx_notGhost);
hIon_s          = hIon_s(idx_notGhost);
bPos            = bPos(idx_notGhost);
bMol            = bMol(idx_notGhost);
alfaSubl_s      = alfaSubl_s(idx_notGhost);
PVapP_s         = PVapP_s(idx_notGhost);
QVapP_s         = QVapP_s(idx_notGhost);

% Other parameters needed
Keq_struc.Mm_s        = Mm_s;
Keq_struc.thetaRot_s  = thetaRot_s;
Keq_struc.L_s         = L_s;
Keq_struc.sigma_s     = sigma_s;
Keq_struc.R_s         = R_s;
Keq_struc.hPlanck     = hPlanck;
Keq_struc.kBoltz      = kBoltz;
Keq_struc.nAvogadro   = nAvogadro;
Keq_struc.nAtoms_s    = nAtoms_s;
Keq_struc.thetaVib_s  = thetaVib_s;
Keq_struc.thetaElec_s = thetaElec_s;
Keq_struc.gDegen_s    = gDegen_s;
Keq_struc.bElectron   = bElectron;
Keq_struc.bMol        = bMol;
Keq_struc.hForm_s     = hForm_s;

% Gas-surface interaction reactions
gasSurfReac_struct.alfaSubl_s   = alfaSubl_s;
gasSurfReac_struct.PVapP_s      = PVapP_s;
gasSurfReac_struct.QVapP_s      = QVapP_s;

if any(L_s==3)
    error('One of the species is non linear (L_s=3). The functions computing the rotational heat capacity and enthalpy are not ready for that.');
end

end % getSpecies_constants
