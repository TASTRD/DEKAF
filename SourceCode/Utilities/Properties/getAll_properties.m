function [varargout] = getAll_properties(mixCnst,eta,p,y_s,hT,Tv,options,varargin)
% getAll_properties computes all the necessary thermodynamic and transport
% properties for the chosen flow assumption.
%
% Usage:
%   (1)  --> typical 2T-CNE call
%       [outputs] = getAll_properties(mixCnst,eta,p,y_s,T,Tv,options)
%
%   (2)  --> typical 1T-CNE call
%       [outputs] = getAll_properties(mixCnst,eta,p,y_s,T,[],options)
%
%   (3)  --> 2T-CNE call providing the enthalpy rather than T
%       [outputs] = getAll_properties(mixCnst,eta,p,y_s,h,Tv,options,'passing_h',Tguess)
%
%   (4)  --> 1T-CNE call providing the enthalpy rather than T
%       [outputs] = getAll_properties(mixCnst,eta,p,y_s,h,[],options,'passing_h',Tguess)
%
%   (5)  --> typical CPG call
%       [outputs] = getAll_properties(mixCnst,eta,p,y_s,T,[],options,cp,R)
%
%   (6.1)  --> typical LTE call
%       [outputs] = getAll_properties(mixCnst,eta,p,[],T,[],options)
%
%   (6.2)  --> typical LTEED call
%       [outputs] = getAll_properties(mixCnst,eta,p,X_E,T,[],options)
%
%   (6.3)  --> to speed up the LTE evaluation by giving a good X_s guess 
%       [outputs] = getAll_properties(...,'guessXsLTE',X_sGuess)
%
%   (7)  --> to correct the temperature boundary condition according to and
%          return also the deltas on the g variables. Requires accurate eta
%       [outputs] = getAll_properties(...,'correctTBC',i_xi)
%
%   (8)  --> to reinforce the diffusion fluxes to be self-consistent in the
%          FOCE_Effective model
%       [outputs] = getAll_properties(...,'selfConsistent',dys_deta)
%
%   (9)  --> to speed up the evaluation by not checking the option defaults
%       [outputs] = getAll_properties(...,'noOptionCheck')
%
%   The list and order of outputs FOR ALL CASES is:
%       [rho , mu , kappa , TTrans , TRot , TVib , TElec , Tel , h , cp , cv , Mm , R , ...
%           y_s , X_s , h_s , D_s , nTfrac_s , source_s , dsources_dT , dsources_dym , kappav , ...
%           hv , cptr , cpv , htr_s , hv_s , sourcev , dsourcev_dT , dsourcev_dym , dcpv_dTv , cpv_s , ...
%           y_E , A_E , C_EF , dys_dYE , dXs_dyE , dXs_dXE ]
%   Certain of these outputs are exclusive to one or another flow
%   assumption, and will be returned empty or with zeros for others (see
%   below)
%
% Inputs and outputs:
%   mixCnst
%       structure with the constants specific to the mixture necessary to
%       obtain the various flow properties.
%   eta             [-]        N_eta x 1
%       wall-normal position vector. It is only used for some estimiations
%       and first guesses, and can be left empty most times 
%   p               [Pa]       N_eta x 1
%       pressure 
%   y_s             [-]        N_eta x N_spec
%       mass fraction 
%       it will be identical to the input y_s for all flow assumptions
%       except for LTE, where it is computed based on p and T
%   X_s             [-]        N_eta x N_spec
%       mole fraction 
%   h               [J/kg]     N_eta x N_spec
%       static enthalpy 
%   T               [K]        N_eta x 1
%       equilibrium or translational-rotational temperature 
%   Tv              [K]        N_eta x 1
%       vibrational-electronic-electron temperature 
%   Tguess          [K]        N_eta x 1
%       initial guess for the NR on T when h is passed 
%       if left empty it is estimated assuming CPG conditions
%   H_e             [J/kg]     1 x 1
%       Total enthalpy at the BL edge 
%   i_xi            [-]        1 x 1
%       indicator of the current streamwise position - used to fix the
%       thermal boundary condition passed through options.trackerTwall and
%       options.trackerTvwall 
%   options
%       structure containing various options defining how the properties
%       must be computed, such as the models and so on. See <a href="matlab:help setDefaults">setDefaults</a>
%
%   rho             [kg/m^3]   N_eta x 1
%       mixture density 
%   mu              [kg/m-s]   N_eta x 1
%       mixture viscosity 
%   kappa           [W/m-K]    N_eta x 1
%       mixture thermal conductivity . This will be either the
%       frozen (everything but LTE) or equilibrium value (LTE)
%   cp              [J/kg-K]   N_eta x 1
%       heat capacity at constant pressure 
%   cv              [J/kg-K]   N_eta x 1
%       heat capacity at constant volume 
%   Mm              [kg/mol]   N_eta x 1
%       mixture molar mass 
%   R               [J/kg-K]   N_eta x 1
%       specific gas constant 
%   h_s             [J/kg]     N_eta x N_spec
%       species enthalpy 
%   D_s             [m^2/s]    (N_eta x N_spec) or (N_eta x N_spec(s) x N_spec(l))
%       species diffusion coefficients. This variable acts as either D_s or
%       D_sl, depending on the diffusion moodel 
%   nTfrac_s         [-]       N_eta x N_spec
%       factor converting each of the species mole fraction into pressure
%       fractions.  See <a href="matlab:help getSpecies_nTfrac">getSpecies_nTfrac</a>
%   source_s        [kg/s]     N_eta x N_spec
%       species chemical source term 
%   dsources_dT     [kg/s-K]   N_eta x N_spec x N_T
%       derivative of the species chemical source term wrt each temperature
%       
%   dsources_dym    [kg/s]     N_eta x N_spec(m) x N_spec(s)
%       derivative of the species chemical source term wrt each mass
%       fraction 
%   kappav          [W/m-K]    N_eta x 1
%       vibrartional-electronic-electron thermal conductivity 
%   hv              [J/kg]     N_eta x 1
%       vibrartional-electronic-electron enthalpy 
%   cptr            [J/kg-K]   N_eta x 1
%       translational-rotational heat capacity 
%   cpv             [J/kg-K]   N_eta x 1
%       vibrartional-electronic-electron heat capacity 
%   htr_s           v[J/kg]    N_eta x N_spec
%       species translational-rotational enthalpy 
%   hv_s            [J/kg]     N_eta x N_spec
%       species vibrartional-electronic-electron enthalpy 
%   sourcev         [J/s]      N_eta x 1
%       vibrational-electronic-electron energy source term 
%   dsourcev_dT     [J/s-K]    N_eta x N_T
%       derivative of the vibrational-electronic-electron energy source
%       term wrt each temperature 
%   dsourcev_dym    [J/s]      N_eta x N_spec
%       derivative of the vibrational-electronic-electron energy source
%       term wrt each mass fraction 
%   dcpv_dT         [J/kg-K^2] N_eta x 1
%       (vib-elec-el) temperature derivative of the
%       vibrartional-electronic-electron heat capacity
%   cpv_s           [J/kg-K]   N_eta x 1
%       species vib-elec-el heat capacity 
%   y_E             [-]        N_eta x N_elem
%       elemental mass fraction
%   A_E             [kg/m-s-K] N_eta x N_elem
%       elemental thermodiffusion coefficient of each element E
%   C_EF            [kg/m-s]   N_eta x N_elem(E) x N_elem(F)
%       elemental molar diffusion coefficient of each element E into
%       element F.
%   dys_dyE         [-]         N_eta x N_spec x N_elem
%       gradient of the equilibrium mass fraction of each species (s) with
%       each elemental mass fracion (E)
%   dXs_dyE         [-]         N_eta x N_spec x N_elem
%       gradient of the equilibrium mole fraction of each species (s) with
%       each elemental mass fracion (E)
%   dXs_dXE         [-]         N_eta x N_spec x N_elem
%       gradient of the equilibrium mole fraction of each species (s) with
%       each elemental mole fracion (E)
%
% Author: Fernando Miro Miro
% Date: July 2019
% GNU Lesser General Public License 3.0

% Checking inputs
if isempty(eta);    eta = 1:length(p);      end
[varargin,bCorrectTBC,    idxH]     = find_flagAndRemove('correctTBC',varargin);            if bCorrectTBC;     i_xi = varargin{idxH};      varargin(idxH) = [];  end
[varargin,bSelfConsistent,idxSC]    = find_flagAndRemove('selfConsistent',varargin);        if bSelfConsistent; dys_deta = varargin{idxSC}; varargin(idxSC) = []; end
[varargin,bNoOptionCheck]           = find_flagAndRemove('noOptionCheck' ,varargin);
[varargin,bEnthalpy,idxT]           = find_flagAndRemove('passing_h' ,varargin);

if ~bNoOptionCheck
    options = setDefaults(options);
end

N_eta = length(p);
N_spec = mixCnst.N_spec;
N_reac = length(mixCnst.A_r);

switch options.flow                                     % elemental-mole-fraction arrangements for equilibrium cases
    case 'LTE';     X_E = repmat(mixCnst.XEq,[N_eta,1]);    % constant elemental mole fraction
    case 'LTEED';   X_E = y_s;                              % storing elemental mole fraction passed as an input through y_s (see usage 6.2)
end

% Preparing necessary inputs for getMixture_h_cp. It is the only get*
% function that requires reshaped inputs. This is because it must be
% optimized for the NR iteration (the most time-demanding in the code's
% overall run time)
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,~] = ...
    reshape_inputs4enthalpy(mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,hT);

if bEnthalpy                    %%%%% THE USER PASSES h and we must compute T
    h = hT;
    Tguess = varargin{idxT};    varargin(idxT) = [];
    [TTrans,TRot,TVib,TElec,Tel,~,y_s,dXs_dT,dXs_dyE,dys_dT,~] = getMixture_T(h,y_s,p,mixCnst,options,Tv,varargin{:},'Tguess',Tguess);
else                            %%%%% THE USER PASSES T
    % extracting inputs
    TTrans  = hT;
    TRot    = hT;
    switch options.numberOfTemp
        case '1T'
            TVib = hT;
            TElec = hT;
            Tel = hT;
        case '2T'
            TVib = Tv;
            TElec = Tv;
            Tel = Tv;
        otherwise
            error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
    end
    switch options.flow
        case {'CPG','TPG','TPGD','CNE'} % nothing to add
        case {'LTE','LTEED'}
            [~,bGuessXs,idx] = find_flagAndRemove('guessXsLTE',varargin);   % looking for the guessXsLTE flag
            if bGuessXs                                                     % if a guess is provided
                varargin{idx} = 'guessXs';                                      % we rename the flag to comply with the usage of getEquilibrium_X_complete
            end
            [X_s,dXs_dT,~,dXs_dyE] = getEquilibrium_X_complete(p,TTrans,'mtimesx',options.mixture,mixCnst,options.modelEquilibrium,1,options,'variable_XE',X_E,'computeXFgrads',varargin{:});
            y_s = getEquilibrium_ys(X_s,mixCnst.Mm_s);
            dys_dT = getEquilibriumDer_dys_dq(X_s,dXs_dT,mixCnst.Mm_s);
        otherwise
            error(['The chosen flow assumption ''',options.flow,''' is not supported']);
    end
end

% if the user specified a dimensional value for the temperature, we must
% update the T vectors, and consequently also the g vectors and their
% derivatives.
if bCorrectTBC && strcmp(options.thermal_BC,'Twall')
    T_w  = options.trackerTwall(i_xi);
    switch options.numberOfTemp
        case '1T';                                  Tv_w = options.trackerTwall(i_xi);
        case '2T'
            switch options.thermalVib_BC
                case {'frozen','Twall'};            Tv_w = options.trackerTvwall(i_xi);
                case {'thermalEquil','ablation'};   Tv_w    = T_w;
                otherwise;                          error(['the chosen thermalVib_BC ''',options.thermalVib_BC,''' is not supported']);
            end
        otherwise;                              error(['the chosen number of temperatures ''',optionsnumberOfTemp,''' is not supported']);
    end
    TTrans  = correctBoundaries(TTrans, T_w, eta,'onlyPosEnd');
    TRot    = correctBoundaries(TRot,   T_w, eta,'onlyPosEnd');
    TVib    = correctBoundaries(TVib,   Tv_w,eta,'onlyPosEnd');
    TElec   = correctBoundaries(TElec,  Tv_w,eta,'onlyPosEnd');
    Tel     = correctBoundaries(Tel,    Tv_w,eta,'onlyPosEnd');
end

% Species enthalpies and heat capacities
TTrans_mat = repmat(TTrans,[1,N_spec]);
TRot_mat = repmat(TRot,[1,N_spec]);
TVib_mat = repmat(TVib,[1,N_spec]);
TElec_mat = repmat(TElec,[1,N_spec]);
Tel_mat = repmat(Tel,[1,N_spec]);
switch options.numberOfTemp
    case '1T'
        switch options.flow
            case 'CPG'
                cp = varargin{1} * ones(N_eta,1);
                R = varargin{2} * ones(N_eta,1);
                cv = cp-R;
                h = cp.*TTrans;
                h_s = NaN*ones(N_eta,N_spec);   htr_s = h_s;    hv_s = h_s; % placeholders
                hv = NaN*ones(N_eta,1);         cptr = hv;      cpv = hv;
            case {'TPG','TPGD','LTE','LTEED','CNE'}
                [h,cp,cv,h_s,~,~,~,hv,cptr,cpv,htr_s,hv_s] = getMixture_h_cp(y_s,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat,R_s_mat,nAtoms_s_mat,...
                    thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);
            otherwise
                error(['The chosen flow=''',options.flow,''' is not supported']);
        end
        N_T = 1; % used later
    case '2T'
        [h,cp,cv,h_s,~,~,~,hv,cptr,cpv,htr_s,hv_s] = getMixture_h_cp(y_s,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat,R_s_mat,nAtoms_s_mat,...
                                                                thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);
        N_T = 2; % used later
        % IMPORTANT! cp defined here is actually cptr. It is done like this to factor out a lot of code
    otherwise
        error(['the chosen number of temperatures ''',numberOfTemp,''' is not supported']);
end

% Density
if strcmp(options.flow,'CPG')
    rho = getMixture_rho([],p,TTrans,[],[],[],[],'specify_R',R,mixCnst.EoS_params,options);
    Mm = mixCnst.R0 ./ R;
    X_s = NaN;
else
    rho     = getMixture_rho(y_s,p,TTrans,Tel,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options);
    [Mm,R]  = getMixture_Mm_R(y_s,TTrans,Tel,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron);
    X_s = getSpecies_X(y_s,Mm,mixCnst.Mm_s);             % computing mole fraction
end

% Get mixture dynamic viscosity and thermal conductivity
[mu,kappa,kappav] = getTransport_mu_kappa(y_s,TTrans,Tel,rho,p,Mm,options,mixCnst,TRot,TVib,TElec,cp);

% Diffusion model
if ~sum(strcmp(options.flow,{'CPG','TPG'}))
    switch options.modelDiffusion
        case 'cstSc';   neededvalue(mixCnst,'Sc_e','Sc_e required in mixCnst for ''cstSc'' diffusion');
                        Sc_e = mixCnst.Sc_e;    Le_e = [];
        case 'cstLe';   neededvalue(mixCnst,'Le_e','Le_e required in mixCnst for ''cstLe'' diffusion');
                        Le_e = mixCnst.Le_e;    Sc_e = [];
        otherwise;      Sc_e = [];              Le_e = [];
    end
    D_s = getTransport_Ds(y_s,TTrans,Tel,rho,p,Mm,mu,kappa,cp,mixCnst,Sc_e,Le_e,options);       % for spec-spec diffusion this is actually D_sl
    if bSelfConsistent && strcmp(options.modelDiffusion,'FOCE_Effective')                       % we ensure that the sum of the diffusion fluxes is zero
        idx_bath = mixCnst.idx_bath;
        D_s(:,idx_bath) = - 1./dys_deta(:,idx_bath) .* (D_s(:,~strcmp(1:N_spec,idx_bath)).*dys_deta(:,~strcmp(1:N_spec,idx_bath))*ones(N_spec-1,1));
    end
    nTfrac_s = getSpecies_nTfrac(TTrans,Tel,rho,y_s,mixCnst.Mm_s,mixCnst.nAvogadro,mixCnst.bElectron);
else
    D_s = NaN;  % placeholders
    nTfrac_s = NaN;
end

% Equilibrium Thermal conductivity and heat capacity (only LTE and LTEED)
if ismember(options.flow,{'LTE','LTEED'})
    protectedvalue(options,'numberOfTemp','1T');
    if options.molarDiffusion;  XYs_inputs = dXs_dT;
    else;                       XYs_inputs = dys_dT;
    end
    if options.bPairDiff % species-species diffusion
        rho_s = y_s.*repmat(rho,[1,N_spec]);
        kappa_reac = getEquilibrium_kappareac('spec-spec',h_s,rho_s,D_s,XYs_inputs);    % NOTE: it's actually D_sl, not D_s
    else % species-mixture diffusion
        kappa_reac = getEquilibrium_kappareac('spec-mix', h_s,rho,  D_s,XYs_inputs);
    end
    kappa = kappa + kappa_reac;

    cp_reac = getEquilibrium_cpreac(h_s,dys_dT);
    cp = cp + cp_reac;
end

% Elemental diffusion coefficients (only LTEED)
if ismember(options.flow,{'LTEED'})
    protectedvalue(options,'numberOfTemp','1T');
    dXs_dXE    = getEquilibriumDer_dXs_dXE(X_E,dXs_dyE,mixCnst.Mm_E);       % obtaining concentration gradients with respect to the elemental mass fraction
%     dXs_dyE    = getEquilibriumDer_dXs_dyE(X_E,dXs_dXE,mixCnst.Mm_E);       
    dys_dyE    = getEquilibriumDer_dys_dq( X_s,dXs_dyE,mixCnst.Mm_s);
    if options.molarDiffusion;  XYs_inputs = {dXs_dT,dXs_dyE};              % inputs depending on the diffusion theory (based on X_s or y_s gradients)
    else;                       XYs_inputs = {dys_dT,dys_dyE};
    end
    [A_E,C_EF] = getEquilibrium_elementalDiffCoeff(options.bPairDiff,XYs_inputs{:}, D_s,rho,y_s,mixCnst.Mm_s,mixCnst.Mm_E,mixCnst.elemStoich_mat);
else
    A_E = NaN; C_EF = NaN; dys_dyE = NaN; dXs_dyE = NaN; dXs_dXE = NaN;% placeholders
    X_E = getElement_XE_from_ys(y_s,mixCnst.Mm_s,mixCnst.elemStoich_mat,options.bChargeNeutrality,mixCnst.elem_list);
end
y_E = getEquilibrium_ys(X_E,mixCnst.Mm_E);      % elemental mass fraction

% Flow chemistry
if strcmp(options.flow,{'CNE'}) && ~isempty(mixCnst.A_r)  % the second condition would mean there are no reactions
    options.EoS = protectedvalue(options,'EoS','idealGas');
    [source_s,~,~,source_sr] = getSpecies_source(TTrans,TRot,TVib,TElec,Tel,y_s,...
        rho,mixCnst.Mm_s,mixCnst.nu_reac,mixCnst.nu_prod,...
        mixCnst.A_r,mixCnst.nT_r,mixCnst.theta_r,mixCnst.qf2T_r,...
        mixCnst.qb2T_r,mixCnst.Keq_struc,options);
    %%% define dOmegas_dq where q is T and ym
    % Computing the derivatives with temperature and mass fractions of the
    % species source terms and the mixture density. These terms are computed
    % to enhance the coupling between the species production term and the
    % system's matrix. We are essentially expanding around the mean the hatted
    % source term.
    % This is used to ensure spectral convergence at later streamwise
    % stages. Otherwise, the source term remains decoupled from the
    % linearized system of equations, rendering first-order convergence
    [dsources_dym,dsourcesr_dym]    = getSpeciesDer_dsource_dym(TTrans,TRot,TVib,TElec,Tel,y_s,p,...
        mixCnst.A_r,mixCnst.nT_r,mixCnst.theta_r,mixCnst.nu_prod,...
        mixCnst.nu_reac,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.R0,...
        mixCnst.qf2T_r,mixCnst.qb2T_r,mixCnst.Keq_struc,options);
    [dsources_dT,dsourcesr_dT]     = getSpeciesDer_dsource_dT(TTrans,TRot,TVib,TElec,Tel,y_s,p,...
        mixCnst.A_r,mixCnst.nT_r,mixCnst.theta_r,mixCnst.nu_prod,...
        mixCnst.nu_reac,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.R0,...
        mixCnst.qf2T_r,mixCnst.qb2T_r,mixCnst.Keq_struc,options);
else
    source_s = zeros(N_eta,N_spec);         dsources_dT = zeros(N_eta,N_spec,N_T);          dsources_dym = zeros(N_eta,N_spec,N_spec);     % placeholders
    source_sr = zeros(N_eta,N_spec,N_reac); dsourcesr_dT = zeros(N_eta,N_spec,N_T,N_reac);  dsourcesr_dym = zeros(N_eta,N_spec,N_spec,N_reac);
end

% Vibrational source term
switch options.numberOfTemp
    case '1T'
    sourcev = zeros(N_eta,1);    dsourcev_dT = zeros(N_eta,N_T);    dsourcev_dym = zeros(N_eta,N_spec);     % placeholders
    case '2T'
    options.EoS = protectedvalue(options,'EoS','idealGas');
    sourcev = getMixture_sourcev(TTrans,TVib,TElec,Tel,y_s,p,rho,source_s,mixCnst.Mm_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.R_s,...
                                mixCnst.aVib_sl,mixCnst.bVib_sl,mixCnst.sigmaVib_s,mixCnst.bMol,mixCnst.bElectron,mixCnst.R0,...
                                mixCnst.nAvogadro,mixCnst.pAtm,source_sr,mixCnst.hIon_s,mixCnst.kBoltz,mixCnst.qEl,mixCnst.epsilon0,mixCnst.consts_Omega11,...
                                mixCnst.bPos,mixCnst.bIonelim,mixCnst.bIonHeavyim,mixCnst.bDissel,mixCnst.bIonAssoc,options,'noQFormv');
    dsourcev_dT = getMixtureDer_dsourcev_dT(TTrans,TVib,TElec,Tel,y_s,p,rho,source_s,dsources_dT,mixCnst.pAtm,mixCnst.R0,...
                                mixCnst.nAvogadro,mixCnst.aVib_sl,mixCnst.bVib_sl,mixCnst.sigmaVib_s,mixCnst.bMol,mixCnst.Mm_s,...
                                mixCnst.R_s,mixCnst.bElectron,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bPos,...
                                mixCnst.epsilon0,mixCnst.kBoltz,mixCnst.qEl,mixCnst.consts_Omega11,dsourcesr_dT,source_sr,mixCnst.hIon_s,...
                                mixCnst.bIonelim,mixCnst.bIonHeavyim,mixCnst.bDissel,mixCnst.bIonAssoc,options.numberOfTemp,options,'noQFormv');
    dsourcev_dym = getMixtureDer_dsourcev_dym(TTrans,TVib,TElec,Tel,y_s,p,rho,dsources_dym,mixCnst.pAtm,mixCnst.R0,...
                                mixCnst.nAvogadro,mixCnst.aVib_sl,mixCnst.bVib_sl,mixCnst.sigmaVib_s,mixCnst.bMol,mixCnst.Mm_s,...
                                mixCnst.R_s,mixCnst.bElectron,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bPos,...
                                mixCnst.epsilon0,mixCnst.kBoltz,mixCnst.qEl,mixCnst.consts_Omega11,dsourcesr_dym,mixCnst.hIon_s,...
                                mixCnst.bIonelim,mixCnst.bIonHeavyim,mixCnst.bDissel,mixCnst.bIonAssoc,options,'noQFormv');
    otherwise
        error(['The chosen numberOfTemp=''',options.numberOfTemp,''' is not supported']);
end
switch options.flow
    case 'CPG'
        dcpv_dTv = zeros(N_eta,1);   cpv_s = zeros(N_eta,N_spec);
    case {'TPG','TPGD','LTE','LTEED','CNE'} % (actually only used in 2T equations)
        % computing also derivatives of cpv, to be used in the expansion about the baseflow of the Cp1 and Cp2 computational variables
        dcpv_dTv    = getMixtureDer_dcv_dTv(TVib,y_s,mixCnst.R_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,options,mixCnst.AThermFuncs_s);
        [~,cpv_s]   = getSpecies_hv_cpv(TVib_mat,TElec_mat,Tel_mat,R_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,options,mixCnst.AThermFuncs_s,nAtoms_s_mat,hForm_s_mat);
        % NOTE: cpv_s is equal to dcpv_dys
    otherwise
        error(['The chosen flow=''',options.flow,''' is not supported']);
end

% preparing outputs
varargout = {rho,mu,kappa,TTrans,TRot,TVib,TElec,Tel,h,cp,cv,Mm,R,y_s,X_s,h_s,D_s,nTfrac_s,source_s,dsources_dT,dsources_dym,...
                kappav,hv,cptr,cpv,htr_s,hv_s,sourcev,dsourcev_dT,dsourcev_dym,dcpv_dTv,cpv_s,y_E,A_E,C_EF,dys_dyE,dXs_dyE,dXs_dXE};

end % getAll_properties
