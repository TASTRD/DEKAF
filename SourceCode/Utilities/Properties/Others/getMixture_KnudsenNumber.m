function Kn = getMixture_KnudsenNumber(p,TTrans,kBoltz,L,dMol)
% getMixture_KnudsenNumber computes the Knudsen number.
%
% Usage:
%   (1)
%       Kn = getMixture_KnudsenNumber(p,TTrans,kBoltz,L,dMol)
%
% Inputs and outputs:
%   p           [Pa]        (N_eta x 1)
%       static pressure
%   TTrans      [K]         (N_eta x 1)
%       translational temperature
%   kBoltz      [J/K]       (1 x 1)
%       boltzmann constant
%   L           [m]         (1 x 1)
%       reference length
%   dMol        [m]         (1 x 1)
%       average hard-shell diameter of molecules (346e-12 for oxygen,
%       364e-12 for nitrogen)
%   Kn          [-]         (N_eta x 1)
%       Knudsen number
%
% Author: Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

Kn = kBoltz*TTrans./(sqrt(2)*pi*dMol^2.*p.*L);

end % getMixture_KnudsenNumber