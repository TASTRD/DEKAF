function a = getMixture_a(TTrans,TRot,TVib,TElec,Tel,y_s,Mm_s,R0,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options,varargin)
% getMixture_a computes the speed of sound
%
% Usage:
%   (1)
%       a = getMixture_a(TTrans,TRot,TVib,TElec,Tel,y_s,Mm_s,R0,nAtoms_s,...
%                       thetaVib_s,thetaElec_s,gDegen_s,bElectron,options)
%
%   (2)
%       a = getMixture_a(...,AThermFuncs_s)
%       |--> usage for polynomial thermal models ('NASA7' or
%       'customPoly7'), where the coefficients of the polynomials must be
%       passed.
%
% Inputs and outputs:
%   y_s             [-]             (N_eta x N_spec)
%       mass fraction matrix 
%   TTrans          [K]             (N_eta x 1)
%       translational temperature
%   TRot            [K]             (N_eta x 1)
%       rotational temperature 
%   TVib            [K]             (N_eta x 1)
%       vibrational temperature
%   TElec           [K]             (N_eta x 1)
%       electronic temperature 
%   Tel             [K]             (N_eta x 1)
%       electron temperature
%   R_s             [J/kg-K]        (N_spec x 1)
%       species gas constant
%   nAtoms_s        [-]             (N_spec x 1)
%       number of atoms in the species' molecule
%   thetaVib_s      [K]             (N_spec x Nvib)
%       species vibrational activation temperature
%   thetaElec_s     [K]             (N_spec x Nstat)
%       species activation temperature of electronic states
%   gDegen_s        [-]             (N_spec x Nstat)
%       degeneracies of the species' electronic state
%   bElectron       [-]             (N_spec x 1)
%       electron boolean 
%   hForm_s         [J/kg]          (N_spec x 1)
%       enthalpy of formation of each species 
%   options
%       necessary fields:
%           .modelThermal
%           .numberOfTemp
%           .neglect_hRot
%           .neglect_hVib
%           .neglect_hElec
%   AThermFuncs_s       [SI]                (N_spec x 1)
%       structured cell array with the anonymous functions for each
%       species. Each cell-array position contains N_coef anonymous
%       functions:
%           .A1, .A2, etc
%
% Notes:
%   N_spec is the number of species in the mixture
%   N_eta is the number of points in the domain
%   N_stat is the number of electronic states for the given species
%
% Children and relation functions:
%   See also getMixture_gam.m, getMixture_Mm_R.m, listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% obtaining specific heat ratio
gam = getMixture_gam(TTrans,TRot,TVib,TElec,Tel,y_s,R0./Mm_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options,varargin{:});
% obtaining gas constant
[~,R] = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);
% speed of sound
a = sqrt(gam.*R.*TTrans);