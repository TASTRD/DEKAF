function [M,gam,R] = getEdge_Mach(T,y_s,u,mixCnst,options,intel,varargin)
% getEdge_Mach computes the Mach number at the boundary-layer edge.
%
% Usage:
%   (1)
%       [M,gam,R] = getEdge_Mach(T,y_s,u,mixCnst,options,intel)
%
%   (2)
%       [M,gam,R] = getEdge_Mach(T,y_s,u,mixCnst,options,intel,Tv)
%       |--> way to call it for two temperatures
%
% Inputs and outputs:
%   T
%       Translational-rotational temperature [N_eta x 1]
%   Tv
%       Vibrational-electronic-electron temperature [N_eta x 1]
%   y_s
%       mass fractions [N_eta x N_spec]
%   u
%       streamwise veloity [N_eta x 1]
%   mixCnst
%       structure containing the mixture constants
%   options
%       structure containing several options
%   intel
%       structure containing several flow-field variables
%   M
%       Mach number [N_eta x 1]
%   gam
%       ratio of heat capacities [N_eta x 1]
%   R
%       ideal gas constant [N_eta x 1]
%
% Author: Fernando Miro Miro
% Date: June 2019
% GNU Lesser General Public License 3.0

switch options.numberOfTemp
    case '1T';          Tv = T;
    case '2T';          Tv = varargin{1};
    otherwise;          error(['the number of temperatures chosen ''',options.numberOfTemp,''' is not supported']);
end

% extracting from mixCnst
R_s         = mixCnst.R_s;
Mm_s        = mixCnst.Mm_s;
R0          = mixCnst.R0;
nAtoms_s    = mixCnst.nAtoms_s;
thetaVib_s  = mixCnst.thetaVib_s;
thetaElec_s = mixCnst.thetaElec_s;
gDegen_s    = mixCnst.gDegen_s;
bElectron   = mixCnst.bElectron;

if strcmp(options.flow,'CPG')
    gam = intel.gam_e(1);
    R = intel.R_e(1);
else
    gam = getMixture_gam(T,T,Tv,Tv,Tv,y_s,R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options,mixCnst.AThermFuncs_s);
    [~,R] = getMixture_Mm_R(y_s,T,Tv,Mm_s,R0,bElectron);
end
M = u./sqrt(R.*gam.*T);

end % getEdge_Mach