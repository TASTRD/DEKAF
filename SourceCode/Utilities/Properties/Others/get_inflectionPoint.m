function [drhodudydy] = get_inflectionPoint(intel)
% get_inflectionPoint computes the generalized inflection point.
%
% Usage:
%   (1)
%       drhodudydy = get_inflectionPoint(intel)
%
% if the D1_y matrix is defined in intel, then it is used, otherwise
% derivatives are obtained with diff.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0


[N_eta,N_x] = size(intel.rho); % number of streamwise positions
drhodudydy = zeros(N_eta,N_x); % allocating
for idx_x = 1:N_x
    rho = intel.rho(:,idx_x);
    if isfield(intel,'D1_y')     % obtaining derivatives "cleanly" if possible
        D1_y = intel.D1_y(:,:,idx_x);
        drho_dy = (D1_y*rho);
    else                            % otherwise we diff them
        drho_dy = diff(intel.rho(:,idx_x))./diff(intel.y(:,idx_x));
        drho_dy = [drho_dy(1);drho_dy]; % repeating the freestream point to account for diff's size reduction
    end
    if isfield(intel,'du_dy')    % obtaining derivatives "cleanly" if possible
        du_dy = intel.du_dy(:,idx_x);
    else                            % otherwise we diff them
        du_dy = diff(intel.u(:,idx_x))./diff(intel.y(:,idx_x));
        du_dy = [du_dy(1);du_dy]; % repeating the freestream point to account for diff's size reduction
    end
    if isfield(intel,'du_dy2')    % obtaining derivatives "cleanly" if possible
        du_dy2 = intel.du_dy2(:,idx_x);
    else                            % otherwise we diff them
        du_dy2 = diff(du_dy)./diff(intel.y(:,idx_x));
        du_dy2 = [du_dy2(1);du_dy2];
    end
    drhodudydy(:,idx_x) = drho_dy.*du_dy + rho.*du_dy2;
end
