function St = getWall_Stanton(dT_dy,T,ys,p,mixCnst,options,rho_ref,U_ref,H_ref,varargin)
% getWall_Stanton computes the Stanton number at the wall.
%
% Usage:
%   (1)
%       St = getWall_Stanton(dT_dy,T,ys,p,mixCnst,options,rho_ref,U_ref,H_ref)
%       |--> usage for one temperature
%
%   (2)
%       St = getWall_Stanton(dT_dy,T,ys,p,mixCnst,options,rho_ref,U_ref,H_ref,'2T',dTv_dy,Tv)
%       |--> usage for two temperatures
%
% Inputs and outputs:
%   dT_dy           [K/m]
%       temperature gradient field (size Ny x Nx)
%   T               [K]
%       temperature field (size Ny x Nx)
%   ys              [-]
%       cell with the N_spec species concentration fields (size Ny x Nx)
%   p               [Pa]
%       pressure field (size Ny x Nx)
%   mixCnst
%       structure with the necessary mixture constants
%   options
%       structure with the necessary flow options
%   rho_ref         [kg/m^3]
%       reference density (size 1 x 1)
%   U_ref           [m/s]
%       reference velocity (size 1 x 1)
%   H_ref           [J/kg]
%       reference total enthalpy (size 1 x 1)
%   dTv_dy          [K/m]
%       vib.-elec.-el. temperature gradient field (size Ny x Nx)
%   Tv               [K]
%       vib.-elec.-el. temperature field (size Ny x Nx)
%   St              [-]
%       Non-reactive Stanton number - it only considers conductive heat
%       fluxes, and not those associated to diffusion fluxes arising from
%       concentration gradients due to chemicalreaction (size 1 x Nx)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if isempty(varargin);   numberOfTemp = '1T';
else;                   numberOfTemp = varargin{1};
end

switch numberOfTemp
    case '1T';      dTv_dy = dT_dy;         Tv = T;
    case '2T';      dTv_dy = varargin{2};   Tv = varargin{3};
    otherwise;      error(['the chosen numberOfTemp ''',numberOfTemp,''' is not supported']);
end

% Obtaining quantities at the wall
T_w         = T(end,:).';           % trans-rot temperature at the wall
dT_dy_w     = dT_dy(end,:).';       % trans-rot temperature gradient at the wall
Tv_w        = Tv(end,:).';          % vib-elec-el temperature at the wall
dTv_dy_w    = dTv_dy(end,:).';      % vib-elec-el temperature gradient at the wall
for s=mixCnst.N_spec:-1:1
    y_sw(:,s) = ys{s}(end,:).';     % species concentration at the wall
end
p_w = p(end,:).';

% Computing auxiliary quantities
switch numberOfTemp
    case '1T'
        TTrans = T_w;     TRot = T_w;       TVib = T_w;       TElec = T_w;     Tel = T_w;
    case '2T'
        TTrans = T_w;     TRot = T_w;
        TVib = Tv_w;      TElec = Tv_w;     Tel = Tv_w;
    otherwise
        error(['the chosen numberOfTemp ''',numberOfTemp,''' is not supported']);
end
rho     = getMixture_rho(y_sw,p_w,TTrans,Tel,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.EoS_params,options);
Mm      = getMixture_Mm_R(y_sw,[],[],mixCnst.Mm_s,[],[]);
[~,cp] = getMixture_h_cp_noMat(y_sw,TTrans,TRot,TVib,TElec,Tel,mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,options,mixCnst.AThermFuncs_s);
[~,kappa,kappav] = getTransport_mu_kappa(y_sw,TTrans,Tel,rho,p_w,Mm,options,mixCnst,TRot,TVib,TElec,cp);

% Computing heat flux and Stanton number
switch numberOfTemp
    case '1T';  Q_w = kappa.*dT_dy_w;
    case '2T';  Q_w = kappa.*dT_dy_w + kappav.*dTv_dy_w;
    otherwise;  error(['the chosen numberOfTemp ''',numberOfTemp,''' is not supported']);
end
St = Q_w.'./(rho_ref*U_ref*H_ref);

end % getWall_Stanton