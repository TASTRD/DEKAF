function [Mhat,y_M1] = get_relativeMach(x,intel,options,c)
% get_relativeMach computes the relative sonic Mach number for a given
% flowfield at a given x location.
%
% Usage:
%   (1)
%       Mhat = get_relativeMach(x,intel,omega,alpha)
%
%   (2)
%       [Mhat,y_M1] = get_relativeMach(x,intel,omega,alpha)
%       |--> returns also the value of y at which Mhat=-1 is reached
%       (relative sonic line)
%
% Inputs and outputs:
%   x
%       streamwise location where the relative Mach number is to be
%       computed. [m]
%   intel
%       classic DEKAF intel structure containing the necessary flow
%       variables. In particular:
%           ys
%               cell {N_spec x 1} of mass fraction profiles (N_eta x N_xi)
%           x
%               matrix of streamwise positions (N_eta x N_xi)
%           T
%               matrix of temperatures (N_eta x N_xi)
%           u
%               matrix of streamwise velocities (N_eta x N_xi)
%           U_e
%               vector of streamwise edge velocities (1 x N_xi)
%           mixCnst
%               structure containing all the mixture constants.
%   options
%       classic DEKAF options structure. It needs whatever fields
%       getMixture_gam uses.
%   c
%       non-dimensional perturbation wave speed for which the relative Mach
%       number is to be computed (non-dimensionalized with 1/U_e)
%   Mhat
%       vector of relative mach numbers (N_eta x 1)
%   y_M1
%       wall-normal position at which the relative sonic line (Mhat=-1) is
%       reached
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% locating where the chosen x lies
[~,idx_x] = min(abs(intel.x(1,:) - x));

% Computing relative Mach number and friends
y_s = cell1D2mat2D(intel.ys);
y_s = reshape(y_s.',[length(intel.ys),size(intel.ys{1},1),size(intel.ys{1},2)]);    % (yx,s) --> (s,y,x)
y_s = y_s(:,:,idx_x).';                                                             % (s,y,x) --> (y,s)
T = intel.T(:,idx_x);
mixCnst = intel.mixCnst;
gam = getMixture_gam(T,T,T,T,T,y_s,mixCnst.R_s,mixCnst.nAtoms_s,...
                   mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,...
                   mixCnst.bElectron,options);                              % ratio of specific heat capacities
[~,R] = getMixture_Mm_R(y_s,T,T,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron); % gas constant
a = sqrt(gam.*R.*T);                                                        % speed of sound
M = intel.u(:,idx_x)./a;                                                    % local Mach number
U_e = intel.U_e(idx_x);                                                     % edge velocity
Mhat = M - U_e*c./a;                                                        % relative mach number

% obtaining BL limits
if nargout>1
    y = intel.y(:,idx_x);
    idx_M1 = find(Mhat > -1,1,'last');                                          % we look for the last point (we start from the bottom) larger than -1
    y_M1 = interp1(Mhat(idx_M1:idx_M1+1),y(idx_M1:idx_M1+1),-1,'spline');       % we interpolate to get the exact position of the sonic line
end

end % get_relativeMach
