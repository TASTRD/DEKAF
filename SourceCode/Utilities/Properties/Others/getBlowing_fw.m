function f_w = getBlowing_fw(m_w,xi,rho_e,U_e,mu_e,varargin)
% getBlowing_fw computes the value of the non-dimensional velocity
% parameter f_w
%
% Usage:
%   (1)
%       f_w = getBlowing_fw(m_w,xi,rho_e,U_e,mu_e)
%       |--> self-similar value of f_w for a flatplate (with df_dxi=0)
%   (2)
%       f_w = getBlowing_fw(m_w,xi,rho_e,U_e,mu_e,r0)
%       |--> self-similar value of f_w for a cone of base radius r0 (with df_dxi=0)
%
% Inputs and outputs:
%   m_w         mass flow rate per unit surface at the wall [kg/s-m^2]
%   xi          marching variable                           [kg^2/m^2-s^2]
%   rho_e       edge density                                [kg/m^3]
%   U_e         edge streamwise velocity                    [m/s]
%   mu_e        edge dynamic viscosity                      [kg/m-s]
%   r0          cone radius                                 [m]
%   f_w         non-dimensional velocity parameter at the wall [-]
%
% all inputs should have the same size
%
% Author: Fernando Miro Miro
% Date: November 2018
% GNU Lesser General Public License 3.0

if isempty(varargin) % self-similar case, with df_dxi = 0
    f_w = -m_w.*sqrt(2*xi) ./ (rho_e.*U_e.*mu_e);
else
    r0 = varargin{1};
    f_w = -m_w.*sqrt(2*xi) ./ (r0.*rho_e.*U_e.*mu_e);
end
