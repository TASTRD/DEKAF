function [deltaStar,TE] = getBL_deltaStar(D_eta,dy_deta,df_deta,j,varargin)
% getBL_deltaStar computes the boundary-layer displacement thickness.
%
% Usage:
%   (1)
%       [deltaStar,TE] = getBL_deltaStar(D_eta,dy_deta,df_deta,j)
%
%   (2)
%       [...] = getBL_deltaStar(I_eta,dy_deta,df_deta,j,'passIntegralMat')
%       |--> allows to pass the integral matrix rather than the
%       differentiation matrix (faster)
%
% Inputs and outpus:
%   D_eta           [-]                 (N_eta x N_eta)
%       differentiation matrix in the (eta) wall-normal direction.
%   I_eta           [-]                 (N_eta x N_eta)
%       integration matrix in the (eta) wall-normal direction.
%   dy_deta         [m]                 (N_eta x N_x)
%       derivative of y wrt the transformed eta variable
%   df_deta         [-]                 (N_eta x N_x)
%       non-dimensional streamwise velocity profiles
%   j               [-]                 (N_eta x N_x)
%       inverse of the non-dimensional mixture density profiles
%   deltaStar       [m]                 (1 x N_x)
%       displacement thickness
%   TE              [m]                 (1 x N_x)
%       truncation error in the computation of the displacement thickness
%
% Author(s): Fernando Miro Miro
%            Koen Groot
% GNU Lesser General Public License 3.0

% checking options
[~,bInt] = find_flagAndRemove('passIntegralMat',varargin);
if bInt;    I_eta = D_eta;                      % the integral matrix was actually passed
else;       I_eta = integral_mat(D_eta,'du');   % obtaining integral matrix (eta = 0 in bottom row)
end
% determining the full integral
deltastarv = I_eta([1 2],:)*(dy_deta.*(1-df_deta./j));
% extracting integral value:
deltaStar = deltastarv(1,:);
% extracting truncation error:
TE = deltastarv(1,:)-deltastarv(2,:);
    
end % getBL_deltaStar