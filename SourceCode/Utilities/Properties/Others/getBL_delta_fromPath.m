function [x,delta,idx] = getBL_delta_fromPath(varargin)
% getBL_delta_fromPath generates a dat file with the location of the
% boundary-layer limit in a marched DEKAF flowfield.
%
%   Examples:
%       (1)     [x,delta] = getBL_delta_fromPath(path)
%
%       (2)     [x,delta] = getBL_delta_fromPath(path,bl_margin)
%           |--> allows you to impose a margin of the total
%           enthalpy that will define the boundary-layer limit.
%
%       (3)     [x,delta] = getBL_delta_fromPath(path,bl_margin,bl_ext)
%           |--> allows you to fix an extending factor for the BL, so that
%           it is set higher/lower than where the bl_margin is
%
%       (4)     [x,delta] = getBL_delta_fromPath(path,bl_margin,bl_ext,savepath)
%
%       (5)     [x,delta,idx] = getBL_delta_fromPath(...)
%           |--> returns also the index of the profile where the boundary
%           layer limit was found
%
%       (6)     [x,delta] = getBL_delta_fromPath(...,'noSave',...)
%           |--> doesn't save the profile
%
%       (7)     [x,delta] = getBL_delta_fromPath(...,'quiet',...)
%           |--> avoids a plot being displayed
%
%       (8)     [x,delta] = getBL_delta_fromPath(...,'fitSqrtX',...)
%           |--> fits the boundary-layer data to a sqrt(2) law
%
%       (9)     [x,delta] = getBL_delta_fromPath(...,'poly3',...)
%           |--> uses a third-order polynomial on the sqrt(x) instead of a
%           first-order (requires fitSqrtx)
%
%       (10)    [x,delta] = getBL_delta_fromPath(...,'chooseXStart',...)
%           |--> asks the user at what streamwise location the boundary
%           layer should start
%
%       (11)    [x,delta] = getBL_delta_fromPath(...,'passingIntel',intel,...)
%           |--> allows to pass the intel structure, rather than the mat
%           file path
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[varargin,quiet]    = find_flagAndRemove('quiet',varargin);
[varargin,fitSqrtX] = find_flagAndRemove('fitSqrtX',varargin);
[varargin,poly3]    = find_flagAndRemove('poly3',varargin);
[varargin,chooseXStart] = find_flagAndRemove('chooseXStart',varargin);
[varargin,noSave]    = find_flagAndRemove('noSave',varargin);
[varargin,passingIntel,idxIntel]    = find_flagAndRemove('passingIntel',varargin);
if passingIntel                 % if we pass intel
    intel = varargin{idxIntel};     % we extract intel from varargin
    path = '';                      % no additional name
    varargin(idxIntel) = [];        % and remove it from varargin
else                            % otherwise
    path = varargin{1};             % we extract the mat file path
    varargin(1) = [];               % and remove it from varargin
end

switch length(varargin)
    case 0
        bl_margin = 0.01; % default
        bl_ext = 1.05; % default
        savepath = ['BL_',path(1:end-3),'dat']; % default
    case 1
        bl_margin = varargin{1};
        bl_ext = 1.05; % default
        savepath = ['BL_',path(1:end-3),'dat']; % default
    case 2
        bl_margin = varargin{1};
        bl_ext = varargin{2};
        savepath = ['BL_',path(1:end-3),'dat']; % default
    case 3
        bl_margin = varargin{1};
        bl_ext = varargin{2};
        savepath = varargin{3};
    otherwise
        error('wrong number of inputs');
end

if chooseXStart
    xStart = input('At what x should the extraction begin? ');
else
    xStart = 0;
end

if passingIntel
    h = intel.h;    u = intel.u;                    % extracting variables from intel
    x = intel.x;    y = intel.y;
else
    load(path,'h','u','x','y');                     % loading DEKAF intel
end
H = h + 0.5*u.^2;                               % total enthalpy
H_e = H(1,:);                                   % edge total enthalpy
x = x(1,:).';                                   % x locations
idxStart = find(x>xStart,1,'first');            % where to start the extraction
x = x(idxStart:end);                            % reshaping x
Nx = length(x);                                 % number of x locations
delta = zeros(Nx,1);                            % allocating
idx = zeros(Nx,1);
for ii=1:Nx                                     % looping x locations
    % Note that this formulation of finding the boundary-layer edge
    % accounts for both cases: if and if there isn't a hump in total enthalpy.
    % 'first' is used because with Chebyshev distribution of points, index 1 is
    % located in the freestream
    idx(ii) = find( abs( (H(:,ii+idxStart-1) - H_e(ii+idxStart-1)) / H_e(ii+idxStart-1) ) > bl_margin,1,'first');
    delta(ii) = y(idx(ii),ii+idxStart-1) * bl_ext; %#ok<NODEF> % extending the boundary-layer limit
    idx(ii) = find(y(:,ii+idxStart-1)>y(idx(ii),ii+idxStart-1) * bl_ext,1,'first');
end % closing x loop

% fitting to 1/sqrt(2) if necessary
if fitSqrtX
    if poly3
        poliOrder = 3;
    else
        poliOrder = 1;
    end
    sqrtx = sqrt(x);
    fit = polyfit(sqrtx,delta,poliOrder); % fitting
    delta_orig = delta; % non-fitted data
    delta = polyval(fit,sqrtx);
end

% writing to file
if ~noSave
dlmwrite(savepath,[x,delta,zeros(Nx,1)],'\t');
end

if ~quiet
    figure
    plot(x,delta);
    xlabel('x [m]'); ylabel('y [m]');
    if fitSqrtX
        hold on;
        plot(x,delta_orig,'--');
        legend('fitted','original');
    end
end
end % getBL_delta_fromPath