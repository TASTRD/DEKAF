function [delta99, eta99, varargout] = getBL_delta(df_deta, df_deta2, y, eta, bGICM, g , Ue_sqrt2xi0, rho, eta_i, L, N, U_e, xi, options,cylCoordFuncs,xPhys)
% getBL_delta computes the boundary-layer height. Various techniques are
% supported.
%
% Usage:
%   (1) Basic
%       delta99 = getBL_delta(df_deta,df_deta2,y)
% 
%   (2) Basic (also size in the eta domain)
%       [delta99, eta99] = getBL_delta(df_deta,df_deta2,y,eta)
% 
%   (3) Basic (also size based on total enthalpy
%       [delta99, eta99, deltaH995, etaH995] = getBL_delta(df_deta,df_deta2,y,eta,false,g)
% 
%   (4) Using GICM for more accuracy
%       [...] = getBL_delta(df_deta,df_deta2,y,eta,true,g,Ue_sqrt2xi0,rho,...
%                            eta_i,L,N,U_e,xi,options,cylCoordFuncs,xPhys)
%
% Inputs:
%    Ue_sqrt2xi0        [m^2/kg]
%       scalar of U_e divided by sqrt(2*xi) at stagnation. For more
%       information, see eval_dimVars. It is equal to
%       sqrt(dUe0_dx/(rho_e(1,1)*mu_e(1,1)));  
%    rho                [kg/m^3]        Ny x Nx
%       mixture mass density profile at this xi station
%    df_deta            [-]             Ny x Nx
%       non-dimensional u velocity profile at this xi station
%    df_deta2           [-]             Ny x Nx
%       eta derivative of non-dimensional u velocity profile at this xi
%       station 
%    y                  [m]             Ny x Nx
%       wall-normal position
%    eta                [-]             Ny x Nx
%       transformed wall-normal position
%   bGICM
%       boolean determining whether the GICM interpolation onto a finer
%       grid should be done or not 
%    eta_i              [-]             1 x 1
%       scalar critical Malik mapping parameter for GICM_interp
%    L                  [-]             1 x 1
%       scalar maximum Malik mapping parameter for GICM_interp
%    N                  [-]             1 x 1
%       scalar number of eta-nodes for GICM_interp
%    U_e                [m/s]           1 x Nx
%       edge u velocity at this xi station
%    xi                 [kg^2/m^2-s^2]  1 x Nx
%       value of self-similar marching coordinate xi at this station
%    options
%       struct of options preset for GICM_interp()
%    cylCoordFuncs
%       structure containing various functions to compute various
%       geometrical quantities related to cylindrical coordinates:
%           .rc(xc)             [m]
%               spanwise radius of curvature
%           .alphac(xc)         [deg]
%               streamwise inclination angle
%           .Ixc_rc2(xc)        [m]
%               integral of (rc/L)^2 wrt xc
%           .Dxc_rc(xc)         [-]
%               derivative of rc wrt xc
%           .Dxc_alphac(xc)     [1/m]
%               derivative of alphac wrt xc
%   xPhys               [m]         (1 x Nx)
%       value of the physical streamwise position at which GICM_interp
%       is to be called
% Outputs:
%    delta99            [m]         (1 x Nx)
%       boundary-layer y thickness based on 99% u
%    eta99              [-]         (1 x Nx)
%       boundary-layer eta thickness based on 99% u
%    deltaH995           [m]         (1 x Nx)
%       boundary-layer y thickness based on 99.5% H
%    etaH995             [-]         (1 x Nx)
%       boundary-layer eta thickness based on 99.5% H
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
% GNU Lesser General Public License 3.0

if nargin<5
    bGICM = false;
end

Nx = size(y,2); % number of streamwise points
for ix=Nx:-1:1
    if size(df_deta,1)==1;  idxX = 1;   % a single profile is passed (SS case)
    else;                   idxX = ix;  % different profiles at each streamwise location (non-SS)
    end
    if bGICM                                                        % WITH GICM INTERPOLATION
        % required field by GICM to handle stagnation (only)
        intel_GICM.Ue_sqrt2xi0 = Ue_sqrt2xi0;

        % GICM required the field of the input structure to be a single y-profile
        intel_GICM.rho = rho(:,idxX);
        intel_GICM.df_deta = df_deta(:,idxX);
        intel_GICM.df_deta2 = df_deta2(:,idxX);
        intel_GICM.g = g(:,idxX);

        % perform GICM
        options.display = false;
        [intel_GICM,eta_GICM] = GICM_interp(eta_i, L, N, {'df_deta','df_deta2','g'},...
            U_e(idxX),xi(ix),intel_GICM,options,'coordsys',options.coordsys,cylCoordFuncs,xPhys(ix));
        df_deta_4interp  = intel_GICM.df_deta_GICM;
        df_deta2_4interp = intel_GICM.df_deta2_GICM;
        g_4interp        = intel_GICM.g_GICM;
        y_4interp        = intel_GICM.y_GICM;
        eta4_interp      = eta_GICM;
    else                                                            % WITHOUT GICM INTERPOLATION
        df_deta_4interp  = df_deta(:,idxX);
        df_deta2_4interp = df_deta2(:,idxX);
        y_4interp        = y(:,ix);
        if nargin>3
            eta4_interp  = eta;
        end
        if nargin>5
            g_4interp    = g(:,idxX);
        end
    end

    if nargin<4;    delta99(1,ix) = interpolateBLlim(df_deta_4interp,df_deta2_4interp,0.99,y_4interp);    eta99=[];
    else;           [delta99(1,ix),eta99(1,ix)] = interpolateBLlim(df_deta_4interp,df_deta2_4interp,0.99,y_4interp,eta4_interp);
    end
    if nargout>2
        if nargin<=5
            error('To compute the BL height based on H, the non-dimensional total enthalpy (g) must be provided');
        else
            if nnz(abs(g_4interp-1)>=0.005)<4
                varargout{1}(1,ix) = NaN; varargout{2}(1,ix) = NaN;
            else
                [varargout{1}(1,ix),varargout{2}(1,ix)] = interpolateBLlim(g_4interp,df_deta2_4interp,0.995,y_4interp,eta4_interp,'noWarning');
            end
        end
    end
end

end % getBL_delta
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [varargout] = interpolateBLlim(BLvar,df_deta2,BLlim,varargin)
% interpolateBLlim interpolates a certain BL variable (typically df_deta or
% g) and determines the BL height based on a certain limit (typically
% 0.99).
%
% Examples:
%   (1)
%       delta99 = interpolateBLlim(df_deta,df_deta2_4interp,0.99,y)
%       |--> typical usage to obtain delta99
%
%   (2)
%       eta99 = interpolateBLlim(df_deta,df_deta2_4interp,0.99,eta)
%       |--> typical usage to obtain eta99
%
%   (3)
%       [delta99,eta99] = interpolateBLlim(df_deta,df_deta2_4interp,0.99,y,eta)
%       |--> typical usage to obtain delta99 and eta99
%
%   (4)
%       deltaH995 = interpolateBLlim(g,df_deta2_4interp,0.995,y)
%       |--> typical usage to obtain the boundary-layer height based on
%       99.5% of the total enthalpy (g) 
%
%   (5)
%       [...] = interpolateBLlim(...,'noWarning')
%       |--> avoids the warning saying that the interpolation was not
%       possible
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
% GNU Lesser General Public License 3.0

[varargin,bNoWarning] = find_flagAndRemove('noWarning',varargin);
nLngths = length(varargin); % number of lengths to interpolate

dBLvarlim = 1-BLlim; % variation of the reference variable that will be regarded as the end of the BL.
dBLvar = abs(1-BLvar);

% restrict to monotonic fp part to just above the first .99-point
stclwdth = 3;
idx_fp_monoind = (find(df_deta2>=0 ...                    increasing part
                    & dBLvar>dBLvarlim,1,'first')   ...   starting from the value above delta_99
                    -stclwdth-1)                    ...   to account for the spline interpolation done over a wider stencil
                    :length(dBLvar);             %        ending at the wall value

% find last index over which u is monotonically increasing in y
if ~isempty(idx_fp_monoind)
    [~,d99indest] = min(abs(dBLvar(idx_fp_monoind)-dBLvarlim));     % point closest to delta_99
    idx4interp = idx_fp_monoind(d99indest)+(-stclwdth:stclwdth);    % range around it
    
    idx4interp(idx4interp>length(dBLvar)) = [];                     % zap indices that go beyond the index range of dBLvar
    idx4interp(idx4interp<1) = [];
    
    idxDecrease = find(diff(dBLvar(idx4interp))<0);                 % indices where dBLvar is decreasing
    idxIncrease = find(diff(dBLvar(idx4interp))>0);                 % indices where dBLvar is increasing
    if length(idxDecrease) > length(idxIncrease) && length(idxDecrease)<2*stclwdth+1
        idx4interp(idxIncrease) = []; %#ok<FNDSB>                       % we must remove them
    elseif length(idxDecrease) < length(idxIncrease) && length(idxIncrease)<2*stclwdth+1
        idx4interp(idxDecrease) = []; %#ok<FNDSB>                       % we must remove them
    else
        % In this case, either the to-be-interpolated function is
        % monotonically decreasing or increasing over the given domain
        % (i.e. length(idxDecrease)=<2*stclwdth+1 or
        % length(idxIncrease)=2*stclwdth+1), so we can proceed with the interpolation
    end

    for ii=nLngths:-1:1
        % spline interpolate to find delta_99
        varargout{ii} = interp1(dBLvar(idx4interp),varargin{ii}(idx4interp),dBLvarlim,'spline');
    end
else
    for ii=nLngths:-1:1
    varargout{ii} = nan;
    end
    if ~bNoWarning
    warning([' --- interpolateBLlim could not populate ''idx_fp_monoind'' successfully!',newline, ...
        'delta99 and eta99 have been set to nan''s here']);
    end
end
end % interpolateBLlim