function varargout = getUniversal_constants()
% GETUNIVERSAL_CONSTANTS Return universal physical constants
%
% Inputs:
%   none
%
% Outputs:
%   R0              - Universal gas constant  J/(K-mol).
%   hPlanck         - Planck's constant       6.62607015e-34 J*s.
%   cLight          - Speed of light          299792458 m/s
%   kBoltz          - Boltzmann constant      1.380649e-23 m^2-kg/K-s^2
%   nAvogadro       - Avogadro constant       6.02214076e23 part / mole
%   pAtm            - STP Pressure constant   1.01325e5 Pa
%   qEl             - elementary charge       1.602176634e-19 C
%   epsilon0        - permittivity of vacuum  C^2/N-m^2
%   sigmaStefBoltz  - Stefan-Boltzmann cst.   W/m^2-K^4
%
% References:
%
% https://en.wikipedia.org/wiki/2019_redefinition_of_the_SI_base_units#Redefinition
% https://www.nist.gov/si-redefinition/introduction-redefining-worlds-measurement-system
% https://en.wikipedia.org/wiki/Stefan%E2%80%93Boltzmann_constant
% (Accessed 2020-02-17)
%
% Author(s): Ethan Beyak, Fernando Miró Miró
%
% GNU Lesser General Public License 3.0

% Defined exactly
hPlanck     = 6.62607015e-34;   % Planck's constant             % 2019
cLight      = 299792458;        % Speed of light                % 2019
kBoltz      = 1.380649e-23;     % Boltzmann constant            % 2019
nAvogadro   = 6.02214076e23;    % Avogadro constant             % 2019
qEl         = 1.602176634e-19;  % Elementary charge             % 2019
mu0         = 4*pi*10^-7;       % Vacuum permeability
pAtm        = 1.01325e5;        % Standard atmospheric pressure

% Derived constants
epsilon0    = 1/(mu0*cLight^2); % Permittivity of vacuum
R0          = kBoltz*nAvogadro; % Universal gas constant
sigmaStefBoltz = 2*pi^5*kBoltz^4/(15*hPlanck^3*cLight^2); % Stefan-Boltzmann constant

% defining outputs
varargout{1} = R0;
varargout{2} = hPlanck;
varargout{3} = cLight;
varargout{4} = kBoltz;
varargout{5} = nAvogadro;
varargout{6} = pAtm;
varargout{7} = qEl;
varargout{8} = epsilon0;
varargout{9} = sigmaStefBoltz;

end
