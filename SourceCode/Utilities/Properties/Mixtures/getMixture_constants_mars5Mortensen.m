function [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
    Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = getMixture_constants_mars5Mortensen()
% getMixture_constants_mars5Mortensen returns the mixture constants for
% the mars5Mortensen mixture.
%
% Usage:
%   (1)
%       [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
%     Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = ...
%                                       getMixture_constants_mars5Mortensen()
%
% Outputs: the same as <a href="matlab:help getMixture_constants">getMixture_constants</a>
%
% Species: (CO2, CO, C, O, O2)
%
% References:
%       Mortensen, C. (2013). PhD Thesis Effects of Thermochemical
%   Nonequilibrium on Hypersonic Boundary-Layer Instability in the Presence
%   of Surface Ablation or Isolated Two-Dimensional Roughness. UCLA.)
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% 5-species Martian atmosphere built from Mortensen's ablating air model with 11 species
spec_list = {'CO2','CO','C','O','O2'};
N_spec = 5;

% Stoichiometric coefficients
reac_list = {'O2 + M <-> 2O + M', ...       %  (#2 in MortensenThesis) dissociation
             'CO2 + M <-> CO + O + M',...   %  (#5)               \|/
             'CO + M <-> C + O + M',...     %  (#7)                v
             'CO + O <-> C + O2',...        % (#11)    ----- exchange
             'CO2 + O <-> O2 + CO',...      % (#13)               \|/
             '2CO <-> CO2 + C',...          % (#20)                v
    };
reac_list_eq = {'CO2 <-> C + 2O',...    % equilibrium reac. CO2 diss.
                'CO <-> C + O',...      % equilibrium reac. CO diss.
                'O2 <-> 2O', ...        % equilibrium reac. O2 diss.
    };
[nu_reac,nu_prod]       = generateStoichiometricMatrices(reac_list,spec_list);
[nuEq_reac,nuEq_prod]   = generateStoichiometricMatrices(reac_list_eq,spec_list);

 % Arrhenius curve fittings for the reactions
 A_r = [  2.75e13 * [1 1 3 3 1], ...              % O2 diss.  (#2)
          1.20e05 * ones(1,N_spec), ...           % CO2 diss. (#5)
          8.50e13 * ones(1,N_spec), ...           % CO diss.  (#7)
          2.00e04, 3.00e02, 1.00e-3,...           % exchange  (#11,13,20)
          ];                                      % [(m^3/mol)^(sum(nu_reac)-1) * K^(1/nT_r)]
 nT_r = [ -1.0 * ones(1,N_spec), ...              % O2 diss.  (#2)
           0.5 * ones(1,N_spec), ...              % CO2 diss. (#5)
          -1.0 * ones(1,N_spec), ...              % CO diss.  (#7)
           1.00, 1.00, 2.00, ...                  % exchange  (#11,13,20)
     ];
theta_r = [    59500 * ones(1,N_spec), ...        % O2 diss.  (#2)
               36850 * ones(1,N_spec), ...        % CO2 diss. (#5)
              129000 * ones(1,N_spec), ...        % CO diss.  (#7)
               69500, 18210, 72390, ...           % exchange  (#11,13,20)
     ];                                                 % [K]
qf2T_r   = [ 0.5 * ones(1,N_spec),...    % O2 diss.  (#2)
             0.5 * ones(1,N_spec),...    % CO2 diss. (#5)
             0.5 * ones(1,N_spec),...    % CO diss.  (#7)
             0, 0, 0,...                 % exchange  (#11,13,20)
             ];                          % [-]
qb2T_r = qf2T_r;

% building structure for the equilibrium constant
Keq_struc.nu_reac = nu_reac';
Keq_struc.nu_prod = nu_prod';
idx_bath        = 1; % CO2
idx_bathElement = 3; % O

% Surface catalytic recombinationo modeling
catReac_list = {'CO + O <-> CO2',...
                'C + O <-> CO',...
                '2O <-> O2'}; % catalytic reactions. They are actually one-directional

[nuCat_reac,nuCat_prod] = generateStoichiometricMatrices(catReac_list,spec_list); % getting stoichiometric coefficients for the gas-surface reactions

% building structure for the gas-surface interaction reactions
gasSurfReac_struct.nuCat_reac   = nuCat_reac.';
gasSurfReac_struct.nuCat_prod   = nuCat_prod.';
gasSurfReac_struct.catReac_list = catReac_list;


% Transposing everything, to have it in the desired form
nu_reac = nu_reac.';        nu_prod = nu_prod.';
nuEq_reac = nuEq_reac.';    nuEq_prod = nuEq_prod.';
nT_r = nT_r(:);             theta_r = theta_r(:);
A_r = A_r(:);
delta_nuEq = nuEq_prod - nuEq_reac;

end % getMixture_constants_mars5Mortensen