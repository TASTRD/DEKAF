function [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
    Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = getMixture_constants_air11Park01()
% getMixture_constants_air11Park01 returns the mixture constants for
% the air11Park01 mixture.
%
% Usage:
%   (1)
%       [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
%     Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = ...
%                                       getMixture_constants_air11Park01()
%
% Outputs: the same as <a href="matlab:help getMixture_constants">getMixture_constants</a>
%
% Species: (e-,N,N+,O,O+,NO,N2,N2+,O2,O2+,NO+)
%
% References:
%   Park, C., Jaffe, R. L., & Partridge, H. (2001). Chemical-Kinetic
%   Parameters of Hyperbolic Earth Entry. Journal of Thermophysics and Heat
%   Transfer, 15(1), 76–90. https://doi.org/10.2514/2.6582  
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% mutation's air11 model works with 11 air species (e-,N,N+,O,O+,NO,N2,N2+,O2,O2+,NO+)
spec_list = {'e-','N','N+','O','O+','NO','N2','N2+','O2','O2+','NO+'};
N_spec = 11;

% Stoichiometric coefficients
reac_list = {'N2 + M <-> 2N + M', ...   % Nitrogen dissociation (governed by sqrt(TTrans*TVib) forward and TTrans backwards)
             'O2 + M <-> 2O + M', ...   % Oxygen dissociation (governed by sqrt(TTrans*TVib) forward and TTrans backwards)
             'O + e- <-> O+ + 2e-', ... % Electron-impact ionization reactions (governed by Tel)
             'N + e- <-> N+ + 2e-', ...
             'N + O <-> NO+ + e-', ...  % Associative ionization reactions (governed by TTrans forward and Tel backwards)
             'N + N <-> N2+ + e-', ...
             'N2 + O <-> NO + N', ...   % Exchange reactions (governed by TTrans)
             'O + NO <-> N + O2'};
reac_list_eq = {'N2 <-> 2N', ...        % equilibrium reac. N2 diss.
                'O2 <-> 2O', ...        % equilibrium reac. O2 diss.
                'NO <-> N + O',...      % equilibrium reac. NO diss.
                ...'N2 <-> 2N+ + 2e-',...     % equilibrium reac. N+ cation creation
                ...'O2 <-> 2O+ + 2e-',...     % equilibrium reac. O+ cation creation
                ...'N2 + O2 <-> 2NO+ + 2e-',...    % equilibrium reac. NO+ cation creation
                'N <-> N+ + e-',...     % equilibrium reac. N+ cation creation
                'O <-> O+ + e-',...     % equilibrium reac. O+ cation creation
                'NO <-> NO+ + e-',...    % equilibrium reac. NO+ cation creation
                'N2 <-> N2+ + e-',...           % equilibrium reac. N2+ cation creation
                'O2 <-> O2+ + e-'};             % equilibrium reac. O2+ cation creation
[nu_reac,nu_prod]       = generateStoichiometricMatrices(reac_list,spec_list);
[nuEq_reac,nuEq_prod]   = generateStoichiometricMatrices(reac_list_eq,spec_list);

 % Arrhenius curve fittings for the reactions
 %              e-      N   N+      O   O+      NO      N2      N2+     O2      O2+  NO+
 A_r = [3e22 * [100,    1,  0.2333, 1,  0.2333, 0.2333, 0.2333, 0.2333, 0.2333, 0,  0.2333], ...    % N2 diss.
        2e21 * [0,      5,  1,      5,  1,      1,      1,      1,      1,      0,  1], ...         % O2 diss.
        3.9e33, 2.5e34, ...                                                                         % Electron-impaxt ioniz.
        5.3e12, 4.4e7, ...                                                                          % Associative ioniz.
        5.7e12, 8.4e12];                                                                            % Exchange
                                                                                                    % [(cm^3/mol)^(sum(nu_reac)-1) * K^(1/nT_r)]
 nT_r = [-1.6*ones(1,11), ...   % N2 diss.
         -1.5*ones(1,11), ...   % O2 diss.
         -3.78, -3.82, ...      % Electron-impact ioniz.
         0, 1.5, ...            % Associative ioniz.
         0.42, 0];              % Exchange
 theta_r = [113200*ones(1,11), ...  % N2 diss.
             59360*ones(1,11), ...  % O2 diss.
             158500,168200, ...     % Electron-impact ioniz.
             31900,67500, ...       % Associative ioniz.
             42938,19400];          % Exchange
qf2T_r   = [ 0.5 * ones(1,11),...   % N2 diss.
             0.5 * ones(1,11),...   % O2 diss.
             1, 1,...               % Electron-impact ioniz.
             0, 0,...               % Associative ioniz.
             0, 0 ];                % Exchange
qb2T_r   = [ zeros(1,11),...        % N2 diss.
             zeros(1,11),...        % O2 diss.
             1, 1,...               % Electron-impact ioniz.
             1, 1,...               % Associative ioniz.
             0, 0 ];                % Exchange
% passing to SI units
A_r = A_r.' .* (1e-6).^(sum(nu_reac,2)-1); % [(cm^3/mol)^(sum(nu_reac)-1) * ...] to [(m^3/mol)^(sum(nu_reac)-1) * ...]
% building structure for the equilibrium constant
Keq_struc.nu_reac = nu_reac';
Keq_struc.nu_prod = nu_prod';
idx_bath        = 7; % N2
idx_bathElement = 2; % N

% Surface catalytic recombinationo modeling
catReac_list = {'2N <-> N2',...                                     % catalytic reactions. They are actually one-directional
                '2O <-> O2'};

[nuCat_reac,nuCat_prod] = generateStoichiometricMatrices(catReac_list,spec_list); % getting stoichiometric coefficients for the gas-surface reactions

% building structure for the gas-surface interaction reactions
gasSurfReac_struct.nuCat_reac   = nuCat_reac.';
gasSurfReac_struct.nuCat_prod   = nuCat_prod.';
gasSurfReac_struct.catReac_list = catReac_list;


% Transposing everything, to have it in the desired form
nu_reac = nu_reac.';        nu_prod = nu_prod.';
nuEq_reac = nuEq_reac.';    nuEq_prod = nuEq_prod.';
nT_r = nT_r(:);             theta_r = theta_r(:);
A_r = A_r(:);
delta_nuEq = nuEq_prod - nuEq_reac;

end % getMixture_constants_air11Park01