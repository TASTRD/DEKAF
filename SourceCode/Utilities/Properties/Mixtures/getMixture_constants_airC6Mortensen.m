function [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
    Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = getMixture_constants_airC6Mortensen()
% getMixture_constants_airC6Mortensen returns the mixture constants for
% the airC6Mortensen mixture.
%
% Usage:
%   (1)
%       [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
%     Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = ...
%                                       getMixture_constants_airC6Mortensen()
%
% Outputs: the same as <a href="matlab:help getMixture_constants">getMixture_constants</a>
%
% Species: (CO2, N, O, NO, N2, O2)
%
% References:
%       Mortensen, C. (2013). PhD Thesis Effects of Thermochemical
%   Nonequilibrium on Hypersonic Boundary-Layer Instability in the Presence
%   of Surface Ablation or Isolated Two-Dimensional Roughness. UCLA.)
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

 % A run-down version of Mortensen's ablating air model works with 6 species
spec_list = {'CO2','N','O','NO','N2','O2'};
N_spec = 6;

% Stoichiometric coefficients
reac_list = {'N2 + M <-> 2N + M', ...       %  (#1 in MortensenThesis) air dissociation
             'O2 + M <-> 2O + M', ...       %  (#2)               \|/
             'NO + M <-> N + O + M', ...    %  (#3)                V
             'N2 + O <-> NO + N',...        %  (#9)    -----air exchange
             'NO + O <-> N + O2',...        % (#10)               \|/
             'N2 + O2 <-> 2NO',...          % (#24)                V
    };
reac_list_eq = {'NO <-> N + O',...      % equilibrium reac. NO diss.
                'N2 <-> 2N', ...        % equilibrium reac. N2 diss.
                'O2 <-> 2O', ...        % equilibrium reac. O2 diss.
    };
[nu_reac,nu_prod]       = generateStoichiometricMatrices(reac_list,spec_list);
[nuEq_reac,nuEq_prod]   = generateStoichiometricMatrices(reac_list_eq,spec_list);

 % Arrhenius curve fittings for the reactions
 A_r = [  3.70e15 * [1, 3, 3, 1, 1, 1], ...       % N2 diss.  (#1)
          2.75e13 * [1, 3, 3, 1, 1, 1], ...       % O2 diss.  (#2)
          2.30e11 * [1, 2, 3, 1, 1, 1], ...       % NO diss.  (#3)
          3.18e07, 2.16e02, 6.69e03  ...          % air exchange (#9,10,24)
          ];                                      % [(m^3/mol)^(sum(nu_reac)-1) * K^(1/nT_r)]
 nT_r = [ -1.6 * ones(1,N_spec), ...              % N2 diss.  (#1)
          -1.0 * ones(1,N_spec), ...              % O2 diss.  (#2)
          -0.5 * ones(1,N_spec), ...              % NO diss.  (#3)
           0.10, 1.29, -2.54  ...                 % air exchange (#9,10,24)
     ];
theta_r = [   113200 * ones(1,N_spec), ...              % N2 diss.  (#1)
               59500 * ones(1,N_spec), ...              % O2 diss.  (#2)
               75500 * ones(1,N_spec), ...              % NO diss.  (#3)
               37700, 19220, 64639  ...                 % air exchange (#9,10,24)
     ];                                                 % [K]
qf2T_r   = [ 0.5 * ones(1,N_spec),...    % N2 diss.
             0.5 * ones(1,N_spec),...    % O2 diss.
             0.5 * ones(1,N_spec),...    % NO diss.
             0, 0, 0];              % Exchange
qb2T_r = qf2T_r;

% building structure for the equilibrium constant
Keq_struc.nu_reac = nu_reac';
Keq_struc.nu_prod = nu_prod';
idx_bath        = 6; % N2
idx_bathElement = 2; % N

% Surface catalytic recombinationo modeling
catReac_list = {'2N <-> N2',...                                     % catalytic reactions. They are actually one-directional
                '2O <-> O2'};

[nuCat_reac,nuCat_prod] = generateStoichiometricMatrices(catReac_list,spec_list); % getting stoichiometric coefficients for the gas-surface reactions

% building structure for the gas-surface interaction reactions
gasSurfReac_struct.nuCat_reac   = nuCat_reac.';
gasSurfReac_struct.nuCat_prod   = nuCat_prod.';
gasSurfReac_struct.catReac_list = catReac_list;


% Transposing everything, to have it in the desired form
nu_reac = nu_reac.';        nu_prod = nu_prod.';
nuEq_reac = nuEq_reac.';    nuEq_prod = nuEq_prod.';
nT_r = nT_r(:);             theta_r = theta_r(:);
A_r = A_r(:);
delta_nuEq = nuEq_prod - nuEq_reac;

end % getMixture_constants_airC6Mortensen