function [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
    Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = getMixture_constants_nonReactive(N_spec)
% getMixture_constants_nonReactive returns empty mixture constants for a
% non-reactive mixture.
%
% Usage:
%   (1)
%       [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
%     Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = ...
%                                       getMixture_constants_nonReactive(N_spec)
%
% Outputs: the same as <a href="matlab:help getMixture_constants">getMixture_constants</a>
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

nu_reac             = zeros(N_spec,0);
nu_prod             = zeros(N_spec,0);
delta_nuEq          = zeros(N_spec,0);
A_r                 = [];
nT_r                = [];
theta_r             = [];
spec_list           = {};
Keq_struc           = struct;
reac_list           = {};
reac_list_eq        = {};
idx_bath            = [];
idx_bathElement     = [];
qf2T_r              = [];
qb2T_r              = [];
gasSurfReac_struct  = struct;

end % getMixture_constants_nonReactive