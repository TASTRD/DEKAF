function [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
    Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = getMixture_constants_air5Park85()
% getMixture_constants_air5Park85 returns the mixture constants for
% the air5Park85 mixture.
%
% Usage:
%   (1)
%       [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
%     Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = ...
%                                       getMixture_constants_air5Park85()
%
% Outputs: the same as <a href="matlab:help getMixture_constants">getMixture_constants</a>
%
% Species: (N,O,NO,N2,O2)
%
% References:
%       Park, C. (1985). On convergence of computation of chemically
%   reacting flows. AIAA Paper, 85–0247. https://doi.org/10.2514/6.1985-247 
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% The air5Park85 model works with 5 air species (N,O,NO,N2,O2)
spec_list = {   'N',    'O',    'NO',   'N2',   'O2'};
N_spec = 5;

% Stoichiometric coefficients
reac_list =    {'O2 + M <-> 2O + M', ...       % Oxygen dissociation
                'N2 + M <-> 2N + M ', ...      % Nitrogen dissociation
                'NO + M <->  N + O + M', ...   % Nitrous-oxide dissociation
                'NO + O <->  N + O2', ...      % Exchange reactions
                'O + N2 <->  N + NO'};
reac_list_eq = {'O2 <-> 2O', ... % equilibrium reac. O2 diss.
                'N2 <-> 2N', ... % equilibrium reac. N2 diss.
                'NO <-> N + O'}; % equilibrium reac. NO diss.
[nu_reac,nu_prod]       = generateStoichiometricMatrices(reac_list,spec_list);
[nuEq_reac,nuEq_prod]   = generateStoichiometricMatrices(reac_list_eq,spec_list);

% Arrhenius curve fittings for the reactions
%         | O2 diss.  atoms          molecules        | N2 diss.  atoms          molecules        | NO diss.  atoms          molecules        | Exchange reac.
A_r     = [ 8.25e19 * ones(1,2) , 2.75e19 * ones(1,3) , 1.11e22 * ones(1,2) , 3.70e21 * ones(1,3) , 4.60e17 * ones(1,2) , 2.30e17 * ones(1,3) , 2.16e8 , 3.18e13 ]; % [(cm^3/mol)^(sum(nu_reac)-1) * K^(1/nT_r)]
nT_r    = [ -1 * ones(1,5)                            , -1.6 * ones(1,5)                          , -0.5 * ones(1,5)                          , 1.29   , 0.1     ]; % [-]
theta_r = [ 59500 * ones(1,5)                         , 113200 * ones(1,5)                        , 75500 * ones(1,5)                         , 19220  , 37700   ]; % [K]
Aeq_r   = [ 1.335 * ones(1,5)                         ,  3.898 * ones(1,5)                        , 1.549 * ones(1,5)                         , 0.215  , 2.349   ;  % A1 eq. constants
           -4.127 * ones(1,5)                         ,-12.611 * ones(1,5)                        ,-7.784 * ones(1,5)                         ,-3.657  ,-4.828   ;  % A2 eq. constants
           -0.616 * ones(1,5)                         ,  0.683 * ones(1,5)                        , 0.228 * ones(1,5)                         , 0.843  , 0.455   ;  % A3 eq. constants
            0.093 * ones(1,5)                         , -0.118 * ones(1,5)                        ,-0.043 * ones(1,5)                         ,-0.136  ,-0.075   ;  % A4 eq. constants
           -0.005 * ones(1,5)                         ,  0.006 * ones(1,5)                        , 0.002 * ones(1,5)                         , 0.007  , 0.004   ]; % A5 eq. constants
qf2T_r   = [ 0.5 * ones(1,5)                          ,    0.5 * ones(1,5)                        ,   0.5 * ones(1,5)                         , 0      , 0       ]; % [-]
qb2T_r   = qf2T_r; % same temperature for forward and backward reactions

% passing to SI units
A_r = A_r.' .* (1e-6).^(sum(nu_reac,2)-1); % [(cm^3/mol)^(sum(nu_reac)-1) * ...] to [(m^3/mol)^(sum(nu_reac)-1) * ...]
Aeq_r(1,1:15) = Aeq_r(1,1:15) + 6*log(10); % passing from cm³ to m³ (only affects dissociation reactions,
                                           % which have a different number of products and reactants)
Keq_struc.Aeq_r = Aeq_r.';
Keq_struc.T0_eq = 1000; % [K] minimum temperature at which the correlation is applicable
Keq_struc.idx_eq = [1,6,11]; % indices of the reactions in Aeq_r corresponding to the equilibrium relations in reac_list_eq
Keq_struc.nu_reac = nu_reac';
Keq_struc.nu_prod = nu_prod';
idx_bath        = 4; % N2
idx_bathElement = 1; % N

% Surface catalytic recombinationo modeling
catReac_list = {'2N <-> N2',...                                     % catalytic reactions. They are actually one-directional
                '2O <-> O2'};

[nuCat_reac,nuCat_prod] = generateStoichiometricMatrices(catReac_list,spec_list); % getting stoichiometric coefficients for the gas-surface reactions

% building structure for the gas-surface interaction reactions
gasSurfReac_struct.nuCat_reac   = nuCat_reac.';
gasSurfReac_struct.nuCat_prod   = nuCat_prod.';
gasSurfReac_struct.catReac_list = catReac_list;


% Transposing everything, to have it in the desired form
nu_reac = nu_reac.';        nu_prod = nu_prod.';
nuEq_reac = nuEq_reac.';    nuEq_prod = nuEq_prod.';
nT_r = nT_r(:);             theta_r = theta_r(:);
A_r = A_r(:);
delta_nuEq = nuEq_prod - nuEq_reac;

end % getMixture_constants_air5Park85