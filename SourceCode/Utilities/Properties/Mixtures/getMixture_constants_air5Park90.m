function [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
    Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = getMixture_constants_air5Park90()
% getMixture_constants_air5Park90 returns the mixture constants for
% the air5Park90 mixture.
%
% Usage:
%   (1)
%       [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
%     Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = ...
%                                       getMixture_constants_air5Park90()
%
% Outputs: the same as <a href="matlab:help getMixture_constants">getMixture_constants</a>
%
% Species: (N,O,NO,N2,O2)
%
% References:
%       Park 1990, Nonequilibrium Hypersonic Aerothermodynamics. John Wiley
%   & Sons. 
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% The mutation's air5 model works with 5 air species (N,O,NO,N2,O2)
spec_list = {   'N',    'O',    'NO',   'N2',   'O2'};
N_spec = 5;

% Stoichiometric coefficients
reac_list =    {'N2 + M <-> 2N + M ', ...      % Nitrogen dissociation
                'O2 + M <-> 2O + M', ...       % Oxygen dissociation
                'NO + M <->  N + O + M', ...   % Nitrous-oxide dissociation
                'O + N2 <->  N + NO', ...      % Exchange reactions
                'NO + O <->  N + O2'}; % Park90
reac_list_eq = {'N2 <-> 2N', ... % equilibrium reac. N2 diss.
                'O2 <-> 2O', ... % equilibrium reac. O2 diss.
                'NO <-> N + O'}; % equilibrium reac. NO diss.
[nu_reac,nu_prod]       = generateStoichiometricMatrices(reac_list,spec_list);
[nuEq_reac,nuEq_prod]   = generateStoichiometricMatrices(reac_list_eq,spec_list);

% Arrhenius curve fittings for the reactions
%         |  N2 diss.                           | O2 diss.                    | NO diss.                | Exchange reac.
A_r     = [ 3.0e22 * [1,1,0.2333,0.2333,0.2333] ,  1.0e22 * [1,1,0.2,0.2,0.2] , 5.0e15 * [20 20 20 1 1] , 6.4e17  , 8.4e12 ]; % [(cm^3/mol)^(sum(nu_reac)-1) * K^(1/nT_r)]
nT_r    = [ -1.6 * ones(1,5)                    ,  -1.5 * ones(1,5)           , 0 * ones(1,5)           , -1      , 0      ]; % [-]
theta_r = [ 113200 * ones(1,5)                  ,  59360 * ones(1,5)          , 75500 * ones(1,5)       , 38400   , 19220  ]; % [K]
qf2T_r   = [ 0.5 * ones(1,5)                    ,    0.5 * ones(1,5)          ,   0.5 * ones(1,5)       , 0      , 0       ]; % [-]
qb2T_r   = qf2T_r; % same temperature for forward and backward reactions
% passing to SI units
A_r = A_r.' .* (1e-6).^(sum(nu_reac,2)-1); % [(cm^3/mol)^(sum(nu_reac)-1) * ...] to [(m^3/mol)^(sum(nu_reac)-1) * ...]
% building structure for the equilibrium constant
Keq_struc.nu_reac = nu_reac';
Keq_struc.nu_prod = nu_prod';
idx_bath        = 4; % N2
idx_bathElement = 1; % N

% Surface catalytic recombinationo modeling
catReac_list = {'2N <-> N2',...                                     % catalytic reactions. They are actually one-directional
                '2O <-> O2'};

[nuCat_reac,nuCat_prod] = generateStoichiometricMatrices(catReac_list,spec_list); % getting stoichiometric coefficients for the gas-surface reactions

% building structure for the gas-surface interaction reactions
gasSurfReac_struct.nuCat_reac   = nuCat_reac.';
gasSurfReac_struct.nuCat_prod   = nuCat_prod.';
gasSurfReac_struct.catReac_list = catReac_list;


% Transposing everything, to have it in the desired form
nu_reac = nu_reac.';        nu_prod = nu_prod.';
nuEq_reac = nuEq_reac.';    nuEq_prod = nuEq_prod.';
nT_r = nT_r(:);             theta_r = theta_r(:);
A_r = A_r(:);
delta_nuEq = nuEq_prod - nuEq_reac;

end % getMixture_constants_air5Park90