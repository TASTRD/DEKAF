function [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
    Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = getMixture_constants_airC11Mortensen(numberOfTemp)
% getMixture_constants_airC11Mortensen returns the mixture constants for
% the airC11Mortensen mixture.
%
% Usage:
%   (1)
%       [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
%     Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = ...
%                                       getMixture_constants_airC11Mortensen(numberOfTemp)
%
% Inputs:
%   numberOfTemp
%       string identifying the number of temperatures used to define the
%       thermodynamic state of the system.
%
% Outputs: the same as <a href="matlab:help getMixture_constants">getMixture_constants</a>
%
% Species: (C3, CO2, C2, CO, CN, C, N, O, NO, N2, O2)
%
% References:
%       Mortensen, C. (2013). PhD Thesis Effects of Thermochemical
%   Nonequilibrium on Hypersonic Boundary-Layer Instability in the Presence
%   of Surface Ablation or Isolated Two-Dimensional Roughness. UCLA.)
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% Mortensen's ablating air model works with 11 species
spec_list = {'C3','CO2','C2','CO','CN','C','N','O','NO','N2','O2'};
N_spec = 11;

% Stoichiometric coefficients
reac_list = {'N2 + M <-> 2N + M', ...       %  (#1 in MortensenThesis) air dissociation
             'O2 + M <-> 2O + M', ...       %  (#2)               \|/
             'NO + M <-> N + O + M', ...    %  (#3)                V
             'C3 + M <-> C2 + C + M',...    %  (#4)    -----carbon-species dissociation
             'CO2 + M <-> CO + O + M',...   %  (#5)                |
             'C2 + M <-> 2C + M',...        %  (#6)                |
             'CO + M <-> C + O + M',...     %  (#7)               \|/
             'CN + M <-> C + N + M',...     %  (#8)                V
             'N2 + O <-> NO + N',...        %  (#9)    -----air exchange
             'NO + O <-> N + O2',...        % (#10)               \|/
             'N2 + O2 <-> 2NO',...          % (#24)                V
             'CO + O <-> C + O2',...        % (#11)    -----carbon-species exchange
             'CN + O <-> NO + C',...        % (#12)                |
             'CO2 + O <-> O2 + CO',...      % (#13)                |
             'CO + C <-> C2 + O',...        % (#14)                |
             'N2 + C <-> CN + N',...        % (#15)                |
             'CN + C <-> C2 + N',...        % (#16)    - - - -     |
             'C3 + C <-> 2C2',...           % (#17)                |
             'CO + N <-> CN + O',...        % (#18)                |
             'CO + N <-> NO + C',...        % (#19)                |
             '2CO <-> CO2 + C',...          % (#20)                |
             'C2 + CO <-> C3 + O',...       % (#21)    - - - -     |
             '2CO <-> C2 + O2',...          % (#22)               \|/
             'CO + NO <-> CO2 + N' ...      % (#23)                V
    };
reac_list_eq = {'C3 <-> 3C',...         % equilibrium reac. C3 diss.
                'CO2 <-> C + 2O',...    % equilibrium reac. CO2 diss.
                'C2 <-> 2C',...         % equilibrium reac. C2 diss.
                'CO <-> C + O',...      % equilibrium reac. CO diss.
                'CN <-> C + N',...      % equilibrium reac. CN diss.
                'NO <-> N + O',...      % equilibrium reac. NO diss.
                'N2 <-> 2N', ...        % equilibrium reac. N2 diss.
                'O2 <-> 2O', ...        % equilibrium reac. O2 diss.
    };
[nu_reac,nu_prod]       = generateStoichiometricMatrices(reac_list,spec_list);
[nuEq_reac,nuEq_prod]   = generateStoichiometricMatrices(reac_list_eq,spec_list);

 % Arrhenius curve fittings for the reactions
 one5 = ones(1,5); one3 = ones(1,3);
 A_r = [  3.70e15 * [one5, 3*one3, one3], ...               % N2 diss.  (#1)
          2.75e13 * [one5, 3*one3, one3], ...               % O2 diss.  (#2)
          2.30e11 * [one5, 2*one3, one3], ...               % NO diss.  (#3)
          1.60e10 * ones(1,N_spec), ...                     % C3 diss.  (#4)
          1.20e05 * ones(1,N_spec), ...                     % CO2 diss. (#5)
          4.50e12 * ones(1,N_spec), ...                     % C2 diss.  (#6)
          8.50e13 * ones(1,N_spec), ...                     % CO diss.  (#7)
          2.50e08 * ones(1,N_spec), ...                     % CN diss.  (#8)
          3.18e07, 2.16e02, 6.69e03, ...                    % air exchange (#9,10,24)
          2.00e04, 1.60e07, 3.00e02, 4.10e04, 2.00e08,...   % carbon-species exchange (#11-15)
          5.00e07, 1.70e03, 2.00e08, 9.00e10, 1.00e-3,...   %                         (#16-20)
          1.20e07, 9.20e05, 1.00e-3 ...                     %                         (#21-23)
          ];                                                % [(m^3/mol)^(sum(nu_reac)-1) * K^(1/nT_r)]
 nT_r = [ -1.6 * ones(1,N_spec), ...              % N2 diss.  (#1)
          -1.0 * ones(1,N_spec), ...              % O2 diss.  (#2)
          -0.5 * ones(1,N_spec), ...              % NO diss.  (#3)
           1.0 * ones(1,N_spec), ...              % C3 diss.  (#4)
           0.5 * ones(1,N_spec), ...              % CO2 diss. (#5)
          -1.0 * ones(1,N_spec), ...              % C2 diss.  (#6)
          -1.0 * ones(1,N_spec), ...              % CO diss.  (#7)
           0.0 * ones(1,N_spec), ...              % CN diss.  (#8)
           0.10, 1.29, -2.54, ...                 % air exchange (#9,10,24)
           1.00, 0.10, 1.00, 0.50, 0.00, ...      % carbon species exchange (#11-15)
           0.00, 1.50, 0.00, -1.00, 2.00, ...     %                         (#16-20)
           0.00, 0.75, 2.00  ...                  %                         (#21-23)
     ];
theta_r = [   113200 * ones(1,N_spec), ...              % N2 diss.  (#1)
               59500 * ones(1,N_spec), ...              % O2 diss.  (#2)
               75500 * ones(1,N_spec), ...              % NO diss.  (#3)
               87480 * ones(1,N_spec), ...              % C3 diss.  (#4)
               36850 * ones(1,N_spec), ...              % CO2 diss. (#5)
               70930 * ones(1,N_spec), ...              % C2 diss.  (#6)
              129000 * ones(1,N_spec), ...              % CO diss.  (#7)
               71000 * ones(1,N_spec), ...              % CN diss.  (#8)
               37700, 19220, 64639, ...                 % air exchange (#9,10,24)
               69500, 14600, 18210, 59790, 23200, ...   % carbon species exchange (#11-15)
               13000, 19580, 38600, 53200, 72390, ...   %                         (#16-20)
               43240, 163300, 20980  ...                %                         (#21-23)
     ];                                                 % [K]
qf2T_r   = [ 0.5 * ones(1,N_spec),...    % N2 diss.  (#1)
             0.5 * ones(1,N_spec),...    % O2 diss.  (#2)
             0.5 * ones(1,N_spec),...    % NO diss.  (#3)
             0.5 * ones(1,N_spec),...    % C3 diss.  (#4)
             0.5 * ones(1,N_spec),...    % CO2 diss. (#5)
             0.5 * ones(1,N_spec),...    % C2 diss.  (#6)
             0.5 * ones(1,N_spec),...    % CO diss.  (#7)
             0.5 * ones(1,N_spec),...    % CN diss.  (#8)
             0, 0, 0,...                 % air exchange (#9,10,24)
             0, 0, 0, 0, 0,...           % carbon species exchange (#11-15)
             0, 0, 0, 0, 0,...           %                         (#16-20)
             0, 0, 0,...                 %                         (#21-23)
             ];                          % [-]
qb2T_r = qf2T_r;

% building structure for the equilibrium constant
Keq_struc.nu_reac = nu_reac';
Keq_struc.nu_prod = nu_prod';
idx_bath        = 11; % O2
idx_bathElement = 2; % N

% Gas-surface interaction modeling
gasSurfReac_list = {'Cgr + O2 <-> CO + O', ...                      % list of oxidation reactions
                    'Cgr + O <-> CO', ...
                    'Cgr + 2O <-> Cgr + O2'};
gasSurfReac_type = {'Park76_opt1', ...                              % type of reactions (how to obtain production rates)
                    'Park76_opt2', ...
                    'Park76_opt2'};
A_GSr     = [0 , 0 , 0];                                            % Arrhenius curve-fits for the reactions (only needed for Arrhenius reactions - none of these)
nT_GSr    = [0 , 0 , 0];
theta_GSr = [0 , 0 , 0];
qf_GSr    = [0 , 0 , 0];                                            % coefficients for the vibrational temperature in the reaction temperature's geometric average
oxidPark76      = {'O2','O','O'};                                   % oxidizing species according to park's 1976 expressions (only needed for Park76_opt1 and Park76_opt2 reactions)
sublReac_spec   = {'C','C2','C3'};                                  % list of species will experience sublimation reactions due to the surface

[nuGS_reac,nuGS_prod]       = generateStoichiometricMatrices(gasSurfReac_list,spec_list); % getting stoichiometric coefficients for the gas-surface reactions
sublSpec_bool = ismember(spec_list,sublReac_spec);                  % boolean storing whether or not each species will sublimate
oxidPark76_idx = cellfun(@(cll)find(ismember(spec_list,cll)),oxidPark76); % index of the oxidizing species according to Park 1976

bPark76_opt1 = strcmp(gasSurfReac_type,'Park76_opt1');              % boolean that is true for park's first option and false for the second

switch numberOfTemp
    case '1T'
        qf_GSr = ones(size(qf_GSr));
    case '2T'
        qf_GSr = [ones(size(qf_GSr)) - qf_GSr ; qf_GSr];
    otherwise
        error(['the chosen number of temperatures ''',numberOfTemp,''' is not supported']);
end

% building structure for the gas-surface interaction reactions
gasSurfReac_struct.nuGS_reac        = nuGS_reac.';
gasSurfReac_struct.nuGS_prod        = nuGS_prod.';
gasSurfReac_struct.gasSurfReac_list = gasSurfReac_list;
gasSurfReac_struct.gasSurfReac_type = gasSurfReac_type;
gasSurfReac_struct.bPark76_opt1     = bPark76_opt1;
gasSurfReac_struct.A_GSr            = A_GSr;
gasSurfReac_struct.nT_GSr           = nT_GSr;
gasSurfReac_struct.theta_GSr        = theta_GSr;
gasSurfReac_struct.sublSpec_bool    = sublSpec_bool;
gasSurfReac_struct.oxidPark76_idx   = oxidPark76_idx;
gasSurfReac_struct.qf_GSr           = qf_GSr;
gasSurfReac_struct.epsilonRadSurf   = 0.9;              % black body coefficient of graphite
gasSurfReac_struct.Tw_max           = 5000;             % [K] maximum temperature allowable by the ablating material



% Transposing everything, to have it in the desired form
nu_reac = nu_reac.';        nu_prod = nu_prod.';
nuEq_reac = nuEq_reac.';    nuEq_prod = nuEq_prod.';
nT_r = nT_r(:);             theta_r = theta_r(:);
A_r = A_r(:);
delta_nuEq = nuEq_prod - nuEq_reac;

end % getMixture_constants_airC11Mortensen