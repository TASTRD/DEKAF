function [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
    Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = getMixture_constants_air5Bortner()
% getMixture_constants_air5Bortner returns the mixture constants for
% the air5Bortner mixture.
%
% Usage:
%   (1)
%       [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
%     Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = ...
%                                       getMixture_constants_air5Bortner()
%
% Outputs: the same as <a href="matlab:help getMixture_constants">getMixture_constants</a>
%
% Species: (N,O,NO,N2,O2)
%
% References:
%       Gupta, R. N., Yos, J. M., Thompson, R. A., & Lee, K.-P. (1990). A
%   review of Reaction Rates and Thermodynamic and Transport Properties for
%   an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%   Calculations to 30000K.
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% The air5Bortner model works with 5 air species (N,O,NO,N2,O2)
spec_list = {   'N',    'O',    'NO',   'N2',   'O2'};
N_spec = 5;

% Stoichiometric coefficients
reac_list =    {'O2 + M <-> 2O + M', ...       % Oxygen dissociation
                'N2 + M <-> 2N + M ', ...      % Nitrogen dissociation
                'NO + M <->  N + O + M', ...   % Nitrous-oxide dissociation
                'NO + O <->  N + O2', ...      % Exchange reactions
                'O + N2 <->  N + NO'};
reac_list_eq = {'O2 <-> 2O', ... % equilibrium reac. O2 diss.
                'N2 <-> 2N', ... % equilibrium reac. N2 diss.
                'NO <-> N + O'}; % equilibrium reac. NO diss.
[nu_reac,nu_prod]       = generateStoichiometricMatrices(reac_list,spec_list);
[nuEq_reac,nuEq_prod]   = generateStoichiometricMatrices(reac_list_eq,spec_list);

% Arrhenius curve fittings for the reactions
%         | O2 diss.            | N2 diss.  N          others           | NO diss.              | Exchange reac.
A_r     = [ 3.61e18 * ones(1,5) , 4.15e22       , 1.92e17 * ones(1,4)   , 3.97e20 * ones(1,5)   , 3.18e9    , 6.75e13   ]; % [(cm^3/mol)^(sum(nu_reac)-1) * K^(1/nT_r)]
nT_r    = [ -1 * ones(1,5)      , -1.5          , -0.5 * ones(1,4)      , -1.5 * ones(1,5)      , 1         , 0         ]; % [-]
theta_r = [ 5.94e4 * ones(1,5)  , 1.131e5       , 1.131e5 * ones(1,4)   , 7.56e4 * ones(1,5)    , 1.97e4    , 3.75e4    ]; % [K]
qf2T_r   = [ 0.5 * ones(1,5)                    ,    0.5 * ones(1,5)    ,   0.5 * ones(1,5)     , 0         , 0       ]; % [-]
qb2T_r   = qf2T_r; % same temperature for forward and backward reactions

% Arrhenius curve fittings for the BACKWARD reactions
%         | O2 diss.            | N2 diss.  N          others           | NO diss.              | Exchange reac.
A_br    = [ 3.01e15 * ones(1,5) , 2.32e21       , 1.09e16 * ones(1,4)   , 1.01e20 * ones(1,5)   , 9.63e11   , 1.5e13    ]; % [(cm^3/mol)^(sum(nu_prod)-1) * K^(1/nT_br)]
nT_br   = [ -0.5 * ones(1,5)    , -1.5          , -0.5 * ones(1,4)      , -1.5 * ones(1,5)      , 0.5       , 0         ]; % [-]
theta_br= [                                 zeros(1,15)                                         , 3.6e3     , 0         ]; % [K] (many have no exponential dependency)

% passing to SI units
A_r  = A_r.'  .* (1e-6).^(sum(nu_reac,2)-1); % [(cm^3/mol)^(sum(nu_reac)-1) * ...] to [(m^3/mol)^(sum(nu_reac)-1) * ...]
A_br = A_br.' .* (1e-6).^(sum(nu_prod,2)-1); % [(cm^3/mol)^(sum(nu_prod)-1) * ...] to [(m^3/mol)^(sum(nu_prod)-1) * ...]

% building structure for the equilibrium constant
Keq_struc.nu_reac = nu_reac';
Keq_struc.nu_prod = nu_prod';
Keq_struc.idx_eq = [5,10,15]; % indices of the reactions in Aeq_r corresponding to the equilibrium relations in reac_list_eq
Keq_struc.A_eqr = A_r./A_br;
Keq_struc.nT_eqr = nT_r.' - nT_br.';
Keq_struc.theta_eqr = theta_r.' - theta_br.';
idx_bath        = 4; % N2
idx_bathElement = 1; % N

% Surface catalytic recombinationo modeling
catReac_list = {'2N <-> N2',...                                     % catalytic reactions. They are actually one-directional
                '2O <-> O2'};

[nuCat_reac,nuCat_prod] = generateStoichiometricMatrices(catReac_list,spec_list); % getting stoichiometric coefficients for the gas-surface reactions

% building structure for the gas-surface interaction reactions
gasSurfReac_struct.nuCat_reac   = nuCat_reac.';
gasSurfReac_struct.nuCat_prod   = nuCat_prod.';
gasSurfReac_struct.catReac_list = catReac_list;


% Transposing everything, to have it in the desired form
nu_reac = nu_reac.';        nu_prod = nu_prod.';
nuEq_reac = nuEq_reac.';    nuEq_prod = nuEq_prod.';
nT_r = nT_r(:);             theta_r = theta_r(:);
A_r = A_r(:);
delta_nuEq = nuEq_prod - nuEq_reac;

end % getMixture_constants_air5Bortner