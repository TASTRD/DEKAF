function [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
    Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = getMixture_constants_mars8Mortensen()
% getMixture_constants_mars8Mortensen returns the mixture constants for
% the mars8Mortensen mixture.
%
% Usage:
%   (1)
%       [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
%     Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = ...
%                                       getMixture_constants_mars8Mortensen()
%
% Outputs: the same as <a href="matlab:help getMixture_constants">getMixture_constants</a>
%
% Species: (CO2, CO, C, N, O, NO, N2, O2)
%
% References:
%       Mortensen, C. (2013). PhD Thesis Effects of Thermochemical
%   Nonequilibrium on Hypersonic Boundary-Layer Instability in the Presence
%   of Surface Ablation or Isolated Two-Dimensional Roughness. UCLA.)
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% 8-species Martian atmosphere built from Mortensen's ablating air model with 11 species
spec_list = {'CO2','CO','C','N','O','NO','N2','O2'};
N_spec = 8;

% Stoichiometric coefficients
reac_list = {'N2 + M <-> 2N + M', ...       %  (#1 in MortensenThesis) dissociation
             'O2 + M <-> 2O + M', ...       %  (#2)                |
             'NO + M <-> N + O + M', ...    %  (#3)                |
             'CO2 + M <-> CO + O + M',...   %  (#5)               \|/
             'CO + M <-> C + O + M',...     %  (#7)                v
             'N2 + O <-> NO + N',...        %  (#9)    -----air exchange
             'NO + O <-> N + O2',...        % (#10)               \|/
             'N2 + O2 <-> 2NO',...          % (#24)                V
             'CO + O <-> C + O2',...        % (#11)    -----carbon-species exchange
             'CO2 + O <-> O2 + CO',...      % (#13)                |
             'CO + N <-> NO + C',...        % (#19)                |
             '2CO <-> CO2 + C',...          % (#20)               \|/
             'CO + NO <-> CO2 + N' ...      % (#23)                V
    };
reac_list_eq = {'CO2 <-> C + 2O',...    % equilibrium reac. CO2 diss.
                'CO <-> C + O',...      % equilibrium reac. CO diss.
                'NO <-> N + O',...      % equilibrium reac. NO diss.
                'N2 <-> 2N', ...        % equilibrium reac. N2 diss.
                'O2 <-> 2O', ...        % equilibrium reac. O2 diss.
    };
[nu_reac,nu_prod]       = generateStoichiometricMatrices(reac_list,spec_list);
[nuEq_reac,nuEq_prod]   = generateStoichiometricMatrices(reac_list_eq,spec_list);

 % Arrhenius curve fittings for the reactions
 one2 = ones(1,2); one3 = ones(1,3);
 A_r = [  3.70e15 * [one2, 3*one3, one3], ...               % N2 diss.  (#1)
          2.75e13 * [one2, 3*one3, one3], ...               % O2 diss.  (#2)
          2.30e11 * [one2, 2*one3, one3], ...               % NO diss.  (#3)
   ...    0 * ones(1,N_spec), ...                           % CO2 diss. (#5) DEBUGGING
          1.20e05 * ones(1,N_spec), ...                     % CO2 diss. (#5)
          8.50e13 * ones(1,N_spec), ...                     % CO diss.  (#7)
          3.18e07, 2.16e02, 6.69e03, ...                    % air exchange (#9,10,24)
          2.00e04, 3.00e02, 9.00e10, 1.00e-3, 1.00e-3 ...   % carbon-species exchange (#11,13,19,20,23)
          ];                                                % [(m^3/mol)^(sum(nu_reac)-1) * K^(1/nT_r)]
 nT_r = [ -1.6 * ones(1,N_spec), ...              % N2 diss.  (#1)
          -1.0 * ones(1,N_spec), ...              % O2 diss.  (#2)
          -0.5 * ones(1,N_spec), ...              % NO diss.  (#3)
           0.5 * ones(1,N_spec), ...              % CO2 diss. (#5)
          -1.0 * ones(1,N_spec), ...              % CO diss.  (#7)
           0.10, 1.29, -2.54, ...                 % air exchange (#9,10,24)
           1.00, 1.00, -1.00, 2.00, 2.00  ...     % carbon-species exchange (#11,13,19,20,23)
     ];
theta_r = [   113200 * ones(1,N_spec), ...              % N2 diss.  (#1)
               59500 * ones(1,N_spec), ...              % O2 diss.  (#2)
               75500 * ones(1,N_spec), ...              % NO diss.  (#3)
               36850 * ones(1,N_spec), ...              % CO2 diss. (#5)
              129000 * ones(1,N_spec), ...              % CO diss.  (#7)
               37700, 19220, 64639, ...                 % air exchange (#9,10,24)
               69500, 18210, 53200, 72390, 20980  ...   % carbon-species exchange (#11,13,19,20,23)
     ];                                                 % [K]
qf2T_r   = [ 0.5 * ones(1,N_spec),...    % N2 diss.  (#1)
             0.5 * ones(1,N_spec),...    % O2 diss.  (#2)
             0.5 * ones(1,N_spec),...    % NO diss.  (#3)
             0.5 * ones(1,N_spec),...    % CO2 diss. (#5)
             0.5 * ones(1,N_spec),...    % CO diss.  (#7)
             0, 0, 0,...                 % air exchange (#9,10,24)
             0, 0, 0, 0, 0,...           % carbon-species exchange (#11,13,19,20,23)
             ];                          % [-]
qb2T_r = qf2T_r;

% building structure for the equilibrium constant
Keq_struc.nu_reac = nu_reac';
Keq_struc.nu_prod = nu_prod';
idx_bath        = 1; % CO2
idx_bathElement = 3; % O

% Surface catalytic recombinationo modeling
catReac_list = {'CO + O <-> CO2',...
                'C + O <-> CO',...
                '2O <-> O2'}; % catalytic reactions. They are actually one-directional

[nuCat_reac,nuCat_prod] = generateStoichiometricMatrices(catReac_list,spec_list); % getting stoichiometric coefficients for the gas-surface reactions

% building structure for the gas-surface interaction reactions
gasSurfReac_struct.nuCat_reac   = nuCat_reac.';
gasSurfReac_struct.nuCat_prod   = nuCat_prod.';
gasSurfReac_struct.catReac_list = catReac_list;

% Transposing everything, to have it in the desired form
nu_reac = nu_reac.';        nu_prod = nu_prod.';
nuEq_reac = nuEq_reac.';    nuEq_prod = nuEq_prod.';
nT_r = nT_r(:);             theta_r = theta_r(:);
A_r = A_r(:);
delta_nuEq = nuEq_prod - nuEq_reac;

end % getMixture_constants_mars8Mortensen