function [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
    Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = getMixture_constants_oxygen2Park01()
% getMixture_constants_oxygen2Park01 returns the mixture constants for
% the oxygen2Park01 mixture.
%
% Usage:
%   (1)
%       [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
%     Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = ...
%                                       getMixture_constants_oxygen2Park01()
%
% Outputs: the same as <a href="matlab:help getMixture_constants">getMixture_constants</a>
%
% Species: (O,O2)
%
% References:
%       Park, C., Jaffe, R. L., & Partridge, H. (2001). Chemical-Kinetic
%   Parameters of Hyperbolic Earth Entry. Journal of Thermophysics and Heat
%   Transfer, 15(1), 76–90. https://doi.org/10.2514/2.6582
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% The oxygen2 model works with 2 oxygen species (O,O2)
spec_list = {'O','O2'};
N_spec = length(spec_list);

% Stoichiometric coefficients
reac_list = {'O2 + M <-> 2O + M'}; % Oxygen dissociation
reac_list_eq = {'O2 <-> 2O'};       % equilibrium reac. O2 diss.

[nu_reac,nu_prod] = ...
    generateStoichiometricMatrices(reac_list,spec_list);
[nuEq_reac,nuEq_prod] = ...
    generateStoichiometricMatrices(reac_list_eq,spec_list);

% Using the same data from 'air5mutation' below
% Arrhenius curve fittings for the reactions
% In A_r, the 1 corresponds to atoms, the 0.2 corresponds to ...
% molecules
%         |  O2 diss.
A_r     =    1.0e22 * [1,0.2] ;             % [(cm^3/mol)^(sum(nu_reac)-1) * K^(1/nT_r)]
nT_r    =   -1.5 * ones(1,N_spec);          % [-]
theta_r =    59360 * ones(1,N_spec);        % [K]
qf2T_r    = 0.5*ones(1,N_spec);             % [-]
qb2T_r   = qf2T_r; % same temperature for forward and backward reactions
% passing to SI units
A_r = A_r.' .* (1e-6).^(sum(nu_reac,2)-1); % [(cm^3/mol)^(sum(nu_reac)-1) * ...] to [(m^3/mol)^(sum(nu_reac)-1) * ...]
% building structure for the equilibrium constant
Keq_struc.nu_reac = nu_reac';
Keq_struc.nu_prod = nu_prod';
idx_bath        = 2; % O2
idx_bathElement = 1; % O

% Surface catalytic recombinationo modeling
catReac_list = {'2O <-> O2'}; % catalytic reactions. They are actually one-directional

[nuCat_reac,nuCat_prod] = generateStoichiometricMatrices(catReac_list,spec_list); % getting stoichiometric coefficients for the gas-surface reactions

% building structure for the gas-surface interaction reactions
gasSurfReac_struct.nuCat_reac   = nuCat_reac.';
gasSurfReac_struct.nuCat_prod   = nuCat_prod.';
gasSurfReac_struct.catReac_list = catReac_list;

% Transposing everything, to have it in the desired form
nu_reac = nu_reac.';        nu_prod = nu_prod.';
nuEq_reac = nuEq_reac.';    nuEq_prod = nuEq_prod.';
nT_r = nT_r(:);             theta_r = theta_r(:);
A_r = A_r(:);
delta_nuEq = nuEq_prod - nuEq_reac;

end % getMixture_constants_oxygen2Park01