function [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
    Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = getMixture_constants_air11Park93Bose()
% getMixture_constants_air11Park93Bose returns the mixture constants for
% the air11Park93Bose mixture.
%
% Usage:
%   (1)
%       [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
%     Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = ...
%                                       getMixture_constants_air11Park93Bose()
%
% Outputs: the same as <a href="matlab:help getMixture_constants">getMixture_constants</a>
%
% Species: (N,O,N2,O2,NO,N+,O+,N2+,O2+,NO+,e-)
%
% Reactions: all from Park (1993), except for the exchange reactions from
% Bose & Candler (1996,1997)
%
% References:
%       Park, C. (1993). Review of chemical-kinetic problems of future NASA
%   missions. I - Earth entries. Journal of Thermophysics and Heat
%   Transfer, 7(3), 385–398. https://doi.org/10.2514/3.496
%       Bose, D., & Candler, G. V. (1996). Thermal rate constants of the
%   N2+O<>NO+N reaction using ab initio 3A’’ and3A’ potential energy
%   surfaces. Journal of Chemical Physics, 104(8), 2825–2833.
%   https://doi.org/10.1063/1.471106
%       Bose, D., & Candler, G. V. (1997). Thermal rate constants of the
%   O2+N <> NO+O reaction based on the 2A’ and 4A’ potential-energy
%   surfaces. The Journal of Chemical Physics, 107(16), 6136–6145.
%   https://doi.org/10.1063/1.475132
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% Park's air11 model works with 11 air species (N,O,N2,O2,NO,N+,O+,N2+,O2+,NO+,e-)
spec_list = {'N','O','N2','O2','NO','N+','O+','N2+','O2+','NO+','e-'};
N_spec = 11;

% Stoichiometric coefficients
reac_list =    {'N2 + M <-> 2N + M ', ...      % Nitrogen dissociation (governed by sqrt(TTrans*TVib) forward and TTrans backwards)
                'O2 + M <-> 2O + M', ...       % Oxygen dissociation (governed by sqrt(TTrans*TVib) forward and TTrans backwards)
                'NO + M <->  N + O + M', ...   % Nitrous-oxide dissociation  (governed by sqrt(TTrans*TVib) forward and TTrans backwards)
                'NO + O <->  N + O2', ...      % Exchange reactions (governed by TTrans)
                'N2 + O <->  NO + N', ...
                'N + O <-> NO+ + e-', ...       % Associative ionization reactions (governed by TTrans forward and Tel backwards)
                'O + O <-> O2+ + e-', ...
                'N + N <-> N2+ + e-', ...
                'NO+ + O <-> N+ + O2', ...      % Charge exchange reactions (governed by TTrans)
                'N+ + N2 <-> N2+ + N', ...
                'O2+ + N <-> N+ + O2', ...
                'O+ + NO <-> N+ + O2', ...
                'O2+ + N2 <-> N2+ + O2', ...
                'O2+ + O <-> O+ + O2', ...
                'NO+ + N <-> O+ + N2', ...
                'NO+ + O2 <-> O2+ + NO', ...
                'NO+ + O <-> O2+ + N', ...
                'O+ + N2 <-> N2+ + O', ...
                'NO+ + N <-> N2+ + O', ...
                'O + e- <-> O+ + 2e-', ...      % Electron-impact ionization reactions (governed by Tel)
                'N + e- <-> N+ + 2e-'};
reac_list_eq = {'N2 <-> 2N', ...        % equilibrium reac. N2 diss.
                'O2 <-> 2O', ...        % equilibrium reac. O2 diss.
                'NO <-> N + O',...      % equilibrium reac. NO diss.
                'N <-> N+ + e-',...     % equilibrium reac. N+ cation creation
                'O <-> O+ + e-',...     % equilibrium reac. O+ cation creation
                'NO <-> NO+ + e-',...   % equilibrium reac. NO+ cation creation
                'N2 <-> N2+ + e-',...   % equilibrium reac. N2+ cation creation
                'O2 <-> O2+ + e-'};     % equilibrium reac. O2+ cation creation
[nu_reac,nu_prod]       = generateStoichiometricMatrices(reac_list,spec_list);
[nuEq_reac,nuEq_prod]   = generateStoichiometricMatrices(reac_list_eq,spec_list);

% Arrhenius curve fittings for the reactions
%              N   O   N2       O2      NO     N+  O+  N2+     O2+     NO+     e-
A_r = [3e22 * [1,  1, 0.2333,  0.2333, 0.2333, 1,  1,  0.2333, 0.2333, 0.2333, 100], ...       % N2 diss.
        1e22 * [1,  1, 0.2,     0.2,    0.2,    1,  1,  0.2,    0.2,    0.2,    0], ...         % O2 diss.
        5e15 * [22, 22, 1,      1,      22,     22, 22, 1,      1,      1,      0], ...         % NO diss.
        5.3e12, 4.4e7, ...                                                                      % Exchange
        8.8e8, 7.1e2, 4.4e7, ...                                                                % Associative ioniz.
        1e12, 1e12, 8.7e13, 1.4e5, 9.9e12, 4e12, 3.4e13, 2.4e13, 7.2e12, 9.1e11, 7.2e13, ...    % Charge exchange
        3.9e33, 2.5e34];                                                                        % Electron-impact Ioniz.
                                                                                                % [(cm^3/mol)^(sum(nu_reac)-1) * K^(1/nT_r)]
nT_r = [-1.6*ones(1,11), ...                                                % N2 diss.
        -1.5*ones(1,11), ...                                                % O2 diss.
         0 * ones(1,11), ...                                                % NO diss.
        0.42, 0, ...                                                        % Exchange
        1, 2.7, 1.5, ...                                                    % Associative ioniz.
        0.5, 0.5, 0.14, 1.90, 0, -0.09, -1.08, 0.41, 0.29, 0.36, 0, ...     % Charge exchange
        -3.78, -3.82];                                                      % Electron-impact ioniz.
theta_r = [113200*ones(1,11), ...                                                   % N2 diss.
            59500*ones(1,11), ...                                                   % O2 diss.
            75500*ones(1,11), ...                                                   % NO diss.
            42938,19400, ...                                                        % Exchange
            31900,80600,67500, ...                                                  % Associative ioniz.
            77200,12200,28600,26600,40700,18000,12800,32600,48700,22800,35500, ...  % Charge exchange
            158500,168600];                                                         % Electron-impact ioniz.
qf2T_r   = [ 0.5 * ones(1,11),...   % N2 diss.
             0.5 * ones(1,11),...   % O2 diss.
             0.5 * ones(1,11),...   % NO diss.
             0, 0,...               % Exchange
             zeros(1,3),...         % Associative ioniz.
             zeros(1,11),...        % Charge exchange
             1, 1];                 % Electron-impact ioniz.
qb2T_r   = [ zeros(1,11),...        % N2 diss.
             zeros(1,11),...        % O2 diss.
             zeros(1,11),...        % NO diss.
             0, 0,...               % Exchange
             ones(1,3),...          % Associative ioniz.
             zeros(1,11),...        % Charge exchange
             1, 1];                 % Electron-impact ioniz.

% passing to SI units
A_r = A_r.' .* (1e-6).^(sum(nu_reac,2)-1); % [(cm^3/mol)^(sum(nu_reac)-1) * ...] to [(m^3/mol)^(sum(nu_reac)-1) * ...]
% building structure for the equilibrium constant
Keq_struc.nu_reac = nu_reac.';
Keq_struc.nu_prod = nu_prod.';
idx_bath        = 3; % N2
idx_bathElement = 2; % N

% Surface catalytic recombinationo modeling
catReac_list = {'2N <-> N2',...                                     % catalytic reactions. They are actually one-directional
                '2O <-> O2'};

[nuCat_reac,nuCat_prod] = generateStoichiometricMatrices(catReac_list,spec_list); % getting stoichiometric coefficients for the gas-surface reactions

% building structure for the gas-surface interaction reactions
gasSurfReac_struct.nuCat_reac   = nuCat_reac.';
gasSurfReac_struct.nuCat_prod   = nuCat_prod.';
gasSurfReac_struct.catReac_list = catReac_list;


% Transposing everything, to have it in the desired form
nu_reac = nu_reac.';        nu_prod = nu_prod.';
nuEq_reac = nuEq_reac.';    nuEq_prod = nuEq_prod.';
nT_r = nT_r(:);             theta_r = theta_r(:);
A_r = A_r(:);
delta_nuEq = nuEq_prod - nuEq_reac;

end % getMixture_constants_air11Park93Bose