function [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
    Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = getMixture_constants_mars5Park94()
% getMixture_constants_mars5Park94 returns the mixture constants for
% the mars5Park94 mixture.
%
% Usage:
%   (1)
%       [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
%     Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = ...
%                                       getMixture_constants_mars5Park94()
%
% Outputs: the same as <a href="matlab:help getMixture_constants">getMixture_constants</a>
%
% Species: (O,C,CO,O2,CO2)
%
% References:
%       Park, C., Howe, J. T., Jaffe, R. L., & Candler, G. V. (1994).
%   Review of chemical-kinetic problems of future NASA missions. II - Mars
%   entries. Journal of Thermophysics and Heat Transfer, 8(1), 9–23.
%   https://doi.org/10.2514/3.496
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% Park's mars-5 model works with 5 species for martian atmosphere (O,C,CO,O2,CO2)
spec_list = {'O','C','CO','O2','CO2'};
N_spec = 5;

% Stoichiometric coefficients
reac_list = {'CO2 + M <-> CO + O + M',...
             'CO + M <-> C + O + M',...
             'O2 + M <-> 2O + M',...
             'CO + O <-> O2 + C',...
             'CO2 + O <-> O2 + CO',...
             'CO + CO <-> CO2 + C',...
    };
reac_list_eq = {'CO2 <-> C + 2O',...
                'CO <-> C + O',...
                'O2 <-> 2O',...
    };
[nu_reac,nu_prod]       = generateStoichiometricMatrices(reac_list,spec_list);
[nuEq_reac,nuEq_prod]   = generateStoichiometricMatrices(reac_list_eq,spec_list);

 % Arrhenius curve fittings for the reactions
 %
 A_r = [1e21 * [14,    14,    6.9,    6.9,    6.9], ...         % CO2 diss.
        1e20 * [3.4,  3.4,    2.3,    2.3,    2.3], ...         % CO diss.
        2e21 * [5,      5,      1,      1,      1], ...         % O2 diss.
        3.9e13,    2.1e13,     2.33e9];                         % Exchange
                                                                % [(cm^3/mol)^(sum(nu_reac)-1) * K^(1/nT_r)]
 nT_r = [-1.5*ones(1,5), ...    % CO2 diss.
         -1.0*ones(1,5), ...    % CO diss.
         -1.5*ones(1,5), ...    % O2 diss.
         -0.18, 0,  0.5];       % exchange
 theta_r = [6.3275e4*ones(1,5), ...                 % CO2 diss.
             1.29e5*ones(1,5), ...                  % CO diss.
             5.975e4*ones(1,5), ...                 % O2 diss.
             6.92e4,    2.78e4, 6.571e4];           % Exchange
qf2T_r   = [ 0.5 * ones(1,5),...    % CO2 diss.
             0.5 * ones(1,5),...    % CO diss.
             0.5 * ones(1,5),...    % O2 diss.
             0, 0, 0];              % Exchange
qb2T_r   = qf2T_r; % same temperature for forward and backward reactions

% passing to SI units
A_r = A_r.' .* (1e-6).^(sum(nu_reac,2)-1); % [(cm^3/mol)^(sum(nu_reac)-1) * ...] to [(m^3/mol)^(sum(nu_reac)-1) * ...]
% building structure for the equilibrium constant
Keq_struc.nu_reac = nu_reac';
Keq_struc.nu_prod = nu_prod';
idx_bath        = 5; % CO2
idx_bathElement = 3; % O

% Surface catalytic recombinationo modeling
catReac_list = {'CO + O <-> CO2',...
                'C + O <-> CO',...
                '2O <-> O2'}; % catalytic reactions. They are actually one-directional

[nuCat_reac,nuCat_prod] = generateStoichiometricMatrices(catReac_list,spec_list); % getting stoichiometric coefficients for the gas-surface reactions

% building structure for the gas-surface interaction reactions
gasSurfReac_struct.nuCat_reac   = nuCat_reac.';
gasSurfReac_struct.nuCat_prod   = nuCat_prod.';
gasSurfReac_struct.catReac_list = catReac_list;


% Transposing everything, to have it in the desired form
nu_reac = nu_reac.';        nu_prod = nu_prod.';
nuEq_reac = nuEq_reac.';    nuEq_prod = nuEq_prod.';
nT_r = nT_r(:);             theta_r = theta_r(:);
A_r = A_r(:);
delta_nuEq = nuEq_prod - nuEq_reac;

end % getMixture_constants_mars5Park94