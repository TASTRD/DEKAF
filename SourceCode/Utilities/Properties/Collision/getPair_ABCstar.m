function lnABCstar_sl = getPair_ABCstar(Tstar,consts_ABCstar)
%GETPAIR_ABCSTAR Calculate the values of ln(Collision Integral Ratio) and
% its derivatives with respect to the interaction pair temperature, Tsl
%
% Usage:
%   lnABCstar_sl = getPair_ABCstar(Tstar,consts_ABCstar);
%
% Inputs and Outputs:
%   Tstar           reduced temperature
%                   N_eta x N_s x N_l
%   consts_ABCstar  struct whose fields corresponds to the curve fit data
%                   of the collision integral ratio (denoted locally as
%                   ABCstar). each field is N_eta x N_s x N_l
%
% Related functions:
% See also eval_Fit_PolyLogLog
%
% Notes:
% The conversion is found from the definition of the OmegaStar and Omega,
% written clearly in Fertig 2001, equation 7.
%
% References:
%   Fertig 2001, Transport Coefficients for High Temperature Nonequilibrium
%       Air Flows
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

% Evaluate the curve fit data for the Tstar derivatives
lnABCstar_sl = eval_Fit(Tstar,consts_ABCstar);

end % getPair_ABCstar.m

