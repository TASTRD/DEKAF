function [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = ...
                adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder, ...
                consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l)
% adaptCapitelliIonNonNeutral2PolyLogLog passes from Capitelli's expression
% for ion-non-parent-neutral collisions to the poly-log-log used in DEKAF.
%
% Usage:
%   (1)
%       [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = ...
%               adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
%               consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l)
%
% Inputs and outputs:
%   alpha
%       polarizability of the neutral species in the collision
%   Trange
%       two-position vector with the minimum and maximum temperature to
%       consider.
%   nOrder
%       order of the poly-log-log fit
%   consts_Omega11, consts_Omega22, consts_Bstar, consts_Cstar 
%       structures with fields A, B, etc., with matrices for which position
%       (s,l) contains the coefficients for the polynomial log-log fit of
%       collision beetween particles s and l. See below for their
%       mathematical significance.
%       Position s,l is populated with the curve fit constants of the
%       collisional data provided.
%   s,l
%       identifier of the collision pair to be populated.
%   stats_fits
%       structure with the statistics of the fittings. Two fields are
%       updated:
%           .errRes
%               residual error of the fit
%           .Rsq
%               R-squared value (coefficient of determination)
%
% References:
%   [1] Capitelli, M., Gorse, C., Longo, S., & Giordano, D. (1998).
%   Transport properties of high temperature air species. AIAA-Paper,
%   98–2936. https://doi.org/10.2514/6.1998-2936
%
% Author: Fernando Miro Miro
% Date: July 2019
% GNU Lesser General Public License 3.0

AngSq2mSq = 1e-20;                                                          % from angstroms squared to meters squared
NT  = 1001;         % HARDCODE ALERT!!
T   = linspace(Trange(1),Trange(end),NT);                                   % temperature vector
O_11   = 425.4*sqrt(alpha./T) * AngSq2mSq;                                  % collision data O_11       (Eq. 13)
O_12   = 0.8333*O_11;                                                       % collision data for O_12   (Eq. 14)
O_13   = 0.7292*O_11;                                                       % collision data for O_13   (Eq. 15)
O_22   = 0.8710*O_11;                                                       % collision data for O_22   (Eq. 16)
Bstar  = (5*O_12 - 4*O_13)./O_11;                                           % computing Bstar
Cstar  = O_12./O_11;                                                        % computing Cstar

[p_O11,  stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = generateFit_PolyLogLog(T,O_11, nOrder); % obtaining fit data
[p_O22,  stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = generateFit_PolyLogLog(T,O_22, nOrder);
[p_Bstar,stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = generateFit_PolyLogLog(T,Bstar,nOrder);
[p_Cstar,stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = generateFit_PolyLogLog(T,Cstar,nOrder);

ABCD = alphabet('capital');                                                 % alphabetical list of letters
for ii=1:nOrder+1                                                           % looping polynomial order
    letter = ABCD{nOrder+2-ii};                                                 % obtaining the letter corresponding to it
    consts_Omega11.(letter)(s,l) =  p_O11(ii);                                  % populating constants structure
    consts_Omega22.(letter)(s,l) =  p_O22(ii);
    consts_Bstar.(letter)(s,l)   =  p_Bstar(ii);
    consts_Cstar.(letter)(s,l)   =  p_Cstar(ii);
end

%%%% DEBUG PLOT
% N_spec = size(consts_Omega11.A,1);
% T_mat = repmat(T',[1,N_spec,N_spec]);
% lnProp_sl = eval_Fit_PolyLogLog(T_mat,consts_Omega11);
% O11_fit = exp(lnProp_sl(:,s,l));
% lnProp_sl = eval_Fit_PolyLogLog(T_mat,consts_Omega22);
% O22_fit = exp(lnProp_sl(:,s,l));
% lnProp_sl = eval_Fit_PolyLogLog(T_mat,consts_Bstar);
% Bstar_fit = exp(lnProp_sl(:,s,l));
% lnProp_sl = eval_Fit_PolyLogLog(T_mat,consts_Cstar);
% Cstar_fit = exp(lnProp_sl(:,s,l));
% figure; Ni=2; Nj=2; ip=0;
% ip=ip+1; subplot(Ni,Nj,ip);
% loglog(T,O_11,    '-', 'displayname','Capitelli'); hold on;
% loglog(T,O11_fit,'--','displayname','PolyLogLog'); hold on;
% xlabel('T [K]'); ylabel('\Omega^{(1,1)} [m^2]');
% ip=ip+1; subplot(Ni,Nj,ip);
% loglog(T,O_22,    '-', 'displayname','Capitelli'); hold on;
% loglog(T,O22_fit,'--','displayname','PolyLogLog'); hold on;
% xlabel('T [K]'); ylabel('\Omega^{(2,2)} [m^2]');
% ip=ip+1; subplot(Ni,Nj,ip);
% loglog(T,Bstar,    '-', 'displayname','Capitelli'); hold on;
% loglog(T,Bstar_fit,'--','displayname','PolyLogLog'); hold on;
% xlabel('T [K]'); ylabel('B^* [-]');
% ip=ip+1; subplot(Ni,Nj,ip);
% loglog(T,Cstar,    '-', 'displayname','Capitelli'); hold on;
% loglog(T,Cstar_fit,'--','displayname','PolyLogLog'); hold on;
% xlabel('T [K]'); ylabel('C^* [-]');
% evalin('caller','suptitle([''Collision '',spec_list{s},''-'',spec_list{l}]);');
%%%% END

end % adaptCapitelliIonNonNeutral2PolyLogLog