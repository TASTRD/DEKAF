function lambda_Debye = getMixture_lambdaDebye(y_s,TTrans,Tel,rho,Mm_s,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl)
%GETMIXTURE_LAMBDADEBYE Calculate the Debye length of a gaseous mixture
%
% Usage:
% lambda_Debye = getMixture_lambdaDebye(y_s,TTrans,Tel,rho,Mm_s,nAvogadro,...
%                                         bPos,bElectron,epsilon0,kBoltz,qEl)
%
% Inputs and Outputs
%   y_s                 [-]
%       species mass fraction (size N_eta x N_spec)
%   TTrans              [K]
%       translational temperature of heavy species (size N_eta x 1)
%   Tel                 [K]
%       translational temperature of electron (size N_eta x 1)
%   rho                 [kg/m^3]
%       mixture density (size N_eta x 1)
%   Mm_s                [kg/mol]
%       species molar mass (size N_spec x 1)
%   nAvogadro           [-]
%       Avogadro's number (size 1 x 1)
%   bPos                [-]
%       positively-charged-species boolean (size N_spec x 1)
%   bElectron           [-]
%       electron boolen (size N_spec x 1)
%   epsilon0            [C^2/N-m^2 ]
%       permittivity of vacuum (size 1 x 1)
%   kBoltz              [m^2-kg/K-s^2]
%       Boltzmann constant (size 1 x 1)
%   qEl                 [C]
%       elementary charge (size 1 x 1)
%
%    lambda_Debye       [m]
%       Debye length (size N_eta x 1)
%
% Note that this functions definition holds for general mixtures under
%   thermal nonequilibrium
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);

ZCharge_s = getSpecies_ZCharge(bPos,bElectron);

n_s = getSpecies_n(rho,y_s,Mm_s,nAvogadro);                 % obtaining species number densities
Zsq = ZCharge_s.^2;

Zsq_2D = repmat(Zsq',[N_eta,1]);                            % repmatting
T_s2D = repmat(TTrans,[1,N_spec]);                          % populating species temperature for heavy particles
T_s2D(:,bElectron) = Tel;                                   % populating species temperature for electrons
% computing debye length
nT_charg = (Zsq_2D.*n_s./T_s2D)*ones(N_spec,1);             % weighted number density of charged particles divided by T (accounting for charges)
lambda_Debye = sqrt(epsilon0*kBoltz./(qEl^2.*nT_charg));

end % getMixture_lambdaDebye
