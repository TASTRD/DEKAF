function [lnOmegaij_sl,Tstar] = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omegaij,bPos)
% getPair_lnOmegaij computes the natural logarithm of a collision integral
%
%   Examples:
%       (1)     [lnOmegaij_sl,Tstar] = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,...
%                           bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omegaij,bPos)
%
%   Inputs and Outputs
%       TTrans          size N_eta x 1
%       Tel             size N_eta x 1
%       y_s             size N_eta x N_spec
%       rho             size N_eta x 1
%       p               size N_eta x 1
%       Mm_s            size N_spec x 1
%       bElectron       size N_spec x 1
%       kBoltz          size 1 x 1
%       R0              size 1 x 1
%       nAvogadro       size 1 x 1
%       qEl             size 1 x 1
%       epsilon0        size 1 x 1
%       Tstas           size N_eta x N_spec x N_spec
%       consts_*        struct with fields .A, .B, .C, .D, .E for the
%                       collision integral curve fit
%       bPos            size N_spec x 1
%       options         necessary fields
%                           .flow
%
% References:
% - Scoggins 2014, Development of Mutation++: MUlticomponent Thermodynamics
% And Transport properties for IONized gases library in C++
% - Scoggins Thesis, Development of numerical methods and study of coupled
% flow, radiation, and ablation phenomena for atmospheric entry
% - Mason 1967, Transport Coefficients of Ionized Gases
% - Fertig 2001, Transport Coefficients for High Temperature
% Nonequilibrium Air Flows
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% sizes
[N_eta,N_spec] = size(y_s);

% Get reduced temperature
if nnz(bElectron)
    [Tstar,lambda_Debye] = getPair_Tstar(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,bPos); % reduced temperature for the charged-charged collisions
else
    Tstar = repmat(TTrans,[1,N_spec,N_spec]);
end

lnOmegaStarij_sl = eval_Fit(Tstar,consts_Omegaij);

% accounting for the fact that the curve fits for charged-charged
% collisions must be divided by the reduced temperature squared
if nnz(bElectron) % for charged-charged collisions we must correct for the fitting being done on Tstar-OmegaStar
    bCharged = getPair_bCharged(bPos,bElectron);
    bCharged_3D = repmat(reshape(bCharged,[1,N_spec,N_spec]),[N_eta,1,1]);
    lambda_Debye3D = repmat(lambda_Debye,[1,N_spec,N_spec]);                % (eta,1) --> (eta,s,l)
    lnLambdaT = 2*log(lambda_Debye3D./Tstar);
    lnLambdaT = lnLambdaT.*bCharged_3D;                                     % we only want to multiply the charged collisions
    % Convert from OmegaStar to Omega (see Fertig 2001, equation 7)
    % Note that lambdaD / Tstar = qEl^2/(kBoltz*4*pi*epsilon0*T_sl)
    % Only add these chain rule terms for the charge-charge interactions
    lnOmegaij_sl    =  lnLambdaT + lnOmegaStarij_sl;

else % otherwise the fittings are directly on T
    lnOmegaij_sl    = lnOmegaStarij_sl;
end
