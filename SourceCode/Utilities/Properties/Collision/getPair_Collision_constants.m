function [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,...
    consts_Estar,consts_Omega14,consts_Omega15,consts_Omega24,...
    consts_Dbar_binary,stats_fits,pairs] ...
    = getPair_Collision_constants(spec_list,opts)
%GETPAIR_COLLISION_CONSTANTS Get constants for curve fits corresponding to
% collision cross-sectional areas from a given reference
%
% Usage:
%
% 1. Using only two primary references only for collisional data, one for
% the neutral-neutral & neutral-electron interactions, one for the charge-
% charge interactions.
%
% [consts_Omega11,consts_Omega22,consts_Bstar,consts_Dbar_binary,stats_fits,pairs,bCharged] ...
%     = getPair_Collision_constants(spec_lis,primeRefNeutral,primeRefCharge,...
%           exceptions,opts);
%
% Inputs:
%
%   spec_list           -   cell array of strings whose entries are the
%                           species in the gaseous mixture
%   opts                -   structure with several options
%
%       .modelCollisionNeutral          string representing the primary
%                                       reference for the data of
%                                       neutral-neutral + electron-neutral
%                                       interactions.
%                                       Valid inputs:
%           'Wright2005'
%               Refs. [2,3] (not all carbon species)
%           'Wright2005Bellemans2015'
%               Refs. [2,3,7] (includes C3 and C2)
%           'Stuckert91'
%               Ref. [6]
%           'GuptaYos90'
%               Ref. [1]
%           'mars5dplr'
%               DPLR curve fits onto Ref. [8]
%           'Yos63'
%               Ref [9]
%           'Capitelli98'
%               Ref [10]
%           'StallCop96'
%               Ref [11]
%           'Capitelli98Stallcop96'
%               Ref [11] for atom-atom collisions, and Ref [10] for all the
%               others
%
%       .modelCollisionChargeCharge     string representing the primary
%                                       reference for the data of
%                                       charge-charge interactions
%           'Mason67Devoto73' - Refs. [4,5]
%       .collisionRefExceptions         struct whose fields are the
%                                       following:
%
%                       .references{r}  cell array of strings whose entries
%                                       are the exceptions to the primary
%                                       reference. Possibilities are
%                                       identical to that of
%                                       primeRefNeutral & primeRefCharge
%                       .pairs{r}{s}    cell of cells where the rth cell
%                                       corresponds to the rth reference.
%                                       the rth cell is a cell array of
%                                       strings whose entries correspond to
%                                       the collision pairs whose
%                                       references will be defined by the
%                                       rth reference, as opposed to the
%                                       primary references.
%       .bCollisionExtrap   boolean. if true, add artificial data points
%                           to improve numerical behavior of polynomial
%                           curve fits. if false, use original data
%                           unaltered.
%       .bCollisionRawData  boolean. if true, output the raw collision data
%                           to the stats_fits struct as .raw_data. if
%                           false, then do not create this .raw_data struct
%                           onto stats_fits
%
% Outputs:
%
%   consts_Omega11      -   Constants corresponding the curve fit for
%                           Omega11, order (1,1); struct
%   consts_Omega22      -   Constants corresponding the curve fit for
%                           Omega22, order (2,2); struct
%   consts_Bstar        -   Constants corresponding the curve fit for
%                           Bstar_sl; struct
%   consts_Cstar        -   Constants corresponding the curve fit for
%                           Cstar_sl; struct
%   consts_Estar        -   Constants corresponding the curve fit for
%                           Cstar_sl; struct
%   consts_Omega14      -   Constants corresponding the curve fit for
%                           Omega14, order (1,4); struct
%   consts_Omega15      -   Constants corresponding the curve fit for
%                           Omega15, order (1,5); struct
%   consts_Omega24      -   Constants corresponding the curve fit for
%                           Omega24, order (2,4); struct
%   consts_Dbar_binary  -   Constants corresponding the curve fit for
%                           Dbar_binary_sl; struct
%                           These coefficients will be only be used if the
%                           modelDiffusion option is set to 'GuptaYos90'
%   stats_fits          -   Statistics on the curve fits produced by DEKAF
%                           (if available/applicable). The fields are
%                       .acc    - accuracy of the data (if available)
%                       .Rsq    - R-squared value of the curve fit (if
%                                 applicable)
%                       .errRes - norm of the residual error of the curve
%                                 fit (if applicable)
%                       .raw_data original data for collision integrals,
%                                 see the option .bCollisionRawData
%                           Values are left to all zeros when unpopulated
%   pairs               -   cell array of strings whose entries are the
%                           total possibilities for two-body interactions
%                           for the given spec_list
%
% Notes:
%
% The outputs which are structs (namely, consts_Omega11,
%   consts_Omega22, and consts_Bstar) have their curve fit values
%   stored in fields A, B, C, D, etc... where each field is a matrix of
%   doubles size N_spec x N_spec (where N_spec is length(spec_list))
% Symmetry is assumed. That is, (s,l) = (l,s). For example, the organization
%   of each matrix would resemble the following for an air5 mixture
%
%         'N'  'O'  'NO' 'N2' 'O2'
%          __   __   __   __   __
%     'N' |a11 a21  a31  a41  a51
%     'O' |a21 a22  a32  a42  a52
%     'NO'|a31 a32  a33  a43  a53
%     'N2'|a41 a42  a43  a44  a54
%     'O2'|a51 a52  a53  a54  a55
%
% Assume the gas is low enough density that the effect of three-body
%   collisions can be neglected.
%
% Examples:
%
% 1. Gupta & Yos 90's air5 curve stats_fits for collision integrals,
%       B* ratio, and binary diffusion coefficients.
%
%   spec_list = {'N','O','NO','N2','O2'};
%   opts.ref = 'GuptaYos90';
%   [OM_11,OM_22,Bstar,Dbar,~,pairs,~] = ...
%       getPair_Collision_constants(spec_list,opts);
%
% 2. This example establishes exceptions given through varargin input
%       for the reference 'Yos63' for the interactions 'O - O' and 'N - N'.
%
%   spec_list = {'N','O','NO','N2','O2'};
%   opts.ref = 'GuptaYos90';
%   opts.exceptions.references = {'Yos63'};
%   opts.exceptions.pairs = {{'O - O','N - N'}};
%   [OM_11,OM_22,Bstar,Dbar,~,pairs,~] = ...
%       getPair_Collision_constants(spec_list,opts)
%
% 3. Wright2005 data for neutrals and Mason67Devoto73 data for charge-charge
%       interactions governed by a screened Coulomb potential
%
%   spec_list = {'N','O','NO','N2','O2','N+','O+','NO+','N2+','O2+','e-'};
%   opts.refNeut = {'Wright2005','Wright2005Bellemans2015'};
%   opts.refChrg = 'Mason67Devoto73';
%   [OM_11,OM_22,Bstar,~,stats_fits,pairs,bCharged] = ...
%       getPair_Collision_constants(spec_list,opts);
%
%
% References:
% [1] Gupta,  R. N.,  Yos,  J. M.,  Thompson,  R. A.,  and Lee,  K.-P.,  "A
%   Review of Reaction Rates and Thermodynamic and Transport Properties for
%   an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%   Calculations to 30000 K," Nasa-rp-1232, National Aeronautics and Space
%   Administration, 1990.
%
% [2] Michael J. Wright, Deepak Bose, Grant E. Palmer, and Eugene Levin.
%   "Recommended Collision Integrals for Transport Property Computations
%    Part 1: Air Species", AIAA Journal, Vol. 43, No. 12 (2005), pp.
%    2558-2564. https://doi.org/10.2514/1.16713
%
% [3] Wright, M. J., Hwang, H. H., & Schwenke, D. W. (2007). Recommended
%   Collision Integrals for Transport Property Computations Part II: Mars
%   and Venus Entries. AIAA Journal, 45(1), 281–288.
%   https://doi.org/10.2514/1.24523
%
% [4] Mason, E. A. (1967). Transport Coefficients of Ionized Gases. Physics of
%   Fluids, 10(8), 1827. https://doi.org/10.1063/1.1762365
%
% [5] Devoto, R. (1973). Transport coefficients of ionized argon. Physics of
%   Fluids, 16(5)
%
% [6] G. Stuckert 1991, PhD. Thesis. Linear Stability Theory of Hypersonic
%   Chemically Reacting Viscous Flows. NOTE: this reference proposes
%   curve-fits for the neutral collisions based on many different authors.
%
% [7] Bellemans, A., & Magin, T. (2015). Calculation of Collision Integrals
%   for Ablation Species. In 8th European Symposium on Aerothermodynamics
%   for Space Vehicles. Lisbon, Portugal.
%
% [8] Bzowski, J., Kestin, J., Mason, E.A., and Uribe, F.J., "Equilibrium
%   and Transport Properties of Gases Mixtures at Low Density: Eleven
%   Polyatomic Gases ad Five Noble Gases," Journal of Phys. and Chem. Ref.
%   Data, Vol. 19, No. 5, pp. 1179-1231, 1990.
%
% [9] Yos 1963 Memo, Wright-Patterson AFB Customer digitized, then curve
%   fit values for O - O, N - N
%
% [10] Capitelli, M., Gorse, C., Longo, S., & Giordano, D. (1998).
% Transport properties of high temperature air species. AIAA-Paper,
% 98–2936. https://doi.org/10.2514/6.1998-2936
%
% [11] Stallcop, J. R., Partridge, H., & Levinj, E. (1996). Analytical Fits
%   for the Determination of the Transport Properties of Air. Journal of
%   Thermophysics and Heat Transfer, 10(4), 697–699.
%   https://doi.org/10.2514/3.847
%
% NASA's DPLR Code for Mars 5 data. DPLR is restricted access. For more
% information on these fits, talk to Ethan Beyak.
%
%
% Verification:
%   See Test_Collision_constants.m for full script on verification.
%   GuptaYos90  -   Figure 10a, page 84. Curve fit agrees for O2 - O2 pair
%                   for pi*Omega11_sl, pi*Omega22_sl, and Bstar_sl.
%
% Related functions:
%   See also eval_Fit_PolyLogLog, eval_Fit_PolyLog, setDefaults
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% Extract inputs
primeRefNeutral     = opts.modelCollisionNeutral;
primeRefCharge      = opts.modelCollisionChargeCharge;
exceptions          = opts.collisionRefExceptions;
bCollisionExtrap    = opts.bCollisionExtrap;

% Generate cell array of strings of all possible two-body interactions
% Generate a reference per pair
N_spec = length(spec_list);
pairsReferences = cell(N_spec,N_spec);
space = repmat({32}, [N_spec,N_spec]); % 32: ascii character code for space
spec_list_rows  = repmat(spec_list,[N_spec,1]);
spec_list_cols  = repmat(spec_list',[1,N_spec]);
pairs           = strcat(spec_list_rows,' -',space,spec_list_cols);

[bAtt,bRep,bCharged] = getPair_bAttRep(spec_list);

pairsReferences(bCharged)   = {primeRefCharge};
pairsReferences(~bCharged)  = {primeRefNeutral};

% Find which (s,l) location in pairs corresponds to the collision specified
% by exceptions.pairs{r}{p}. Account for mirrored string input.
% Then replace references with exceptions provided through exceptions.references
for r=1:length(exceptions.references)
    for p=1:length(exceptions.pairs{r})
        myPair = exceptions.pairs{r}{p};
        dash = strfind(myPair,'-');
        myPairFlipped = [myPair(dash+2:end),' - ',myPair(1:dash-2)];
        bFoundRefException = strcmp(pairs,myPair) | strcmp(pairs,myPairFlipped);
        pairsReferences(bFoundRefException) = exceptions.references(r);
    end
end

% Initialize arrays of structs
orderMax = 7; %there's no more than 7th-order polynomials
consts_Omega11  = allocateCollisionConsts(N_spec,orderMax);
consts_Omega22  = allocateCollisionConsts(N_spec,orderMax);
consts_Bstar    = allocateCollisionConsts(N_spec,orderMax);
consts_Cstar    = allocateCollisionConsts(N_spec,orderMax);
consts_Estar    = allocateCollisionConsts(N_spec,orderMax);
consts_Omega14  = allocateCollisionConsts(N_spec,orderMax);
consts_Omega15  = allocateCollisionConsts(N_spec,orderMax);
consts_Omega24  = allocateCollisionConsts(N_spec,orderMax);
consts_Dbar_binary = allocateCollisionConsts(N_spec,orderMax); % This struct's fields will be nonzero only when using the GuptaYos collision data

% Statistics of curve stats_fits for collisional data, if available
stats_fits.acc.Omega11 = zeros(N_spec,N_spec);     stats_fits.Rsq.Omega11 = zeros(N_spec,N_spec);     stats_fits.errRes.Omega11 = zeros(N_spec,N_spec);
stats_fits.acc.Omega22 = zeros(N_spec,N_spec);     stats_fits.Rsq.Omega22 = zeros(N_spec,N_spec);     stats_fits.errRes.Omega22 = zeros(N_spec,N_spec);
stats_fits.acc.Bstar   = zeros(N_spec,N_spec);     stats_fits.Rsq.Bstar   = zeros(N_spec,N_spec);     stats_fits.errRes.Bstar   = zeros(N_spec,N_spec);
stats_fits.acc.Cstar   = zeros(N_spec,N_spec);     stats_fits.Rsq.Cstar   = zeros(N_spec,N_spec);     stats_fits.errRes.Cstar   = zeros(N_spec,N_spec);
stats_fits.acc.Estar   = zeros(N_spec,N_spec);     stats_fits.Rsq.Estar   = zeros(N_spec,N_spec);     stats_fits.errRes.Estar   = zeros(N_spec,N_spec);
stats_fits.acc.Omega14 = zeros(N_spec,N_spec);     stats_fits.Rsq.Omega14 = zeros(N_spec,N_spec);     stats_fits.errRes.Omega14 = zeros(N_spec,N_spec);
stats_fits.acc.Omega15 = zeros(N_spec,N_spec);     stats_fits.Rsq.Omega15 = zeros(N_spec,N_spec);     stats_fits.errRes.Omega15 = zeros(N_spec,N_spec);
stats_fits.acc.Omega24 = zeros(N_spec,N_spec);     stats_fits.Rsq.Omega24 = zeros(N_spec,N_spec);     stats_fits.errRes.Omega24 = zeros(N_spec,N_spec);

if strcmp(primeRefNeutral,'Stuckert91')
    consts_Omega11.fitType = 'polyLog'; % Stuckert is the only one using poly-log curve fits
    consts_Omega22.fitType = 'polyLog';
    consts_Bstar.fitType = 'polyLog';
    consts_Cstar.fitType = 'polyLog';
    consts_Estar.fitType = 'polyLog';
    consts_Omega14.fitType = 'polyLog';
    consts_Omega15.fitType = 'polyLog';
    consts_Omega24.fitType = 'polyLog';
    if ~isempty(exceptions.references)
        error('Stuckert''s collisional curve fits do not allow exceptions with the current code architecture.');
    end
else % all other data is fitted with a poly-log-log
    consts_Omega11.fitType = 'polyLogLog';
    consts_Omega22.fitType = 'polyLogLog';
    consts_Bstar.fitType = 'polyLogLog';
    consts_Cstar.fitType = 'polyLogLog';
    consts_Estar.fitType = 'polyLogLog';
    consts_Omega14.fitType = 'polyLogLog';
    consts_Omega15.fitType = 'polyLogLog';
    consts_Omega24.fitType = 'polyLogLog';
    consts_Dbar_binary.fitType = 'polyLogLog';
end

% HARDCODE WARNING
% Define extrapolation domain query points
Tstar_extrap1 = [1e9,1e10,1e11];         % unitless
Tstar_extrap2 = [1e5,1e7,1e9,1e11];         % unitless
T_extrap_el_neut = [200, 500];      % Kelvin

% unit conversions
AngSq2mSq = 1e-20; % from angstroms squared to meters squared

% logarithmic unit conversions
lnPiAngSq2lnmSq = -log(pi) - 20*log(10);                                    % logarithmic addend converting from pi*Angstroms squared to m^2
lncmSqAtm2lnmSq = - 4*log(10) + log(101325);                                % logarithmic addend converting from cm^2*atm to m^2*Pa
for s=N_spec:-1:1
    for l=N_spec:-1:1
        ic11 = 1; ic22 = 1; icB = 1;                                                        % initializing anti-fatfingeringcounters
        clear('Bstar','Cstar','Estar','O14','O15','O24','O11','O22','T','T2','Textrap');    % clearing variables for tabulated values
        switch pairs{s,l}
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%% NEUTRAL - NEUTRAL INTERACTIONS %%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%% OXYGEN - NITROGEN INTERACTIONS %%%%%%%%%%%%%%%%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            case 'N - N'
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N - N'
                        T   =           [300   500   1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[8.07  7.03  5.96  5.15  4.39  4.14  3.94  3.61  3.37  2.92  2.62];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[9.11  7.94  6.72  5.82  4.98  4.70  4.48  4.14  3.88  3.43  3.11];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 5;
                        stats_fits.acc.Omega22(s,l)   = 5;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #6
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0033;
                        consts_Omega11.B(s,l) = -0.0572;
                        consts_Omega11.A(s,l) =  5.0452 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #6
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0118;
                        consts_Omega22.B(s,l) = -0.0960;
                        consts_Omega22.A(s,l) =  4.3252 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #6
                        consts_Bstar.C(s,l) =  0.0002;
                        consts_Bstar.B(s,l) =  0.0002;
                        consts_Bstar.A(s,l) =  0.0537;

                        % Table VII, page 57, pair #6
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0033;
                        consts_Dbar_binary.B(s,l) =  1.5572;
                        consts_Dbar_binary.A(s,l) = -11.1616 + lncmSqAtm2lnmSq;

                        % Warn the user that this collisional data is wrong
                        warning(['Collisional data. as well as the diffusion curve ',...
                            'fit for N-N and O-O collisions in GuptaYos90 is known ',...
                            'to be very wrong. The correct collisional data can be ',...
                            'obtained using Yos63, however there are no correct curve ',...
                            'fits for the diffusion coefficients. It is strongly ',...
                            'recommended to use the most updated collisional data ',...
                            'coming from Wright2005.']);

                    case 'Yos63'
                        % Units are in pi*A^2
                        % Data digitized from Figure 3, p. 9
                        % Curve fit coefficients created in DEKAF (Test_Collision_constants.m)
                        consts_Omega11.D(s,l) =  0.006841300217901;
                        consts_Omega11.C(s,l) = -0.174346812824011;
                        consts_Omega11.B(s,l) =  1.160648835708513;
                        consts_Omega11.A(s,l) =  1.029119608275975 + lnPiAngSq2lnmSq;

                        % Repeated from GuptaYos90, as it is identical to Yos63
                        % Table IX, page 65, pair #10
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0207;
                        consts_Omega22.B(s,l) =  0.0780;
                        consts_Omega22.A(s,l) =  3.5658 + lnPiAngSq2lnmSq;

                        % Using GuptaYos90's
                        % Table X, page 69, pair #10
                        consts_Bstar.C(s,l) =  0.0002;
                        consts_Bstar.B(s,l) =  0.0;
                        consts_Bstar.A(s,l) =  0.0549;

                        % Using GuptaYos90's
                        % Table VII, page 57, pair #10
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0034;
                        consts_Dbar_binary.B(s,l) =  1.5572;
                        consts_Dbar_binary.A(s,l) = -11.1729 + lncmSqAtm2lnmSq;

                        % Warn the user that this diffusion curve fit is wrong
                        warning('The diffusion curve fit coefficients for Yos63 (coming from GuptaYos90 are known to be wrong.');

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 48.8486  * AngSq2mSq;
                        consts_Omega11.B(s,l) = 12.8048 * AngSq2mSq;
                        consts_Omega11.C(s,l) = 1.29682 * AngSq2mSq;
                        consts_Omega11.D(s,l) = -4.79933e-2 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 30.7193 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -4.87036 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 2.12776e-1 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    case 'Capitelli98'
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 1.9939E+01  9.0652E+02 -8.4875E-01 2.3809E+00  3.8782E-02  4.8674E-01];
                        a12_vec = [ 9.3402E+00  1.0845E+03 -1.2761E+00 7.9736E-01  3.5324E-02  4.4711E-01];
                        a13_vec = [ 5.5929E+00  2.1522E+03 -1.6966E+00 4.4782E-01  2.4128E-02  4.4567E-01];
                        a22_vec = [ 2.3736E+01  9.1364E+02 -7.7981E-01 2.7874E+00  4.9002E-02  4.6224E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);
                        
                    case {'Stallcop96','Capitelli98Stallcop96'}
                        % Coefficients from Stallcop's table 1 (n=1) for 1000<T<30000
                        consts_Omega11.A(s,l) =  5.8357 + lnPiAngSq2lnmSq;
                        consts_Omega11.B(s,l) = -0.9011;
                        consts_Omega11.C(s,l) =  0.10789;
                        consts_Omega11.D(s,l) = -0.005544;
                        
                        % Coefficients from Stallcop's table 1 (n=2) for 1000<T<30000
                        consts_Omega22.A(s,l) =  7.2135 + lnPiAngSq2lnmSq;
                        consts_Omega22.B(s,l) = -1.2781;
                        consts_Omega22.C(s,l) =  0.14276;
                        consts_Omega22.D(s,l) = -0.006464;
                        
                        % Coefficients from Stallcop's table 2 for 1000<T<30000
                        consts_Bstar.A(s,l) =  0.3210;
                        consts_Bstar.B(s,l) = -0.0595;
                        consts_Bstar.C(s,l) =  0.00488;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N - O','O - N'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N - O','O - N'
                        T   =           [300   500   1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[8.32  7.34  6.22  5.26  4.45  4.21  4.01  3.69  3.43  2.98  2.66];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[9.08  8.15  7.09  6.06  5.14  4.88  4.67  4.34  4.07  3.56  3.21];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 5;
                        stats_fits.acc.Omega22(s,l)   = 5;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #9
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) =  0.0048;
                        consts_Omega11.B(s,l) = -0.4195;
                        consts_Omega11.A(s,l) =  5.7774 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #9
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) =  0.0065;
                        consts_Omega22.B(s,l) = -0.4467;
                        consts_Omega22.A(s,l) =  6.0426 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #9
                        consts_Bstar.C(s,l) =  0.0147;
                        consts_Bstar.B(s,l) = -0.2628;
                        consts_Bstar.A(s,l) =  1.2943;

                        % Table VII, page 57, pair #9
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) = -0.0048;
                        consts_Dbar_binary.B(s,l) =  1.9195;
                        consts_Dbar_binary.A(s,l) = -11.9261 + lncmSqAtm2lnmSq;

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 92.0598  * AngSq2mSq;
                        consts_Omega11.B(s,l) = -26.2928 * AngSq2mSq;
                        consts_Omega11.C(s,l) = 2.68509 * AngSq2mSq;
                        consts_Omega11.D(s,l) = -9.52538e-2 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 50.9065 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -9.30328 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 4.55652e-1 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    case 'Capitelli98'
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 2.2683E+01  1.2470E+03 -1.0271E+00 1.9320E+00  6.4407E-02  5.0887E-01];
                        a12_vec = [ 1.0660E+01  2.3223E+03 -1.4924E+00 9.1182E-01  2.4379E-02  5.4850E-01];
                        a13_vec = [ 5.8682E+01  5.0777E+04 -1.9247E+00 5.3712E+00  1.0570E-01  5.8600E-01];
                        a22_vec = [ 2.1163E+01  8.6509E+02 -8.1169E-01 2.6270E+00  2.2254E-02  5.6508E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);
                        
                    case {'Stallcop96','Capitelli98Stallcop96'}
                        % Coefficients from Stallcop's table 1 (n=1) for 1000<T<30000
                        consts_Omega11.A(s,l) =  9.3209 + lnPiAngSq2lnmSq;
                        consts_Omega11.B(s,l) = -2.0865;
                        consts_Omega11.C(s,l) =  0.24250;
                        consts_Omega11.D(s,l) = -0.010620;
                        
                        % Coefficients from Stallcop's table 1 (n=2) for 1000<T<30000
                        consts_Omega22.A(s,l) = 10.1745 + lnPiAngSq2lnmSq;
                        consts_Omega22.B(s,l) = -2.3145;
                        consts_Omega22.C(s,l) =  0.26472;
                        consts_Omega22.D(s,l) = -0.011223;
                        
                        % Coefficients from Stallcop's table 2 for 1000<T<30000
                        consts_Bstar.A(s,l) =  0.9845;
                        consts_Bstar.B(s,l) = -0.2089;
                        consts_Bstar.C(s,l) =  0.01326;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case 'O - O'
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O - O'
                        T   =           [300   500  1000  2000  4000  5000  6000  8000 10000 15000 20000];    % K
                        O11 = AngSq2mSq*[8.53  7.28  5.89  4.84  4.00  3.76  3.57  3.27  3.05  2.65  2.39];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[9.46  8.22  6.76  5.58  4.67  4.41  4.20  3.88  3.64  3.21  2.91];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 5;
                        stats_fits.acc.Omega22(s,l)   = 5;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #10
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0034;
                        consts_Omega11.B(s,l) = -0.0572;
                        consts_Omega11.A(s,l) =  4.9901 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #10
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0207;
                        consts_Omega22.B(s,l) =  0.0780;
                        consts_Omega22.A(s,l) =  3.5658 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #10
                        consts_Bstar.C(s,l) =  0.0002;
                        consts_Bstar.B(s,l) =  0.0;
                        consts_Bstar.A(s,l) =  0.0549;

                        % Table VII, page 57, pair #10
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0034;
                        consts_Dbar_binary.B(s,l) =  1.5572;
                        consts_Dbar_binary.A(s,l) = -11.1729 + lncmSqAtm2lnmSq;

                        % Warn the user that this collisional data is wrong
                        warning(['Collisional data, as well as the diffusion curve ',...
                            'fit for N-N and O-O collisions in GuptaYos90 is known ',...
                            'to be very wrong. The correct collisional data can be ',...
                            'obtained using Yos63, however there are no correct curve ',...
                            'fits for the diffusion coefficients. It is strongly ',...
                            'recommended to use the most updated collisional data ',...
                            'coming from Wright2005.']);
                    case 'Yos63'
                        % Units are in pi*A^2
                        % Data digitized from Figure 3, p. 9
                        % Curve fit coefficients created in DEKAF (Test_Collision_constants.m)
                        consts_Omega11.D(s,l) =  0.002674333604028;
                        consts_Omega11.C(s,l) = -0.091226087100709;
                        consts_Omega11.B(s,l) =  0.682609658804143;
                        consts_Omega11.A(s,l) =  1.712950529889002 + lnPiAngSq2lnmSq;

                        % Repeated from GuptaYos90, as it is identical to Yos63
                        % Table IX, page 65, pair #10
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0207;
                        consts_Omega22.B(s,l) =  0.0780;
                        consts_Omega22.A(s,l) =  3.5658 + lnPiAngSq2lnmSq;

                        % Using GuptaYos90's
                        % Table X, page 69, pair #10
                        consts_Bstar.C(s,l) =  0.0002;
                        consts_Bstar.B(s,l) =  0.0;
                        consts_Bstar.A(s,l) =  0.0549;

                        % Using GuptaYos90's
                        % Table VII, page 57, pair #10
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0034;
                        consts_Dbar_binary.B(s,l) =  1.5572;
                        consts_Dbar_binary.A(s,l) = -11.1729 + lncmSqAtm2lnmSq;

                        % Warn the user that this diffusion curve fit is wrong
                        warning('The diffusion curve fit coefficients for Yos63 (coming from GuptaYos90 are known to be wrong.');

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 8.14007 * AngSq2mSq;
                        consts_Omega11.B(s,l) = 0.794136 * AngSq2mSq;
                        consts_Omega11.C(s,l) = -2.51533e-1 * AngSq2mSq;
                        consts_Omega11.D(s,l) = 1.14876e-2 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 17.8332 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -2.05843 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 5.65914e-2 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    case 'mars5dplr'
                        % Original source: Levin, E., Partridge, H., and Stallcop, J.R., "Collision Integrals and High
                        % Temperature Transport Properties for N-N, O-O, and N-O," Journal of
                        % Thermophysics, Vol. 4, No. 4, Oct. 1990, pp. 469-477.
                        %
                        % These are tabulated in above ref. between 250-100000 K. Fits are computed using
                        % data between 300-20000 K, non-inclusive.
                        %
                        % Curve fit data for 'O - O' (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-6.4040535E-03  1.4629949E-01 -1.3892121E+00  2.0903441E+03];
                        p_O22 = [-4.2451096E-03  9.6820337E-02 -9.9770795E-01  8.3320644E+02];
                        p_Bstar = [1.8546102E-04  3.3127237E-03 -7.7302959E-02  1.5994284E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);

                    case 'Capitelli98'
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 2.2077E+01  1.1401E+03 -8.9608E-01 2.8716E+00  4.9476E-02  4.8371E-01];
                        a12_vec = [ 1.3163E+01  1.4855E+03 -1.2581E+00 1.3503E+00  4.9169E-02  4.5654E-01];
                        a13_vec = [ 8.1273E+00  2.5461E+03 -1.6310E+00 7.8355E-01  3.4738E-02  4.5586E-01];
                        a22_vec = [ 2.3400E+01  1.0581E+03 -8.0168E-01 3.4801E+00  1.9999E-02  5.4191E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);
                        
                    case {'Stallcop96','Capitelli98Stallcop96'}
                        % Coefficients from Stallcop's table 1 (n=1) for 1000<T<30000
                        consts_Omega11.A(s,l) =  9.4075 + lnPiAngSq2lnmSq;
                        consts_Omega11.B(s,l) = -2.0005;
                        consts_Omega11.C(s,l) =  0.21628;
                        consts_Omega11.D(s,l) = -0.009051;
                        
                        % Coefficients from Stallcop's table 1 (n=2) for 1000<T<30000
                        consts_Omega22.A(s,l) = 10.1214 + lnPiAngSq2lnmSq;
                        consts_Omega22.B(s,l) = -2.1995;
                        consts_Omega22.C(s,l) =  0.23745;
                        consts_Omega22.D(s,l) = -0.009691;
                        
                        % Coefficients from Stallcop's table 2 for 1000<T<30000
                        consts_Bstar.A(s,l) =  1.1642;
                        consts_Bstar.B(s,l) = -0.2390;
                        consts_Bstar.C(s,l) =  0.01430;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'NO - N','N - NO'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'NO - N','N - NO'
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000];    % K
                        O11 = AngSq2mSq*[8.21  7.86  6.99  5.90  4.91  4.61  4.37  4.01  3.73  3.27];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[9.65  9.26  8.29  7.07  5.94  5.60  5.33  4.91  4.60  4.06];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #13
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0185;
                        consts_Omega11.B(s,l) =  0.0118;
                        consts_Omega11.A(s,l) =  4.0590 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #13
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0196;
                        consts_Omega22.B(s,l) =  0.0478;
                        consts_Omega22.A(s,l) =  4.0321 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #13
                        consts_Bstar.C(s,l) =  0.0038;
                        consts_Bstar.B(s,l) = -0.0425;
                        consts_Bstar.A(s,l) =  0.2574;

                        % Table VII, page 57, pair #13
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0185;
                        consts_Dbar_binary.B(s,l) =  1.4882;
                        consts_Dbar_binary.A(s,l) = -10.3301 + lncmSqAtm2lnmSq;

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 23.8527  * AngSq2mSq;
                        consts_Omega11.B(s,l) = -3.30555 * AngSq2mSq;
                        consts_Omega11.C(s,l) = 1.33010e-1 * AngSq2mSq;
                        consts_Omega11.D(s,l) = -1.22410e-3 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 25.8300 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -3.24326 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 1.01807e-1 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 1.8376E+01  5.8513E+02 -1.0885E+00 2.1989E-02  3.7017E-01  2.7990E-01];
                        a12_vec = [ 1.9001E+00  6.2564E+02 -1.7235E+00 4.7035E-02  2.4421E-02  3.2671E-01];
                        a13_vec = [ 6.3924E-01  1.7922E+03 -2.3300E+00 2.4350E-02  6.2276E-03  3.5811E-01];
                        a22_vec = [ 5.2091E+00  1.2579E+02 -7.9721E-01 2.2715E-01  6.0530E-02  2.9981E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'NO - O','O - NO'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'NO - O','O - NO'
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000];    % K
                        O11 = AngSq2mSq*[7.57  7.27  6.55  5.62  4.78  4.52  4.31  4.00  3.76  3.35];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[8.79  8.47  7.66  6.64  5.69  5.40  5.17  4.82  4.55  4.08];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #14
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0179;
                        consts_Omega11.B(s,l) =  0.0152;
                        consts_Omega11.A(s,l) =  3.9996 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #14
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0203;
                        consts_Omega22.B(s,l) =  0.0730;
                        consts_Omega22.A(s,l) =  3.8818 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #14
                        consts_Bstar.C(s,l) =  0.0033;
                        consts_Bstar.B(s,l) = -0.0366;
                        consts_Bstar.A(s,l) =  0.2332;

                        % Table VII, page 57, pair #14
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0179;
                        consts_Dbar_binary.B(s,l) =  1.4848;
                        consts_Dbar_binary.A(s,l) = -10.3155 + lncmSqAtm2lnmSq;

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 20.4792 * AngSq2mSq;
                        consts_Omega11.B(s,l) =  -2.68431 * AngSq2mSq;
                        consts_Omega11.C(s,l) =  1.02676e-1 * AngSq2mSq;
                        consts_Omega11.D(s,l) =  -9.19816e-4 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 21.8949 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -2.58841 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 7.65005e-2 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 1.0948E+02  3.9522E+03 -9.2591E-01 5.6774E+00  1.0000E+00  3.4478E-01];
                        a12_vec = [ 4.9973E+01  6.8422E+03 -1.4022E+00 2.4320E+00  4.1518E-01  3.6638E-01];
                        a13_vec = [ 1.5346E+01  1.2856E+04 -1.9585E+00 8.1792E-01  1.1084E-01  3.8800E-01];
                        a22_vec = [ 3.4111E+02  1.2306E+04 -7.6446E-01 3.4503E+01  9.6152E-01  4.1455E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case 'NO - NO'
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'NO - NO'
                        T   =           [300   500   600   1000  2000  4000  5000  6000  8000 10000 15000];    % K
                        O11 = AngSq2mSq*[11.66 10.33 9.97  9.09  7.90  6.60  6.24  5.96  5.54  5.23  4.70];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[13.25 11.58 11.15 10.16 9.07  7.91  7.53  7.21  6.73  6.36  5.72];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #15
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0364;
                        consts_Omega11.B(s,l) =  0.3825;
                        consts_Omega11.A(s,l) =  2.4718 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #15
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0453;
                        consts_Omega22.B(s,l) =  0.5624;
                        consts_Omega22.A(s,l) =  1.7669 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #15
                        consts_Bstar.C(s,l) = -0.0027;
                        consts_Bstar.B(s,l) =  0.0700;
                        consts_Bstar.A(s,l) = -0.2553;

                        % Table VII, page 57, pair #15
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0364;
                        consts_Dbar_binary.B(s,l) =  1.1176;
                        consts_Dbar_binary.A(s,l) = -8.9695 + lncmSqAtm2lnmSq;

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 44.6565 * AngSq2mSq;
                        consts_Omega11.B(s,l) = -9.04282 * AngSq2mSq;
                        consts_Omega11.C(s,l) = 7.03582e-1 * AngSq2mSq;
                        consts_Omega11.D(s,l) = -2.04722e-2 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 35.4988 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -5.14838 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 2.13750e-1 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 3.5215E+02  1.7562E+04 -8.8485E-01 2.9253E+01  6.3570E-01  4.5570E-01];
                        a12_vec = [ 1.1494E+02  1.2389E+04 -1.2491E+00 7.3328E+00  3.4827E-01  4.2860E-01];
                        a13_vec = [ 1.9167E+02  5.3633E+04 -1.6030E+00 1.1561E+01  6.3881E-01  4.3160E-01];
                        a22_vec = [ 3.7951E+02  1.7383E+04 -7.9157E-01 3.7919E+01  1.8054E-01  5.4195E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N2 - N','N - N2'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N2 - N','N - N2'
                        T   =           [300    600   1000  2000  4000  6000  8000 10000];   % K
                        O11 = AngSq2mSq*[10.10  8.57  7.70  6.65  5.65  5.05  4.61  4.25];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[11.21  9.68  8.81  7.76  6.73  6.18  5.74  5.36];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 10;
                        stats_fits.acc.Omega22(s,l)   = 10;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [p_O11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = generateFit_PolyLogLog(T,O11,nOrder);
                        [p_O22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = generateFit_PolyLogLog(T,O22,nOrder);

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11);

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #4
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0194;
                        consts_Omega11.B(s,l) =  0.0119;
                        consts_Omega11.A(s,l) =  4.1055 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #4
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0190;
                        consts_Omega22.B(s,l) =  0.0239;
                        consts_Omega22.A(s,l) =  4.1782 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #4
                        consts_Bstar.C(s,l) =  0.0043;
                        consts_Bstar.B(s,l) = -0.0494;
                        consts_Bstar.A(s,l) =  0.2850;

                        % Table VII, page 57, pair #4
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0195;
                        consts_Dbar_binary.B(s,l) =  1.4880;
                        consts_Dbar_binary.A(s,l) = -10.3654 + lncmSqAtm2lnmSq;

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 26.5913 * AngSq2mSq;
                        consts_Omega11.B(s,l) = -3.89355 * AngSq2mSq;
                        consts_Omega11.C(s,l) = 1.64717e-1 * AngSq2mSq;
                        consts_Omega11.D(s,l) = -1.55533e-3 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 29.1411 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -3.88328 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 1.29356e-1 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 2.3258E+01  1.9547E+03 -1.2673E+00 9.9578E-01  1.0382E-01  4.4037E-01];
                        a12_vec = [ 2.3258E+01  1.4173E+04 -1.8460E+00 1.2542E+00  7.0239E-02  4.9160E-01];
                        a13_vec = [ 1.4489E-01  4.8346E+02 -2.3505E+00 9.0340E-03  3.2446E-04  5.3192E-01];
                        a22_vec = [ 2.0822E+01  9.8291E+02 -1.0120E+00 1.1670E+00  6.8362E-02  4.3962E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N2 - O','O - N2'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N2 - O','O - N2'
                        T   =           [300  1000  2000  4000  5000 10000 15000];    % K
                        O11 = AngSq2mSq*[8.07  5.93  5.17  4.77  4.31  3.71  3.38];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[8.99  6.72  5.91  5.22  5.01  4.36  3.95];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #7
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0139;
                        consts_Omega11.B(s,l) = -0.0825;
                        consts_Omega11.A(s,l) =  4.5785 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #7
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0169;
                        consts_Omega22.B(s,l) = -0.0143;
                        consts_Omega22.A(s,l) =  4.4195 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #7
                        consts_Bstar.C(s,l) =  0.0042;
                        consts_Bstar.B(s,l) = -0.0471;
                        consts_Bstar.A(s,l) =  0.2747;

                        % Table VII, page 57, pair #7
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0140;
                        consts_Dbar_binary.B(s,l) =  1.5824;
                        consts_Dbar_binary.A(s,l) = -10.8819 + lncmSqAtm2lnmSq;

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 22.5205 * AngSq2mSq;
                        consts_Omega11.B(s,l) = -3.09176 * AngSq2mSq;
                        consts_Omega11.C(s,l) = 1.23348e-1 * AngSq2mSq;
                        consts_Omega11.D(s,l) = -1.13011e-3 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 24.3359 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -3.02479 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 9.39904e-2 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 4.2411E-11  7.0283E-09 -1.7738E+00 -1.3666E-12  1.4068E-12  2.5682E-01];
                        a12_vec = [ 9.3078E+01  3.7746E+05 -2.4904E+00 1.0000E+00  1.8012E+00  3.0818E-01];
                        a13_vec = [ 7.1865E+01  8.4650E+06 -3.3838E+00 2.3305E+00  1.0000E+00  3.4250E-01];
                        a22_vec = [ 4.8434E-11  1.3958E-09 -1.3115E+00 -2.0696E-12  1.9493E-12  2.2447E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N2 - NO','NO - N2'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N2 - NO','NO - N2'
                        T   =           [300   500   600    1000  2000  4000  5000  6000  8000 10000 15000];   % K
                        O11 = AngSq2mSq*[11.88 10.61 10.24  9.35  8.12  6.82  6.43  6.12  5.66  5.31  4.71];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[13.44 11.87 11.44 10.48  9.32  8.04  7.61  7.27  6.74  6.33  5.62];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #11
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0291;
                        consts_Omega11.B(s,l) =  0.2324;
                        consts_Omega11.A(s,l) =  3.2082 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #11
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0385;
                        consts_Omega22.B(s,l) =  0.4226;
                        consts_Omega22.A(s,l) =  2.4507 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #11
                        consts_Bstar.C(s,l) = -0.0045;
                        consts_Bstar.B(s,l) =  0.1010;
                        consts_Bstar.A(s,l) = -0.3872;

                        % Table VII, page 57, pair #11
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0291;
                        consts_Dbar_binary.B(s,l) =  1.2676;
                        consts_Dbar_binary.A(s,l) = -9.6878 + lncmSqAtm2lnmSq;

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 47.9581 * AngSq2mSq;
                        consts_Omega11.B(s,l) = -10.0316 * AngSq2mSq;
                        consts_Omega11.C(s,l) = 7.99819e-1 * AngSq2mSq;
                        consts_Omega11.D(s,l) = -2.36812e-2 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 37.2011 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -5.51237 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 2.32371e-1 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 1.6324E+02  7.2640E+03 -9.5137E-01 8.2746E+00  1.0000E+00  3.4180E-01];
                        a12_vec = [ 1.1646E+02  1.9357E+04 -1.4606E+00 4.4626E+00  8.8277E-01  3.3773E-01];
                        a13_vec = [ 2.6945E+01  1.7648E+04 -1.9074E+00 1.1168E+00  1.8471E-01  3.5476E-01];
                        a22_vec = [ 1.7855E+02  7.4922E+03 -8.3567E-01 1.3283E+01  4.0509E-01  3.9878E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case 'N2 - N2'
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N2 - N2'
                        T   =           [300   600    1000  2000  4000  6000  8000 10000];   % K
                        O11 = AngSq2mSq*[12.23 10.60  9.79  8.60  7.49  6.87  6.43  6.06];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[13.72 11.80 10.94  9.82  8.70  8.08  7.58  7.32];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 10;
                        stats_fits.acc.Omega22(s,l)   = 10;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 4.2447E+02  2.5868E+04 -1.0330E+00 2.7547E+01  1.0000E+00  4.2785E-01];
                        a12_vec = [ 2.7590E+02  4.6513E+04 -1.4489E+00 1.5340E+01  8.8021E-01  4.1708E-01];
                        a13_vec = [ 1.3522E+02  6.1370E+04 -1.8121E+00 7.5032E+00  4.3667E-01  4.2663E-01];
                        a22_vec = [ 7.5032E+02  4.3769E+04 -9.6115E-00 5.5575E+01  7.3450E-01  4.7747E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #1
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0112;
                        consts_Omega11.B(s,l) = -0.1182;
                        consts_Omega11.A(s,l) =  4.8464 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #1
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0203;
                        consts_Omega22.B(s,l) =  0.0683;
                        consts_Omega22.A(s,l) =  4.0900 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #1
                        consts_Bstar.C(s,l) = -0.0073;
                        consts_Bstar.B(s,l) =  0.1444;
                        consts_Bstar.A(s,l) = -0.5625;

                        % Table VII, page 57, pair #1
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0112;
                        consts_Dbar_binary.B(s,l) =  1.6182;
                        consts_Dbar_binary.A(s,l) = -11.3091 + lncmSqAtm2lnmSq;

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 51.4675 * AngSq2mSq;
                        consts_Omega11.B(s,l) = -11.0430 * AngSq2mSq;
                        consts_Omega11.C(s,l) = 8.97446e-1 * AngSq2mSq;
                        consts_Omega11.D(s,l) = -2.69370e-2 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 38.5429 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -5.76339 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 2.44525e-1 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O2 - N','N - O2'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O2 - N','N - O2'
                        T   =           [500   600   1000  2000  4000  5000  6000  8000 10000 15000];   % K
                        O11 = AngSq2mSq*[7.56  7.26  6.55  5.60  4.75  4.49  4.28  3.96  3.72  3.31];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[8.79  8.47  7.68  6.63  5.67  5.38  5.14  4.78  4.51  4.04];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #5
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0179;
                        consts_Omega11.B(s,l) =  0.0152;
                        consts_Omega11.A(s,l) =  3.9996 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #5
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0203;
                        consts_Omega22.B(s,l) =  0.0730;
                        consts_Omega22.A(s,l) =  3.8818 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #5
                        consts_Bstar.C(s,l) =  0.0033;
                        consts_Bstar.B(s,l) = -0.0366;
                        consts_Bstar.A(s,l) =  0.2332;

                        % Table VII, page 57, pair #5
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0179;
                        consts_Dbar_binary.B(s,l) =  1.4848;
                        consts_Dbar_binary.A(s,l) = -10.2810 + lncmSqAtm2lnmSq;

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 20.7291 * AngSq2mSq;
                        consts_Omega11.B(s,l) = -2.74065 * AngSq2mSq;
                        consts_Omega11.C(s,l) = 1.05659e-1 * AngSq2mSq;
                        consts_Omega11.D(s,l) = -9.50560e-4 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 22.2070 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -2.65001 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 7.90574e-2 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 3.4675E+01  1.3056E+03 -1.1164E+00 3.4754E-01  5.7951E-01  3.0097E-01];
                        a12_vec = [ 4.7427E+00  1.7737E+03 -1.7517E+00 1.4546E-01  5.1947E-02  3.4688E-01];
                        a13_vec = [ 1.1480E+00  3.3633E+03 -2.3376E+00 4.9886E-02  9.5303E-03  3.7913E-01];
                        a22_vec = [ 9.7073E+01  2.5179E+03 -8.2806E-01 4.4350E+00  1.0000E+00  3.1601E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O2 - O','O - O2'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O2 - O','O - O2'
                        T   =           [300    600   1000  2000  4000  6000  8000 10000];   % K
                        O11 = AngSq2mSq*[9.10   7.58  6.74  5.70  4.78  4.29  3.96  3.71];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[10.13  8.61  7.78  6.71  5.67  5.13  4.78  4.50];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 10;
                        stats_fits.acc.Omega22(s,l)   = 10;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #8
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0226;
                        consts_Omega11.B(s,l) =  0.1300;
                        consts_Omega11.A(s,l) =  3.3363 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #8
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0247;
                        consts_Omega22.B(s,l) =  0.1783;
                        consts_Omega22.A(s,l) =  3.2517 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #8
                        consts_Bstar.C(s,l) =  0.0024;
                        consts_Bstar.B(s,l) = -0.0245;
                        consts_Bstar.A(s,l) =  0.1808;

                        % Table VII, page 57, pair #8
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0226;
                        consts_Dbar_binary.B(s,l) =  1.3700;
                        consts_Dbar_binary.A(s,l) = -9.6631 + lncmSqAtm2lnmSq;

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 19.0316 * AngSq2mSq;
                        consts_Omega11.B(s,l) = -2.38970 * AngSq2mSq;
                        consts_Omega11.C(s,l) = 8.79072e-2 * AngSq2mSq;
                        consts_Omega11.D(s,l) = -7.70805e-4 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 20.1385 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -2.27247 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 6.41073e-2 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    case 'mars5dplr'
                        % Original source: Stallcop, J.R., Partridge, H., and Levin, E., "Effective Potential Energies
                        % and Transport Cross Sections for Atom-Molecule Interactions of Nitrogen and
                        % Oxygen," Physical Review A, Vol. 64, 2001.
                        %
                        % These are tabulated in above ref. between 100-10000 K. Fits are computed using
                        % data between 300-10000 K, inclusive.
                        %
                        % Curve fit data for 'O2 - O' (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-4.8405803E-03  1.0297688E-01 -9.6876576E-01  6.1629812E+02];
                        p_O22 = [-3.7969686E-03  7.6789981E-02 -7.3056809E-01  3.3958171E+02];
                        p_Bstar = [0.0000000E+00  2.3451298E-03 -1.8538309E-02  1.1548051E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 5.4824E+01  2.6124E+03 -1.2756E+00 -1.0000E+00  1.7971E+00  2.4756E-01];
                        a12_vec = [ 1.1271E+01  5.9978E+03 -1.8948E+00 2.1033E-01  2.2547E-01  2.9521E-01];
                        a13_vec = [ 3.0924E+00  3.1551E+04 -2.7099E+00 1.1433E-01  4.6291E-02  3.2618E-01];
                        a22_vec = [ 6.4059E+01  1.6075E+03 -9.2241E-01 1.0000E+00  1.5258E+00  2.5189E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O2 - NO','NO - O2'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O2 - NO','NO - O2'
                        T   =           [300   500    600   1000  2000  4000  5000  6000  8000 10000 15000];   % K
                        O11 = AngSq2mSq*[11.39 10.10  9.75  8.89  7.74  6.56  6.23  5.98  5.59  5.31  4.82];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[12.93 11.32 10.90  9.94  8.89  7.80  7.45  7.17  6.73  6.39  5.80];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #12
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0438;
                        consts_Omega11.B(s,l) =  0.5352;
                        consts_Omega11.A(s,l) =  1.7252 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #12
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0522;
                        consts_Omega22.B(s,l) =  0.7045;
                        consts_Omega22.A(s,l) =  1.0738 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #12
                        consts_Bstar.C(s,l) = -0.0010;
                        consts_Bstar.B(s,l) =  0.0410;
                        consts_Bstar.A(s,l) = -0.1312;

                        % Table VII, page 57, pair #12
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0438;
                        consts_Dbar_binary.B(s,l) =  0.9647;
                        consts_Dbar_binary.A(s,l) = -8.2380 + lncmSqAtm2lnmSq;

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 41.4549 * AngSq2mSq;
                        consts_Omega11.B(s,l) = -8.12614 * AngSq2mSq;
                        consts_Omega11.C(s,l) = 6.16928e-1 * AngSq2mSq;
                        consts_Omega11.D(s,l) = -1.76403e-2 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 33.8259 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -4.80694 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 1.96719e-1 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 1.5090E+02  6.7215E+03 -9.1171E-01 9.6404E+00  8.2038E-01  3.4903E-01];
                        a12_vec = [ 9.6359E+01  1.1890E+04 -1.3477E+00 4.4156E+00  7.4636E-01  3.3393E-01];
                        a13_vec = [ 3.2746E+01  1.4087E+04 -1.7751E+00 1.4877E+00  2.5246E-01  3.4294E-01];
                        a22_vec = [ 2.4059E+02  1.0381E+04 -8.1156E-01 2.1388E+01  3.9864E-01  4.2191E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O2 - N2','N2 - O2'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O2 - N2','N2 - O2'
                        T   =           [300    1000  2000  4000  5000 10000 15000];   % K
                        O11 = AngSq2mSq*[10.16  7.39  6.42  5.59  5.35  4.60  4.20];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[11.23  8.36  7.35  6.47  6.21  5.42  4.94];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 2.9109E+02  1.6995E+04 -9.4388E-01 2.4968E+01  2.8136E-01  5.1156E-01];
                        a12_vec = [ 2.3289E+02  2.6336E+04 -1.2642E+00 1.6690E+01  3.9632E-01  4.8003E-01];
                        a13_vec = [ 1.3425E+02  3.0382E+04 -1.5533E+00 9.1243E+00  2.6978E-01  4.7767E-01];
                        a22_vec = [ 2.9974E+02  1.6635E+04 -8.9024E-01 2.7140E+01  1.0368E-01  5.7374E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);

                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #2
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0465;
                        consts_Omega11.B(s,l) =  0.5729;
                        consts_Omega11.A(s,l) =  1.6185 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #2
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0558;
                        consts_Omega22.B(s,l) =  0.7590;
                        consts_Omega22.A(s,l) =  0.8955 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #2
                        consts_Bstar.C(s,l) = -0.0019;
                        consts_Bstar.B(s,l) =  0.0602;
                        consts_Bstar.A(s,l) = -0.2175;

                        % Table VII, page 57, pair #2
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0465;
                        consts_Dbar_binary.B(s,l) =  0.9271;
                        consts_Dbar_binary.A(s,l) = -8.1137 + lncmSqAtm2lnmSq;

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 44.7050 * AngSq2mSq;
                        consts_Omega11.B(s,l) = -9.09957 * AngSq2mSq;
                        consts_Omega11.C(s,l) = 7.10763e-1 * AngSq2mSq;
                        consts_Omega11.D(s,l) = -2.07387e-2 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 35.9891 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -5.27049 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 2.20331e-1 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case 'O2 - O2'
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O2 - O2'
                        T   =           [300    500   600   1000  2000  4000  5000  6000  8000 10000 15000];   % K
                        O11 = AngSq2mSq*[11.12  9.88  9.53  8.69  7.60  6.52  6.22  5.99  5.64  5.39  4.94];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[12.62 11.06 10.65  9.72  8.70  7.70  7.38  7.12  6.73  6.42  5.89];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        a11_vec = [ 4.1068E+02  2.4065E+04 -8.8536E-01 4.3180E+01  1.4356E-01  6.1155E-01];
                        a12_vec = [ 2.8255E+02  2.6422E+04 -1.1429E+00 2.5231E+01  2.0801E-01  5.6608E-01];
                        a13_vec = [ 2.1907E+02  3.3527E+04 -1.3774E+00 1.8137E+01  2.2481E-01  5.5076E-01];
                        a22_vec = [ 5.3367E+02  2.8676E+04 -8.3866E-01 5.6127E+01  6.1295E-02  6.8066E-01];

                        Trange = [300,10000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(a22_vec,Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Bstar,s,l,'Bstar',a12_vec,a13_vec);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts_Cstar,s,l,'Cstar',a12_vec);


                    case 'GuptaYos90'
                        % Table VIII, page 61, pair #3
                        consts_Omega11.D(s,l) =  0.0;
                        consts_Omega11.C(s,l) = -0.0410;
                        consts_Omega11.B(s,l) =  0.4977;
                        consts_Omega11.A(s,l) =  1.8302 + lnPiAngSq2lnmSq;

                        % Table IX, page 65, pair #3
                        consts_Omega22.D(s,l) =  0.0;
                        consts_Omega22.C(s,l) = -0.0485;
                        consts_Omega22.B(s,l) =  0.6475;
                        consts_Omega22.A(s,l) =  1.2607 + lnPiAngSq2lnmSq;

                        % Table X, page 69, pair #3
                        consts_Bstar.C(s,l) =  0.0001;
                        consts_Bstar.B(s,l) =  0.0181;
                        consts_Bstar.A(s,l) = -0.0306;

                        % Table VII, page 57, pair #3
                        consts_Dbar_binary.D(s,l) =  0.0;
                        consts_Dbar_binary.C(s,l) =  0.0410;
                        consts_Dbar_binary.B(s,l) =  1.0023;
                        consts_Dbar_binary.A(s,l) = -8.3597 + lncmSqAtm2lnmSq;

                    case 'Stuckert91'
                        % Table 1, page 211
                        consts_Omega11.A(s,l) = 38.2716 * AngSq2mSq;
                        consts_Omega11.B(s,l) = -7.19797 * AngSq2mSq;
                        consts_Omega11.C(s,l) = 5.29570e-1 * AngSq2mSq;
                        consts_Omega11.D(s,l) = -1.48088e-2 * AngSq2mSq;

                        % Table 2, page 212
                        consts_Omega22.A(s,l) = 31.6950 * AngSq2mSq;
                        consts_Omega22.B(s,l) = -4.35791 * AngSq2mSq;
                        consts_Omega22.C(s,l) = 1.74228e-1 * AngSq2mSq;

                        % Footnote page 195
                        consts_Bstar.A(s,l) =  1.06;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    case 'mars5dplr'
                        % Original source: Bzowski, J., Kestin, J., Mason, E.A., and Uribe, F.J., "Equilibrium and
                        % Transport Properties of Gases Mixtures at Low Density: Eleven Polyatomic
                        % Gases ad Five Noble Gases," Journal of Phys. and Chem. Ref. Data, Vol. 19,
                        % No. 5, pp. 1179-1231, 1990.
                        %
                        % Computed using the universal collision integral concept of Boushehri et.al.
                        %
                        % Curve fit data for 'O2 - O2' (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-8.0682650E-04  1.6602480E-02 -3.1472774E-01  1.4116458E+02];
                        p_O22 = [-6.2931612E-03  1.4624645E-01 -1.3006927E+00  1.8066892E+03];
                        p_Bstar = [0.0000000E+00  0.0000000E+00  9.9493999E-03  1.0323213E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%% ARGON -AIR INTERACTIONS %%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            case 'Ar - Ar'
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'Ar - Ar'
                        T   =           [300   500  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[11.67 10.05  8.61  7.50  6.47  6.16  5.90  5.51  5.22  4.72  4.41];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[12.88 11.12  9.66  8.60  7.57  7.23  6.96  6.53  6.20  5.60  5.17];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 5;
                        stats_fits.acc.Omega22(s,l)   = 5;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'Ar - N','N - Ar'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'Ar - N'
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000];   % K
                        O11 = AngSq2mSq*[7.94  7.62  6.79  5.72  4.76  4.47  4.25  3.89  3.64  3.19];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[9.31  8.96  8.03  6.84  5.75  5.42  5.17  4.77  4.47  3.95];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'Ar - N2','N2 - Ar'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'Ar - N2'
                        T   =           [300   500  1000  2000  4000  5000  6000  8000 10000 15000];   % K
                        O11 = AngSq2mSq*[10.93  9.57  8.35  7.40  6.51  6.22  5.98  5.61  5.31  4.80];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[11.97 10.50  9.28  8.38  7.53  7.25  7.02  6.63  6.32  5.73];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'Ar - NO','NO - Ar'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'Ar - NO'
                        T   =           [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000];   % K
                        O11 = AngSq2mSq*[11.44 10.10  9.73  8.86  7.78  6.66  6.32  6.06  5.67  5.37  4.86];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[13.03 11.33 10.89  9.90  8.85  7.79  7.44  7.15  6.71  6.37  5.77];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'Ar - O','O - Ar'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'Ar - O'
                        T   =           [300   500  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[9.62  8.72  7.58  6.52  5.54  5.24  5.00  4.64  4.37  3.90  3.58];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[11.11 10.13  8.87  7.69  6.60  6.26  6.00  5.59  5.28  4.74  4.37];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'Ar - O2','O2 - Ar'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'Ar - O2'
                        T   =           [300   500  1000  2000  4000  5000  6000  8000 10000 15000];   % K
                        O11 = AngSq2mSq*[11.30  9.86  8.52  7.42  6.39  6.08  5.83  5.44  5.16  4.67];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[12.33 10.82  9.57  8.54  7.48  7.14  6.87  6.44  6.12  5.54];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures (within 10% accuracy)
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%% CARBON -AIR INTERACTIONS %%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            case 'C - C'
                switch pairsReferences{s,l}
                    case 'mars5dplr'
                        % Original source: Stallcop, J.R., Partridge, H., Pradhan, A., and Levin, E., "Potential Energies
                        % and Collision Integrals for Interactions of Carbon and Nitrogen Atoms,"
                        % Journal of Thermophysics and Heat Transfer, Vol. 14, No. 4, Oct. 2000,
                        % pp. 480-488
                        %
                        %
                        % These are tabulated in above ref. between 100-30000 K. Fits are computed using
                        % data between 300-20000 K, non-inclusive.
                        %
                        % Curve fit data for 'C - C' (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-1.5553378E-02  3.3609231E-01 -2.6829275E+00  5.5227444E+04];
                        p_O22 = [-1.0759359E-02  2.3514931E-01 -1.9828256E+00  1.2173852E+04];
                        p_Bstar = [0.0000000E+00  1.5258105E-02 -1.9494112E-01  2.1383076E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[12.34 10.50  9.96  8.66  7.14  5.84  5.43  5.11  4.59  4.17  3.40  2.86];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[13.44 11.50 10.93  9.44  7.77  6.31  5.92  5.60  5.11  4.71  3.94  3.38];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 5;
                        stats_fits.acc.Omega22(s,l)   = 5;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C - O','O - C'}
                switch pairsReferences{s,l}
                    case 'mars5dplr'
                        % Original source: Gupta Eqn. 44b: simple mixing rule between pure species collision integrals.
                        % Should have good accuracy when base pure species data is of high fidelity.
                        % This was spot checked using available Q.M. data from Stallcop et.al, with
                        % good results for atom-atom and atom-molecule interactions.
                        %
                        % Curve fit data for {'C - O','O - C'} (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-1.1234140E-02  2.4508879E-01 -2.0503802E+00  1.0833278E+04];
                        p_O22 = [-1.0162909E-02  2.2689348E-01 -1.9479492E+00  9.8826939E+03];
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [300   500  2000  4000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[10.35  8.82  6.49  5.29  4.56  4.08  3.70  3.02  2.66];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[11.36  9.79  7.06  5.87  5.17  4.69  4.30  3.57  3.18];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C - CO','CO - C'}
                switch pairsReferences{s,l}
                    case 'mars5dplr'
                        % Original source: Magin, T., Degrez, G., and Sokolova, I., AIAA Paper No. 2002-2226, May 2002
                        %
                        % Curve fit data for {'C - CO','CO - C'} (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-1.3093193E-03  1.5968467E-02 -2.5104707E-01  8.6243651E+01];
                        p_O22 = [-1.2147990E-03  1.4899867E-02 -2.3753715E-01  9.4190315E+01];
                        p_Bstar = [1.7420198E-04 -2.5498641E-03  2.2484370E-02  1.0384059E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000];   % K
                        O11 = AngSq2mSq*[9.01  8.62  7.72  6.57  5.51  5.19  4.94  4.55  4.27  3.77];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[10.48 10.08  9.07  7.80  6.63  6.26  5.98  5.54  5.21  4.63];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 30;
                        stats_fits.acc.Omega22(s,l)   = 30;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C - O2','O2 - C'}
                switch pairsReferences{s,l}
                    case 'mars5dplr'
                        % Original source: Magin, T., Degrez, G., and Sokolova, I., AIAA Paper No. 2002-2226, May 2002
                        %
                        % Curve fit data for {'C - O2','O2 - C'} (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-1.0879416E-03  1.1997429E-02 -2.1927912E-01  7.7021435E+01];
                        p_O22 = [-1.0061962E-03  1.1139684E-02 -2.0753461E-01  8.4271593E+01];
                        p_Bstar = [1.4993702E-04 -2.1507909E-03  1.9630958E-02  1.0427872E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[8.33  8.01  7.21  6.22  5.26  4.97  4.74  4.40  4.15  3.70  3.40];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[9.62  9.29  8.41  7.31  6.26  5.94  5.69  5.30  5.01  4.50  4.16];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 30;
                        stats_fits.acc.Omega22(s,l)   = 30;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C - CO2','CO2 - C'}
                switch pairsReferences{s,l}
                    case 'mars5dplr'
                        % Original source: Magin, T., Degrez, G., and Sokolova, I., AIAA Paper No. 2002-2226, May 2002
                        %
                        % Curve fit data for {'C - CO2','CO2 - C'} (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-8.2006933E-04  8.0735040E-03 -1.8580992E-01  8.8090085E+01];
                        p_O22 = [-7.5545407E-04  7.4306172E-03 -1.7595594E-01  9.6265380E+01];
                        p_Bstar = [1.0055488E-04 -1.2761016E-03  1.2958944E-02  1.0545672E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[10.84 10.50  9.52  8.22  7.08  6.72  6.41  6.00  5.67  5.10  4.71];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[12.48 12.08 10.98  9.62  8.36  7.95  7.62  7.15  6.78  6.14  5.71];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 30;
                        stats_fits.acc.Omega22(s,l)   = 30;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O - CO','CO - O'}
                switch pairsReferences{s,l}
                    case 'mars5dplr'
                        % Original source: Magin, T., Degrez, G., and Sokolova, I., AIAA Paper No. 2002-2226, May 2002
                        %
                        % Curve fit data for {'O - CO','CO - O'} (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-9.2195680E-04  9.3627999E-03 -1.9755831E-01  6.9170978E+01];
                        p_O22 = [-8.4742308E-04  8.5762396E-03 -1.8648005E-01  7.5580503E+01];
                        p_Bstar = [1.2490027E-04 -1.7155111E-03  1.6320853E-02  1.0485898E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[7.40  7.11  6.37  5.42  4.55  4.29  4.09  3.77  3.53  3.12  2.85];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[8.61  8.29  7.46  6.42  5.46  5.17  4.93  4.58  4.31  3.84  3.52];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 30;
                        stats_fits.acc.Omega22(s,l)   = 30;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O - CO2','CO2 - O'}
                switch pairsReferences{s,l}
                    case 'mars5dplr'
                        % Original source: Magin, T., Degrez, G., and Sokolova, I., AIAA Paper No. 2002-2226, May 2002
                        %
                        % Curve fit data for {'O - CO2','CO2 - O'} (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-6.0009773E-04  4.9259799E-03 -1.5733544E-01  7.6423379E+01];
                        p_O22 = [-5.5030515E-04  4.4672805E-03 -1.4917344E-01  8.3369484E+01];
                        p_Bstar = [7.5831197E-05 -9.5785952E-04  1.0694750E-02  1.0541875E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[9.15  8.88  8.12  7.11  6.19  5.91  5.68  5.34  5.09  4.62  4.33];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[10.44 10.13  9.29  8.22  7.23  6.92  6.66  6.28  6.01  5.49  5.15];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 30;
                        stats_fits.acc.Omega22(s,l)   = 30;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case 'CO - CO'
                switch pairsReferences{s,l}
                    case 'mars5dplr'
                        % Original source: Bzowski, J., Kestin, J., Mason, E.A., and Uribe, F.J., "Equilibrium and
                        % Transport Properties of Gases Mixtures at Low Density: Eleven Polyatomic
                        % Gases ad Five Noble Gases," Journal of Phys. and Chem. Ref. Data, Vol. 19,
                        % No. 5, pp. 1179-1231, 1990.
                        %
                        % Computed using the universal collision integral concept of Boushehri et.al.
                        %
                        % Curve fit data for 'CO - CO' (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-3.7894450E-03  6.4890939E-02 -5.5019539E-01  2.1301859E+02];
                        p_O22 = [-6.1963698E-03  1.2272791E-01 -9.9364250E-01  7.1572877E+02];
                        p_Bstar = [-2.1630615E-03  5.0526902E-02 -3.5829330E-01  2.4270721E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);
                    case {'Wright2005','Wright2005Bellemans2015'}

                        T   =           [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[12.12 10.90 10.54  9.63  8.41  7.09  6.67  6.33  5.81  5.42  4.74  4.29];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[13.67 12.18 11.77 10.81  9.57  8.16  7.70  7.33  6.75  6.31  5.54  5.01];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'CO - O2','O2 - CO'}
                switch pairsReferences{s,l}
                    case 'mars5dplr'
                        % Original source: Bzowski, J., Kestin, J., Mason, E.A., and Uribe, F.J., "Equilibrium and
                        % Transport Properties of Gases Mixtures at Low Density: Eleven Polyatomic
                        % Gases ad Five Noble Gases," Journal of Phys. and Chem. Ref. Data, Vol. 19,
                        % No. 5, pp. 1179-1231, 1990.
                        %
                        % Computed using the universal collision integral concept of Boushehri et.al.
                        %
                        % Curve fit data for {'CO - O2','O2 - CO'} (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-1.7831430E-03  2.8638342E-02 -3.4131755E-01  1.3883381E+02];
                        p_O22 = [-6.2259945E-03  1.3348420E-01 -1.1351760E+00  1.0916853E+03];
                        p_Bstar = [-1.1890610E-03  2.7551576E-02 -1.8942013E-01  1.6337697E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[11.60 10.37 10.02  9.15  7.96  6.76  6.40  6.12  5.69  5.36  4.80  4.42];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[13.11 11.60 11.19 10.25  9.13  7.93  7.54  7.23  6.73  6.36  5.70  5.25];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'CO - CO2','CO2 - CO'}
                switch pairsReferences{s,l}
                    case 'mars5dplr'
                        % Original source: Bzowski, J., Kestin, J., Mason, E.A., and Uribe, F.J., "Equilibrium and
                        % Transport Properties of Gases Mixtures at Low Density: Eleven Polyatomic
                        % Gases ad Five Noble Gases," Journal of Phys. and Chem. Ref. Data, Vol. 19,
                        % No. 5, pp. 1179-1231, 1990.
                        %
                        % Computed using the universal collision integral concept of Boushehri et.al.
                        %
                        % Curve fit data for {'CO - CO2','CO2 - CO'} (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-5.9766970E-03  1.3462256E-01 -1.2027136E+00  1.5959580E+03];
                        p_O22 = [-1.0031038E-02  2.3415415E-01 -1.9927531E+00  1.3790102E+04];
                        p_Bstar = [-2.1363011E-03  5.1106616E-02 -3.8468797E-01  2.7772618E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);
                    case {'Wright2005','Wright2005Bellemans2015'}

                        T   =           [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[14.18 12.36 11.88 10.77  9.52  8.26  7.86  7.54  7.05  6.68  6.04  5.60];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[16.24 13.92 13.32 12.03 10.74  9.50  9.08  8.74  8.21  7.79  7.07  6.56];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O2 - CO2','CO2 - O2'}
                switch pairsReferences{s,l}
                    case 'mars5dplr'
                        % Original source: Bzowski, J., Kestin, J., Mason, E.A., and Uribe, F.J., "Equilibrium and
                        % Transport Properties of Gases Mixtures at Low Density: Eleven Polyatomic
                        % Gases ad Five Noble Gases," Journal of Phys. and Chem. Ref. Data, Vol. 19,
                        % No. 5, pp. 1179-1231, 1990.
                        %
                        % Computed using the universal collision integral concept of Boushehri et.al.
                        %
                        % Curve fit data for {'O2 - CO2','CO2 - O2'} (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-5.2872232E-03  1.2885512E-01 -1.2240137E+00  1.8474936E+03];
                        p_O22 = [-9.4655767E-03  2.3157720E-01 -2.0428398E+00  1.7423159E+04];
                        p_Bstar = [-9.9302275E-04  2.4343551E-02 -1.8837796E-01  1.7546936E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[13.68 11.84 11.36 10.27  9.08  7.97  7.64  7.38  6.98  6.69  6.17  5.82];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[15.74 13.35 12.76 11.47 10.23  9.15  8.80  8.53  8.10  7.77  7.19  6.79];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case 'CO2 - CO2'
                switch pairsReferences{s,l}
                    case 'mars5dplr'
                        % Original source: Bzowski, J., Kestin, J., Mason, E.A., and Uribe, F.J., "Equilibrium and
                        % Transport Properties of Gases Mixtures at Low Density: Eleven Polyatomic
                        % Gases ad Five Noble Gases," Journal of Phys. and Chem. Ref. Data, Vol. 19,
                        % No. 5, pp. 1179-1231, 1990.
                        %
                        % Computed using the universal collision integral concept of Boushehri et.al.
                        %
                        % Curve fit data for 'CO2 - CO2' (See DPLR's gupta.tran)
                        % Data given as pi*Omega with Omega measured in
                        % Angstroms squared
                        p_O11 = [-9.9663228E-03  2.5169053E-01 -2.2743951E+00  4.1218450E+04];
                        p_O22 = [-1.3173600E-02  3.3381799E-01 -2.9631754E+00  3.0807419E+05];
                        p_Bstar = [0.0000000E+00  0.0000000E+00  0.0000000E+00  1.1500000E+00];

                        % Note that DPLR's curve fit convention is to express the form of the fit
                        % as pi*omega(1,1) = D1*T^(A1*lnT*lnT + B1*lnT + C1), not the typical cubic
                        % poly-log-log format of pi*omega(1,1) = exp(d1)*T^(a1*lnT*lnT + b1*lnT + c1)
                        % To correct these fits for our format of poly-log-log, we pre-ln the last
                        % entry in each provided fit.
                        p_O11(end) = log(p_O11(end));
                        p_O22(end) = log(p_O22(end));
                        p_Bstar(end) = log(p_Bstar(end));

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.D(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.C(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.B(s,l) =  p_Bstar(icB); icB=icB+1;
                        consts_Bstar.A(s,l) =  p_Bstar(icB);
                    case {'Wright2005','Wright2005Bellemans2015'}

                        T   =           [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[17.35 14.39 13.66 12.12 10.66  9.47  9.13  8.86  8.44  8.12  7.54  7.15];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[20.35 16.45 15.50 13.58 11.92 10.69 10.32 10.02  9.57  9.21  8.59  8.15];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'CN - O','O - CN'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[7.79  7.50  6.69  5.72  4.82  4.55  4.33  4.00  3.75  3.32  3.03];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[9.09  8.76  7.87  6.79  5.78  5.47  5.23  4.85  4.57  4.07  3.74];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 30;
                        stats_fits.acc.Omega22(s,l)   = 30;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'CN - O2','O2 - CN'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[9.90  9.56  8.62  7.49  6.44  6.11  5.85  5.45  5.16  4.64  4.29];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[11.42 11.05 10.02  8.78  7.61  7.24  6.95  6.51  6.18  5.59  5.20];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 30;
                        stats_fits.acc.Omega22(s,l)   = 30;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
                %%%%%%%%%% --> C3 interactions
            case 'C3 - C3'
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.467804e-01 -1.601422e+00 9.257562e+00];
                        p_O22 = [-5.214044e-03 1.527806e-01 -1.632066e+00 9.357244e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C3 - CO2','CO2 - C3'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.363111e-01 -1.401547e+00 9.168908e+00];
                        p_O22 = [-5.214044e-03 1.417366e-01 -1.424124e+00 9.249849e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C3 - C2','C2 - C3'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.325755e-01 -1.333808e+00 8.049742e+00];
                        p_O22 = [-5.214044e-03 1.377959e-01 -1.353703e+00 8.125335e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C3 - CO','CO - C3'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.337048e-01 -1.354088e+00 8.088798e+00];
                        p_O22 = [-5.214044e-03 1.389873e-01 -1.374783e+00 8.165938e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C3 - CN','CN - C3'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.322091e-01 -1.327266e+00 8.000878e+00];
                        p_O22 = [-5.214044e-03 1.374095e-01 -1.346903e+00 8.075984e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C3 - C','C - C3'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.255034e-01 -1.210720e+00 7.289965e+00];
                        p_O22 = [-5.214044e-03 1.303356e-01 -1.225820e+00 7.357228e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C3 - N','N - C3'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.304685e-01 -1.296431e+00 7.583566e+00];
                        p_O22 = [-5.214044e-03 1.355733e-01 -1.314859e+00 7.656438e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C3 - O','O - C3'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.200694e-01 -1.120727e+00 6.759194e+00];
                        p_O22 = [-5.214044e-03 1.246033e-01 -1.132394e+00 6.821559e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C3 - NO','NO - C3'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.336563e-01 -1.353213e+00 8.057950e+00];
                        p_O22 = [-5.214044e-03 1.389361e-01 -1.373873e+00 8.135022e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C3 - N2','N2 - C3'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.337048e-01 -1.354088e+00 8.086201e+00];
                        p_O22 = [-5.214044e-03 1.389873e-01 -1.374783e+00 8.163341e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C3 - O2','O2 - C3'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.352671e-01 -1.382427e+00 8.157431e+00];
                        p_O22 = [-5.214044e-03 1.406353e-01 -1.404244e+00 8.236809e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
                %%%%%%%%%% --> C3 interactions
            case 'C2 - C2'
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.183714e-01 -1.093423e+00 7.067818e+00];
                        p_O22 = [-5.214044e-03 1.228121e-01 -1.104063e+00 7.128907e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C2 - CO2','CO2 - C2'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.322738e-01 -1.328420e+00 8.487972e+00];
                        p_O22 = [-5.214044e-03 1.374777e-01 -1.348103e+00 8.563163e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C2 - CO','CO - C2'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.194489e-01 -1.110703e+00 7.090079e+00];
                        p_O22 = [-5.214044e-03 1.239487e-01 -1.121993e+00 7.151964e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C2 - CN','CN - C2'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.179848e-01 -1.087260e+00 7.024771e+00];
                        p_O22 = [-5.214044e-03 1.224043e-01 -1.097670e+00 7.085585e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C2 - C','C - C2'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.113337e-01 -9.844008e-01 6.435295e+00];
                        p_O22 = [-5.214044e-03 1.153881e-01 -9.910089e-01 6.492305e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C2 - N','N - C2'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.214606e-01 -1.143387e+00 7.046901e+00];
                        p_O22 = [-5.214044e-03 1.260708e-01 -1.155912e+00 7.110401e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C2 - O','O - C2'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.206650e-01 -1.130396e+00 7.006113e+00];
                        p_O22 = [-5.214044e-03 1.252316e-01 -1.142429e+00 7.068954e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C2 - NO','NO - C2'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.194489e-01 -1.110703e+00 7.065997e+00];
                        p_O22 = [-5.214044e-03 1.239487e-01 -1.121993e+00 7.127882e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C2 - N2','N2 - C2'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.194489e-01 -1.110703e+00 7.087710e+00];
                        p_O22 = [-5.214044e-03 1.239487e-01 -1.121993e+00 7.149595e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'C2 - O2','O2 - C2'}
                switch pairsReferences{s,l}
                    case {'Bellemans2015','Wright2005Bellemans2015'}
                        % Original source: Bellemans, A., & Magin, T.
                        % (2015). Calculation of Collision Integrals for
                        % Ablation Species. In 8th European Symposium on
                        % Aerothermodynamics for Space Vehicles. Lisbon,
                        % Portugal. (also in mutation++)
                        %
                        p_O11 = [-4.942712e-03 1.210112e-01 -1.136038e+00 7.139661e+00];
                        p_O22 = [-5.214044e-03 1.255968e-01 -1.148285e+00 7.202786e+00];
                        Bstar = 1.15;   Cstar = 0.92; % this is actually coming from Wright2007

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 0;
                        stats_fits.acc.Omega22(s,l)   = 0;
                        stats_fits.acc.Bstar(s,l)     = 0;

                        % Store data in structs
                        consts_Omega11.D(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.C(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.B(s,l) =  p_O11(ic11); ic11=ic11+1;
                        consts_Omega11.A(s,l) =  p_O11(ic11) + lnPiAngSq2lnmSq;

                        consts_Omega22.D(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.C(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.B(s,l) =  p_O22(ic22); ic22=ic22+1;
                        consts_Omega22.A(s,l) =  p_O22(ic22) + lnPiAngSq2lnmSq;

                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

                %%%%%%%%%% --> CN interactions

            case 'CN - CN'

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[10.67 10.24  9.19  7.79  6.52  6.13  5.83  5.36  5.01  4.41  4.01];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[12.50 12.02 10.85  9.28  7.85  7.42  7.07  6.54  6.14  5.44  4.98];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

            case {'CN - CO2','CO2 - CN'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[12.41 11.99 10.96  9.59  8.34  7.95  7.65  7.17  6.82  6.19  5.77];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[14.22 13.76 12.64 11.14  9.77  9.33  8.99  8.47  8.07  7.37  6.89];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 30;
                        stats_fits.acc.Omega22(s,l)   = 30;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

            case {'CN - CO','CO - CN'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}

                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000];   % K
                        O11 = AngSq2mSq*[10.07  9.74  8.73  7.55  6.44  6.10  5.83  5.41  5.10  4.56];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[11.68 11.31 10.20  8.89  7.65  7.27  6.96  6.50  6.15  5.53];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

            case {'C - CN','CN - C'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[8.25  7.90  7.03  5.91  4.88  4.58  4.34  3.97  3.69  3.21  2.90];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[9.72  9.32  8.36  7.10  5.94  5.59  5.31  4.88  4.56  4.01  3.64];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 30;
                        stats_fits.acc.Omega22(s,l)   = 30;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

                %%%%%%%%%% --> N interactions with carbon species (other than C2 and C3)

            case {'N - CO2','CO2 - N'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [300   500  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[12.00 10.33  8.87  7.80  6.89  6.61  6.40  6.05  5.79  5.34  5.02];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[13.17 11.36  9.87  8.81  7.87  7.59  7.35  6.99  6.72  6.22  5.88];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

            case {'N - CO','CO - N'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [300   500  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[10.54  9.09  7.69  6.53  5.43  5.06  4.76  4.29  3.92  3.26  2.82];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[11.80 10.24  8.85  7.71  6.59  6.24  5.94  5.43  5.02  4.24  3.69];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

            case {'N - CN','CN - N'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[8.68  8.30  7.29  6.02  4.87  4.53  4.26  3.85  3.54  3.02  2.68];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[10.33  9.90  8.76  7.32  6.00  5.60  5.29  4.81  4.46  3.84  3.44];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 30;
                        stats_fits.acc.Omega22(s,l)   = 30;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

            case {'N - C','C - N'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[9.17  7.65  7.25  6.43  5.63  4.81  4.54  4.33  3.98  3.69  3.13  2.72];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[10.31  8.67  8.33  7.33  6.29  5.33  5.07  4.87  4.53  4.26  3.71  3.27];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 5;
                        stats_fits.acc.Omega22(s,l)   = 5;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

                %%%%%%%%%% --> NO interactions with carbon species (other than C2 and C3)

            case {'NO - CO2','CO2 - NO'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[14.03 12.11 11.62 10.49  9.28  8.04  7.67  7.37  6.93  6.61  6.04  5.66];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[16.16 13.68 13.05 11.72 10.44  9.30  8.92  8.62  8.14  7.78  7.13  6.68];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

            case {'NO - CO','CO - NO'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[11.88 10.60 10.24  9.35  8.12  6.82  6.40  6.12  5.66  5.31  4.71  4.30];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[13.43 11.87 11.44 10.48  9.32  8.04  7.61  7.27  6.74  6.33  5.62  5.14];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

            case {'NO - CN','CN - NO'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[10.31  9.91  8.94  7.64  6.47  6.11  5.82  5.39  5.06  4.50  4.12];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[12.00 11.56 10.49  9.05  7.73  7.33  7.01  6.51  6.14  5.50  5.06];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 30;
                        stats_fits.acc.Omega22(s,l)   = 30;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

            case {'NO - C','C - NO'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[7.97  7.65  6.86  5.82  4.87  4.58  4.35  4.00  3.74  3.29  3.00];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[9.34  8.98  8.10  6.93  5.86  5.54  5.28  4.88  4.58  4.07  3.72];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

                %%%%%%%%%% --> N2 interactions with carbon species (other than C2 and C3)

            case {'N2 - CO2','CO2 - N2'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[14.17 12.36 11.88 10.77  9.52  8.25  7.86  7.54  7.05  6.68  6.04  5.60];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[16.24 13.91 13.32 12.03 10.74  9.49  9.08  8.74  8.20  7.79  7.06  6.56];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

            case {'N2 - CO','CO - N2'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [300   500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[14.17 12.36 11.88 10.77  9.52  8.25  7.86  7.54  7.05  6.68  6.04  5.60];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[13.67 12.18 11.77 10.81  9.57  8.16  7.70  7.33  6.75  6.31  5.54  5.02];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

            case {'N2 - CN','CN - N2'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [500   600  1000  2000  4000  5000  6000  8000 10000 15000 20000];   % K
                        O11 = AngSq2mSq*[10.90 10.43  9.29  7.81  6.47  6.07  5.75  5.26  4.90  4.27  3.86];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[12.83 12.30 11.03  9.37  7.86  7.39  7.03  6.47  6.05  5.32  4.84];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 30;
                        stats_fits.acc.Omega22(s,l)   = 30;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

            case {'N2 - C','C - N2'}

                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        T   =           [300   600  1000  2000  4000  6000  8000 10000];   % K
                        O11 = AngSq2mSq*[12.28 10.28  9.22  7.85  6.64  5.96  5.47  5.07];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[13.58 11.36 10.18  8.77  7.46  6.78  6.28  5.94];   % original data: A^2; converted to SI m^2;
                        Bstar = 1.15;
                        Cstar = 0.92;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % For the Bstar and Cstar curve fits, use a constant value over all temperatures
                        % pre-ln it for the eval_Fit_PolyLogLog.m function in DEKAF (it returns the natural log of the value).
                        consts_Bstar.A(s,l)    = log(Bstar);
                        consts_Cstar.A(s,l)    = log(Cstar);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%% ELECTRON - NEUTRAL INTERACTIONS %%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            case {'e- - N','N - e-'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'e- - N','N - e-'
                        T   =           [2000  4000  5000  6000  8000 10000 15000 20000];    % K
                        O11 = AngSq2mSq*[9.04  4.06  3.33  2.93  2.53  2.34  2.13  1.98];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[5.68  3.71  3.52  3.42  3.30  3.20  2.95  2.58];   % original data: A^2; converted to SI m^2;
                        Bstar = [1.52  1.04  1.00  0.99  1.01  1.03  1.16  1.40];           % original data: dimensionless
                        Cstar = [0.63  0.67  0.74  0.79  0.87  0.90  0.93  0.89];           % original data: dimensionless

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 35;
                        stats_fits.acc.Omega22(s,l)   = 35;
                        stats_fits.acc.Bstar(s,l)     = 20;
                        stats_fits.acc.Cstar(s,l)     = 20;

                        % Append additional data points to further on the
                        % T domain to reduce the behavior of diverging
                        % curve fits. Note that this slightly degrades the
                        % quality of the curve fit for most reasonable
                        % temperatures relevant to application; in return,
                        % this allows stable numerical computation of
                        % temperatures as low as ~200 K.
                        if bCollisionExtrap
                            [~,   logO11  ] = extrap_data_prior_fit(log(T),log(O11),  log(T_extrap_el_neut),'left','linear');
                            [~,   logO22  ] = extrap_data_prior_fit(log(T),log(O22),  log(T_extrap_el_neut),'left','linear');
                            [~,   logBstar] = extrap_data_prior_fit(log(T),log(Bstar),log(T_extrap_el_neut),'left','linear');
                            [logT,logCstar] = extrap_data_prior_fit(log(T),log(Cstar),log(T_extrap_el_neut),'left','linear');
                            O11     = exp(logO11);
                            O22     = exp(logO22);
                            Bstar   = exp(logBstar);
                            Cstar   = exp(logCstar);
                            T = exp(logT);
                            Textrap = T_extrap_el_neut; % storing temperatures that were not in the original dataset
                            nOrder_O11 = 5;         % Increase order by two if adding points to capture inflection of curves moreso
                            nOrder_O22 = 5;
                            nOrder_Bstar = 5;
                            nOrder_Cstar = 5;
                        else
                            nOrder_O11 = 3;         % Use the original cubic fits if not appending artificial points
                            nOrder_O22 = 3;
                            nOrder_Bstar = 3;
                            nOrder_Cstar = 3;
                        end

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        %                         nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder_O11,  consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder_O22,  consts_Omega22,s,l);
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)  ] = getPair_fitFromTable(T,Bstar,nOrder_Bstar,consts_Bstar,  s,l);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)  ] = getPair_fitFromTable(T,Cstar,nOrder_Cstar,consts_Cstar,  s,l);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        coeff_mat = [...
                             8.9279E+00  8.6314E+00  8.4009E+00  8.2043E+00  8.0388E+00  8.2257E+00 ; ... a1
                             1.1613E+00  1.0738E+00  9.8544E-01  9.4398E-01  9.0527E-01  9.5133E-01 ; ... a2
                             1.6494E+00  1.8882E+00  1.9023E+00  1.8527E+00  1.8833E+00  8.7677E-01 ; ... a3
                             1.2796E-01  1.2214E-01  1.1026E-01  1.0647E-01  1.0051E-01  6.8519E-02 ; ... a4
                             2.7394E-01  3.3398E-01  4.4086E-01  4.8117E-01  5.4116E-01  1.6570E-01 ; ... a5
                            -2.7594E-01 -3.4447E-01 -3.6762E-01 -3.6661E-01 -3.8773E-01  3.4127E-02 ; ... a6
                             0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  3.5900E-01 ; ... a7
                             0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  0.0000E+00  9.6380E+00 ; ... a8
                             1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  1.0000E+00  9.0876E-01 ; ... a9
                        ];
                        %       O11         O12         O13         O14         O15         O22

                        Trange = [300,20000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 5;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega14,stats_fits.errRes.Omega14(s,l),stats_fits.Rsq.Omega14(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,4),Trange,nOrder,consts_Omega14,s,l,'Omega14');
                        [consts_Omega15,stats_fits.errRes.Omega15(s,l),stats_fits.Rsq.Omega15(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,5),Trange,nOrder,consts_Omega15,s,l,'Omega15');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,6),Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Bstar,s,l,'Bstar',coeff_mat(:,2),coeff_mat(:,3));
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Cstar,s,l,'Cstar',coeff_mat(:,2));

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'e- - N2','N2 - e-'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'e- - N2','N2 - e-'
                        T   =           [500  1000  2000  4000  5000  6000  8000 10000 15000 20000];    % K
                        O11 = AngSq2mSq*[1.56  2.17  2.91  3.59  3.80  3.93  3.99  3.91  3.57  3.29];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[1.46  2.07  2.96  3.88  4.09  4.15  4.04  3.85  3.41  3.12];   % original data: A^2; converted to SI m^2;
                        Bstar =         [0.69  0.61  0.82  0.83  0.96  1.08  1.22  1.25  1.20  1.14];           % original data: dimensionless
                        Cstar =         [1.18  1.16  1.12  1.09  1.08  1.05  0.99  0.95  0.91  0.91];           % original data: dimensionless

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 20;
                        stats_fits.acc.Cstar(s,l)     = 20;

                        % Append additional data points to further on the
                        % T domain to reduce the behavior of diverging
                        % curve fits. Note that this slightly degrades the
                        % quality of the curve fit for most reasonable
                        % temperatures relevant to application; in return,
                        % this allows stable numerical computation of
                        % temperatures as low as ~200 K.
                        if bCollisionExtrap
                            Textrap = T_extrap_el_neut(~ismember(T_extrap_el_neut,T)); % storing temperatures that were not in the original dataset
                            [~,   logO11]   = extrap_data_prior_fit(log(T),log(O11),  log(Textrap),'left','linear');
                            [~,   logO22]   = extrap_data_prior_fit(log(T),log(O22),  log(Textrap),'left','linear');
                            [~,   logBstar] = extrap_data_prior_fit(log(T),log(Bstar),log(Textrap),'left','linear');
                            [logT,logCstar] = extrap_data_prior_fit(log(T),log(Cstar),log(Textrap),'left','linear');
                            O11     = exp(logO11);
                            O22     = exp(logO22);
                            Bstar   = exp(logBstar);
                            Cstar   = exp(logCstar);
                            T = exp(logT);
                            nOrder_Bstar = 5;       % Increase order by two if adding points to capture inflection of curves moreso
                        else
                            nOrder_Bstar = 3;       % Use the original fits if not appending artificial points
                        end

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,      consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,      consts_Omega22,s,l);
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)  ] = getPair_fitFromTable(T,Bstar,nOrder_Bstar,consts_Bstar,  s,l);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)  ] = getPair_fitFromTable(T,Cstar,nOrder,      consts_Cstar,  s,l);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        coeff_mat = [...
                             4.4763E+00  6.2997E+00  6.0853E+00  5.8136E+00  5.4329E+00  7.7099E+00 ; ... a1
                             8.9123E+00  2.8143E+00  2.5330E+00  2.4462E+00  2.3732E+00  3.6418E+00 ; ... a2
                             8.6558E+01  6.0373E+00  9.8515E+00  9.3499E+00  6.5229E+00  6.9892E+01 ; ... a3
                            -1.1907E+01 -6.1715E-07 -1.1814E-10 -1.1687E-11 -5.6704E-11 -2.9740E-01 ; ... a4
                             7.3977E-01  5.9733E+00  9.2906E+00  1.0256E+01  9.6931E+00  1.3600E+00 ; ... a5
                             8.1569E-02 -1.3242E-01 -4.0522E-01 -3.9827E-01 -2.4519E-01 -7.0673E-01 ; ... a6
                             1.7704E+00  2.1061E+00  2.4046E+00  2.6488E+00  2.8589E+00  2.1153E+00 ; ... a7
                             9.2013E+00  8.9217E+00  8.6973E+00  8.5107E+00  8.3525E+00  8.9418E+00 ; ... a8
                             8.0978E-01  7.2869E-01  6.7041E-01  6.2512E-01  5.9211E-01  8.3996E-01 ; ... a9
                        ];
                        %       O11         O12         O13         O14         O15         O22

                        Trange = [500,15000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3; nOrder_Bstar = 4;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega14,stats_fits.errRes.Omega14(s,l),stats_fits.Rsq.Omega14(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,4),Trange,nOrder,consts_Omega14,s,l,'Omega14');
                        [consts_Omega15,stats_fits.errRes.Omega15(s,l),stats_fits.Rsq.Omega15(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,5),Trange,nOrder,consts_Omega15,s,l,'Omega15');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,6),Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder_Bstar,consts_Bstar,s,l,'Bstar',coeff_mat(:,2),coeff_mat(:,3));
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Cstar,s,l,'Cstar',coeff_mat(:,2));

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'e- - NO','NO - e-'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'e- - NO','NO - e-'
                        T   =           [2000  4000  5000  6000  8000 10000 15000 20000];    % K
                        O11 = AngSq2mSq*[4.53  4.64  4.29  3.97  3.48  3.17  2.75  2.55];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[5.64  4.52  4.05  3.73  3.37  3.18  2.92  2.75];   % original data: A^2; converted to SI m^2;
                        Bstar = [1.09  1.38  1.34  1.28  1.19  1.13  1.07  1.07];           % original data: dimensionless
                        Cstar = [1.15  0.90  0.87  0.85  0.85  0.87  0.90  0.93];           % original data: dimensionless

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 35;
                        stats_fits.acc.Omega22(s,l)   = 35;
                        stats_fits.acc.Bstar(s,l)     = 20;
                        stats_fits.acc.Cstar(s,l)     = 20;

                        % Append additional data points to further on the
                        % T domain to reduce the behavior of diverging
                        % curve fits. Note that this slightly degrades the
                        % quality of the curve fit for most reasonable
                        % temperatures relevant to application; in return,
                        % this allows stable numerical computation of
                        % temperatures as low as ~200 K.
                        if bCollisionExtrap
                            [~,   logO11  ] = extrap_data_prior_fit(log(T),log(O11),  log(T_extrap_el_neut),'left','linear');
                            [~,   logO22  ] = extrap_data_prior_fit(log(T),log(O22),  log(T_extrap_el_neut),'left','linear');
                            [~,   logBstar] = extrap_data_prior_fit(log(T),log(Bstar),log(T_extrap_el_neut),'left','linear');
                            [logT,logCstar] = extrap_data_prior_fit(log(T),log(Cstar),log(T_extrap_el_neut),'left','linear');
                            O11     = exp(logO11);
                            O22     = exp(logO22);
                            Bstar   = exp(logBstar);
                            Cstar   = exp(logCstar);
                            T = exp(logT);
                            Textrap = T_extrap_el_neut; % storing temperatures that were not in the original dataset
                            nOrder_O11 = 5;         % Increase order by two if adding points to capture inflection of curves moreso
                            nOrder_O22 = 4;
                            nOrder_Bstar = 5;
                        else
                            nOrder_O11 = 3;         % Use the original fits if not appending artificial points
                            nOrder_O22 = 2;
                            nOrder_Bstar = 3;
                        end

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder_O11,  consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder_O22,  consts_Omega22,s,l);
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)  ] = getPair_fitFromTable(T,Bstar,nOrder_Bstar,consts_Bstar,  s,l);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)  ] = getPair_fitFromTable(T,Cstar,nOrder,      consts_Cstar,  s,l);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        coeff_mat = [...
                            -7.2965E-01 -1.4887E+01 -9.5704E+00 -4.8381E+00  5.8041E-01  3.8989E-02 ; ... a1
                             1.3332E+01  1.8148E+01  1.7653E+01  1.6713E+01  1.3987E+01  1.4692E+01 ; ... a2
                             7.8565E+02  2.5286E+03  1.4649E+03  8.8349E+02  3.9144E+02  6.6332E+02 ; ... a3
                            -1.9259E+02 -1.0586E+03 -5.5148E+02 -2.8954E+02 -9.7881E+01 -1.6621E+02 ; ... a4
                             2.8336E-01  2.5251E-01  3.6509E-01  5.0399E-01  7.0323E-01  5.9703E-01 ; ... a5
                            -6.3703E-02  9.4792E-02  1.4218E-01  2.0687E-01  2.8223E-01  1.9528E-01 ; ... a6
                             9.3029E-01  1.0562E+00  1.1651E+00  1.2667E+00  1.3560E+00  1.0178E+00 ; ... a7
                             8.8086E+00  8.5064E+00  8.2614E+00  8.0651E+00  7.9087E+00  8.6724E+00 ; ... a8
                             9.6216E-01  8.7879E-01  8.2346E-01  7.9767E-01  7.8024E-01  9.6611E-01 ; ... a9
                        ];
                        %       O11         O12         O13         O14         O15         O22

                        Trange = [300,15000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 5;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega14,stats_fits.errRes.Omega14(s,l),stats_fits.Rsq.Omega14(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,4),Trange,nOrder,consts_Omega14,s,l,'Omega14');
                        [consts_Omega15,stats_fits.errRes.Omega15(s,l),stats_fits.Rsq.Omega15(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,5),Trange,nOrder,consts_Omega15,s,l,'Omega15');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,6),Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Bstar,s,l,'Bstar',coeff_mat(:,2),coeff_mat(:,3));
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Cstar,s,l,'Cstar',coeff_mat(:,2));

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'e- - O','O - e-'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'e- - O','O - e-'
                        T   =           [1000  2000  4000  5000  6000  8000 10000 15000 20000];    % K
                        O11 = AngSq2mSq*[0.72  0.85  0.98  1.02  1.05  1.09  1.13  1.20  1.26];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[0.82  1.05  1.34  1.44  1.52  1.65  1.73  1.85  1.90];   % original data: A^2; converted to SI m^2;
                        Bstar = [0.81  0.85  0.89  0.90  0.91  0.90  0.89  0.87  0.86];           % original data: dimensionless
                        Cstar = [1.09  1.08  1.06  1.05  1.05  1.05  1.05  1.05  1.06];           % original data: dimensionless

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 30;
                        stats_fits.acc.Omega22(s,l)   = 30;
                        stats_fits.acc.Bstar(s,l)     = 20;
                        stats_fits.acc.Cstar(s,l)     = 20;

                        % Append additional data points to further on the
                        % T domain to reduce the behavior of diverging
                        % curve fits. Note that this slightly degrades the
                        % quality of the curve fit for most reasonable
                        % temperatures relevant to application; in return,
                        % this allows stable numerical computation of
                        % temperatures as low as ~200 K.
                        if bCollisionExtrap
                            [~,   logO11  ] = extrap_data_prior_fit(log(T),log(O11),  log(T_extrap_el_neut),'left','linear');
                            [~,   logO22  ] = extrap_data_prior_fit(log(T),log(O22),  log(T_extrap_el_neut),'left','linear');
                            [~,   logBstar] = extrap_data_prior_fit(log(T),log(Bstar),log(T_extrap_el_neut),'left','linear');
                            [logT,logCstar] = extrap_data_prior_fit(log(T),log(Cstar),log(T_extrap_el_neut),'left','linear');
                            O11     = exp(logO11);
                            O22     = exp(logO22);
                            Bstar   = exp(logBstar);
                            Cstar   = exp(logCstar);
                            T = exp(logT);
                            Textrap = T_extrap_el_neut; % storing temperatures that were not in the original dataset
                        end

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)  ] = getPair_fitFromTable(T,Bstar,nOrder,consts_Bstar,  s,l);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)  ] = getPair_fitFromTable(T,Cstar,nOrder,consts_Cstar,  s,l);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        coeff_mat = [...
                             1.0568E+01  1.0276E+01  1.0069E+01  9.9739E+00  9.9629E+00  9.9767E+00 ; ... a1
                             1.3640E+00  1.3728E+00  1.2781E+00  1.3564E+00  1.4615E+00  1.6871E+00 ; ... a2
                             3.0548E+04  7.8394E+04  1.2008E+05  2.7669E+05  9.4493E+05  2.9697E+05 ; ... a3
                             2.4439E-01  2.2579E-01  2.1584E-01  2.2906E-01  1.9454E-01  5.6206E-02 ; ... a4
                             1.2512E-01  2.0701E-01  2.7462E-01  1.7466E-01  2.2494E-01  1.4522E-01 ; ... a5
                            -4.2290E+00	-4.6626E+00	-4.9024E+00  5.2350E+00	-5.7280E+00	-5.1321E+00 ; ... a6
                             2.7943E-01  2.4241E-01  2.5935E-01  2.4244E-01  1.9924E-01  7.1705E-02 ; ... a7
                             8.6407E+00  8.1977E+00  8.0276E+00  7.7435E+00  7.4822E+00  8.4309E+00 ; ... a8
                             1.7624E+00  1.5622E+00  1.5527E+00  1.4870E+00  1.3722E+00  1.3830E+00 ; ... a9
                        ];
                        %       O11         O12         O13         O14         O15         O22

                        Trange = [300,30000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 5;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega14,stats_fits.errRes.Omega14(s,l),stats_fits.Rsq.Omega14(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,4),Trange,nOrder,consts_Omega14,s,l,'Omega14');
                        [consts_Omega15,stats_fits.errRes.Omega15(s,l),stats_fits.Rsq.Omega15(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,5),Trange,nOrder,consts_Omega15,s,l,'Omega15');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,6),Trange,3,     consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Bstar,s,l,'Bstar',coeff_mat(:,2),coeff_mat(:,3));
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Cstar,s,l,'Cstar',coeff_mat(:,2));

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'e- - O2','O2 - e-'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'e- - O2','O2 - e-'
                        T   =           [1000  2000  4000  5000  6000  8000 10000 15000 20000];    % K
                        O11 = AngSq2mSq*[1.31  1.72  1.99  2.04  2.06  2.06  2.05  1.99  1.96];   % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[1.30  1.73  2.10  2.18  2.23  2.29  2.31  2.32  2.31];   % original data: A^2; converted to SI m^2;
                        Bstar = [0.68  0.86  0.99  1.02  1.04  1.07  1.07  1.03  1.00];           % original data: dimensionless
                        Cstar = [1.16  1.10  1.04  1.03  1.01  0.99  0.98  0.98  0.98];           % original data: dimensionless

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 20;
                        stats_fits.acc.Omega22(s,l)   = 20;
                        stats_fits.acc.Bstar(s,l)     = 20;
                        stats_fits.acc.Cstar(s,l)     = 20;

                        % Append additional data points to further on the
                        % T domain to reduce the behavior of diverging
                        % curve fits. Note that this slightly degrades the
                        % quality of the curve fit for most reasonable
                        % temperatures relevant to application; in return,
                        % this allows stable numerical computation of
                        % temperatures as low as ~200 K.
                        if bCollisionExtrap
                            [~,   logO11  ] = extrap_data_prior_fit(log(T),log(O11),  log(T_extrap_el_neut),'left','linear');
                            [~,   logO22  ] = extrap_data_prior_fit(log(T),log(O22),  log(T_extrap_el_neut),'left','linear');
                            [~,   logBstar] = extrap_data_prior_fit(log(T),log(Bstar),log(T_extrap_el_neut),'left','linear');
                            [logT,logCstar] = extrap_data_prior_fit(log(T),log(Cstar),log(T_extrap_el_neut),'left','linear');
                            O11     = exp(logO11);
                            O22     = exp(logO22);
                            Bstar   = exp(logBstar);
                            Cstar   = exp(logCstar);
                            T = exp(logT);
                            Textrap = T_extrap_el_neut; % storing temperatures that were not in the original dataset
                        end

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)  ] = getPair_fitFromTable(T,Bstar,nOrder,consts_Bstar,  s,l);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)  ] = getPair_fitFromTable(T,Cstar,nOrder,consts_Cstar,  s,l);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        coeff_mat = [...
                             8.3307E+00  8.3440E+00  8.2363E+00  7.7713E+00  8.0580E+00  8.1575E+00 ; ... a1
                             2.1057E+00  2.1878E+00  2.2106E+00  2.2180E+00  2.2437E+00  2.8053E+00 ; ... a2
                             1.3703E+02  2.6005E+02  3.5049E+02  8.0242E+01  4.9008E+02  9.3652E+00 ; ... a3
                             1.6462E-03 -4.7740E-02 -1.0523E-01 -2.9025E-06 -2.0701E-01 -4.1748E-04 ; ... a4
                             2.2034E+00 -7.8236E-01 -8.9547E-01  4.7426E+00 -1.0225E+00  3.6655E+00 ; ... a5
                            -1.6638E+00 -1.8609E+00 -1.9861E+00 -1.3707E+00 -2.1302E+00 -1.8979E-01 ; ... a6
                            -7.1918E-01 -9.0948E-01 -9.7555E-01 -9.8561E-01 -1.0577E+00 -7.6150E-01 ; ... a7
                             9.7316E+00  9.4672E+00  9.2465E+00  9.0635E+00  8.9127E+00  9.3932E+00 ; ... a8
                             1.0537E+00  1.0447E+00  1.0010E+00  9.5788E-01  9.4543E-01  9.6517E-01 ; ... a9
                        ];
                        %       O11         O12         O13         O14         O15         O22 

                        Trange = [300,30000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 5;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega14,stats_fits.errRes.Omega14(s,l),stats_fits.Rsq.Omega14(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,4),Trange,nOrder,consts_Omega14,s,l,'Omega14');
                        [consts_Omega15,stats_fits.errRes.Omega15(s,l),stats_fits.Rsq.Omega15(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,5),Trange,nOrder,consts_Omega15,s,l,'Omega15');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,6),Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Bstar,s,l,'Bstar',coeff_mat(:,2),coeff_mat(:,3));
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA2_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Cstar,s,l,'Cstar',coeff_mat(:,2));
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%% CATION - NEUTRAL INTERACTIONS %%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            case {'NO+ - N2','N2 - NO+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'NO+ - N2','N2 - NO+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[33.9 24.2 14.6 9.68 8.13 7.35 6.87 6.54 6.10 5.79 5.59];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[33.7 25.1 15.8 10.6 8.97 8.16 7.69 7.36 6.90 6.67 6.36];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.75; % N2 as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);
                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});

                end
            case {'NO+ - O2','O2 - NO+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'NO+ - O2','O2 - NO+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[32.2 22.5 13.7 9.25 7.82 7.11 6.66 6.36 5.93 5.64 5.44];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[31.8 23.5 14.9 10.2 8.66 7.91 7.47 7.16 6.72 6.50 6.20];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.6; % O2 as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'NO+ - NO','NO - NO+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'NO+ - NO','NO - NO+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[49.2 41.6 35.0 31.1 29.4 28.2 27.3 26.6 25.7 25.1 24.7];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[32.7 24.4 15.6 10.6 8.99 8.20 7.74 7.42 6.97 6.76 6.45];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        coeff_mat = [...
                            -8.1007E+02 -8.1015E+02 -8.1009E+02 -8.0815E+02 ; ... a1
                             8.1015E+02  8.1023E+02  8.1017E+02  8.0826E+02 ; ... a2
                            -4.6391E-06 -4.9170E-06 -4.7522E-06  1.4346E-09 ; ... a3
                             7.5318E-04  7.6379E-04  7.7546E-04  3.3750E-07 ; ... a4
                             5.5734E-04  6.2676E-04  5.9147E-04  2.1668E-04 ; ... a5
                             7.3176E-02  6.9886E-02  7.3438E-02  5.0007E-01 ; ... a6
                        ];

                        Trange = [300,30000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,4),Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Bstar,s,l,'Bstar',coeff_mat(:,2),coeff_mat(:,3));
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Cstar,s,l,'Cstar',coeff_mat(:,2));

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'NO+ - N','N - NO+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'NO+ - N','N - NO+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[25.0 17.5 11.3 8.21 7.20 6.68 6.34 6.10 5.76 5.52 5.36];    % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[25.4 18.8 12.3 8.97 7.91 7.37 7.04 6.81 6.46 6.27 6.04];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.13; % N as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'NO+ - O','O - NO+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'NO+ - O','O - NO+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[21.4 14.8 9.64 7.20 6.38 5.94 5.65 5.43 5.13 4.91 4.76];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[22.0 15.8 10.4 7.89 7.07 6.62 6.34 6.13 5.82 5.66 5.41];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 0.77; % O as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N2+ - N2','N2 - N2+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N2+ - N2','N2 - N2+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[49.1 43.7 37.0 32.2 30.1 28.9 28.1 27.5 26.6 26.0 25.6];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[33.7 25.3 16.8 12.1 10.7 9.93 9.50 9.19 8.74 8.41 8.20];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        coeff_mat = [...
                            -1.5707E+02 -1.5988E+02 -1.5655E+02 -6.1066E+01 ; ... a1
                             1.6358E+02  1.6168E+02  1.6306E+02  1.0830E+02 ; ... a2
                            -1.5782E-03 -4.4121E-04 -1.6250E-03  1.6620E-05 ; ... a3
                             4.6818E-02  1.2796E-02  4.7258E-02  1.9752E-04 ; ... a4
                             3.7122E-02  1.0896E-02  3.9996E-02  9.6025E-02 ; ... a5
                             5.8367E-02  5.7820E-02  5.7329E-02  5.0012E-01 ; ... a6
                        ];

                        Trange = [300,30000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,4),Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Bstar,s,l,'Bstar',coeff_mat(:,2),coeff_mat(:,3));
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Cstar,s,l,'Cstar',coeff_mat(:,2));

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N2+ - O2','O2 - N2+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N2+ - O2','O2 - N2+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[31.6 23.3 15.2 10.9 9.53 8.85 8.41 8.11 7.69 7.39 7.17];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[32.7 24.6 16.1 11.7 10.4 9.67 9.26 8.97 8.54 8.22 8.02];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.6; % O2 as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N2+ - NO','NO - N2+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N2+ - NO','NO - N2+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[32.2 23.1 15.2 11.2 9.85 9.18 8.74 8.44 8.02 7.73 7.51];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[32.7 24.6 16.5 12.1 10.7 10.0 9.59 9.30 8.86 8.54 8.35];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.7; % NO as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N2+ - N','N - N2+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N2+ - N','N - N2+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[24.9 18.1 12.6 9.94 9.03 8.54 8.21 7.98 7.65 7.41 7.23];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[26.1 19.5 13.6 10.7 9.78 9.26 8.96 8.74 8.40 8.14 7.98];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.13; % N as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N2+ - O','O - N2+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N2+ - O','O - N2+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[21.3 15.6 11.0 8.87 8.10 7.68 7.38 7.17 6.86 6.63 6.46];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[22.2 16.5 11.8 9.60 8.85 8.42 8.15 7.95 7.62 7.37 7.21];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 0.77; % O as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O2+ - N2','N2 - O2+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O2+ - N2','N2 - O2+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[32.9 23.2 13.9 8.55 6.78 5.92 5.40 5.05 4.60 4.30 4.10];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[31.4 24.1 15.4 9.56 7.59 6.64 6.10 5.74 5.26 4.96 4.73];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.75; % N2 as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O2+ - O2','O2 - O2+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O2+ - O2','O2 - O2+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[48.5 40.9 34.2 30.6 28.7 27.5 26.1 25.3 24.2 23.6 23.2];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[31.0 23.6 14.7 9.09 7.27 6.38 5.88 5.55 5.10 4.81 4.59];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's curve fits (appendix)
                        coeff_mat = [...
                            -1.1575E+02 -2.5560E+02 -2.0496E+02  1.4651E+03 ; ... a1
                             3.7386E+02  2.7178E+02  5.3410E+02  4.5141E+02 ; ... a2
                            -4.8436E-02 -2.8455E-03 -4.3204E-02 -4.7817E-01 ; ... a3
                             3.6486E+00  1.4886E-01  4.7254E+00  1.0000E+00 ; ... a4
                             2.6117E-01  1.0396E-01  5.0113E-01  3.1366E+00 ; ... a5
                             1.4145E-01  7.4847E-02  1.1552E-01  4.9971E-01 ; ... a6
                        ];

                        Trange = [300,30000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,4),Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Bstar,s,l,'Bstar',coeff_mat(:,2),coeff_mat(:,3));
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Cstar,s,l,'Cstar',coeff_mat(:,2));

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O2+ - NO','NO - O2+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O2+ - NO','NO - O2+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[30.7 22.2 13.6 8.47 6.77 5.94 5.44 5.10 4.66 4.37 4.17];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[30.2 23.4 15.1 9.47 7.57 6.65 6.12 5.77 5.31 5.01 4.79];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.7; % NO as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O2+ - N','N - O2+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O2+ - N','N - O2+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[23.2 16.7 10.4 6.87 5.72 5.14 4.79 4.54 4.21 3.99 3.84];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[23.8 18.2 11.6 7.64 6.37 5.74 5.37 5.13 4.78 4.55 4.38];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.13; % N as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O2+ - O','O - O2+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O2+ - O','O - O2+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[19.5 13.9 8.63 5.88 4.98 4.52 4.23 4.02 3.74 3.54 3.40];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[20.8 15.4 9.66 6.54 5.57 5.07 4.78 4.57 4.28 4.08 3.92];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 0.77; % O as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N+ - NO','NO - N+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N+ - NO','NO - N+
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[31.3 22.4 13.8 9.02 7.48 6.71 6.24 5.92 5.50 5.21 5.01];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[31.2 23.7 15.2 9.97 8.28 7.45 6.98 6.66 6.21 5.92 5.70];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.7; % NO as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N+ - N2','N2 - N+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N+ - N2','N2 - N+
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[30.6 22.1 13.7 8.99 7.43 6.65 6.17 5.85 5.41 5.11 4.91];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[31.9 23.8 15.3 10.0 8.27 7.42 6.93 6.60 6.14 5.84 5.62];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.75; % N2 as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N+ - O2','O2 - N+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N+ - O2','O2 - N+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[30.5 21.7 13.2 8.63 7.17 6.44 5.98 5.68 5.26 4.98 4.79];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[30.4 22.9 14.6 9.55 7.96 7.18 6.72 6.41 5.98 5.69 5.47];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.6; % O2 as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N+ - N','N - N+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N+ - N','N - N+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[43.3 38.2 34.3 31.4 30.0 29.0 28.3 27.7 26.9 26.2 25.6];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[20.4 16.4 13.3 10.5 9.15 8.33 7.74 7.26 6.48 5.84 5.31];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98'}
                        % coefficients for Capitelli's curve fits (appendix)
                        coeff_mat = [...
                            -1.3960E+02 -1.2812E+02 -1.3692E+02 -1.8598E+08 ; ... a1
                             3.6208E+02  3.8252E+02  3.7076E+02  1.8598E+08 ; ... a2
                            -4.3674E-02 -5.0422E-02 -4.6642E-02  6.4006E-08 ; ... a3
                             2.7103E+00  3.2848E+00  3.0216E+00  1.6400E+01 ; ... a4
                             3.0103E-01  2.4802E-01  3.0143E-01  1.0000E+00 ; ... a5
                             1.2418E-01  1.4072E-01  1.3162E-01  4.7003E-01 ; ... a6
                        ];

                        Trange = [300,30000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,4),Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Bstar,s,l,'Bstar',coeff_mat(:,2),coeff_mat(:,3));
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Cstar,s,l,'Cstar',coeff_mat(:,2));
                        
                    case {'Stallcop96','Capitelli98Stallcop96'}
                        % Coefficients from Stallcop's table 1 (n=1) for 1000<T<30000
                        consts_Omega11.A(s,l) =  6.7850 + lnPiAngSq2lnmSq;
                        consts_Omega11.B(s,l) = -0.5684;
                        consts_Omega11.C(s,l) =  0.05154;
                        consts_Omega11.D(s,l) = -0.001936;
                        
                        % Coefficients from Stallcop's table 1 (n=2) for 1000<T<30000
                        consts_Omega22.A(s,l) =  16.4097 + lnPiAngSq2lnmSq;
                        consts_Omega22.B(s,l) = -4.4319;
                        consts_Omega22.C(s,l) =  0.53728;
                        consts_Omega22.D(s,l) = -0.023378;
                        
                        % Coefficients from Stallcop's table 2 for 1000<T<30000
                        consts_Bstar.A(s,l) =  0.1023;
                        consts_Bstar.B(s,l) = -0.0102;
                        consts_Bstar.C(s,l) =  0.00072;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'N+ - O','O - N+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'N+ - O','O - N+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[21.6 15.1 10.9 8.33 7.07 6.21 5.56 5.06 4.33 3.82 3.44];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[21.8 15.4 11.1 8.61 7.52 6.75 6.13 5.64 4.90 4.37 3.98];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98'}
                        % coefficients for Capitelli's curve fits (appendix)
                        coeff_mat = [...
                             7.0546E+00  7.6038E+00  8.3999E+00  7.5016E+00 ; ... a1
                             1.5774E+03  1.1282E+03  9.2587E+02  1.1305E+03 ; ... a2
                            -1.0493E+00 -1.0559E+00 -1.0601E+00 -9.0625E-01 ; ... a3
                             3.9567E-01  4.2961E-01  4.7469E-01  5.6489E-01 ; ... a4
                             4.1793E-03  5.3188E-03  6.7752E-03  3.5543E-03 ; ... a5
                             6.3764E-01  6.4415E-01  6.4716E-01  6.3384E-01 ; ... a6
                        ];

                        Trange = [300,30000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,4),Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Bstar,s,l,'Bstar',coeff_mat(:,2),coeff_mat(:,3));
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Cstar,s,l,'Cstar',coeff_mat(:,2));
                        
                    case {'Stallcop96','Capitelli98Stallcop96'}
                        % Coefficients from Stallcop's table 1 (n=1) for 1000<T<30000
                        consts_Omega11.A(s,l) =  1.2270 + lnPiAngSq2lnmSq;
                        consts_Omega11.B(s,l) =  1.2238;
                        consts_Omega11.C(s,l) = -0.16112;
                        consts_Omega11.D(s,l) =  0.004677;
                        
                        % Coefficients from Stallcop's table 1 (n=2) for 1000<T<30000
                        consts_Omega22.A(s,l) =  3.6578 + lnPiAngSq2lnmSq;
                        consts_Omega22.B(s,l) =  0.2917;
                        consts_Omega22.C(s,l) = -0.04636;
                        consts_Omega22.D(s,l) =  0.000269;
                        
                        % Coefficients from Stallcop's table 2 for 1000<T<30000
                        consts_Bstar.A(s,l) =  -1.3866;
                        consts_Bstar.B(s,l) =   0.3625;
                        consts_Bstar.C(s,l) =  -0.01999;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O+ - NO','NO - O+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O+ - NO','NO - O+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[31.4 22.4 13.6 8.64 7.01 6.21 5.73 5.40 4.97 4.68 4.48];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[30.5 23.5 15.1 9.61 7.81 6.93 6.43 6.10 5.64 5.35 5.13];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.7; % NO as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O+ - N2','N2 - O+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O+ - N2','N2 - O+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[31.7 22.8 13.8 8.68 7.01 6.18 5.68 5.34 4.90 4.60 4.40];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[31.2 23.9 15.3 9.68 7.83 6.92 6.40 6.06 5.59 5.28 5.06];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.75; % N2 as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O+ - O2','O2 - O+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O+ - O2','O2 - O+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[33.7 23.4 13.7 8.45 6.80 6.00 5.51 5.19 4.76 4.47 4.28];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[31.8 23.6 14.7 9.26 7.52 6.68 6.19 5.87 5.43 5.14 4.92];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98','Capitelli98Stallcop96'}
                        % coefficients for Capitelli's expressions for ion-non-parent-neutral collisions (sec. 3a2)
                        Trange = [300,30000];
                        nOrder  = 3;
                        alpha = 1.6; % O2 as a neutral species
                        [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits] = adaptCapitelliIonNonNeutral2PolyLogLog(alpha,Trange,nOrder,...
                         consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,stats_fits,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O+ - N','N - O+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O+ - N','N - O+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[23.3 16.3 11.8 9.23 8.09 7.36 6.80 6.34 5.60 5.02 4.56];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[23.6 16.6 11.8 9.07 7.99 7.37 6.94 6.60 6.02 5.53 5.11];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98'}
                        % coefficients for Capitelli's curve fits (appendix)
                        coeff_mat = [...
                             3.9346E-01  4.1859E-01  4.6966E-01  4.9141E-01 ; ... a1
                             6.7619E+01  5.2033E+01  4.6150E+01  4.7448E+01 ; ... a2
                            -8.5579E-01 -8.5611E-01 -8.5645E-01 -6.2444E-01 ; ... a3
                             3.9606E-02  4.2620E-02  4.8058E-02  8.3568E-02 ; ... a4
                             3.0435E-05  3.6537E-05  4.6548E-05  1.4153E-05 ; ... a5
                             7.9289E-01  8.0866E-01  8.1726E-01  8.5148E-01 ; ... a6
                        ];

                        Trange = [300,30000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,4),Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Bstar,s,l,'Bstar',coeff_mat(:,2),coeff_mat(:,3));
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Cstar,s,l,'Cstar',coeff_mat(:,2));
                        
                    case {'Stallcop96','Capitelli98Stallcop96'}
                        % Coefficients from Stallcop's table 1 (n=1) for 1000<T<30000
                        consts_Omega11.A(s,l) =  12.4370 + lnPiAngSq2lnmSq;
                        consts_Omega11.B(s,l) = -2.9705;
                        consts_Omega11.C(s,l) =  0.35528;
                        consts_Omega11.D(s,l) = -0.015950;
                        
                        % Coefficients from Stallcop's table 1 (n=2) for 1000<T<30000
                        consts_Omega22.A(s,l) =  21.4600 + lnPiAngSq2lnmSq;
                        consts_Omega22.B(s,l) = -6.1340;
                        consts_Omega22.C(s,l) =  0.71601;
                        consts_Omega22.D(s,l) = -0.029254;
                        
                        % Coefficients from Stallcop's table 2 for 1000<T<30000
                        consts_Bstar.A(s,l) = -1.3265;
                        consts_Bstar.B(s,l) =  0.3249;
                        consts_Bstar.C(s,l) = -0.01657;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case {'O+ - O','O - O+'}
                switch pairsReferences{s,l}
                    case {'Wright2005','Wright2005Bellemans2015'}
                        % Raw data for 'O+ - O','O - O+'
                        % By Wright2005, we are actually referencing both
                        % Levin and Wright 2004 for O11 and O22, with
                        % Wright 2005 for Bstar
                        % Tables 2 & 3, p. 145-6 (p.3-4 of the article)
                        T   = [300 500 1000 2000 3000 4000 5000 6000 8000 10000 12000];                           % K
                        O11 = AngSq2mSq*[33.6 29.6 27.0 24.9 23.8 23.1 22.6 22.2 21.6 21.1 20.8];         % original data: A^2; converted to SI m^2;
                        O22 = AngSq2mSq*[20.5 14.8 11.1 8.72 7.64 6.94 6.39 5.95 5.26 4.75 4.36];         % original data: A^2; converted to SI m^2;
                        % Wright 2005, Table 3, p. 2560
                        Bstar = 1.20;
                        Cstar = 0.85;

                        % Store accuracy of data (values are percentages)
                        stats_fits.acc.Omega11(s,l)   = 25;
                        stats_fits.acc.Omega22(s,l)   = 25;
                        stats_fits.acc.Bstar(s,l)     = 10;
                        stats_fits.acc.Cstar(s,l)     = 10;

                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals,
                        % use a quartic polynomial log-log curve fit
                        nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  nOrder,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  nOrder,consts_Omega22,s,l);

                        % "pre-ln" for the function, eval_Fit_PolyLogLog
                        consts_Bstar.A(s,l)   = log(Bstar);
                        consts_Cstar.A(s,l)   = log(Cstar);

                    case {'Capitelli98'}
                        % coefficients for Capitelli's curve fits (appendix)
                        coeff_mat = [...
                            -1.1682E+02 -1.2217E+02 -1.1681E+01  3.6880E-12 ; ... a1
                             3.2563E+02  3.2403E+02  2.7424E+02  9.1735E-12 ; ... a2
                            -3.9601E-02 -3.6500E-02 -6.0360E-02 -2.5404E-01 ; ... a3
                             4.1463E+00  3.9708E+00  5.5075E+00  4.1706E-13 ; ... a4
                             3.1988E-01  4.5589E-01  3.2206E-01  4.2236E-15 ; ... a5
                             1.0766E-01  1.0641E-01  1.7246E-01  5.4721E-01 ; ... a6
                        ];

                        Trange = [300,30000];
                        % Generate curve stats_fits
                        % For the Omega11 and Omega22 collision integrals, use a cubic polynomial log-log curve fit
                        nOrder  = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Omega11,s,l,'Omega11');
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,4),Trange,nOrder,consts_Omega22,s,l,'Omega22');
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Bstar,s,l,'Bstar',coeff_mat(:,2),coeff_mat(:,3));
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)]   = adaptCapitelliFitA1_2PolyLogLog(coeff_mat(:,1),Trange,nOrder,consts_Cstar,s,l,'Cstar',coeff_mat(:,2));
                        
                    case {'Stallcop96','Capitelli98Stallcop96'}
                        % Coefficients from Stallcop's table 1 (n=1) for 1000<T<30000
                        consts_Omega11.A(s,l) =  6.5348 + lnPiAngSq2lnmSq;
                        consts_Omega11.B(s,l) = -0.5757;
                        consts_Omega11.C(s,l) =  0.05288;
                        consts_Omega11.D(s,l) = -0.001950;
                        
                        % Coefficients from Stallcop's table 1 (n=2) for 1000<T<30000
                        consts_Omega22.A(s,l) =  5.7079 + lnPiAngSq2lnmSq;
                        consts_Omega22.B(s,l) = -0.4402;
                        consts_Omega22.C(s,l) =  0.03706;
                        consts_Omega22.D(s,l) = -0.002682;
                        
                        % Coefficients from Stallcop's table 2 for 1000<T<30000
                        consts_Bstar.A(s,l) =  0.2386;
                        consts_Bstar.B(s,l) = -0.0432;
                        consts_Bstar.C(s,l) =  0.00260;

                        % Not specified, and appearing only in the
                        % expression of the electron thermal conductivity
                        % (not for Air5). Therefore it is fixed as a dummy
                        % of 1
                        consts_Cstar.A(s,l) =  1;

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end

                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%% CHARGE - CHARGE INTERACTIONS %%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            case bAtt % list of attraction pairs (charge-charge)
                switch pairsReferences{s,l}
                    case {'Mason67Devoto73','Capitelli98Stallcop96'}
                        % Raw data for attraction charged-charged species
                        % See Table 1, page 1829 of Mason et al. 1967
                        T = [0.1 0.2 0.3 0.4 0.6 0.8 1.0 2.0 3.0 4.0...
                            6.0 8.0 10.0 20.0 30.0 40.0 60.0 80.0...
                            100.0 200.0 300.0 400.0 600.0 800.0...
                            10^3 10^4 10^5 10^6 10^7 10^8];
                        O11 = [0.0630 0.1364 0.1961 0.2480 0.3297...
                            0.3962 0.4519 0.6467 0.7746 0.8719 1.0173...
                            1.1259 1.2130 1.4972 1.6716 1.7984 1.9807...
                            2.1123 2.2156 2.5427 2.7380 2.8780 3.0767...
                            3.2185 3.3289 4.4759 5.6273 6.7786 7.9299...
                            9.0812];
                        O22 = [0.0384 0.0967 0.1557 0.2100 0.3037...
                            0.3818 0.4483 0.6840 0.8385 0.9541 1.1240...
                            1.2486 1.3473 1.6626 1.8517 1.9872 2.1801...
                            2.3184 2.4266 2.7672 2.9687 3.1121 3.3146...
                            3.4583 3.5699 4.7211 5.8724 7.0237 8.1750...
                            9.3262];
                        % See Table 2, page 1830 of Mason et al. 1967
                        Bstar = [1.4695 1.4577 1.4297 1.3987 1.3668 1.3425...
                            1.3252 1.2798 1.2585 1.2442 1.2255 1.2133...
                            1.2043 1.1798 1.1673 1.1590 1.1481 1.1409...
                            1.1358 1.1220 1.1151 1.1106 1.1045 1.1005...
                            1.0975 1.0734 1.0584 1.0484 1.0414 1.0362];
                        Cstar = [0.7573 0.6585 0.6150 0.5852 0.5549 0.5356...
                            0.5226 0.4901 0.4756 0.4663 0.4546 0.4471...
                            0.4417 0.4273 0.4200 0.4154 0.4094 0.4056...
                            0.4029 0.3956 0.3919 0.3894 0.3862 0.3841...
                            0.3825 0.3702 0.3627 0.3577 0.3541 0.3515];
                        % See Table 3, page 1831 Mason et al. 1967
                        Estar = [0.8411 0.8121 0.774 0.746 0.7103 0.6878 ...
                            0.6726 0.6344 0.6174 0.6073 0.5951 0.5878 ...
                            0.5827 0.5696 0.5634 0.5595 0.5549 0.5521 ...
                            0.5501 0.5448 0.5419 0.5401 0.5377 0.5361 ...
                            0.535 0.5265 0.5213 0.5178 0.5153 0.5134];
                        % See Table 3, page 619 Devoto 1973
                        T2 = [0.1 0.2 0.3 0.4 0.6 0.8 1 2 3 4 6 8 10 20 30 ...
                            40 60 80 100 200 300 400 600 800 1000 1.00E+004];
                        O14 = [0.0285 0.046 0.0578 0.0669 0.0806 0.091 0.0993 ...
                            0.1269 0.144 0.1566 0.1748 0.188 0.1983 0.2307 ...
                            0.2497 0.2634 0.2828 0.2969 0.3081 0.3433 0.3634 ...
                            0.3778 0.3981 0.4125 0.4236 0.5388];
                        O15 = [0.0227 0.0353 0.0437 0.05 0.0596 0.0668 0.0725 ...
                            0.0915 0.1032 0.1118 0.1241 0.133 0.14 0.1616 ...
                            0.1744 0.1835 0.1967 0.2062 0.2137 0.2373 0.2506 ...
                            0.2602 0.2737 0.2833 0.2908 0.3675];
                        O24 = [0.0284 0.0652 0.0956 0.1208 0.1606 0.1914 ...
                            0.2166 0.3007 0.353 0.3914 0.4468 0.4869 0.5183 ...
                            0.6163 0.6738 0.7149 0.7738 0.8164 0.8502 0.9566 ...
                            1.0161 1.0588 1.1194 1.1628 1.1959 1.5413];

                        % Append an additional data point to further on the
                        % Tstar domain to reduce the behavior of diverging
                        % curve fits. Note that this slightly degrades the
                        % quality of the curve fit for most reasonable
                        % temperatures relevant to application; in return,
                        % this allows stable numerical computation of
                        % temperatures as low as ~200 K.
                        if bCollisionExtrap
                            [~,    logO11]   = extrap_data_prior_fit(log(T), log(O11),  log(Tstar_extrap1),'right','linear');
                            [~,    logO22]   = extrap_data_prior_fit(log(T), log(O22),  log(Tstar_extrap1),'right','linear');
                            [~,    logBstar] = extrap_data_prior_fit(log(T), log(Bstar),log(Tstar_extrap1),'right','linear');
                            [~,    logCstar] = extrap_data_prior_fit(log(T), log(Cstar),log(Tstar_extrap1),'right','linear');
                            [logT, logEstar] = extrap_data_prior_fit(log(T), log(Estar),log(Tstar_extrap1),'right','linear');
                            [~,    logO14]   = extrap_data_prior_fit(log(T2),log(O14),  log(Tstar_extrap2),'right','linear');
                            [~,    logO15]   = extrap_data_prior_fit(log(T2),log(O15),  log(Tstar_extrap2),'right','linear');
                            [logT2,logO24]   = extrap_data_prior_fit(log(T2),log(O24),  log(Tstar_extrap2),'right','linear');
                            O11     = exp(logO11);
                            O22     = exp(logO22);
                            Bstar   = exp(logBstar);
                            Cstar   = exp(logCstar);
                            Estar   = exp(logEstar);
                            O14     = exp(logO14);
                            O15     = exp(logO15);
                            O24     = exp(logO24);
                            T  = exp(logT);
                            T2 = exp(logT2);
                            Textrap = Tstar_extrap1; % storing temperatures that were not in the original dataset
                            Textrap2 = Tstar_extrap2;
                        end

                        % Generate curve stats_fits
                        % For the Omega11, Omega22, and Bstar collision integrals, use a quartic polynomial log-log curve fit
                        %                         nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  5,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  5,consts_Omega22,s,l);
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)  ] = getPair_fitFromTable(T,Bstar,3,consts_Bstar,  s,l);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)  ] = getPair_fitFromTable(T,Cstar,5,consts_Cstar,  s,l);
                        [consts_Estar,  stats_fits.errRes.Estar(s,l),  stats_fits.Rsq.Estar(s,l)  ] = getPair_fitFromTable(T,Estar,5,consts_Estar,  s,l);
                        [consts_Omega14,stats_fits.errRes.Omega14(s,l),stats_fits.Rsq.Omega14(s,l)] = getPair_fitFromTable(T2,O14, 5,consts_Omega14,s,l);
                        [consts_Omega15,stats_fits.errRes.Omega15(s,l),stats_fits.Rsq.Omega15(s,l)] = getPair_fitFromTable(T2,O15, 5,consts_Omega15,s,l);
                        [consts_Omega24,stats_fits.errRes.Omega24(s,l),stats_fits.Rsq.Omega24(s,l)] = getPair_fitFromTable(T2,O24, 5,consts_Omega24,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case bRep % list of repulsive pairs (charge-charge)
                switch pairsReferences{s,l}
                    case {'Mason67Devoto73','Capitelli98Stallcop96'}
                        % Raw data for attraction charged-charged species
                        % See Table 1, page 1829 of Mason et al. 1967
                        T = [0.1 0.2 0.3 0.4 0.6 0.8 1.0 2.0 3.0 4.0...
                            6.0 8.0 10.0 20.0 30.0 40.0 60.0 80.0...
                            100.0 200.0 300.0 400.0 600.0 800.0...
                            10^3 10^4 10^5 10^6 10^7 10^8];
                        O11 = [0.0224 0.0511 0.0797 0.1072 0.1584...
                            0.2050 0.2474 0.4177 0.5442 0.6455 0.8026...
                            0.9230 1.0207 1.3431 1.5412 1.6847 1.8898...
                            2.0368 2.1515 2.5087 2.7168 2.8635 3.0687...
                            3.2135 3.3256 4.4763 5.6273 6.7786 7.9299...
                            9.0812];
                        O22 = [0.0304 0.0697 0.1086 0.1459 0.2144...
                            0.2757 0.3310 0.5460 0.6999 0.8197 1.0006...
                            1.1358 1.2435 1.5892 1.7959 1.9438 2.1531...
                            2.3019 2.4171 2.7713 2.9747 3.1177 3.3185...
                            3.4610 3.5719 4.7211 5.8724 7.0237 8.1750...
                            9.3262];
                        % See Table 2, page 1830 of Mason et al. 1967
                        Bstar = [1.3646 1.3865 1.3946 1.3976 1.3972 1.3933...
                            1.3884 1.3627 1.3420 1.3255 1.3011 1.2833...
                            1.2697 1.2295 1.2083 1.1945 1.1770 1.1659...
                            1.1580 1.1365 1.1254 1.1181 1.1089 1.1033...
                            1.0994 1.0733 1.0584 1.0484 1.0414 1.0362];
                        Cstar = [0.7486 0.7104 0.6858 0.6675 0.6411 0.6221...
                            0.6075 0.5632 0.5391 0.5230 0.5022 0.4886...
                            0.4789 0.4529 0.4404 0.4326 0.4230 0.4170...
                            0.4128 0.4013 0.3955 0.3920 0.3876 0.3849...
                            0.3831 0.3702 0.3627 0.3577 0.3541 0.3515];
                        % See Table 3, page 1831 Mason et al. 1967
                        Estar = [0.8146 0.7836 0.7634 0.7483 0.7265 0.7108 ...
                            0.6988 0.6628 0.6436 0.6311 0.6152 0.6052 ...
                            0.5981 0.5797 0.5714 0.5663 0.56 0.5561 ...
                            0.5533 0.5456 0.5419 0.5398 0.5373 0.5358 ...
                            0.5348 0.5265 0.5213 0.5178 0.5153 0.5134];
                        % See Table 3, page 619 Devoto 1973
                        T2 = [0.1 0.2 0.3 0.4 0.6 0.8 1 2 3 4 6 8 10 20 30 ...
                            40 60 80 100 200 300 400 600 800 1000 1.00E+004];
                        O14 = [0.011 0.0221 0.0316 0.0399 0.0536 0.0648 ...
                            0.0742 0.1065 0.1268 0.1416 0.1627 0.1777 ...
                            0.1894 0.2254 0.2462 0.261 0.2817 0.2964 ...
                            0.3078 0.3429 0.3633 0.3778 0.3981 0.4125 ...
                            0.4236 0.5388];
                        O15 = [0.0093 0.0181 0.0255 0.0317 0.0419 0.05 ...
                            0.0567 0.0792 0.093 0.103 0.1172 0.1272 ...
                            0.1349 0.1589 0.1727 0.1825 0.1963 0.2061 ...
                            0.2136 0.237 0.2506 0.2602 0.2737 0.2833 ...
                            ...0.2098 0.3675]; %%%% As in Devoto's 1973 paper
                            0.2908 0.3675];    %%%% correcting the potential typo
                        O24 = [0.0208 0.0445 0.0661 0.0856 0.1193 0.1476 ...
                            0.1719 0.2587 0.3154 0.3574 0.4182 0.462 ...
                            0.4962 0.6027 0.6649 0.7089 0.7707 0.8145 ...
                            0.8483 0.9534 1.0149 1.0583 1.1193 1.1624 ...
                            1.1959 1.5413];

                        % Append an additional data point to further on the
                        % Tstar domain to reduce the behavior of diverging
                        % curve fits. Note that this slightly degrades the
                        % quality of the curve fit for most reasonable
                        % temperatures relevant to application; in return,
                        % this allows stable numerical computation of
                        % temperatures as low as ~200 K.
                        if bCollisionExtrap
                            [~,    logO11]   = extrap_data_prior_fit(log(T), log(O11),  log(Tstar_extrap1),'right','linear');
                            [~,    logO22]   = extrap_data_prior_fit(log(T), log(O22),  log(Tstar_extrap1),'right','linear');
                            [~,    logBstar] = extrap_data_prior_fit(log(T), log(Bstar),log(Tstar_extrap1),'right','linear');
                            [~,    logCstar] = extrap_data_prior_fit(log(T), log(Cstar),log(Tstar_extrap1),'right','linear');
                            [logT, logEstar] = extrap_data_prior_fit(log(T), log(Estar),log(Tstar_extrap1),'right','linear');
                            [~,    logO14]   = extrap_data_prior_fit(log(T2),log(O14),  log(Tstar_extrap2),'right','linear');
                            [~,    logO15]   = extrap_data_prior_fit(log(T2),log(O15),  log(Tstar_extrap2),'right','linear');
                            [logT2,logO24]   = extrap_data_prior_fit(log(T2),log(O24),  log(Tstar_extrap2),'right','linear');
                            O11     = exp(logO11);
                            O22     = exp(logO22);
                            Bstar   = exp(logBstar);
                            Cstar   = exp(logCstar);
                            Estar   = exp(logEstar);
                            O14     = exp(logO14);
                            O15     = exp(logO15);
                            O24     = exp(logO24);
                            T  = exp(logT);
                            T2 = exp(logT2);
                            Textrap = Tstar_extrap1; % storing temperatures that were not in the original dataset
                            Textrap2 = Tstar_extrap2;
                        end

                        % Generate curve stats_fits
                        % For the Omega11, Omega22, and Bstar collision integrals, use a quartic polynomial log-log curve fit
                        %                         nOrder = 3;
                        [consts_Omega11,stats_fits.errRes.Omega11(s,l),stats_fits.Rsq.Omega11(s,l)] = getPair_fitFromTable(T,O11,  5,consts_Omega11,s,l);
                        [consts_Omega22,stats_fits.errRes.Omega22(s,l),stats_fits.Rsq.Omega22(s,l)] = getPair_fitFromTable(T,O22,  5,consts_Omega22,s,l);
                        [consts_Bstar,  stats_fits.errRes.Bstar(s,l),  stats_fits.Rsq.Bstar(s,l)  ] = getPair_fitFromTable(T,Bstar,5,consts_Bstar,  s,l);
                        [consts_Cstar,  stats_fits.errRes.Cstar(s,l),  stats_fits.Rsq.Cstar(s,l)  ] = getPair_fitFromTable(T,Cstar,5,consts_Cstar,  s,l);
                        [consts_Estar,  stats_fits.errRes.Estar(s,l),  stats_fits.Rsq.Estar(s,l)  ] = getPair_fitFromTable(T,Estar,5,consts_Estar,  s,l);
                        [consts_Omega14,stats_fits.errRes.Omega14(s,l),stats_fits.Rsq.Omega14(s,l)] = getPair_fitFromTable(T2,O14, 5,consts_Omega14,s,l);
                        [consts_Omega15,stats_fits.errRes.Omega15(s,l),stats_fits.Rsq.Omega15(s,l)] = getPair_fitFromTable(T2,O15, 5,consts_Omega15,s,l);
                        [consts_Omega24,stats_fits.errRes.Omega24(s,l),stats_fits.Rsq.Omega24(s,l)] = getPair_fitFromTable(T2,O24, 5,consts_Omega24,s,l);

                    otherwise
                        error('Reference ''%s'' is invalid or currently unsupported!',pairsReferences{s,l});
                end
            case 'Air - Air'
                [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,...
                    consts_Estar,consts_Omega14,consts_Omega15,consts_Omega24] = NaNUnusuedCollision(s,l,...
                    consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,...
                    consts_Estar,consts_Omega14,consts_Omega15,consts_Omega24);
            otherwise
                warning('Species pair ''%s'' is invalid or currently unsupported! collision will be set to NaN',pairs{s,l});
                [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,...
                    consts_Estar,consts_Omega14,consts_Omega15,consts_Omega24] = NaNUnusuedCollision(s,l,...
                    consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,...
                    consts_Estar,consts_Omega14,consts_Omega15,consts_Omega24);
        end
        % saving collisional raw data
        if exist('T','var') && opts.bCollisionRawData
            stats_fits.raw_data.T{s,l} = T;
            stats_fits.raw_data.O11{s,l} = O11;
            stats_fits.raw_data.O22{s,l} = O22;
            if exist('Bstar','var')
                if length(Bstar)==1;    stats_fits.raw_data.Bstar{s,l} = Bstar*ones(size(T));
                else;                   stats_fits.raw_data.Bstar{s,l} = Bstar;
                end
            end
            if exist('Cstar','var')
                if length(Cstar)==1;    stats_fits.raw_data.Cstar{s,l} = Cstar*ones(size(T));
                else;                   stats_fits.raw_data.Cstar{s,l} = Cstar;
                end
            end
            if exist('Estar','var')
                if length(Estar)==1;    stats_fits.raw_data.Estar{s,l} = Estar*ones(size(T));
                else;                   stats_fits.raw_data.Estar{s,l} = Estar;
                end
            end
            if exist('O14','var')
                if length(O14)==1;      stats_fits.raw_data.O14{s,l} = O14*ones(size(T2));
                else;                   stats_fits.raw_data.O14{s,l} = O14;
                end
            end
            if exist('O15','var')
                if length(O15)==1;      stats_fits.raw_data.O15{s,l} = O15*ones(size(T2));
                else;                   stats_fits.raw_data.O15{s,l} = O15;
                end
            end
            if exist('O24','var')
                if length(O24)==1;      stats_fits.raw_data.O24{s,l} = O24*ones(size(T2));
                else;                   stats_fits.raw_data.O24{s,l} = O24;
                end
            end
            if exist('T2','var')
                stats_fits.raw_data.T2{s,l} = T2;
            end
            if exist('Textrap','var')
                stats_fits.raw_data.Textrap{s,l} = Textrap;
            end
            if exist('Textrap2','var')
                stats_fits.raw_data.Textrap2{s,l} = Textrap2;
            end
        end
    end
end

end % getPair_Collision_constants.m

function [consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,...
    consts_Estar,consts_Omega14,consts_Omega15,consts_Omega24] = NaNUnusuedCollision(s,l,...
    consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,...
    consts_Estar,consts_Omega14,consts_Omega15,consts_Omega24)

consts_Omega11.F(s,l) =  NaN;   consts_Omega11.E(s,l) =  NaN;    consts_Omega11.D(s,l) =  NaN;
consts_Omega11.C(s,l) =  NaN;   consts_Omega11.B(s,l) =  NaN;    consts_Omega11.A(s,l) =  NaN;
consts_Omega22.F(s,l) =  NaN;   consts_Omega22.E(s,l) =  NaN;    consts_Omega22.D(s,l) =  NaN;
consts_Omega22.C(s,l) =  NaN;   consts_Omega22.B(s,l) =  NaN;    consts_Omega22.A(s,l) =  NaN;
consts_Bstar.F(s,l) =  NaN;   consts_Bstar.E(s,l) =  NaN;    consts_Bstar.D(s,l) =  NaN;
consts_Bstar.C(s,l) =  NaN;   consts_Bstar.B(s,l) =  NaN;    consts_Bstar.A(s,l) =  NaN;
consts_Cstar.F(s,l) =  NaN;   consts_Cstar.E(s,l) =  NaN;    consts_Cstar.D(s,l) =  NaN;
consts_Cstar.C(s,l) =  NaN;   consts_Cstar.B(s,l) =  NaN;    consts_Cstar.A(s,l) =  NaN;
consts_Estar.F(s,l) =  NaN;   consts_Estar.E(s,l) =  NaN;    consts_Estar.D(s,l) =  NaN;
consts_Estar.C(s,l) =  NaN;   consts_Estar.B(s,l) =  NaN;    consts_Estar.A(s,l) =  NaN;
consts_Omega14.F(s,l) =  NaN;   consts_Omega14.E(s,l) =  NaN;    consts_Omega14.D(s,l) =  NaN;
consts_Omega14.C(s,l) =  NaN;   consts_Omega14.B(s,l) =  NaN;    consts_Omega14.A(s,l) =  NaN;
consts_Omega15.F(s,l) =  NaN;   consts_Omega15.E(s,l) =  NaN;    consts_Omega15.D(s,l) =  NaN;
consts_Omega15.C(s,l) =  NaN;   consts_Omega15.B(s,l) =  NaN;    consts_Omega15.A(s,l) =  NaN;
consts_Omega24.F(s,l) =  NaN;   consts_Omega24.E(s,l) =  NaN;    consts_Omega24.D(s,l) =  NaN;
consts_Omega24.C(s,l) =  NaN;   consts_Omega24.B(s,l) =  NaN;    consts_Omega24.A(s,l) =  NaN;

end
