function dlambdaD_dym = getMixtureDer_dlambdaD_dym(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl)
% getMixtureDer_dlambdaD_dym computes the derivative of the Debye length
% with respect to all system temperatures
%
% Usage:
%   (1)
%       dlambdaD_dym = getMixtureDer_dlambdaD_dym(y_s,TTrans,Tel,p,rho,Mm_s,R0,...
%                               nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl)
%
% Inputs and outputs:
%   y_s                 [-]
%       species mass fraction (size N_eta x N_spec)
%   TTrans              [K]
%       translational temperature of heavy species (size N_eta x 1)
%   Tel                 [K]
%       translational temperature of electron (size N_eta x 1)
%   p                   [Pa]
%       mixture pressure (size N_eta x 1)
%   rho                 [kg/m^3]
%       mixture density (size N_eta x 1)
%   Mm_s                [kg/mol]
%       species molar mass (size N_spec x 1)
%   R0           [J/mol-K]
%       Universal gas constant (size 1 x 1)
%   nAvogadro           [-]
%       Avogadro's number (size 1 x 1)
%   bPos                [-]
%       positively-charged-species boolean (size N_spec x 1)
%   bElectron           [-]
%       electron boolen (size N_spec x 1)
%   epsilon0            [C^2/N-m^2 ]
%       permittivity of vacuum (size 1 x 1)
%   kBoltz              [m^2-kg/K-s^2]
%       Boltzmann constant (size 1 x 1)
%   qEl                 [C]
%       elementary charge (size 1 x 1)
%   dlambdaD_dym         [m/K]
%       derivative of the Debye length with respect to all system temperatures
%       (size N_eta x N_spec)
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

% retrieving necessary properties
[N_eta,N_spec] = size(y_s);
lambdaD     = getMixture_lambdaDebye(y_s,TTrans,Tel,rho,Mm_s,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl); % Debye length (eta,1)
drho_dym    = getMixtureDer_drho_dym(p,y_s,R0,Mm_s,bElectron,TTrans,Tel);   % derivative of density wrt species mass fractions (eta,m)
dns_dym     = getSpeciesDer_dns_dym(y_s,drho_dym,rho,nAvogadro,Mm_s);       % temperature derivative of the species number density (eta,s,m)
n_s         = getSpecies_n(rho,y_s,Mm_s,nAvogadro);                         % species number density (eta,s)
T_s         = getSpecies_T(TTrans,Tel,bElectron);                           % species temperature (eta,s)
ZCharge_s   = getSpecies_ZCharge(bPos,bElectron);                           % species unit charge (s,1)

% repmatting
ZCharge_s2D = repmat(ZCharge_s.',[N_eta,1]);                                % (s,1) --> (eta,s)
lambdaD_2D  = repmat(lambdaD,    [1,N_spec]);                               % (eta,1) --> (eta,m)
dns_dym_3D  = permute(dns_dym,[1,3,2]);                                     % (eta,m,s) --> (eta,s,m)

% computing the various addends
sum1 = (ZCharge_s2D.^2.*n_s./T_s) * ones(N_spec,1);                         % (eta,1)
for m=N_spec:-1:1
    sum2(:,m) = (ZCharge_s2D.^2 .* dns_dym_3D(:,:,m)./T_s)*ones(N_spec,1);  % (eta,m)
end

% final assembly
sum1_2D     = repmat(sum1,[1,N_spec]);                                      % (eta,1) --> (eta,m)
dlambdaD_dym = -1./(2*lambdaD_2D) .* epsilon0*kBoltz/qEl^2./sum1_2D.^2 .* sum2;

end % getMixtureDer_dlambdaD_dym