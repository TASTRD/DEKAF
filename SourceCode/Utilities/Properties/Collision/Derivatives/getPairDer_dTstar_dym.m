function dTstar_dym = getPairDer_dTstar_dym(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl)
% getPairDer_dTstar_dym computes the mass-fraction derivatives of the
% reduced temperature.
%
% Usage:
%   (1)
%       dTstar_dym = getPairDer_dTstar_dym(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl)
%
% Inputs and outputs:
%   y_s                 [-]
%       species mass fraction (size N_eta x N_spec)
%   TTrans              [K]
%       translational temperature of heavy species (size N_eta x 1)
%   Tel                 [K]
%       translational temperature of electron (size N_eta x 1)
%   p                   [Pa]
%       mixture pressure (size N_eta x 1)
%   rho                 [kg/m^3]
%       mixture density (size N_eta x 1)
%   Mm_s                [kg/mol]
%       species molar mass (size N_spec x 1)
%   R0           [J/mol-K]
%       Universal gas constant (size 1 x 1)
%   nAvogadro           [-]
%       Avogadro's number (size 1 x 1)
%   bPos                [-]
%       positively-charged-species boolean (size N_spec x 1)
%   bElectron           [-]
%       electron boolen (size N_spec x 1)
%   epsilon0            [C^2/N-m^2 ]
%       permittivity of vacuum (size 1 x 1)
%   kBoltz              [m^2-kg/K-s^2]
%       Boltzmann constant (size 1 x 1)
%   qEl                 [C]
%       elementary charge (size 1 x 1)
%   dTstar_dym           [-]
%       concentration derivatives of the reduced temperature
%       (size N_eta x N_spec(s) x N_spec(l) x N_spec(m))
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

% Obtaining sizes
[N_eta,N_spec] = size(y_s);

% Obtaining auxiliary quantities
dlambdaD_dym     = getMixtureDer_dlambdaD_dym(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl); % (eta,m)
T_sl             = getPair_Tsl(TTrans,Tel,bElectron);                        % collision temperature                        (eta,s,l)
dchargedFact_dym = dlambdaD_dym *kBoltz/(qEl^2/(4*pi*epsilon0));             % ym der. of factor for charged collisions     (eta,m)
bCharged         = getPair_bCharged(bPos,bElectron);                         % charged-collision boolean                    (s,l)

% repmatting
T_sl4D              = repmat(        T_sl,                       [1,1,1,N_spec]);           % (eta,s,l) --> (eta,s,l,m)
dchargedFact_dym4D  = repmat(permute(dchargedFact_dym,[1,3,4,2]),[1,N_spec,N_spec,1]);      % (eta,m) ----> (eta,s,l,m)
bCharged_4D         = repmat(permute(bCharged,        [3,1,2,4]),[N_eta,1,1,N_spec]);       % (s,l) ------> (eta,s,l,m)

% Modifying the charged factor and its derivative to account for the
% non-charged collisions not being dominated by Tstar, but by T_sl alone
dchargedFact_dym4D(~bCharged_4D) = 0;

% assembling
dTstar_dym = dchargedFact_dym4D.*T_sl4D;

end % getPairDer_dTstar_dT