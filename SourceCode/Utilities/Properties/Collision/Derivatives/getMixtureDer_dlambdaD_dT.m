function dlambdaD_dT = getMixtureDer_dlambdaD_dT(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,numberOfTemp)
% getMixtureDer_dlambdaD_dT computes the derivative of the Debye length
% with respect to all system temperatures
%
% Usage:
%   (1)
%       dlambdaD_dT = getMixtureDer_dlambdaD_dT(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,...
%                                       bPos,bElectron,epsilon0,kBoltz,qEl,numberOfTemp)
%
% Inputs and outputs:
%   y_s                 [-]
%       species mass fraction (size N_eta x N_spec)
%   TTrans              [K]
%       translational temperature of heavy species (size N_eta x 1)
%   Tel                 [K]
%       translational temperature of electron (size N_eta x 1)
%   p                   [Pa]
%       mixture pressure (size N_eta x 1)
%   rho                 [kg/m^3]
%       mixture density (size N_eta x 1)
%   Mm_s                [kg/mol]
%       species molar mass (size N_spec x 1)
%   R0           [J/mol-K]
%       Universal gas constant (size 1 x 1)
%   nAvogadro           [-]
%       Avogadro's number (size 1 x 1)
%   bPos                [-]
%       positively-charged-species boolean (size N_spec x 1)
%   bElectron           [-]
%       electron boolen (size N_spec x 1)
%   epsilon0            [C^2/N-m^2 ]
%       permittivity of vacuum (size 1 x 1)
%   kBoltz              [m^2-kg/K-s^2]
%       Boltzmann constant (size 1 x 1)
%   qEl                 [C]
%       elementary charge (size 1 x 1)
%   numberOfTemp
%       string identifying the number of temperatures considered in the
%       system.
%   dlambdaD_dT         [m/K]
%       derivative of the Debye length with respect to all system temperatures
%       (size N_eta x N_T)
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

% retrieving necessary properties
[N_eta,N_spec] = size(y_s);
switch numberOfTemp
    case '1T'
        N_T         = 1;
        drho_dT     = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,TTrans);            % (eta,T)
        dTs_dT      = ones(N_eta,N_spec);                                               % (eta,s,T)
    case '2T'
        N_T         = 2;
        drho_dT     = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,[TTrans,Tel],'2T'); % (eta,T)
        dTs_dT      = repmat(permute([~bElectron , bElectron],[3,1,2]),[N_eta,1,1]);    % [dTs_dTTrans , dTs_dTel] --> (s,T) --> (eta,s,T)
    otherwise
        error(['the chosen numberOfTemp=''',numberOfTemp,''' is not supported']);
end
lambdaD     = getMixture_lambdaDebye(y_s,TTrans,Tel,rho,Mm_s,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl); % Debye length (eta,1)
dns_dT      = getSpeciesDer_dns_dT(y_s,drho_dT,nAvogadro,Mm_s);             % temperature derivative of the species number density (eta,s,T)
n_s         = getSpecies_n(rho,y_s,Mm_s,nAvogadro);                         % species number density (eta,s)
T_s         = getSpecies_T(TTrans,Tel,bElectron);                           % species temperature (eta,s)
ZCharge_s   = getSpecies_ZCharge(bPos,bElectron);                           % species unit charge (s,1)

% repmatting
ZCharge_s2D = repmat(ZCharge_s.',[N_eta,1]);                                % (s,1) --> (eta,s)
lambdaD_2D  = repmat(lambdaD,    [1,N_T]);                                  % (eta,1) --> (eta,T)

% computing the various addends
sum1 = (ZCharge_s2D.^2.*n_s./T_s) * ones(N_spec,1);                         % (eta,1)
for iT=N_T:-1:1
    sum2(:,iT) = (ZCharge_s2D.^2 .* (dns_dT(:,:,iT)./T_s - n_s./T_s.^2.*dTs_dT(:,:,iT)))*ones(N_spec,1); % (eta,T)
end

% final assembly
sum1_2D     = repmat(sum1,[1,N_T]);                                         % (eta,1) --> (eta,T)
dlambdaD_dT = -1./(2*lambdaD_2D) .* epsilon0*kBoltz/qEl^2./sum1_2D.^2 .* sum2;

end % getMixtureDer_dlambdaD_dT