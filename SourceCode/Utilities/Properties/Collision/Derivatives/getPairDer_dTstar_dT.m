function dTstar_dT = getPairDer_dTstar_dT(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,numberOfTemp)
% getPairDer_dTstar_dT computes the temperature derivatives of the reduced
% temperature.
%
% Usage:
%   (1)
%       dTstar_dT = getPairDer_dTstar_dT(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,numberOfTemp)
%
% Inputs and outputs:
%   y_s                 [-]
%       species mass fraction (size N_eta x N_spec)
%   TTrans              [K]
%       translational temperature of heavy species (size N_eta x 1)
%   Tel                 [K]
%       translational temperature of electron (size N_eta x 1)
%   p                   [Pa]
%       mixture pressure (size N_eta x 1)
%   rho                 [kg/m^3]
%       mixture density (size N_eta x 1)
%   Mm_s                [kg/mol]
%       species molar mass (size N_spec x 1)
%   R0           [J/mol-K]
%       Universal gas constant (size 1 x 1)
%   nAvogadro           [-]
%       Avogadro's number (size 1 x 1)
%   bPos                [-]
%       positively-charged-species boolean (size N_spec x 1)
%   bElectron           [-]
%       electron boolen (size N_spec x 1)
%   epsilon0            [C^2/N-m^2 ]
%       permittivity of vacuum (size 1 x 1)
%   kBoltz              [m^2-kg/K-s^2]
%       Boltzmann constant (size 1 x 1)
%   qEl                 [C]
%       elementary charge (size 1 x 1)
%   numberOfTemp
%       string identifying the number of temperatures considered in the
%       system.
%   dTstar_dT           [-]
%       temperature derivatives of the reduced temperature
%       (size N_eta x N_spec x N_spec x N_T)
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

% Obtaining sizes
switch numberOfTemp
    case '1T';  N_T = 1;
    case '2T';  N_T = 2;
    otherwise;  error(['the chosen numberOfTemp ''',numberOfTemp,''' is not supported']);
end
[N_eta,N_spec] = size(y_s);

% Obtaining auxiliary quantities
lambda_Debye    = getMixture_lambdaDebye(y_s,TTrans,Tel,rho,Mm_s,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl); % Debye length          (eta,1)
dlambdaD_dT     = getMixtureDer_dlambdaD_dT(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,numberOfTemp); %  (eta,T)
T_sl            = getPair_Tsl(TTrans,Tel,bElectron);                        % collision temperature                                     (eta,s,l)
dTsl_dT         = getPairDer_dTsl_dT(TTrans,bElectron,numberOfTemp);        % derivative wrt all temperatures                           (eta,s,l,T)
chargedFact     = lambda_Debye*kBoltz/(qEl^2/(4*pi*epsilon0));              % factor to be applied on T_sl for charged collisions       (eta,1)
dchargedFact_dT = dlambdaD_dT *kBoltz/(qEl^2/(4*pi*epsilon0));              % ... and its derivative wrt T                              (eta,T)
bCharged        = getPair_bCharged(bPos,bElectron);                         % charged-collision boolean                                 (s,l)

% repmatting
T_sl4D              = repmat(        T_sl,                      [1,1,1,N_T]);           % (eta,s,l) --> (eta,s,l,T)
chargedFact4D       = repmat(        chargedFact,               [1,N_spec,N_spec,N_T]); % (eta,1) ----> (eta,s,l,T)
dchargedFact_dT4D   = repmat(permute(dchargedFact_dT,[1,3,4,2]),[1,N_spec,N_spec,1]);   % (eta,T) ----> (eta,s,l,T)
bCharged_4D         = repmat(permute(bCharged,       [3,1,2,4]),[N_eta,1,1,N_T]);       % (s,l) ------> (eta,s,l,T)

% Modifying the charged factor and its derivative to account for the
% non-charged collisions not being dominated by Tstar, but by T_sl alone
chargedFact4D(~bCharged_4D)     = 1;
dchargedFact_dT4D(~bCharged_4D) = 0;

% assembling
dTstar_dT = dchargedFact_dT4D.*T_sl4D + chargedFact4D.*dTsl_dT;

end % getPairDer_dTstar_dT