function dlnOmegaij_dym = getPairDer_dlnOmegaij_dym(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omegaij)
% getPairDer_dlnOmegaij_dym returns the temperature derivatives of the
% natural logarithm of a collision integral.
%
% Usage:
%   (1)
%       dlnOmegaij_dym = getPairDer_dlnOmegaij_dym(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omegaij)
%
% Inputs and outputs:
%   y_s                 [-]
%       species mass fraction (size N_eta x N_spec)
%   TTrans              [K]
%       translational temperature of heavy species (size N_eta x 1)
%   Tel                 [K]
%       translational temperature of electron (size N_eta x 1)
%   p                   [Pa]
%       mixture pressure (size N_eta x 1)
%   rho                 [kg/m^3]
%       mixture density (size N_eta x 1)
%   Mm_s                [kg/mol]
%       species molar mass (size N_spec x 1)
%   R0           [J/mol-K]
%       Universal gas constant (size 1 x 1)
%   nAvogadro           [-]
%       Avogadro's number (size 1 x 1)
%   bPos                [-]
%       positively-charged-species boolean (size N_spec x 1)
%   bElectron           [-]
%       electron boolen (size N_spec x 1)
%   epsilon0            [C^2/N-m^2 ]
%       permittivity of vacuum (size 1 x 1)
%   kBoltz              [m^2-kg/K-s^2]
%       Boltzmann constant (size 1 x 1)
%   qEl                 [C]
%       elementary charge (size 1 x 1)
%   consts_Omegaij      [-]
%       struct with fields .A, .B, .C, .D, .E, .F for the collision
%       integral curve fit. Each field has size N_spec x N_spec
%   dlnOmegaij_dym      [-]
%       concentration derivatives of the natural logarithm of a collision
%       integral (size N_eta x N_spec(s) x N_spec(l) x N_spec(m))
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

% Obtaining sizes
[N_eta,N_spec] = size(y_s);

% Computing auxiliary variables
Tstar           = getPair_Tstar(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,bPos);                   % (eta,s,l)
dTstar_dym      = getPairDer_dTstar_dym(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl);      % (eta,s,l,m)
lambdaDebye     = getMixture_lambdaDebye(y_s,TTrans,Tel,rho,Mm_s,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl);          % (eta,1)
dlambdaD_dym    = getMixtureDer_dlambdaD_dym(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl); % (eta,m)
bCharged        = getPair_bCharged(bPos,bElectron);                                                                      % (s,l)
[~,dlnOmegaStarijsl_dTstar] = eval_Fit(Tstar,consts_Omegaij);                                                            % (eta,s,l)

% repmatting
Tstar4D                   = repmat(        Tstar,                   [1,1,1,N_spec]);           % (eta,s,l) --> (eta,s,l,m)
lambdaDebye4D             = repmat(        lambdaDebye,             [1,N_spec,N_spec,N_spec]); % (eta,1) ----> (eta,s,l,m)
dlambdaD_dym4D            = repmat(permute(dlambdaD_dym,[1,3,4,2]), [1,N_spec,N_spec,1]);      % (eta,m) ----> (eta,s,l,m)
bCharged4D                = repmat(permute(bCharged,    [3,1,2,4]), [N_eta,1,1,N_spec]);       % (s,l) ------> (eta,s,l,m)
dlnOmegaStarijsl_dTstar4D = repmat(dlnOmegaStarijsl_dTstar,         [1,1,1,N_spec]);           % (eta,s,l) --> (eta,s,l,m)

% Computing derivatives of the factor premultiplying OmegaijStar_sl for
% charged collisions
dlnChargedFact_dym = 2./lambdaDebye4D .* dlambdaD_dym4D - 2./Tstar4D .* dTstar_dym; % (eta,s,l,m)
dlnChargedFact_dym(~bCharged4D) = 0; % the factor is not necessary for non-charged collisions

% Assembling
dlnOmegaij_dym = dlnChargedFact_dym + dlnOmegaStarijsl_dTstar4D.*dTstar_dym;

end % getPairDer_dlnOmegaij_dym