function dTsl_dT = getPairDer_dTsl_dT(TTrans,bElectron,numberOfTemp)
% getPairDer_dTsl_dT computes the derivative of the temperature for
% a collision with respect to all the system temperatures.
%
% Usage:
%   (1)
%       dTsl_dT = getPairDer_dTsl_dT(TTrans,bElectron,numberOfTemp)
%
% Inputs and outputs:
%   TTrans          [K]
%       translational temperature of heavy particles (size N_eta x 1)
%   bElectron       [-]
%       electron boolean (size N_spec x 1)
%   numberOfTemp
%       string identifying the number of temperatures considered in the
%       system.
%   dTsl_dT         [-]
%       derivative of the temperature driving the collisions between
%       species s and l, with respect to all temperatures
%       (size N_eta x N_spec x N_spec x N_T)
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

N_eta = length(TTrans);
N_spec = length(bElectron);

switch numberOfTemp
    case '1T'
        dTsl_dT = ones(N_eta,N_spec,N_spec);
    case '2T'
        dTsl_dT(:,:,:,1) = ones( N_eta,N_spec,N_spec);  % allocating d/dTTrans derivatives
        dTsl_dT(:,:,:,2) = zeros(N_eta,N_spec,N_spec);  % allocating d/dTel derivatives
        dTsl_dT(:,bElectron,:,1) = 0;                   % all collisions with electrons have T_sl = Tel
        dTsl_dT(:,:,bElectron,1) = 0;
        dTsl_dT(:,bElectron,:,2) = 1;
        dTsl_dT(:,:,bElectron,2) = 1;
    otherwise
        error(['the chosen numberOfTemp=''',numberOfTemp,''' is not supported']);
end

end % getPairDer_dTsl_dT