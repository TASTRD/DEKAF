function dlnOmegaij_dT = getPairDer_dlnOmegaij_dT(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omegaij,numberOfTemp)
% getPairDer_dlnOmegaij_dT returns the temperature derivatives of the
% natural logarithm of a collision integral.
%
% Usage:
%   (1)
%       dlnOmegaij_dT = getPairDer_dlnOmegaij_dT(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,...
%                               bPos,bElectron,epsilon0,kBoltz,qEl,consts_Omegaij,numberOfTemp)
%
% Inputs and outputs:
%   y_s                 [-]
%       species mass fraction (size N_eta x N_spec)
%   TTrans              [K]
%       translational temperature of heavy species (size N_eta x 1)
%   Tel                 [K]
%       translational temperature of electron (size N_eta x 1)
%   p                   [Pa]
%       mixture pressure (size N_eta x 1)
%   rho                 [kg/m^3]
%       mixture density (size N_eta x 1)
%   Mm_s                [kg/mol]
%       species molar mass (size N_spec x 1)
%   R0           [J/mol-K]
%       Universal gas constant (size 1 x 1)
%   nAvogadro           [-]
%       Avogadro's number (size 1 x 1)
%   bPos                [-]
%       positively-charged-species boolean (size N_spec x 1)
%   bElectron           [-]
%       electron boolen (size N_spec x 1)
%   epsilon0            [C^2/N-m^2 ]
%       permittivity of vacuum (size 1 x 1)
%   kBoltz              [m^2-kg/K-s^2]
%       Boltzmann constant (size 1 x 1)
%   qEl                 [C]
%       elementary charge (size 1 x 1)
%   consts_Omegaij      [-]
%       struct with fields .A, .B, .C, .D, .E, .F for the collision
%       integral curve fit. Each field has size N_spec x N_spec
%   numberOfTemp
%       string identifying the number of temperatures considered in the
%       system.
%   dlnOmegaij_dT       [1/K]
%       temperature derivatives of the natural logarithm of a collision
%       integral (size N_eta x N_spec x N_spec x N_T)
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

% Obtaining sizes
switch numberOfTemp
    case '1T';  N_T = 1;
    case '2T';  N_T = 2;
    otherwise;  error(['the chosen numberOfTemp ''',numberOfTemp,''' is not supported']);
end
[N_eta,N_spec] = size(y_s);

% Computing auxiliary variables
Tstar       = getPair_Tstar(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,bPos);                               % (eta,s,l)
dTstar_dT   = getPairDer_dTstar_dT(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,numberOfTemp);      % (eta,s,l,T)
lambdaDebye = getMixture_lambdaDebye(y_s,TTrans,Tel,rho,Mm_s,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl);                      % (eta,1)
dlambdaD_dT = getMixtureDer_dlambdaD_dT(y_s,TTrans,Tel,p,rho,Mm_s,R0,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl,numberOfTemp); % (eta,T)
bCharged    = getPair_bCharged(bPos,bElectron);                                                                                  % (s,l)
[~,dlnOmegaStarijsl_dTstar] = eval_Fit(Tstar,consts_Omegaij);                                                                    % (eta,s,l)

% repmatting
Tstar4D                   = repmat(        Tstar,                   [1,1,1,N_T]);           % (eta,s,l) --> (eta,s,l,T)
lambdaDebye4D             = repmat(        lambdaDebye,             [1,N_spec,N_spec,N_T]); % (eta,1) ----> (eta,s,l,T)
dlambdaD_dT4D             = repmat(permute(dlambdaD_dT, [1,3,4,2]), [1,N_spec,N_spec,1]);   % (eta,T) ----> (eta,s,l,T)
bCharged4D                = repmat(permute(bCharged,    [3,1,2,4]), [N_eta,1,1,N_T]);       % (s,l) ------> (eta,s,l,T)
dlnOmegaStarijsl_dTstar4D = repmat(dlnOmegaStarijsl_dTstar,         [1,1,1,N_T]);           % (eta,s,l) --> (eta,s,l,T)

% Computing derivatives of the factor premultiplying OmegaijStar_sl for
% charged collisions
dlnChargedFact_dT = 2./lambdaDebye4D .* dlambdaD_dT4D - 2./Tstar4D .* dTstar_dT; % (eta,s,l,T)
dlnChargedFact_dT(~bCharged4D) = 0; % the factor is not necessary for non-charged collisions

% Assembling
dlnOmegaij_dT = dlnChargedFact_dT + dlnOmegaStarijsl_dTstar4D.*dTstar_dT;

end % getPairDer_dlnOmegaij_dT