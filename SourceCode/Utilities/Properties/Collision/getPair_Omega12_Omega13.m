function  [Omega12,Omega13,dOmega12_dTsl,dOmega13_dTsl,dOmega12_dTsl2,dOmega13_dTsl2] ...
    = getPair_Omega12_Omega13(Omega11,        Bstar,        Cstar,varargin)
%getPair_Omega12_Omega13 Calculate the collision integrals of order (1,3)
% and (1,2) given the collision integral order (1,1), Bstar, and Cstar
% ratio. Additionally, return their temperature derivatives.
%
% Usage:
% (1)
%   [Omega13,Omega12] = getPair_Omega12_Omega13(Omega11,Bstar,Cstar);
%
% (2)
%   [Omega13,Omega12,dOmega13_dT,dOmega12_dT] = ...
%   getPair_Omega12_Omega13(Omega11,       Bstar,       Cstar,
%                          dOmega11_dTsl, dBstar_dTsl, dCstar_dTsl,
%                          dOmega11_dTsl2,dBstar_dTsl2,dCstar_dTsl2);
%
% Inputs and Outputs
%   Omega11             collision integral of order (1,1)
%   Bstar               collision integral ratio
%                       (5*Omega12 - 4*Omega13)/Omega11
%   Cstar               collision integral ratio
%                       Omega12/Omega11
%   d*_dTsl             derivatives of * with respect to their appropriate
%                       temperature according to the (s,l) pair, Tsl
%
%                       (e.g., if e- is in the interaction, use T_el. else,
%                       use the governing heavy temperature.)
%
%   d*_dTsl2            second derivative of * with respect to Tsl
%
%   Omega13             collision integral of order (1,3)
%   Omega12             collision integral of order (1,2)
%
% References:
%   Scoggins, J. (2017). Development of numerical methods and
%   study of coupled flow, radiation, and ablation phenomena for
%   atmospheric entry. von Karman Institute. Equations 4.17, 4.18
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% Zeroth order derivatives
Omega12 = Cstar.*Omega11;
Omega13 = 1/4*(5*Omega12-Omega11.*Bstar);

if nargout>2
    dOmega11_dTsl = varargin{1};
    dBstar_dTsl = varargin{2};
    dCstar_dTsl = varargin{3};
    % First order derivatives
    dOmega12_dTsl = dCstar_dTsl.*Omega11 + Cstar.*dOmega11_dTsl;
    dOmega13_dTsl = 1/4*(5*dOmega12_dTsl - dOmega11_dTsl.*Bstar - Omega11.*dBstar_dTsl);
end

if nargout>4
    dOmega11_dTsl2 = varargin{4};
    dBstar_dTsl2 = varargin{5};
    dCstar_dTsl2 = varargin{6};
    % Second order derivatives
    dOmega12_dTsl2 = dCstar_dTsl2.*Omega11 + 2*dCstar_dTsl.*dOmega11_dTsl + ...
        Cstar.*dOmega11_dTsl2;
    dOmega13_dTsl2 = 1/4*(-dOmega11_dTsl2.*Bstar - dOmega11_dTsl.*dBstar_dTsl - ...
        dOmega11_dTsl.*dBstar_dTsl - Omega11.*dBstar_dTsl2 +...
        +5*dOmega12_dTsl2);
end

end % getPair_Omega12_Omega13

