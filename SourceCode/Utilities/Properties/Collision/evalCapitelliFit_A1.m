function Omega_ij = evalCapitelliFit_A1(a1,a2,a3,a4,a5,a6,T)
% evalCapitelliFit_A1 evaluates the first curve fit in Capitelli's appendix.
%
% Usage:
%   (1)
%       Omega_ij = evalCapitelliFit_A1(a1,a2,a3,a4,a5,a6,T)
%
% Inputs and outputs:
%   a1-a6       [Ang^2]         1 x 1
%       coefficients in the curve fit
%   T           [K]             N_eta x 1
%       temperature
%   Omega_ij    [m^2]           N_eta x 1
%       collision integral
%
% References:
%   [1] Capitelli, M., Gorse, C., Longo, S., & Giordano, D. (1998).
%   Transport properties of high temperature air species. AIAA-Paper,
%   98–2936. https://doi.org/10.2514/6.1998-2936
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

AngSq2mSq = 1e-20;
Omega_ij = AngSq2mSq * (a1 + a2.*T.^a3) ./ (a4 + a5.*T.^a6);

end % evalCapitelliFit_A1