function T_sl = getPair_Tsl(TTrans,Tel,bElectron)
%GETPAIR_TSL Calculate the collision-governing temperature for a pair of
% species (s,l)
%
% Usage:
%   T_sl = getPair_Tsl(TTrans,Tel,bElectron);
%
% collisions involving electrons should be governed by the
% electron temperature (as proposed by Scoggins 2014), whereas the others
% should be governed by the heavy temperature.
%
% Inputs and Outputs
%
%       TTrans          size N_eta x 1
%       Tel             size N_eta x 1
%       bElectron       size N_spec x 1
%       T_sl            size N_eta x N_spec x N_spec
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

N_spec = length(bElectron);

Tel_3D = repmat(Tel,[1,N_spec,N_spec]);         % (eta,1) --> (eta,s,l)
T_sl = repmat(TTrans,[1,N_spec,N_spec]);        % most collisions are governed by the heavy translational temperature
T_sl(:,bElectron,:) = Tel_3D(:,bElectron,:);    % collisions with electrons are governed by the electron temperature
T_sl(:,:,bElectron) = Tel_3D(:,:,bElectron);

end % getPair_Tsl
