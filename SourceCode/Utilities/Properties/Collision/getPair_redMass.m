function mu_sl = getPair_redMass(Mm_s,nAvogadro)
%GETPAIR_REDMASS Calculate the reduced mass of a binary pair of species
%
% Usage:
%   (1)
%       mu_sl = getPair_redMass(Mm_s,nAvogadro)
%
% Inputs:
%   Mm_s        -   molecular mass of species [kg/mol]
%                   (N_s x 1)
%   nAvogadro   -   Avogadro's constant [part/mol]
%                   scalar
%
% Outputs:
%   mu_sl       -   reduced mass of binary pairs of species [kg]
%                   (N_s x N_l)
%
% References:
% M. Fertig, A. Dohr, H.-H. Frühauf. "Transport Coefficients for
%   High-Temperature Nonequilibrium Air Flows", Journal of Thermophysics
%   and Heat Transfer, Vol. 15, No. 2 (2001), pp. 148-156.
%   https://doi.org/10.2514/2.6603 (See equation (3))
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

N_spec = length(Mm_s);
Mm_s2D = repmat(Mm_s,[1,N_spec]);
Mm_l2D = repmat(Mm_s',[N_spec 1]);

mu_sl = 1/nAvogadro * (Mm_s2D.*Mm_l2D)./(Mm_s2D + Mm_l2D);

end % getPair_redMass.m
