function [consts,errRes,Rsq] = getPair_fitFromTable(T,O,nOrder,consts,s,l)
% getPair_fitFromTable returns the polynomial fit constants, as well as the
%
%
% Usage:
%   (1)
%       [consts,errRes,Rsq] = getPair_fitFromTable(T,O,nOrder,consts,s,l)
%
% Inputs and outputs:
%   T
%       vector of temperatures
%   O
%       vector of collisional cross sections (paired with T)
%   nOrder
%       order of the polynomial approximation used
%   consts
%   	structure of matrices with the polynomial curve-fit coefficients
%   	for each collision pair
%   errRes
%       residual error of the fit
%   Rsq
%       R-squared value (coefficient of determination)
%   s, l
%       species identifier of the collision pair. It is used to know where
%       the new information must be allocated in the consts, errRes and Rsq
%       matrices
%
% Author: Fernando Miro Miro
% Date: December 2019
% GNU Lesser General Public License 3.0

list_abcd = alphabet('capital');                            % list of letters
[p_O,errRes,Rsq] = generateFit_PolyLogLog(T,O,nOrder);      % generating fit data
p_O = flip(p_O);                                            % we flip them to have them in the order we use in the polynomial curve-fit evaluation
for ic = 1:nOrder+1                                         % looping coefficients
    consts.(list_abcd{ic})(s,l) = p_O(ic);                      % storing in consts
end

end % getPair_fitFromTable