function [Omegaijplusone,Omegaijplustwo] = ...
    getPair_Omegaijplus_Recurrence(T_sl,Omegaij,j,dOmegaij_dTsl,dOmegaij_dTsl2)
%getPair_Omegaijplus_Recurrence Calculate a higher order collision
% integral, i.e., (i,j+1) order, by the recurrence relation.
%
% Usage:
%   [Omegaijplusone,Omegaijplustwo] = ...
% getPair_Omegaijplus_Recurrence(T_sl,Omegaij,j,dOmegaij_dTsl,dOmegaij_dTsl2);
%
%   T_sl                temperature of the interaction pair according to
%                       appropriate definition of the collision integral.
%
%                       If an electron is a part of the (s,l)
%                       pair, use the governing T_el for the (s,l) spot
%                       in the N_spec by N_spec slice. If there is no
%                       electron for the (s,l) pair, use the governing
%                       TTrans for the heavy interaction.
%
%                       See Scoggins 2017 Eq. 4.12 for more information.
%
%                       size N_eta x N_spec x N_spec
%
%   Omegaij             collision integral of order (i,j)
%                       size N_eta x N_spec x N_spec
%   j                   order j of the collision integral Omegaij
%                       positive integer
%
%   d*_dTsl             derivative of * with respect to their appropriate
%                       temperature according to the (s,l) pair, T_sl
%
%                       size N_eta x N_spec x N_spec
%
%   d*_dTsl2            second derivative of * with respect to T_sl twice
%
%                       size N_eta x N_spec x N_spec
%
% References:
%   Scoggins, J. (2017). Development of numerical methods and
%   study of coupled flow, radiation, and ablation phenomena for
%   atmospheric entry. von Karman Institute.
%
% Author(s): Ethan Beyak
% GNU Lesser General Public License 3.0

% Equation (4.41) in Scoggins 2017 Thesis
% This is derived by differentiating the definition of the collision
% integral (Eq. 4.12) very carefully.

Omegaijplusone = Omegaij + T_sl./(j+2).*dOmegaij_dTsl;

Omegaijplustwo = Omegaijplusone + T_sl./(j+2).*dOmegaij_dTsl + ...
    T_sl.^2 /((j+3)*(j+2)).*dOmegaij_dTsl2;

end % getPair_Omegaijplus_Recurrence

