function Omega_ij = evalCapitelliFit_A2(a1,a2,a3,a4,a5,a6,a7,a8,a9,T)
% evalCapitelliFit_A2 evaluates the second curve fit in Capitelli's appendix.
%
% Usage:
%   (1)
%       Omega_ij = evalCapitelliFit_A2(a1,a2,a3,a4,a5,a6,a7,a8,a9,T)
%
% Inputs and outputs:
%   a1-a9       [Ang^2]         1 x 1
%       coefficients in the curve fit
%   T           [K]             N_eta x 1
%       temperature
%   Omega_ij    [m^2]           N_eta x 1
%       collision integral
%
% References:
%   [1] Capitelli, M., Gorse, C., Longo, S., & Giordano, D. (1998).
%   Transport properties of high temperature air species. AIAA-Paper,
%   98–2936. https://doi.org/10.2514/6.1998-2936
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

AngSq2mSq = 1e-20;
lnT = log(T);
Omega_Ang = (a3*lnT.^a6 .* exp((lnT-a1)/a2))./(exp((lnT-a1)/a2)+exp(-(lnT-a1)/a2)) + ...
             a7*exp(-((lnT-a8)/a9).^2) + a4*(1+lnT.^a5);
                    
Omega_ij = AngSq2mSq * Omega_Ang; % unit change

end % evalCapitelliFit_A2