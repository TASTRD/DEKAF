function [lnDelta1_sl,dlnDelta1sl_dT] = getPair_Collision_Delta1(TTrans,Mm_s,R0,lnOmega11_sl,varargin)
%GETPAIR_COLLISION_DELTA1 Calculate weighted collision Delta, order 1, as
% defined by Gupta Yos et al., 1990.
%
% Examples:
%   (1) lnDelta1_sl = getPair_Collision_Delta1(TTrans,Mm_s,R0,lnOmega11_sl);
%   (2) [lnDelta1_sl,dlnDelta1sl_dT] = getPair_Collision_Delta1(TTrans,Mm_s,R0,lnOmega11_sl,dlnOmega11sl_dT)
%
% Inputs:
%   T               -   equilibrium temperature, size (N_eta x 1)
%   Mm_s            -   species molecular mass, size (N_spec x 1)
%   R0              -   universal gas constant
%   lnOmega11_sl    -   natural logarithm of the (1,1) order collision
%                       integral, size (N_eta x N_s x N_l)
%   dlnOmega11sl_dT -   temperature derivative of lnOmega11_sl
%                       size (N_eta x N_s x N_l)
%
%   IMPORTANT NOTE      The units under the log of lnOmegaXX_sl should be
%                       SI, aka meters squared, else this function will not
%                       compute the correct value for lnDelta_sl.
%
% Outputs:
%   lnDelta1_sl     -   weighted collision cross-sectional area of order 1
%                       defined by GuptaYos 1990, Equation (34)
%                       size (N_eta x N_s x N_l)
%   dlnDelta1sl_dT  -   derivative of lnDelta1_sl with respect to T
%                       size (N_eta x N_s x N_l)
%
%  All of the outputs are passed as natural logarithms in order to avoid
%   MATLAB's integer overflow, similar to reaction equilibrium constants
%
% References:
% Gupta,  R. N.,  Yos,  J. M.,  Thompson,  R. A.,  and Lee,  K.-P.,  "A
%   Review of Reaction Rates and Thermodynamic and Transport Properties for
%   an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%   Calculations to 30000 K," Nasa-rp-1232, National Aeronautics and Space
%   Administration, 1990.
%
% Children and related functions:
%   See also getPair_Collision_constants.m, eval_Fit.m
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

N_eta = size(TTrans,1);
N_spec = size(Mm_s,1);

Mm_l    = repmat(reshape(Mm_s,[1,1,N_spec]),[N_eta,N_spec,1]); % Size (N_eta x N_s x N_l)
Mm_s    = repmat(reshape(Mm_s,[1,N_spec,1]),[N_eta,1,N_spec]); % Size (N_eta x N_s x N_l)
TTrans       = repmat(TTrans,[1,N_spec,N_spec]);

% Logarithms of Equations (34) in Gupta Yos 1990
lnDelta1_sl = log(8/3)  + 1/2*(log(2*Mm_s) + log(Mm_l) - log(pi*R0*TTrans) - log(Mm_s+Mm_l)) + log(pi) + lnOmega11_sl;

% Compute the temperature derivative if desired
if nargout>1
    ic = 1;
    dlnOmega11sl_dT = varargin{ic}; %ic = ic + 1;
    dlnDelta1sl_dT = -1./(2*TTrans) + dlnOmega11sl_dT;
end

end % getPair_Collision_Delta1.m
