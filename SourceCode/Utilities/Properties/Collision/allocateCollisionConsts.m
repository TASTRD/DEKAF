function consts = allocateCollisionConsts(N_spec,order)
% allocateCollisionConsts allocates the structure of collision curve-fit
% constants, by setting them all equal to zero.
%
% Usage:
%   (1)
%       consts = allocateCollisionConsts(N_spec,order)
%
% Inputs and outputs:
%   N_spec
%       number of species
%   order
%       order of the polynomial to be used. There will be order+1 fields in
%       the consts structure.
%   consts
%       structure with various matrices in .A, .B, .C, etc. The number of
%       fields depends on order.
%
% Author: Fernando Miro Miro
% Date: December 2019
% GNU Lesser General Public License 3.0

list_abcd = alphabet('capital');
zeroMat = zeros(N_spec,N_spec);
for ii=1:order+1
    consts.(list_abcd{ii}) = zeroMat;
end

end % allocateCollisionConsts