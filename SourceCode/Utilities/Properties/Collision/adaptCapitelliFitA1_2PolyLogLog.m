function [consts,errRes,Rsq] = adaptCapitelliFitA1_2PolyLogLog(a_vec,Trange,nOrder,consts,s,l,flag,varargin)
% adaptCapitelliFitA1_2PolyLogLog passes from the first curve fit in
% Capitelli's appendix to the poly-log-log used in DEKAF. 
%
% Usage:
%   (1)
%       [consts,errRes,Rsq] = adaptCapitelliFitA1_2PolyLogLog(a_vec,Trange,nOrder,consts,s,l,'')
%
%   (2)
%       [...] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts,s,l,'Cstar',a12_vec)
%       |--> computes the fit for Cstar instead of Omega, passing
%       Capitelli's curve-fit coefficients for O_11 and O_12
%
%   (3)
%       [...] = adaptCapitelliFitA1_2PolyLogLog(a11_vec,Trange,nOrder,consts,s,l,'Bstar',a12_vec,a13_vec)
%       |--> computes the fit for Bstar instead of Omega, passing
%       Capitelli's curve-fit coefficients for O_11, O_12 and O_13
%
% Inputs and outputs:
%   a_vec
%       contains the 6 curve-fit coefficients in Capitelli's appendix
%       (see <a href="matlab:help evalCapitelliFit_A1">evalCapitelliFit_A1</a>)
%   a11_vec, a12_vec, a13_vec
%       curve-fit coefficients in Capitelli's appendix for collision (1,1),
%       (1,2) and (1,3)
%   Trange
%       two-position vector with the minimum and maximum temperature to
%       consider.
%   nOrder
%       order of the poly-log-log fit
%   consts
%       structure with fields A, B, etc., with matrices for which position
%       (s,l) contains the coefficients for the polynomial log-log fit of
%       collision beetween particles s and l. See below for their
%       mathematical significance.
%       Position s,l is populated with the curve fit constants of the
%       collisional data provided.
%   s,l
%       identifier of the collision pair to be populated.
%   errRes
%       residual error of the fit
%   Rsq
%       R-squared value (coefficient of determination)
%
% References:
%   [1] Capitelli, M., Gorse, C., Longo, S., & Giordano, D. (1998).
%   Transport properties of high temperature air species. AIAA-Paper,
%   98–2936. https://doi.org/10.2514/6.1998-2936
%
% See also: evalCapitelliFit_A1
%
% Author: Fernando Miro Miro
% Date: July 2019
% GNU Lesser General Public License 3.0

a_cell = num2cell(a_vec);
switch flag
    case 'Bstar'
        a12_cell = num2cell(varargin{1});
        a13_cell = num2cell(varargin{2});
    case 'Cstar'
        a12_cell = num2cell(varargin{1});
end

 % from angstroms squared to meters squared
NT  = 1001;         % HARDCODE ALERT!!
T   = linspace(Trange(1),Trange(end),NT);                                   % temperature vector
O   = evalCapitelliFit_A1(a_cell{:},T);                                     % collision data O_ij
switch flag
    case 'Bstar'                                                            % we want to compute Bstar
    O_12    = evalCapitelliFit_A1(a12_cell{:},T);                               % collision data for O_12
    O_13    = evalCapitelliFit_A1(a13_cell{:},T);                               % collision data for O_13
    O       = (5*O_12 - 4*O_13)./O;                                             % computing Bstar
    case 'Cstar'                                                            % we want to compute Cstar
    O_12    = evalCapitelliFit_A1(a12_cell{:},T);                               % collision data for O_12
    O       = O_12./O;                                                          % computing Cstar
end

[p_O,errRes,Rsq] = generateFit_PolyLogLog(T,O,nOrder);                      % obtaining fit data

ABCD = alphabet('capital');                                                 % alphabetical list of letters
for ii=1:nOrder+1                                                           % looping polynomial order
    letter = ABCD{nOrder+2-ii};                                                 % obtaining the letter corresponding to it
    consts.(letter)(s,l) =  p_O(ii);                                            % populating constants structure
end

%%%% DEBUG PLOT
% N_spec = size(consts.A,1);
% lnProp_sl = eval_Fit_PolyLogLog(repmat(T',[1,N_spec,N_spec]),consts);
% O_fit = exp(lnProp_sl(:,s,l));
% switch flag
%     case 'Omega11';     figure; subplot(2,2,1);
%     case 'Omega22';             subplot(2,2,2);
%     case 'Bstar';               subplot(2,2,3);
%     case 'Cstar';               subplot(2,2,4);
% end
% loglog(T,O,    '-', 'displayname','Capitelli'); hold on;
% loglog(T,O_fit,'--','displayname','PolyLogLog'); hold on;
% xlabel('T [K]');
% switch flag
%     case 'Omega11';     ylabel('\Omega^{(1,1)} [m^2]');
%     case 'Omega22';     ylabel('\Omega^{(2,2)} [m^2]');
%     case 'Bstar';       ylabel('B^* [-]');
%     case 'Cstar';       ylabel('C^* [-]'); legend();
% end
% evalin('caller','suptitle([''Collision '',spec_list{s},''-'',spec_list{l}]);');
%%%% END

end % adaptCapitelliFitA1_2PolyLogLog