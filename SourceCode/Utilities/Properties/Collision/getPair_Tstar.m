function [Tstar,lambda_Debye] = getPair_Tstar(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,bPos)
% getPair_Tstar computes the reduced temperature of a mixture required
% for the evaluation of the collision integrals of charged-charged
% collisions.
%
% It will output a different reduced temperature to be used for each
% collision. The reasoning is that the temperature used for them should be
% different: collisions involving electrons should be governed by the
% electron temperature (as proposed by Scoggins 2014), whereas the others
% should be governed by the heavy temperature.
%
% IMPORTANT! For collisions (s,l) between non-charged species, the
% mass-averaged collision temperature (provided by getPair_T) is returned
% instead
%
%   Examples:
%       (1)     Tstar = getPair_Tstar(TTrans,Tel,y_s,rho,Mm_s,...
%                                   bElectron,kBoltz,nAvogadro,qEl,epsilon0)
%
%   Inputs and Outputs
%       TTrans          size N_eta x 1
%       Tel             size N_eta x 1
%       y_s             size N_eta x N_spec
%       rho             size N_eta x 1
%       Mm_s            size N_spec x 1
%       bElectron       size N_spec x 1
%       kBoltz          size 1 x 1
%       nAvogadro       size 1 x 1
%       qEl             size 1 x 1
%       epsilon0        size 1 x 1
%       bPos            size N_spec x 1
%       Tstar           size N_eta x N_spec x N_spec
%
% References:
% - Scoggins 2014, Development of Mutation++: MUlticomponent Thermodynamics
% And Transport properties for IONized gases library in C++
% - Scoggins Thesis, Development of numerical methods and study of coupled
% flow, radiation, and ablation phenomena for atmospheric entry
% - Mason 1967, Transport Coefficients of Ionized Gases
% - Fertig 2001, Transport Coefficients for High Temperature
% Nonequilibrium Air Flows
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% sizes
[N_eta,N_spec] = size(y_s);

% Obtaining Debye length
lambda_Debye = getMixture_lambdaDebye(y_s,TTrans,Tel,rho,Mm_s,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl);

% preparing 3D inputs
lambda_Debye3D = repmat(lambda_Debye,[1,N_spec,N_spec]);    % (eta,1) --> (eta,s,l)
T_sl = getPair_Tsl(TTrans,Tel,bElectron);                   % temperature for each collision (eta,s,l)
bCharged = getPair_bCharged(bPos,bElectron);
bCharged_3D = repmat(reshape(bCharged,[1,N_spec,N_spec]),[N_eta,1,1]);

chargedFact = lambda_Debye3D*kBoltz/(qEl^2/(4*pi*epsilon0)); % factor to be applied on T_sl for charged collisions
chargedFact(~bCharged_3D) = 1;                               % ... but not on non-charged ones (they work with simply T_sl)

Tstar = chargedFact .* T_sl;

end % getPair_Tstar