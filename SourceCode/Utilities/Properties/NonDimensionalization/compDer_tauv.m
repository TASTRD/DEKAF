function dZ_dtauv = compDer_tauv(dZ_dTv,dZ_dT,cptr,cpv,Tv_e)
%compDer_tauv Calculate the derivative of input Z with respect to
% nondimensionalized vibrational temperature, translating from physical to
% computational variables.
%
% Usage:
%   (1)
%       dZ_dtauv = compDer_tauv(dZ_dTv,dZ_dT,cptr,cpv,Tv_e)
%
% Inputs and outputs
%   dZ_dTv      vib-elec-el temperature derivative of a property (N_eta x whatever)
%   dZ_dT       temperature derivative of a property (N_eta x whatever)
%   cptr        trans-rot heat capacity (N_eta x whatever)
%   cpv         vib-elec-el heat capacity (N_eta x whatever)
%   Tv_e        edge vib-elec-el temperature (1 x 1)
%   dZ_dtauv    derivative of the variable with respect to tauv
%               (non-dimensional vibrational-electronic-electron enthalpy)
%               (N_eta x whatever)
%
% See also: compDer_all, compDer_g, compDer_df_deta, compDer_ym
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

dZ_dtauv = Tv_e * (dZ_dTv + cpv./cptr.*dZ_dT);

end % compDer_tauv