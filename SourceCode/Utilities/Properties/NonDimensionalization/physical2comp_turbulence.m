function [CT,a1T] = physical2comp_turbulence(C,a1,muT,PrT,mu,cp,kappa,options)
% physical2comp_turbulence modifies the necessary computational variables
% (C and a1) due to turbulence eddy viscosity.
%
% Usage:
%   (1)
%       [CT,a1T] = physical2comp_turbulence(C,a1,muT,PrT,mu,cp,kappa,options)
%
% Inputs:
%   C           [-]             N_eta x 1
%       Chapman-Rubesin parameter
%   a1          [-]             N_eta x 1
%       Chapman-Rubesin parameter divided by the Prandtl number
%   muT         [kg/m-s]        N_eta x 1
%       turbulent eddy viscosity
%   PrT         [-]             N_eta x 1
%       turbulent eddy prandtl number
%   mu          [kg/m-s]        N_eta x 1
%       (laminar) viscosity
%   cp          [J/kg-K]        N_eta x 1
%       specific heat capacity at constant pressure
%   kappa       [W/K-m]        N_eta x 1
%       thermal conductivity
%   options
%       classic DEKAF options structure
%
% Outputs:
%   CT          [-]             N_eta x 1
%       Chapman-Rubesin parameter modified by turbulence
%   a1T         [-]             N_eta x 1
%       Chapman-Rubesin parameter divided by the Prandtl number modified by
%       turbulence
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

protectedvalue(options,'flow',{'CPG','TPG'});

Pr = mu.*cp./kappa;

CT  = (1+muT./mu).*C;
a1T = (1+muT./mu .* Pr./PrT).*a1;

%%% DEBUGGING
%{
figure; Ni=1; Nj=3;ic=0;
eta = length(C):-1:1;
ic=ic+1; subplot(Ni,Nj,ic); plot(C, eta,'-',CT, eta,'--'); title('C');
ic=ic+1; subplot(Ni,Nj,ic); plot(a1,eta,'-',a1T,eta,'--'); title('a1'); 
ic=ic+1; subplot(Ni,Nj,ic); plot(mu,eta,'-',muT,eta,'--'); title('mu'); 
legend('laminar','turbulent');
%}
%%%

end % physical2comp_turbulence