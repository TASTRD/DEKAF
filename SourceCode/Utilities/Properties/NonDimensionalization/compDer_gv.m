function dZ_dgv = compDer_gv(dZ_dTv,dZ_dT,h_e,cptr,cpv)
%compDer_gv Calculate the derivative of input Z with respect to
% nondimensionalized vibrational enthalpy, translating from physical to
% computational variables.
%
% Usage:
%   (1)
%       dZ_dgv = compDer_gv(dZ_dTv,dZ_dT,h_e,cptr,cpv)
%
% Inputs and outputs
%   dZ_dT       temperature derivative of a property (N_eta x whatever)
%   dZ_dTv      vibrational temperature derivative of a property (N_eta x whatever)
%   h_e         edge static enthalpy (1 x 1)
%   cptr        heat capacity at constant pressure of the translational and
%               rotational energy modes (N_eta x 1)
%   cpv         heat capacity at constant pressure of the
%               vibrational-electronic-electron energy modes (N_eta x 1)
%   dZ_dgv      derivative of the variable with respect to gv
%               (non-dimensional vibrational-electronic-electron enthalpy)
%               (N_eta x whatever)
%
% See also: compDer_all, compDer_g, compDer_df_deta, compDer_ym, compDer_tauv
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

sizesl = size(dZ_dTv); % we obtain the size, to see if we have to repmat cp
cptr      = repmat(cptr,[1,sizesl(2:end)]); % (eta,1) --> (eta,whatever)
cpv       = repmat(cpv, [1,sizesl(2:end)]); % (eta,1) --> (eta,whatever)

dZ_dgv = h_e * (dZ_dTv ./ cpv - dZ_dT ./ cptr) ;

end % compDer_gv