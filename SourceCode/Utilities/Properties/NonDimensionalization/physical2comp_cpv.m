function [dCpv_dg,dCpv_ddfdeta,dCpv_dym,dCpv_dtauv] = physical2comp_cpv(dcpv_dTv,cpv_s,H_e,h_e,U_e,Tv_e,cptr,cpv,df_deta,h_s,options)
% physical2comp_cpv passes from the physical heat-capacity derivatives to
% their computational expressions.
%
% Usage:
%   (1)
%       [dCpv_dg,dCpv_ddfdeta,dCpv_dym,dCpv_dtauv] = ...
%                           physical2comp_cpv(dcpv_dTv,cpv_s,H_e,h_e,U_e,Tv_e,cptr,cpv,df_deta,h_s,options)
%
% Inputs and outputs:
%   dcpv_dTv                (N_eta x 1)                     [J/kg-K^2]
%       derivative of the heat capacity at constant pressure of the
%       vib-elec-el energy modes wrt the vib-elec-el temperature
%   cpv_s                   (N_eta x N_spec)                [J/kg-K]
%       species heat-capacity ratio at constant pressure of the vib-elec-el
%       energy modes
%   H_e                     (1 x 1)                         [J/kg]
%       total enthalpy at the boundary-layer edge
%   h_e                     (1 x 1)                         [J/kg]
%       static enthalpy at the boundary-layer edge
%   U_e                     (1 x 1)                         [m/s]
%       streamwise velocity at the boundary-layer edge
%   Tv_e                    (1 x 1)                         [K]
%       vib-elec-el temperature at the boundary-layer edge
%   cptr                    (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure of the translational-rotational
%       energy modes
%   cpv                     (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure of the vibrational-electronic-
%       electron enerrgy modes
%   df_deta                 (N_eta x 1)                     [-]
%       non-dimensional velocity (u/U_e)
%   h_s                     (N_eta x N_spec)                [J/kg]
%       species enthalpy
%   options
%       classic DEKAF options structure
%
% Author: Fernando Miro Miro
% Date: October 2019
% GNU Lesser General Public License 3.0

% Non-dimensionalizing variables
dCpv_dTv        = dcpv_dTv * Tv_e/h_e;
dCpv_dym_expl   = cpv_s * Tv_e/h_e;
dCpv_dT         = zeros(size(dcpv_dTv));

% Derivatives with respect to the computational variables
switch options.numberOfTemp
    case '2T'
        % Computing computational derivatives
        [dCpv_dg,dCpv_ddfdeta,dCpv_dym,dCpv_dtauv] = compDer_all(dCpv_dT,H_e,U_e,cptr,df_deta,dCpv_dym_expl,h_s,dCpv_dTv,cpv,Tv_e);
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

end % physical2comp_cpv