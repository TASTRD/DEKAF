function dZ_ddfdeta = compDer_df_deta(dZ_dT,U_e,cp,df_deta)
%COMPDER_DF_DETA Calculate the derivative of input Z with respect to
% nondimensionalized horizontal velocity component, translating from
% physical to computational variables.
%
% Usage:
%   (1)
%       dZ_ddfdeta = compDer_df_deta(dZ_dT,U_e,cp,df_deta)
%       |--> how to call the function for numberOfTemp='1T' - using cp
%
%   (2)
%       dZ_ddfdeta = compDer_df_deta(dZ_dT,U_e,cptr,df_deta)
%       |--> how to call the function for numberOfTemp='2T' - using cptr
%
% Inputs and outputs
%   dZ_dT       temperature derivative of a property (N_eta x whatever)
%   U_e         edge velocity (1 x 1)
%   cp          heat capacity at constant pressure (N_eta x 1)
%   cptr        heat capacity at constant pressure of the translational and
%               rotational energy modes (N_eta x 1)
%   df_deta     wall-normal derivative of the velocity self-similar
%               variable (N_eta x 1)
%   dZ_ddfdeta  derivative of the variable with respect to df_deta (N_eta x whatever)
%
% See also: compDer_all, compDer_g, compDer_ym, compDer_gv, compDer_tauv
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
% GNU Lesser General Public License 3.0

sizesl = size(dZ_dT); % we obtain the size, to see if we have to repmat df_deta and cp
df_deta = repmat(df_deta,   [1,sizesl(2:end)]); % (eta,1) --> (eta,whatever)
cp      = repmat(cp,        [1,sizesl(2:end)]); % (eta,1) --> (eta,whatever)

dZ_ddfdeta = -dZ_dT .* U_e.^2 ./ cp .* df_deta;

end % compDer_df_deta