function [varargout] = physical2comp_sourceGS(sourceGS,dsourceGS_dT,dsourceGS_dym,cp,h_s,df_deta,H_e,U_e,rho_e,mu_e,options,varargin)
% physical2comp_sourceGS passes from the physical gas-surface-interaction
% total mass source term and derivatives to its computational expressions.
%
% Usage:
%   (1)
%       [Mabl,dMabl_dg,dMabl_ddfdeta,dMabl_dym] = physical2comp_sourceGS(...
%                           sourceGS,dsourceGS_dT,dsourceGS_dym,cp,h_s,df_deta,...
%                           H_e,U_e,rho_e,mu_e,options)
%       |--> for options.numberOfTemp='1T'
%
%   (2)
%       [Mabl,dMabl_dg,dMabl_ddfdeta,dMabl_dym,dMabl_dtauv] = physical2comp_sourceGS(...
%                           sourceGS,dsourceGS_dT,dsourceGS_dym,cptr,h_s,df_deta,...
%                           H_e,U_e,rho_e,mu_e,options,cpv,Tv_e)
%       |--> for options.numberOfTemp='2T'
%
% Inputs and outputs:
%   sourceGS                (N_eta x 1)                     [kg/s-m^2]
%       physical mixture mass source term
%   dsourceGS_dT            (N_eta x N_T)                   [kg/s-m^2-K]
%       derivative of the physical mixture mass source term with the various
%       temperatures
%   dsourceGS_dym           (N_eta x N_spec(m))             [kg/s-m^2]
%       derivative of the physical mixture mass source term with the various
%       mass fractions
%   Mabl                    (N_eta x 1)                     [m-s/kg]
%       computational mixture mass source term
%   dMabl_dg                (N_eta x 1)                     [m-s/kg]
%       computational derivative with respect to g of the mixture mass source
%       term
%   dMabl_ddfdeta           (N_eta x 1)                     [m-s/kg]
%       computational derivative with respect to df_deta of the mixture mass
%       source term
%   dMabl_dtauv             (N_eta x 1)                     [m-s/kg]
%       computational derivative with respect to tauv of the mixture mass source
%       term
%   dMabl_dym               (N_eta x N_spec(m))             [m-s/kg]
%       computational derivative with respect to ym of the mixture mass source
%       term
%   cp                      (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure
%   cptr                    (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure of the translational-rotational
%       energy modes
%   cpv                     (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure of the vibrational-electronic-
%       electron enerrgy modes
%   h_s                     (N_eta x N_spec)                [J/kg]
%       species enthalpy
%   df_deta                 (N_eta x 1)                     [-]
%       non-dimensional velocity (u/U_e)
%   H_e                     (1 x 1)                         [J/kg]
%       total enthalpy at the boundary-layer edge
%   Tv_e                    (1 x 1)                         [K]
%       vib-elec-el temperature at the boundary-layer edge
%   U_e                     (1 x 1)                         [m/s]
%       streamwise velocity at the boundary-layer edge
%   rho_e                   (1 x 1)                         [kg/m^3]
%       mixture density at the boundary-layer edge
%   mu_e                    (1 x 1)                         [kg/m-s]
%       dynamic viscosity at the boundary-layer edge
%   options
%       classic DEKAF options structure
%
% Author: Fernando Miro Miro
% Date: December 2018
% GNU Lesser General Public License 3.0

switch options.numberOfTemp
    case '1T'
        % nothing to modify
    case '2T'
        dsourceGS_dTv = dsourceGS_dT(:,2);
        dsourceGS_dT(:,2) = [];
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

% Computational variables and their DIMENSIONAL derivatives
Mabl            = 1/(rho_e*mu_e*U_e)*sourceGS;
dMabl_dT        = 1/(rho_e*mu_e*U_e)*dsourceGS_dT;
dMabl_dym_expl  = 1/(rho_e*mu_e*U_e)*dsourceGS_dym;

varargout{1} = Mabl;
% Derivatives with respect to the computational variables
switch options.numberOfTemp
    case '1T'
        [varargout{2:nargout}] = compDer_all(dMabl_dT,H_e,U_e,cp,df_deta,dMabl_dym_expl,h_s);
    case '2T'
        % Additional arrangements depending on the number of temperatures
        dMabl_dTv       = 1/(rho_e*mu_e*U_e)*dsourceGS_dTv;
        % extracting variables from varargin (T_e)
        cpv     = varargin{1};
        Tv_e    = varargin{2};
        % Computing computational derivatives
        [varargout{2:nargout}] = compDer_all(dMabl_dT,H_e,U_e,cp,df_deta,dMabl_dym_expl,h_s,dMabl_dTv,cpv,Tv_e);
        % NOTE: cp and h_s are actually cptr and htr_s here
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

end % physical2comp_sourceGS