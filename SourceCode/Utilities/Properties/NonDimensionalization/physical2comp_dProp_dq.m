function [dC_dq,da1_dq,da2_dq,da0_dq,dj_dq,varargout] = physical2comp_dProp_dq(Pr,C,rho,mu,cp,kappa,...
                                        Ecu,Ecw,rho_e,mu_e,drho_dq,dmu_dq,dcp_dq,dkappa_dq,varargin)
% physical2comp_dProp_dq Calculates nondimensional coefficients' derivatives
%
% OLD FUNCTION !!! We no longer expand the properties around the base flow
%
% Example not multispecies:
% [dC_dq,da1_dq,da2_dq,da0_dq,dj_dq] = physical2comp_dProp_dq(Pr,C,rho,mu,...
%                   cp,kappa,Ecu,Ecw,rho_e,mu_e,drho_dq,dmu_dq,dcp_dq,dkappa_dq)
%
% Example multispecies:
% [dC_dq,da1_dq,da2_dq,da0_dq,dj_dq,dCs_dq,das1_dq,das2_dq] = ...
%       physical2comp_dProp_dq(Pr,C,rho,mu,cp,kappa,Ecu,Ecw,rho_e,mu_e,drho_dq,...
%                           dmu_dq,dcp_dq,dkappa_dq,D_s,dDs_dq,C_s,dhs_dq,g_s,a1,H_e)
%
% Note that dhs_dq for q=T will simply be the heat capacity at constant
% pressure of each species (cp_s).
%
% Also note that the inputs D_s, dDs_dq and C_s could be also D_sl, dDsl_dq
% and C_sl. In that case, the outputs dCs_dq and das2_dq would actually be
% dCsl_dq and dasl2_dq.
%
% Children and related functions:
%   See also listOfVariablesExplained, eval_thermo, setDefaults
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

switch nargin
    case 14 % number of inputs required for non-multispecies
        MultiSpec = false; % tracker
    case 21 % number of inputs required for multispecies
        MultiSpec = true; % tracker
        % additional inputs required: D_s, dDs_dq, C_s, dhs_dq, g_s, a1, H_e
        D_s     = varargin{1}; % it could actually be also D_sl, it doesn't really matter
        dDs_dq  = varargin{2};
        C_s     = varargin{3};
        dhs_dq  = varargin{4};
        g_s     = varargin{5};
        a1      = varargin{6};
        H_e     = varargin{7};
    otherwise
        error('Wrong number of inputs');
end

if size(drho_dq,2)>1
    error('Function not ready for multi-index derivatives (like d/dym)');
end

% Calculate derivatives
dC_dq   = 1/(rho_e*mu_e) * (dmu_dq.*rho + drho_dq.*mu);
dPr_dq  = mu./kappa.*dcp_dq + cp./kappa.*dmu_dq - cp.*mu./kappa.^2.*dkappa_dq;
da1_dq  = 1./Pr.*dC_dq - C./Pr.^2.*dPr_dq;
da2_dq  = Ecu/(1+Ecu/2)*(dC_dq-da1_dq);
da0_dq  = Ecw/(1+Ecu/2)*dC_dq;
dj_dq   = -rho_e./rho.^2.*drho_dq;

if MultiSpec                    % derivatives for multispecies
    sizesl = size(D_s); % size of the diffusion variable - either N_eta x N_spec or N_eta x N_spec x N_spec

    % repmatting mixture quantities depending on the size of the Diffusion
    % variable D_s (or D_sl). species-species diffusion will make the
    % repmatted variables 3D, whereas species-mixture diffusion will make
    % them 2D
    rho_mat         = repmat(rho,       [1,sizesl(2:end)]);
    mu_mat          = repmat(mu,        [1,sizesl(2:end)]);
    drho_dq_mat     = repmat(drho_dq,   [1,sizesl(2:end)]);
    dmu_dq_mat      = repmat(dmu_dq,    [1,sizesl(2:end)]);
    C_mat           = repmat(C,         [1,sizesl(2:end)]);
    dC_dq_mat       = repmat(dC_dq,     [1,sizesl(2:end)]);
    a1_mat          = repmat(a1,        [1,sizesl(2:end)]);
    da1_dq_mat      = repmat(da1_dq,    [1,sizesl(2:end)]);

    % Computing derivatives
    Sc_s        = mu_mat./(rho_mat.*D_s); % in fact this is Sc_s or Sc_sl, depending on what the user passes
    dgs_dq      = dhs_dq/H_e;
    dScs_dq     = 1./(rho_mat.*D_s).*dmu_dq_mat - mu_mat./(rho_mat.^2.*D_s).*drho_dq_mat - mu_mat./(rho_mat.*D_s.^2).*dDs_dq;

    dCs_dq      = dC_dq_mat./Sc_s - C_mat./Sc_s.^2.*dScs_dq; % again, this could be indistinctly dCs_dq or dCsl_dq
    das1_dq     = 1/(1+Ecu/2) * (dgs_dq.*a1_mat + da1_dq_mat.*g_s);
    das2_dq     = 1/(1+Ecu/2) * (dgs_dq.*C_s + dCs_dq.*g_s); % FIXME: this expression may not be true for species-species diffusion

    % preparing outputs
    varargout{1} = dCs_dq;
    varargout{2} = das1_dq;
    varargout{3} = das2_dq;
end

end

