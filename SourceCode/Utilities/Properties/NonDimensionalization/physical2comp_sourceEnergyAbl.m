function [varargout] = physical2comp_sourceEnergyAbl(sourceAbl,dsourceAbl_dT,dsourceAbl_dym,cp,h_s,df_deta,H_e,U_e,rho_e,mu_e,options,varargin)
% physical2comp_sourceEnergyAbl passes from the physical ablative surface
% energy source term and derivatives to its computational expressions.
%
% Usage:
%   (1)
%       [OmegaAbl,dOmegaAbl_dg,dOmegaAbl_ddfdeta,dOmegaAbl_dym] = physical2comp_sourceEnergyAbl(...
%                           sourceAbl,dsourceAbl_dT,dsourceAbl_dym,cp,h_s,df_deta,...
%                           H_e,U_e,rho_e,mu_e,options)
%       |--> for options.numberOfTemp='1T'
%
%   (2)
%       [OmegaAbl,dOmegaAbl_dg,dOmegaAbl_ddfdeta,dOmegaAbl_dym,dOmegaAbl_dtauv] = physical2comp_sourceEnergyAbl(...
%                           sourceAbl,dsourceAbl_dT,dsourceAbl_dym,cptr,h_s,df_deta,...
%                           H_e,U_e,rho_e,mu_e,options,cpv,Tv_e)
%       |--> for options.numberOfTemp='2T'
%
% Inputs and outputs:
%   sourceAbl               (N_eta x 1)                     [kg/s-m^2]
%       physical energy source term
%   dsourceAbl_dT           (N_eta x N_T)                   [kg/s-m^2-K]
%       derivative of the physical energy source term with the various
%       temperatures
%   dsourceAbl_dym          (N_eta x N_spec(m))             [kg/s-m^2]
%       derivative of the physical energy source term with the various
%       mass fractions
%   OmegaAbl                  (N_eta x 1)                     [m-s/kg]
%       computational mixture mass source term
%   dOmegaAbl_dg              (N_eta x 1)                     [m-s/kg]
%       computational derivative with respect to g of the energy source
%       term
%   dOmegaAbl_ddfdeta         (N_eta x 1)                     [m-s/kg]
%       computational derivative with respect to df_deta of the energy
%       source term
%   dOmegaAbl_dtauv           (N_eta x 1)                     [m-s/kg]
%       computational derivative with respect to tauv of the energy source
%       term
%   dOmegaAbl_dym             (N_eta x N_spec(m))             [m-s/kg]
%       computational derivative with respect to ym of the energy source
%       term
%   cp                      (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure
%   cptr                    (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure of the translational-rotational
%       energy modes
%   cpv                     (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure of the vibrational-electronic-
%       electron enerrgy modes
%   h_s                     (N_eta x N_spec)                [J/kg]
%       species enthalpy
%   df_deta                 (N_eta x 1)                     [-]
%       non-dimensional velocity (u/U_e)
%   H_e                     (1 x 1)                         [J/kg]
%       total enthalpy at the boundary-layer edge
%   Tv_e                    (1 x 1)                         [K]
%       vib-elec-el temperature at the boundary-layer edge
%   U_e                     (1 x 1)                         [m/s]
%       streamwise velocity at the boundary-layer edge
%   rho_e                   (1 x 1)                         [kg/m^3]
%       mixture density at the boundary-layer edge
%   mu_e                    (1 x 1)                         [kg/m-s]
%       dynamic viscosity at the boundary-layer edge
%   options
%       classic DEKAF options structure
%
% Author: Fernando Miro Miro
% Date: December 2018
% GNU Lesser General Public License 3.0

switch options.numberOfTemp
    case '1T'
        % nothing to modify
    case '2T'
        dsourceAbl_dTv = dsourceAbl_dT(:,2);
        dsourceAbl_dT(:,2) = [];
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

% Computational variables and their DIMENSIONAL derivatives
frac = 1./(U_e*H_e*mu_e*rho_e);
OmegaAbl            = frac*sourceAbl;
dOmegaAbl_dT        = frac*dsourceAbl_dT;
dOmegaAbl_dym_expl  = frac*dsourceAbl_dym;

varargout{1} = OmegaAbl;
% Derivatives with respect to the computational variables
switch options.numberOfTemp
    case '1T'
        [varargout{2:nargout}] = compDer_all(dOmegaAbl_dT,H_e,U_e,cp,df_deta,dOmegaAbl_dym_expl,h_s);
    case '2T'
        % Additional arrangements depending on the number of temperatures
        dOmegaAbl_dTv       = frac*dsourceAbl_dTv;
        % extracting variables from varargin (T_e)
        cpv     = varargin{1};
        Tv_e    = varargin{2};
        % Computing computational derivatives
        [varargout{2:nargout}] = compDer_all(dOmegaAbl_dT,H_e,U_e,cp,df_deta,dOmegaAbl_dym_expl,h_s,dOmegaAbl_dTv,cpv,Tv_e);
        % NOTE: cp and h_s are actually cptr and htr_s here
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

end % physical2comp_sourceEnergyAbl