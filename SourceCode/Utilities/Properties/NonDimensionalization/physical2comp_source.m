function [varargout] = physical2comp_source(source_s,dsources_dT,dsources_dym,TTrans,Tel,p,y_s,cp,h_s,df_deta,R0,Mm_s,bElectron,H_e,U_e,rho_e,mu_e,options,varargin)
% physical2comp_source passes from the physical species source term and
% derivatives to its computational expressions.
%
% Usage:
%   (1)
%       [Omega_s,dOmegas_dg,dOmegas_ddfdeta,dOmegas_dym] = physical2comp_source(...
%                           source_s,dsources_dT,dsources_dym,TTrans,Tel,p,y_s,cp,...
%                           h_s,df_deta,R0,Mm_s,bElectron,H_e,U_e,rho_e,mu_e,options)
%       |--> for options.numberOfTemp='1T'
%
%   (2)
%       [Omega_s,dOmegas_dg,dOmegas_ddfdeta,dOmegas_dym,dOmegas_dtauv] = physical2comp_source(...
%                           source_s,dsources_dT,dsources_dym,TTrans,Tel,p,y_s,cptr,...
%                           h_s,df_deta,R0,Mm_s,bElectron,H_e,U_e,rho_e,mu_e,options,cpv,Tv_e)
%       |--> for options.numberOfTemp='2T'
%
% Inputs and outputs:
%   source_s                (N_eta x N_spec)                [kg/s]
%       physical species source term
%   dsources_dT             (N_eta x N_spec x N_T)          [kg/s-K]
%       derivative of the physical species source term with the various
%       temperatures
%   dsources_dym            (N_eta x N_spec(m) x N_spec(s)) [kg/s]
%       derivative of the physical species source term with the various
%       mass fractions
%   Omega_s                 (N_eta x N_spec)                [m^2-s^2/kg^2]
%       computational species source term
%   dOmegas_dg              (N_eta x N_spec)                [m^2-s^2/kg^2]
%       computational derivative with respect to g of the species source
%       term
%   dOmegas_ddfdeta         (N_eta x N_spec)                [m^2-s^2/kg^2]
%       computational derivative with respect to df_deta of the species
%       source term
%   dOmegas_dtauv             (N_eta x N_spec)                [m^2-s^2/kg^2]
%       computational derivative with respect to tauv of the species source
%       term
%   dOmegas_dym             (N_eta x N_spec(s) x N_spec(m)) [m^2-s^2/kg^2]
%       computational derivative with respect to ym of the species source
%       term
%   TTrans                  (N_eta x 1)                     [K]
%       translational temperature
%   Tel                     (N_eta x 1)                     [K]
%       electron temperature
%   p                       (N_eta x 1)                     [Pa]
%       pressure
%   y_s                     (N_eta x N_spec)                [-]
%       species mass fraction
%   cp                      (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure
%   cptr                    (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure of the translational-rotational
%       energy modes
%   cpv                     (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure of the vibrational-electronic-
%       electron enerrgy modes
%   h_s                     (N_eta x N_spec)                [J/kg]
%       species enthalpy
%   df_deta                 (N_eta x 1)                     [-]
%       non-dimensional velocity (u/U_e)
%   R0                      (1 x 1)                         [J/kg-K]
%       universal gas constant
%   Mm_s                    (N_spec x 1)                    [kg/mol]
%       species molar mass
%   bElectron               (N_spec x 1)                    [-]
%       species electron boolean
%   H_e                     (1 x 1)                         [J/kg]
%       total enthalpy at the boundary-layer edge
%   Tv_e                    (1 x 1)                         [K]
%       vib-elec-el temperature at the boundary-layer edge
%   U_e                     (1 x 1)                         [m/s]
%       streamwise velocity at the boundary-layer edge
%   rho_e                   (1 x 1)                         [kg/m^3]
%       mixture density at the boundary-layer edge
%   mu_e                    (1 x 1)                         [kg/m-s]
%       dynamic viscosity at the boundary-layer edge
%   options
%       classic DEKAF options structure
%
% Author: Fernando Miro Miro
% Date: December 2018
% GNU Lesser General Public License 3.0

options.EoS = protectedvalue(options,'EoS','idealGas');

[~,N_spec] = size(y_s);
rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);
drho_dym = getMixtureDer_drho_dym(p,y_s,R0,Mm_s,bElectron,TTrans,Tel);
drho_dT = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,[TTrans,Tel],options.numberOfTemp);

switch options.numberOfTemp
    case '1T'
        % nothing to modify
    case '2T'
        dsources_dTv = dsources_dT(:,:,2);
        dsources_dT(:,:,2) = [];
        drho_dTv = drho_dT(:,2);
        drho_dT(:,2) = [];
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

% Resizing
rho_2D = repmat(rho,[1,N_spec]);                            % (eta,1) --> (eta,s)
drho_dT = repmat(drho_dT,[1,N_spec]);                       % (eta,1) --> (eta,s)
rho_3D = repmat(rho,[1,N_spec,N_spec]);                     % (eta,1) --> (eta,s,m)
drho_dym = repmat(permute(drho_dym,[1,3,2]),[1,N_spec,1]);  % (eta,m) --> (eta,s,m)
sources_3D = repmat(source_s,[1,1,N_spec]);                 % (eta,s) --> (eta,s,m)
dsources_dym_esm = permute(dsources_dym,[1,3,2]);           % (eta,m,s) --> (eta,s,m)

% Computational variables and their DIMENSIONAL derivatives
Omega_s = source_s./(rho_2D*rho_e*mu_e*U_e^2);
dOmegas_dT = 1/(rho_e*mu_e*U_e^2)*( 1./rho_2D.*dsources_dT - source_s./(rho_2D.^2).*drho_dT );
dOmegas_dym_expl= 1/(rho_e*mu_e*U_e^2)*( 1./rho_3D.*dsources_dym_esm - sources_3D./(rho_3D.^2).*drho_dym );

varargout{1} = Omega_s;
% Derivatives with respect to the computational variables
switch options.numberOfTemp
    case '1T'
        [varargout{2:nargout}] = compDer_all(dOmegas_dT,H_e,U_e,cp,df_deta,dOmegas_dym_expl,h_s);
    case '2T'
        % Additional arrangements depending on the number of temperatures
        drho_dTv = repmat(drho_dTv,[1,N_spec]); % (eta,1) --> (eta,s)
        dOmegas_dTv = 1/(rho_e*mu_e*U_e^2)*( 1./rho_2D.*dsources_dTv - source_s./(rho_2D.^2).*drho_dTv );
        % extracting variables from varargin (Tv_e)
        cpv     = varargin{1};
        Tv_e    = varargin{2};
        % Computing computational derivatives
        [varargout{2:nargout}] = compDer_all(dOmegas_dT,H_e,U_e,cp,df_deta,dOmegas_dym_expl,h_s,dOmegas_dTv,cpv,Tv_e);
        % NOTE: cp and h_s are actually cptr and htr_s here
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

