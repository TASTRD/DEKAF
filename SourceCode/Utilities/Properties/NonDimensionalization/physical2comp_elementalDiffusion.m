function [agE,auE,aYE,CYEF] = physical2comp_elementalDiffusion(rho,A_E,C_EF,a_s1,C_sl,dXs_dyE,dys_dyE,cp_eq,h_s,U_e,H_e,rho_e,mu_e,bPairDiff)
% physical2comp_elementalDiffusion obtains the computational variables from
% the physical, related to elemental-diffusion terms.
%
% Usage:
%   (1) ---> for species-to-species molar diffusion
%       [agE,auE,aYE,CYEF] = physical2comp_elementalDiffusion(rho,A_E,C_EF,a_s1,C_sl,dXs_dyE,dys_dyE,cp_eq,h_s,U_e,H_e,rho_e,mu_e,true)
%
%   (2) ---> for species-to-species mass diffusion
%       [agE,auE,aYE,CYEF] = physical2comp_elementalDiffusion(rho,A_E,C_EF,a_s1,C_sl,dys_dyE,dys_dyE,cp_eq,h_s,U_e,H_e,rho_e,mu_e,true)
%
%   (3) ---> for species-to-mixture molar diffusion
%       [agE,auE,aYE,CYEF] = physical2comp_elementalDiffusion(rho,A_E,C_EF,a_s1,C_s, dXs_dyE,dys_dyE,cp_eq,h_s,U_e,H_e,rho_e,mu_e,false)
%
%   (4) ---> for species-to-mixture mass diffusion
%       [agE,auE,aYE,CYEF] = physical2comp_elementalDiffusion(rho,A_E,C_EF,a_s1,C_s, dys_dyE,dys_dyE,cp_eq,h_s,U_e,H_e,rho_e,mu_e,false)
%
% Inputs and outputs:
%   rho             [kg/m^3]    N_eta x 1
%       mixture density
%   A_E             [kg/m-s-K]  N_eta x N_elem
%       elemental thermodiffusion coefficients
%   C_EF            [kg/m-s]    N_eta x N_elem(E) x N_elem(F)
%       elemental molar diffusion coefficients
%   dXs_dyE         [-]         N_eta x N_spec x N_elem
%       gradient of the equilibrium mole fraction of each species (s) with
%       each elemental mass fracion (E)
%   dys_dyE         [-]         N_eta x N_spec x N_elem
%       gradient of the equilibrium mass fraction of each species (s) with
%       each elemental mass fracion (E)
%   cp_eq           [J/kg-K]    N_eta x 1
%       equilibrium heat capacity
%   h_s             [J/kg]      N_eta x N_spec
%       species enthalpies
%   U_e             [m/s]       1 x 1
%       streamwise velocity at the edge
%   H_e             [J/kg]      1 x 1
%       semi-total enthalpy at the edge
%   rho_e           [kg/m^3]    1 x 1
%       mixture density at the edge
%   mu_e            [kg/m-s]    1 x 1
%       viscosity at the edge
%   C_sl            [-]         N_eta x N_spec(s) x N_spec(l)
%       non-dimensional multicomponent diffusion coefficient (as obtained 
%       by <a href="matlab:help physical2comp_props">physical2comp_props</a> for spec-spec diffusion)
%   C_s             [-]         N_eta x N_spec
%       non-dimensional effective diffusion coefficient (as obtained by 
%       <a href="matlab:help physical2comp_props">physical2comp_props</a> for spec-mix diffusion)
%   a_s1             [-]         N_eta x N_spec
%       non-dimensional energy coefficient (as obtained by <a href="matlab:help physical2comp_props">physical2comp_props</a>)
%   agE             [-]         N_eta x N_elem
%       non-dimensional grouping variable multiplying dg_deta in the
%       elemental-conservation equation
%   auE             [-]         N_eta x N_elem
%       non-dimensional grouping variable multiplying df_deta2 in the
%       elemental-conservation equation
%   aYE             [-]         N_eta x N_elem
%       non-dimensional grouping variable multiplying dyE_deta in the
%       energy equation
%   CYEF            [-]         N_eta x N_elem(E) x N_elem(F)
%       non-dimensional grouping variable multiplying dyF_deta in the
%       elemental-conservation equation
%
% NOTE: aYE corresponds to: aD2_E - aY1_E, and CYEF corresponds to: cY_EF -
% aY_EF as appearing in the DEKAF 2020 technical report
% 
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,N_spec,N_elem] = size(dys_dyE);

% Coefficients for the elemental-conservation equation
cp_eq_2D = repmat(cp_eq,[1,N_elem]); % (eta,1) --> (eta,E)
rho_2D   = repmat(rho,  [1,N_elem]); % (eta,1) --> (eta,E)

agE = A_E.*rho_2D./(rho_e*mu_e) .* H_e  ./cp_eq_2D;
auE = A_E.*rho_2D./(rho_e*mu_e) .* U_e^2./cp_eq_2D;

h_s_3D      = repmat(permute(h_s,    [1,3,2]),[1,N_elem]);  % (eta,s) ----> (eta,F,s)
dys_dyF_3D  =        permute(dys_dyE,[1,3,2]);              % (eta,s,F) --> (eta,F,s)
sum_hsdysdyF = sum(h_s_3D.*dys_dyF_3D , 3);                 % (eta,F)

A_E_3D          = repmat(        A_E,                   [1,1,     N_elem]); % (eta,E) --> (eta,E,F)
rho_3D          = repmat(        rho,                   [1,N_elem,N_elem]); % (eta,1) --> (eta,E,F)
cp_eq_3D        = repmat(        cp_eq,                 [1,N_elem,N_elem]); % (eta,1) --> (eta,E,F)
sum_hsdysdyF_3D = repmat(permute(sum_hsdysdyF,[1,3,2]), [1,N_elem,1]);      % (eta,F) --> (eta,E,F)

CYEF = rho_3D./(rho_e*mu_e) .* (C_EF - A_E_3D.*sum_hsdysdyF_3D./cp_eq_3D); % (eta,E,F)

% Coefficients for the energy equation
for E=N_elem:-1:1
    aY1_E(:,E) = (a_s1.*dys_dyE(:,:,E))*ones(N_spec,1);
end
if bPairDiff                                                    % spec-spec diffusion
    C_sl_4D     = repmat(permute(C_sl,   [1,4,2,3]),[1,N_elem,1,1]); % (eta,s,l) --> (eta,E,s,l)
    dXl_dyE_4D  = repmat(permute(dXs_dyE,[1,3,4,2]),[1,1,N_spec,1]); % (eta,l,E) --> (eta,E,s,l)
    aD2_E = sum(sum(C_sl_4D.*dXl_dyE_4D , 3) , 4);                   % (eta,E)
else                                                            % spec-mix diffusion
    C_s_3D      = repmat(permute(C_sl,   [1,3,2]),[1,N_elem,1,1]);  % (eta,s,l) --> (eta,E,s)
    dXs_dyE_3D  =        permute(dXs_dyE,[1,3,2]);                  % (eta,s,E) --> (eta,E,s)
    aD2_E = sum(C_s_3D.*dXs_dyE_3D , 3);                            % (eta,E)
end
aYE = aD2_E - aY1_E;                                                % assembling

end % physical2comp_elementalDiffusion