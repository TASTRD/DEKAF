function [dZ_dg,dZ_ddfdeta,varargout] = compDer_all(dZ_dT,H_e,U_e,cp,df_deta,varargin)
%compDer_all Calculate all derivatives of input Z with respect to the
% computational variables.
%
% Usage:
%   (1)
%       [dZ_dg,dZ_ddfdeta] = compDer_all(dZ_dT,H_e,U_e,cp,df_deta)
%       |--> If ym is not a system variable
%
%   (2)
%       [dZ_dg,dZ_ddfdeta,dZ_dym] = compDer_all(dZ_dT,H_e,U_e,cp,df_deta,...
%                                                           dZ_dym_expl,h_m)
%       |--> If ym is a system variable
%
%   (3)
%       [dZ_dg,dZ_ddfdeta,dZ_dym,dZ_dtauv] = compDer_all(dZ_dT,H_e,U_e,cptr,...
%                               df_deta,dZ_dym_expl,h_m,dZ_dTv,cpv,Tv_e)
%       |--> If we also have a two-temperature model
%
% Inputs and outputs
%   dZ_dT       temperature derivative of a property (N_eta x whatever)
%   dZ_dym_expl explicit derivative of a property Z with repect to its mass
%               fractions (N_eta x whatever x N_spec)
%   dZ_dTv      vibrational temperature derivative of a property (N_eta x whatever)
%   H_e         edge total enthalpy (1 x 1)
%   U_e         edge velocity (1 x 1)
%   Tv_e        edge vib-elec-el temperature (1 x 1)
%   cp          heat capacity at constant pressure (N_eta x 1)
%   cptr        heat capacity at constant pressure of the translational and
%               rotational energy modes (N_eta x 1)
%   cpv         heat capacity at constant pressure of the vibrational,
%               electronic and electron energy modes (N_eta x 1)
%   h_m         species enthalpy (N_eta x N_spec)
%   df_deta     wall-normal derivative of the velocity self-similar
%               variable (N_eta x 1)
%   dZ_dg       derivative of the variable with respect to g
%               (non-dimensional total enthalpy) (N_eta x whatever)
%   dZ_ddfdeta  derivative of the variable with respect to df_deta (N_eta x whatever)
%   dZ_dym      derivative of the variable with respect to the mass
%               fractions including also implicit derivatives
%               (N_eta x whatever x N_spec)
%   dZ_dtauv    derivative of the variable with respect to tauv
%               (non-dimensional vibrational-electronic-electron enthalpy)
%               (N_eta x whatever)
%
% See also: compDer_g, compDer_df_deta, compDer_ym, compDer_tauv
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

dZ_dg = compDer_g(dZ_dT,H_e,cp);
dZ_ddfdeta = compDer_df_deta(dZ_dT,U_e,cp,df_deta);

switch length(varargin)
    case 0
        varargout{1} = [];
        varargout{2} = [];
    case 2                          % ym and 1T
        dZ_dym_expl = varargin{1};
        h_m = varargin{2};
        dZ_dym = compDer_ym(dZ_dym_expl,h_m,cp,dZ_dT);
        varargout{1} = dZ_dym;
        varargout{2} = zeros(size(dZ_dg));
    case 5                          % ym and 2T
        dZ_dym_expl = varargin{1};
        h_m         = varargin{2};
        dZ_dTv      = varargin{3};
        cpv         = varargin{4};
        Tv_e        = varargin{5};
        cptr = cp;
        dZ_dym = compDer_ym(dZ_dym_expl,h_m,cptr,dZ_dT);
        dZ_dtauv = compDer_tauv(dZ_dTv,dZ_dT,cptr,cpv,Tv_e);
        varargout{1} = dZ_dym;
        varargout{2} = dZ_dtauv;
    otherwise
        error(['the number of inputs (',num2str(nargin),') is not supported']);
end

end % compDer_all