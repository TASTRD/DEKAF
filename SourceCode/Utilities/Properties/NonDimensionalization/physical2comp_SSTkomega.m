function [AK,AW,BW,aK0,aK1,aK2,aW0,aW1,aW2] = physical2comp_SSTkomega(rho,mu,muT,wT,betaK,betaW,...
                        sigmaK,sigmaW,alphaW,gammaT,rho_e,mu_e,U_e,W_0,kT_e,wT_e,t,sourceScaleVol)
% physical2comp_SSTkomega returns the computational variables needed for
% the two additional conservation equations in the SST k-omega turbulence
% model.
%
% Usage:
%   (1)
%       [AK,AW,BW,aK0,aK1,aK2,aW0,aW1,aW2] = physical2comp_SSTkomega(rho,mu,muT,wT,betaK,betaW,...
%                   sigmaK,sigmaW,alphaW,gammaT,rho_e,mu_e,U_e,W_0,kT_e,wT_e,t,sourceScaleVol)
%
% Inputs:
%   rho         [kg/m^3]                                (N_eta x 1)
%       mixture density
%   mu          [kg/m-s]                                (N_eta x 1)
%       laminar viscosity
%   muT         [kg/m-s]                                (N_eta x 1)
%       turbulent viscosity
%   wT          [1/s]                                   (N_eta x 1)
%       turbulent vorticity
%   betaK, betaW, sigmaK, sigmaW, alphaW, gammaT        (N_eta x 1)
%       parameters appearing in the turbulent-kinetic-energy and vorticity
%       equations.
%   rho_e       [kg/m^3]                                (1 x 1)
%       edge density
%   mu_e        [kg/m-s]                                (1 x 1)
%       edge viscosity
%   U_e         [m/s]                                   (1 x 1)
%       edge streamwise velocity
%   W_0         [m/s]                                   (1 x 1)
%       edge spanwise velocity
%   kT_e        [J/kg]                                  (1 x 1)
%       edge turbulent kinetic energy
%   wT_e        [1/s]                                   (1 x 1)
%       edge turbulent vorticity energy
%   t           [-]                                     (N_eta x 1)
%       curvature term
%   sourceScaleVol [-]                                  (N_eta x 1)
%       curvature term for volumetric source terms
%
% Outputs:
%   AK, AW, BW, aK0, aK1, aK2, aW0, aW1, aW2
%       non-dimensional grouping parameters in the turbulent-kinetic-energy
%       and vorticity-conservation equations in the SSTkomega model.
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

C  = rho.*mu ./(rho_e*mu_e);                        % Chapman-Rubesin parameters
CT = rho.*muT./(rho_e*mu_e);

AK = betaK*wT_e/(rho_e*mu_e*U_e^2) .* sourceScaleVol;
AW = betaW*wT_e/(rho_e*mu_e*U_e^2) .* sourceScaleVol;
BW = rho.^2.*alphaW./wT .* kT_e/(rho_e.*mu_e) .* t.^2;

aK0 = W_0^2/kT_e * CT;
aK1 = U_e^2/kT_e * CT;
aK2 = C + sigmaK.*CT;

aW0 = W_0^2/wT_e .* rho.*gammaT./muT .* CT;
aW1 = U_e^2/wT_e .* rho.*gammaT./muT .* CT;
aW2 = C + sigmaW.*CT;

end % physical2comp_SSTkomega