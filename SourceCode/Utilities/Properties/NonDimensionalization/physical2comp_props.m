function [C,a1,a2,a0,j,varargout]...
    = physical2comp_props(rho,mu,cp_in,kappa_in,Ecu,Ecw,rho_e,mu_e,h_e,Tv_e,t,varargin)
%PHYSICAL2COMP_PROPS Calculate nondimensional, computational properties
%  from given dimensional, physical quantities
%
% Usage:
%   (1.1) [C,a1,a2,a0,j] = physical2comp_props(rho,mu,cp,kappa,Ecu,Ecw,rho_e,mu_e,h_e,Tv_e,t);
%       |--> without multispecies terms
%
%   (1.2) [C,a1,a2,a0,j,C_s,a_s1,a_s2,zeta_s] = physical2comp_terms(rho,mu, ...
%       |             cp,kappa,Ecu,Ecw,rho_e,mu_e,h_e,Tv_e,t,D_s,h_s,Mm,Mm_s,false);
%       |--> with spec-mix terms
%
%   (1.3) [C,a1,a2,a0,j,C_sl,a_s1,a_sl2,zeta_s] = physical2comp_terms(rho,mu, ...
%       |             cp,kappa,Ecu,Ecw,rho_e,mu_e,h_e,Tv_e,t,D_sl,h_s,Mm,Mm_s,true,y_s);
%       |--> with spec-spec terms
%
%   (2.1) [C,a1,a2,a0,j,av1] = physical2comp_props(rho,mu,{cp,cpv},{kappa,kappav},...
%       |                                                   Ecu,Ecw,rho_e,mu_e,h_e,Tv_e,t);
%       |--> two-temperature model without multispecies terms
%
%   (2.2) [C,a1,a2,a0,j,C_s,a_s1,a_s2,zeta_s,av1,av2,Cpv,av_s2] = physical2comp_terms(rho,mu, ...
%       |             {cptr,cpv},{kappatr,kappav},Ecu,Ecw,rho_e,mu_e,h_e,Tv_e,t,D_s,{htr_s,hv_s},Mm,Mm_s,nTfrac_s,false,cpv_s);
%       |--> two-temperature model with spec-mix terms
%
%   (2.3) [C,a1,a2,a0,j,C_sl,a_s1,a_sl2,zeta_s,av1,av2,Cpv,av_sl2] = physical2comp_terms(rho,mu, ...
%       |             {cptr,cpv},{kappatr,kappav},Ecu,Ecw,rho_e,mu_e,h_e,Tv_e,t,D_sl,{htr_s,hv_s},Mm,Mm_s,nTfrac_s,true,y_s,cpv_s);
%       |--> two-temperature model with spec-spec terms
%
% The false/true boolean tells us if we are doing spec-mix or spec-spec
% diffusion.
%
% For a detailed definition of what all of thtese coefficients are, the
% best reference for now is:
%
%   Miró Miró, F., Beyak, E. S., Mullen, D., Pinna, F., & Reed, H. L.
% (2018). Ionization and Dissociation Effects on Hypersonic Boundary-Layer
% Stability. In 31st Congress of the International Council of the
% Aeronautical Sciences. Belo Horizonte, Brazil.
%
%   Inputs:
%       rho         size N_eta x 1
%       mu          size N_eta x 1
%       cp          size N_eta x 1
%       cptr        size N_eta x 1
%       cpv         size N_eta x 1
%       kappa       size N_eta x 1
%       kappatr     size N_eta x 1
%       kappav      size N_eta x 1
%       Ecu         size 1 x 1
%       Ecw         size 1 x 1
%       rho_e       size 1 x 1
%       mu_e        size 1 x 1
%       h_e         size 1 x 1
%       Tv_e        size 1 x 1
%       t           size N_eta x 1 (transverse curvature term)
%       D_s         size N_eta x N_spec
%       D_sl        size N_eta x N_spec x N_spec
%       h_s         size N_eta x N_spec
%       htr_s       size N_eta x N_spec
%       hv_s        size N_eta x N_spec
%       cpv_s       size N_eta x N_spec
%       Mm          size N_eta x 1
%       Mm_s        size N_spec x 1
%       nTfrac_s    size N_eta x N_spec
%       y_s         size N_eta x N_spec
%
%   Outputs:
%       C           size N_eta x 1
%       a1          size N_eta x 1
%       a2          size N_eta x 1
%       a0          size N_eta x 1
%       j           size N_eta x 1
%       C_s         size N_eta x N_spec
%       C_sl        size N_eta x N_spec x N_spec
%       a_s1        size N_eta x N_spec
%       a_s2        size N_eta x N_spec
%       a_sl2       size N_eta x N_spec x N_spec
%       zeta_s      size N_eta x N_spec
%       g_s         size N_eta x N_spec
%       Cpv         size N_eta x 1
%       av1         size N_eta x 1
%       av2         size N_eta x 1
%       av_s2       size N_eta x N_spec
%       av_sl2      size N_eta x N_spec x N_spec
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

if iscell(kappa_in) && iscell(cp_in) % if the user passes two thermal conductivities (2-T model)
    numberOfTemp = '2T';
    kappa = kappa_in{1};
    kappav = kappa_in{2};
    cp = cp_in{1};
    cpv = cp_in{2};
elseif (iscell(kappa_in) && ~iscell(cp_in)) || (~iscell(kappa_in) && iscell(cp_in))
    error('cp and kappa must both be either cells or vectors');
else
    kappa = kappa_in;
    cp = cp_in;
    numberOfTemp = '1T';
end

% Compute nondimensional terms
Pr = mu.*cp./kappa;

C = rho.*mu./(rho_e*mu_e) .* t.^2;
a1 = C./Pr;
a2 = Ecu/(1+Ecu/2)*(C - a1);
a0 = Ecw/(1+Ecu/2)*C;
j = rho_e./rho;
% Multispec
ic = 1; % initializing index counters
jc = 1;
if ~isempty(varargin)
    MultiSpec = true;
    D_sl = varargin{jc}; jc=jc+1; % it could actually be D_sl or D_s
    if strcmp(numberOfTemp,'2T')
        htr_s = varargin{jc}{1}; % translational-rotational species enthalpy
        hv_s = varargin{jc}{2}; jc=jc+1;  % vibrational-electronic-electron species enthalpy
        h_s = htr_s + hv_s;     % species enthalpy of all energy modes
    else
        h_s = varargin{jc}; jc=jc+1;      % species enthalpy of all energy modes
    end
    Mm          = varargin{jc}; jc=jc+1;
    Mm_s        = varargin{jc}; jc=jc+1;
    nTfrac_s    = varargin{jc}; jc=jc+1;
    bPairDiff   = varargin{jc}; jc=jc+1; % tells us if we are doing spec-spec diffusion (true) or spec-mix (false)

    [N_eta,N_spec,N_l] = size(D_sl); % N_l will be 1 if the user enters D_s
    Mm_s    = repmat(Mm_s', [N_eta,1]);
    Mm      = repmat(Mm,    [1,N_spec]);
    mu_ND   = repmat(mu,    [1,N_spec,N_l]);
    rho_ND  = repmat(rho,   [1,N_spec,N_l]);
    C_ND    = repmat(C,     [1,N_spec,N_l]);
    a1_2D   = repmat(a1,    [1,N_spec]);

    Sc_sl   = mu_ND./(rho_ND.*D_sl);        % Sc_sl or Sc_s
    g_s     = h_s/h_e;
    g_sND   = repmat(g_s,[1,1,N_l]);        % repeating over l
    C_sl    = C_ND./Sc_sl;                  % C_sl or C_s
    a_s1    = 1/(1+Ecu/2)*g_s.*a1_2D;
    a_sl2   = 1/(1+Ecu/2)*g_sND.*C_sl;      % a_sl2 or a_s2
    zeta_s  = Mm./Mm_s .* nTfrac_s;         % passing from pressure fraction (equivalent to mole fraction unless ionized and 2T) to mass fraction

    if bPairDiff
        y_s   = varargin{jc}; jc=jc+1;
        a_sl2 = a_sl2.*repmat(y_s,[1,1,N_spec]);
        C_sl  = C_sl .*repmat(y_s,[1,1,N_spec]);
    end

    varargout{ic} = C_sl; ic=ic+1;
    varargout{ic} = a_s1; ic=ic+1;
    varargout{ic} = a_sl2; ic=ic+1;
    varargout{ic} = zeta_s; ic=ic+1;
else
    MultiSpec = false;
end

if strcmp(numberOfTemp,'2T') % additional variables for the two-temperature models
    av1 = Tv_e/h_e * kappav./mu .* C;                                        % conduction coefficient in the vib-elec-el energy equation
    av2 = (kappav./mu - cpv./cp .* kappa./mu) .* Tv_e/(h_e*(1+Ecu/2)) .* C;  % conduction coefficient in the total energy equation
    Cpv = cpv*Tv_e/h_e;                                                      % heat capacity ratio in the vib-elec-el energy equation

    % remember that cp is actually cptr here
    varargout{ic} = av1; ic=ic+1;
    varargout{ic} = av2; ic=ic+1;
    varargout{ic} = Cpv; ic=ic+1;
    if MultiSpec
        cpv_s   = varargin{jc}; %jc=jc+1;   % readinig inputs
        Cpv_s   = cpv_s * Tv_e/h_e;
        Cpv_sND = repmat(Cpv_s,[1,1,N_l]);   % repeating over l (if spec-mix diffusion coefficients are passed N_l=1)
        av_sl2  = Cpv_sND.*C_sl;             % av_sl2 or av_s2

        varargout{ic} = av_sl2; %ic=ic+1;
    end
end

end % physical2comp_props

