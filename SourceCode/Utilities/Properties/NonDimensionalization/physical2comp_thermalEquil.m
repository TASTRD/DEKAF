function [Bv_tauv,Bv_g,Bv_ys] = physical2comp_thermalEquil(cpv,cptr,h_s,H_e,Tv_e)
% physical2comp_thermalEquil obtains the computational variables needed to
% impose the thermal equilibrium condition in the equation system.
%
% Usage:
%   (1)
%       [Bv_tauv,Bv_g,Bv_ys] = physical2comp_thermalEquil(cpv,cptr,h_s,H_e,Tv_e)
%
% Inputs and outputs:
%   cpv
%       vib-elec-el heat capacity at constant pressure (1 x 1)
%   cptr
%       trans-rot heat capacity at constant pressure (1 x 1)
%   h_s
%       species enthalpies (1 x N_spec)
%   H_e
%       edge total enthalpy (1 x 1)
%   Tv_e
%       edge vib-elec-el temperature (1 x 1)
%   Bv_tauv
%       term multiplying the tauv fluctuation in the linearized equation
%       system (1 x 1)
%   Bv_g
%       term multiplying the g fluctuation in the linearized equation
%       system (1 x 1)
%   Bv_ys
%       term multiplying the ys fluctuations in the linearized equation
%       system (1 x N_spec)
%
% See also:
%
% Author: Fernando Miro Miro
% Date: April 2019
% GNU Lesser General Public License 3.0

Bv_tauv = 1-cpv./cptr;
Bv_g = H_e./(Tv_e.*cptr);
Bv_ys = matrix2D2cell1D(h_s./(cptr.*Tv_e));

end % physical2comp_thermalEquil