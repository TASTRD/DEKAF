function [varargout] = physical2comp_sourceGSs(sourceGSs,dsourceGSs_dT,dsourceGSs_dym,cp,h_s,df_deta,H_e,U_e,rho_e,mu_e,options,varargin)
% physical2comp_sourceGSs passes from the physical gas-surface-interaction
% species source term and derivatives to its computational expressions.
%
% Usage:
%   (1)
%       [Mabl_s,dMabls_dg,dMabls_ddfdeta,dMabls_dym] = physical2comp_sourceGSs(...
%                           sourceGSs,dsourceGSs_dT,dsourceGSs_dym,cp,h_s,df_deta,...
%                           H_e,U_e,rho_e,mu_e,options)
%       |--> for options.numberOfTemp='1T'
%
%   (2)
%       [Mabl_s,dMabls_dg,dMabls_ddfdeta,dMabls_dym,dMabls_dtauv] = physical2comp_sourceGSs(...
%                           sourceGSs,dsourceGSs_dT,dsourceGSs_dym,cptr,h_s,df_deta,...
%                           H_e,U_e,rho_e,mu_e,options,cpv,Tv_e)
%       |--> for options.numberOfTemp='2T'
%
% Inputs and outputs:
%   sourceGSs               (N_eta x N_spec)                [kg/s-m^2]
%       physical species source term
%   dsourceGSs_dT           (N_eta x N_spec x N_T)          [kg/s-m^2-K]
%       derivative of the physical species source term with the various
%       temperatures
%   dsourceGSs_dym          (N_eta x N_spec(m) x N_spec(s)) [kg/s-m^2]
%       derivative of the physical species source term with the various
%       mass fractions
%   Mabl_s                  (N_eta x N_spec)                [m-s/kg]
%       computational species source term
%   dMabls_dg               (N_eta x N_spec)                [m-s/kg]
%       computational derivative with respect to g of the species source
%       term
%   dMabls_ddfdeta          (N_eta x N_spec)                [m-s/kg]
%       computational derivative with respect to df_deta of the species
%       source term
%   dMabls_dtauv            (N_eta x N_spec)                [m-s/kg]
%       computational derivative with respect to tauv of the species source
%       term
%   dMabls_dym              (N_eta x N_spec(s) x N_spec(m)) [m-s/kg]
%       computational derivative with respect to ym of the species source
%       term
%   cp                      (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure
%   cptr                    (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure of the translational-rotational
%       energy modes
%   cpv                     (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure of the vibrational-electronic-
%       electron enerrgy modes
%   h_s                     (N_eta x N_spec)                [J/kg]
%       species enthalpy
%   df_deta                 (N_eta x 1)                     [-]
%       non-dimensional velocity (u/U_e)
%   H_e                     (1 x 1)                         [J/kg]
%       total enthalpy at the boundary-layer edge
%   Tv_e                    (1 x 1)                         [K]
%       vib-elec-el temperature at the boundary-layer edge
%   U_e                     (1 x 1)                         [m/s]
%       streamwise velocity at the boundary-layer edge
%   rho_e                   (1 x 1)                         [kg/m^3]
%       mixture density at the boundary-layer edge
%   mu_e                    (1 x 1)                         [kg/m-s]
%       dynamic viscosity at the boundary-layer edge
%   options
%       classic DEKAF options structure
%
% Author: Fernando Miro Miro
% Date: December 2018
% GNU Lesser General Public License 3.0

switch options.numberOfTemp
    case '1T'
        % nothing to modify
    case '2T'
        dsourceGSs_dTv = dsourceGSs_dT(:,:,2);
        dsourceGSs_dT(:,:,2) = [];
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

% Resizing
dsourceGSs_dym_esm = permute(dsourceGSs_dym,[1,3,2]);           % (eta,m,s) --> (eta,s,m)

% Computational variables and their DIMENSIONAL derivatives
Mabl_s          = 1/(rho_e*mu_e*U_e)*sourceGSs;
dMabls_dT       = 1/(rho_e*mu_e*U_e)*dsourceGSs_dT;
dMabls_dym_expl = 1/(rho_e*mu_e*U_e)*dsourceGSs_dym_esm;

varargout{1} = Mabl_s;
% Derivatives with respect to the computational variables
switch options.numberOfTemp
    case '1T'
        [varargout{2:nargout}] = compDer_all(dMabls_dT,H_e,U_e,cp,df_deta,dMabls_dym_expl,h_s);
    case '2T'
        % Additional arrangements depending on the number of temperatures
        dMabls_dTv       = 1/(rho_e*mu_e*U_e)*dsourceGSs_dTv;
        % extracting variables from varargin (T_e)
        cpv     = varargin{1};
        Tv_e    = varargin{2};
        % Computing computational derivatives
        [varargout{2:nargout}] = compDer_all(dMabls_dT,H_e,U_e,cp,df_deta,dMabls_dym_expl,h_s,dMabls_dTv,cpv,Tv_e);
        % NOTE: cp and h_s are actually cptr and htr_s here
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

end % physical2comp_sourceGSs