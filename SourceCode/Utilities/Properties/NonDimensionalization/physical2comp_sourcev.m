function [varargout] = physical2comp_sourcev(sourcev,dsourcev_dT,dsourcev_dym,TTrans,Tel,p,y_s,cptr,cpv,...
                                h_s,df_deta,R0,Mm_s,bElectron,H_e,h_e,Tv_e,U_e,rho_e,mu_e,options)
% physical2comp_sourcev passes from the physical species source term and
% derivatives to its computational expressions.
%
% Usage:
%   (1)
%       [OmegaV,dOmegaV_dg,dOmegaV_ddfdeta,dOmegaV_dym,dOmegaV_dtauv] = physical2comp_sourcev(...
%                           sourcev,dsourcev_dT,dsourcev_dym,TTrans,Tel,p,y_s,cptr,cpv,...
%                           h_s,df_deta,R0,Mm_s,bElectron,H_e,h_e,Tv_e,U_e,rho_e,mu_e,options)
%
% Inputs and outputs:
%   sourcev                 (N_eta x 1)                     [J/s-m^3]
%       physical vibrational source term
%   dsourcev_dT             (N_eta x N_T)                   [J/s-m^3-K]
%       derivative of the physical vibrational source term with the various
%       temperatures
%   dsourcev_dym            (N_eta x N_spec)                [J/s-m^3-K]
%       derivative of the physical vibrational source term with the various
%       mass fractions
%   OmegaV                  (N_eta x 1)                     [m^2-s^2/kg^2]
%       computational species source term
%   dOmegaV_dg              (N_eta x 1)                     [m^2-s^2/kg^2]
%       computational derivative with respect to g of the vibrational
%       source term
%   dOmegaV_ddfdeta         (N_eta x 1)                     [m^2-s^2/kg^2]
%       computational derivative with respect to df_deta of the vibrational
%       source term
%   dOmegaV_dtauv           (N_eta x 1)                     [m^2-s^2/kg^2]
%       computational derivative with respect to tauv of the vibrational
%       source term
%   dOmegaV_dym             (N_eta x N_spec)                [m^2-s^2/kg^2]
%       computational derivative with respect to ym of the vibrational
%       source term
%   TTrans                  (N_eta x 1)                     [K]
%       translational temperature
%   Tel                     (N_eta x 1)                     [K]
%       electron temperature
%   p                       (N_eta x 1)                     [Pa]
%       pressure
%   y_s                     (N_eta x N_spec)                [-]
%       species mass fraction
%   cptr                    (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure of the translational-rotational
%       energy modes
%   cpv                     (N_eta x 1)                     [J/kg-K]
%       heat capacity at constant pressure of the vibrational-electronic-
%       electron enerrgy modes
%   h_s                     (N_eta x N_spec)                [J/kg]
%       species enthalpy
%   df_deta                 (N_eta x 1)                     [-]
%       non-dimensional velocity (u/U_e)
%   R0                      (1 x 1)                         [J/kg-K]
%       universal gas constant
%   Mm_s                    (N_spec x 1)                    [kg/mol]
%       species molar mass
%   bElectron               (N_spec x 1)                    [-]
%       species electron boolean
%   H_e                     (1 x 1)                         [J/kg]
%       total enthalpy at the boundary-layer edge
%   h_e                     (1 x 1)                         [J/kg]
%       static enthalpy at the boundary-layer edge
%   Tv_e                    (1 x 1)                         [K]
%       vib-elec-el temperature at the boundary-layer edge
%   U_e                     (1 x 1)                         [m/s]
%       streamwise velocity at the boundary-layer edge
%   rho_e                   (1 x 1)                         [kg/m^3]
%       mixture density at the boundary-layer edge
%   mu_e                    (1 x 1)                         [kg/m-s]
%       dynamic viscosity at the boundary-layer edge
%   options
%       classic DEKAF options structure
%
% Author: Fernando Miro Miro
% Date: December 2018
% GNU Lesser General Public License 3.0

options.EoS = protectedvalue(options,'EoS','idealGas');

[~,N_spec] = size(y_s);
rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);
drho_dym    = getMixtureDer_drho_dym(p,y_s,R0,Mm_s,bElectron,TTrans,Tel);
drho_dT     = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,[TTrans,Tel],options.numberOfTemp);

switch options.numberOfTemp
    case '2T'
        dsourcev_dTv = dsourcev_dT(:,2);
        dsourcev_dT(:,2) = [];
        drho_dTv = drho_dT(:,2);
        drho_dT(:,2) = [];
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

% Resizing
rho_2D = repmat(rho,[1,N_spec]);                            % (eta,1) --> (eta,m)
sourcev_2D = repmat(sourcev,[1,N_spec]);                    % (eta,1) --> (eta,m)

% Computational variables and their DIMENSIONAL derivatives
OmegaV          = sourcev./(rho*rho_e*mu_e*U_e^2*h_e);
dOmegaV_dT      = 1/(rho_e*mu_e*U_e^2*h_e)*( 1./rho.*   dsourcev_dT  - sourcev   ./(rho.^2)   .*drho_dT );
dOmegaV_dTv     = 1/(rho_e*mu_e*U_e^2*h_e)*( 1./rho.*   dsourcev_dTv - sourcev   ./(rho.^2)   .*drho_dTv );
dOmegaV_dym_expl= 1/(rho_e*mu_e*U_e^2*h_e)*( 1./rho_2D.*dsourcev_dym - sourcev_2D./(rho_2D.^2).*drho_dym );

varargout{1} = OmegaV;
% Derivatives with respect to the computational variables
switch options.numberOfTemp
    case '2T'
        % Computing computational derivatives
        [varargout{2:nargout}] = compDer_all(dOmegaV_dT,H_e,U_e,cptr,df_deta,dOmegaV_dym_expl,h_s,dOmegaV_dTv,cpv,Tv_e);
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

