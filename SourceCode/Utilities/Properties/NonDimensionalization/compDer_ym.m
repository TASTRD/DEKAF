function dZ_dym = compDer_ym(dZ_dym_expl,h_m,cp,dZ_dT,varargin)
%COMPDER_ym Calculate the derivative of input Z with respect to
% nondimensional mass fractions, translating from physical to computational
% variables.
%
% Usage:
%   (1)
%       dZ_dym = compDer_ym(dZ_dym_expl,h_m,cp,dZ_dT)
%       |--> how to call the function for numberOfTemp='1T'
%
%   (2)
%       dZ_dym = compDer_ym(dZ_dym_expl,h_m,cptr,dZ_dT)
%       |--> how to call the function for numberOfTemp='2T' with tauv as
%       the computational variable
%
%   (3)
%       dZ_dym = compDer_ym(dZ_dym_expl,h_m,cptr,dZ_dT,hv_m,cpv,dZ_dTv)
%       |--> how to call the function for numberOfTemp='2T' for gv as the
%       computational variable
%
% Inputs and outputs
%   dZ_dym_expl explicit derivative of a property Z with repect to its mass
%               fractions (N_eta x whatever x N_spec)
%   dZ_dT       temperature derivative of a property Z (N_eta x whatever)
%   dZ_dTv      vibrational temperature derivative of a property Z
%               (N_eta x whatever)
%   h_m         species enthalpy (N_eta x N_spec)
%   hv_m        species enthalpy due to vibrational-electronic-electron
%               modes (N_eta x N_spec)
%   cp          heat capacity at constant pressure (N_eta x 1)
%   cptr        heat capacity at constant pressure of the translational and
%               rotational energy modes (N_eta x 1)
%   cpv         heat capacity at constant pressure of the
%               vibrational-electronic-electron energy modes (N_eta x 1)
%   dZ_dym      derivative of the variable with respect to the mass
%               fractions including also implicit derivatives
%               (N_eta x whatever x N_spec)
%
% See also: compDer_all, compDer_g, compDer_df_deta, compDer_gv,
% compDer_tauv
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
% GNU Lesser General Public License 3.0

sizesl = size(dZ_dym_expl);                                                 % we obtain the size, to see if we have to repmat
cp      = repmat(cp,        [1,sizesl(2:end)]);                             % (eta,1)        --> (eta,whatever,m)
h_m     = repmat(permute(h_m,[1,3:length(sizesl),2]), [1,sizesl(2:end-1),1]); % (eta,m)    --> (eta,whatever,m)
dZ_dT   = repmat(dZ_dT,  [ones(1,length(sizesl)-1),sizesl(end)]);           % (eta,whatever) --> (eta.whatever,m)
if isempty(varargin)                                                        % one-temperature case
    dZ_dym = dZ_dym_expl - h_m ./ cp .* dZ_dT;
else                                                                        % two-temperature case
    hv_m = varargin{1};
    cpv = varargin{2};
    dZ_dTv = varargin{3};
    cpv         = repmat(cpv,        [1,sizesl(2:end)]);                        % (eta,1)        --> (eta,whatever,m)
    hv_m = repmat(permute(hv_m,[1,3:length(sizesl),2]), [1,sizesl(2:end-1),1]); % (eta,m)      --> (eta,whatever,m)
    dZ_dTv      = repmat(dZ_dTv,  [ones(1,length(sizesl)-1),sizesl(end)]);      % (eta,whatever) --> (eta.whatever,m)
    dZ_dym = dZ_dym_expl - h_m ./ cp .* dZ_dT - hv_m ./ cpv .* dZ_dTv;
    % NOTE: here h_m and cp are actually htr_m and cptr
end

end % compDer_ym

