function dZ_dg = compDer_g(dZ_dT,H_e,cp)
%COMPDER_G Calculate the derivative of input Z with respect to
% nondimensionalized total enthalpy, translating from physical to
% computational variables.
%
% Usage:
%   (1)
%       dZ_dg = compDer_g(dZ_dT,H_e,cp)
%       |--> how to call the function for numberOfTemp='1T' - using cp
%
%   (2)
%       dZ_dg = compDer_g(dZ_dT,H_e,cptr)
%       |--> how to call the function for numberOfTemp='2T' - using cptr
%
% Inputs and outputs
%   dZ_dT       temperature derivative of a property (N_eta x whatever)
%   H_e         edge total enthalpy (1 x 1)
%   cp          heat capacity at constant pressure (N_eta x 1)
%   cptr        heat capacity at constant pressure of the translational and
%               rotational energy modes (N_eta x 1)
%   dZ_dg       derivative of the variable with respect to g
%               (non-dimensional total enthalpy) (N_eta x whatever)
%
% See also: compDer_all, compDer_g, compDer_df_deta, compDer_ym,
% compDer_gv, compDer_tauv
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
% GNU Lesser General Public License 3.0

sizesl = size(dZ_dT); % we obtain the size, to see if we have to repmat cp
cp      = repmat(cp,[1,sizesl(2:end)]); % (eta,1) --> (eta,whatever)

dZ_dg = dZ_dT .* H_e ./ cp;

end % compDer_g