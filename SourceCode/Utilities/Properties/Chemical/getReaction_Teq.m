function Teq_r = getReaction_Teq(Tf_r,Tb_r)
% getReaction_Teq computes the temperature to be used for the equilibrium
% constant of each reaction.
%
% Usage:
%   (1)
%       Teq_r = getReaction_Teq(Tf_r,Tb_r)
%
% Inputs and outputs:
%   Tf_r    forward reaction temperature (N_eta x N_reac)
%   Tb_r    backward reaction temperature (N_eta x N_reac)
%   Teq_r   equilibrium reaction temperature (N_eta x N_reac)
%
% See also: getReaction_T, listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

Teq_r = sqrt(Tf_r.*Tb_r);