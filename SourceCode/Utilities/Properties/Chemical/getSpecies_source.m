function [source_s,lnkf_r,lnkb_r,source_sr] = getSpecies_source(TTrans,TRot,TVib,TElec,Tel,y_s,...
                    rho,Mm_s,nu_reac,nu_prod,A_r,nT_r,theta_r,qf_r,qb_r,Keq_struc,options)
%GETSPECIES_SOURCE Compute the species mass source terms from a set
% of chemical reactions
%
% Usage:
%   (1)
%       [source_s,lnkf_r,lnkb_r,source_sr] = getSpecies_source(TTrans,TRot,TVib,TElec,Tel,y_s,...
%                           rho,Mm_s,nu_reac,nu_prod,A_r,nT_r,theta_r,qf_r,qb_r,Keq_struc,options)
%
% Inputs:
%   TTrans      -   translational temperature [K]. size (N_eta x 1)
%   TRot        -   rotational temperature [K]. size (N_eta x 1)
%   TVib        -   vibrational temperature [K]. size (N_eta x 1)
%   TElec       -   electronic temperature [K]. size (N_eta x 1)
%   Tel         -   electron temperature [K]. size (N_eta x 1)
%   y_s         -   species mass fractions [-].
%                   Matrix of size (N_eta x N_spec)
%   rho         -   mixture mass density [kg/m^3]
%                   Vector of size (N_eta x 1)
%   Mm_s        -   species molecular molar mass [kg/mol].
%                   Vector size (N_spec x 1)
%   nu_reac     -   stoichiometric coefficient of each species as a reactant
%                   of each chemical reaction [-].
%                   Matrix of size (N_spec x N_reac).
%   nu_prod     -   stoichiometric coefficient of each species as a product
%                   of each chemical reaction [-].
%                   Matrix of size (N_spec x N_reac).
%   A_r         -   Preexponential factor of each chemical reaction [mol/s *
%                   (mol/m^3)^(nu_p(s,r))]. Vector of size (N_reac x 1)
%   nT_r        -   Temperature exponent in the rate constant of each
%                   chemical reaction [-]. Vector of size (N_reac x 1)
%   theta_r     -   Activation temperature of each chemical reaction [K].
%                   Vector of size (N_reac x 1)
%   qf_r        -   coefficient in the geometric average defining the
%                   reaction temperature for forward reactions [-].
%                   Vector of size (N_reac x 1)
%   qb_r        -   coefficient in the geometric average defining the
%                   reaction temperature for backward reactions [-].
%                   Vector of size (N_reac x 1)
%   Keq_struc   -   structure containing all the necessary parameters for
%                   the computation of the equilibrium constant.
%   options     -   necessary fields
%                       modelKb
%                       and all those necessary for getReaction_lnKeq
%
% Outputs:
%   source_s    -   species mass generation source term per unit volume
%                   summed over all of the chemical reactions  [kg/(m^3*s)]
%   lnkf_r      -   natural logarithm of the forward reaction constant
%                   [mol/s *(mol/m^3)^(sum(nu_reac(s,r)))
%                   Matrix of size N_eta x N_reac
%   lnkb_r      -   natural logarithm of the backward reaction constant
%                   [mol/s *(mol/m^3)^(sum(nu_prod(s,r)))
%                   Matrix of size N_eta x N_reac
%   source_sr   -   species mass generation source term per unit volume
%                   for each reaction  [kg/(m^3*s)]
%                   Matrix of size N_eta x N_spec x N_reac
%
% Notes:
% The calculation of the backward rate, kb_r, requires the following
% assumption:
%   Given values of temperature and the reactant concentrations, the rate
%   at which a reaction proceeds in a given direction is the same whether
%   the system is or is not in equilibrium. This is not necessarily true
%   since other simultaneous non-equilibrium processes (e.g., vibration)
%   can in principle alter the rate of the reaction.
%   In other words, we assume the rate constant to be a function only of
%   temperature and independent of the non-equilibrium variables.
%
% References:
% W. G. Vincenti and C. H. Kruger, "Introduction to Physical Gas Dynamics",
%   Wiley, New York, 1965. See Chapter 7, page 225.
%
% Children and related functions:
%   See also getReaction_kf.m, getReaction_Keq.m, getReaction_source.m,
%   listOfVariablesExplained
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% Obtaining required shapes
[N_spec,N_reac] = size(nu_reac);
N_eta = length(TTrans);
delta_nu = nu_prod - nu_reac; % stoichiometric difference matrix

% obtaining reaction temperatures
switch options.numberOfTemp
    case '1T'
        Tf_r = repmat(TTrans,[1,N_reac]);
        Tb_r = repmat(TTrans,[1,N_reac]);
    case '2T'
        Tf_r = getReaction_T(qf_r,TTrans,TVib);
        Tb_r = getReaction_T(qb_r,TTrans,TVib);
    otherwise
        error(['the chosen thermal models ''',numberOfTemp,''' is not supported']);
end

% computing equilibrium temperature (geometric average of forward and
% backward)
Teq_r = getReaction_Teq(Tf_r,Tb_r);

% Obtaining reaction rates
lnkf_r = getReaction_lnkf(Tf_r,A_r,nT_r,theta_r); % forward reaction rate
lnKeq_r = getReaction_lnKeq(Teq_r,TTrans,TRot,TVib,TElec,Tel,delta_nu,Keq_struc,options); % equilibrium constant
lnkf_r_atTbr = getReaction_lnkf(Tb_r,A_r,nT_r,theta_r); % forward reaction rate at Tb_r
lnkb_r = lnkf_r_atTbr - lnKeq_r; % backward reaction rate

% Computing species source term due to every reaction
source_sr = getReaction_source(lnkf_r,lnkb_r,rho,y_s,Mm_s,nu_reac,nu_prod);
% size N_eta x N_source x N_reac

% summing over the source terms of every reaction to obtain the total species source term
source_s = zeros(N_eta,N_spec);
for r = 1:N_reac
    source_s = source_s + source_sr(:,:,r);
end

end
