function T_r = getReaction_T(q_r,varargin)
% getReaction_T computes the temperature defining each reaction
%
% The reaction temperature is defined as a geometric average (determined by
% the coefficients q_r) of the different temperatures defining the system.
%
% Usage:
%   (1) T_r = getReaction_T(q_r,T1)
%       | (with size(q_r) = 1 x N_reac
%       |--> one-temperature model
%
%   (2) T_r = getReaction_T(q_r,T1,T2)
%       | (with size(q_r) = 2 x N_reac
%       |--> two-temperature model
%
% Inputs & Outputs:
%   q_r         size N_T x N_reac
%   T1,T2,...   size N_eta x 1
%   T_r         size N_eta x N_reac
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_T,N_reac] = size(q_r);
N_eta = length(varargin{1});
q_r = repmat(reshape(q_r.',[1,N_reac,N_T]),[N_eta,1,1]);    % (iT,r) --> (eta,r,iT)
T_2D = [varargin{:}]; % building matrix with all the temperatures (N_eta x N_T)
T_3D = repmat(reshape(T_2D,[N_eta,1,N_T]),[1,N_reac,1]);    % (eta,iT) --> (eta,r,iT)

T_r = ones(N_eta,N_reac);   % initializing
for iT=1:N_T                % looping temperatures
    T_r = T_r.*T_3D(:,:,iT).^q_r(:,:,iT);
end