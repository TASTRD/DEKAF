function dsourcesr_dT = getReactionDer_dsourcesr_dT(nu_reac,nu_prod,rho_l,drhol_dT,Mm_s,lnkf_r,lnkb_r,dlnkfr_dT,dlnkbr_dT)
% getSpeciesDer_dsource_dT returns a 4D matrix with the derivatives of the
% different contributions of each reaction to the species source terms with
% respect to the different temperatures in the used thermal model.
%
% Usage:
%   (1)
%       dsourcesr_dT = getReactionDer_dsourcesr_dT(nu_reac,nu_prod,rho_l,drhol_dT,...
%                                             Mm_s,lnkf_r,lnkb_r,dlnkfr_dT,dlnkbr_dT)
%
% Inputs and outputs:
%  - nu_prod        N_spec x N_reac
%  - nu_reac        N_spec x N_reac
%  - rho_l          N_eta x N_spec
%  - drhol_dT       N_eta x N_spec x N_T
%  - Mm_s           N_spec x 1
%  - lnkf_r         N_eta x N_reac
%  - lnkb_r         N_eta x N_reac
%  - dlnkfr_dT      N_eta x N_reac x N_T
%  - dlnkbr_dT      N_eta x N_reac x N_T
%  - dsourcesr_dT   N_eta x N_spec x N_T x N_reac
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% Obtaining sizes
[N_spec,N_reac] = size(nu_reac);
[N_eta,~,N_T] = size(drhol_dT);

% Reshaping variables such that they have the desired shapes
nu_reac_rl4D    = repmat(permute(nu_reac,[3,2,4,1]),[N_eta,1,N_T,1]);       % (l,r) --> (eta,r,iT,l)
nu_prod_rl4D    = repmat(permute(nu_prod,[3,2,4,1]),[N_eta,1,N_T,1]);       % (l,r) --> (eta,r,iT,l)
rhol_4D         = repmat(permute(rho_l,[1,3,4,2]),[1,N_reac,N_T,1]);        % (eta,l) --> (eta,r,iT,l)
drhol_dT_4D     = repmat(permute(drhol_dT,[1,4,3,2]),[1,N_reac,1,1]);       % (eta,l,iT) --> (eta,r,iT,l)

rhol_3D         = repmat(permute(rho_l,[1,3,2]),[1,N_reac,1]);              % (eta,l) --> (eta,r,l)
nu_reac_rl3D    = repmat(permute(nu_reac,[3,2,1]),[N_eta,1,1]);             % (l,r) --> (eta,r,l)
nu_prod_rl3D    = repmat(permute(nu_prod,[3,2,1]),[N_eta,1,1]);             % (l,r) --> (eta,r,l)


% building sumation terms
sum_f = zeros(N_eta,N_reac);
sum_b = zeros(N_eta,N_reac);
sum_dfdT = zeros(N_eta,N_reac,N_T);
sum_dbdT = zeros(N_eta,N_reac,N_T);
for l = 1:N_spec
    sum_f = sum_f + log(rhol_3D(:,:,l) ./ Mm_s(l)) .* (nu_reac_rl3D(:,:,l)); % (eta,r)
    sum_b = sum_b + log(rhol_3D(:,:,l) ./ Mm_s(l)) .* (nu_prod_rl3D(:,:,l));
    sum_dfdT = sum_dfdT + nu_reac_rl4D(:,:,:,l)./rhol_4D(:,:,:,l) .* drhol_dT_4D(:,:,:,l); % (eta,r,iT)
    sum_dbdT = sum_dbdT + nu_prod_rl4D(:,:,:,l)./rhol_4D(:,:,:,l) .* drhol_dT_4D(:,:,:,l);
end
Afr = exp(lnkf_r + sum_f);      Afr = repmat(Afr,[1,1,N_T]);                % (eta,r) --> (eta,r,iT)
Abr = exp(lnkb_r + sum_b);      Abr = repmat(Abr,[1,1,N_T]);                % (eta,r) --> (eta,r,iT)
dAfr_dT = Afr .* (dlnkfr_dT + sum_dfdT);                                    % (eta,r,iT)
dAbr_dT = Abr .* (dlnkbr_dT + sum_dbdT);                                    % (eta,r,iT)

% Reshaping variables for the sumation
dAfr_dT_4D = repmat(permute(dAfr_dT,[1,4,3,2]),[1,N_spec,1,1]);             % (eta,r,iT) --> (eta,s,iT,r)
dAbr_dT_4D = repmat(permute(dAbr_dT,[1,4,3,2]),[1,N_spec,1,1]);             % (eta,r,iT) --> (eta,s,iT,r)
delanu_sr4D = repmat(permute(nu_prod-nu_reac,[3,1,4,2]),[N_eta,1,N_T,1]);   % (s,r) --> (eta,s,iT,r)
Mm_s4D = repmat(permute(Mm_s,[2,1,3]),[N_eta,1,N_T,N_reac]);                % (s,1) --> (eta,s,iT,r)

% Assembling
dsourcesr_dT = Mm_s4D.*delanu_sr4D .* (dAfr_dT_4D - dAbr_dT_4D);

end % getReactionDer_dsourcesr_dT