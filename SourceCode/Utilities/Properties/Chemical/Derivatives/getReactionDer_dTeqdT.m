function dTeqr_dT = getReactionDer_dTeqdT(Tf_r,Tb_r,dTfr_dT,dTbr_dT)
% getReactionDer_dTeqdT computes the derivative of the temperature to be
% used for the equilibrium constant of each reaction, with respect to the
% temperature of each energy mode
%
% Usage:
%   (1)
%       dTeqr_dT = getReactionDer_dTeqdT(Tf_r,Tb_r)
%
% Inputs and outputs:
%   Tf_r        forward reaction temperature (N_eta x N_reac)
%   Tb_r        backward reaction temperature (N_eta x N_reac)
%   dTfr_dT     derivative of the forward reaction temperature (N_eta x N_reac x N_T)
%   dTbr_dT     derivative of the backward reaction temperature (N_eta x N_reac x N_T)
%   dTeqr_dT    derivative of the equilibrium reaction temperature (N_eta x N_reac x N_T)
%
% See also: getReaction_Teq, getReaction_T, getReactionDer_dTrdT,
% listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_T = size(dTfr_dT,3);
Teq_r = getReaction_Teq(Tf_r,Tb_r);

Teq_r = repmat(Teq_r,[1,1,N_T]); % repeating over the modes
Tf_r = repmat(Tf_r,[1,1,N_T]);
Tb_r = repmat(Tb_r,[1,1,N_T]);

dTeqr_dT = 1./(2*Teq_r).*(Tf_r.*dTbr_dT + Tb_r.*dTfr_dT);