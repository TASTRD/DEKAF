function dlnKeqr_dym = getReactionDer_dlnKeqr_dym(y_s,T,p,Mm_s,nu_reac,nu_prod,Keq_struc,rho,drho_dym,options)
%GETREACTIONDER_DLNKEQR_DYM Derivative of natural logarithm of equilibrium
% constant of reaction r wrt temperature
%
% FIXME: THIS IS A USELESS FUNCTION. JUST KEPT AS PLACE HOLDER
%
% Inputs:
%  - y_s                        (size N_eta x N_spec)
%  - T                          (size N_eta x 1)
%  - p                          (size N_eta x 1)
%  - Mm_s                       (size N_spec x 1)
%  - nu_reac                    (size N_spec x N_reac) nu'_sr
%  - nu_prod                    (size N_spec x N_reac) nu''_sr
%  - Keq_struc                  structure
%  - rho                        (size N_eta x 1)
%
% Outputs:
%   dlnKeqr_dT  -   natural logarithm of reaction equilibrium constant
%                   derivative wrt temperature
%                   (size N_eta x N_spec x N_reac)
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

[N_spec,N_reac] = size(nu_reac);
N_eta = length(T);

    dlnKeqr_dym = zeros(N_eta,N_spec,N_reac);
    %{
if strcmp(options.modelEquilibrium,'Park85')
    dlnKeqr_dym = zeros(N_eta,N_spec,N_reac);
elseif strcmp(options.modelEquilibrium,'RRHO')
    R0          = Keq_struc.R0;
    bElectron   = Keq_struc.bElectron;
    pAtm        = Keq_struc.pAtm;

    % The pressure used to compute the gibbs free energy doesn't really
    % matter, as long as it is the same as that used in the evaluation of
    % the lnKeq_r.
    pAtm_vec    = repmat(pAtm,[N_eta,1]);

    ddeltaGr_dym = getReactionDer_ddeltaGr_dym(nu_reac,nu_prod,Mm_s,T,pAtm_vec,rho,y_s,drho_dym,R0,bElectron);

    T = repmat(T,[1,N_spec,N_reac]);

    dlnKeqr_dym = - 1./(R0*T).*ddeltaGr_dym;
end
%}
end % getReactionDer_dlnKeqr_dym.m
