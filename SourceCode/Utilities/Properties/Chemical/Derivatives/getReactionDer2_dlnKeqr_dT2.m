function dlnKeqr_dT2 = getReactionDer2_dlnKeqr_dT2(T,delta_nu,Keq_struc,options)
% getReactionDer2_dlnKeqr_dT2 computes the second derivative with
% temperature of the equilibrium constant.
%
% Usage:
%   (1)
%       dlnKeqr_dT = getReactionDer2_dlnKeqr_dT(y_s,T,delta_nu,Keq_struc,options)
%
% Inputs:
%   T           -   temperature [K]. Vector of size (N_eta x 1)
%   delta_nu    -   stoichiometric jump matrix (prod-reac) of the reactions (N_spec x N_reac)
%   Keq_struc   -   struct containing the following fields depending on options.modelEquilibrium:
%
%    1) options.modelEquilibrium = 'Park85'
%
%           It uses Park's 1985 polynomial curve fit:
%
%               lnKeq = exp(A1 + A2*Z + ... + A5*Z^4)       with Z=10000/T
%
%            .Aeq_r
%               five coefficients (A1, A2, ..., A5) in the polynomial fit
%               of the reaction equilibrium constant. Matrix of size (N_reac x 5).
%            .T0_eq
%               minimum temperature up to which the fitting is applicable. (1 x 1) [K]
%            .idx_eq
%               index vector detailing which reactions are to be used for
%               the LTE equilibrium system.
%
%
%    2) options.modelEquilibrium = 'RRHO'
%
%           It uses the expression for the equilibrium constant coming from
%           the partition functions of the different energy modes assuming
%           a Rigid-Rotor & Harmonic-Oscillator.
%
%            .Mm_s                  (N_spec x 1)        [kg/mol]
%            .R_s                   (N_spec x 1)        [J/kg-K]
%            .thetaRot_s            (N_spec x 1)        [K]
%            .thetaVib_s            (N_spec x 1)        [K]
%            .thetaElec_s           (N_spec x 1)        [K]
%            .L_s                   (N_spec x 1)        [-]
%            .sigma_s               (N_spec x 1)        [-]
%            .gDegen_s              (N_spec x N_mod)    [-]
%            .bElectron             (N_spec x 1)        [-]
%            .kBoltz                (1 x 1)             [m² kg/s²-K]
%            .hPlanck               (1 x 1)             [J s]
%            .nAvogadro             (1 x 1)             [1/mol]
%            .hForm_s               (N_spec x 1)        [J/kg]
%
%    3) options.modelEquilibrium = 'Arrhenius'
%
%           It uses an Arrhenius-type expression:
%               lnKeq = ln(A_eqr) + nT_eqr * ln(T) - theta_eqr/T
%
%            .A_eqr                 (N_reac x 1)  [(m^3/mol)^sum(delta_nu) * K^(1/nT_r)]
%            .nT_eqr                (N_reac x 1)        [-]
%            .theta_eqr             (N_reac x 1)        [K]
%            .idx_eq
%               index vector detailing which reactions are to be used for
%               the LTE equilibrium system.
%
%    4) options.modelEquilibrium = 'Park90'
%
%           UNSUPPORTED
%
%   options -   structure containing relevant options, such as modelEquilibrium,
%               which specifies what model is to be used for keq
%
% Outputs:
%   dlnKeqr_dT2  -   natural logarithm of reaction equilibrium constant
%                    derivative wrt temperature twice
%                    (size N_eta x N_reac)
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_eta = length(T);

switch options.modelEquilibrium
    case 'Park85'
        % Extract Aeq_r from input struct
        Aeq_r = Keq_struc.Aeq_r;
        T0_eq = Keq_struc.T0_eq;

        % for LTE we only want the equilibrium constants for the reactions used
        % in the equilibrium system (for air5: O2 diss, N2 diss and NO diss)
        if ismember(options.flow,{'LTE','LTEED'})
            idx_eq = Keq_struc.idx_eq;
            Aeq_r = Aeq_r(idx_eq,:);
        end

        % reshaping
        [N_reac,N_order] = size(Aeq_r);
        Tmat = repmat(T,[1,N_reac]);
        Aeq_r = repmat(reshape(Aeq_r,[1,N_reac,N_order]),[N_eta,1,1]);

        % Obtaining Z coefficient and evaluating Keq_r
        Z = 10000./Tmat;
        dZ_dT = -10000./Tmat.^2;
        dZ_dT2 = 20000./Tmat.^3;
        lnKeqr_fit  = Aeq_r(:,:,1) + Aeq_r(:,:,2) .* Z + Aeq_r(:,:,3) .* (Z).^2 + Aeq_r(:,:,4) .* (Z).^3 +   Aeq_r(:,:,5) .* (Z).^4;
        dlnKeqr_dZ  =                Aeq_r(:,:,2) +    2*Aeq_r(:,:,3) .* (Z)  + 3*Aeq_r(:,:,4) .* (Z).^2 + 4*Aeq_r(:,:,5) .* (Z).^3;
        dlnKeqr_dZ2 =                                  2*Aeq_r(:,:,3)         + 6*Aeq_r(:,:,4) .* (Z)   + 12*Aeq_r(:,:,5) .* (Z).^2;
        dlnKeqrfit_dT = dlnKeqr_dZ.*dZ_dT;
        dlnKeqrfit_dT2 = dlnKeqr_dZ2.*dZ_dT.^2 + dlnKeqr_dZ.*dZ_dT2;               % derivatives of the curve fitting

        % applying correction (cutting off at T0_eq)
        gT          = 0.5*(1-tanh(T-T0_eq));             % weighing functions
        dgT_dT      = -0.5./(cosh(T-T0_eq)).^2;
        dgT_dT2     = tanh(T-T0_eq)./(cosh(T-T0_eq)).^2;
        gTmat       = repmat(gT     ,[1,N_reac]);
        dgT_dTmat   = repmat(dgT_dT ,[1,N_reac]);
        dgT_dT2mat  = repmat(dgT_dT2,[1,N_reac]);

        Z0     =  10000/T0_eq;
        dZ_dT0 = -10000/T0_eq^2;
        lnKeq_r0    =  Aeq_r(:,:,1) + Aeq_r(:,:,2) .* Z0 + Aeq_r(:,:,3) .* (Z0).^2 + Aeq_r(:,:,4) .* (Z0).^3 +   Aeq_r(:,:,5) .* (Z0).^4;
        %     dlnKeqrextr_dT = 0;                                                     % 0th-order extrapolation after Z0
        dlnKeqrextr_dT =  dZ_dT0 .* (Aeq_r(:,:,2) +     2*Aeq_r(:,:,3) .* (Z0) +  3*Aeq_r(:,:,4) .* (Z0).^2 + 4*Aeq_r(:,:,5) .* (Z0).^3);
        lnKeqr_extr = lnKeq_r0 + dlnKeqrextr_dT.*(Tmat-T0_eq);                  % 1st-order extrapolation after Z0
        dlnKeqrextr_dT2 = 0;                                                    % 1st-order extrapolation after Z0

        dlnKeqr_dT2 = dgT_dT2mat.*lnKeqr_extr + 2*dgT_dTmat.*dlnKeqrextr_dT +   gTmat .*dlnKeqrextr_dT2 ...
            - dgT_dT2mat.*lnKeqr_fit  - 2*dgT_dTmat.*dlnKeqrfit_dT + (1-gTmat).*dlnKeqrfit_dT2;

    case 'Arrhenius'
        % Extract Aeq_r from input struct
        nT_eqr = Keq_struc.nT_eqr;
        theta_eqr = Keq_struc.theta_eqr;

        % for LTE we only want the equilibrium constants for the reactions used
        % in the equilibrium system (for air5: O2 diss, N2 diss and NO diss)
        if ismember(options.flow,{'LTE','LTEED'})
            idx_eq = Keq_struc.idx_eq;
            nT_eqr = nT_eqr(idx_eq);
            theta_eqr = theta_eqr(idx_eq);
        end

        N_reac = length(nT_eqr);
        T = repmat(T,[1,N_reac]);
        nT_eqr = repmat(nT_eqr.',[N_eta,1]);
        theta_eqr = repmat(theta_eqr.',[N_eta,1]);

        dlnKeqr_dT2 = -nT_eqr./T.^2 - 2*theta_eqr./T.^3;

    case 'RRHO'
        protectedvalue(options,'modelThermal',{'RRHO','RRHO-1LA'});

        R_s         = Keq_struc.R_s;
        nAtoms_s    = Keq_struc.nAtoms_s;
        thetaVib_s  = Keq_struc.thetaVib_s;
        thetaElec_s = Keq_struc.thetaElec_s;
        gDegen_s    = Keq_struc.gDegen_s;
        bElectron   = Keq_struc.bElectron;
        hForm_s     = Keq_struc.hForm_s;
        N_spec = length(hForm_s); % nuber of species

        % computing species enthalpies
        [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,T_mat] = ...
            reshape_inputs4enthalpy(R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,T);

        [~,~,~,h_s,~,~,cvMod_s]  = getMixture_h_cp(ones(N_eta,N_spec)/N_spec,T_mat,T_mat,T_mat,T_mat,T_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options);
        e_s = h_s-R_s_mat.*T_mat;                                                           % species internal energy
        cv_s = cvMod_s.cvTrans_s + cvMod_s.cvRot_s + cvMod_s.cvVib_s + cvMod_s.cvElec_s;    % species heat capacity

        dlnQshForm_dT2 = (cv_s - 2*e_s./T_mat) ./ (R_s_mat.*T_mat.^2);                      % really cool result of operating with the lnKeq expression

        [N_spec,N_reac] = size(delta_nu);
        dlnQshForm_dT2   = repmat(reshape(dlnQshForm_dT2, [N_eta,1,N_spec]),  [1,N_reac,1]); % repeating over r
        delta_nu         = repmat(reshape(delta_nu',     [1,N_reac,N_spec]), [N_eta,1,1]);   % repeating over eta

        dlnKeqr_dT2 = zeros(N_eta,N_reac);                                      % allocating dlnKeqr_dT2
        for s=1:N_spec                                                          % suming over the species
            dlnKeqr_dT2 = dlnKeqr_dT2 + delta_nu(:,:,s) .* dlnQshForm_dT2(:,:,s);
        end
    otherwise
        error(['the chosen Keq model ''',options.modelEquilibrium,''' is not supported.']);
end

end % getReactionDer2_dlnKeqr_dT2.m