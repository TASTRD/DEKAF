function dlnkfr_dT = getReactionDer_dlnkfr_dT(T_r,nT_r,theta_r)
% GETREACTIONDER_DLNKFR_DT returns the derivative with temperature of the
% logarithm of the forward reaction rates.
%
% Usage:
%   (1)
%       dlnkfr_dT = getReactionDer_dlnkfr_dT(T_r,nT_r,theta_r)
%
% Inputs:
%  - T_r        [N_eta x N_reac] (to be repeated over dim. 2)
%  - nT_r       [N_reac x 1] (to be reshaped to dim 1 and repeated over dim. 2)
%  - theta_r    [N_reac x 1] (to be reshaped to dim 1 and repeated over dim. 2)
%
% Output: dlnkfr_dT     (size N_eta x N_reac)
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

% obtaining necessary sizes
N_eta = size(T_r,1);

% reshaping
nT_r = repmat(nT_r.',[N_eta,1]);
theta_r = repmat(theta_r.',[N_eta,1]);

dlnkfr_dT = nT_r./T_r + theta_r./T_r.^2;

end