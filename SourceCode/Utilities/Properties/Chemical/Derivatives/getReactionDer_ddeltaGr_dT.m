function ddeltaGr_dT = getReactionDer_ddeltaGr_dT(nu_reac,nu_prod,T,y_s,p,rho,drho_dT,R_s,Mm_s,...
                                                 thetaRot_s,L_s,sigma_s,nAtoms_s,thetaVib_s,gDegen_s,thetaElec_s,...
                                                 bElectron,R0,nAvogadro,hPlanck,kBoltz,options)
%GETREACTIONDER_DDELTAGR_DT Derivative of reactions' jump in gibbs free
% energy wrt temperature
%
% Inputs:
%  - nu_reac                    (size N_spec x N_reac) nu'_sr
%  - nu_prod                    (size N_spec x N_reac) nu''_sr
%  - Mm_s                       (size N_spec x 1)
%  - T                          (size N_eta x 1)
%  - y_s                        (size N_eta x N_spec)
%  - p                          (size N_eta x N_spec)
%  - rho                        (size N_eta x 1)
%  - drho_dT                    (size N_eta x 1)
%  - R_s                        (size N_spec x 1)
%  - thetaRot_s                 (size N_spec x 1)
%  - L_s                        (size N_spec x 1)
%  - omega_s                    (size N_spec x 1)
%  - nAtoms_s                   (size N_spec x 1)
%  - thetaVib_s                 (size N_spec x 1)
%  - gDegen_s                   (size N_spec x N_stat)
%  - thetaElec_s                (size N_spec x N_stat)
%  - bElectron                  (size N_spec x 1)
%  - R0,nAvogadro,hPlanck,kBoltz (size 1)
%
% Outputs:
%   ddeltaGr_dT -   reactions' jump in gibbs free energy derivative wrt
%                   temperature
%                   (size N_eta x N_reac)
%
% Notes:
%   inputs are repmat'ed to [N_eta x N_reac x N_spec] where
%   the third dimension corresponds to the species index, s.
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

dgibbs_dT = getSpeciesDer_dgibbs_dT(T,y_s,p,rho,drho_dT,R_s,thetaRot_s,L_s,sigma_s,...
                                            Mm_s,nAtoms_s,thetaVib_s,gDegen_s,...
                                            thetaElec_s,bElectron,R0,nAvogadro,hPlanck,kBoltz,options);
[N_eta,N_spec]  = size(y_s);
N_reac          = size(nu_reac,2);
nu_reac     = repmat(reshape(nu_reac',[1,N_reac,N_spec]),[N_eta,1,1]);
nu_prod     = repmat(reshape(nu_prod',[1,N_reac,N_spec]),[N_eta,1,1]);
Mm_s        = repmat(reshape(Mm_s,[1,1,N_spec]),[N_eta,N_reac,1]);
dgibbs_dT   = repmat(reshape(dgibbs_dT,[N_eta,1,N_spec]),[1,N_reac,1]);

ddeltaGr_dT = zeros([N_eta,N_reac]);
for s=1:N_spec
    ddeltaGr_dT = ddeltaGr_dT + (nu_prod(:,:,s) - nu_reac(:,:,s)).*Mm_s(:,:,s).*dgibbs_dT(:,:,s);
end

end % getReactionDer_ddeltaGr_dT.m
