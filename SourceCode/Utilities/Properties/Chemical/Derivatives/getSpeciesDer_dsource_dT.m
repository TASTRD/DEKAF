function [dsources_dT,dsourcesr_dT] = getSpeciesDer_dsource_dT(TTrans,TRot,TVib,TElec,Tel,y_s,p,A_r,nT_r,theta_r,nu_prod,nu_reac,...
                                                Mm_s,bElectron,R0,qf_r,qb_r,Keq_struc,options)
% getSpeciesDer_dsource_dT returns a matrix with the derivatives of the
% different species source terms with respect to the different temperatures
% in the used thermal model.
%
% Usage:
%   (1)
%       dsources_dT = getSpeciesDer_dsource_dT(TTrans,TRot,TVib,TElec,Tel,...
%                               y_s,p,A_r,nT_r,theta_r,nu_prod,nu_reac,Mm_s,...
%                               bElectron,R0,qf_r,qb_r,Keq_struc,options)
%
%   (2)
%       [dsources_dT,dsourcesr_dT] = getSpeciesDer_dsource_dT(...)
%
% Inputs and outputs:
%  - TTrans         N_eta x 1
%  - TRot           N_eta x 1
%  - TVib           N_eta x 1
%  - TElec          N_eta x 1
%  - Tel            N_eta x 1
%  - y_s            N_eta x N_spec
%  - p              N_eta x 1
%  - A_r            N_reac x 1
%  - nT_r           N_reac x 1
%  - theta_r        N_reac x 1
%  - nu_prod        N_spec x N_reac
%  - nu_reac        N_spec x N_reac
%  - Mm_s           N_spec x 1
%  - bElectron      N_spec x 1
%  - R0             1 x 1
%  - qf_r           N_reac x N_T
%  - qb_r           N_reac x N_T
%  - Keq_struc      structure with several properties necessary for Keq_struc
%  - dsources_dT    N_eta x N_spec x N_T
%  - dsourcesr_dT   N_eta x N_spec x N_T x N_reac
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

options.EoS = protectedvalue(options,'EoS','idealGas');

% obtaining necessary sizes
[N_eta,N_spec] = size(y_s);
[N_T,N_reac] = size(qf_r);
delta_nu = nu_prod - nu_reac; % stoichiometric difference matrix

% obtaining reaction temperatures
switch options.numberOfTemp
    case '1T'
        TGroup = TTrans;
        Tf_r = repmat(TTrans,[1,N_reac]);   % (eta,r)
        Tb_r = repmat(TTrans,[1,N_reac]);   % (eta,r)
        dTfr_dT = ones(N_eta,N_reac);       % (eta,r,iT)
        dTbr_dT = ones(N_eta,N_reac);       % (eta,r,iT)
    case '2T'
        TGroup = [TTrans,TVib];
        Tf_r = getReaction_T(qf_r,TTrans,TVib);             % (eta,r)
        Tb_r = getReaction_T(qb_r,TTrans,TVib);             % (eta,r)
        dTfr_dT = getReactionDer_dTrdT(qf_r,TTrans,TVib);   % (eta,r,iT)
        dTbr_dT = getReactionDer_dTrdT(qb_r,TTrans,TVib);   % (eta,r,iT)
    otherwise
        error(['the chosen thermal models ''',numberOfTemp,''' is not supported']);
end

% Calculating species partial densities and their derivatives
rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);
rho_mat = repmat(rho,[1,N_spec]);
rho_l = y_s.*rho_mat;                                                   % (eta,l)
drho_dT = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,TGroup,options.numberOfTemp);    % (eta,iT)
drhol_dT = getSpeciesDer_drhol_dT(drho_dT,y_s);                             % (eta,l,iT)

% Computing equilibrium temperature
Teq_r = getReaction_Teq(Tf_r,Tb_r);                                         % (eta,r)

% Calculating the reaction rates
lnkf_r = getReaction_lnkf(Tf_r,A_r,nT_r,theta_r);                           % (eta,r)
lnkf_r_Tbr = getReaction_lnkf(Tb_r,A_r,nT_r,theta_r);                       % (eta,r) We need lnkf both with Tf_r and with Tb_r (for lnkb_r)
lnKeq_r = getReaction_lnKeq(Teq_r,TTrans,TRot,TVib,TElec,Tel,delta_nu,Keq_struc,options); % (eta,r)
lnkb_r = lnkf_r_Tbr - lnKeq_r;                                              % backward reaction rate

% Calculating temperature derivatives of the reaction rates
dlnkfr_dTfr = getReactionDer_dlnkfr_dT(Tf_r,nT_r,theta_r);                  % (eta,r)
dlnkfr_dTbr = getReactionDer_dlnkfr_dT(Tb_r,nT_r,theta_r);                  % (eta,r) We need lnkf both with Tf_r and with Tb_r (for lnkb_r)
dlnKeqr_dT = getReactionDerMultiT_dlnKeqr_dT(TTrans,TRot,TVib,TElec,Tel,delta_nu,qf_r,qb_r,Keq_struc,options); % (eta,r,iT)
dlnkfr_dTfr_3D = repmat(dlnkfr_dTfr,[1,1,N_T]);                             % (eta,r) --> (eta,r,iT)
dlnkfr_dTbr_3D = repmat(dlnkfr_dTbr,[1,1,N_T]);                             % (eta,r) --> (eta,r,iT)
dlnkfr_dT = dlnkfr_dTfr_3D.*dTfr_dT;
dlnkbr_dT = dlnkfr_dTbr_3D.*dTbr_dT - dlnKeqr_dT;                            % (eta,r,iT)

% Computing contribution of each reaction and assembling
dsourcesr_dT = getReactionDer_dsourcesr_dT(nu_reac,nu_prod,rho_l,drhol_dT,Mm_s,lnkf_r,lnkb_r,dlnkfr_dT,dlnkbr_dT);

dsources_dT = zeros(N_eta,N_spec,N_T);
for r=1:N_reac
    dsources_dT = dsources_dT + dsourcesr_dT(:,:,:,r); % (eta,s,iT)
end

end % getSpeciesDer_dsource_dT.m
