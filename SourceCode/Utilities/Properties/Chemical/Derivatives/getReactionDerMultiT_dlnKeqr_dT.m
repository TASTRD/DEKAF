function dlnKeqr_dT = getReactionDerMultiT_dlnKeqr_dT(TTrans,TRot,TVib,TElec,Tel,delta_nu,qf_r,qb_r,Keq_struc,options)
% getReactionDerMultiT_dlnKeqr_dT computes the derivative of natural
% logarithm of equilibrium constant of reaction r wrt a temperature
%
% Usage:
%   (1)
%       dlnKeqr_dT = getReactionDerMultiT_dlnKeqr_dT(TTrans,TRot,TVib,
%                           TElec,Tel,delta_nu,qf_r,qb_r,Keq_struc,options)
%       |--> two temperature model
%
% Inputs:
%  - TTrans         translational temperature [K]. Vector of size (N_eta x 1)
%  - TRot           rotational temperature [K]. Vector of size (N_eta x 1)
%  - TVib           vibrational temperature [K]. Vector of size (N_eta x 1)
%  - TElec          electronic temperature [K]. Vector of size (N_eta x 1)
%  - Tel            electron temperature [K]. Vector of size (N_eta x 1)
%  - delta_nu       (size N_spec x N_reac) nu''_sr-nu'_sr
%  - qf_r           coefficient in the geometric average defining the
%                   reaction temperature for forward reactions [-].
%                   Vector of size (N_reac x 1)
%  - qb_r           coefficient in the geometric average defining the
%                   reaction temperature for backward reactions [-].
%                   Vector of size (N_reac x 1)
%  - Keq_struc      structure
%  - options        structure
%
% Outputs:
%   dlnKeqr_dT  -   natural logarithm of reaction equilibrium constant
%                   derivative wrt temperature
%                   (size N_eta x N_reac x N_T)
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_eta = length(TTrans);
N_spec = size(delta_nu,1);
[N_T,N_reac] = size(qf_r);

% Obtaining equilibrium temperatures
switch options.numberOfTemp
    case '1T'
        % set of temperatures for each energy group and derivatives
        TGroup = TTrans;
        Tf_r = repmat(TTrans,[1,N_reac]);
        Tb_r = repmat(TTrans,[1,N_reac]);
        dTfr_dT = ones(N_eta,N_reac);
        dTbr_dT = ones(N_eta,N_reac);
    case '2T'
        % set of temperatures for each energy group and derivatives
        TGroup = [TTrans,TVib];
        Tf_r = getReaction_T(qf_r,TTrans,TVib);
        Tb_r = getReaction_T(qb_r,TTrans,TVib);
        dTfr_dT = getReactionDer_dTrdT(qf_r,TTrans,TVib);
        dTbr_dT = getReactionDer_dTrdT(qb_r,TTrans,TVib);
    otherwise
        error(['the chosen thermal model''',options.numberOfTemp,''' is not supported']);
end

% Computing equilibrium temperature and its derivatives
Teq_r = getReaction_Teq(Tf_r,Tb_r);                             % (eta,r)
dTeqr_dT = getReactionDer_dTeqdT(Tf_r,Tb_r,dTfr_dT,dTbr_dT);    % (eta,r,iT)

switch options.modelEquilibrium
    case 'Park85'
        % Extract Aeq_r from input struct
        Aeq_r = Keq_struc.Aeq_r;

        % for LTE we only want the equilibrium constants for the reactions used
        % in the equilibrium system (for air5: O2 diss, N2 diss and NO diss)
        if ismember(options.flow,{'LTE','LTEED'})
            idx_eq = Keq_struc.idx_eq;
            Aeq_r = Aeq_r(idx_eq,:);
        end

        % reshaping
        [N_reac,N_order] = size(Aeq_r);
        Aeq_r = repmat(reshape(Aeq_r,[1,N_reac,N_order]),[N_eta,1,1]);

        % Obtaining Z coefficient and evaluating Keq_r
        Z = 10000./Teq_r;
        dlnKeqr_dTeqr = Aeq_r(:,:,2) + 2*Aeq_r(:,:,3) .* (Z) + 3*Aeq_r(:,:,4) .* (Z).^2 + 4*Aeq_r(:,:,5) .* (Z).^3;
        dlnKeqr_dT = dlnKeqr_dTeqr.* dTeqr_dT;

    case 'Arrhenius'
        % Extract Aeq_r from input struct
        nT_eqr = Keq_struc.nT_eqr;
        theta_eqr = Keq_struc.theta_eqr;

        % for LTE we only want the equilibrium constants for the reactions used
        % in the equilibrium system (for air5: O2 diss, N2 diss and NO diss)
        if ismember(options.flow,{'LTE','LTEED'})
            idx_eq = Keq_struc.idx_eq;
            nT_eqr = nT_eqr(idx_eq);
            theta_eqr = theta_eqr(idx_eq);
        end

        nT_eqr = repmat(nT_eqr.',[N_eta,1]);
        theta_eqr = repmat(theta_eqr.',[N_eta,1]);

        dlnKeqr_dTeqr = nT_eqr./Teq_r + theta_eqr./Teq_r.^2;
        dlnKeqr_dT = dlnKeqr_dTeqr.* dTeqr_dT;

    case 'RRHO'
        % extracting from inputs
        R_s         = Keq_struc.R_s;
        nAtoms_s    = Keq_struc.nAtoms_s;
        thetaVib_s  = Keq_struc.thetaVib_s;
        thetaElec_s = Keq_struc.thetaElec_s;
        gDegen_s    = Keq_struc.gDegen_s;
        bElectron   = Keq_struc.bElectron;
        hForm_s     = Keq_struc.hForm_s;

        % Computing sensible internal energies
        [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,...
            hForm_s_mat,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat] = ...
            reshape_inputs4enthalpy(R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,...
            bElectron,hForm_s,TTrans,TRot,TVib,TElec,Tel); % reshaping to the sizes getSpecies_h_cp needs

        [~,~,~,hMod_s] = getSpecies_h_cp(TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat,R_s_mat,...
            nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
            bElectron_mat,hForm_s_mat,options);

        T_s = TTrans_mat;                                                   % computing species translational temperature
        T_s(:,bElectron) = Tel_mat(:,bElectron);
        eTrans_s = hMod_s.hTrans_s - R_s_mat.*T_s;                          % species translational internal energy

        switch options.numberOfTemp
            case '1T'
                % computing species enthalpies
                hGroupSens_s = eTrans_s + hMod_s.hRot_s + hMod_s.hVib_s + hMod_s.hElec_s;
            case '2T'
                % computing species sensible enthalpies for each energy group
                hGroupSens_s(:,:,1) = eTrans_s.*(~bElectron_mat) + hMod_s.hRot_s;
                hGroupSens_s(:,:,2) = eTrans_s.*bElectron_mat + hMod_s.hVib_s + hMod_s.hElec_s;
            otherwise
                error(['the chosen thermal model''',options.numberOfTemp,''' is not supported']);
        end

        % using the kinetical definintion of the enthalpy to obtain the
        % derivative of the logarithm of the partition function
        TGroup_3D = repmat(permute(TGroup,[1,3,2]),[1,N_spec,1]);       % (eta,iT) --> (eta,s,iT)
        R_s_mat3D = repmat(R_s_mat,[1,1,N_T]);                          % (eta,s) --> (eta,s,iT)
        dlnQs_dT = hGroupSens_s./(R_s_mat3D.*TGroup_3D.^2);             % (eta,s,iT)

        % Assembling with the formation enthalpy and friends
        deltanu_sr_4D = repmat(permute(delta_nu,[3,2,4,1]),[N_eta,1,N_T,1]);            % (s,r) --> (eta,r,iT,s)
        dlnQs_dT_4D = repmat(permute(dlnQs_dT,[1,4,3,2]),[1,N_reac,1,1]);               % (eta,s,iT) --> (eta,r,iT,s)
        Teq_r_3D = repmat(Teq_r,[1,1,N_T]);                                             % (eta,r) --> (eta,r,iT)

        dlnKeqr_dT = zeros(N_eta,N_reac,N_T);           % allocating dlnKeqr_dT (eta,r,iT)
        for s = 1:N_spec                                % suming over the species
            dlnKeqr_dT = dlnKeqr_dT + deltanu_sr_4D(:,:,:,s) .* (dlnQs_dT_4D(:,:,:,s) + hForm_s(s)./(R_s(s)*Teq_r_3D.^2).*dTeqr_dT);
        end

    otherwise
        error(['the chosen Keq model ''',options.modelEquilibrium,''' is not supported.']);
end
end % getReactionDer_dlnKeqr_dT.m
