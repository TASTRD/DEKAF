function dsourcesr_dym = getReactionDer_dsourcesr_dym(nu_reac,nu_prod,Mm_s,rho_l,drhol_dym,lnkf_r,lnkb_r)
% getReactionDer_dsourcesr_dym returns a 4D matrix with the derivatives with
% respect to the species mass fractions y_m of the contribution of the
% different reactions to the different species source terms.
%
% Usage:
%   (1)
%       dsourcesr_dym = getReactionDer_dsourcesr_dym(nu_reac,nu_prod,...
%                                       Mm_s,rho_l,drhol_dym,lnkf_r,lnkb_r)
% Inputs and outputs:
%  - nu_prod        N_spec x N_reac
%  - nu_reac        N_spec x N_reac
%  - Mm_s           N_spec x 1
%  - rho_l          N_eta x N_spec(l)
%  - drhol_dym      N_eta x N_spec(m) x N_spec(l)
%  - lnkf_r         N_eta x N_reac
%  - lnkb_r         N_eta x N_reac
%  - dsourcesr_dym  N_eta x N_spec(m) x N_spec(s) x N_reac
%
% the output has size N_eta x N_spec x N_spec x N_reac, corresponding to
% subindices eta - m - s - r. For example, the derivative of the
% contribution of reaction 11 to the source term of species 4 with respect
% to the mass fraction of species 2 will be in pos. (:,2,4,11)
%
% See also: listOfVariablesExplained
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0


% obtaining sizes
[N_eta,N_spec,~] = size(drhol_dym);
N_reac = size(nu_reac,2);

% Reshaping variables such that they have the desired shapes
delta_nu = nu_prod - nu_reac; % stoichiometric difference matrix
rho4D_l          = repmat(permute(rho_l,[1,3,4,2]),[1,N_spec,N_reac,1]);    % (eta,l) --> (eta,m,r,l)
drhol_dym4D      = repmat(permute(drhol_dym,[1,2,4,3]),[1,1,N_reac,1]);     % (eta,m,l) --> (eta,m,r,l)
nu_reac_rl       = repmat(permute(nu_reac,[3,4,2,1]),[N_eta,N_spec,1,1]);   % (l,r) --> (eta,m,r,l)
nu_prod_rl       = repmat(permute(nu_prod,[3,4,2,1]),[N_eta,N_spec,1,1]);   % (l,r) --> (eta,m,r,l)
lnkf3D_r         = repmat(permute(lnkf_r,[1,3,2]),[1,N_spec,1]);            % (eta,r) --> (eta,m,r)
lnkb3D_r         = repmat(permute(lnkb_r,[1,3,2]),[1,N_spec,1]);            % (eta,r) --> (eta,m,r)

% building product terms
prod_f = zeros(N_eta,N_spec,N_reac);
prod_b = zeros(N_eta,N_spec,N_reac);
prod_dfdym = zeros(N_eta,N_spec,N_reac);
prod_dbdym = zeros(N_eta,N_spec,N_reac);
for l = 1:N_spec
    prod_f = prod_f + log(rho4D_l(:,:,:,l) ./ Mm_s(l)) .* (nu_reac_rl(:,:,:,l)); % (eta,m,r)
    prod_b = prod_b + log(rho4D_l(:,:,:,l) ./ Mm_s(l)) .* (nu_prod_rl(:,:,:,l)); % (eta,m,r)
    prod_dfdym = prod_dfdym + nu_reac_rl(:,:,:,l)./rho4D_l(:,:,:,l) .* drhol_dym4D(:,:,:,l); % (eta,m,r)
    prod_dbdym = prod_dbdym + nu_prod_rl(:,:,:,l)./rho4D_l(:,:,:,l) .* drhol_dym4D(:,:,:,l); % (eta,m,r)
end

dAfr_dym = exp(lnkf3D_r + prod_f) .* (prod_dfdym);              % (eta,m,r)
dAbr_dym = exp(lnkb3D_r + prod_b) .* (prod_dbdym);

% repeating the product terms over s, and then adding over the reactions
dAfr_dym4D  = repmat(permute(dAfr_dym,[1,2,4,3]),[1,1,N_spec,1]);           % (eta,m,r) --> (eta,m,s,r)
dAbr_dym4D  = repmat(permute(dAbr_dym,[1,2,4,3]),[1,1,N_spec,1]);           % (eta,m,r) --> (eta,m,s,r)
Mm_s4D      = repmat(permute(Mm_s,[2,3,1]),[N_eta,N_spec,1,N_reac]);        % (s,1)     --> (eta,m,s,r)
deltanu_sr  = repmat(permute(delta_nu,[3,4,1,2]),[N_eta,N_spec,1,1]);       % (s,r)     --> (eta,m,s,r)

dsourcesr_dym = Mm_s4D.*deltanu_sr.*(dAfr_dym4D - dAbr_dym4D);

end % getReactionDer_dsourcesr_dym