function ddeltaGr_dym = getReactionDer_ddeltaGr_dym(nu_reac,nu_prod,Mm_s,T,p,rho,y_s,drho_dym,R0,bElectron)
%GETREACTIONDER_DDELTAGR_DYM Derivative of reactions' jump in gibbs free
% energy wrt species mass fractions
%
% Inputs:
%   nu_reac     -   reactant coefficient matrix, nu'_sr
%                   [N_spec x N_reac]
%   nu_prod     -   product coefficient matrix, nu''_sr
%                   [N_spec x N_reac]
%   Mm_s        -   species molar mass
%                   [N_spec x 1]
%   T           -   Temperature
%                   [N_eta x 1]
%   p           -   Mixture pressure
%                   [N_eta x 1]
%   rho         -   Mixture density
%                   [N_eta x 1]
%   y_s         -   Mixture density
%                   [N_eta x N_spec]
%   drho_dym    -   Mixture density
%                   [N_eta x N_spec]
%   R0          -   Universal gas constant
%                   [1]
%   bElectron   -   electron boolean
%                   [N_spec x 1]
%
% Outputs:
%   ddeltaGr_dym-   reactions' jump in gibbs free energy wrt species mass
%                   fractions
%                   [N_eta x N_spec x N_reac]
%
% Notes:
%   inputs are repmat'ed to [N_eta x N_spec x N_reac x N_spec] where
%   the second dimension corresponds to the first  species index, m, and...
%   the fourth dimension corresponds to the second species index, s.
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

dgibbs_dym  = getSpeciesDer_dgibbs_dym(T,T,p,rho,y_s,drho_dym,Mm_s,R0,bElectron);

[N_eta,N_spec,~]= size(dgibbs_dym);
N_reac          = size(nu_reac,2);
nu_reac     = repmat(reshape(nu_reac',[1,1,N_reac,N_spec]),[N_eta,N_spec,1,1]);
nu_prod     = repmat(reshape(nu_prod',[1,1,N_reac,N_spec]),[N_eta,N_spec,1,1]);
Mm_s        = repmat(reshape(Mm_s,[1,1,1,N_spec]),[N_eta,N_spec,N_reac,1]);
dgibbs_dym  = repmat(reshape(dgibbs_dym,[N_eta,N_spec,1,N_spec]),[1,1,N_reac,1]);

ddeltaGr_dym = zeros([N_eta,N_spec,N_reac]);
for s=1:N_spec
    ddeltaGr_dym = ddeltaGr_dym + (nu_prod(:,:,:,s) - nu_reac(:,:,:,s)).*Mm_s(:,:,:,s).*dgibbs_dym(:,:,:,s);
end

end % getReactionDer_ddeltaGr_dym.m
