function [dsources_dym,dsourcesr_dym] = getSpeciesDer_dsource_dym(TTrans,TRot,TVib,TElec,Tel,y_s,p,A_r,nT_r,theta_r,...
                                nu_prod,nu_reac,Mm_s,bElectron,R0,qf_r,qb_r,Keq_struc,options)
% getSpeciesDer_dsource_dym returns a 3D matrix with the derivatives with
% respect to the species mass fractions y_m of the different species source terms.
%
% Usage:
%   (1)
%       dsources_dym = getSpeciesDer_dsource_dym(TTrans,TRot,TVib,TElec,Tel,...
%                               y_s,p,A_r,nT_r,theta_r,nu_prod,nu_reac,Mm_s,...
%                               bElectron,R0,qf_r,qb_r,Keq_struc,options)
%
%   (2)
%       [dsources_dym,dsourcesr_dym] = getSpeciesDer_dsource_dym(...)
%       |--> returns also the contribution of each reaction to dsources_dym
%
% Inputs and outputs:
%  - TTrans         N_eta x 1
%  - TRot           N_eta x 1
%  - TVib           N_eta x 1
%  - TElec          N_eta x 1
%  - Tel            N_eta x 1
%  - y_s            N_eta x N_spec
%  - p              N_eta x 1
%  - A_r            N_reac x 1
%  - nT_r           N_reac x 1
%  - theta_r        N_reac x 1
%  - nu_prod        N_spec x N_reac
%  - nu_reac        N_spec x N_reac
%  - Mm_s           N_spec x 1
%  - bElectron      N_spec x 1
%  - R0             1 x 1
%  - qf_r           N_reac x N_T
%  - qb_r           N_reac x N_T
%  - Keq_struc      structure with several properties necessary for Keq_struc
%  - dsources_dym   N_eta x N_spec(m) x N_spec(s)
%  - dsourcesr_dym  N_eta x N_spec(m) x N_spec(s) x N_reac
%
% the output has size N_eta x N_spec x N_spec, corresponding to subindices
% eta - m - s. For example, the derivative of the source term of species 4
% with respect to the mass fraction of species 2 will be in pos. (:,2,4)
%
% See also: listOfVariablesExplained
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

options.EoS = protectedvalue(options,'EoS','idealGas');

% obtaining necessary sizes
N_eta = length(TTrans);
[N_spec,N_reac] = size(nu_reac);
delta_nu = nu_prod - nu_reac; % stoichiometric difference matrix

% Calculating species partial densities and their derivatives
rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);
rho_mat     = repmat(rho,[1,N_spec]);
rho_l       = y_s.*rho_mat;                                                 % (eta,l)
drho_dym    = getMixtureDer_drho_dym(p,y_s,R0,Mm_s,bElectron,TTrans,Tel);   % (eta,m)
drhol_dym   = getSpeciesDer_drhol_dym(rho,drho_dym,y_s);                    % (eta,m,l)

% obtaining reaction temperatures
switch options.numberOfTemp
    case '1T'
%         TGroup_cell = {TTrans};
        Tf_r = repmat(TTrans,[1,N_reac]);   % (eta,r)
        Tb_r = repmat(TTrans,[1,N_reac]);   % (eta,r)
    case '2T'
%         TGroup_cell = {TTrans,TVib};
        Tf_r = getReaction_T(qf_r,TTrans,TVib);             % (eta,r)
        Tb_r = getReaction_T(qb_r,TTrans,TVib);             % (eta,r)
    otherwise
        error(['the chosen thermal models ''',numberOfTemp,''' is not supported']);
end

% Computing equilibrium temperature
Teq_r = getReaction_Teq(Tf_r,Tb_r);                                         % (eta,r)

% Calculating the reaction rates
lnkf_r = getReaction_lnkf(Tf_r,A_r,nT_r,theta_r);                           % (eta,r)
lnKeq_r = getReaction_lnKeq(Teq_r,TTrans,TRot,TVib,TElec,Tel,delta_nu,Keq_struc,options); % (eta,r)
lnkf_r_atTbr = getReaction_lnkf(Tb_r,A_r,nT_r,theta_r); % forward reaction rate at Tb_r
lnkb_r = lnkf_r_atTbr - lnKeq_r;                                            % (eta,r)

% computing source term derivatives
dsourcesr_dym = getReactionDer_dsourcesr_dym(nu_reac,nu_prod,Mm_s,rho_l,drhol_dym,lnkf_r,lnkb_r);
dsources_dym = zeros(N_eta,N_spec,N_spec);
for r=1:N_reac
    dsources_dym = dsources_dym + dsourcesr_dym(:,:,:,r);
end

end % getSpeciesDer_dsource_dym