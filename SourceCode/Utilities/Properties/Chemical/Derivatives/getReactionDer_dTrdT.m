function dTr_dT = getReactionDer_dTrdT(q_r,varargin)
% getReactionDer_dTrdT obtains the derivative of the reaction temperature
% with respect to each of the temperatures.
%
% Usage:
%   (1)
%       dTr_dT = getReactionDer_dTrdT(qMod_r,T1,T2)
%       |--> two temperatures
%
%   (2)
%       dTr_dT = getReactionDer_dTrdT(qMod_r,T1,...,TN)
%       |--> N temperatures
%
% Inputs & Outputs:
%   q_r         size N_T x N_reac
%   T1,T2,...   size N_eta x 1
%   dTr_dT      size N_eta x N_reac x N_T
%
% See also: getReaction_T, listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% temperature of each mode
TMod = [varargin{:}];

% reshaping and repmatting
[N_eta,N_T] = size(TMod);
N_reac = size(q_r,2);
TMod = repmat(reshape(TMod,[N_eta,1,N_T]),[1,N_reac,1]);    % (eta,iT) --> (eta,r,iT)
q_r = repmat(permute(q_r,[3,2,1]),[N_eta,1,1]);       % (iT,r) --> (eta,r,iT)

% Computing all derivatives vectorially
prod = ones(N_eta,N_reac);
for iT = 1:N_T
    prod = prod .* TMod(:,:,iT).^q_r(:,:,iT);
end
prod_3D = repmat(prod,[1,1,N_T]); % (eta,r) --> (eta,r,iT)
dTr_dT = q_r./TMod .* prod_3D;