function source_sr = getReaction_source(lnkf_r,lnkb_r,rho,y_s,Mm_s,nu_reac,nu_prod)
%GETREACTION_SOURCE Compute the mass generation source term per unit volume
% for a chemically-reacting species
%
% source_sr=getReaction_source(lnkf_r,lnkb_r,rho,y_s,Mm_s,nu_reac,nu_prod);
%
% GETREACTION_SOURCE computes the species source terms due to every species
% (matrix of size N_eta x N_spec x N_reac), for a given set of forward and
% backward reaction rates, species mass fractions, a mixture density and
% chemistry properties. The required inputs are:
%
% Inputs:
%   lnkf_r      -   natural logarithm of the forward reaction constant
%                   [mol/s *(mol/m^3)^(sum(nu_reac(s,r)))
%                   Matrix of size N_eta x N_reac
%   lnkb_r      -   natural logarithm of the backward reaction constant
%                   [mol/s *(mol/m^3)^(sum(nu_prod(s,r)))
%                   Matrix of size N_eta x N_reac
%   rho         -   mixture density [kg/m^3]. Vector of size (N_eta x 1)
%   y_s         -   species mass fractions [-]
%                   Matrix of size (N_eta x N_spec)
%   Mm_s        -   Molecular molar mass. [kg/mol].
%                   Vector of size (N_spec x 1)
%   nu_reac     -   stoichiometric coefficient of each species as a
%                   reactant of each chemical reaction [-].
%                   Matrix of size (N_spec x N_reac).
%   nu_prod     -   stoichiometric coefficient of each species as a product
%                   of each chemical reaction [-].
%                   Matrix of size (N_spec x N_reac).
%
% Outputs:
%   source_sr   -   Species mass source term    [kg/(m^3*s)]
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% obtaining necessary sizes
[N_eta,N_spec] = size(y_s);
[~,N_reac] = size(lnkf_r);

% Reshaping variables such that they have the desired shapes
Mm3D_s = repmat(Mm_s',[N_eta,1,N_reac]);                                % (s,1) ----> (eta,s,r)
nu3D_reac = repmat(reshape(nu_reac,[1,N_spec,N_reac]),[N_eta,1,1]);     % (s,r) ----> (eta,s,r)
nu3D_prod = repmat(reshape(nu_prod,[1,N_spec,N_reac]),[N_eta,1,1]);     % (s,r) ----> (eta,s,r)
lnkf3D_r = repmat(reshape(lnkf_r,[N_eta,1,N_reac]),[1,N_spec,1]);       % (eta,r) --> (eta,s,r)
lnkb3D_r = repmat(reshape(lnkb_r,[N_eta,1,N_reac]),[1,N_spec,1]);       % (eta,r) --> (eta,s,r)

rho_s = y_s .* repmat(rho,[1,N_spec]); % building vector of species densities (eta,s)

rho4D_l = repmat(reshape(rho_s,[N_eta,1,1,N_spec]),[1,N_spec,N_reac,1]);        % (eta,l) --> (eta,s,r,l)
nu4D_reac = repmat(reshape(nu_reac',[1,1,N_reac,N_spec]),[N_eta,N_spec,1,1]);   % (l,r) ----> (eta,s,r,l)
nu4D_prod = repmat(reshape(nu_prod',[1,1,N_reac,N_spec]),[N_eta,N_spec,1,1]);   % (l,r) ----> (eta,s,r,l)

% building product terms
prod_f = zeros(N_eta,N_spec,N_reac);
prod_b = zeros(N_eta,N_spec,N_reac);
for l = 1:N_spec
    prod_f = prod_f + log(rho4D_l(:,:,:,l) ./ Mm_s(l)) .* (nu4D_reac(:,:,:,l)); % (eta,s,r)
    prod_b = prod_b + log(rho4D_l(:,:,:,l) ./ Mm_s(l)) .* (nu4D_prod(:,:,:,l)); % (eta,s,r)
end
prod_f = prod_f + lnkf3D_r;
prod_b = prod_b + lnkb3D_r;

% building species source terms due to every reaction
source_sr = Mm3D_s .* (nu3D_prod-nu3D_reac) .* (exp(prod_f) - exp(prod_b));

end
