function deltaG_r = getReaction_deltaGr(nu_reac,nu_prod,T,p,R_s,Mm_s,thetaRot_s,L_s,...
                                        sigma_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,...
                                        kBoltz,hPlanck,nAvogadro,hForm_s)
%GETREACTION_DELTAGR Calculate reaction jump in gibbs free energy
%
%  - nu_reac                    (size N_spec x N_reac) nu'_sr
%  - nu_prod                    (size N_spec x N_reac) nu''_sr
%  - p                          (size N_eta x 1)
%  - T                          (size N_eta x 1)
%  - R_s                        (size N_spec x 1)
%  - Mm_s                       (size N_spec x 1)
%  - thetaRot_s                 (size N_spec x 1)
%  - L_s                        (size N_spec x 1)
%  - sigma_s                    (size N_spec x 1)
%  - thetaVib_s                 (size N_spec x 1)
%  - thetaElec_s                (size N_spec x N_stat)
%  - gDegen_s                   (size N_spec x N_stat)
%  - bElectron                  (size N_spec x 1)
%  - nAvogadro,hPlanck,kBoltz   (size 1)
%  - hForm_s                    (size N_spec x 1)
%
% Output: deltaG_r              (size N_eta x N_reac)
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

[N_spec,N_reac] = size(nu_reac);
N_eta = length(T);

gibbs_s = getSpecies_gibbs(T,p,R_s,Mm_s,thetaRot_s,L_s,sigma_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,kBoltz,hPlanck,nAvogadro,hForm_s);

nu_reac = repmat(reshape(nu_reac',[1,N_reac,N_spec]),[N_eta,1,1]);
nu_prod = repmat(reshape(nu_prod',[1,N_reac,N_spec]),[N_eta,1,1]);
gibbs_s = repmat(reshape(gibbs_s,[N_eta,1,N_spec]) ,[1,N_reac,1]);

deltaG_r = zeros([N_eta,N_reac]);
for s=1:N_spec
    deltaG_r = deltaG_r + (nu_prod(:,:,s)-nu_reac(:,:,s)).*Mm_s(s).*gibbs_s(:,:,s);
end

end % getReaction_deltaGr.m
