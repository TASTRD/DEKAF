function lnKeq_r = getReaction_lnKeq(Teq_r,TTrans,TRot,TVib,TElec,Tel,delta_nu,Keq_struc,options)
%GETREACTION_LNKEQ Compute the natural logarithm of the equilibrium
% constants for user-specified chemical reactions
%
% Usage:
%   (1)
%       lnKeq_r = getReaction_lnKeq(Teq_r,TTrans,TRot,TVib,TElec,Tel,delta_nu,Keq_struc,options);
%
% Inputs:
%   Teq_r       -   temperature driving the equilibrium of each reaction [K]. Vector of size (N_eta x N_reac)
%   TTrans      -   translational temperature [K]. Vector of size (N_eta x 1)
%   TRot        -   rotational temperature [K]. Vector of size (N_eta x 1)
%   TVib        -   vibrational temperature [K]. Vector of size (N_eta x 1)
%   TElec       -   electronic temperature [K]. Vector of size (N_eta x 1)
%   Tel         -   electron temperature [K]. Vector of size (N_eta x 1)
%   delta_nu    -   stoichiometric jump matrix (prod-reac) of the reactions (N_spec x N_reac)
%   Keq_struc   -   struct containing the following fields depending on options.modelEquilibrium:
%
%    1) options.modelEquilibrium = 'Park85'
%
%           It uses Park's 1985 polynomial curve fit:
%
%               lnKeq = exp(A1 + A2*Z + ... + A5*Z^4)       with Z=10000/T
%
%            .Aeq_r
%               five coefficients (A1, A2, ..., A5) in the polynomial fit
%               of the reaction equilibrium constant. Matrix of size (N_reac x 5).
%            .T0_eq
%               minimum temperature up to which the fitting is applicable. (1 x 1) [K]
%            .idx_eq
%               index vector detailing which reactions are to be used for
%               the LTE equilibrium system.
%
%
%    2) options.modelEquilibrium = 'RRHO'
%
%           It uses the expression for the equilibrium constant coming from
%           the partition functions of the different energy modes assuming
%           a Rigid-Rotor & Harmonic-Oscillator.
%
%            .Mm_s                  (N_spec x 1)        [kg/mol]
%            .R_s                   (N_spec x 1)        [J/kg-K]
%            .thetaRot_s            (N_spec x 1)        [K]
%            .thetaVib_s            (N_spec x 1)        [K]
%            .thetaElec_s           (N_spec x 1)        [K]
%            .L_s                   (N_spec x 1)        [-]
%            .sigma_s               (N_spec x 1)        [-]
%            .gDegen_s              (N_spec x N_mod)    [-]
%            .bElectron             (N_spec x 1)        [-]
%            .kBoltz                (1 x 1)             [m² kg/s²-K]
%            .hPlanck               (1 x 1)             [J s]
%            .nAvogadro             (1 x 1)             [1/mol]
%            .hForm_s               (N_spec x 1)        [J/kg]
%
%    3) options.modelEquilibrium = 'Arrhenius'
%
%           It uses an Arrhenius-type expression:
%               lnKeq = ln(A_eqr) + nT_eqr * ln(T) - theta_eqr/T
%
%            .A_eqr                 (N_reac x 1)  [(m^3/mol)^sum(delta_nu) * K^(1/nT_r)]
%            .nT_eqr                (N_reac x 1)        [-]
%            .theta_eqr             (N_reac x 1)        [K]
%            .idx_eq
%               index vector detailing which reactions are to be used for
%               the LTE equilibrium system.
%
%    4) options.modelEquilibrium = 'Park90'
%
%           UNSUPPORTED
%
%   options -   structure containing relevant options, such as modelEquilibrium,
%               which specifies what model is to be used for keq
%
% Outputs:
%   lnKeq   -   natural logarithm of the equilibrium constants of the
%               chemical reactions described by delta_nu.
%               matrix size of (N_eta x N_reac)
%
% Notes:
% The natural logarithm is computed to avoid MATLAB's integer overflow
%
% see also: setDefaults
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% obtaining domain length
N_eta = size(Teq_r,1);

switch options.modelEquilibrium
    case 'Park85'
        % Extract Aeq_r from input struct
        Aeq_r = Keq_struc.Aeq_r;
        T0_eq = Keq_struc.T0_eq;

        % for LTE we only want the equilibrium constants for the reactions used
        % in the equilibrium system (for air5: O2 diss, N2 diss and NO diss)
        if ismember(options.flow,{'LTE','LTEED'})
            idx_eq = Keq_struc.idx_eq;
            Aeq_r = Aeq_r(idx_eq,:);
        end

        % reshaping
        [N_reac,N_order] = size(Aeq_r);
        Aeq_r = repmat(reshape(Aeq_r,[1,N_reac,N_order]),[N_eta,1,1]);

        % Obtaining Z coefficient and evaluating Keq_r
        Z = 10000./Teq_r;
        lnKeq_r = Aeq_r(:,:,1) + Aeq_r(:,:,2) .* Z + Aeq_r(:,:,3) .* (Z).^2 + Aeq_r(:,:,4) .* (Z).^3 + Aeq_r(:,:,5) .* (Z).^4;

        % applying correction (cutting off at T0_eq)
        Z0 = 10000/T0_eq;
        gT = 0.5*(1-tanh(Teq_r-T0_eq));
        lnKeq_r0 = Aeq_r(:,:,1) + Aeq_r(:,:,2) .* Z0 + Aeq_r(:,:,3) .* (Z0).^2 + Aeq_r(:,:,4) .* (Z0).^3 + Aeq_r(:,:,5) .* (Z0).^4;
        %     lnKeqr1 = lnKeq_r0; % cutting off at the value at T0_eq

        % making linear after T0_eq
        dZ_dT0 = -10000./T0_eq.^2;
        dlnKeqr_dT0 =   dZ_dT0 .* (Aeq_r(:,:,2) +    2*Aeq_r(:,:,3) .* (Z0) +  3*Aeq_r(:,:,4) .* (Z0).^2 + 4*Aeq_r(:,:,5) .* (Z0).^3);
        lnKeq_r1 = lnKeq_r0 + dlnKeqr_dT0.*(Teq_r-T0_eq);

        lnKeq_r = gT.*lnKeq_r1 + (1-gT).*lnKeq_r;

    case 'Arrhenius'
        % Extract Aeq_r from input struct
        A_eqr = Keq_struc.A_eqr;
        nT_eqr = Keq_struc.nT_eqr;
        theta_eqr = Keq_struc.theta_eqr;

        % for LTE we only want the equilibrium constants for the reactions used
        % in the equilibrium system (for air5: O2 diss, N2 diss and NO diss)
        if ismember(options.flow,{'LTE','LTEED'})
            idx_eq = Keq_struc.idx_eq;
            A_eqr = A_eqr(idx_eq);
            nT_eqr = nT_eqr(idx_eq);
            theta_eqr = theta_eqr(idx_eq);
        end

        lnKeq_r = getReaction_lnkf(Teq_r,A_eqr,nT_eqr,theta_eqr); % using the same function as for forward reaction rates

    case 'RRHO'
        % extracting from the Keq structure
        Mm_s        = Keq_struc.Mm_s;
        R_s         = Keq_struc.R_s;
        thetaRot_s  = Keq_struc.thetaRot_s;
        L_s         = Keq_struc.L_s;
        sigma_s     = Keq_struc.sigma_s;
        thetaVib_s  = Keq_struc.thetaVib_s;
        thetaElec_s = Keq_struc.thetaElec_s;
        gDegen_s    = Keq_struc.gDegen_s;
        bElectron   = Keq_struc.bElectron;
        kBoltz      = Keq_struc.kBoltz;
        hPlanck     = Keq_struc.hPlanck;
        nAvogadro   = Keq_struc.nAvogadro;
        hForm_s     = Keq_struc.hForm_s;

        % important sizes
        [N_spec,N_reac] = size(delta_nu);

        % obtaining partition functions of the different energy modes
        lnQTransV_s =   getSpecies_lnQTransV(TTrans,Tel,Mm_s,nAvogadro,hPlanck,kBoltz,bElectron);
        if ~options.neglect_hRot;       lnQRot_s =   getSpecies_lnQRot(TRot,thetaRot_s,L_s,sigma_s);
        else;                           lnQRot_s =   zeros(size(lnQTransV_s));
        end
        if ~options.neglect_hVib;       lnQVib_s =   getSpecies_lnQVib(TVib,thetaVib_s);
        else;                           lnQVib_s =   zeros(size(lnQTransV_s));
        end
        if ~options.neglect_hElec;      lnQElec_s =  getSpecies_lnQElec(TElec,gDegen_s,thetaElec_s,bElectron);
        else;                           lnQElec_s =  zeros(size(lnQTransV_s));
        end
        lnQ_s       =   lnQTransV_s + lnQRot_s + lnQVib_s + lnQElec_s;

        % repmatting to make the other operations vectorially
        delta_nu = repmat(reshape(delta_nu',[1,N_reac,N_spec]),[N_eta,1,1]);
        lnQ_s = repmat(reshape(lnQ_s,[N_eta,1,N_spec]),[1,N_reac,1]);

        % computing lnKeq
        lnKeq_r = zeros(N_eta,N_reac);
        for s = 1:N_spec
            lnKeq_r = lnKeq_r + delta_nu(:,:,s) .* (lnQ_s(:,:,s) - log(nAvogadro) - hForm_s(s)./(R_s(s).*Teq_r));
        end

    case 'Park90'
        error('Keq model Park90 is currently unsupported.');
        lnKeq_r = Aeq_r(:,:,1) ./ Z + Aeq_r(:,:,2) + Aeq_r(:,:,3) .* log(Z) + Aeq_r(:,:,4) .* Z + Aeq_r(:,:,5) .* Z.^2;
    otherwise
        error(['the chosen modelEquilibrium ''',options.modelEquilibrium,''' is not supported.']);
end

end % getReaction_lnKeq
