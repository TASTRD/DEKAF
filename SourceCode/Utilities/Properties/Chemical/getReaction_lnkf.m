function lnkf_r = getReaction_lnkf(T_r,A_r,nT_r,theta_r)
%GETREACTION_LNKF Compute the forward reaction rate for a user-specified
% set of chemical reactions
%
% Note that this function is the general modified-Arrhenius relation
% and can be applied equally to kb reaction rates, despite being named kf
%
% lnkf_r = getReaction_lnkf(T_r,A_r,nT_r,theta_r);
%
% Inputs:
%   T_r     -   temperature driving each reaction [K]. size (N_eta x N_reac)
%   Ar      -   Preexponential factor of each chemical reaction [mol/s ·
%               (mol/m^3)^(nu_p(s,r))]. Vector of size (N_reac x 1)
%   nT_r    -   Temperature exponent in the rate constant of each chemical
%               reaction [-]. Vector of size (N_reac x 1)
%   theta_r -   Activation temperature of each chemical reaction [K].
%               Vector of size (N_reac x 1)
%
% Outputs:
%   lnkf_r  -   natural logarithm of the forward reaction constants of the
%               matrix size of (N_eta x N_reac)
%
% Notes:
% The natural logarithm is computed to avoid MATLAB's integer overflow
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

% adjusting sizes
N_eta = size(T_r,1);

A_r = repmat(A_r',[N_eta,1]);
nT_r = repmat(nT_r',[N_eta,1]);
theta_r = repmat(theta_r',[N_eta,1]);

% computing kf_r
% we use logarithms such that we are not restricted to exponentials of maximum 200 (matlab's working precision)
lnkf_r = log(A_r) + nT_r.*log(T_r) - theta_r./T_r;
end
