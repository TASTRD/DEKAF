function [h_s,cp_s,cv_s,varargout] = getSpecies_h_cp(TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options,varargin)
% getSpecies_h_cp calculates the species enthalpy and the heat capacities.
%
% Usage:
%   (1)
%       [h_s,cp_s,cv_s] = getSpecies_h_cp(TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,...
%                           thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options)
%
%   (2)
%       [...,hMod,cpMod,cvMod] = getSpecies_h_cp(...)
%       |--> will also return structures with the enthalpies and heat
%       capacities of the different energy modes.
%
%   (3)
%       [h_s,cp_s,cv_s] = getSpecies_h_cp(...,AThermFuncs_s)
%       |--> usage for polynomial thermal models ('NASA7' or
%       'customPoly7'), where the coefficients of the polynomials must be
%       passed.
%
% Inputs:
%   TTrans          [K]             (N_eta x N_spec)
%       translational temperature
%   TRot            [K]             (N_eta x N_spec)
%       rotational temperature 
%   TVib            [K]             (N_eta x N_spec)
%       vibrational temperature
%   TElec           [K]             (N_eta x N_spec)
%       electronic temperature 
%   Tel             [K]             (N_eta x N_spec)
%       electron temperature
%   R_s             [J/kg-K]        (N_eta x N_spec)
%       species gas constant
%   nAtoms_s        [-]             (N_eta x N_spec)
%       number of atoms in the species' molecule
%   thetaVib_s      [K]             (N_eta x N_spec x Nvib)
%       species vibrational activation temperature
%   thetaElec_s     [K]             (N_eta x N_spec x Nstat)
%       species activation temperature of electronic states
%   gDegen_s        [-]             (N_eta x N_spec x Nstat)
%       degeneracies of the species' electronic state
%   bElectron       [-]             (N_eta x N_spec)
%       electron boolean 
%   hForm_s         [J/kg]          (N_eta x N_spec)
%       enthalpy of formation of each species 
%   options
%       necessary fields:
%           .modelThermal
%           .numberOfTemp
%           .neglect_hRot
%           .neglect_hVib
%           .neglect_hElec
%   AThermFuncs_s       [SI]                (N_spec x 1)
%       structured cell array with the anonymous functions for each
%       species. Each cell-array position contains N_coef anonymous
%       functions:
%           .A1, .A2, etc
%
% Outputs:
%   h_s                 [J/kg]              (N_eta x N_spec)
%       species enthalpy
%   cp_s                [J/kg-K]            (N_eta x N_spec)
%       species heat capacity at constant pressure
%   cv_s                [J/kg-K]            (N_eta x N_spec)
%       species heat capacity at constant volume
%   hMod
%       structure with the contribution of each energy mode to the species
%       enthalpy
%   cpMod
%       structure with the contribution of each energy mode to the species
%       heat capacity at constant pressure
%   cvMod
%       structure with the contribution of each energy mode to the species
%       heat capacity at constant volume
%
% Notes:
% This function assumes the sizes of the inputs are correct: N_eta x N_spec
% (with the exception of thetaElec_s and gDegen_s: these will have their 3rd
% dimension represent the number of electronic states considered)
% This input format is unlike most other get<Property> functions because
% getSpecies_h_cp is called for every iteration within Newton-Raphson, so
% speed and reducing redundancy is critical for performance.
%
% See also: listOfVariablesExplained, getSpecies_h_cp_RRHO,
% getSpecies_h_cp_NASA7
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

switch options.modelThermal
    case {'RRHO','RRHO-1LA'}
        [h_s,cp_s,cv_s,varargout{1:nargout-3}] = getSpecies_h_cp_RRHO(TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options);
    case {'NASA7','customPoly7'}
        AThermFuncs_s = varargin{1};
        [h_s,cp_s,cv_s,varargout{1:nargout-3}] = getSpecies_h_cp_NASA7(TTrans,Tel,AThermFuncs_s,R_s,nAtoms_s,bElectron,hForm_s,options);
    otherwise
        error(['The chosen modelThermal ''',options.modelThermal,''' is not supported']);
end


end % getSpecies_h_cp