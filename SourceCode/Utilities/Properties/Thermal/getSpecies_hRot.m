function [hRot_s,cpRot_s] = getSpecies_hRot(T,R_s,nAtoms_s)
% GET_SPECIES_HROT(T,R_s,nAtom) is a function that returns the species
% rotational enthalpy and its corresponding specific heat.
%
% The required inputs are:
%   - T: temperature matrix of size N_eta x N_spec
%   - R_s: species gas constant N_eta x N_spec
%   - nAtoms_s: number of atoms in the species' molecule N_eta x N_spec
%
% IMPORTANT!!! This function is only valid for linear molecules. It should
% be changed to use L_s/2 instead (see sec. 4.1 in "Miró Miró, F. (2020).
% Numerical Investigation of Hypersonic Boundary-Layer Stability and
% Transition in the presence of Ablation Phenomena. Université Libre de
% Bruxelles and von Karman Insitute for Fluid Dynamics.")
%
% Notes:
% Assume molecules behave as rigid rotors
%
% GNU Lesser General Public License 3.0

% calculating heat capacities and enthalpies (zero if its a single-atom
% spcies)
cpRot_s = R_s .* (nAtoms_s>1);
hRot_s = R_s.*T .* (nAtoms_s>1);