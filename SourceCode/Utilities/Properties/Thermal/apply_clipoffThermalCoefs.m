function [ATherm_s_out,Tlims_s_out] = apply_clipoffThermalCoefs(ATherm_s_in,Tlims_s_in)
% apply_clipoffThermalCoefs modifies the ATherm_s and Tlims_s cells in
% order to effectively clip off the thermal properties at the two extremes
% of their temperature ranges.
%
% Usage:
%   (1)
%       [ATherm_s,Tlims_s] = apply_clipoffThermalCoefs(ATherm_s,Tlims_s)
%
% Inputs and outputs:
%   Tlims_s         [K]
%       N_spec x 1 cell with vectors (size 1 x N_range+1) with the limits
%       of applicability of the different polynomials.
%   ATherm_s        [SI]
%       N_spec x 1 cell with matrices (size N_coef x N_range) with the
%       polynomial coefficients for a polynomial thermal model. N_range
%       must be equal to the length of each Tlims_s{s} plus one.
%
% Two additional columns are added to each matrix in ATherm_s and in
% Tlims_s.
%      
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_spec = length(ATherm_s_in); % obtaining sizes
[N_coef,~] = size(ATherm_s_in{1});

for s=N_spec:-1:1
    switch N_coef
        case 7
            T0 = Tlims_s_in{s}(1); % temperature at the extremes
            T1 = Tlims_s_in{s}(end);
            [h0,cp0,g0] = evalPoly_onePoint(T0,ATherm_s_in{s}(:,1));        % h, cp and g at the extremes
            [h1,cp1,g1] = evalPoly_onePoint(T1,ATherm_s_in{s}(:,end));
            A60 = h0 - cp0*T0;                                              % independent term in the h expression at the extremes
            A61 = h1 - cp1*T1;
            A70 = -(g0/T0 - A60/T0 - cp0*(1-log(T0)));                      % independent term in the g expression at the extremes
            A71 = -(g1/T1 - A61/T1 - cp1*(1-log(T1)));
            first_col = [cp0   ; 0 ; 0 ; 0 ; 0 ; A60 ; A70];                % coefficients on the first column for extrapolation
            last_col  = [cp1   ; 0 ; 0 ; 0 ; 0 ; A61 ; A71];                % coefficients on the last column for extrapolation
            ATherm_s_out{s} = [first_col , ATherm_s_in{s} , last_col];      % extending matrix of coefficients
            Tlims_s_out{s}  = [   0      , Tlims_s_in{s}  , Inf];           % extending vector of temperature limits
        otherwise
            error(['The number of coefficients in the polynomial fit (',num2str(N_coef),') is not supported']);
    end
end

end % apply_clipoffThermalCoefs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [h_over_R_s,cp_over_R_s,g_over_R_s] = evalPoly_onePoint(T,Acoefs)
% evalPoly_onePoint evaluates the polynomial for cp at one temperature
% point
%
% Usage:
%   (1)
%       cp = evalPoly_onePoint(T,Acoefs)
%
% Inputs and outputs:
%   T
%       temperature at which it must be evaluated.
%   Acoefs
%       vector with the A1, A2, etc coefficients
%   h_over_R_s
%       enthalpy divided by R_s at T
%   cp_over_R_s
%       heat capacity divided by R_s at T
%   g_over_R_s
%       gibbs free energy divided by R_s at T
%
% This function is local to apply_clipoffThermalCoefs
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

Ncoef = length(Acoefs);
for ic=Ncoef:-1:1
AThermalFuncs_s.(['A',num2str(ic)]) = @(T)Acoefs(ic);
end
[h_over_R_s,cp_over_R_s,g_over_R_s] = eval_NASA7_fit(T,AThermalFuncs_s);

end % evalPoly_onePoint
