function [gDegenVib_s,thetaVibs_out] = getSpecies_gDegenVib(thetaVibs_in)
% getSpecies_gDegenVib passes from a unit-degeneracy vibrational-mode
% ordering, to a multi-degeneracy ordering.
%
% Examples:
%   (1)
%       thetaVib_s = [932.1 932.1 1914.1 3373.8]
%       [gDegenVib_s,thetaVib_s] = getSpecies_gDegenVib(thetaVib_s)
%           gDegenVib_s = [2 1 1]
%           thetaVib_s = [932.1 1914.1 3373.8]
%
% The input can also be a matrix with multiple species along the first
% dimension (one row for each species)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_spec = size(thetaVibs_in,1);                                              % number of species to loop
for s=N_spec:-1:1                                                           % looping species
    theta_vec   = unique(thetaVibs_in(s,thetaVibs_in(s,:)~=0));                 % vector of unique non-zero values of thetaVib
    gvec        = cellfun(@(cll)nnz(ismember(thetaVibs_in,cll)&cll~=0),num2cell(theta_vec)); % times each value in theta_vec appeears in the original
    Nunique     = length(theta_vec);                                            % number of unique temperatures

    thetaVibs_out(s,1:Nunique) = theta_vec;                                     % populating outputs
    gDegenVib_s(s,1:Nunique)   = gvec;
end

end % getSpecies_gDegenVib