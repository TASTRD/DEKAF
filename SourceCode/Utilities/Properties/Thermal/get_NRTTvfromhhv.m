function [Tn,Tvn,cptrn,cpvn] = get_NRTTvfromhhv(h,hv,y_s,Tv0,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,...
                thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options)
% get_NRTTvfromhhv obtains the trans-rot and the vib-elec-el temperature
% from the total and vib-elec-el enthalpy using a Newton-Raphson iterative
% procedure.
%
% Usage:
%   (1)
%       [Tn,Tvn] = get_NRTTvfromhhv(h,hv,y_s,Tv0,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,...
%                 thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options)
%   (2)
%       [Tn,Tvn,cptrn,cpvn] = get_NRTTvfromhhv(...)
%       |--> returns also the heat capacities of the different energy modes
%
% Inputs and outputs
%   h               size (N_eta x 1) - objective enthalpy
%   hv              size (N_eta x 1) - objective vib-elec-el enthalpy
%   y_s             size (N_eta x N_spec)
%   Tv0             size (N_eta x 1) - Initial guess for Tv
%   R_s_mat         size (N_eta x N_spec)
%   nAtoms_s_mat    size (N_eta x N_spec)
%   thetaVib_s_mat  size (N_eta x N_spec x Nvib)
%   thetaElec_s_mat size (N_eta x N_spec x Nmod)
%   gDegen_s_mat    size (N_eta x N_spec x Nmod)
%   bElectron_mat   size (N_eta x N_spec)
%   hForm_s_mat     size (N_eta x N_spec)
%   options         options structure
%   Tn              size (N_eta x 1)    - Solution after the NR iteration
%
% See also: get_NRTfroms
%
% Author: Fernando Miro Miro
% Date: November 2018

N_spec = size(y_s,2);
[Tvn,cpvn] = get_NRTvfromhv(hv,y_s,Tv0,R_s_mat,thetaVib_s_mat,...
                        thetaElec_s_mat,gDegen_s_mat,bElectron_mat,options);

% Obtaining the trans-rot temperature
cptrn = getMixture_cptr(y_s,R_s_mat,nAtoms_s_mat,bElectron_mat,options); % trans-rot heat capacity
hForm = (y_s.*hForm_s_mat)*ones(N_spec,1);                          % mixture formation enthalpy
htr = h-hv-hForm;                                                   % mixture sensible trans-rot enthalpy
Tn = htr./cptrn;                                                    % trans-rot temperature

end % get_NRTTvfromhhv