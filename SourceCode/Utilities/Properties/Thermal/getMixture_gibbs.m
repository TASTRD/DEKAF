function gibbs = getMixture_gibbs(y_s,T,p,R_s,Mm_s,thetaRot_s,L_s,sigma_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,kBoltz,hPlanck,nAvogadro,hForm_s,options,varargin)
% getMixture_gibbs calculates the mixture gibbs free energy
%
% Usage:
%   (1)
%       gibbs = getMixture_gibbs(y_s,T,p,R_s,Mm_s,thetaRot_s,L_s,sigma_s,thetaVib_s,thetaElec_s,...
%                           gDegen_s,bElectron,kBoltz,hPlanck,nAvogadro,hForm_s,options)
%
%   (2)
%       ... = getMixture_gibbs(...,AThermFuncs_s)
%       |---> usage for polynomial thermal models ('NASA7' or
%       'customPoly7'), where the coefficients of the polynomials must be
%       passed.
%
% IMPORTANT: this function is not ready for multi-temperature models
%
% Inputs:
%   y_s             [-]             (N_eta x N_spec)
%       species mass fraction
%   T               [K]             (N_eta x 1)
%       temperature
%   p               [Pa]            (N_eta x 1)
%       temperature
%   R_s             [J/kg-K]        (N_spec x 1)
%       species gas constant
%   Mm_s            [kg/mol]        (N_spec x 1)
%       species molar weight
%   nAtoms_s        [-]             (N_spec x 1)
%       number of atoms in the species' molecule
%   thetaRot_s      [K]             (N_spec x 1)
%       species rotational activation temperature
%   L_s             [-]             (N_spec x 1)
%       species linearity factor
%   sigma_s         [-]             (N_spec x 1)
%       species steric factor
%   thetaVib_s      [K]             (N_spec x Nvib)
%       species vibrational activation temperature
%   thetaElec_s     [K]             (N_spec x Nstat)
%       species activation temperature of electronic states
%   gDegen_s        [-]             (N_spec x Nstat)
%       degeneracies of the species' electronic state
%   bElectron       [-]             (N_spec x 1)
%       electron boolean 
%   kBoltz          [SI]            (N_spec x 1)
%       Boltzmann constant
%   hPlanck         [SI]            (N_spec x 1)
%       Plancks constant
%   nAvogadro       [SI]            (N_spec x 1)
%       Avogadro's constant
%   hForm_s         [J/kg]          (N_spec x 1)
%       enthalpy of formation of each species 
%   options
%       necessary fields:
%           .modelThermal
%           .numberOfTemp
%           .neglect_hRot
%           .neglect_hVib
%           .neglect_hElec
%   AThermFuncs_s       [SI]                (N_spec x 1)
%       structured cell array with the anonymous functions for each
%       species. Each cell-array position contains N_coef anonymous
%       functions:
%           .A1, .A2, etc
%
% Outputs:
%   gibbs           [J/kg]           (N_eta x N_spec)
%       gibbs free energy of the mixture
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

protectedvalue(options,'numberOfTemp','1T');

N_spec = length(Mm_s);
gibbs_s = getSpecies_gibbs(T,p,R_s,Mm_s,thetaRot_s,L_s,sigma_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,kBoltz,hPlanck,nAvogadro,hForm_s,options,varargin{:});
gibbs = (gibbs_s.*y_s)*ones(N_spec,1);

end % getMixture_gibbs
