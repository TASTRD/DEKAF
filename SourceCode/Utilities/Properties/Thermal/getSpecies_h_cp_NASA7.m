function [h_s,cp_s,cv_s,varargout] = getSpecies_h_cp_NASA7(TTrans,Tel,AThermFuncs_s,R_s,nAtoms_s,bElectron,hForm_s,options)
% getSpecies_h_cp_NASA7 calculates the species enthalpy and heat capacities
% using NASA's 7-coefficient polynomial approximations:
%
%   cp_s/R_s = A1 + A2*T + A3*T^2 + A4*T^3 + A5*T^4                     [-]
%
%   h_s/(R_s*T) = A1 + A2/2*T + A3/3*T^2 + A4/4*T^3 + A5/5*T^4 + A6/T   [-]
%
% Usage:
%   (1)
%       [h_s,cp_s,cv_s] = getSpecies_h_cp_NASA7(T,AThermFuncs_s,R_s)
%
%   (2)
%       [...,hMod,cpMod,cvMod] = getSpecies_h_cp_NASA7(...)
%       |--> will also return structures with the enthalpies and heat
%       capacities of the different energy modes.
%
% IMPORTANT!!!
%   This function supports also a two-temperature usage. In it, the fits
%   are evaluated at Tv, substituting from them the
%   translational-rotational contribution assuming a RRHO also at Tv. That
%   way, the combined contribution of the vibrational and electronic modes
%   is retrieved. The translational-rotational modes are then obtained at T
%   (or Tv for electrons), also assuming a RRHO model.
%
% IMPORTANT #2!!!
%   neglect_hRot, neglect_hVib, and neglect_hElec ARE NOT SUPPORTED by this
%   function - all energy modes are always considered.
%   Also, there is no separation between the individual contribution of the
%   vibrational and the electronic energy modes. There ensemble
%   contribution is stored in the Vib mode.
%
% Inputs:
%   TTrans              [K]                 (N_eta x N_spec)
%       translational temperature
%   TVib                [K]                 (N_eta x N_spec)
%       vibrational temperature
%   AThermFuncs_s       [SI]                (N_spec x 1)
%       structured cell array with the anonymous functions for each
%       species. Each cell-array position contains N_coef anonymous
%       functions:
%           .A1, .A2, etc
%   R_s                 [J/kg-K]            (N_eta x N_spec)
%       species-specific gas constant
%   nAtoms_s            [-]                 (N_eta x N_spec)
%       number of atoms per species
%   bElectron           [-]                 (N_eta x N_spec)
%       electron boolean
%   hForm_s             [J/kg]              (N_eta x N_spec)
%       species formation enthalpy at 0K
%   options
%       classic DEKAF options structure. Must contain:
%           .numberOfTemp
%
% Outputs:
%   h_s                 [J/kg]              (N_eta x N_spec)
%       species enthalpy
%   cp_s                [J/kg-K]            (N_eta x N_spec)
%       species heat capacity at constant pressure
%   cv_s                [J/kg-K]            (N_eta x N_spec)
%       species heat capacity at constant volume
%   hMod
%       structure with the contribution of each energy mode to the species
%       enthalpy
%   cpMod
%       structure with the contribution of each energy mode to the species
%       heat capacity at constant pressure
%   cvMod
%       structure with the contribution of each energy mode to the species
%       heat capacity at constant volume
%
% See also: getSpecies_h_cp_RRHO, listOfVariablesExplained,
% eval_NASA7_VibElec, eval_NASA7_fit
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

protectedvalue(options,'neglect_hRot',false);
protectedvalue(options,'neglect_hVib',false);
protectedvalue(options,'neglect_hElec',false);

[N_eta,N_spec] = size(TTrans);                  % number of species
N_ranges = length(fieldnames(AThermFuncs_s));   % number of ranges of temperature
if N_ranges~=7                                  % sanity check
    error(['The number of coefficients in the polynomial curve fit (',N_ranges,') is not supported']);
end

switch options.numberOfTemp
    case '1T'
        TRot = TTrans;
        for s=N_spec:-1:1                                                   % looping species
            [h_over_R_s(:,s),cp_over_R_s(:,s)] = eval_NASA7_fit(TTrans(:,1),AThermFuncs_s(s));
        end
        h_s  = R_s.*h_over_R_s;                                             % multiplying by the specific gas constant
        cp_s = R_s.*cp_over_R_s;
        cv_s = cp_s - R_s;
        
        if nargout>3                                                        % computing individual contributions of the different energy modes
            [hTrans_s,cpTrans_s]    = getSpecies_hTrans(TTrans,Tel,R_s,bElectron);
            [hRot_s,cpRot_s]        = getSpecies_hRot(TRot,R_s,nAtoms_s);
            hVibElec_s  = h_s  - hTrans_s  - hRot_s - hForm_s;
            cpVibElec_s = cp_s - cpTrans_s - cpRot_s;
        end
    case '2T'
        [hVibElec_s,cpVibElec_s] = eval_NASA7_VibElec(Tel,AThermFuncs_s,R_s,nAtoms_s,bElectron,hForm_s); % thermal vibrational/electron properties at Tv
        [hTrans_s,cpTrans_s]     = getSpecies_hTrans(TTrans,Tel,R_s,bElectron);     % thermal translational properties at T
        [hRot_s,cpRot_s]         = getSpecies_hRot(TTrans,R_s,nAtoms_s);            % thermal rotational properties at T
        h_s     = hTrans_s + hRot_s + hVibElec_s + hForm_s;                         % assembling enthalpies etc
        cp_s    = cpTrans_s + cpRot_s + cpVibElec_s;
        cv_s    = cp_s-R_s;
    otherwise;      error(['The chosen numberOfTemp ''',options.numberOfTemp,''' is not supported']);
end

if nargout>3                                                        % preparing individual contributions of the different energy modes
    hMod.hTrans_s   = hTrans_s;
    hMod.hRot_s     = hRot_s;
    hMod.hVib_s     = hVibElec_s;
    hMod.hElec_s    = zeros(N_eta,N_spec);

    cpMod.cpTrans_s = cpTrans_s;
    cpMod.cpRot_s   = cpRot_s;
    cpMod.cpVib_s   = cpVibElec_s;
    cpMod.cpElec_s  = zeros(N_eta,N_spec);

    cvMod.cvTrans_s = cpTrans_s-R_s;
    cvMod.cvRot_s   = cpMod.cpRot_s;
    cvMod.cvVib_s   = cpMod.cpVib_s;
    cvMod.cvElec_s  = cpMod.cpElec_s;

    varargout{1} = hMod;
    varargout{2} = cpMod;
    varargout{3} = cvMod;
end


end % getSpecies_h_cp_NASA7