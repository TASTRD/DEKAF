function [h,dh_deta,dh_deta2] = getEnthalpy_semitotal2static(H,dH_deta,dH_deta2,U,dU_deta,dU_deta2)
%GETENTHALPY_SEMITOTAL2STATIC Calculate the static enthalpy and its derivatives
% given semi-total enthalpy of the horizontal component of the flow, the
% horizontal velocity, and their derivatives.
%
% Inputs:
%   H, dH_deta, dH_deta2    -   semi-total enthalpy of the flow and its
%                               first and second spatial derivative normal
%                               to the wall
%   U, dU_deta, dU_deta2    -   the horizontal velocity and its first
%                               and second spatial derivative normal to
%                               the wall
%
% By semi-total enthalpy, we mean H = h + u^2 / 2, neglecting other
% components of velocity (i.e., v and w).
%
% Outputs:
%   h, dh_deta, dh_deta2    -   static enthalpy and its first and second
%                               spatial derivative normal to the wall
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

h = H - 1/2*U.^2;
dh_deta = dH_deta - U.*dU_deta;
dh_deta2 = dH_deta2 - dU_deta.^2 - U.*dU_deta2;

end % getEnthalpy_semitotal2static.m

