function gam = getMixture_gam(TTrans,TRot,TVib,TElec,Tel,y_s,R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options,varargin)
% GETMIXTURE_GAM returns the mixture's ratio of specific
% heats for a given temperature, and a given mixture characterized by its
% different properties and constants.
%
% Usage:
%   (1)
%       gam = getMixture_gam(TTrans,TRot,TVib,TElec,Tel,y_s,R_s,nAtoms_s,...
%                       thetaVib_s,thetaElec_s,gDegen_s,bElectron,options);
%
%   (2)
%       ... = getMixture_gam(...,AThermFuncs_s)
%       |--> usage for polynomial thermal models ('NASA7' or
%       'customPoly7'), where the coefficients of the polynomials must be 
%       passed.
%
% Inputs:
%   y_s             [-]             (N_eta x N_spec)
%       mass fraction matrix 
%   TTrans          [K]             (N_eta x 1)
%       translational temperature
%   TRot            [K]             (N_eta x 1)
%       rotational temperature 
%   TVib            [K]             (N_eta x 1)
%       vibrational temperature
%   TElec           [K]             (N_eta x 1)
%       electronic temperature 
%   Tel             [K]             (N_eta x 1)
%       electron temperature
%   R_s             [J/kg-K]        (N_spec x 1)
%       species gas constant
%   nAtoms_s        [-]             (N_spec x 1)
%       number of atoms in the species' molecule
%   thetaVib_s      [K]             (N_spec x Nvib)
%       species vibrational activation temperature
%   thetaElec_s     [K]             (N_spec x Nstat)
%       species activation temperature of electronic states
%   gDegen_s        [-]             (N_spec x Nstat)
%       degeneracies of the species' electronic state
%   bElectron       [-]             (N_spec x 1)
%       electron boolean 
%   hForm_s         [J/kg]          (N_spec x 1)
%       enthalpy of formation of each species 
%   options
%       necessary fields:
%           .modelThermal
%           .numberOfTemp
%           .neglect_hRot
%           .neglect_hVib
%           .neglect_hElec
%   AThermFuncs_s       [SI]                (N_spec x 1)
%       structured cell array with the anonymous functions for each
%       species. Each cell-array position contains N_coef anonymous
%       functions:
%           .A1, .A2, etc
%
% Notes:
%   N_spec is the number of species in the mixture
%   N_eta is the number of points in the domain
%   N_stat is the number of electronic states for the given species
%
% Children and relation functions:
%   See also getSpecies_h_cp.m, listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
%
% GNU Lesser General Public License 3.0

dummyhForm_vec = zeros(size(nAtoms_s)); % dummy vector, not important because the enthalpy is not used here

[~,cp,cv] = getMixture_h_cp_noMat(y_s,TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,...
                    thetaVib_s,thetaElec_s,gDegen_s,bElectron,dummyhForm_vec,options,varargin{:});

gam = cp./cv;

end
