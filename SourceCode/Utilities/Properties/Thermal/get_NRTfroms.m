function Tn = get_NRTfroms(s,y_s,p,T0,R0,Mm_s,bElectron,nAtoms_s,thetaVib_s,thetaElec_s,...
                gDegen_s,hForm_s,nAvogadro,hPlanck,kBoltz,L_s,sigma_s,thetaRot_s,options)
% get_NRTfroms obtains the temperature from the entropy using a
% Newton-Raphson iterative procedure
%
% Usage:
%   (1)
%       Tn = get_NRTfroms(s,y_s,pT0,R0,Mm_s,bElectron,nAtoms_s,thetaVib_s,thetaElec_s,...
%                 gDegen_s,hForm_s,nAvogadro,hPlanck,kBoltz,L_s,sigma_s,thetaRot_s,options)
%
% Inputs and outputs
%   s               size (N_eta x 1) - objective entropy
%   y_s             size (N_eta x N_spec)
%   p               size (N_eta x 1)
%   T0              size (N_eta x 1) - Initial guess for T
%   R0              size (1 x 1)
%   Mm_s            size (N_spec x 1)
%   bElectron       size (N_spec x 1)
%   nAtoms_s        size (N_spec x 1)
%   thetaVib_s      size (N_spec x Nvib)
%   thetaElec_s     size (N_spec x Nmod)
%   gDegen_s        size (N_spec x Nmod)
%   hForm_s         size (N_spec x 1)
%   nAvogadro       size (1 x 1)
%   hPlanck         size (1 x 1)
%   kBoltz          size (1 x 1)
%   L_s             size (N_spec x 1)
%   sigma_s         size (N_spec x 1)
%   thetaRot_s      size (N_spec x 1)
%   options         options structure
%   Tn              size (N_eta x 1)    - Solution after the NR iteration
%
% See also: get_NRTfromh
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

options.EoS = protectedvalue(options,'EoS','idealGas');

protectedvalue(options,'modelThermal',{'RRHO','RRHO-1LA'});

R_s = R0./Mm_s;

% transforming properties to matrices
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
             bElectron_mat,hForm_s_mat,~] = reshape_inputs4enthalpy(R_s,...
             nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,T0);
N_spec = length(Mm_s);
% preparing NR loop
s_ref   = s;                % Reference entropy (target of the NR algorithm)
Tn      = T0;               % intial guess
func    = @(s) s - s_ref;   % defining objective function
n       = 0;                % initializing loop variables
conv    = 1;
while conv>options.T_tol && n<options.T_itMax
    n               = n+1;
    % Get entropy and entropy gradient with temperature for the nth iteration
    Tn_mat = repmat(Tn,[1,N_spec]);
    rho = getMixture_rho(y_s,p,Tn,Tn,R0,Mm_s,bElectron);
    [~,cp_s] = getSpecies_h_cp(Tn_mat,Tn_mat,Tn_mat,Tn_mat,Tn_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
             bElectron_mat,hForm_s_mat,options);
    sn = getMixture_s(Tn,Tn,Tn,Tn,Tn,rho,y_s,R_s,nAtoms_s,...
                    thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,Mm_s,...
                    nAvogadro,hPlanck,kBoltz,L_s,sigma_s,thetaRot_s,options);
    ds_dTn = getMixtureDer_ds_dT(y_s,Tn,Tn,p,cp_s,R0,Mm_s,bElectron,nAvogadro);
    funcT_n         = func(sn);
    dfuncT_n_dT_n   = ds_dTn;
    Tn1             = Tn - funcT_n ./ dfuncT_n_dT_n;
    % Convergence criterion
    conv = norm((Tn1-Tn)./Tn,inf);
    % Prepare for next iteration
    Tn = Tn1;
end
if n==options.T_itMax
    warning(['Maximum number of iterations (',num2str(n),') when computing the temperature using Newton-Raphson''s algorithm. The infinite norm of the discrepancy was ', ...
        num2str(conv),'. This can be changed through options.T_itMax and .T_tol.']);
end