function HForm0K_s = getSpecies_HForm0K(formReac_s,spec_list,R_s,Mm_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,HForm298K_s,options)
% getSpecies_HForm0K computes the species heat at formation at 0k.
%
%   Examples
%       (1)     HForm0K_s = getSpecies_HForm0K(R_s,Mm_s,nAtoms_s,thetaVib_s, ...
%                               thetaElec_s,gDegen_s,bElectron,HForm298K_s)
%
%   Inputs & Outputs:
%       -   R_s             size N_spec x 1
%       -   Mm_s            size N_spec x 1
%       -   nAtoms_s        size N_spec x 1
%       -   thetaVib_s      size N_spec x 1
%       -   thetaElec_s     size N_spec x N_stat
%       -   gDegen_s        size N_spec x N_stat
%       -   bElectron       size N_spec x 1
%       -   HForm298K_s     size N_spec x 1         [J/mol]
%       -   HForm0K_s       size N_spec x 1         [J/mol]
%       -   options         necessary fields
%                               neglect_hElec
%
% Note by Martin J Pitt on why the heat of formation of CO2 at 0K is larger
% than at 298K:
%
% "Exothermic compounds are those that have lower energies than their
% elements, the difference being the exotherm. In this case we can measure
% it directly by combusting carbon with oxygen. In many other cases we have
% to deduce it by using a series of reactions, noting the energies and
% calculating what it would be if it could be carried out directly. (Using
% Hess's law - Wikipedia)
%
% The C=O bonds in CO2 have a high bond energy (the energy you would need
% to break them) which is more than either of the C-C or O=O bonds in the
% elements and their sum. Thus to create C and O2 from CO2 you would have
% to put in energy to break the two C=O bonds. You would get some back from
% the C-C and O=O bonds but less. Thus you need to add a lot of energy to
% convert CO2 into its elements. (You can decompose it at high
% temperatures.)
%
% The reverse process gives off heat.
%
% Without life, you would not have carbon and oxygen together on the
% planet. Plants use sunlight to provide the energy to break down CO2. They
% do not convert it into elements, but into sugars, cellulose etc and give
% off oxygen. "
% (https://www.quora.com/Why-is-the-formation-of-CO2-exothermic)
%
% see also: setDefaults, listsOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

protectedvalue(options,'modelThermal',{'RRHO','RRHO-1LA'});

% getting formation enthalpies when needed
[hSens298_solid,solid_spec,gas_spec] = getSpecies_solids(formReac_s);
N_solid = length(solid_spec);                                               % number of solid species
idx_gas = cellfun(@(cll)find(ismember(spec_list,cll)),gas_spec);            % indices of the gas species in spec_list
HSens298_solid = hSens298_solid.*Mm_s(idx_gas);                             % [J/kg] --> [J/mol]
spec_list = [spec_list,solid_spec];                                         % appending solid species to the list of species

T = 298;                                                                    % temperature for which we need to compute the sensible enthalpy
N_spec = length(Mm_s);                                                      % number of species

[nuForm_reac,nuForm_prod] = generateStoichiometricMatrices(formReac_s,spec_list); % formation stoichiometric matrices
scaleFact = diag(nuForm_prod); % scaling factor to normalize the reactions (E.g.: 1/2 N2 <-> N instead of N2 <-> 2N)
scaleFact(scaleFact==0) = 1; % if the scaling factor is zero (no reaction), we make the normalization 1 (no normalization)

[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,HForm298K_s_mat,T_mat] = ...
                reshape_inputs4enthalpy(R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,HForm298K_s,T);
Mm_s_mat = repmat(Mm_s',[length(T),1]);
hForm298K_s_mat = HForm298K_s_mat ./ Mm_s_mat; % J/mol --> J/kg

ysDum = ones(size(T_mat)); % dummy place holder for the mass fraction
[~,~,~,h_s]  = getMixture_h_cp(ysDum,T_mat,T_mat,T_mat,T_mat,T_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm298K_s_mat,options);

HSens298K_s = h_s .* Mm_s_mat - HForm298K_s_mat; % J/kg --> J/mol

% appending enthalpy of formation of solids
HForm298K_s_mat = [HForm298K_s_mat,zeros(size(solid_spec))]; % solids have
HSens298K_s = [HSens298K_s,HSens298_solid];

HTot0K_s = HForm298K_s_mat - HSens298K_s; % species enthalpy at 0K
HTot0K_s_mat = repmat(HTot0K_s,[N_spec,1]); % repeating over the reactions

HForm0K_s = (((nuForm_prod - nuForm_reac) .* HTot0K_s_mat) * ones(N_spec+N_solid,1)) ./ scaleFact;
end % getSpecies_HForm0K