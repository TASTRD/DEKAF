function lnQElec_s = getSpecies_lnQElec(T,gDegen_s,thetaElec_s,bElectron)
% GETSPECIES_LNQELEC(T,gDegen_s,thetaElec_s,bElectron) returns the natural
% logarithm of the partition function of the electronic energy mode of
% each species.
%
% Th inputs are
%  - T size N_eta x 1 (to be repeated over dim. 2)
%  - gDegen_s size N_spec x N_stat x 1 (to be reshaped and repeated over dim. 1)
%  - thetaElec_s size N_spec x N_stat x 1 (to be reshaped and repeated over dim. 1)
%  - bElectron size N_spec x 1 (to be reshaped and repeated over dim. 1)
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

N_eta = length(T);
[N_spec,N_stat] = size(gDegen_s);

% reshaping
gDegen_s = repmat(reshape(gDegen_s,[1,N_spec,N_stat]),[N_eta,1,1]);
thetaElec_s = repmat(reshape(thetaElec_s,[1,N_spec,N_stat]),[N_eta,1,1]);
bElectron = repmat(bElectron',[N_eta,1]);
T = repmat(T,[1,N_spec]);

% calulating lnQElec
sum1 = zeros(size(T));
for ii=1:N_stat
    sum1 = sum1 + gDegen_s(:,:,ii) .* exp(-thetaElec_s(:,:,ii)./T);
end

lnQElec_s = log(sum1);

% enforcing that electrons don't have electronic energy
lnQElec_s(bElectron) = log(2); % Account for two possibilities: spin 1/2 or -1/2