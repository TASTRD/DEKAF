function lnQTransV_s = getSpecies_lnQTransV(TTrans,Tel,Mm_s,nAvogadro,hPlanck,kBoltz,bElectron)
% GETSPECIES_LNQTRANSV returns the natural logarithm of the partition
% function of the translational energy mode of each species divided by the
% volume it occupies.
%
% Usage:
%   (1)
%       lnQTransV_s = getSpecies_lnQTransV(TTrans,Tel,Mm_s,nAvogadro,...
%                                               hPlanck,kBoltz,bElectron)
%
% The inputs are
%  - TTrans     size N_eta x 1 (to be repeated over dim. 2)
%  - Tel        size N_eta x 1 (to be repeated over dim. 2)
%  - Mm_s       size N_spec x 1 (to be reshaped and repeated over dim. 1)
%  - nAvogadro, hPlanck, kBoltz size 1
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

N_eta = length(TTrans);
N_spec = length(Mm_s);

m_s = Mm_s/nAvogadro;

% reshaping
m_s = repmat(m_s',[N_eta,1]);
T_s = repmat(TTrans,[1,N_spec]);
Tel = repmat(Tel,[1,N_spec]);
T_s(:,bElectron) = Tel(:,bElectron);

% calulating lQTransV
lnQTransV_s = 3/2*(log(2*pi.*kBoltz./(hPlanck^2)) + log(m_s.*T_s));