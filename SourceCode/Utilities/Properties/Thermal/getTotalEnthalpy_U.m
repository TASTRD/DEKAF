function H = getTotalEnthalpy_U(h,U)
%GETTOTALENTHALPY_U Calculate the total enthalpy of the horizontal velocity
% of the flow given its static enthalpy
%
% H = getTotalEnthalpy_U(h,U);
%
% Inputs:
%   h   -   static enthalpy
%   U   -   horizontal velocity component
%
% Outputs:
%   H   -   total enthalpy of horizontal velocity component
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

H = h + 1/2*U.^2;

end % getTotalEnthalpy_U.m

