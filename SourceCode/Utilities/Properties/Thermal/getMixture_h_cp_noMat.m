function [varargout] = getMixture_h_cp_noMat(y_s,TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,...
                                            thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options,varargin)
% getMixture_h_cp_noMat returns the enthalpy and the heat capacities of the mixture.
%
% The difference between this function and getMixture_h_cp, is that this
% one does not require all inputs to be of size N_eta x N_spec.
%
% Usage: the same as getMixture_h_cp, but with different input sizes
%   (1)
%       h = getMixture_h_cp_noMat(y_s,TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,...
%                   thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options,AThermFuncs_s)
%   (etc)
%
% Inputs:
%   y_s             [-]             (N_eta x N_spec)
%       mass fraction matrix 
%   TTrans          [K]             (N_eta x 1)
%       translational temperature
%   TRot            [K]             (N_eta x 1)
%       rotational temperature 
%   TVib            [K]             (N_eta x 1)
%       vibrational temperature
%   TElec           [K]             (N_eta x 1)
%       electronic temperature 
%   Tel             [K]             (N_eta x 1)
%       electron temperature
%   R_s             [J/kg-K]        (N_spec x 1)
%       species gas constant
%   nAtoms_s        [-]             (N_spec x 1)
%       number of atoms in the species' molecule
%   thetaVib_s      [K]             (N_spec x Nvib)
%       species vibrational activation temperature
%   thetaElec_s     [K]             (N_spec x Nstat)
%       species activation temperature of electronic states
%   gDegen_s        [-]             (N_spec x Nstat)
%       degeneracies of the species' electronic state
%   bElectron       [-]             (N_spec x 1)
%       electron boolean 
%   hForm_s         [J/kg]          (N_spec x 1)
%       enthalpy of formation of each species 
%   options
%       necessary fields:
%           .modelThermal
%           .numberOfTemp
%           .neglect_hRot
%           .neglect_hVib
%           .neglect_hElec
%   AThermFuncs_s       [SI]                (N_spec x 1)
%       structured cell array with the anonymous functions for each
%       species. Each cell-array position contains N_coef anonymous
%       functions:
%           .A1, .A2, etc
%
% Outputs: the same as getMixture_cp
%
% See also: getMixture_h_cp, reshape_inputs4enthalpy
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% resizing inputs
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
             bElectron_mat,hForm_s_mat,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat] = reshape_inputs4enthalpy(...
             R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,TTrans,TRot,TVib,TElec,Tel);

[varargout{1:nargout}] = getMixture_h_cp(y_s,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,varargin{:});

end % getMixture_h_cp_noMat