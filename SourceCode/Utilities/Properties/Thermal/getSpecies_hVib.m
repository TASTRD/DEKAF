function [hVib_s,cpVib_s] = getSpecies_hVib(T,R_s,thetaVib_s,varargin)
% getSpecies_hVib(T,R_s) is a function that returns the species
% vibrational enthalpy and its corresponding specific heat.
%
% Usage:
%   (1)
%       [hVib_s,cpVib_s] = getSpecies_hVib(T,R_s,thetaVib_s)
%       |--> assumes T and thetaVib_s have both size N_eta x N_spec x N_vib
%       and R_s has size N_eta x N_spec
%
%   (2)
%       [...] = getSpecies_hVib(...,'notRepmatted')
%       |--> assumes the sizes specified below
%
% Inputs and outputs:
%   T           [K]
%       temperature matrix of size N_eta x 1
%   R_s         [J/kg-K]
%       species gas constant N_spec x 1
%   thetaVib_s  [K]
%       species vibrational activation temperature N_spec x N_vib
%   hVib_s      [J/kg]
%       species vibrational enthalpy N_eta x N_spec
%   cpVib_s     [J/kg-K]
%       species vibrational heat capacity N_eta x N_spec
%
% Notes:
% Assume molecules behave as harmonic oscillators
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

% Checking options
N_vib = size(thetaVib_s,3);
[~,bRepmat] = find_flagAndRemove('notRepmatted',varargin); % checking if we have to repmat
if bRepmat
    N_eta  = length(T);
    N_spec = length(R_s);
    R_s         = repmat(        R_s.',              [N_eta,1,N_vib]);  % (s,1) ----> (eta,s,v)
    thetaVib_s  = repmat(permute(thetaVib_s,[3,1,2]),[N_eta,1,1]);      % (s,v) ----> (eta,s,v)
    T           = repmat(        T,                  [1,N_spec,N_vib]); % (eta,1) --> (eta,s,v)
end

% calculating heat capacities and enthalpies
cpVib_s = zeros(size(T));
hVib_s = zeros(size(T));
for lv = 1:N_vib % suming over the different vibrational modes
    cpVibAddend = (R_s.*thetaVib_s(:,:,lv).^2.*exp(thetaVib_s(:,:,lv)./T))./(T.^2.*(exp(thetaVib_s(:,:,lv)./T)-1).^2); % obtained using maxima
    hVibAddend = (R_s.*thetaVib_s(:,:,lv))./(exp(thetaVib_s(:,:,lv)./T)-1);

    % If zero vibrational temperature, do not sum this term (to avoid evil nans)
    cpVibAddend(thetaVib_s(:,:,lv)==0) = 0;
    hVibAddend(thetaVib_s(:,:,lv)==0) = 0;

    cpVib_s = cpVib_s + cpVibAddend;
    hVib_s = hVib_s + hVibAddend;
end

% we explicitly make the vibrational energy of all monatomic particles
% zero
cpVib_s(thetaVib_s(:,:,1)==0) = 0;
hVib_s(thetaVib_s(:,:,1)==0) = 0;