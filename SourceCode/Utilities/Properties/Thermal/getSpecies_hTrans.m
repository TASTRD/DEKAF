function [hTrans_s,cpTrans_s] = getSpecies_hTrans(TTrans,Tel,R_s,bElectron)
% GET_SPECIES_HTRANS(T,R_s) is a function that returns the species
% translational enthalpy and its corresponding specific heat.
%
% The required inputs are:
%   - TTrans: translational temperature (N_eta x N_spec)
%   - Tel: electron temperature (N_eta x N_spec)
%   - R_s: species gas constant N_spec x N_spec
%   - bElectron: electron boolean (N_eta x N_spec)
%
% GNU Lesser General Public License 3.0

% heat capacity at constant pressure of translational mode
cpTrans_s = 5/2*R_s;

% enthalpy
hTrans_s = 5/2*R_s.*TTrans; % heavy particles use translational temperature
h_el_s = 5/2*R_s.*Tel;       % electrons use electron temperature
hTrans_s(bElectron) = h_el_s(bElectron);

end % getSpecies_hTrans.m