function Tv0 = getGuess_Tv(intel,options)
% getGuess_Tv obtains the initial guess for Tv based on the flow conditions
%
% Usage:
%   (1)
%       Tv0 = getGuess_Tv(intel,options)
%
% Inputs and outputs:
%   intel   - DEKAF's classic intel structure
%   options - DEKAF's classic options structure
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

if options.marchYesNo   % checking what xi location we're at
    i_xi = intel.i_xi;
else                    % if we are not marching, we fix this to one
    i_xi = 1;
end

if ~isfield(intel,'Tvguess')                        % if there is no previous value to use as an initial guess
    switch options.thermal_BC                           % depending on the wall condition
        case 'Twall'                                        % isothermal wall
            a = 2;                                              % HARDCODE ALERT!!
            eta = intel.eta;
            T_w = options.trackerTvwall(i_xi);
            Tv0 = intel.Tv_e + (T_w-intel.Tv_e) * exp(-a*eta);  % we use an exponentially decreasing function from the wall value to the freestream
        case 'H_F'                                          % constant heat-flux condition
            Tv0 = intel.Tv_e*ones(size(intel.gv));              % we just take it constant and equal to the edge value
        otherwise                                           % all other BCs --> break
            error(['the chosen thermal_BC ''',options.thermal_BC,''' is not supported']);
    end
else                                                % if there is a previous value
    Tv0 = intel.Tvguess;                                % using previous value as the initial guess
end