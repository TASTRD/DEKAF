function [Tvn,cpvn] = get_NRTvfromhv(hv,y_s,Tv0,R_s_mat,thetaVib_s_mat,...
                        thetaElec_s_mat,gDegen_s_mat,bElectron_mat,options)
% get_NRTTvfromhhv obtains the trans-rot and the vib-elec-el temperature
% from the total and vib-elec-el enthalpy using a Newton-Raphson iterative
% procedure.
%
% Usage:
%   (1)
%       [Tvn,cpvn] = get_NRTvfromhv(hv,y_s,Tv0,R_s_mat,thetaVib_s_mat,...
%                         thetaElec_s_mat,gDegen_s_mat,bElectron_mat,options)
%
% Inputs and outputs
%   hv              size (N_eta x 1) - objective vib-elec-el enthalpy
%   y_s             size (N_eta x N_spec)
%   Tv0             size (N_eta x 1) - Initial guess for Tv
%   R_s_mat         size (N_eta x N_spec)
%   thetaVib_s_mat  size (N_eta x N_spec x Nvib)
%   thetaElec_s_mat size (N_eta x N_spec x Nmod)
%   gDegen_s_mat    size (N_eta x N_spec x Nmod)
%   bElectron_mat   size (N_eta x N_spec)
%   options         options structure
%   Tn              size (N_eta x 1)    - Solution after the NR iteration
%
% See also: get_NRTfroms
%
% Author: Fernando Miro Miro
% Date: June 2019
% GNU Lesser General Public License 3.0

N_spec = size(y_s,2);
hv_ref  = hv;                   % Reference enthalpy (target of the NR algorithm
Tvn     = Tv0;                  % intial guess
func    = @(hv) hv - hv_ref;    % defining objective function
n       = 0;                    % initializing loop variables
conv    = 1;
while conv>options.T_tol && n<options.T_itMax
    n               = n+1;
    % Get enthalpy and cp for the nth iteration
    Tvn_mat = repmat(Tvn,[1,N_spec]); % repeating over the species to get a matrix N_eta x N_spec
    [hvn,cpvn] = getMixture_hv_cpv(y_s,Tvn_mat,Tvn_mat,Tvn_mat,R_s_mat,thetaVib_s_mat,...
                                    thetaElec_s_mat,gDegen_s_mat,bElectron_mat,options);
    % NOTE: we also output cptr, because since it does not depend on T, we
    % can then use it to get T from h without a NR iteration
    funcT_n         = func(hvn);
    dfuncT_n_dT_n   = cpvn;
    Tvn1             = Tvn - funcT_n ./ dfuncT_n_dT_n;
    % Convergence criterion
    conv = norm((Tvn1-Tvn)./Tvn,inf);
    % Prepare for next iteration
    Tvn = Tvn1;
end
if n==options.T_itMax
    warning(['Maximum number of iterations (',num2str(n),') when computing the vibrational temperature using Newton-Raphson''s algorithm. The infinite norm of the discrepancy was ', ...
        num2str(conv),'. This can be changed through options.T_itMax and .T_tol.']);
end

end % get_NRTTvfromhhv