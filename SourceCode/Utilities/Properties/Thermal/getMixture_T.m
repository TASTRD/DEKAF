function [TTrans,TRot,TVib,TElec,Tel,cp,y_s,dXs_dT,dXs_dyE,dys_dT,cpv] = getMixture_T(h,y_s,p,mixCnst,options,varargin)
% getMixture_T computes the mixture temperatures from the enthalpies.
%
% Usage:
%   (1)
%       [TTrans,TRot,TVib,TElec,Tel,cp,y_s,dXs_dT,dXs_dyE,dys_dT,cpv] = getMixture_T(h,y_s,p,mixCnst,options)
%
%   (2)
%       [...] = getMixture_T(h,y_s,p,mixCnst,options,Tv)
%       |--> two-temperature model usage
%
%   (3)
%       [...] = getMixture_T(h,X_E,p,mixCnst,options)
%       |--> LTEED usage
%
%   (4)
%       [...] = getMixture_T(...,'Tguess',T0)
%       |--> allows to provide an initial guess for the temperature
%
%   (5)
%       [...] = getMixture_T(...,'guessXsLTE',XsGuessLTE)
%       |--> allows to provide an initial guess for the equilibrium
%       concentration in LTE or LTEED conditions
%
% Inputs and outputs:
%   h                   [J/kg]              (N_eta x 1)
%       static total enthalpy
%   T0                  [K]                 (N_eta x 1)
%       initial guess for the temperature
%   y_s                 [-]                 (N_eta x N_spec)
%       species mass fractions
%   X_E                 [-]                 (N_eta x N_elem)
%       elemental mole fractions
%   p                   [Pa]                (N_eta x 1)
%       static pressure
%   mixCnst
%       structure containing the necessary mixture constants
%   options
%       structure containing various flow options
%   Tv                  [K]                 (N_eta x 1)
%       vibrational-electronic-electron temperature
%   TTrans, TRot, TVib, TElec, Tel [K]      (N_eta x 1)
%       translational, rotational, vibrational, electronic and electron
%       temperatures
%   cp                  [J/kg-K]            (N_eta x 1)
%       heat capacity at constant volume
%   dXs_dT, dys_dT      [1/K]               (N_eta x N_spec)
%       equilibrium concentration gradients with temperature
%   dXs_dyE             [-]                 (N_eta x N_spec x N_elem)
%       equilibrium concentration gradients with the elemental mass fractions
%   cpv                 [J/kg-K]            (N_eta x 1)
%       heat capacity of the vibrational-electronic-electron energy modes
%
% Author: Fernando Miro Miro
% Date: June 2019
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);
[T0,varargin] = parse_optional_input(varargin,'Tguess',[]);
[X_sGuess,varargin] = parse_optional_input(varargin,'guessXsLTE',[]);

switch options.flow
    case 'CPG' % only case where the temperature will be obtained from a applying a constant cp on the h
        cp      = repmat(mixCnst.cp_CPG,size(h)); % we import the heat capacity
        TTrans  = h./cp;
        TRot    = TTrans; % the temperatures of all modes are the same
        TVib    = TTrans;
        TElec   = TTrans;
        Tel     = TTrans;
        dXs_dT  = NaN*zeros(N_eta,N_spec); % placeholders
        dys_dT  = NaN*zeros(N_eta,N_spec);
        dXs_dyE = NaN*zeros(N_eta,N_spec);
        cpv     = NaN*zeros(N_eta,N_spec);
    case {'LTE','LTEED'} % for LTE, obtaining the temperature will be obtained from solving the equilibrium equations coupled with the expression for the enthalpy
        opts_NR = options;                                                          % defining options structure for the newton-raphson iteration
        opts_NR.display = false;                                                    % silencing messages from the NR algorithm
        switch options.flow
            case 'LTE';         X_E = repmat(mixCnst.XEq,[length(h),1]);    % the elemental mole fractions are fixed
            case 'LTEED';       X_E = y_s;                                  % y_s is actually X_E (see usage 3)
            otherwise;          error(['The provided value of flow ''',options.flow,''' is not supported']);
        end
        if isempty(T0)||isempty(X_sGuess);  extraInputs = {};                      % no initial guess was provided
        else;                               extraInputs = {'guessXs',X_sGuess,T0}; % initial guesses provided
        end
        [X_s,T,cp] = getEquilibrium_X_complete(p,h,'mtimesxh',options.mixture,mixCnst,options.modelEquilibrium,1,opts_NR,'variable_XE',X_E,extraInputs{:});
        y_s                 = getEquilibrium_ys(X_s,mixCnst.Mm_s);
        % Limit the mass fraction according to options.ys_lim (typically ~1e-35)
        % Then recompute the mole fractions and its T derivative
        y_s                 = limit_XYs(y_s,options.ys_lim,mixCnst.idx_bath);
        X_s                 = getSpecies_X(y_s,[],mixCnst.Mm_s);
        [dXs_dT,~,dXs_dyE]  = getEquilibriumDer_X(T,X_s,p,X_E,mixCnst,options.mixture,1,options,'computeXFgrads');
        dys_dT              = getEquilibriumDer_dys_dq(X_s,dXs_dT,mixCnst.Mm_s);
        TTrans  = T;
        TRot    = T;
        TVib    = T;
        TElec   = T;
        Tel     = T;
        cpv     = zeros(N_eta,N_spec); % placeholder
    otherwise % for all other flow assumptions we get the temperature from the total enthalpy using an iterative Newton-Raphson method
        [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,~] = ...
                    reshape_inputs4enthalpy(mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,h);

        switch options.numberOfTemp
            case '1T'
                % Initialize NR Algorithm
                if isempty(T0)                          % obtaining intial guess for T
                    hForm = (y_s.*hForm_s_mat)*ones(N_spec,1);
                    check_hForm_vs_h(hForm,h,y_s,(1:length(h)).',mixCnst.spec_list);
                    cptr = getMixture_cptr(y_s,R_s_mat,nAtoms_s_mat,bElectron_mat,options);
                    T0      = (h-hForm)./cptr;          % Initial guess for temperature T (CPG relation)
                end
                [Tn,cpn] = get_NRTfromh(h,y_s,T0,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,...
                            thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);

                % Final values from NR convergence
                TTrans  = Tn;
                TRot    = Tn;
                TVib    = Tn;
                TElec   = Tn;
                Tel     = Tn;
                cp      = cpn;
                cpv     = zeros(N_eta,N_spec); % placeholder
            case '2T'
                % For 2-T we solve for tauv, so we don't need to make a
                % Newton Raphson to obtain Tv. Moreover, the relationship
                % between htr and T is linear (cptr~=cptr(T)), so there is
                % not need for a NR at all.
                Tv = varargin{1};
                Tv_mat = repmat(Tv,[1,N_spec]);
                [hv,cpv] = getMixture_hv_cpv(y_s,Tv_mat,Tv_mat,Tv_mat,R_s_mat,thetaVib_s_mat,...
                                                thetaElec_s_mat,gDegen_s_mat,bElectron_mat,options,mixCnst.AThermFuncs_s,nAtoms_s_mat,hForm_s_mat);
                cp = getMixture_cptr(y_s,R_s_mat,nAtoms_s_mat,bElectron_mat,options); % trans-rot heat capacity
                hForm = (y_s.*hForm_s_mat)*ones(N_spec,1);                          % mixture formation enthalpy
                htr = h-hv-hForm;                                                   % mixture sensible trans-rot enthalpy
                T = htr./cp;                                                        % trans-rot temperature
                % cp is actually cptr here

                % Final values from NR convergence
                TTrans  = T;
                TRot    = T;
                TVib    = Tv;
                TElec   = Tv;
                Tel     = Tv;
            otherwise
                error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
        end
        dXs_dT = NaN*zeros(N_eta,N_spec); % placeholders
        dys_dT = NaN*zeros(N_eta,N_spec);
        dXs_dyE = NaN*zeros(N_eta,N_spec);
end

end % getMixture_T