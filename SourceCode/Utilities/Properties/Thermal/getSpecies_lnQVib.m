function lnQVib_s = getSpecies_lnQVib(T,thetaVib_s)
% GETSPECIES_LNQVIB(T,thetaVib_s) returns the natural logarithm of the
% partition function of the vibrational energy mode of each species.
%
% The inputs are
%  - T size N_eta x 1 (to be repeated over dim. 2)
%  - thetaVib_s size  N_spec x N_vib (to be reshaped and repeated over dim. 1)
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

N_eta = length(T);
[N_spec,N_vib] = size(thetaVib_s);

% reshaping
thetaVib_s = repmat(reshape(thetaVib_s,[1,N_spec,N_vib]),[N_eta,1,1]);      % N_eta x N_spec x N_vib
T = repmat(T,[1,N_spec]);

% calculating lnQVib
lnQVib_s = zeros(N_eta,N_spec); % initialize
for lv = 1:N_vib
    lnQVibAddend = -log(1-exp(-thetaVib_s(:,:,lv)./T));                     % !will be inf if thetaVib_s is zero

    % If zero vibrational temperature, do not sum this term (to avoid evil nans)
    lnQVibAddend(thetaVib_s(:,:,lv)==0) = 0;

    lnQVib_s = lnQVib_s + lnQVibAddend;
end

% atoms have no vibrational energy
lnQVib_s(thetaVib_s(:,:,1)==0) = 0;
