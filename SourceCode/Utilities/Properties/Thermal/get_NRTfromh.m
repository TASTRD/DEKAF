function [Tn,cpn] = get_NRTfromh(h,y_s,T0,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,...
                thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,varargin)
% get_NRTfromh obtains the temperature from the enthalpy using a
% Newton-Raphson iterative procedure
%
% Usage:
%   (1)
%       Tn = get_NRTfromh(h,y_s,T0,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,...
%            thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options)
%   (2)
%       [Tn,cpn] = get_NRTfromh(...)
%       |--> returns also the heat capacity at constant pressure
%   (3)
%       [...] = get_NRTfromh(...,AThermFuncs_s)
%       |--> usage for polynomial thermal models ('NASA7' or
%       'customPoly7'), where the coefficients of the polynomials must be 
%       passed.
%
% Inputs and outputs
%   h               size (N_eta x 1) - objective enthalpy
%   y_s             size (N_eta x N_spec)
%   T0              size (N_eta x 1) - Initial guess for T
%   R_s_mat         size (N_eta x N_spec)
%   nAtoms_s_mat    size (N_eta x N_spec)
%   thetaVib_s_mat  size (N_eta x N_spec x Nvib)
%   thetaElec_s_mat size (N_eta x N_spec x Nmod)
%   gDegen_s_mat    size (N_eta x N_spec x Nmod)
%   bElectron_mat   size (N_eta x N_spec)
%   hForm_s_mat     size (N_eta x N_spec)
%   options         options structure
%   AThermFuncs_s   structured cell array with anonymous functions 
%                   (see <a href="matlab:help getMixture_h_cp">getMixture_h_cp</a>)
%   Tn              size (N_eta x 1)    - Solution after the NR iteration
%   cpn             size (N_eta x 1)
%
% See also: get_NRTfroms
%
% Author: Fernando Miro Miro
% Date: November 2018

N_spec = size(y_s,2);
h_ref   = h;                % Reference enthalpy (target of the NR algorithm
Tn      = T0;               % intial guess
func    = @(h) h - h_ref;   % defining objective function
n       = 0;                % initializing loop variables
conv    = 1;
while conv>options.T_tol && n<options.T_itMax
    n               = n+1;
    % Get enthalpy and cp for the nth iteration
    Tn_mat = repmat(Tn,[1,N_spec]); % repeating over the species to get a matrix N_eta x N_spec
    [hn,cpn,~,~]      = getMixture_h_cp(y_s,Tn_mat,Tn_mat,Tn_mat,Tn_mat,Tn_mat,...
                                        R_s_mat,nAtoms_s_mat,thetaVib_s_mat,...
                                        thetaElec_s_mat,gDegen_s_mat,bElectron_mat,...
                                        hForm_s_mat,options,varargin{:});
    funcT_n         = func(hn);
    dfuncT_n_dT_n   = cpn;
    Tn1             = Tn - funcT_n ./ dfuncT_n_dT_n;
    % Convergence criterion
    conv = norm((Tn1-Tn)./Tn,inf);
    % Prepare for next iteration
    Tn = Tn1;
end
if n==options.T_itMax
    warning(['Maximum number of iterations (',num2str(n),') when computing the temperature using Newton-Raphson''s algorithm. The infinite norm of the discrepancy was ', ...
        num2str(conv),'. This can be changed through options.T_itMax and .T_tol.']);
end