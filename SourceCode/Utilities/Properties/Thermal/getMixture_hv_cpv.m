function [hv,cpv] = getMixture_hv_cpv(y_s,TVib,TElec,Tel,R_s,...
                        thetaVib_s,thetaElec_s,gDegen_s,bElectron,options,varargin)
% getMixture_hv_cpv returns the vib-elec-el enthalpy and heat capacity of
% the mixture.
%
% Usage:
%   (1)     
%       [hv,cpv] = getMixture_hv_cpv(y_s,TVib,TElec,Tel,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options)
%
%   (2)     
%       [...] = getMixture_hv_cpv(...,AThermFuncs_s,nAtoms_s,hForm_s)
%       |--> usage for polynomial thermal models ('NASA7' or
%       'customPoly7'), where the coefficients of the polynomials must be
%       passed.
%
%
% IMPORTANT!!!
%   for the 'NASA7' and 'customPoly7' thermal models, neglect_hVib, and
%   neglect_hElec ARE NOT SUPPORTED - all energy modes are always considered.   
%   Also, there is no separation between the individual contribution of the
%   vibrational and the electronic energy modes. There ensemble
%   contribution is stored in the Vib mode.
%
% The required inputs are:
%   TVib            [K]             (N_eta x N_spec)
%       vibrational temperature
%   TElec           [K]             (N_eta x N_spec)
%       electronic temperature 
%   Tel             [K]             (N_eta x N_spec)
%       electron temperature
%   R_s             [J/kg-K]        (N_eta x N_spec)
%       species gas constant
%   nAtoms_s        [-]             (N_eta x N_spec)
%       number of atoms in the species' molecule
%   thetaVib_s      [K]             (N_eta x N_spec x Nvib)
%       species vibrational activation temperature
%   thetaElec_s     [K]             (N_eta x N_spec x Nstat)
%       species activation temperature of electronic states
%   gDegen_s        [-]             (N_eta x N_spec x Nstat)
%       degeneracies of the species' electronic state
%   bElectron       [-]             (N_eta x N_spec)
%       electron boolean 
%   hForm_s         [J/kg]          (N_eta x N_spec)
%       enthalpy of formation of each species 
%   options
%       necessary fields:
%           .modelThermal
%           .numberOfTemp
%           .neglect_hRot
%           .neglect_hVib
%           .neglect_hElec
%   AThermFuncs_s       [SI]                (N_spec x 1)
%       structured cell array with the anonymous functions for each
%       species. Each cell-array position contains N_coef anonymous
%       functions:
%           .A1, .A2, etc
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% obtaining number of species
N_spec = size(y_s,2);

% obtaining mixture enthalpy
[hv_s,cpv_s] = getSpecies_hv_cpv(TVib,TElec,Tel,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options,varargin{:});

cpv = (y_s.*cpv_s) * ones(N_spec,1); % this product is the same as summing over all the species
hv = (y_s.*hv_s) * ones(N_spec,1);

end % getMixture_hv_cpv