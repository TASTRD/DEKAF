function [hVibElec_s,cpVibElec_s] = eval_NASA7_VibElec(T2D,AThermFuncs_s,R_s,nAtoms_s,bElectron,hForm_s)
% eval_NASA7_VibElec evaluates the NASA-7 polynomial for a single species,
% in order to obtain the Vib-Elec energy and heat capacity
%
%   cp_s/R_s = A1 + A2*T + A3*T^2 + A4*T^3 + A5*T^4                     [-]
%
%   h_s/(R_s*T) = A1 + A2/2*T + A3/3*T^2 + A4/4*T^3 + A5/5*T^4 + A6/T   [-]
%
% Usage:
%   (1)
%       [hVibElec_s,cpVib_s] = eval_NASA7_VibElec(T,AThermFuncs_s,R_s,nAtoms_s,bElectron,hForm_s)
%
% Inputs and outputs:
%   T               [K]             (N_eta x N_spec)
%       temperature at which to evaluate the fit
%   AThermFuncs_s   [SI]            (N_spec x 1)
%       structure with anonymous functions for the A1-A6 coefficients
%   R_s             [J/kg-K]        (N_eta x N_spec)
%       species-specific gas constant
%   nAtoms_s        [-]             (N_eta x N_spec)
%       number of atoms by species
%   bElectron       [-]             (N_eta x N_spec)
%       electron boolean
%   hForm_s         [J/kg]          (N_eta x N_spec)
%       species formation enthalpy at 0K
%   hVibElec_s      [J/kg]          (N_eta x N_spec)
%       enthalpy due to the vibrational and electronic energy modes
%   cpVibElec_s     [J/kg-K]        (N_eta x N_spec)
%       heat capacity due to the vibrational and electronic energy modes
%
% See also: getSpecies_h_cp_NASA7, eval_NASA7_fit
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

T = T2D(:,1);
N_spec = length(AThermFuncs_s);
for s=N_spec:-1:1                                                       % looping species
    [h_over_R_s(:,s),cp_over_R_s(:,s)] = eval_NASA7_fit(T,AThermFuncs_s(s)); % evaluating fit
end
[hTrans_s,cpTrans_s]    = getSpecies_hTrans(T2D,T2D,R_s,bElectron);     % thermal translational properties at T
[hRot_s,cpRot_s]        = getSpecies_hRot(T2D,R_s,nAtoms_s);            % thermal rotational properties at T
hVibElec_s  = h_over_R_s .*R_s - hTrans_s  - hRot_s - hForm_s;          % removing contribution of the translational, rotational and formation energy
cpVibElec_s = cp_over_R_s.*R_s - cpTrans_s - cpRot_s;

end % eval_NASA7_VibElec