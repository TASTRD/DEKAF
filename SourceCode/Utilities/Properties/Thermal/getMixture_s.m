function s = getMixture_s(TTrans,TRot,TVib,TElec,Tel,rho,y_s,R_s,nAtoms_s,...
                    thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,Mm_s,...
                    nAvogadro,hPlanck,kBoltz,L_s,sigma_s,thetaRot_s,options)
% getMixture_s returns the entropy per unit mass of the mixture
%
% Usage:
%   (1)
%       s = getMixture_s(TTrans,TRot,TVib,TElec,Tel,rho,y_s,R_s,nAtoms_s,...
%                     thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,Mm_s,...
%                     nAvogadro,hPlanck,kBoltz,L_s,sigma_s,thetaRot_s,options)
%
% Inputs and outputs:
%   TTrans      -   N_eta x 1
%   TRot        -   N_eta x 1
%   TVib        -   N_eta x 1
%   TElec       -   N_eta x 1
%   Tel         -   N_eta x 1
%   rho         -   N_eta x 1
%   y_s         -   N_eta x N_spec
%   R_s         -   N_spec x 1
%   nAtoms_s    -   N_spec x 1
%   thetaVib_s  -   N_spec x 1
%   thetaElec_s -   N_spec x N_mod
%   gDegen_s    -   N_spec x N_mod
%   bElectron   -   N_spec x 1
%   hForm_s     -   N_spec x 1
%   Mm_s        -   N_spec x 1
%   nAvogadro   -   1 x 1
%   hPlanck     -   1 x 1
%   kBoltz      -   1 x 1
%   L_s         -   N_spec x 1
%   sigma_s     -   N_spec x 1
%   thetaRot_s  -   N_spec x 1
%   options     -   necessary fields
%                       neglect_hElec
%
% Children and relation functions:
%   See also getSpecies_s.m, listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% sizes etc
[N_eta,N_spec] = size(y_s); % number of species

% computing species entropies
s_s = getSpecies_s_plus_Rslnns(TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,...
                    thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,Mm_s,...
                    nAvogadro,hPlanck,kBoltz,L_s,sigma_s,thetaRot_s,options);

% obtaining number densities
lnn_s = log(getSpecies_n(rho,y_s,Mm_s,nAvogadro));
lnn_s(y_s==0) = 0;                                      % condition to ensure that there is no 0*Inf situation
R_s = repmat(R_s.',[N_eta,1]);

% mixing
s = ((s_s-R_s.*lnn_s).*y_s)*ones(N_spec,1);
end % getMixture_s