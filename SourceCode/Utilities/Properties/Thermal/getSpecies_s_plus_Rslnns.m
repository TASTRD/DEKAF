function s_s = getSpecies_s_plus_Rslnns(TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,...
                    thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,Mm_s,...
                    nAvogadro,hPlanck,kBoltz,L_s,sigma_s,thetaRot_s,options)
% getSpecies_s_plus_Rslnns returns the species entropy per unit mass plus
% R_s*log(n_s)
%
% This term grouping is done to avoid numerical overflow due to n_s being 0
%
% Usage:
%   (1)
%       s_s = getSpecies_s_plus_Rslnns(TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,...
%                     thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,Mm_s,...
%                     nAvogadro,hPlanck,kBoltz,L_s,sigma_s,thetaRot_s,options)
%
% Inputs and outputs:
%   TTrans      -   N_eta x 1
%   TRot        -   N_eta x 1
%   TVib        -   N_eta x 1
%   TElec       -   N_eta x 1
%   Tel         -   N_eta x 1
%   R_s         -   N_spec x 1
%   nAtoms_s    -   N_spec x 1
%   thetaVib_s  -   N_spec x 1
%   thetaElec_s -   N_spec x N_mod
%   gDegen_s    -   N_spec x N_mod
%   bElectron   -   N_spec x 1
%   hForm_s     -   N_spec x 1
%   Mm_s        -   N_spec x 1
%   nAvogadro   -   1 x 1
%   hPlanck     -   1 x 1
%   kBoltz      -   1 x 1
%   L_s         -   N_spec x 1
%   sigma_s     -   N_spec x 1
%   thetaRot_s  -   N_spec x 1
%   options     -   necessary fields
%                       neglect_hElec
%
% Children and relation functions:
%   See also getSpecies_h_cp.m, getSpecies_lnQTransV.m,
%   getSpecies_lnQRot.m, getSpecies_lnQVib.m, getSpecies_lnQElec.m,
%   getSpecies_n.m, listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

protectedvalue(options,'modelThermal',{'RRHO','RRHO-1LA'});


[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
 bElectron_mat,dummyhForm_mat,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat] ...
    = reshape_inputs4enthalpy(R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,...
        bElectron,hForm_s,TTrans,TRot,TVib,TElec,Tel);

if nnz(bElectron)
    TElec_mat(:,bElectron) = Tel; % electron temperature for the "electronic energy mode" (free-electron spin)
end

% Obtaining species enthalpies
[~,~,~,hMod_s] = getSpecies_h_cp(TTrans_mat,TRot_mat,TVib_mat,TElec_mat,...
    Tel_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
    bElectron_mat,dummyhForm_mat,options);

% obtaining partition function logarithms
lnQTransV_s = getSpecies_lnQTransV(TTrans,Tel,Mm_s,nAvogadro,hPlanck,kBoltz,bElectron);
if ~options.neglect_hRot;       lnQRot_s =   getSpecies_lnQRot(TRot,thetaRot_s,L_s,sigma_s);
else;                           lnQRot_s =   zeros(size(lnQTransV_s));
end
if ~options.neglect_hVib;       lnQVib_s =   getSpecies_lnQVib(TVib,thetaVib_s);
else;                           lnQVib_s =   zeros(size(lnQTransV_s));
end
if ~options.neglect_hElec;      lnQElec_s =  getSpecies_lnQElec(TElec,gDegen_s,thetaElec_s,bElectron);
else;                           lnQElec_s =  zeros(size(lnQTransV_s));
end

% computing entropy
s_s = R_s_mat .* (lnQTransV_s + lnQRot_s + lnQVib_s + lnQElec_s) + ...
    hMod_s.hTrans_s ./ TTrans_mat + hMod_s.hRot_s ./ TRot_mat + hMod_s.hVib_s ./ TVib_mat + hMod_s.hElec_s ./ TElec_mat;
end % getSpecies_s_plus_Rslnns