function [hElec_s,cpElec_s] = getSpecies_hElec(T,R_s,thetaElec_s,gDegen_s,bElectron)
% getSpecies_hElec(T,R_s,thetaElec_s,gDegen_s)
% is a function that returns the species electronic enthalpy and its
% corresponding specific heat.
%
% The required inputs are:
%   - T: temperature matrix of size N_eta x N_spec
%   - R_s: species gas constant N_eta x N_spec
%   - thetaElec_s: activation temperature of the species' electronic states N_eta x N_spec x N_stat
%   - gDegen_s: degeneracies of the species' electronic state N_eta x N_spec x N_stat
%   - bElectron: boolean electron matrix N_eta x N_spec
%
% Notes:
%   QElec = sum(g_L*exp(-theta_L/T)) over all electronic energy levels
%   eElec = RT^2*(dlnQElec_dT)
%   cvElec = deElec_dT
%   hElec = eElec
%   cpElec = cvElec
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

N_stat = size(gDegen_s,3);
sum1 = zeros(size(T));
sum2 = zeros(size(T));
sum3 = zeros(size(T));
for ii=1:N_stat
    sum1 = sum1 + gDegen_s(:,:,ii)                              .* exp(-thetaElec_s(:,:,ii)./T);
    sum2 = sum2 + gDegen_s(:,:,ii)    .* thetaElec_s(:,:,ii)    .* exp(-thetaElec_s(:,:,ii)./T);
    sum3 = sum3 + gDegen_s(:,:,ii)    .* thetaElec_s(:,:,ii).^2 .* exp(-thetaElec_s(:,:,ii)./T);
end

hElec_s = R_s .* sum2 ./ sum1;
cpElec_s = R_s./T.^2 .*(sum3./sum1 - (sum2./sum1).^2);

% we enforce that electrons don't have electronic energy
hElec_s(bElectron) = 0;
cpElec_s(bElectron) = 0;
