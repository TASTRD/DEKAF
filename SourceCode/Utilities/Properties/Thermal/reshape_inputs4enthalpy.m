function [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,varargout] = ...
    reshape_inputs4enthalpy(R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,varargin)
% reshape_inputs4enthalpy reshapes to matrix form the inputs required by
% getMixture_h_cp.
%
% This is necessary because getMixture_h_cp is optimized for speed, and
% therefore does not reshape inputs inside.
%
% Examples:
%    (1)    [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
%            bElectron_mat,hForm_s_mat,T_mat] = reshape_inputs4enthalpy(R_s,...
%            nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,T)
%
%    (2)    [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
%            bElectron_mat,hForm_s_mat,T1_mat,T2_mat,...] = reshape_inputs4enthalpy(...
%            R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,T1,T2,...)
%
% For detailed information on the different variables see also:
% listOfVariablesExplained, setDefaults, getMixture_h_cp
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

T_struct = varargin;                % the variable input arguments are the different temperatures to be reshaped
N_T = length(T_struct);             % number of temperatures
N_eta = length(T_struct{1});        % length of temperature vectors
[N_spec,N_stat] = size(gDegen_s);   % number of species and electronic states
N_vib = size(thetaVib_s,2);         % number of vibrational frequencies

R_s_mat         = repmat(R_s',[N_eta,1]);
nAtoms_s_mat    = repmat(nAtoms_s',[N_eta,1]);
bElectron_mat   = repmat(bElectron',[N_eta,1]);
hForm_s_mat     = repmat(hForm_s',[N_eta,1]);
thetaVib_s_mat  = repmat(reshape(thetaVib_s,[1,N_spec,N_vib]),[N_eta,1,1]);
thetaElec_s_mat = repmat(reshape(thetaElec_s,[1,N_spec,N_stat]),[N_eta,1,1]);
gDegen_s_mat    = repmat(reshape(gDegen_s,[1,N_spec,N_stat]),[N_eta,1,1]);

Tmat_struct = cell(N_T,1); % allocating
for iT = 1:N_T % looping temperatures
    Tmat_struct{iT} = repmat(T_struct{iT},[1,N_spec]);
end
varargout = Tmat_struct; % assigning temperature outputs
