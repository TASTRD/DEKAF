function [hv_s,cpv_s] = getSpecies_hv_cpv(TVib,TElec,Tel,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options,varargin)
% getSpecies_hv_cpv Calculates the vib-elec-el enthalpy and heat capacity
% of the mixture
%
% Usage:
%   (1)
%       [hv_s,cpv_s] = getSpecies_hv_cpv(TVib,TElec,Tel,R_s,thetaVib_s,...
%                                   thetaElec_s,gDegen_s,bElectron,options)
%
%   (2)
%       [...] = getSpecies_hv_cpv(...,AThermFuncs_s,nAtoms_s,hForm_s)
%       |--> usage for polynomial thermal models ('NASA7' or
%       'customPoly7'), where the coefficients of the polynomials must be
%       passed.
%
% IMPORTANT!!!
%   for the 'NASA7' and 'customPoly7' thermal models, neglect_hVib, and
%   neglect_hElec ARE NOT SUPPORTED - all energy modes are always considered.   
%   Also, there is no separation between the individual contribution of the
%   vibrational and the electronic energy modes. There ensemble
%   contribution is stored in the Vib mode.
%
% The required inputs are:
%   TVib            [K]             (N_eta x N_spec)
%       vibrational temperature
%   TElec           [K]             (N_eta x N_spec)
%       electronic temperature 
%   Tel             [K]             (N_eta x N_spec)
%       electron temperature
%   R_s             [J/kg-K]        (N_eta x N_spec)
%       species gas constant
%   nAtoms_s        [-]             (N_eta x N_spec)
%       number of atoms in the species' molecule
%   thetaVib_s      [K]             (N_eta x N_spec x Nvib)
%       species vibrational activation temperature
%   thetaElec_s     [K]             (N_eta x N_spec x Nstat)
%       species activation temperature of electronic states
%   gDegen_s        [-]             (N_eta x N_spec x Nstat)
%       degeneracies of the species' electronic state
%   bElectron       [-]             (N_eta x N_spec)
%       electron boolean 
%   hForm_s         [J/kg]          (N_eta x N_spec)
%       enthalpy of formation of each species 
%   options
%       necessary fields:
%           .modelThermal
%           .numberOfTemp
%           .neglect_hRot
%           .neglect_hVib
%           .neglect_hElec
%   AThermFuncs_s       [SI]                (N_spec x 1)
%       structured cell array with the anonymous functions for each
%       species. Each cell-array position contains N_coef anonymous
%       functions:
%           .A1, .A2, etc
%
% Notes:
% This function assumes the sizes of the inputs are correct: N_eta x N_spec
% (with the exception of thetaElec_s and gDegen_s: these will have their 3rd
% dimension represent the number of electronic states considered)
% This input format is unlike most other get<Property> functions because
% getSpecies_h_cp is called for every iteration within Newton-Raphson, so
% speed and reducing redundancy is critical for performance.
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% computing energy and heat capacity of the different energy modes
switch options.modelThermal
    case {'RRHO','RRHO-1LA'}
        if ~options.neglect_hVib;       [hVib_s,cpVib_s]        = getSpecies_hVib(TVib,R_s,thetaVib_s);
        else;                           hVib_s = zeros(size(TVib));
                                        cpVib_s = zeros(size(TVib));
        end
        if ~options.neglect_hElec;      [hElec_s,cpElec_s]  = getSpecies_hElec(TElec,R_s,thetaElec_s,gDegen_s,bElectron);
        else;                           hElec_s = zeros(size(TVib));
                                        cpElec_s = zeros(size(TVib));
        end
    case {'NASA7','customPoly7'}
        protectedvalue(options,'neglect_hVib',false);
        protectedvalue(options,'neglect_hElec',false);
        AThermFuncs_s   = varargin{1};
        nAtoms_s        = varargin{2};
        hForm_s         = varargin{3};
        [hVib_s,cpVib_s] = eval_NASA7_VibElec(TVib,AThermFuncs_s,R_s,nAtoms_s,bElectron,hForm_s);
        hElec_s  = zeros(size(hVib_s));
        cpElec_s = zeros(size(cpVib_s));
    otherwise
        error(['The chosen modelThermal ''',options.modelThermal,''' is not supported']);
end

% assembling energy modes
hv_s = hVib_s + hElec_s;
cpv_s = cpVib_s + cpElec_s;
if nnz(bElectron)>0
    [hv_s(bElectron),cpv_s(bElectron)] = getSpecies_hTrans(Tel(bElectron),Tel(bElectron),R_s(bElectron),bElectron(bElectron));
    % bElectron(bElectron) feels like inception, but it's what we want - the
    % electron booleans for those species that are electrons
    % IMPORTANT!! The electron's translational energy goes into the hv_s
    % energy group, but the formation enthalpy goes into the htr_s group.
end
