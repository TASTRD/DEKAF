function lnQRot_s = getSpecies_lnQRot(T,thetaRot_s,L_s,sigma_s)
% GETSPECIES_LNQROT(T,thetaRot_s,L_s,sigma_s) returns the natural logarithm
% of the partition function of the rotational energy mode of each species.
%
% Th inputs are
%  - T size N_eta x 1 (to be repeated over dim. 2)
%  - thetaRot_s size N_spec x 1 (to be reshaped and repeated over dim. 1)
%  - L_s                        (size N_spec x 1)
%  - sigma_s                    (size N_spec x 1)
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

N_eta = length(T);
N_spec = length(thetaRot_s);

% reshaping
% I_s         = repmat(I_s',[N_eta,1]);
thetaRot_s  = repmat(thetaRot_s',[N_eta,1]);
L_s         = repmat(L_s',[N_eta,1]);
sigma_s     = repmat(sigma_s',[N_eta,1]);
T           = repmat(T,[1,N_spec]);

% calulating lnQRot
% lnQRot_s = L_s/2 .* log((8*pi^2*kBoltz*I_s.*T)./hPlanck^2) - log(sigma_s);
lnQRot_s = L_s/2 .* log(T./thetaRot_s) - log(sigma_s);

% atoms have no rotational energy
lnQRot_s(thetaRot_s==0) = 0;