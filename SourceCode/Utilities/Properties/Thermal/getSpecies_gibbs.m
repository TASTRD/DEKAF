function gibbs_s = getSpecies_gibbs(T,p,R_s,Mm_s,thetaRot_s,L_s,sigma_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,kBoltz,hPlanck,nAvogadro,hForm_s,options,varargin)
% getSpecies_gibbs calculates the species gibbs free energy
%
% Usage:
%   (1)
%       gibbs_s = getSpecies_gibbs(T,p,R_s,Mm_s,thetaRot_s,L_s,sigma_s,thetaVib_s,thetaElec_s,...
%                           gDegen_s,bElectron,kBoltz,hPlanck,nAvogadro,hForm_s,options)
%
%   (2)
%       ... = getSpecies_gibbs(...,AThermFuncs_s)
%       |---> usage for polynomial thermal models ('NASA7' or
%       'customPoly7'), where the coefficients of the polynomials must be
%       passed.
%
% IMPORTANT: this function is not ready for multi-temperature models
%
% Inputs:
%   T               [K]             (N_eta x 1)
%       temperature
%   p               [Pa]            (N_eta x 1)
%       temperature
%   R_s             [J/kg-K]        (N_spec x 1)
%       species gas constant
%   Mm_s            [kg/mol]        (N_spec x 1)
%       species molar weight
%   nAtoms_s        [-]             (N_spec x 1)
%       number of atoms in the species' molecule
%   thetaRot_s      [K]             (N_spec x 1)
%       species rotational activation temperature
%   L_s             [-]             (N_spec x 1)
%       species linearity factor
%   sigma_s         [-]             (N_spec x 1)
%       species steric factor
%   thetaVib_s      [K]             (N_spec x Nvib)
%       species vibrational activation temperature
%   thetaElec_s     [K]             (N_spec x Nstat)
%       species activation temperature of electronic states
%   gDegen_s        [-]             (N_spec x Nstat)
%       degeneracies of the species' electronic state
%   bElectron       [-]             (N_spec x 1)
%       electron boolean 
%   kBoltz          [SI]            (N_spec x 1)
%       Boltzmann constant
%   hPlanck         [SI]            (N_spec x 1)
%       Plancks constant
%   nAvogadro       [SI]            (N_spec x 1)
%       Avogadro's constant
%   hForm_s         [J/kg]          (N_spec x 1)
%       enthalpy of formation of each species 
%   options
%       necessary fields:
%           .modelThermal
%           .numberOfTemp
%           .neglect_hRot
%           .neglect_hVib
%           .neglect_hElec
%   AThermFuncs_s       [SI]                (N_spec x 1)
%       structured cell array with the anonymous functions for each
%       species. Each cell-array position contains N_coef anonymous
%       functions:
%           .A1, .A2, etc
%
% Outputs:
%   gibbs_s        [J/kg]           (N_eta x N_spec)
%       gibbs free energy of each species 
%
% Notes:
%   See Anderson Eqn (11.46) and use the fact that g = h - sT
%
% References:
%   Anderson, John D. Jr., "Hypersonic and High-Temperature Gas Dynamics"
% Second Edition", American Institute of Aeronautics and Astronautics, 2006
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

protectedvalue(options,'numberOfTemp','1T');
protectedvalue(options,'neglect_hRot',false);
protectedvalue(options,'neglect_hVib',false);
protectedvalue(options,'neglect_hElec',false);

% obtaining sizes
N_eta = length(T);
N_spec = length(R_s);

switch options.modelThermal
    case 'RRHO'
        % Calculating the natural logarithm of the partition function of each energy mode
        lnQTransV_s = getSpecies_lnQTransV(T,T,Mm_s,nAvogadro,hPlanck,kBoltz,bElectron);
        lnQRot_s = getSpecies_lnQRot(T,thetaRot_s,L_s,sigma_s);
        lnQVib_s = getSpecies_lnQVib(T,thetaVib_s);
        lnQElec_s = getSpecies_lnQElec(T,gDegen_s,thetaElec_s,bElectron);
        
        % reshaping
        T = repmat(T,[1,N_spec]);
        p = repmat(p,[1,N_spec]);
        R_s = repmat(R_s',[N_eta,1]);
        hForm_s = repmat(hForm_s',[N_eta,1]);
        
        % Calculating gibbs free energy
        gibbs_s = -R_s.*T.*(lnQTransV_s - log(p./(kBoltz.*T)) + lnQRot_s + lnQVib_s + lnQElec_s) + hForm_s;
    case {'NASA7','customPoly7'}
        AThermFuncs_s = varargin{1};
        for s=N_spec:-1:1
        [~,~,gibbs_over_R_s(:,s)] = eval_NASA7_fit(T,AThermFuncs_s(s));
        end
        R_s = repmat(R_s',[N_eta,1]);
        gibbs_s = gibbs_over_R_s.*R_s;
    otherwise
        error(['The chosen modelThermal ''',options.modelThermal,''' is not supported']);
end
end
