function [h_over_R_s,cp_over_R_s,varargout] = eval_NASA7_fit(T,AThermFuncs_s)
% eval_NASA7_fit evaluates the NASA-7 polynomial fit
%
%   cp_s/R_s = A1 + A2*T + A3*T^2 + A4*T^3 + A5*T^4                     [-]
%
%   h_s/(R_s*T) = A1 + A2/2*T + A3/3*T^2 + A4/4*T^3 + A5/5*T^4 + A6/T   [-]
%
%   g_s/(R_s*T) = A1*(1-log(T)) - A2/2*T - A3/6*T^2 - A4/12*T^3 - A5/20*T^4 + A6/T - A7   [-]
%
% Usage:
%   (1)
%       [h_over_R_s,cp_over_R_s,g_over_R_s] = eval_NASA7_fit(T,AThermFuncs_s)
%
%   (2)
%       [...,dcp_dT_over_R_s] = eval_NASA7_fit(...)
%       |--> returns also the gradient of cp with T over R_s
%
% Inputs and outputs:
%   T               [K]             (N_eta x 1)
%       temperature at which to evaluate the fit
%   AThermFuncs_s   [SI]
%       structure with anonymous functions for the A1-A6 coefficients
%
% See also: getSpecies_h_cp_NASA7
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

A1 = AThermFuncs_s.A1(T);    A2 = AThermFuncs_s.A2(T);                    % evaluating coefficients
A3 = AThermFuncs_s.A3(T);    A4 = AThermFuncs_s.A4(T);
A5 = AThermFuncs_s.A5(T);    A6 = AThermFuncs_s.A6(T);
h_over_R_s  = A1.*T + A2/2.*T.^2 + A3/3.*T.^3 + A4/4.*T.^4 + A5/5.*T.^5 + A6;    % species enthalpy
cp_over_R_s = A1    + A2  .*T    + A3  .*T.^2 + A4  .*T.^3 + A5  .*T.^4;         % species heat capacity at cst. p
if nargout>3
    varargout{2} =    A2         + 2*A3.*T    + 3*A4.*T.^2 + 4*A5.*T.^3;         % temperature derivative of species cp
end
if nargout>2
    A7 = AThermFuncs_s.A7(T);
    varargout{1} = T.*(A1.*(1-log(T)) - A2/2.*T - A3/6.*T.^2 - A4/12.*T.^3 - A5/20.*T.^4 + A6./T - A7); % species gibbs free energy
end

end % eval_NASA7_fit