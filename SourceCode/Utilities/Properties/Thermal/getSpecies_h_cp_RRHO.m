function [h_s,cp_s,cv_s,varargout] = getSpecies_h_cp_RRHO(TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options)
% GETSPECIES_H_CP Calculates the species enthalpy and the heat capacities
% using the RRHO model.
%
% Usage:
%   (1)
%       [h_s,cp_s,cv_s] = getSpecies_h_cp_RRHO(TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,...
%                           thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options)
%
%   (2)
%       [h_s,cp_s,cv_s,hMod,cpMod,cvMod] = getSpecies_h_cp_RRHO(...)
%       |--> will also return structures with the enthalpies and heat
%       capacities of the different energy modes.
%
% Inputs:
%   TTrans          [K]             (N_eta x N_spec)
%       translational temperature
%   TRot            [K]             (N_eta x N_spec)
%       rotational temperature 
%   TVib            [K]             (N_eta x N_spec)
%       vibrational temperature
%   TElec           [K]             (N_eta x N_spec)
%       electronic temperature 
%   Tel             [K]             (N_eta x N_spec)
%       electron temperature
%   R_s             [J/kg-K]        (N_eta x N_spec)
%       species gas constant
%   nAtoms_s        [-]             (N_eta x N_spec)
%       number of atoms in the species' molecule
%   thetaVib_s      [K]             (N_eta x N_spec x Nvib)
%       species vibrational activation temperature
%   thetaElec_s     [K]             (N_eta x N_spec x Nstat)
%       species activation temperature of electronic states
%   gDegen_s        [-]             (N_eta x N_spec x Nstat)
%       degeneracies of the species' electronic state
%   bElectron       [-]             (N_eta x N_spec)
%       electron boolean 
%   hForm_s         [J/kg]          (N_eta x N_spec)
%       enthalpy of formation of each species 
%   options
%       necessary fields:
%           .modelThermal
%           .numberOfTemp
%           .neglect_hRot
%           .neglect_hVib
%           .neglect_hElec
%
% Outputs:
%   h_s                 [J/kg]              (N_eta x N_spec)
%       species enthalpy
%   cp_s                [J/kg-K]            (N_eta x N_spec)
%       species heat capacity at constant pressure
%   cv_s                [J/kg-K]            (N_eta x N_spec)
%       species heat capacity at constant volume
%   hMod
%       structure with the contribution of each energy mode to the species
%       enthalpy
%   cpMod
%       structure with the contribution of each energy mode to the species
%       heat capacity at constant pressure
%   cvMod
%       structure with the contribution of each energy mode to the species
%       heat capacity at constant volume
%
% Notes:
% This function assumes the sizes of the inputs are correct: N_eta x N_spec
% (with the exception of thetaElec_s and gDegen_s: these will have their 3rd
% dimension represent the number of electronic states considered)
% This input format is unlike most other get<Property> functions because
% getSpecies_h_cp_RRHO is called for every iteration within Newton-Raphson, so
% speed and reducing redundancy is critical for performance.
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

[hTrans_s,cpTrans_s]    = getSpecies_hTrans(TTrans,Tel,R_s,bElectron);
if ~options.neglect_hRot;           [hRot_s,cpRot_s]        = getSpecies_hRot(TRot,R_s,nAtoms_s);
else;                               hRot_s = zeros(size(TTrans));       cpRot_s = zeros(size(TTrans));
end
if ~options.neglect_hVib;           [hVib_s,cpVib_s]        = getSpecies_hVib(TVib,R_s,thetaVib_s);
else;                               hVib_s = zeros(size(TTrans));       cpVib_s = zeros(size(TTrans));
end
if ~options.neglect_hElec;          [hElec_s,cpElec_s]  = getSpecies_hElec(TElec,R_s,thetaElec_s,gDegen_s,bElectron);
else;                               hElec_s = zeros(size(TTrans));      cpElec_s = zeros(size(TTrans));
end

% obtaining mixture enthalpy
cp_s = cpTrans_s + cpRot_s + cpVib_s + cpElec_s;
cv_s = cp_s - R_s;
switch options.modelThermal
    case 'RRHO'
        h_s = hTrans_s + hRot_s + hVib_s + hElec_s + hForm_s;
    case 'RRHO-1LA' % first linear approximation to RRHO
        h_s = cp_s.*TTrans + hForm_s;
    otherwise
        error(['the chosen thermal model ''',options.modelThermal,''' is not supported.']);
end

% building structures with different energy modes
if nargout>3
    hMod.hTrans_s = hTrans_s;
    hMod.hRot_s = hRot_s;
    hMod.hVib_s = hVib_s;
    hMod.hElec_s = hElec_s;

    cpMod.cpTrans_s = cpTrans_s;
    cpMod.cpRot_s = cpRot_s;
    cpMod.cpVib_s = cpVib_s;
    cpMod.cpElec_s = cpElec_s;

    cvMod.cvTrans_s = cpTrans_s-R_s;
    cvMod.cvRot_s = cpRot_s;
    cvMod.cvVib_s = cpVib_s;
    cvMod.cvElec_s = cpElec_s;

    varargout{1} = hMod;
    varargout{2} = cpMod;
    varargout{3} = cvMod;
end

end % getSpecies_h_cp_RRHO