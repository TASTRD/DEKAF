function [gg,dgg_deta,dgg_deta2] = getEnthalpy_semitotal2static_nonDim(g,dg_deta,dg_deta2,df_deta,df_deta2,df_deta3,Ecu)
% getEnthalpy_semitotal2static_nonDim transforms from the non-dimensional
% semi-total enthalpy g to the static enthalpy gg.
%
% Usage:
% (1)
%       [gg,dgg_deta,dgg_deta2] = getEnthalpy_semitotal2static_nonDim(g,dg_deta,dg_deta2,df_deta,df_deta2,df_deta3,Ecu)
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

a = 1+Ecu/2;
gg        = a*g        - Ecu/2*df_deta.^2;
dgg_deta  = a*dg_deta  - Ecu*df_deta.*df_deta2;
dgg_deta2 = a*dg_deta2 - Ecu.*df_deta2.^2 - Ecu*df_deta.*df_deta3;