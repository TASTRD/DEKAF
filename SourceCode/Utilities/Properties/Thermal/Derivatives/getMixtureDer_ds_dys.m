function ds_dym = getMixtureDer_ds_dys(y_s,T,p,R0,Mm_s,bElectron,nAvogadro,nAtoms_s,...
                                  thetaVib_s,thetaElec_s,gDegen_s,hForm_s,hPlanck,...
                                  kBoltz,L_s,sigma_s,thetaRot_s,options)
% getSpeciesDer_dss_dym computes the derivative of the mixture entropy with
% respect to the mass fraction of a species s
%
% Usage:
%   (1)
%       ds_dys = getMixtureDer_ds_dys(y_s,TTrans,Tel,p,R0,Mm_s,bElectron,nAvogadro,nAtoms_s,...
%                                   thetaVib_s,thetaElec_s,gDegen_s,hForm_s,hPlanck,kBoltz,...
%                                   L_s,sigma_s,thetaRot_s)
%
% Inputs and Outputs:
%   y_s                 N_eta x N_spec
%   TTrans              N_eta x 1
%   Tel                 N_eta x 1
%   p                   N_eta x 1
%   R0                  1 x 1
%   Mm_s                N_spec x 1
%   bElectron           N_spec x 1
%   nAvogadro           1 x 1
%   nAtoms_s            N_spec x 1
%   thetaVib_s          N_spec x 1
%   thetaElec_s         N_spec x N_mod
%   gDegen_s            N_spec x N_mod
%   hForm_s             N_spec x 1
%   hPlanck             1 x 1
%   kBoltz              1 x 1
%   L_s                 N_spec x 1
%   sigma_s             N_spec x 1
%   thetaRot_s          N_spec x 1
%   options             structure
%   dss_dym             N_eta x N_spec x N_spec (eta,m,s)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

options.EoS          = protectedvalue(options,'EoS',         'idealGas');
options.numberOfTemp = protectedvalue(options,'numberOfTemp','1T');

% computing intermediate variables
TTrans = T;     TRot = T;
TVib = T;       TElec = T;
Tel = T;
rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);
R_s = R0./Mm_s;
s_s = getSpecies_s_plus_Rslnns(TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,...
                      thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,Mm_s,...
                      nAvogadro,hPlanck,kBoltz,L_s,sigma_s,thetaRot_s,options);
dss_dym = getSpeciesDer_dss_dym(y_s,TTrans,Tel,p,R0,Mm_s,bElectron,nAvogadro);

% obtaining number densities
lnn_s = log(getSpecies_n(rho,y_s,Mm_s,nAvogadro));
lnn_s(y_s==0) = 0;                                      % condition to ensure that there is no 0*Inf situation

% resizing
[N_eta,N_spec] = size(y_s);
delta_ms    = repmat(permute(eye(N_spec),[3,1,2]),  [N_eta,1,1]);   % (m,s)   --> (eta,m,s)
s_s         = repmat(permute(s_s,[1,3,2]),          [1,N_spec,1]);  % (eta,s) --> (eta,m,s)
y_s         = repmat(permute(y_s,[1,3,2]),          [1,N_spec,1]);  % (eta,s) --> (eta,m,s)
lnn_s       = repmat(permute(lnn_s,[1,3,2]),        [1,N_spec,1]);  % (eta,s) --> (eta,m,s)

ds_dym = zeros(N_eta,N_spec);
for s=1:N_spec
    ds_dym = ds_dym + delta_ms(:,:,s) .* (s_s(:,:,s)-R_s(s).*lnn_s(:,:,s)) + y_s(:,:,s).*dss_dym(:,:,s);
end
