function dcvVibs_dT = getSpeciesDer_dcvVibs_dT(T,R_s,thetaVib_s)
% getSpeciesDer_dcvVibs_dT is a function that returns the temperature
% derivative of the species vibrational specific heat at constant volume.
%
% dcvVibs_dT = getSpeciesDer_dcvVibs_dT(T,R_s,thetaVib_s)
%
% The required inputs are:
%   - T: temperature matrix of size N_eta x 1
%   - R_s: species gas constant N_spec x 1
%   - thetaVib_s: species vibrational activation temperature N_spec x N_vib
%
% Assumptions: molecules behave as harmonic oscillators
%
% Notes: unlike the other enthalpy and cv functions, this one is NOT used
% in the newton-raphson algorithm to obtain T from h, so it must not be
% optimized for speed. For this reason, inputs are expected in their usual
% size.
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

% reshaping
N_eta       = length(T);
[N_spec,N_vib]= size(thetaVib_s);
T           = repmat(T,         [1,N_spec]);
R_s         = repmat(R_s',      [N_eta,1]);
thetaVib_s  = repmat(reshape(thetaVib_s,[1,N_spec,N_vib]), [N_eta,1,1]);

% calculating derivative of the heat capacity
dcvVibs_dT  = zeros(N_eta,N_spec);
for lv = 1:N_vib
    addend = R_s.*thetaVib_s(:,:,lv).^2./T.^3 .* (2*thetaVib_s(:,:,lv)./T .* exp(2*thetaVib_s(:,:,lv)./T) ./ (exp(thetaVib_s(:,:,lv)./T)-1).^3 - exp(thetaVib_s(:,:,lv)./T).*(2+thetaVib_s(:,:,lv)./T)./(exp(thetaVib_s(:,:,lv)./T)-1).^2 );
    addend(thetaVib_s(:,:,lv)==0) = 0;
    dcvVibs_dT = dcvVibs_dT + addend;
end

end % getSpeciesDer_dcvVibs_dT