function ds_dT = getMixtureDer_ds_dT(y_s,TTrans,Tel,p,cp_s,R0,Mm_s,bElectron,nAvogadro)
% getMixtureDer_ds_dT computes the derivative of the mixture entropy with
% respect to temperature
%
% NOTE: THIS FUNCTION IS INVALID FOR THERMAL NONEQUILIBRIUM
% FIXME
%
% Usage:
%   (1)
%       ds_dT = getMixtureDer_ds_dT(y_s,TTrans,Tel,p,cp_s,R0,Mm_s,bElectron,nAvogadro)
%
% Inputs and Outputs:
%   y_s                 N_eta x N_spec
%   TTrans              N_eta x 1
%   Tel                 N_eta x 1
%   p                   N_eta x 1
%   cp_s                N_eta x N_spec
%   R0                  1 x 1
%   Mm_s                N_spec x 1
%   bElectron           N_spec x 1
%   nAvogadro           1 x 1
%   dss_dT              N_eta x N_spec
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% computing intermediate variables
N_spec = size(y_s,2);
dss_dT = getSpeciesDer_dss_dT(y_s,TTrans,Tel,p,cp_s,R0,Mm_s,bElectron,nAvogadro);
ds_dT = (dss_dT.*y_s)*ones(N_spec,1);