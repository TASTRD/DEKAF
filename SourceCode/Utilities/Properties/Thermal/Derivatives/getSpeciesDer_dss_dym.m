function [dss_dym] = getSpeciesDer_dss_dym(y_s,TTrans,Tel,p,R0,Mm_s,bElectron,nAvogadro)
% getSpeciesDer_dss_dym computes the derivative of the entropy of species s
% with respect to the mass fraction of another species m
%
% NOTE: THIS FUNCTION IS INVALID FOR THERMAL NONEQUILIBRIUM
% FIXME
%
% Usage:
%   (1)
%       dss_dym = getSpeciesDer_dss_dym(y_s,TTrans,Tel,p,R0,Mm_s,bElectron,nAvogadro)
%
% Inputs and Outputs:
%   y_s                 N_eta x N_spec
%   TTrans              N_eta x 1
%   Tel                 N_eta x 1
%   p                   N_eta x 1
%   R0                  1 x 1
%   Mm_s                N_spec x 1
%   bElectron           N_spec x 1
%   nAvogadro           1 x 1
%   dss_dym             N_eta x N_spec x N_spec (eta,m,s)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% computing intermediate variables
R_s = R0./Mm_s;
rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);
n_s = getSpecies_n(rho,y_s,Mm_s,nAvogadro);
drho_dym = getMixtureDer_drho_dym(p,y_s,R0,Mm_s,bElectron,TTrans);
drhos_dym = getSpeciesDer_drhol_dym(rho,drho_dym,y_s);

% resizing
[N_eta,N_spec] = size(y_s);
n_s = repmat(permute(n_s,[1,3,2]),[1,N_spec,1]);        % (eta,s) --> (eta,m,s)
R_s = repmat(permute(R_s,[2,3,1]),[N_eta,N_spec,1]);    % (s,1) --> (eta,m,s)
Mm_s = repmat(permute(Mm_s,[2,3,1]),[N_eta,N_spec,1]);  % (s,1) --> (eta,m,s)

% computing gradient
dss_dym = -R_s./n_s.*nAvogadro./Mm_s.*drhos_dym;