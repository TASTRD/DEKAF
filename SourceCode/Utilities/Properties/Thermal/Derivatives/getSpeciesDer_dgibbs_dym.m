function dgibbs_dym = getSpeciesDer_dgibbs_dym(TTrans,Tel,p,rho,y_s,drho_dym,Mm_s,R0,bElectron)
%GETSPECIESDER_DGIBBS_DYM Derivative of species gibbs free energy wrt
% species mass fractions
%
% Inputs:
%   TTrans      -   Temperature for the translational mode (N_eta x 1)
%   Tel         -   Temperature for the electrons (N_eta x 1)
%   p           -   mixture static pressure (N_eta x 1)
%   rho         -   mixture mass density (N_eta x 1)
%   y_s         -   species mass fractions, matrix of size (N_eta x N_spec)
%   drho_dym    -   mixture mass density derivative wrt mass fraction
%                   (N_eta x N_m)
%   Mm_s        -   species mixture molar mass, vector of size
%                   (N_spec x 1)
%   R0          -   universal gas constant
%   bElectron   -   electron boolean (N_spec x 1)
%
% Outputs:
%   dgibbs_dym  -   species partial pressure derivative wrt temperature
%                   [N_eta x N_spec x N_spec]
%
% Notes:
%   m is the first species index, i.e., the second dimension of dgibbs_dym
%   s is the second species index, i.e., the third dimension of dgibbs_dym
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

dp_dym = getMixtureDer_dp_dym(rho,y_s,TTrans,Tel,Mm_s,R0,bElectron,drho_dym); % size [N_eta x N_m]

[N_eta,N_spec]  = size(dp_dym);
R_s             = repmat(reshape(R0./Mm_s,[1,1,N_spec]),[N_eta,N_spec,1]);  % size [N_eta x N_m x N_s]
bElectron       = repmat(reshape(bElectron,[1,1,N_spec]),[N_eta,N_spec,1]);  % size [N_eta x N_m x N_s]
TTrans          = repmat(TTrans,[1,N_spec,N_spec]);
Tel             = repmat(Tel,[1,N_spec,N_spec]);
p               = repmat(p,[1,N_spec,N_spec]);
dp_dym          = repmat(dp_dym,[1,1,N_spec]);

dgibbs_dym      = eval_func_Heavy_el(@(T) R_s.*T./p.*dp_dym,TTrans,Tel,bElectron); % size [N_eta x N_m x N_s]

end % getSpeciesDer_dgibbs_dym.m
