function dss_dT = getSpeciesDer_dss_dT(y_s,TTrans,Tel,p,cp_s,R0,Mm_s,bElectron,nAvogadro)
% getSpeciesDer_dss_dT computes the derivative of the entropy of species s
% with respect to temperature
%
% Usage:
%   (1)
%       dss_dT = getSpeciesDer_dss_dT(y_s,TTrans,Tel,p,cp_s,R0,Mm_s,bElectron,nAvogadro)
%
% Inputs and Outputs:
%   y_s                 N_eta x N_spec
%   TTrans              N_eta x 1
%   Tel                 N_eta x 1
%   p                   N_eta x 1
%   cp_s                N_eta x N_spec
%   R0                  1 x 1
%   Mm_s                N_spec x 1
%   bElectron           N_spec x 1
%   nAvogadro           1 x 1
%   dss_dT              N_eta x N_spec
%
% FIXME: THIS FUNCTION IS INVALID FOR THERMAL NONEQUILIBRIUM
%
% FIXME: THIS FUNCTION IS INVALID FOR NON-IDEAL EQUATIONS OF STATE
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% computing intermediate variables
R_s = R0./Mm_s;
rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);
n_s = getSpecies_n(rho,y_s,Mm_s,nAvogadro);
drho_dT = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,TTrans);
drhos_dT = getSpeciesDer_drhol_dT(drho_dT,y_s);

% resizing
[N_eta,N_spec] = size(y_s);
TTrans  = repmat(TTrans,[1,N_spec]);                % (eta,1) --> (eta,s)
R_s     = repmat(permute(R_s,[2,1]),[N_eta,1]);     % (s,1) --> (eta,s)
Mm_s    = repmat(permute(Mm_s,[2,1]),[N_eta,1]);    % (s,1) --> (eta,s)

% computing gradient
dss_dT = R_s./TTrans - R_s./n_s.*nAvogadro./Mm_s.*drhos_dT + cp_s./TTrans;