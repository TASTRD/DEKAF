function dhVibs_dT = getSpeciesDer_dhVibs_dT(T,R_s,thetaVib_s,numberOfTemp)
% getSpeciesDer_dhVibs_dT computes the derivative of the species
% vibrational enthalpy wrt all the system temperatures.
%
% Usage:
%   (1)
%       dhVibs_dT = getSpeciesDer_dhVibs_dT(T,R_s,thetaVib_s,numberOfTemp)
%
% Inputs and outputs:
%   T           [K]
%       temperature matrix of size N_eta x 1
%   R_s         [J/kg-K]
%       species gas constant N_spec x 1
%   thetaVib_s  [K]
%       species vibrational activation temperature N_spec x N_vib
%   numberOfTemp
%       string identifying the number of temperatures that are used to
%       describe the thermal state of the gas
%   dhVibs_dT           [J/kg-K]
%       derivative of the species vibrational enthalpy wrt all the system
%       temperatures (size N_eta x N_spec x N_T)
%
% Author(s): Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

[~,cpVib_s] = getSpecies_hVib(T,R_s,thetaVib_s,'notRepmatted');

switch numberOfTemp
    case '1T'
        dhVibs_dT = cpVib_s;
    case '2T'
        dhVibs_dT(:,:,2) = cpVib_s; % Tv derivative
        dhVibs_dT(:,:,1) = 0;       % T derivative
        % this last declaration is not really necessary, but we keep it for clarity
    otherwise
        error(['the chosen numberOfTemp ''',numberOfTemp,''' is not supported']);
end

end % getSpeciesDer_dhVibs_dT