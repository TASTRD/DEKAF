function dgibbs_dT = getSpeciesDer_dgibbs_dT(T,y_s,p,rho,drho_dT,R_s,thetaRot_s,L_s,sigma_s,...
                                             Mm_s,nAtoms_s,thetaVib_s,gDegen_s,...
                                             thetaElec_s,bElectron,R0,nAvogadro,hPlanck,kBoltz,options)
% GETSPECIESDER_DGIBBS_DT(T,y_s,rho,drho_dT,R_s,thetaRot_s,Mm_s,nAtoms_s,thetaVib_s,
% gDegen_s,thetaElec_s,nAvogadro,hPlanck,kBoltz) returns the derivative with
% temperature of the gibbs free energy of each species. The inputs are:
%
% IMPORTANT: this function is not ready for multi-temperature models
%
%  - T                          (size N_eta x 1)
%  - y_s                        (size N_eta x N_spec)
%  - p                          (size N_eta x N_spec)
%  - rho                        (size N_eta x 1)
%  - drho_dT                    (size N_eta x 1)
%  - R_s                        (size N_spec x 1)
%  - thetaRot_s                 (size N_spec x 1)
%  - L_s                        (size N_spec x 1)
%  - sigma_s                    (size N_spec x 1)
%  - Mm_s                       (size N_spec x 1)
%  - nAtoms_s                   (size N_spec x 1)
%  - thetaVib_s                 (size N_spec x 1)
%  - gDegen_s                   (size N_spec x N_stat)
%  - thetaElec_s                (size N_spec x N_stat)
%  - bElectron                  (size N_spec x 1)
%  - R0,nAvogadro,hPlanck,kBoltz (size 1)
%
% Output: dgibbs_dT             (size N_eta x N_spec)
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

N_eta = length(T);
N_spec = length(Mm_s);
N_stat = size(thetaElec_s,2);

% Calculating the natural logarithm of the partition function of each energy mode
lnQTransV_s = getSpecies_lnQTransV(T,T,Mm_s,nAvogadro,hPlanck,kBoltz,bElectron);
lnQRot_s = getSpecies_lnQRot(T,thetaRot_s,L_s,sigma_s);
lnQVib_s = getSpecies_lnQVib(T,thetaVib_s);
lnQElec_s = getSpecies_lnQElec(T,gDegen_s,thetaElec_s,bElectron);

dp_dT = getMixtureDer_dp_dT(rho,y_s,T,T,Mm_s,R0,bElectron,drho_dT);     % derivative of mixture pressure with temperature

% Repeating and reshaping
% we reshape to feed the h functions because they're optimized to work
% vectorially such that the iteration on h to get T is as fast as possible.
nAtoms_s            = repmat(nAtoms_s',[N_eta,1]);
R_s                 = repmat(R_s',[N_eta,1]);
thetaVib_s          = repmat(thetaVib_s',[N_eta,1]);
thetaElec_s         = repmat(reshape(thetaElec_s,[1,N_spec,N_stat]),[N_eta,1,1]);
gDegen_s            = repmat(reshape(gDegen_s,[1,N_spec,N_stat]),[N_eta,1,1]);
bElectron           = repmat(bElectron',[N_eta,1]);
T                   = repmat(T,[1,N_spec]);
p                   = repmat(p,[1,N_spec]);
dp_dT               = repmat(dp_dT,[1,N_spec]);

% obtaining derivatives from the enthalpies of the different modes
% assume ideal gas for lnQtrans
hTrans_s = getSpecies_hTrans(T,T,R_s,true(size(R_s)));
dlnQTrans_dT = (hTrans_s - R_s.*T) ./ (R_s.*T.^2);                               % derivative of the logarithm of the partition function of the translational energy mode of the species
dlnQRot_dT = getSpecies_hRot(T,R_s,nAtoms_s) ./ (R_s.*T.^2);                     % derivative of the logarithm of the partition function of the rotational energy mode of the species
dlnQVib_dT = getSpecies_hVib(T,R_s,thetaVib_s) ./ (R_s.*T.^2);                     % derivative of the logarithm of the partition function of the vibrational energy mode of the species
dlnQElec_dT = getSpecies_hElec(T,R_s,thetaElec_s,gDegen_s,bElectron) ./ (R_s.*T.^2); % derivative of the logarithm of the partition function of the electronic energy mode of the species

% Calculating derivatives of the gibbs free energies of the species
dgibbs_dT = -R_s.*(lnQTransV_s - log(p./(kBoltz.*T)) + lnQRot_s + lnQVib_s + lnQElec_s) ...
    - R_s.*T.*(dlnQTrans_dT - 1./p.*dp_dT + 1./T + dlnQRot_dT + dlnQVib_dT + dlnQElec_dT);

end