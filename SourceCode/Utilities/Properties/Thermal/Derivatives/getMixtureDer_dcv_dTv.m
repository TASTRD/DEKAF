function dcv_dTv = getMixtureDer_dcv_dTv(Tv,y_s,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options,varargin)
% getMixtureDer_dcv_dTv computes the derivative of the heat capacity ratio
% with respect to the vib-elec-el temperature.
%
% This function can also be used as dcv_dT or dcp_dT for 1-T models, since
% the only contributions to these derivatives are those of the Vib. and
% Elec. energy modes, which have a non-constant cv.
%
% Usage:
%   (1)
%       dcv_dTv = getMixtureDer_dcv_dTv(Tv,y_s,R_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,options)
%
%   (2)
%       [...] = getMixtureDer_dcv_dTv(...,AThermFuncs_s)
%       |--> usage for polynomial thermal models ('NASA7' or
%       'customPoly7'), where the coefficients of the polynomials must be
%       passed.
%
% IMPORTANT!!!
%   for the 'NASA7' and 'customPoly7' thermal models, neglect_hVib, and
%   neglect_hElec ARE NOT SUPPORTED - all energy modes are always considered.
%   Also, there is no separation between the individual contribution of the
%   vibrational and the electronic energy modes. There ensemble
%   contribution is stored in the Vib mode.
%
% The required inputs are:
%   TVib            [K]             (N_eta x N_spec)
%       vibrational temperature
%   TElec           [K]             (N_eta x N_spec)
%       electronic temperature
%   Tel             [K]             (N_eta x N_spec)
%       electron temperature
%   R_s             [J/kg-K]        (N_eta x N_spec)
%       species gas constant
%   thetaVib_s      [K]             (N_eta x N_spec x Nvib)
%       species vibrational activation temperature
%   thetaElec_s     [K]             (N_eta x N_spec x Nstat)
%       species activation temperature of electronic states
%   gDegen_s        [-]             (N_eta x N_spec x Nstat)
%       degeneracies of the species' electronic state
%   bElectron       [-]             (N_eta x N_spec)
%       electron boolean
%   options
%       necessary fields:
%           .modelThermal
%           .numberOfTemp
%           .neglect_hRot
%           .neglect_hVib
%           .neglect_hElec
%   AThermFuncs_s       [SI]                (N_spec x 1)
%       structured cell array with the anonymous functions for each
%       species. Each cell-array position contains N_coef anonymous
%       functions:
%           .A1, .A2, etc
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_spec = size(y_s,2); % number of species
% contribution of the various energy modes
switch options.modelThermal
    case {'RRHO','RRHO-1LA'}
        if options.neglect_hVib;    dcvVibs_dT = zeros(size(y_s));
        else;                       dcvVibs_dT = getSpeciesDer_dcvVibs_dT(Tv,R_s,thetaVib_s);
        end
        if options.neglect_hElec;   dcvElecs_dT = zeros(size(y_s));
        else;                       dcvElecs_dT = getSpeciesDer_dcvElecs_dT(Tv,R_s,thetaElec_s,gDegen_s,bElectron);
        end
    case {'NASA7','customPoly7'}
        protectedvalue(options,'neglect_hVib',false);
        protectedvalue(options,'neglect_hElec',false);
        AThermFuncs_s   = varargin{1};
        for s=N_spec:-1:1
            [~,~,~,dcvVibs_dT_over_R] = eval_NASA7_fit(Tv(:,1),AThermFuncs_s(s));
            dcvVibs_dT(:,s) = R_s(s).*dcvVibs_dT_over_R;
        end
        dcvElecs_dT = zeros(size(y_s));
    otherwise
        error(['The chosen modelThermal ''',options.modelThermal,''' is not supported']);
end
% Assembling
dcv_dTv = (y_s .* dcvVibs_dT + dcvElecs_dT) * ones(N_spec,1);

end % getMixtureDer_dcv_dTv