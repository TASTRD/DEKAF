function dcvElecs_dT = getSpeciesDer_dcvElecs_dT(T,R_s,thetaElec_s,gDegen_s,bElectron)
% getSpeciesDer_dcvElecs_dT is a function that returns the temperature
% derivative of the species electronic specific heat at constant volume.
%
% dcvElecs_dT = getSpeciesDer_dcvElecs_dT(T,R_s,thetaElec_s,gDegen_s,bElectron)
%
% The required inputs are:
%   - T: temperature matrix of size N_spec x 1
%   - R_s: species gas constant N_spec x 1
%   - thetaElec_s: activation temperature of the species' electronic states N_spec x N_stat x 1
%   - gDegen_s: degeneracies of the species' electronic state N_spec x N_stat x 1
%   - bElectron: boolean electron matrix N_eta x N_spec
%
% Assumptions:
%   QElec = sum(g_L*exp(-theta_L/T)) over all electronic energy levels
%   eElec = RT^2*(dlnQElec_dT)
%   cvElec = deElec_dT
%
% Notes: unlike the other enthalpy and cv functions, this one is NOT used
% in the newton-raphson algorithm to obtain T from h, so it must not be
% optimized for speed. For this reason, inputs are expected in their usual
% size.
%
% See also: listOfVariablesExplained
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% resizing
N_eta = length(T);
[N_spec,N_stat] = size(gDegen_s);
gDegen_s    = repmat(reshape(gDegen_s,      [1,N_spec,N_stat]), [N_eta,1,1]);
thetaElec_s = repmat(reshape(thetaElec_s,   [1,N_spec,N_stat]), [N_eta,1,1]);
T           = repmat(T,                                         [1,N_spec]);
R_s         = repmat(R_s',                                      [N_eta,1]);

% computing sums
sum1 = zeros(size(T));
sum2 = zeros(size(T));
sum3 = zeros(size(T));
sum4 = zeros(size(T));
for ii=1:N_stat
    sum1 = sum1 + gDegen_s(:,:,ii)                           .* exp(-thetaElec_s(:,:,ii)./T);
    sum2 = sum2 + gDegen_s(:,:,ii) .* thetaElec_s(:,:,ii)    .* exp(-thetaElec_s(:,:,ii)./T);
    sum3 = sum3 + gDegen_s(:,:,ii) .* thetaElec_s(:,:,ii).^2 .* exp(-thetaElec_s(:,:,ii)./T);
    sum4 = sum4 + gDegen_s(:,:,ii) .* thetaElec_s(:,:,ii).^3 .* exp(-thetaElec_s(:,:,ii)./T);
end

% evaluating derivative
dcvElecs_dT = -2*R_s./T.^3 .* (sum3./sum1 - (sum2./sum1).^2) + R_s./T.^4 .* (sum4./sum1 - 3*sum3.*sum2./sum1.^2 + 2*(sum2./sum1).^3);

% enforcing that electrons don't have electronic energy
dcvElecs_dT(:,bElectron) = 0;
