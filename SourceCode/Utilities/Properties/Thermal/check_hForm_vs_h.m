function [] = check_hForm_vs_h(hForm,h,y_s,eta,spec_list)
% check_hForm_vs_h compares the formation enthalpy at every point in the
% domain to the total enthalpy.
%
% If the formation enthalpy is larger than the total, it launches an error
% and plots the comparison.
%
% Inputs:
%   hForm
%       mixture formation enthalpy [N_eta x 1]
%   h
%       mixture total enthalpy [N_eta x 1]
%   y_s
%       species mass fractions [N_eta x N_spec]
%   eta
%       self-similar wall-normal variable [N_eta x 1]
%   spec_list
%       cell of strings with the name of each species
%
% Author: Fernando Miro Miro
% Date: May 2019
% GNU Lesser General Public License 3.0

if any(hForm>h)
    [~,lines,linewidths] = get_plttngStyles();
    figure
    subplot(1,2,1);
    plot(hForm,eta,'k',h,eta,'r'); legend('h^{Form}','h');
    xlabel('h [J/kg]'); ylabel('\eta [-]'); ylim([0,eta(round(end/2))]);
    subplot(1,2,2);
    N_spec = length(spec_list);
    for s=1:N_spec
        semilogx(y_s(:,s),eta,lines{s},'Color',two_colors(s,N_spec,'Rbw2'),'linewidth',linewidths(s)); hold on;
    end
    xlabel('Y_s [-]'); ylabel('\eta [-]'); ylim([0,eta(round(end/2))]); legend(spec_list{:});
    error('the formation enthalpy is larger than the mixture somewhere in the domain.');
end


end % check_hForm_vs_h