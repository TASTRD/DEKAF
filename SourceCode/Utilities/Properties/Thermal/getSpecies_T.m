function T_s = getSpecies_T(TTrans,Tel,bElectron)
% getSpecies_T returns the temperature describing the translational motion
% of each species.
%
% It will be the translational for all heavy species, and the electron for
% electron species.
%
% Usage:
%   (1)
%       T_s = getSpecies_T(TTrans,Tel,bElectron)
%
% Inputs and outputs:
%   TTrans
%       translational temperature (N_eta x 1)
%   Tel
%       electron temperature (N_eta x 1)
%   bElectron
%       electron boolean (N_eta x 1)
%   T_s
%       species temperature (N_eta x N_spec)
%
% Author(s): Fernando Miro Miro
% Date: May 2019
% GNU Lesser General Public License 3.0

N_spec = length(bElectron);
T_s = repmat(TTrans,[1,N_spec]);    % by default --> translational temperature
if any(bElectron)                   % if there are electron species
    T_s(:,bElectron) = Tel;             % |--> electron temperature
end

end % getSpecies_T