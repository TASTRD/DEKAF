function cptr = getMixture_cptr(y_s,R_s,nAtoms_s,bElectron,options)
% getMixture_cptr returns the trans-rot heat capacity of the mixture
% assuming a 2T model.
%
% Examples
%   (1)     cptr = getMixture_cptr(y_s,R_s,nAtoms_s,bElectron,options)
%
% IMPORTANT!! Electrons are excluded from this computation, since their
% translational energy is collected into the vib-elec-el energy grouping
%
% The required inputs are:
%   - y_s: mass fraction matrix of size (N_eta x N_spec)
%   - R_s: species gas constant (N_eta x N_spec)
%   - nAtoms_s: number of atoms in the species' molecule (N_eta x N_spec)
%   - options: options structure that must contain:
%            .neglect_hRot
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% obtaining number of species
N_spec = size(y_s,2);

cpTrans_s = 5/2*R_s;
if options.neglect_hRot;    cpRot_s = zeros(size(cpTrans_s));
else;                       cpRot_s = (nAtoms_s>1).*R_s;
end

cptr_s = ~bElectron .* (cpTrans_s + cpRot_s); % trans-rot heat capacity of each species
cptr = (y_s.*cptr_s)*ones(N_spec,1);
