function AThermFuncs_s = populate_thermalPolyCoefs(ATherm_s,Tlims_s,bLinearInterp)
% populate_thermalPolyCoefs populates the anonymous functions needed to obtain
% the curve-fit coefficients for each species depending on the temperature.
%
% Usage:
%   (1)
%       AThermFuncs_s = populate_thermalPolyCoefs(ATherm_s,Tlims,bLinearInterp)
%
% Inputs and outputs:
%   Tlims_s         [K]
%       N_spec x 1 cell with vectors (size 1 x N_range+1) with the limits
%       of applicability of the different polynomials.
%   ATherm_s        [SI]
%       N_spec x 1 cell with matrices (size N_coef x N_range) with the
%       polynomial coefficients for a polynomial thermal model. N_range
%       must be equal to the length of each Tlims_s{s} plus one.
%   bLinearInterp
%       boolean determining whether a linear interpolation should be
%       applied around the limits of applicability of the different
%       polynomials  
%   AThermFuncs_s   [SI]                (N_spec x 1)
%       structured cell array with the anonymous functions for each
%       species. Each cell-array position contains N_coef anonymous
%       functions:
%           .A1, .A2, etc
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_spec = length(ATherm_s); % obtaining sizes
[N_coef,N_range] = size(ATherm_s{1});

for s=N_spec:-1:1 % looping species
    if length(Tlims_s{s})~=N_range+1
        error(['The number of temperature-range limits (',num2str(length(Tlims_s{s})),') for species ',num2str(s),' is not compatible with the size (N_coef x N_range) of the ATherm_s matrix (',num2str(N_coef),' x ',num2str(N_range),')']);
    end
    for ic=N_coef:-1:1                                              % looping coefficients
        if bLinearInterp                                                % we must perform a linear smoothing between coefficients
            Tlim_mat = [Tlims_s{s} ; Tlims_s{s}];                           % matrix of temperature limits
            for ir=3:N_range-1                                              % looping ranges
                if Tlims_s{s}(ir)<2000;     Tmarg = 200;                        % temperature marge around which to linearly interpolate the coefficients [K]
                else;                       Tmarg = 500;
                end
                Tlim_mat(1,ir) = Tlims_s{s}(ir) - Tmarg;                        % applying limits of the interpolation region
                Tlim_mat(2,ir) = Tlims_s{s}(ir) + Tmarg;
            end
            AThermFuncs_s(s).(['A',num2str(ic)]) = @(T) sum( (T>=Tlim_mat(2,1:end-1) & T<Tlim_mat(1,2:end)) .* ATherm_s{s}(ic,:) , 2) + ... % non-interpolated region
                                                        sum( (T>=Tlim_mat(1,3:end-2) & T<Tlim_mat(2,3:end-2)) .* ...                        % interpolating region
                                       matLinearInterp(Tlim_mat(1,3:end-2),Tlim_mat(2,3:end-2),ATherm_s{s}(ic,2:end-2),ATherm_s{s}(ic,3:end-1),T) , 2);
% ---- DEVELOPER'S NOTE ----------------------------------------------------------------------------------------------------------
% What we do in the different temperature ranges, for an example case with four different ranges, plus the clipped region (0-T0-T1-T2-T3-T4-Inf), is:
%
%   Temperature
%  0------------T0----------------T1----------------T2----------------T3----------------T4------------Inf      (original ranges)
%  0------------T0------------T10---T11---------T20---T21---------T30---T31-------------T4------------Inf      (after applying interpolating margins)
%  |            |              |     |           |     |           |     |              |              | 
%  |    A0      |      A1      |A1-A2|    A2     |A2-A3|    A3     |A3-A4|      A4      |     Ainf     |       (coefficients used in each region)
%  |            |              |     |           |     |           |     |              |              | 
%  |    no      |      no      | yes |    no     | yes |    no     | yes |      no      |      no      |       (interpolation - yes/no)
%
% To do that, we define a temperature-limit matrix (Tlim_mat) with:
%
%   [ 0    T0     T10    T20    T30    T4   Inf 
%
%     0    T0     T11    T21    T31    T4   Inf ]
%
% Note that the coefficient vector ATherm_s{s}(ic,:) contains:
%   [ A0 , A1 , A2 , A3 , A4 , Ainf ]
%
% The definition of the anonymous function has two main parts, the first one (and row) accounts for the non-interpolated region:
%         0-T0  ,  T0-T10  ,  T11-T20  ,  T21-T30  ,  T31-T4  ,  T4-Inf
% Therefore the lower limit of these non-interpolating ranges is Tlim_mat(2,1:end-1), and the top limit is Tlim_mat(1,2:end) 
% and the actual values of A to be applied there are A0-Ainf, corresponding to ATherm_s{s}(ic,:)
%   The second part (and row) of the definition of the anonymous function accounts for the interpolated region:
%       T10-T11  ,  T20-T21  ,  T30-T31
% Therefore the lower limit of these interpolating ranges is Tlim_mat(1,3:end-2), and the top limit is Tlim_mat(2,3:end-2)
% and the actual values of A at the lower limit are A1-A3, corresponding to ATherm_s{s}(ic,2:end-2), 
% whilst the values of A at the top limits are A2-A4, corresponding to ATherm_s{s}(ic,3:end-1).
% --------------------------------------------------------------------------------------------------------------------------------
        else                                                            % we don't have to make a linear interpolation, so the expression is much simpler
            AThermFuncs_s(s).(['A',num2str(ic)]) = @(T) sum( (T>=Tlims_s{s}(1:end-1) & T<Tlims_s{s}(2:end)) .* ATherm_s{s}(ic,:) , 2); % adding contribution
        end
    end
end

end % populate_thermalPolyCoefs