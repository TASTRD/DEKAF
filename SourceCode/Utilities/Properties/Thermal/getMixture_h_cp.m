function [h,cp,cv,h_s,varargout] = getMixture_h_cp(y_s,TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,...
                                            thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options,varargin)
% getMixture_h_cp returns the enthalpy and the heat capacities of the mixture.
%
% Examples
%   (1)     h = getMixture_h_cp(y_s,TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,...
%                   thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options)
%
%   (2)     h = getMixture_h_cp(...,AThermFuncs_s)
%     |--> usage for the polynomial thermal models
%
%   (3)     [h,cp,cv] = getMixture_h_cp(...)
%     |--> also outputs the mixture heat capacities at constant pressure
%     and constant volume
%
%   (4)     [h,cp,cv,h_s] = getMixture_h_cp(...)
%     |--> also outputs the species enthalpies
%
%   (5)     [h,cp,cv,h_s,hMod_s,cpMod_s,cvMod_s] = getMixture_h_cp(...)
%     |--> also outputs structures with the species enthalpies and heat
%     capacities separated into the different contribution of the separate
%     modes.
%
%   (6)     [h,cp,cv,h_s,hMod_s,cpMod_s,cvMod_s,hv,cptr,cpv,htr_s,hv_s] = getMixture_h_cp(...)
%     |--> also outputs the mixture vibrational enthalpy and heat capacity
%     at constant pressure for a 2-T model (putting together Vibrational,
%     Electronic and electron energy)
%
% Inputs:
%   y_s             [-]             (N_eta x N_spec)
%       mass fraction matrix 
%   TTrans          [K]             (N_eta x N_spec)
%       translational temperature
%   TRot            [K]             (N_eta x N_spec)
%       rotational temperature 
%   TVib            [K]             (N_eta x N_spec)
%       vibrational temperature
%   TElec           [K]             (N_eta x N_spec)
%       electronic temperature 
%   Tel             [K]             (N_eta x N_spec)
%       electron temperature
%   R_s             [J/kg-K]        (N_eta x N_spec)
%       species gas constant
%   nAtoms_s        [-]             (N_eta x N_spec)
%       number of atoms in the species' molecule
%   thetaVib_s      [K]             (N_eta x N_spec x Nvib)
%       species vibrational activation temperature
%   thetaElec_s     [K]             (N_eta x N_spec x Nstat)
%       species activation temperature of electronic states
%   gDegen_s        [-]             (N_eta x N_spec x Nstat)
%       degeneracies of the species' electronic state
%   bElectron       [-]             (N_eta x N_spec)
%       electron boolean 
%   hForm_s         [J/kg]          (N_eta x N_spec)
%       enthalpy of formation of each species 
%   options
%       necessary fields:
%           .modelThermal
%           .numberOfTemp
%           .neglect_hRot
%           .neglect_hVib
%           .neglect_hElec
%   AThermFuncs_s       [SI]                (N_spec x 1)
%       structured cell array with the anonymous functions for each
%       species. Each cell-array position contains N_coef anonymous
%       functions:
%           .A1, .A2, etc
%
% Outputs:
%   h                   [J/kg]              (N_eta x 1)
%       mixture enthalpy
%   cp                  [J/kg-K]            (N_eta x 1)
%       mixture heat capacity at constant pressure
%   cv                  [J/kg-K]            (N_eta x 1)
%       mixture heat capacity at constant volume
%   h_s                 [J/kg]              (N_eta x N_spec)
%       species enthalpy
%   cp_s                [J/kg-K]            (N_eta x N_spec)
%       species heat capacity at constant pressure
%   cv_s                [J/kg-K]            (N_eta x N_spec)
%       species heat capacity at constant volume
%   hMod
%       structure with the contribution of each energy mode to the species
%       enthalpy
%   cpMod
%       structure with the contribution of each energy mode to the species
%       heat capacity at constant pressure
%   cvMod
%       structure with the contribution of each energy mode to the species
%       heat capacity at constant volume
%   hv                  [J/kg]              (N_eta x 1)
%       mixture vibrational-electronic-electron enthalpy
%   cptr                [J/kg-K]            (N_eta x 1)
%       mixture translational-rotational heat capacity at constant pressure
%   cpv                 [J/kg-K]            (N_eta x 1)
%       mixture vibrational-electronic-electron enthalpy heat capacity at
%       constant pressure 
%   htr_s               [J/kg]              (N_eta x N_spec)
%       species translational-rotational enthalpy
%   hv_s                [J/kg]              (N_eta x N_spec)
%       species vibrational-electronic-electron enthalpy
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% obtaining number of species
N_spec = size(y_s,2);

% obtaining mixture enthalpy
if nargout<=4 % we don't want the decompostiion of the energies into the different modes
[h_s,cp_s,cv_s] = getSpecies_h_cp(TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options,varargin{:});
else % we do
[h_s,cp_s,cv_s,hMod_s,cpMod_s,cvMod_s] = getSpecies_h_cp(TTrans,TRot,TVib,TElec,Tel,R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options,varargin{:});
end

cp = (y_s.*cp_s) * ones(N_spec,1); % this product is the same as summing over all the species
cv = (y_s.*cv_s) * ones(N_spec,1);
h = (y_s.*h_s) * ones(N_spec,1);

% additional, only when the user wants also the h, cp and cv of the
% different energy modes
if nargout>4
    varargout{1} = hMod_s;
    varargout{2} = cpMod_s;
    varargout{3} = cvMod_s;
    hv = (y_s.*(hMod_s.hVib_s + hMod_s.hElec_s + hMod_s.hTrans_s.*bElectron)) * ones(N_spec,1);
    cptr = (y_s.*(cpMod_s.cpTrans_s.*(~bElectron) + cpMod_s.cpRot_s)) * ones(N_spec,1);
    cpv = (y_s.*(cpMod_s.cpVib_s + cpMod_s.cpElec_s + cpMod_s.cpTrans_s.*bElectron)) * ones(N_spec,1);
    htr_s = hMod_s.hTrans_s.*(~bElectron) + hMod_s.hRot_s + hForm_s; % IMPORTANT!! all formation enthalpies (also those from electrons) go into the trans-rot energy
    hv_s = hMod_s.hVib_s + hMod_s.hElec_s + hMod_s.hTrans_s.*bElectron;
    % note: we add the translational energy, but multiplied by the electron
    % boolean, so that effectively we are only adding the electron
    % translational energy
    varargout{4} = hv;      % mixture vibrational-electronic-electron enthalpy
    varargout{5} = cptr;    % mixture translational-rotational heat capacity at constant pressure
    varargout{6} = cpv;     % mixture vibrational-electronic-electron heat capacity at constant pressure
    varargout{7} = htr_s;   % species translational-rotational enthalpy
    varargout{8} = hv_s;    % species vibrational-electronic-electron enthalpy
end
