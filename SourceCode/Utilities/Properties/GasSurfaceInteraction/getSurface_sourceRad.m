function sourceRad = getSurface_sourceRad(sigmaStefBoltz,epsilonRadSurf,TRad)
% getSurface_sourceRad computes the radiative source term due to the
% radiation at the surface.
%
% Usage:
%   (1)
%       sourceRad = getSurface_sourceRad(sigmaStefBoltz,epsilonRadSurf,TRad)
%
% Inputs and outputs:
%   sigmaStefBoltz
%       Stefan-Boltzmann constant (size 1 x 1)
%   epsilonRadSurf
%       surface black-body coefficient (size 1 x 1)
%   TRad
%       temperature driving the surface radiation (size Nw x 1)
%   sourceRad
%       radiative source term (size Nw x 1)
%
% Author(s): Fernando Miro Miro
% Date: November 2018
% GNU Lesser General Public License 3.0

sourceRad = sigmaStefBoltz * epsilonRadSurf * TRad.^4;

end % getSurface_sourceRad