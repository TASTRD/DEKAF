function dsourceRad_dT = getSurfaceDer_dsourceRad_dT(sigmaStefBoltz,epsilonRadSurf,TRad,options)
% getSurfaceDer_dsourceRad_dT computes the temperature derivative of the
% radiative source term due to the radiation at the surface.
%
% Usage:
%   (1)
%       dsourceRad_dT = getSurfaceDer_dsourceRad_dT(sigmaStefBoltz,epsilonRadSurf,TRad,options)
%
% Inputs and outputs:
%   sigmaStefBoltz
%       Stefan-Boltzmann constant (size 1 x 1)
%   epsilonRadSurf
%       surface black-body coefficient (size 1 x 1)
%   TRad
%       temperature driving the surface radiation (size Nw x 1)
%   options
%       classic DEKAF options structure
%   dsourceRad_dT
%       temperature derivative of the radiative source term (size Nw x 1)
%
% Author(s): Fernando Miro Miro
% Date: November 2018
% GNU Lesser General Public License 3.0

dsourceRad_dT = 4 * sigmaStefBoltz * epsilonRadSurf * TRad.^3;

switch options.numberOfTemp
    case '1T'
        % nothing more is needed
    case '2T'
        dsourceRad_dT = [dsourceRad_dT , zeros(size(dsourceRad_dT))];
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

end % getSurface_sourceRad