function dsourcesubls_dym = getSurfaceSpeciesDer_dsourcesubls_dym(TTransw,Telw,rhow,drho_dym,y_sw,R_s,bElectron,alfaSubl_s,sublSpec_bool,options)
% getSurfaceSpecies_sourceSubl computes the species source terms due to
% sublimation reactions.
%
% Usage:
%   (1)
%       dsourcesubls_dym = getSurfaceSpeciesDer_dsourcesubls_dym(TTransw,Telw,rhow,drho_dym,...
%                               y_sw,R_s,bElectron,pAtm,alfaSubl_s,sublSpec_bool,options)
%
% Inputs and outputs:
%   TTransw             wall translational temperature (Nw x 1)
%   Telw                wall electron temperature (Nw x 1)
%   rhow                wall mixture density (Nw x 1)
%   drho_dym            mass-fraction derivatives of the wall mixture density (Nw x N_spec)
%   y_sw                wall mass fraction of the sublimating species (Nw x N_spec)
%   R_s                 gas constant of the sublimating species (N_spec x 1)
%   bElectron           electron boolean of the sublimating species (N_spec x 1)
%   alfaSubl_s          sublimation probabilities (N_spec x 1)
%   sublSpec_bool       vector of booleans containing which species are sublimating (N_spec x 1)
%   dsourcesubls_dym    mass-fraction derivativees of the source term of
%                       the sublimation species (Nw x N_spec(m) x N_sublSpec(s))
%
% Author(s): Fernando Miro Miro
% Date: December 2018
% GNU Lesser General Public License 3.0

dps_dym = getSpeciesDer_dps_dym(rhow,R_s,TTransw,y_sw,drho_dym,options.numberOfTemp,bElectron,Telw);

% Obtaining sizes
[Nw,N_spec] = size(y_sw);
N_sublSpec = nnz(sublSpec_bool);

% Computing source term derivative
alfaSubl_s  = repmat(permute(alfaSubl_s(sublSpec_bool),[2,3,1]),  [Nw,N_spec,1]);          % (s,1) --> (w,m,s)
R_s         = repmat(permute(R_s(sublSpec_bool),[2,3,1]),         [Nw,N_spec,1]);          % (s,1) --> (w,m,s)
TTransw_3D  = repmat(TTransw,                                      [1,N_spec,N_sublSpec]); % (w,s) --> (w,m,s)
dps_dym = dps_dym(:,:,sublSpec_bool);                   % keeping only the derivatives of those species that sublimate

dsourcesubls_dym = - alfaSubl_s .* dps_dym ./sqrt(2*pi*R_s .*TTransw_3D);

end % getSurfaceSpeciesDer_dsourcesubls_dT