function dsourceGSs_dT = getSurfaceSpeciesDer_dsourceGSs_dT(TTransw,Telw,p,y_sw,R0,Mm_s,bElectron,pAtm,gasSurfReac_struct,options)
% getSurfaceSpeciesDer_dsourceGSs_dT computes the temperature derivative of
% the species source terms due to all gas-surface interaction reactions.
%
% IMPORTANT!! This assumes all surface reactions to be one-directional. The
% backward reactions are neglected.
%
% It computes reactions specified through a stoichiometric matrix, and
% those due to sublimation.
%
% Usage:
%   (1)
%       dsourceGSs_dT = getSurfaceSpeciesDer_dsourceGSs_dT(TTransw,Telw,p,...
%                   y_sw,R0,Mm_s,bElectron,pAtm,gasSurfReac_struct,options)
%
% Inputs and outputs:
%   TTransw             wall translational temperature (Nw x 1)
%   Telw                wall electron temperature (Nw x 1)
%   p                   mixture pressure (Nw x 1)
%   y_sw                wall species mass fraction (Nw x N_spec)
%   R0                  universal gas constant (size 1 x 1)
%   Mm_s                species molar mass (size N_spec x 1)
%   bElectron           electron boolean of the sublimating species (N_spec x 1)
%   pAtm                atmospheric pressure (1 x 1)
%   gasSurfReac_struct  structure containing those variables needed for the
%                       gas-surface interaction modeling. It should contain:
%           nuGS_reac
%               stoichiometric coefficients of the reactants in gas-surface
%               reactions (size N_spec x N_GSreac)
%           nuGS_prod
%               stoichiometric coefficients of the products in gas-surface
%               reactions  (size N_spec x N_GSreac)
%           gasSurfReac_type
%               cell of strings containing the type of reaction
%               corresponding to each of the reactions detailed by
%               nuGS_reac and nuGS_prod. Types are detailed in
%               getSurface_source (size N_GSreac x 1)
%           A_GSr
%               vector with the reaction-rate preexponential constant for
%               Arrhenius-type surface reactions. It is zero for
%               non-Arrhenius reactions (size N_GSreac x 1)
%           nT_GSr
%               vector with the reaction-rate exponential constant for
%               Arrhenius-type surface reactions. It is zero for
%               non-Arrhenius reactions (size N_GSreac x 1)
%           qf_GSr
%               vector with the exponential coefficients to be used to
%               determine the reaction temperature for each of the
%               gas-surface interaction reactions (size N_GSreac x 1)
%           theta_GSr
%               vector with the reaction-rate activation temperatures for
%               Arrhenius-type surface reactions. It is zero for
%               non-Arrhenius reactions (size N_GSreac x 1)
%           sublSpec_bool
%               vector of booleans keeping track of whether each of the
%               species in the mixture will sublimate from the surface.
%           oxidPark76_idx
%               vector of positions corresponding to the oxidizing species
%               for each of the Park76 reactions. It is irrelevant for
%               non-Arrhenius reactions (size N_GSreac x 1)
%           alfaSubl_s
%               species sublimation probabilities (N_spec x 1)
%           PVapP_s
%               species vapor pressure P coefficient (N_spec x 1)
%           QVapP_s
%               species vapor pressure Q coefficient (N_spec x 1)
%   dsourceGSs_dT   derivative of the GS source terms wrt the various
%                   temperatures (size N_eta x N_spec x N_T)
%
% References:
%   [1] Park, C. (1976). Effects of Atomic Oxygen on Graphite Ablation.
%       AIAA Journal, 14(11), 1640–1642. https://doi.org/https://doi.org/10.2514/3.7267
%   [2] Mortensen, C. (2013). Effects of Thermochemical Nonequilibrium on
%       Hypersonic Boundary-Layer Instability in the Presence of Surface
%       Ablation or Isolated Two-Dimensional Roughness. UCLA.
%
% See also: getSurfaceReaction_kf_park76, getSurfaceSpecies_sourceSubl,
% getReaction_lnkf, getReactionDer_dsourcesr_dT,
% getSurfaceReactionDer_dlnkf_dT_park76,
% getSurfaceSpeciesDer_dsourcesubls_dT
%
% Author(s): Fernando Miro Miro
% Date: November 2018
% GNU Lesser General Public License 3.0

options.EoS = protectedvalue(options,'EoS','idealGas');

% Extracting properties from
alfaSubl_s          = gasSurfReac_struct.alfaSubl_s;            % sublimation properties
PVapP_s             = gasSurfReac_struct.PVapP_s;
QVapP_s             = gasSurfReac_struct.QVapP_s;
sublSpec_bool       = gasSurfReac_struct.sublSpec_bool;
nuGS_reac           = gasSurfReac_struct.nuGS_reac;             % other gas-surface reactions
nuGS_prod           = gasSurfReac_struct.nuGS_prod;
gasSurfReac_type    = gasSurfReac_struct.gasSurfReac_type;
A_GSr               = gasSurfReac_struct.A_GSr;
nT_GSr              = gasSurfReac_struct.nT_GSr;
qf_GSr              = gasSurfReac_struct.qf_GSr;
theta_GSr           = gasSurfReac_struct.theta_GSr;
oxidPark76_idx      = gasSurfReac_struct.oxidPark76_idx;

% Obtaining sizes and reaction temperatures
[N_spec,N_GSreac] = size(nuGS_reac);
Nw = length(TTransw);
switch options.numberOfTemp
    case '1T'
        TGroup      = TTransw;
        Tf_GSr      = repmat(TTransw,[1,N_GSreac]);   % (eta,r)
        dTfGSr_dT   = ones(Nw,N_GSreac);              % (eta,r,iT)
    case '2T'
        TGroup      = [TTransw,Telw];
        Tf_GSr      = getReaction_T(qf_GSr,TTransw,Telw);           % (eta,r)
        dTfGSr_dT   = getReactionDer_dTrdT(qf_GSr,TTransw,Telw);    % (eta,r,iT)
    otherwise
        error(['the chosen thermal models ''',options.numberOfTemp,''' is not supported']);
end
N_T = size(TGroup,2);

% Classifying reactions
bIdx_Park76Opt1 = ismember(gasSurfReac_type,'Park76_opt1');                 % reactions corresponding to Park's first option (eq. 2.44 & 2.45 in Ref. [2])
bIdx_Park76Opt2 = ismember(gasSurfReac_type,'Park76_opt2');                 % reactions corresponding to Park's second option (eq. 2.44 & 2.46 in Ref. [2])
bIdx_Arrhenius  = ismember(gasSurfReac_type,'Arrhenius');                   % reactions following a classic Arrhenius law

% Computing the different reaction rates
lnkb_GSr    = -Inf*ones(Nw,N_GSreac);                                       % NEGLECTING backward reactions [ln(0) = -Inf]
dlnkbGSr_dT =    0*ones(Nw,N_GSreac,N_T);
lnkf_GSr    = -Inf*ones(Nw,N_GSreac);                                       % allocating
dlnkfGSr_dT = -Inf*ones(Nw,N_GSreac,N_T);
if nnz(bIdx_Park76Opt1) > 0                                                 % if there are some reactions following Park's first option
    Mm_oxid1    = Mm_s(oxidPark76_idx(bIdx_Park76Opt1));                       % molar mass of the oxidizer for each reaction
    Tf_r        = Tf_GSr(:,bIdx_Park76Opt1);
    dTfr_dT     = dTfGSr_dT(:,bIdx_Park76Opt1,:);
    lnkf_GSr(:,bIdx_Park76Opt1)      = getSurfaceReaction_kf_park76(Tf_r,R0,Mm_oxid1,'opt1'); % we compute the reaction rates
    dlnkfGSr_dTr                     = getSurfaceReactionDer_dlnkf_dT_park76(Tf_r,'opt1');
    dlnkfGSr_dT(:,bIdx_Park76Opt1,:) = repmat(dlnkfGSr_dTr,[1,1,N_T]).*dTfr_dT;           % chain rule
end
if nnz(bIdx_Park76Opt2) > 0                                                 % if there are some reactions following Park's second option
    Mm_oxid2    = Mm_s(oxidPark76_idx(bIdx_Park76Opt2));                       % molar mass of the oxidizer for each reaction
    Tf_r        = Tf_GSr(:,bIdx_Park76Opt2);
    dTfr_dT     = dTfGSr_dT(:,bIdx_Park76Opt2,:);
    lnkf_GSr(:,bIdx_Park76Opt2)      = getSurfaceReaction_kf_park76(Tf_r,R0,Mm_oxid2,'opt2'); % we compute the reaction rates
    dlnkfGSr_dTr                     = getSurfaceReactionDer_dlnkf_dT_park76(Tf_r,'opt2');
    dlnkfGSr_dT(:,bIdx_Park76Opt2,:) = repmat(dlnkfGSr_dTr,[1,1,N_T]).*dTfr_dT;           % chain rule
end
if nnz(bIdx_Arrhenius) > 0
    Tf_r        = Tf_GSr(:,bIdx_Arrhenius);
    dTfr_dT     = dTfGSr_dT(:,bIdx_Arrhenius,:);
    lnkf_GSr(:,bIdx_Arrhenius)      = getReaction_lnkf(Tf_r,A_GSr,nT_GSr,theta_GSr); % we compute the reaction rates following a classic Arrhenius expression
    dlnkfGSr_dTfr                   = getReactionDer_dlnkfr_dT(Tf_r,nT_GSr,theta_GSr);
    dlnkfGSr_dT(:,bIdx_Arrhenius,:) = repmat(dlnkfGSr_dTfr,[1,1,N_T]).*dTfr_dT;           % chain rule
end

% Computing species source term due to every reaction
rho = getMixture_rho(y_sw,p,TTransw,Telw,R0,Mm_s,bElectron);
rho_mat = repmat(rho,[1,N_spec]);
rho_l = y_sw.*rho_mat;                                                   % (eta,l)
drho_dT = getMixtureDer_drho_dT(p,y_sw,R0,Mm_s,bElectron,TGroup,options.numberOfTemp);    % (eta,iT)
drhol_dT = getSpeciesDer_drhol_dT(drho_dT,y_sw);                             % (eta,l,iT)
dsourceGSsr_dT = getReactionDer_dsourcesr_dT(nuGS_reac,nuGS_prod,rho_l,drhol_dT,Mm_s,lnkf_GSr,lnkb_GSr,dlnkfGSr_dT,dlnkbGSr_dT);
% size Nw x N_spec x N_T x N_GSreac

% summing over the source terms of every reaction to obtain the total species source term
dsourceGSs_dT = zeros(Nw,N_spec,N_T);
for r = 1:N_GSreac
    dsourceGSs_dT = dsourceGSs_dT + dsourceGSsr_dT(:,:,:,r);
end

% computing sublimation source terms
dsourcesubls_dT = zeros(Nw,N_spec,N_T); % allocating
R_s = R0./Mm_s;

dsourcesubls_dT(:,sublSpec_bool,:) = getSurfaceSpeciesDer_dsourcesubls_dT(TTransw,Telw,rho,drho_dT,y_sw(:,sublSpec_bool),R_s(sublSpec_bool),...
                                                    bElectron(sublSpec_bool),pAtm,PVapP_s(sublSpec_bool),...
                                                    QVapP_s(sublSpec_bool),alfaSubl_s(sublSpec_bool),options);

% assembling source terms due to both reactions
dsourceGSs_dT = dsourceGSs_dT + dsourcesubls_dT;

end % getSurfaceSpeciesDer_dsourceGSs_dT