function dsourceGSs_dym = getSurfaceSpeciesDer_dsourceGSs_dym(TTransw,Telw,p,y_sw,R0,Mm_s,bElectron,gasSurfReac_struct,options)
% getSurfaceSpeciesDer_dsourceGSs_dym computes the derivative wrt the mass
% fractions of the species source terms due to all gas-surface interaction
% reactions.
%
% IMPORTANT!! This assumes all surface reactions to be one-directional. The
% backward reactions are neglected.
%
% It computes reactions specified through a stoichiometric matrix, and
% those due to sublimation.
%
% Usage:
%   (1)
%       dsourceGSs_dym = getSurfaceSpeciesDer_dsourceGSs_dym(TTransw,Telw,p,...
%                   y_sw,R0,Mm_s,bElectron,gasSurfReac_struct,options)
%
% Inputs and outputs:
%   TTransw             wall translational temperature (Nw x 1)
%   Telw                wall electron temperature (Nw x 1)
%   p                   mixture pressure (Nw x 1)
%   y_sw                wall species mass fraction (Nw x N_spec)
%   R0                  universal gas constant (size 1 x 1)
%   Mm_s                species molar mass (size N_spec x 1)
%   bElectron           electron boolean of the sublimating species (N_spec x 1)
%   gasSurfReac_struct  structure containing those variables needed for the
%                       gas-surface interaction modeling. It should contain:
%           nuGS_reac
%               stoichiometric coefficients of the reactants in gas-surface
%               reactions (size N_spec x N_GSreac)
%           nuGS_prod
%               stoichiometric coefficients of the products in gas-surface
%               reactions  (size N_spec x N_GSreac)
%           gasSurfReac_type
%               cell of strings containing the type of reaction
%               corresponding to each of the reactions detailed by
%               nuGS_reac and nuGS_prod. Types are detailed in
%               getSurface_source (size N_GSreac x 1)
%           A_GSr
%               vector with the reaction-rate preexponential constant for
%               Arrhenius-type surface reactions. It is zero for
%               non-Arrhenius reactions (size N_GSreac x 1)
%           nT_GSr
%               vector with the reaction-rate exponential constant for
%               Arrhenius-type surface reactions. It is zero for
%               non-Arrhenius reactions (size N_GSreac x 1)
%           qf_GSr
%               vector with the exponential coefficients to be used to
%               determine the reaction temperature for each of the
%               gas-surface interaction reactions (size N_GSreac x 1)
%           theta_GSr
%               vector with the reaction-rate activation temperatures for
%               Arrhenius-type surface reactions. It is zero for
%               non-Arrhenius reactions (size N_GSreac x 1)
%           sublSpec_bool
%               vector of booleans keeping track of whether each of the
%               species in the mixture will sublimate from the surface.
%           oxidPark76_idx
%               vector of positions corresponding to the oxidizing species
%               for each of the Park76 reactions. It is irrelevant for
%               non-Arrhenius reactions (size N_GSreac x 1)
%           alfaSubl_s
%               species sublimation probabilities (N_spec x 1)
%   dsourceGSs_dym  derivative of the GS source terms wrt the various
%                   mass fractions (size N_eta x N_spec(m) x N_spec(s))
%
% References:
%   [1] Park, C. (1976). Effects of Atomic Oxygen on Graphite Ablation.
%       AIAA Journal, 14(11), 1640–1642. https://doi.org/https://doi.org/10.2514/3.7267
%   [2] Mortensen, C. (2013). Effects of Thermochemical Nonequilibrium on
%       Hypersonic Boundary-Layer Instability in the Presence of Surface
%       Ablation or Isolated Two-Dimensional Roughness. UCLA.
%
% See also: getSurfaceReaction_kf_park76, getSurfaceSpecies_sourceSubl,
% getReaction_lnkf,
%
% Author(s): Fernando Miro Miro
% Date: November 2018
% GNU Lesser General Public License 3.0

options.EoS = protectedvalue(options,'EoS','idealGas');

% Extracting properties from
alfaSubl_s          = gasSurfReac_struct.alfaSubl_s;            % sublimation properties
sublSpec_bool       = gasSurfReac_struct.sublSpec_bool;
nuGS_reac           = gasSurfReac_struct.nuGS_reac;             % other gas-surface reactions
nuGS_prod           = gasSurfReac_struct.nuGS_prod;
gasSurfReac_type    = gasSurfReac_struct.gasSurfReac_type;
A_GSr               = gasSurfReac_struct.A_GSr;
nT_GSr              = gasSurfReac_struct.nT_GSr;
qf_GSr              = gasSurfReac_struct.qf_GSr;
theta_GSr           = gasSurfReac_struct.theta_GSr;
oxidPark76_idx      = gasSurfReac_struct.oxidPark76_idx;

% Obtaining sizes and reaction temperatures
[N_spec,N_GSreac] = size(nuGS_reac);
Nw = length(TTransw);
switch options.numberOfTemp
    case '1T'
        TGroup      = TTransw;
        Tf_GSr      = repmat(TTransw,[1,N_GSreac]);   % (eta,r)
    case '2T'
        TGroup      = [TTransw,Telw];
        Tf_GSr      = getReaction_T(qf_GSr,TTransw,Telw);           % (eta,r)
    otherwise
        error(['the chosen thermal models ''',numberOfTemp,''' is not supported']);
end
N_T = size(TGroup,2);

% Classifying reactions
bIdx_Park76Opt1 = ismember(gasSurfReac_type,'Park76_opt1');                 % reactions corresponding to Park's first option (eq. 2.44 & 2.45 in Ref. [2])
bIdx_Park76Opt2 = ismember(gasSurfReac_type,'Park76_opt2');                 % reactions corresponding to Park's second option (eq. 2.44 & 2.46 in Ref. [2])
bIdx_Arrhenius  = ismember(gasSurfReac_type,'Arrhenius');                   % reactions following a classic Arrhenius law

% Computing the different reaction rates
lnkb_GSr    = -Inf*ones(Nw,N_GSreac);                                       % NEGLECTING backward reactions [ln(0) = -Inf]
lnkf_GSr    = -Inf*ones(Nw,N_GSreac);                                       % allocating
if nnz(bIdx_Park76Opt1) > 0                                                 % if there are some reactions following Park's first option
    Mm_oxid1    = Mm_s(oxidPark76_idx(bIdx_Park76Opt1));                       % molar mass of the oxidizer for each reaction
    Tf_r        = Tf_GSr(:,bIdx_Park76Opt1);
    lnkf_GSr(:,bIdx_Park76Opt1)      = getSurfaceReaction_kf_park76(Tf_r,R0,Mm_oxid1,'opt1'); % we compute the reaction rates
end
if nnz(bIdx_Park76Opt2) > 0                                                 % if there are some reactions following Park's second option
    Mm_oxid2    = Mm_s(oxidPark76_idx(bIdx_Park76Opt2));                       % molar mass of the oxidizer for each reaction
    Tf_r        = Tf_GSr(:,bIdx_Park76Opt2);
    lnkf_GSr(:,bIdx_Park76Opt2)      = getSurfaceReaction_kf_park76(Tf_r,R0,Mm_oxid2,'opt2'); % we compute the reaction rates
end
if nnz(bIdx_Arrhenius) > 0
    Tf_r        = Tf_GSr(:,bIdx_Arrhenius);
    lnkf_GSr(:,bIdx_Arrhenius)      = getReaction_lnkf(Tf_r,A_GSr,nT_GSr,theta_GSr); % we compute the reaction rates following a classic Arrhenius expression
end

% Computing species source term due to every reaction
rho = getMixture_rho(y_sw,p,TTransw,Telw,R0,Mm_s,bElectron);
rho_mat = repmat(rho,[1,N_spec]);
rho_l = y_sw.*rho_mat;                                                      % (eta,l)
drho_dym = getMixtureDer_drho_dym(p,y_sw,R0,Mm_s,bElectron,TTransw,Telw);   % (eta,m)
drhol_dym = getSpeciesDer_drhol_dym(rho,drho_dym,y_sw);                     % (eta,m,l)
dsourceGSsr_dym = getReactionDer_dsourcesr_dym(nuGS_reac,nuGS_prod,Mm_s,rho_l,drhol_dym,lnkf_GSr,lnkb_GSr);
% size Nw x N_spec x N_T x N_GSreac

% summing over the source terms of every reaction to obtain the total species source term
dsourceGSs_dym = zeros(Nw,N_spec,N_spec);
for r = 1:N_GSreac
    dsourceGSs_dym = dsourceGSs_dym + dsourceGSsr_dym(:,:,:,r);
end

% computing sublimation source terms
dsourcesubls_dym = zeros(Nw,N_spec,N_spec); % allocating
R_s = R0./Mm_s;

dsourcesubls_dym(:,:,sublSpec_bool) = getSurfaceSpeciesDer_dsourcesubls_dym(TTransw,Telw,rho,drho_dym,y_sw,R_s,bElectron,alfaSubl_s,sublSpec_bool,options);

% assembling source terms due to both reactions
dsourceGSs_dym = dsourceGSs_dym + dsourcesubls_dym;

end % getSurfaceSpeciesDer_dsourceGSs_dym