function dsourceGSs_dT = getSurfaceDer_catalytic_dsourceGS_dT(p,y_s,nuCat_prod,nuCat_reac,Mm_s,bElectron,TTrans,Tel,R0,numberOfTemp,wallYs_BC,varargin)
% getSurfaceDer_catalytic_dsourceGS_dT computes the temperature derivatives
% of the species source term at the wall due to catalytic recombination
% reactions.
%
% Usage:
%   (1)
%       dsourceGSs_dT = getSurfaceDer_catalytic_dsourceGS_dT(p,y_s,nuCat_prod,nuCat_reac,Mm_s,...
%                                           bElectron,TTrans,Tel,R0,numberOfTemp,'catalyticK',K_r)
%       |--> usage when providing the catalytic-recombination velocities K_r
%
%   (2)
%       dsourceGSs_dT = getSurfaceDer_catalytic_dsourceGS_dT(p,y_s,nuCat_prod,nuCat_reac,Mm_s,...
%                                           bElectron,TTrans,Tel,R0,numberOfTemp,'catalyticGamma',gammaCat_r)
%       |--> usage when providing the catalytic-recombination probabilities gammaCat_r
%
% Inputs and outputs:
%   p                           [Pa]
%       pressure (size N_eta x 1)
%   y_r                         [-]
%       species mass fraction (size N_eta x N_spec)
%   nuCat_prod, nuCat_reac      [-]
%       stoichiometric coefficients of products and reactants in catalytic
%       recombination reactions (size N_spec x N_reac)
%   Mm_s                        [kg/mol]
%       species molar mass (size N_spec x 1)
%   bElectron                   [-]
%       electron boolean (size N_spec x 1)
%   TTrans                      [K]
%       translational temperature of heavy particles (size N_eta x 1)
%   Tel                         [K]
%       translational temperature of electrons (size N_eta x 1)
%   R0                          [J/kg-mol]
%       ideal gas constant (size 1 x 1)
%   numberOfTemp
%       string identifying the number of temperatures used to describe the
%       thermal state of the system
%   K_r                         [m/s]
%       catalytic recombination velocity of each reaction (size N_reac x 1)
%   gammaCat_r                  [-]
%       catalytic recombination probability of each reaction (size N_reac x 1)
%   dsourceGSs_dT               [kg/s-m^2-K]
%       temperature derivative of the species production rate per unit
%       surface due to catalytic recombination reactions
%       (size N_eta x N_spec x N_T)
%
% Author: Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

[N_spec,N_reac] = size(nuCat_prod);
N_eta           = length(TTrans);

% Computing auxiliary quantities
Mm_s3D = repmat(Mm_s.',[N_eta,1,N_reac]);                       % (s,1) ----> (eta,s,r)
Mm_s2D = repmat(Mm_s.',[N_reac,1]);                             % (s,1) ----> (r,s)
MmReac_r = (nuCat_reac.' .* Mm_s2D)*ones(N_spec,1);             % (r,1)

switch wallYs_BC
    case 'catalyticGamma'
        gammaCat_r      = varargin{1}(:);                                       % [-] (r,1)
        gammaCat_r3D    = repmat(permute(gammaCat_r,[2,3,1]),[N_eta,N_spec,1]);     % (r,1) ----> (eta,s,r)
        TTrans3D        = repmat(        TTrans,             [1,N_spec,N_reac]);    % (eta,1) --> (eta,s,r)
        K_sr            = gammaCat_r3D.*sqrt(R0.*TTrans3D./(2*pi*Mm_s3D));          % (eta,s,r)
        dKsr_dT         = gammaCat_r3D.*sqrt(R0./(8*pi*Mm_s3D.*TTrans3D));          % (eta,s,r)
    case 'catalyticK'
        K_sr            = repmat(permute(varargin{1}(:),[2,3,1]),[N_eta,N_spec,1]); % [m/s] (r,1) ----> (eta,s,r)
        dKsr_dT         = zeros(N_eta,N_spec,N_reac);                               % (eta,s,r)
    otherwise
        error(['Error: the chosen wallYs_BC ''',wallYs_BC,''' is not supported']);
end

switch numberOfTemp
    case '1T'
        drho_dT = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,TTrans);            % (eta,T)
        N_T = 1;
    case '2T'
        drho_dT = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,[TTrans,Tel],'2T'); % (eta,T)
        N_T = 2;
        dKsr_dT = cat(4,dKsr_dT,zeros(N_eta,N_spec,N_reac));                        % (eta,s,r) --> (eta,s,r,T)
    otherwise
        error(['Error: the chosen numberOfTemp ''',numberOfTemp,''' is not supported']);
end
rho         = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);                   % (eta,1)
drho_dT3D   = repmat(permute(drho_dT,[1,3,2]),[1,N_spec,1]);                        % (eta,T) --> (eta,s,T)
rho2D       = repmat(rho,[1,N_spec]);                                               % (eta,1) --> (eta,s)
nuCat_reac3D= repmat(permute(nuCat_reac,[3,1,2]),[N_eta,1,1]);                      % (s,r) ----> (eta,s,r)

% Computing source term due to each catalytic reaction
for r=N_reac:-1:1                                                           % looping reactions
    for iT=N_T:-1:1                                                             % looping temperatures
        dmCatr_dT(:,r,iT)  = (nuCat_reac3D(:,:,r).*Mm_s3D(:,:,r)./MmReac_r(r) .* y_s .* (drho_dT3D(:,:,iT).*K_sr(:,:,r) + rho2D.*dKsr_dT(:,:,r,iT)))*ones(N_spec,1); % (eta,r,T)
    end
end

% Computing species source term
delta_nuCat   = nuCat_prod - nuCat_reac;                                            % (s,r)
delta_nuCat3D = repmat(permute(delta_nuCat,[3,2,1]),[N_eta,1,1]);                   % (s,r) ----> (eta,r,s)
MmReac_r2D    = repmat(        MmReac_r.',          [N_eta,1]);                     % (r,1) ----> (eta,r)
for s=N_spec:-1:1                                                           % looping species
    for iT=N_T:-1:1                                                             % looping temperatures
        dsourceGSs_dT(:,s,iT) = (dmCatr_dT(:,:,iT).*delta_nuCat3D(:,:,s).*Mm_s(s)./MmReac_r2D)*ones(N_reac,1);    % (eta,s,T)
    end
end

end % getSurfaceDer_catalytic_dsourceGS_dT