function sourceGS_s = getSurface_catalytic_sourceGS(p,y_s,nuCat_prod,nuCat_reac,Mm_s,bElectron,TTrans,Tel,R0,wallYs_BC,varargin)
% getSurface_catalytic_sourceGS computes the species source term at the
% wall due to catalytic recombination reactions.
%
% Usage:
%   (1)
%       sourceGS_s = getSurface_catalytic_sourceGS(p,y_s,nuCat_prod,nuCat_reac,Mm_s,...
%                                       bElectron,TTrans,Tel,R0,'catalyticK',K_r)
%       |--> usage when providing the catalytic-recombination velocities K_r
%
%   (2)
%       sourceGS_s = getSurface_catalytic_sourceGS(p,y_s,nuCat_prod,nuCat_reac,Mm_s,...
%                                       bElectron,TTrans,Tel,R0,'catalyticGamma',gammaCat_r)
%       |--> usage when providing the catalytic-recombination probabilities gammaCat_r
%
% Inputs and outputs:
%   p                           [Pa]
%       pressure (size N_eta x 1)
%   y_r                         [-]
%       species mass fraction (size N_eta x N_spec)
%   nuCat_prod, nuCat_reac      [-]
%       stoichiometric coefficients of products and reactants in catalytic
%       recombination reactions (size N_spec x N_reac)
%   Mm_s                        [kg/mol]
%       species molar mass (size N_spec x 1)
%   bElectron                   [-]
%       electron boolean (size N_spec x 1)
%   TTrans                      [K]
%       translational temperature of heavy particles (size N_eta x 1)
%   Tel                         [K]
%       translational temperature of electrons (size N_eta x 1)
%   R0                          [J/kg-mol]
%       ideal gas constant (size 1 x 1)
%   K_r                         [m/s]
%       catalytic recombination velocity of each reaction (size N_reac x 1)
%   gammaCat_r                  [-]
%       catalytic recombination probability of each reaction (size N_reac x 1)
%   sourceGS_s                  [kg/s-m^2]
%       species production rate per unit surface due to catalytic
%       recombination reactions (size N_eta x N_spec)
%
% Author: Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

[N_spec,N_reac] = size(nuCat_prod);
N_eta           = length(TTrans);


Mm_s3D = repmat(Mm_s.',[N_eta,1,N_reac]);                       % (s,1) ----> (eta,s,r)
Mm_s2D = repmat(Mm_s.',[N_reac,1]);                             % (s,1) ----> (r,s)
MmReac_r = (nuCat_reac.' .* Mm_s2D)*ones(N_spec,1);             % (r,1)

switch wallYs_BC
    case 'catalyticGamma'
        gammaCat_r      = varargin{1}(:);                                       % [-] (r,1)
        gammaCat_r3D    = repmat(permute(gammaCat_r,[2,3,1]),[N_eta,N_spec,1]);     % (r,1) ----> (eta,s,r)
        TTrans3D        = repmat(        TTrans,             [1,N_spec,N_reac]);    % (eta,1) --> (eta,s,r)
        K_sr            = gammaCat_r3D.*sqrt(R0.*TTrans3D./(2*pi*Mm_s3D));          % (eta,s,r)
    case 'catalyticK'
        K_sr            = repmat(permute(varargin{1}(:),[2,3,1]),[N_eta,N_spec,1]); % [m/s] (r,1) ----> (eta,s,r)
    otherwise
        error(['Error: the chosen wallYs_BC ''',wallYs_BC,''' is not supported']);
end

% Computing source term due to each catalytic reaction
nuCat_reac3D    = repmat(permute(nuCat_reac,[3,1,2]),[N_eta,1,1]);                  % (s,r) ----> (eta,s,r)
rho = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);
for r=N_reac:-1:1
    mCat_r(:,r)  = rho.*(K_sr(:,:,r).*nuCat_reac3D(:,:,r).*Mm_s3D(:,:,r)./MmReac_r(r) .* y_s)*ones(N_spec,1); % (eta,r)
end

% Computing species source term
delta_nuCat   = nuCat_prod - nuCat_reac;                                % (s,r)
delta_nuCat3D = repmat(permute(delta_nuCat,[3,2,1]),[N_eta,1,1]);       % (s,r) ----> (eta,r,s)
MmReac_r2D    = repmat(        MmReac_r.',          [N_eta,1]);         % (r,1) ----> (eta,r)
for s=N_spec:-1:1                                                   % looping species
    sourceGS_s(:,s) = (mCat_r.*delta_nuCat3D(:,:,s).*Mm_s(s)./MmReac_r2D)*ones(N_reac,1);    % (eta,s)
end

end % getSurface_catalytic_sourceGS