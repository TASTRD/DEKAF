function dsourcesubls_dT = getSurfaceSpeciesDer_dsourcesubls_dT(TTransw,Telw,rhow,drho_dT,y_sw,R_s,bElectron,pAtm,PVapP_s,QVapP_s,alfaSubl_s,options)
% getSurfaceSpecies_sourceSubl computes the species source terms due to
% sublimation reactions.
%
% Usage:
%   (1)
%       dsourcesubls_dT = getSurfaceSpeciesDer_dsourcesubls_dT(TTransw,Telw,rhow,drho_dT,...
%                               y_sw,R_s,bElectron,pAtm,PVapP_s,QVapP_s,alfaSubl_s,options)
%
% Inputs and outputs:
%   TTransw             wall translational temperature (Nw x 1)
%   Telw                wall electron temperature (Nw x 1)
%   rhow                wall mixture density (Nw x 1)
%   drho_dT             temperature derivatives of the wall mixture density (Nw x N_T)
%   y_sw                wall mass fraction of the sublimating species (Nw x N_sublSpec)
%   R_s                 gas constant of the sublimating species (N_sublSpec x 1)
%   bElectron           electron boolean of the sublimating species (N_sublSpec x 1)
%   pAtm                atmospheric pressure (1 x 1)
%   PVapP_s             vapor pressure P coefficient (N_sublSpec x 1)
%   QVapP_s             vapor pressure Q coefficient (N_sublSpec x 1)
%   alfaSubl_s          sublimation probabilities (N_sublSpec x 1)
%   dsourcesubls_dT     source term of the sublimation species (Nw x N_sublSpec x N_T)
%
% Author(s): Fernando Miro Miro
% Date: December 2018
% GNU Lesser General Public License 3.0

% obtaining reaction temperatures
switch options.numberOfTemp
    case '1T'
        TGroup = TTransw;
    case '2T'
        TGroup = [TTransw,Telw];
    otherwise
        error(['the chosen thermal models ''',numberOfTemp,''' is not supported']);
end

p_s     = getSpecies_p(y_sw,TTransw,Telw,R_s,rhow,bElectron); % computing species partial pressures
dps_dT  = getSpeciesDer_dps_dT(rhow,R_s,TGroup,y_sw,drho_dT,options.numberOfTemp,bElectron);

% Computing species vapor pressure
[Nw,N_sublSpec] = size(y_sw);
N_T = size(TGroup,2);
PVapP_s     = repmat(PVapP_s.',    [Nw,1]);            % (s,1) --> (w,s)
QVapP_s     = repmat(QVapP_s.',    [Nw,1]);            % (s,1) --> (w,s)
TTransw_2D  = repmat(TTransw,      [1,N_sublSpec]);    % (w,1) --> (w,s)

pVap_s      =  pAtm                           * exp(PVapP_s./TTransw_2D + QVapP_s);
dpVaps_dT   = -pAtm * PVapP_s./TTransw_2D.^2 .* exp(PVapP_s./TTransw_2D + QVapP_s);
switch options.numberOfTemp
    case '1T'
        % nothing additional to do
    case '2T'
        dpVaps_dT(:,:,2) = zeros(Nw,N_sublSpec);
    otherwise
        error(['the chosen thermal models ''',numberOfTemp,''' is not supported']);
end

% Computing source term derivative
alfaSubl_s  = repmat(alfaSubl_s.', [Nw,1,N_T]);        % (s,1) --> (w,s,T)
R_s         = repmat(R_s.',        [Nw,1,N_T]);        % (s,1) --> (w,s,T)
TTransw_3D  = repmat(TTransw,      [1,N_sublSpec,N_T]);% (w,s) --> (w,s,T)

dsourcesubls_dT        =                          alfaSubl_s       .* (dpVaps_dT - dps_dT) ./sqrt(2*pi*R_s       .*TTransw_3D);
dsourcesubls_dT(:,:,1) = dsourcesubls_dT(:,:,1) - alfaSubl_s(:,:,1)/2 .* (pVap_s - p_s)    ./sqrt(2*pi*R_s(:,:,1).*TTransw_2D.^3);

end % getSurfaceSpeciesDer_dsourcesubls_dT