function dlnkfGSr_dT = getSurfaceReactionDer_dlnkf_dT_park76(Tw,optFlag)
% getSurfaceReactionDer_dlnkf_dT_park76 computes the temperature
% derivative of the forward reaction rates of surface reactions as
% proposed by Park.
%
% It is important to note that the derivative is done with respect to the
% reaction temperature. The chain rule must be then used to obtain it as a
% function of the actual translational, vibrational, or whatever
% temperatures.
%
% Usage:
%   (1)
%       dlnkfGSr_dT = getSurfaceReactionDer_dlnkf_dT_park76(Tw,optFlag)
%
% Inputs and outputs:
%   Tw              Wall temperatures (size Nw x N_GSreac)
%   optFlag         string identifying if we want to use the first option
%                   proposed by Park (Eq. 2 in Ref. [1] and Eq. 2.45 in
%                   Ref. [2]) or the second (Eq. 3 in Ref. [1] and Eq. 2.46
%                   in Ref. [2])
%   dlnkfGSr_dT     temperature derivative of the forward reaction rates of
%                   Gas-Surface interactions (size Nw x N_GSreac)
%
% References:
%   [1] Park, C. (1976). Effects of Atomic Oxygen on Graphite Ablation.
%       AIAA Journal, 14(11), 1640–1642. https://doi.org/https://doi.org/10.2514/3.7267
%   [2] Mortensen, C. (2013). Effects of Thermochemical Nonequilibrium on
%       Hypersonic Boundary-Layer Instability in the Presence of Surface
%       Ablation or Isolated Two-Dimensional Roughness. UCLA.
%
% Author(s): Fernando Miro Miro
% Date: November 2018
% GNU Lesser General Public License 3.0

% computing amplitude terms
switch optFlag
    case 'opt1'
        num = (1.43e-3 + 0.01*exp(-1450./Tw));                              % numerator
        den = (1 + 2e-4*exp(13000./Tw));                                    % denominator
        Alfa = num ./ den ;                                                 % Eq. 2 in Ref. [1] and Eq. 2.45 in Ref. [2]
        dAlfa_dT = 14.50./Tw.^2.*exp(-1450./Tw) ./ den + 2.6./Tw.^2 .* exp(13000./Tw) .* num./den.^2;
    case 'opt2'
        Alfa = 0.63 * exp(-1160./Tw);                                       % Eq. 3 in Ref. [1] and Eq. 2.46 in Ref. [2]
        dAlfa_dT = 0.63*1160./Tw.^2 .* exp(-1160./Tw);
    otherwise
        error(['the chosen option flag ''',optFlag,''' is not supported']);
end

% computing reaction rates
dlnkfGSr_dT = 1./Alfa .* dAlfa_dT + 1./(2*Tw);

end % getSurfaceReactionDer_dlnkf_dT_park76