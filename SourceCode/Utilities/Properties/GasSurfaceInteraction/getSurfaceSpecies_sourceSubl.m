function source_subls = getSurfaceSpecies_sourceSubl(TTransw,Telw,rhow,y_sw,R_s,bElectron,pAtm,PVapP_s,QVapP_s,alfaSubl_s)
% getSurfaceSpecies_sourceSubl computes the species source terms due to
% sublimation reactions.
%
% Usage:
%   (1)
%       source_subls = getSurfaceSpecies_sourceSubl(TTransw,Telw,rhow,y_sw,R_s,...
%                               bElectron,pAtm,PVapP_s,QVapP_s,alfaSubl_s)
%
% Inputs and outputs:
%   TTransw             wall translational temperature (Nw x 1)
%   Telw                wall electron temperature (Nw x 1)
%   rhow                wall mixture density (Nw x 1)
%   y_sw                wall mass fraction of the sublimating species (Nw x N_sublSpec)
%   R_s                 gas constant of the sublimating species (N_sublSpec x 1)
%   bElectron           electron boolean of the sublimating species (N_sublSpec x 1)
%   pAtm                atmospheric pressure (1 x 1)
%   PVapP_s             vapor pressure P coefficient (N_sublSpec x 1)
%   QVapP_s             vapor pressure Q coefficient (N_sublSpec x 1)
%   alfaSubl_s          sublimation probabilities (N_sublSpec x 1)
%   source_subls        source term of the sublimation species (Nw x N_sublSpec)
%
% Author(s): Fernando Miro Miro
% Date: November 2018
% GNU Lesser General Public License 3.0

p_s = getSpecies_p(y_sw,TTransw,Telw,R_s,rhow,bElectron); % computing species partial pressures

% resizing
[Nw,N_sublSpec] = size(y_sw);
PVapP_s     = repmat(PVapP_s.',    [Nw,1]);            % (s,1) --> (w,s)
QVapP_s     = repmat(QVapP_s.',    [Nw,1]);            % (s,1) --> (w,s)
alfaSubl_s  = repmat(alfaSubl_s.', [Nw,1]);            % (s,1) --> (w,s)
R_s         = repmat(R_s.',        [Nw,1]);            % (s,1) --> (w,s)
TTransw     = repmat(TTransw,      [1,N_sublSpec]);    % (w,1) --> (w,s)

% Computing species vapor pressure
pVap_s = pAtm * exp(PVapP_s./TTransw + QVapP_s);
source_subls = alfaSubl_s.*(pVap_s - p_s) .* sqrt(1./(2*pi*R_s.*TTransw));

end % getSurfaceSpecies_sourceSubl