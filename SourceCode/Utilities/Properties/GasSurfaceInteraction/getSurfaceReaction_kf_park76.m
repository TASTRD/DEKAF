function lnkf_GSr = getSurfaceReaction_kf_park76(Tw,R0,Mm_oxid,optFlag)
% getSurfaceReaction_kf_park76 computes the forward reaction rates of
% surface reactions as proposed by Park
%
% Usage:
%   (1)
%       lnkf_GSr = getSurfaceReaction_kf_park76(Tw,R0,Mm_oxid,optFlag)
%
% Inputs and outputs:
%   Tw              Wall temperatures (size Nw x N_GSreac)
%   R0              Universal gas constant (size 1 x 1)
%   Mm_oxid         Species molar mass of the oxidizer (size N_GSreac x 1)
%   optFlag         string identifying if we want to use the first option
%                   proposed by Park (Eq. 2 in Ref. [1] and Eq. 2.45 in
%                   Ref. [2]) or the second (Eq. 3 in Ref. [1] and Eq. 2.46
%                   in Ref. [2])
%   lnkf_GSr        forward reaction rates of Gas-Surface interactions
%                   (size Nw x N_GSreac)
%
% References:
%   [1] Park, C. (1976). Effects of Atomic Oxygen on Graphite Ablation.
%       AIAA Journal, 14(11), 1640–1642. https://doi.org/https://doi.org/10.2514/3.7267
%   [2] Mortensen, C. (2013). Effects of Thermochemical Nonequilibrium on
%       Hypersonic Boundary-Layer Instability in the Presence of Surface
%       Ablation or Isolated Two-Dimensional Roughness. UCLA.
%
% Author(s): Fernando Miro Miro
% Date: November 2018
% GNU Lesser General Public License 3.0

% obtaining sizes
[Nw,~] = size(Tw);            % number of wall positions

% computing amplitude terms
switch optFlag
    case 'opt1'
        lnAlfa = log( (1.43e-3 + 0.01*exp(-1450./Tw)) ./ (1 + 2e-4*exp(13000./Tw)) );    % Eq. 2 in Ref. [1] and Eq. 2.45 in Ref. [2]
    case 'opt2'
        lnAlfa = log(0.63) - 1160./Tw;                                                  % Eq. 3 in Ref. [1] and Eq. 2.46 in Ref. [2]
    otherwise
        error(['the chosen option flag ''',optFlag,''' is not supported']);
end

% resizing
Mm_oxid = repmat(Mm_oxid.',[Nw,1]);    % (r,1) --> (w,r)

% computing reaction rates
lnkf_GSr = lnAlfa + 0.5 * log(R0*Tw./(2*pi*Mm_oxid));       % Eq. 2.44 in Ref. [2]