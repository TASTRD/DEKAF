function [sourceAbl,dsourceAbl_dT,dsourceAbl_dym] = getSurface_energySource(TTrans,TRot,TVib,TElec,Tel,sourceGS_s,dsourceGSs_dT,dsourceGSs_dym,R_s,...
    nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,sigmaStefBoltz,epsilonRadSurf,options)
% getSurface_energySource computes the source term appearing in the
% surface-energy-balance equation.
%
% Usage:
%   (1)
%       [sourceAbl,dsourceAbl_dT,dsourceAbl_dym] = getSurface_energySource(TTrans,TRot,TVib,TElec,Tel,...
%               sourceGS_s,dsourceGSs_dT,dsourceGSs_dym,R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,...
%               bElectron,hForm_s,sigmaStefBoltz,epsilonRadSurf,options)
%
% Inputs and outputs:
%   TTrans
%       translational temperature (Neta x 1)
%   TRot
%       rotational temperature (Neta x 1)
%   TVib
%       vibrational temperature (Neta x 1)
%   TElec
%       electronic temperature (Neta x 1)
%   Tel
%       electron temperature (Neta x 1)
%   sourceGS_s
%       species surface mass production source term (Neta x N_spec)
%   dsourceGSs_dT
%       temperature derivative(s) of the species surface mass production
%       source term (Neta x N_spec x N_T)
%   dsourceGSs_dym
%       mass-fraction derivatives of the species surface mass production
%       source term (Neta x N_spec(m) x N_spec(s))
%   R_s
%       species specific gas constant (N_spec x 1)
%   nAtoms_s
%       species number of atoms (N_spec x 1)
%   thetaVib_s
%       species activation temperature of each vibrational mode (N_spec x Nvib)
%   thetaElec_s
%       species activation temperature of each electronic energy level (N_spec x Nstat)
%   gDegen_s
%       species degeneracy of each electronic energy level (N_spec x Nstat)
%   bElectron
%       species electron boolean (N_spec x 1)
%   hForm_s
%       species formation enthalpy (N_spec x 1)
%   options
%       options structure with necessary fields
%          .neglect_hRot
%          .neglect_hVib
%          .neglect_hElec
%   sourceAbl
%       surface energy source term (Neta x 1)
%   dsourceGSs_dT
%       temperature derivative(s) of the surface energy source term (Neta x N_T)
%   dsourceGSs_dym
%       mass-fraction derivatives of the surface energy source term (Neta x N_spec(m))
%
% See also: getSurface_sourceRad, getSurface_sourceConv
%
% Author: Fernando Miro Miro
% Date: April 2019
% GNU Lesser General Public License 3.0

% radiative source term
sourceRad       = getSurface_sourceRad(       sigmaStefBoltz,epsilonRadSurf,TTrans);
dsourceRad_dT   = getSurfaceDer_dsourceRad_dT(sigmaStefBoltz,epsilonRadSurf,TTrans,options);
dsourceRad_dym  = zeros(size(sourceGS_s));
% convective source term
sourceConv      = getSurface_sourceConv(        TTrans,TRot,TVib,TElec,Tel,sourceGS_s,              R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options);
dsourceConv_dT  = getSurfaceDer_dsourceConv_dT( TTrans,TRot,TVib,TElec,Tel,sourceGS_s,dsourceGSs_dT,R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options);
dsourceConv_dym = getSurfaceDer_dsourceConv_dym(TTrans,TRot,TVib,TElec,Tel,dsourceGSs_dym,          R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options);

% assembling
sourceAbl       = sourceRad      + sourceConv;
dsourceAbl_dT   = dsourceRad_dT  + dsourceConv_dT;
dsourceAbl_dym  = dsourceRad_dym + dsourceConv_dym;

end % getSurface_energySource