function dsourceGSs_dym = getSurfaceDer_catalytic_dsourceGS_dym(p,y_s,nuCat_prod,nuCat_reac,Mm_s,bElectron,TTrans,Tel,R0,wallYs_BC,varargin)
% getSurfaceDer_catalytic_dsourceGS_dym computes the concentration
% derivatives of the species source term at the wall due to catalytic
% recombination reactions.
%
% Usage:
%   (1)
%       dsourceGSs_dym = getSurfaceDer_catalytic_dsourceGS_dym(p,y_s,nuCat_prod,nuCat_reac,Mm_s,...
%                                           bElectron,TTrans,Tel,R0,'catalyticK',K_r)
%       |--> usage when providing the catalytic-recombination velocities K_r
%
%   (2)
%       dsourceGSs_dym = getSurfaceDer_catalytic_dsourceGS_dym(p,y_s,nuCat_prod,nuCat_reac,Mm_s,...
%                                           bElectron,TTrans,Tel,R0,'catalyticGamma',gammaCat_r)
%       |--> usage when providing the catalytic-recombination probabilities gammaCat_r
%
% Inputs and outputs:
%   p                           [Pa]
%       pressure (size N_eta x 1)
%   y_r                         [-]
%       species mass fraction (size N_eta x N_spec)
%   nuCat_prod, nuCat_reac      [-]
%       stoichiometric coefficients of products and reactants in catalytic
%       recombination reactions (size N_spec x N_reac)
%   Mm_s                        [kg/mol]
%       species molar mass (size N_spec x 1)
%   bElectron                   [-]
%       electron boolean (size N_spec x 1)
%   TTrans                      [K]
%       translational temperature of heavy particles (size N_eta x 1)
%   Tel                         [K]
%       translational temperature of electrons (size N_eta x 1)
%   R0                          [J/kg-mol]
%       ideal gas constant (size 1 x 1)
%   K_r                         [m/s]
%       catalytic recombination velocity of each reaction (size N_reac x 1)
%   gammaCat_r                  [-]
%       catalytic recombination probability of each reaction (size N_reac x 1)
%   dsourceGSs_dym              [kg/s-m^2]
%       concentration derivative of the species production rate per unit
%       surface due to catalytic recombination reactions
%       (size N_eta x N_spec(m) x N_spec(s))
%
% Author: Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

[N_spec,N_reac] = size(nuCat_prod);
N_eta           = length(TTrans);

% Computing auxiliary quantities
Mm_s3D = repmat(Mm_s.',[N_eta,1,N_reac]);                       % (s,1) ----> (eta,s,r)
Mm_s2D = repmat(Mm_s.',[N_reac,1]);                             % (s,1) ----> (r,s)
MmReac_r = (nuCat_reac.' .* Mm_s2D)*ones(N_spec,1);             % (r,1)

switch wallYs_BC
    case 'catalyticGamma'
        gammaCat_r      = varargin{1}(:);                                       % [-] (r,1)
        gammaCat_r3D    = repmat(permute(gammaCat_r,[2,3,1]),[N_eta,N_spec,1]);     % (r,1) ----> (eta,s,r)
        TTrans3D        = repmat(        TTrans,             [1,N_spec,N_reac]);    % (eta,1) --> (eta,s,r)
        K_sr            = gammaCat_r3D.*sqrt(R0.*TTrans3D./(2*pi*Mm_s3D));          % (eta,s,r)
    case 'catalyticK'
        K_sr            = repmat(permute(varargin{1}(:),[2,3,1]),[N_eta,N_spec,1]); % [m/s] (r,1) ----> (eta,s,r)
    otherwise
        error(['Error: the chosen wallYs_BC ''',wallYs_BC,''' is not supported']);
end

drho_dym    = getMixtureDer_drho_dym(p,y_s,R0,Mm_s,bElectron,TTrans,Tel);           % (eta,m)
rho         = getMixture_rho(y_s,p,TTrans,Tel,R0,Mm_s,bElectron);                   % (eta,1)
drho_dym3D  = repmat(permute(drho_dym,   [1,3,2]),[1,N_spec,1]);                    % (eta,m) --> (eta,s,m)
rho2D       = repmat(        rho,                 [1,N_spec]);                      % (eta,1) --> (eta,s)
delta_sm    = repmat(permute(eye(N_spec),[3,1,2]),[N_eta,1,1]);                     % (s,m) ----> (eta,s,m)
nuCat_reac3D= repmat(permute(nuCat_reac, [3,1,2]),[N_eta,1,1]);                     % (s,r) ----> (eta,s,r)

% Computing source term due to each catalytic reaction
for r=N_reac:-1:1                                                           % looping reactions
    for m=N_spec:-1:1                                                           % looping species
        dmCatr_dym(:,r,m)  = (nuCat_reac3D(:,:,r).*Mm_s3D(:,:,r)./MmReac_r(r) .* K_sr(:,:,r) .* (drho_dym3D(:,:,m).*y_s + rho2D.*delta_sm(:,:,m)))*ones(N_spec,1); % (eta,r,m)
    end
end

% Computing species source term
delta_nuCat   = nuCat_prod - nuCat_reac;                                            % (s,r)
delta_nuCat3D = repmat(permute(delta_nuCat,[3,2,1]),[N_eta,1,1]);                   % (s,r) ----> (eta,r,s)
MmReac_r2D    = repmat(        MmReac_r.',          [N_eta,1]);                     % (r,1) ----> (eta,r)
for s=N_spec:-1:1                                                           % looping species (s)
    for m=N_spec:-1:1                                                           % looping species (m)
        dsourceGSs_dym(:,s,m) = (dmCatr_dym(:,:,m).*delta_nuCat3D(:,:,s).*Mm_s(s)./MmReac_r2D)*ones(N_reac,1);    % (eta,s,m)
    end
end
dsourceGSs_dym = permute(dsourceGSs_dym,[1,3,2]);                                   % (eta,s,m) --> (eta,m,s)

end % getSurfaceDer_catalytic_dsourceGS_dym