function dsourceConv_dym = getSurfaceDer_dsourceConv_dym(TTrans,TRot,TVib,TElec,Tel,dsourceGSs_dym,R_s,...
                                nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options)
% getSurfaceDer_dsourceConv_dym returns the temperature derivatives of the source term for the convected energy in
% ablating walls.
%
% Usage:
%   (1)
%       dsourceConv_dym = getSurfaceDer_dsourceConv_dym(TTrans,TRot,TVib,TElec,Tel,dsourceGSs_dym,R_s,...
%                                 nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options)
%
% Inputs and outputs:
%   TTrans
%       translational temperature (Neta x 1)
%   TRot
%       rotational temperature (Neta x 1)
%   TVib
%       vibrational temperature (Neta x 1)
%   TElec
%       electronic temperature (Neta x 1)
%   Tel
%       electron temperature (Neta x 1)
%   dsourceGSs_dym
%       mass-fraction derivatives of the species surface mass production
%       source term (Neta x N_spec(m) x N_spec(s))
%   R_s
%       species specific gas constant (N_spec x 1)
%   nAtoms_s
%       species number of atoms (N_spec x 1)
%   thetaVib_s
%       species activation temperature of each vibrational mode (N_spec x Nvib)
%   thetaElec_s
%       species activation temperature of each electronic energy level (N_spec x Nstat)
%   gDegen_s
%       species degeneracy of each electronic energy level (N_spec x Nstat)
%   bElectron
%       species electron boolean (N_spec x 1)
%   hForm_s
%       species formation enthalpy (N_spec x 1)
%   options
%       options structure with necessary fields
%          .neglect_hRot
%          .neglect_hVib
%          .neglect_hElec
%   dsourceConv_dym
%       mass-fraction derivatives of the source term corresponding to the
%       energy convected away in the ablative surface-energy balance
%       equation (N_eta x N_spec(m))
%
% See also: getSurfaceSpecies_sourceGS
%
% Author: Fernando Miro Miro
% Date: April 2019
% GNU Lesser General Public License 3.0

protectedvalue(options,'modelThermal',{'RRHO','RRHO-1LA'});

[N_eta,N_spec,~] = size(dsourceGSs_dym); % number of species

% adapting inputs to how getSpecies_h_cp wants them
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
             bElectron_mat,hForm_s_mat,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat] = reshape_inputs4enthalpy(R_s,...
             nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,TTrans,TRot,TVib,TElec,Tel);

% computing species enthalpies
h_s = getSpecies_h_cp(TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat,R_s_mat,nAtoms_s_mat,...
                thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options);

dsourceGSs_dym = permute(dsourceGSs_dym,[1,3,2]);                   % (eta,m,s) --> (eta,s,m)
dsourceConv_dym = zeros(N_eta,N_spec);                              % allocating (eta,m)
for m=1:N_spec                                                      % looping differentiating species
    dsourceConv_dym(:,m) = (h_s.*dsourceGSs_dym(:,:,m))*ones(N_spec,1); % summing over the (other - m) species
end

end % getSurface_sourceConv