function D_sl = getPair_D_multicomp_StefanMaxwell(D_sl_b,y_s,TTrans,Tel,Mm_s,bElectron,bPos,qEl,kBoltz,bAmbipolar,varargin)
% getPair_D_multicomp_StefanMaxwell computes the multicomponent diffusion
% coefficients for each pair of species inverting Stefan-Maxwell's system
% of equations.
%
%   Examples:
%       (1)     D_sl = getPair_D_multicomp_StefanMaxwell(D_sl_b,y_s,TTrans,Tel,...
%                                   Mm_s,bElectron,bPos,qEl,kBoltz,bAmbipolar)
%
%       (2)     D_sl = getPair_D_multicomp_StefanMaxwell(...,a)
%               |--> Allows to fix the scaling factor for the imposition of
%               the Lagrange condition (see Magin). This changes the
%               diffusion coefficients, but not the fluxes.
%
%   Inputs and Outputs:
%       D_sl_b          size N_eta x N_spec x N_spec
%       y_s             size N_eta x N_spec
%       TTrans          size N_eta x 1
%       Tel             size N_eta x 1
%       Mm_s            size N_spec x 1
%       bElectron       size N_spec x 1
%       bPos            size N_spec x 1
%       qEl             size 1 x 1
%       kBoltz          size 1 x 1
%       D_sl            size N_eta x N_spec x N_spec
%       bAmbipolar      size 1 x 1
%
% NOTE: does not include ambipolar condition
%
% References:
%   () Taylor 1993, Multicomponent Mass Transfer pag. 20 & 68.
%   () Stuckert Thesis 1991, Arizona State Univ. pag. 202.
%   () Magin, T. E., & Degrez, G. (2004). Transport algorithms for
%   partially ionized and unmagnetized plasmas. Journal of Computational
%   Physics, 198(2), 424–449.
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);                                                 % obtaining sizes

GV_sl = getPair_GV(TTrans,Tel,y_s,Mm_s,bElectron,D_sl_b);                   % main matrix coefficients

% regularizing GV_sl matrix
y_s3D       = repmat(        y_s               ,[1,1,N_spec]);              % (eta,s) --> (eta,s,l)
y_l3D       = repmat(permute(y_s,      [1,3,2]),[1,N_spec,1]);              % (eta,l) --> (eta,s,l)

if isempty(varargin);   a = 1./max(max(D_sl_b,[],2),[],3);                  % default scaling factor
else;                   a = varargin{1}*ones(N_eta,1);                      % user-specified scaling factor
end
a_3D = repmat(a,[1,N_spec,N_spec]);                                         % (eta,1) --> (eta,s,l)
GV_sl = GV_sl + a_3D.*y_s3D.*y_l3D;                                                 % imposing the sum of diffusion fluxes to be zero

if any(bElectron) && bAmbipolar                                             % ionized mixtures - ambipolar electric field contribution
    % computing extra quantities
    T_s = getSpecies_T(TTrans,Tel,bElectron);                                   % species temperature
    [kCharge_s,sKCharge] = getSpecies_kCharge(TTrans,y_s,Mm_s,bPos,bElectron,qEl,kBoltz); % electric-field grouping variables

    % reshaping
    sKCharge_2D = repmat(sKCharge,[1,N_spec]);                           % (eta,1) --> (eta,s)
    TTrans_2D   = repmat(TTrans  ,[1,N_spec]);                           % (eta,1) --> (eta,s)

    % Concatenating matrices (Eq. 23 from Magin 2004)
    Y_11 = permute(                     GV_sl                   ,[2,3,1]);      % (eta,s) --> (s,l,eta)
    Y_12 = permute(-kCharge_s./sKCharge_2D .* TTrans_2D./T_s    ,[2,3,1]);      % (eta,s) --> (s,l,eta)
    Y_21 = permute(-kCharge_s./sKCharge_2D                      ,[3,2,1]);      % (eta,l) --> (s,l,eta)
    Y_22 = zeros(1,1,N_eta);                                                    % (s,l,eta)

    Y = [ Y_11 , Y_12 ;
          Y_21 , Y_22 ];
else                                                                        % non-ionized mixtures - no electric-field contribution
    Y = permute(GV_sl,[2,3,1]);
end

% Inverting Y matrix to obtain D_sl
Y_inv = multiInv(Y);                                                         % inverting
if any(bElectron) && bAmbipolar                                             % ionized mixtures
    D_sl = permute(Y_inv(1:N_spec,1:N_spec,:),[3,1,2]);                         % avoiding last row and column (related Y_12, Y_21 and Y_22)
else                                                                        % neutral mixtures
    D_sl = permute(Y_inv,                     [3,1,2]);                         % keeping all terms
end

end % getPair_D_multicomp_StefanMaxwell