function [D_sl,D_sm] = getPair_D_multicomp_Ramshaw(D_sl_b,y_s,TTrans,Tel,Mm_s,bElectron,R0,bPos,varargin)
% getPair_D_multicomp_Ramshaw computes the multicomponent diffusion
% coefficients for each pair of species using Ramshaw's correction.
%
% Note: by default it enforces the ambipolar condition. Not enforcing it,
% for ionized mixtures, will result in the flow-field having a non-zero
% electric field which should be solved for.
%
%   Examples:
%       (1)     D_sl = getPair_D_multicomp_Ramshaw(D_sl_b,y_s,TTrans,Tel,Mm_s,bElectron,R0,bPos)
%
%       (2)     [D_sl,D_sm] = getPair_D_multicomp_Ramshaw(...)
%           |--> outputs also the species effective diffusion coefficients
%
%       (2)     [D_sl,D_sm] = getPair_D_multicomp_Ramshaw(...,bAmbipolar)
%       |--> allows to pass a boolean determining if the ambipolar
%       condition should be enforced (true-default) or not (false)
%
%   Inputs and Outputs:
%       D_sl_b          size N_eta x N_spec x N_spec
%       y_s             size N_eta x N_spec
%       TTrans          size N_eta x 1
%       Tel             size N_eta x 1
%       Mm_s            size N_spec x 1
%       bElectron       size N_spec x 1
%       R0              size 1
%       D_sl            size N_eta x N_spec x N_spec
%       bPos            size N_spec x 1
%
% References:
% - Ramshaw 1993, Hydrodynamic Theory of Multicomponent Diffusion and
% Thermal Diffusion in Multitemperature Gas Mixtures
% - Fertig 2001, Transport Coefficients for High Temperature Nonequilibrium
% Air Flows (clearer, but here we use mutation's expressions)
% - Lee 1984, Basic governing equations for the flight regimes of
% aeroassisted orbital transfer vehicles (for the ambipolar condition)
%
% See also: getSpecies_D_effective, apply_ambipolar_D_Effective
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
%
% GNU Lesser General Public License 3.0

if isempty(varargin)
    bAmbipolar = true;
else
    bAmbipolar = varargin{1};
end

[N_eta,N_spec] = size(y_s);                                                 % obtaining sizes

% Obtaining unit charge
ZCharge_s = getSpecies_ZCharge(bPos,bElectron);

Mm = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);                     % mixture molar mass
X_s = getSpecies_X(y_s,Mm,Mm_s);                                            % mole fractions
D_sm = getSpecies_D_effective(D_sl_b,y_s,Mm,Mm_s,bElectron,bPos,bAmbipolar);

Mm_e = Mm_s(bElectron);     % electron molar mass
if isempty(Mm_e)            % in case there are no electrons, we fix it to zero,
    Mm_e = 0;               % to make sure the ambipolar term dissappears
end

% reshaping and repmatting for 3D
delta_sl = repmat(reshape(eye(N_spec,N_spec),[1,N_spec,N_spec]),[N_eta,1,1]); % (s,l) --> (eta,s,l)
delta_el3D = repmat(reshape(bElectron,[1,1,N_spec]),[N_eta,N_spec,1]);        % (l,1) --> (eta,s,l)
ZCharge_l3D = repmat(reshape(ZCharge_s,[1,1,N_spec]),[N_eta,N_spec,1]);     % (l,1) --> (eta,s,l)
Mm_l3D = repmat(reshape(Mm_s,[1,1,N_spec]),[N_eta,N_spec,1]);               % (l,1) --> (eta,s,l)
X_l3D = repmat(reshape(X_s,[N_eta,1,N_spec]),[1,N_spec,1]);                 % (eta,l) --> (eta,s,l)
y_l3D = repmat(reshape(y_s,[N_eta,1,N_spec]),[1,N_spec,1]);                 % (eta,l) --> (eta,s,l)
D_lm3D = repmat(reshape(D_sm,[N_eta,1,N_spec]),[1,N_spec,1]);               % (eta,l) --> (eta,s,l)

% reshaping and repmatting for 2D
delta_el2D = repmat(reshape(bElectron,[1,1,N_spec]),[N_eta,1,1]);           % (l,1) --> (eta,1,l)
ZCharge_l2D = repmat(reshape(ZCharge_s,[1,1,N_spec]),[N_eta,1,1]);          % (l,1) --> (eta,1,l)
y_e2D = repmat(y_s(:,bElectron),[1,1,N_spec]);                              % (eta,1) --> (eta,1,l)
X_e2D = repmat(X_s(:,bElectron),[1,1,N_spec]);                              % (eta,1) --> (eta,1,l)
X_l2D = reshape(X_s,[N_eta,1,N_spec]);                                      % (eta,l) --> (eta,1,l)
y_l2D = reshape(y_s,[N_eta,1,N_spec]);                                      % (eta,l) --> (eta,1,l)
Mm_l2D = repmat(reshape(Mm_s,[1,1,N_spec]),[N_eta,1,1]);                    % (l,1) --> (eta,1,l)
D_lm2D = reshape(D_sm,[N_eta,1,N_spec]);                                    % (eta,l) --> (eta,1,l)

if bAmbipolar                                                               % enforcing ambipolar condition
    ambipolarFactor = 1+ZCharge_l3D.*Mm_e./Mm_l3D;                              % factor accounting for the ambipolar condition (see Fertig 2001 and Lees 1984)

    % computing diffusion coefficients
    D_sl = (delta_sl-y_l3D.*ambipolarFactor)./X_l3D     .*    (1-y_l3D)./(1-X_l3D) .* D_lm3D .* (1-delta_el3D); % diffusion coefficients for rows other than that of the electron species
    % note: the (1-delta_el) factor at the end ensures that the column
    % corresponding to electron species is zero
    if nnz(bElectron)                                                           % if there is an electron species
        D_el = ((1-y_e2D).*ZCharge_l2D - y_e2D.*Mm_l2D./Mm_e)./X_e2D .* (1-y_l2D)./(1-X_l2D) .* D_lm2D .* (1-delta_el2D); % diffusion coefficient for the row of the electron species
        D_sl(:,bElectron,:) = D_el;                                             % we populate the corresponding row
    end
else                                                                        % no ambipolar condition
    D_sl = (delta_sl-y_l3D)./X_l3D.*(1-y_l3D)./(1-X_l3D) .* D_lm3D;
end