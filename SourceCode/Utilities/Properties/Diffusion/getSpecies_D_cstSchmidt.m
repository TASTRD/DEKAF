function [D_s,dDs_dT] = getSpecies_D_cstSchmidt(Sc,mu,rho,N_spec,varargin)
% getSpecies_D_cstSchmidt calculates the species diffusion coefficient
% assuming a constant Schmidt number, as well as its derivative with
% respect to temperature
%
%   Examples:
%       (1) D_s          = getSpecies_D_cstSchmidt(Sc,mu,rho,N_spec)
%       (2) [D_s,dDs_dT] = getSpecies_D_cstSchmidt(Sc,mu,rho,N_spec,dmu_dT,drho_dT)
%
% Inputs & Outputs:
%   Sc          -   Schmidt number [-].
%                   Single value size (1 x 1)
%   mu          -   mixture viscosity [kg/m-s].
%                   Vector of size (N_eta x 1)
%   rho         -   mixture density [kg/m-s].
%                   Vector of size (N_eta x 1)
%   N_spec      -   Number of species [-]
%   D_s         -   Species diffusion coefficient
%                   Matrix of size (N_eta x N_spec)
%   varargin    -   remaining inputs are for calculating the derivative
%                   of D_s with respect to T
%       dmu_dT  -   Vector of size (N_eta x 1)
%       drho_dT -   Vector of size (N_eta x 1)
%       dDs_dT  -   Species diffusion coefficient derivative wrt T
%                   Matrix of size (N_eta x N_spec)
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

D_s = mu./(rho*Sc);
D_s = repmat(D_s,[1,N_spec]); % repeating once for every species

if nargout>1
    ic = 1;
    dmu_dT  = varargin{ic};  ic = ic + 1;
    drho_dT = varargin{ic}; %ic = ic + 1;
    dDs_dT  = 1/Sc * (1./rho.*dmu_dT - mu./rho.^2.*drho_dT);
    dDs_dT  = repmat(dDs_dT,[1,N_spec]); % repeating once for every species
end

end % getSpecies_D_cstSchmidt.m
