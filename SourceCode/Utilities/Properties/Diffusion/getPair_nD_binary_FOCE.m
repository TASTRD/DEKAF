function [D_sl_b,nD_sl_b] = ...
    getPair_nD_binary_FOCE(TTrans,Tel,y_s,rho,consts_Omega11,Mm_s,...
        kBoltz,nAvogadro,bElectron,qEl,epsilon0,bPos,varargin)
%GETPAIR_ND_BINARY_FOCE Calculates the species-species binary diffusion
% coefficient using the first-order Chapman Enskog (FOCE) approximation.
%
% Usage:
% 1.
% [D_sl_b,nD_sl_b] = ...
%   getPair_nD_binary_FOCE(TTrans,Tel,y_s,rho,p,consts_Omega11,Mm_s,...
%       kBoltz,R0,nAvogadro,bElectron,qEl,epsilon0,bPos)
%
% Inputs:
%   TTrans          N_eta x 1
%   Tel             N_eta x 1
%   y_s             N_eta x N_spec
%   rho             only needed if the user wants D_sl_b. for nD_sl_b it isn't
%                   needed, and therefore a dummy input can be passed
%                   N_eta x 1
%   consts_Omega11  struct. each field is N_spec x N_spec
%   Mm_s            N_spec x 1
%   kBoltz          1 x 1
%   nAvogadro       1 x 1
%   bElectron       N_spec x 1
%   qEl             elementary charge (1 x 1)
%   epsilon0        permittivity of vacuum (1 x 1)
%   bPos            boolean for positively charged particles (N_spec x 1)
%   options         necessary fields
%                       flow
%
% Outputs:
%   D_sl_b          binary diffusion coefficient (N_eta x N_s x N_l)
%   nD_sl_b         binary diffusion coefficient multiplied by mixture
%                   number density
%                   (N_eta x N_s x N_l)
%
% References:
% Magin, T. E. and Degrez, G., "Transport of partially ionized and
%   unmagnetized plasmas," Physical Review E, Vol. 70, 2004.
% Scoggins, J. (2017). Development of numerical methods and study of
%   coupled flow, radiation, and ablation phenomena for atmospheric entry.
%   von Karman Institute.
%
% Children functions:
% See also getPair_redMass.m, getPair_lnOmegaij.m
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

if ~isempty(varargin)
    ic = 1;
    options = varargin{ic};     ic = ic + 1;
else
    options = struct;
end

% Calculate reduced mass of each pair of particles
mu_sl = getPair_redMass(Mm_s,nAvogadro); % (N_s x N_l)

% Calculate (1,1) diffusion collision integral
lnOmega11_sl = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega11,bPos);

Omega11_sl = exp(lnOmega11_sl);

[N_eta,N_spec,~] = size(Omega11_sl);

m_e = Mm_s(bElectron)/nAvogadro; % electron mass
if isempty(m_e) % in case there are no electron we fix it to zero as a place-holder
    m_e = 0;
end

% Reshaping matrices for 3D calculation
TTrans3D    = repmat(TTrans,[1,N_spec,N_spec]);
Tel3D       = repmat(Tel,[1,N_spec,N_spec]);
mu_sl3D     = repmat(reshape(mu_sl,[1,N_spec,N_spec]),[N_eta,1,1]);

% Equation A.1 from Scoggins 2017 Thesis
% Note that Magin's & Scoggins's notation Qbar == pi*Omega11
nD_sl_b = 3/16 * sqrt(2*pi*kBoltz*TTrans3D./mu_sl3D) ./ (pi*Omega11_sl);
if nnz(bElectron)
    nD_se_b = 3/16 * sqrt(2*pi*kBoltz*Tel3D./m_e) ./ (pi*Omega11_sl); % electron-heavy diffusion
    nD_ee_b = 3/8  * sqrt(  pi*kBoltz*Tel3D./m_e) ./ (pi*Omega11_sl); % electron-electron diffusion
    nD_sl_b(:,bElectron,:) = nD_se_b(:,bElectron,:); % populating rows of electron-heavy diffusion
    nD_sl_b(:,:,bElectron) = nD_se_b(:,:,bElectron); % populating columns of electron-heavy diffusion
    nD_sl_b(:,bElectron,bElectron) = nD_ee_b(:,bElectron,bElectron); % populating electron-electron diffusion
end

% Get mixture number density
n = getMixture_n(y_s,rho,Mm_s,nAvogadro);
n3D = repmat(n,[1,N_spec,N_spec]);

% Calculate binary diffusion coefficient
D_sl_b = nD_sl_b ./ n3D;

end % getPair_nD_binary_FOCE.m
