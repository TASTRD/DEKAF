function D_sm = apply_ambipolar_D_Effective(D_sm,bElectron,bPos)
% apply_ambipolar_D_Effective modifies the effective diffusion coefficients
% to account for the ambipolar condition (local charge balance).
%
% Examples:
%   apply_ambipolar_D_Effective(D_sm,bElectron,bPos)
%
% Inputs and Outputs:
%   D_sm        size N_eta x N_spec
%   bElectron   size N_spec x 1
%
% References:
% Fertig 2001, Transport Coefficients for High Temperature Nonequilibrium
% Air Flows (clearer, but here we use mutation's expression)
% Lee 1984, Basic governing equations for the flight regimes of
% aeroassisted orbital transfer vehicles (for the ambipolar condition)
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(D_sm); % obtaining sizes
ZCharge_s = getSpecies_ZCharge(bPos,bElectron);
ambipolarFactor = ones(N_spec,1) + (ZCharge_s>0); % the ambipolar factor is 2 for ions (charge larger than 0) and 1 for all others (See lee 1984)
ambipolarFactor_2D = repmat(ambipolarFactor',[N_eta,1]);

D_sm = D_sm.*ambipolarFactor_2D; % ambipolar condition
