function J_s = getSpecies_Js(varargin)
% getSpecies_Js computes the species diffusion flux.
%
% Usage:
%   (1)
%       J_s = getSpecies_Js('spec-mix',d_s,D_s,rho)
%       |--> modeling diffusion of each species within the mixture
%
%   (2)
%       J_s = getSpecies_Js('spec-spec',d_s,D_sl,rho_s)
%       |--> modeling diffusion of each species within each other species
%
% Inputs and outputs:
%   d_s         species diffusion forcing vector (e.g.: dXs/dy) [N_eta x N_spec]
%   D_s         species effective diffusion coefficient [N_eta x N_spec]
%   D_sl        multicomponent diffusion coefficient [N_eta x N_spec x N_spec]
%   rho         mixture density [N_eta x N_spec]
%   rho_s       species partial density [N_eta x N_spec]
%   J_s         species mass diffusion flux
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

ic = 1;
flag = varargin{ic}; ic=ic+1;
d_s = varargin{ic}; ic=ic+1;
[N_eta,N_spec] = size(d_s);             % sizes
switch flag
    case 'spec-mix'
        D_s = varargin{ic}; ic=ic+1;
        rho = varargin{ic}; %ic=ic+1;
        J_s = D_s.*d_s.*repmat(rho,[1,N_spec]);
    case 'spec-spec'
        D_sl = varargin{ic}; ic=ic+1;
        rho_s = varargin{ic}; %ic=ic+1;
        J_s = zeros(N_eta,N_spec);      % initializing
        for l=1:N_spec                  % looping species
            J_s = J_s + D_sl(:,:,l).*repmat(d_s(:,l),[1,N_spec]); % summing contributions
        end
        J_s = J_s.*rho_s;               % final factor
    otherwise
        error(['the chosen flag ''',flag,''' is not supported']);
end

