function D_sl_b = getPair_D_binary_GuptaYos(p,T,consts_Dbar_binary)
%GETPAIR_D_BINARY_GUPTAYOS Calculates the species binary diffusion
% coefficient using the curve fit prescribed by Gupta Yos 1990 paper
%
% Usage:
%   D_sl_b = getPair_D_binary_GuptaYos(p,T,consts_Dbar_binary);
%
% Inputs & Outputs:
%   p           -   boundary-layer pressure (N_eta x 1) [Pa].
%   T           -   temperature under thermodynamic equilibrium (N_eta x 1)
%                   measured in Kelvin
%   const_Dbar_binary - structure containing the fields necessary for
%                   eval_Fit
%   D_sl_b      -   Species binary diffusion coefficient [m^2 / sec]
%                   Matrix of size (N_eta x N_s x N_l)
%
% See also: eval_Fit, convert2SI_Logarithm
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

N_spec = size(consts_Dbar_binary.A,1);
T_3D = repmat(T,[1,N_spec,N_spec]); % eval_fig_PolyLogLog requires T to be 3D
lnDbar_sl = eval_Fit(T_3D,consts_Dbar_binary);

p3D = repmat(p,[1,N_spec,N_spec]);

D_sl_b = exp(lnDbar_sl)./p3D;

end % getPair_D_binary_GuptaYos.m
