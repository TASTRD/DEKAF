function dDs_dT = getSpeciesDer_dDs_dT_Fick(D_sl,y_s,Mm,Mm_s,dXs_dT,dDsl_dT)
%GETSPECIESDER_DS_DT_FICK Calculate the derivative of spec-mix diffusion
% coefficient with respect to translational temperature
%
% Usage:
%   dDs_dT = getSpeciesDer_dDs_dT_Fick(D_sl,y_s,Mm,Mm_s,dXs_dT,dDsl_dT);
%
% Inputs and Outputs:
%   D_sl        N_eta x N_spec x N_spec
%   y_s         N_eta x N_spec
%   Mm          N_eta x 1
%   Mm_s        N_spec x 1
%   dXs_dT      N_eta x N_spec
%   dDsl_dT     N_eta x N_spec x N_spec
%
%   dDs_dT      N_eta x N_spec
%
% Children and related functions:
% See also getSpecies_D_Fick.m
%
% References:
% Schrooyen, P. (2015). Numerical simulation of aerothermal flows through
% ablative thermal protection systems. UCL. See equation 2.33.
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);

X_s2D = getSpecies_X(y_s,Mm,Mm_s);
X_l3D = repmat(reshape(X_s2D,[N_eta,1,N_spec]),[1,N_spec,1]);

% Sum X_l / D_sl without l = s
lSum = zeros(N_eta,N_spec);
for l=1:N_spec
    sNotL = 1:N_spec;
    sNotL(l) = [];
    lSum = lSum + X_l3D(:,sNotL,l)./D_sl(:,sNotL,l);
end

dXl_dT  = repmat(reshape(dXs_dT,[N_eta,1,N_spec]),[1,N_spec,1]);

% Calculate the deriv. of lSum wrt temperature (transl || equilibrium)
lSumDer = zeros(N_eta,N_spec);
for l=1:N_spec
    sNotL = 1:N_spec;
    sNotL(l) = [];
    lSumDer = lSumDer + dXl_dT(:,sNotL,l)./D_sl(:,sNotL,l) + ...
        -X_l3D(:,sNotL,l)./D_sl(:,sNotL,l).^2 .* dDsl_dT(:,sNotL,l);
end

dDs_dT = -dys_dT./lSum - (1-y_s).*(lSum).^(-2).*lSumDer;

end % getSpeciesDer_dDs_dT_Fick.m
