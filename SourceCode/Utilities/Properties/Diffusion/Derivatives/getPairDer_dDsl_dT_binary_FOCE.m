function dDsl_dT = getPairDer_dDsl_dT_binary_FOCE(TTrans,y_s,...
    rho,dys_dT,drho_dT,consts_Omega11,Mm_s,kBoltz,nAvogadro)
%GETPAIRDER_DDSL_DT_BINARY_FOCE Calculates the temperature-derivative of
% species-species binary diffusion coefficient using the first-order
% Chapman Enskog (FOCE) approximation.
%
% Usage:
%
% FIXME: Current implementation only accounts for diffusion coefficients of
% heavy-heavy interactions: no heavy-electron, or electron-electron
% interactions are accounted for. See Scoggin's 2017 Appendix A for these
% formulae.
%
% OLD FUNCTION! NOT SUPPORTED
%
% Inputs:
%   TTrans          N_eta x 1
%   consts_Omega11  struct. each field is N_spec x N_spec
%   Mm_s            N_spec x 1
%   kBoltz          1 x 1
%   nAvogadro       1 x 1
%
% Outputs:
%   dDsl_dT         binary diffusion coefficient (N_eta x N_s x N_l)
%
% References:
% Magin, T. E. and Degrez, G., "Transport of partially ionized and
%   unmagnetized plasmas," Physical Review E, Vol. 70, 2004.
% Scoggins, J. (2017). Development of numerical methods and study of
%   coupled flow, radiation, and ablation phenomena for atmospheric entry.
%   von Karman Institute.
%
% Children functions:
% See also getPair_redMass.m, eval_Fit.m,
%          getPair_nD_binary_FOCE.m
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

% Calculate reduced mass of each pair of particles
mu_sl = getPair_redMass(Mm_s,nAvogadro); % (N_s x N_l)

% Calculate (1,1) diffusion collision integral
[lnOmega11_sl,dlnOmega11sl_dT] = eval_Fit(TTrans,consts_Omega11);

% Exponentiate
Omega11_sl = exp(lnOmega11_sl);
dOmega11sl_dT = Omega11_sl .* dlnOmega11sl_dT;

[N_eta,N_spec,~] = size(Omega11_sl);

% Reshaping matrices for 3D calculation
TTrans3D    = repmat(TTrans,[1,N_spec,N_spec]);
mu_sl3D     = repmat(reshape(mu_sl,[1,N_spec,N_spec]),[N_eta,1,1]);

n = getMixture_n(y_s,rho,Mm_s,nAvogadro);
dn_dT = getMixtureDer_dn_dT(y_s,rho,dys_dT,drho_dT,Mm_s,nAvogadro);

n3D = repmat(n,[1,N_spec,N_spec]);
dn_dT3D = repmat(dn_dT,[1,N_spec,N_spec]);

% Equation A.1 from Scoggins 2017 Thesis
% Note that Magin's & Scoggins's notation Qbar == pi*Omega11
dDsl_dT = 3/16 * sqrt(2*pi*kBoltz./mu_sl3D) / pi .* ...
        (   dn_dT3D.*sqrt(TTrans3D)./Omega11_sl + ...
            n3D./(2*sqrt(TTrans3D).*Omega11_sl) + ...
            n3D.*sqrt(TTrans3D).*-1./Omega11_sl.^2.*dOmega11sl_dT );

end % getPairDer_dDsl_dT_binary_FOCE.m
