function [D_s,dDs_dT] = getSpecies_D_cstLe(Le,rho,kappa,cp,N_spec,varargin)
% getSpecies_D_cstLe calculates the species diffusion coefficient
% assuming a constant Lewis number, as well as its derivative with
% respect to temperature
%
%   Examples:
%       (1) D_s          = getSpecies_D_cstLe(Le,rho,kappa,cp,N_spec)
%       (2) [D_s,dDs_dT] = getSpecies_D_cstLe(Le,rho,kappa,cp,N_spec,drho_dT,dkappa_dT,dcp_dT)
%
% Inputs & Outputs:
%   Sc          -   Schmidt number [-].
%                   Single value size (1 x 1)
%   rho         -   mixture density [kg/m-s].
%                   Vector of size (N_eta x 1)
%   kappa       -   mixture frozen thermal conductivity [W/K-m].
%                   Vector of size (N_eta x 1)
%   cp          -   mixture heat capacity [J/kg-K].
%                   Vector of size (N_eta x 1)
%   N_spec      -   Number of species [-]
%   D_s         -   Species diffusion coefficient
%                   Matrix of size (N_eta x N_spec)
%   varargin    -   remaining inputs are for calculating the derivative
%                   of D_s with respect to T
%       drho_dT -   Vector of size (N_eta x 1)
%       dkappa_dT-  Vector of size (N_eta x 1)
%       dcp_dT  -   Vector of size (N_eta x 1)
%       dDs_dT  -   Species diffusion coefficient derivative wrt T
%                   Matrix of size (N_eta x N_spec)
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

D_s = Le*kappa./(rho.*cp);
D_s = repmat(D_s,[1,N_spec]); % repeating once for every species

if nargout>1
    ic = 1;
    drho_dT = varargin{ic};  ic = ic + 1;
    dkappa_dT = varargin{ic};  ic = ic + 1;
    dcp_dT = varargin{ic};  %ic = ic + 1;
    dDs_dT  = Le*(dkappa_dT./(rho.*cp) - kappa./(rho.^2.*cp).*drho_dT - kappa./(rho.*cp.^2).*dcp_dT);
    dDs_dT  = repmat(dDs_dT,[1,N_spec]); % repeating once for every species
end

end % getSpecies_D_cstLe.m
