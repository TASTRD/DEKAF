function D_sm = getSpecies_D_effective(D_sl_b,y_s,Mm,Mm_s,bElectron,bPos,varargin)
% getSpecies_D_effective calculates the species effective diffusion
% coefficient.
%
% Note: by default it enforces the ambipolar condition. Not enforcing it,
% for ionized mixtures, will result in the flow-field having a non-zero
% electric field which should be solved for.
%
% Usage:
%   (1)     D_sm = getSpecies_D_effective(D_sl_b,y_s,Mm,Mm_s);
%
%   (2)     D_sm = getSpecies_D_effective(D_sl_b,y_s,Mm,Mm_s,bAmpipolar);
%       |--> allows to pass a boolean determining if the ambipolar
%       condition should be enforced (true-default) or not (false)
%
% Inputs and Outputs:
%   D_sl_b      binary diffusion coefficient
%               N_eta x N_spec x N_spec
%   y_s         N_eta x N_spec
%   Mm          N_eta x 1
%   Mm_s        N_spec x 1
%   bElectron   N_spec x 1
%   bPos        N_spec x 1
%
% References:
% Ramshaw 1993, Hydrodynamic Theory of Multicomponent Diffusion and Thermal
% Diffusion in Multitemperature Gas Mixtures
% Fertig 2001, Transport Coefficients for High Temperature Nonequilibrium
% Air Flows (clearer, but here we use mutation's expression)
% Lee 1984, Basic governing equations for the flight regimes of
% aeroassisted orbital transfer vehicles (for the ambipolar condition)
%
% See also: apply_ambipolar_D_Effective
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

if isempty(varargin)
    bAmbipolar = true;
else
    bAmbipolar = varargin{1};
end

% Obtaining unit charge
[N_eta,N_spec] = size(y_s);

X_s = getSpecies_X(y_s,Mm,Mm_s);
X_l3D = repmat(reshape(X_s,[N_eta,1,N_spec]),[1,N_spec,1]);

% Sum X_l / D_sl without l = s
lSum = zeros(N_eta,N_spec);
for s=1:N_spec
    lNotS = 1:N_spec;
    lNotS(s) = [];
    sumand = X_l3D(:,s,lNotS)./D_sl_b(:,s,lNotS);
    lSum(:,s) = reshape(sumand,[N_eta,N_spec-1])*ones(N_spec-1,1);
end

D_sm = (1 - X_s) ./ lSum;

% enforcing ambipolar condition
if bAmbipolar
    D_sm = apply_ambipolar_D_Effective(D_sm,bElectron,bPos);
    bElectron_mat = repmat(bElectron',[N_eta,1]);
    D_sm = D_sm .* (~bElectron_mat); % making the diffusion coefficient of electrons zero
end

end % getSpecies_D_effective.m
