function D_sl = getPair_D_multicomp_Exact(D_sl_b,y_s,TTrans,Tel,Mm_s,bElectron,R0)
% getPair_D_multicomp_Exact computes the exact multicomponent diffusion
% coefficients for each pair of species, using the exact method proposed by
% Hirschfelder.
%
%   Examples:
%       (1)     D_sl = getPair_D_multicomp_Exact(D_sl_b,y_s,TTrans,Tel,Mm_s,bElectron,R0)
%
%   Inputs and Outputs:
%       D_sl_b          size N_eta x N_spec x N_spec
%       y_s             size N_eta x N_spec
%       TTrans          size N_eta x 1
%       Tel             size N_eta x 1
%       Mm_s            size N_spec x 1
%       bElectron       size N_spec x 1
%       R0              size 1
%       D_sl            size N_eta x N_spec x N_spec
%
% NOTE: does not include ambipolar condition
%
% References:
% -->   Hirschfelder 1954, Molecular theory of liquids and gases, eq.
%       8.2-49 (pag. 541)
%       Curtiss, C. F., & Hirschfelder, J. O. (1949). Transport properties
%       of multicomponent gas mixtures. The Journal of Chemical Physics,
%       17(6), 550–555.
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);                                                 % obtaining sizes

Mm = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);                     % mixture molar mass
X_s = getSpecies_X(y_s,Mm,Mm_s);                                            % mole fractions

% computing summation term for K
sum_XD = zeros(N_eta,N_spec);                                               % initializing sum of X/D_sl
for s=1:N_spec
    kNotS = 1:N_spec;
    kNotS(s) = [];                                                              % all positions except for s
    D_sk_b = permute(D_sl_b(:,s,kNotS),[1,3,2]);                                % (eta,s,k) -> (eta,k,s)
    sum_XD(:,s) = (X_s(:,kNotS)./D_sk_b) * ones(N_spec-1,1);                    % summing over k
end
sum_XM = (X_s.*repmat(Mm_s.',[N_eta,1])) * ones(N_spec,1); % sum of X_s times the molar mass

% computing K diffusion matrix
X_s_3D = repmat(X_s,[1,1,N_spec]);                                          % (eta,s) -> (eta,s,l)
% X_l_3D = repmat(permute(X_s,[1,3,2]),[1,N_spec,1]);                         % (eta,l) -> (eta,s,l)
Mm_s_3D = repmat(reshape(Mm_s,[1,N_spec,1]),[N_eta,1,N_spec]);              % (s,1) -> (eta,s,l)
Mm_l_3D = repmat(reshape(Mm_s,[1,1,N_spec]),[N_eta,N_spec,1]);              % (l,1) -> (eta,s,l)
sum_XD_3D = repmat(sum_XD,[1,1,N_spec]);                                    % (eta,s) -> (eta,s,l)
K = X_s_3D./D_sl_b + Mm_l_3D./Mm_s_3D .* sum_XD_3D;                         % Hirschfelder 1954
% K = X_s_3D.*X_l_3D./D_sl_b + X_l_3D.*Mm_l_3D./Mm_s_3D .* sum_XD_3D;         % Curtis & Hirschfelder 1949

eye_3D = repmat(reshape(eye(N_spec,N_spec),[1,N_spec,N_spec]),[N_eta,1,1]); % 3D identity matrix
K = K.*(~eye_3D); % making diagonal elements of the s,l dimensions of the K diffusion matrix equal to zero
% for the technique see: https://www.mathworks.com/matlabcentral/answers/336070-how-to-replace-the-diagonal-elements-of-a-matrix-with-0-fro-avoiding-self-loops

% computing determinants and minors
% [det_K,K_minors_sl] = detCrout_3D(K);
%%%%% TESTING ALTERNATIVE TO DETCROUT
det_K = zeros(N_eta,1);
K_minors_sl = zeros(N_eta,N_spec,N_spec);
for ii=1:N_eta
    for s=1:N_spec
        for l=1:N_spec
            mat = squeeze(K(ii,:,:));
            mat(s,:) = [];
            mat(:,l) = [];
            K_minors_sl(ii,s,l) = (-1)^(s+l) * det(mat);
        end
    end
    det_K(ii) = det(squeeze(K(ii,:,:)));
%     disp(['Completed eta ',num2str(ii),'/',num2str(N_eta)]);
end
%%%%% UNTIL HERE
K_minors_ls = permute(K_minors_sl,[1,3,2]);                                 % permuting to have the transpose (see Hirschfelder K^(ji) which correponds to our K^(ls))

% Computing multicomponent diffusion
K_minors_ls2D = reshape(K_minors_ls,[N_eta,N_spec^2]);
% K_minors_ss = K_minors_sl .* eye_3D;                                        % retaining only the diagonal
K_minors_ss = repmat(K_minors_ls2D(:,1:N_spec+1:N_spec^2),[1,1,N_spec]);    % retaining only the diagonal
det_K_3D = repmat(det_K,[1,N_spec,N_spec]);                                 % (eta,1) -> (eta,s,l)
% Mm_3D = repmat(Mm,[1,N_spec,N_spec]);                                       % (eta,1) -> (eta,s,l)
sum_XM_3D = repmat(sum_XM,[1,N_spec,N_spec]);                               % (eta,1) -> (eta,s,l)
% y_s_3D = repmat(y_s,[1,1,N_spec]);                                          % (eta,s) -> (eta,s,l)

D_sl = 1./Mm_l_3D .* sum_XM_3D .* (K_minors_ls - K_minors_ss)./det_K_3D;    % Hirschfelder 1954
% D_sl = 1./y_s_3D .* 1./Mm_l_3D .* sum_XM_3D .* (K_minors_ls - K_minors_ss)./det_K_3D;    % Hirschfelder 1954 (with modifications - rho instead of rho_s premultiplying)
% D_sl = 1./Mm_l_3D .* Mm_3D .* (K_minors_ls - K_minors_ss)./det_K_3D;        % unknown source
% D_sl = 1./Mm_l_3D .* Mm_3D .* X_s_3D.*(K_minors_ls - K_minors_ss)./det_K_3D; % Curtis & Hirschfelder 1949
end
