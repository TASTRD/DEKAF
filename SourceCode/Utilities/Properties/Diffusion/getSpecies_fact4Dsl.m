function fact = getSpecies_fact4Dsl(TTrans,Tel,y_s,p,rho,bElectron,Mm_s,nAvogadro,kBoltz,options)
% getSpecies_fact4Dsl computes the factor that must multiply the
% multicomponent diffusion coefficients for ionized mixtures in thermal
% non-equilibrium.
%
% Usage:
%   (1)
%       fact = getSpecies_fact4Dsl(TTrans,Tel,y_s,p,rho,bElectron,Mm_s,nAvogadro,kBoltz,options)
%
% Inputs and outputs:
%   TTrans
%       Translational temperature (N_eta x 1)
%   Tel
%       electron temperature (N_eta x 1)
%   y_s
%       species mass fraction (N_eta x N_spec)
%   p
%       mixture pressure (N_eta x 1)
%   rho
%       mixture density (N_eta x 1)
%   bElectron
%       electron boolean (N_spec x 1)
%   Mm_s
%       species molar mass (N_spec x 1)
%   nAvogadro
%       avogadro's number (1 x 1)
%   kBoltz
%       boltzman's constant (1 x 1)
%   options
%       classic DEKAF options structure. Must contain:
%           .bPairDiff
%               boolean determining if it is a spec-spec diffusion theory
%               (true) or a spec-mix one (false)
%   fact
%       factor to be applied on the diffusion coefficient (N_eta x N_spec)
%       or (N_eta x N_spec x N_spec) depending on the diffusion theory.
%
% Author(s): Fernando Miro Miro
% Date: May 2019
% GNU Lesser General Public License 3.0

N_spec = length(Mm_s);

T_s = getSpecies_T(TTrans,Tel,bElectron);
n   = getMixture_n(y_s,rho,Mm_s,nAvogadro);

p2D = repmat(p,[1,N_spec]);
n2D = repmat(n,[1,N_spec]);

fact = p2D./(n2D.*kBoltz.*T_s);

if options.bPairDiff
    fact = repmat(fact,[1,1,N_spec]);
end

end % getSpecies_fact4Dsl