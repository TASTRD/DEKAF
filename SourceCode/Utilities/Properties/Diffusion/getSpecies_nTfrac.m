function nTfrac_s = getSpecies_nTfrac(TTrans,Tel,rho,y_s,Mm_s,nAvogadro,bElectron)
% getSpecies_nTfrac computes the species fraction of number density and
% temperature needed for the definition of zeta.
%
% Usage:
%   (1)
%       nTfrac_s = getSpecies_nTfrac(TTrans,Tel,rho,y_s,Mm_s,nAvogadro,bElectron)
%
% Inputs and outputs:
%   TTrans
%       Translational temperature (N_eta x 1)
%   Tel
%       electron temperature (N_eta x 1)
%   rho
%       mixture density (N_eta x 1)
%   y_s
%       species mass fractions (N_eta x N_spec)
%   Mm_s
%       species molar mass (N_spec x 1)
%   nAvogadro
%       avogadro's constant (1 x 1)
%   bElectron
%       electron boolean (N_spec x 1)
%   nTfrac_s
%       species fraction of number density and temperature (N_eta x N_spec)
%
% Author(s): Fernando Miro Miro
% Date: May 2019
% GNU Lesser General Public License 3.0

N_spec = length(bElectron);

n_s = getSpecies_n(rho,y_s,Mm_s,nAvogadro); % computing species properties
T_s = getSpecies_T(TTrans,Tel,bElectron);

n       = n_s       *ones(N_spec,1); % summing over species
nTdenom = (n_s.*T_s)*ones(N_spec,1);

nTfrac_s = T_s .* repmat(n./nTdenom,[1,N_spec]); % assembling

end % getSpecies_nTfrac