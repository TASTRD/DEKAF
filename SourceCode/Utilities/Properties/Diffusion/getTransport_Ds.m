function [D_s] = getTransport_Ds(y_s,TTrans,Tel,rho,p,Mm,mu,kappa,cp,mixCnst,Sc_e,Le_e,options,varargin)
% getTransport_Ds computes the diffusion coefficient for any diffusion
% model implemented in DEKAF.
%
% Usage:
%   (1)
%       [D_s] = getTransport_Ds(y_s,TTrans,Tel,rho,p,Mm,mu,kappa,cp,mixCnst,Sc_e,Le_e,options)
%
%   (2)
%       [D_s] = getTransport_Ds(...,a)
%       |--> Allows to fix the scaling factor for the imposition of the
%       Lagrange condition for Stefan-Maxwell diffusion (see Magin)
%
% Inputs and Outputs:
%
%   y_s             N_eta x N_spec
%   TTrans          N_eta x 1
%   Tel             N_eta x 1
%   rho             N_eta x 1
%   p               N_eta x 1
%   Mm              N_eta x 1
%   mu              N_eta x 1
%   kappa           N_eta x 1
%   cp              N_eta x 1
%   mixCnst         structure
%   Sc_e            1 x 1
%   Le_e            1 x 1
%   options         structure
%
% References:
%   () Magin, T. E., & Degrez, G. (2004). Transport algorithms for
%   partially ionized and unmagnetized plasmas. Journal of Computational
%   Physics, 198(2), 424–449.
%
% Related functions:
% See also getAll_constants
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0


N_spec = size(y_s,2); % number of species

if options.bcstSc
    D_s = getSpecies_D_cstSchmidt(Sc_e,mu,rho,N_spec); % size (N_eta x N_spec)
elseif options.bcstLe
    D_s = getSpecies_D_cstLe(Le_e,rho,kappa,cp,N_spec); % size (N_eta x N_spec)
else
    % obtaining binary diffusion coefficients
    switch options.modelDiffusionBinary
        case 'FOCE'
            [D_sl_b,~] = getPair_nD_binary_FOCE(TTrans,Tel,y_s,rho,...
                mixCnst.consts_Omega11,mixCnst.Mm_s,...
                mixCnst.kBoltz,mixCnst.nAvogadro,mixCnst.bElectron,...
                mixCnst.qEl,mixCnst.epsilon0,mixCnst.bPos);
        case 'GuptaYos90'
            D_sl_b = getPair_D_binary_GuptaYos(p,TTrans,mixCnst.consts_Dbar_binary);
        otherwise
            error(['the chosen binary diffusion model ''',options.modelDiffusionBinary,''' is not supported.']);
    end
    % obtaining multicomponent diffusion coefficients
    switch options.modelDiffusionMulti
        case 'Exact'
            D_s  = getPair_D_multicomp_Exact(D_sl_b,y_s,TTrans,Tel,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.R0);
        case 'StefanMaxwell'
            D_s = getPair_D_multicomp_StefanMaxwell(D_sl_b,y_s,TTrans,Tel,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.bPos,mixCnst.qEl,mixCnst.kBoltz,false,varargin{:});
        case 'StefanMaxwellAmbipolar'
            D_s = getPair_D_multicomp_StefanMaxwell(D_sl_b,y_s,TTrans,Tel,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.bPos,mixCnst.qEl,mixCnst.kBoltz,true, varargin{:});
        case 'Ramshaw'
            [D_s,~] = getPair_D_multicomp_Ramshaw(D_sl_b,y_s,TTrans,Tel,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.R0,mixCnst.bPos,false);
        case 'RamshawAmbipolar'
            [D_s,~] = getPair_D_multicomp_Ramshaw(D_sl_b,y_s,TTrans,Tel,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.R0,mixCnst.bPos,true);
        case 'Effective'
            D_s = getSpecies_D_effective(D_sl_b,y_s,Mm,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.bPos,false);
        case 'EffectiveAmbipolar'
            D_s = getSpecies_D_effective(D_sl_b,y_s,Mm,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.bPos,true);
        case 'Klentzman'
            if N_spec > 2
                error('The Klentzman diffusion assumption only works for mixtures of two species!');
            end
            D_s = repmat(D_sl_b(:,1,2),[1,2]);
        case 'Binary'
            D_s = D_sl_b;
        otherwise
            error(['the chosen multicomponent diffusion model ''',options.modelDiffusionMulti,''' is not supported.']);
    end
end

% imposing correction of the diffusion coefficients for ionized mixtures in thermal non-equilibrium
if any(mixCnst.bElectron)
    fact = getSpecies_fact4Dsl(TTrans,Tel,y_s,p,rho,mixCnst.bElectron,mixCnst.Mm_s,mixCnst.nAvogadro,mixCnst.kBoltz,options);
    D_s = D_s.*fact;
end

end