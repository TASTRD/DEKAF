function D_s = getSpecies_D_Fick(D_sl,y_s,Mm,Mm_s)
%GETSPECIES_D_FICK calculates the species-mixture diffusion coefficient
% averaging the binary diffusion coefficient.
%
% Usage:
%   D_s = getSpecies_D_Fick(D_sl,y_s,Mm,Mm_s);
%
% Inputs and Outputs:
%   D_sl        multicomponent diffusion coefficient
%               N_eta x N_spec x N_spec
%   y_s         N_eta x N_spec
%   Mm          N_eta x 1
%   Mm_s        N_eta x N_spec
%
% References:
% Schrooyen, P. (2015). Numerical simulation of aerothermal flows through
% ablative thermal protection systems. UCL. See equation 2.33.
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);

X_s2D = getSpecies_X(y_s,Mm,Mm_s);
X_l3D = repmat(reshape(X_s2D,[N_eta,1,N_spec]),[1,N_spec,1]);

% Sum X_l / D_sl without l = s
lSum = zeros(N_eta,N_spec);
for s=1:N_spec
    lNotS = 1:N_spec;
    lNotS(s) = [];
    sumand = X_l3D(:,s,lNotS)./D_sl(:,s,lNotS);
    lSum(:,s) = reshape(sumand,[N_eta,N_spec-1])*ones(N_spec-1,1);
end

% Equation 2.33 from SchrooyenThesis 2015
D_s = (1 - y_s) ./ lSum;

end % getSpecies_D_Fick.m
