function [kCharge_s,sKCharge] = getSpecies_kCharge(TTrans,y_s,Mm_s,bPos,bElectron,qEl,kBoltz)
% getSpecies_kCharge computes the grouping parameter k appearing in the
% expression for the diffusion fluxes.
%
% Usage:
%   (1)
%       kCharge_s = getSpecies_kCharge(TTrans,y_s,Mm_s,bPos,bElectron,qEl,kBoltz)
%
%   (2)
%       [kCharge_s,sKCharge] = getSpecies_kCharge(TTrans,y_s,Mm_s,bPos,bElectron,qEl,kBoltz)
%
% Inputs and outputs:
%   TTrans
%       translational temperature (N_eta x 1)
%   y_s
%       mass fractions (N_eta x N_spec)
%   Mm_s
%       mixture molar mass (N_spec x 1)
%   bPos
%       positively-charged-species boolean (N_spec x 1)
%   bElectron
%       electron boolean (N_spec x 1)
%   qEl
%       charge of an electron (1 x 1)
%   kBoltz
%       Boltzmann constant (1 x 1)
%   kCharge_s
%       grouping parameter k appearing in the expression for the diffusion
%       fluxes (N_eta x N_spec)
%   sKCharge
%       scaling parameter to be used for the evaluation of the
%       Stefan-Maxwell diffusion fluxes
%
% References:
%   () Magin, T. E., & Degrez, G. (2004). Transport algorithms for
%   partially ionized and unmagnetized plasmas. Journal of Computational
%   Physics, 198(2), 424–449. https://doi.org/10.1016/j.jcp.2004.01.012
%
% Author(s): Fernando Miro Miro
% Date: May 2019
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);

ZCharge_s = getSpecies_ZCharge(bPos,bElectron); % computing species properties
Mm = getMixture_Mm_R(y_s,[],[],Mm_s,[],[]);
X_s = getSpecies_X(y_s,Mm,Mm_s);

% reshaping
ZCharge_s = repmat(ZCharge_s.',[N_eta,1]);      % (s,1) --> (eta,s)
TTrans = repmat(TTrans,[1,N_spec]);             % (eta,1) --> (eta,s)


XZ = (X_s.*ZCharge_s) * ones(N_spec,1);         % summing over species
XZ = repmat(XZ,[1,N_spec]);                     % (eta,1) --> (eta,s)

kCharge_s = qEl * (X_s.*ZCharge_s - y_s.*XZ) ./ (TTrans*kBoltz);
sKCharge = max(kCharge_s,[],2);

end % getSpecies_kCharge