function [varargout] = getMixture_constants(mixture,pAtm,R0,options)
% GETMIXTURE_CONSTANTS Return the necessary constants defining a
%   mixture's chemical reactions.
%
% [nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,...
%     Keq_struc,reac_list,reac_list_eq,idx_bath,idx_bathElement,qf2T_r,qb2T_r,...
%         gasSurfReac_struct] = getMixture_constants(mixture,pAtm,R0,options)
%
% Inputs:
%   mixture
%       string identifying the gas mixture according to the options
%       available in getMixture_constants 
%   pAtm
%       standard atmospheric pressure, 101325 Pa, scalar
%   R0
%       universal gas constant, 8.3144598 J/(K-mol), scalar
%
% Outputs:
%   nu_reac             [-]                                 (N_spec x N_reac)
%       stoichiometric coefficient of each species as a reactant ofeach
%       chemical reaction. 
%   nu_prod             [-]                                 (N_spec x N_reac)
%       toichiometric coefficient of each species as a product ofeach
%       chemical reaction. 
%   delta_nuEq          [-]                                 (N_spec x N_reacEq)
%       stoichiometric coefficient of each species as a product of the
%       equilibrium conditions. 
%   A_r                 [(m^3/mol)^(sum(nu_reac)-1) * K^(1/nT_r)]   (N_reac x 1) 
%       reaction rate preexponential constant.
%   nT_r                [-]                                 (N_reac x 1)
%       reaction rate exponential constant
%   theta_r             [K]                                 (N_reac x 1)
%       reaction activation temperature
%   N_spec              [-]                                 (1 x 1)
%       number of species in the reactions
%   spec_list           [-]                                 (N_spec x 1)
%       cell array of strings whose entries are the names of the species in
%       the chemical reactions 
%   Keq_struc           [-]
%       structure with the required inputs for the equilibrium-constant
%       computation see <a href="matlab:help getReaction_lnKeq">getReaction_lnKeq</a>
%   reac_list           [-]                                 (N_reac x 1)
%       cell of strings with the different reactions
%   reac_list_eq        [-]                                 (N_reacEq x 1)
%       cell of strings with the different equilibrium conditions
%   idx_bath            [-]                                 (1 x 1)
%       integer, index corresponding to the "general" bath
%                       species of the mixture
%   idx_bathElement     [-]                                 (1 x 1)
%       integer, index corresponding to the "general" bath element of the
%       mixture
%   qf2T_r, qb2T_r      [-]                                 (2 x N_reac)
%       exponentials on the different temperatures in a 2T thermal model
%       for the geometric average returning the temperature at which each
%       of the forward and backward reaction rates should be evaluated. The
%       order of the temperatures is (T, Tv).
%   gasSurfReac_struct  [-]
%       structure containing those variables needed for the gas-surface
%       interaction modeling. It may contain: 
%           nuGS_reac   [-]                             (N_spec x N_GSreac)
%               stoichiometric coefficients of the reactants in gas-surface
%               reactions
%           nuGS_prod   [-]                             (N_spec x N_GSreac)
%               stoichiometric coefficients of the products in gas-surface
%               reactions
%           gasSurfReac_type    [-]                     (N_GSreac x 1)
%               cell of strings containing the type of reaction
%               corresponding to each of the reactions detailed by
%               nuGS_reac and nuGS_prod. Types are detailed in
%               getSurface_source
%           A_GSr       [(m^3/mol)^(sum(nuGS_reac)-1) * K^(1/nT_GSr)]    (N_GSreac x 1)
%               vector with the reaction-rate preexponential constant for
%               Arrhenius-type surface reactions. It is zero for
%               non-Arrhenius reactions
%           nT_GSr      [-]                             (N_GSreac x 1)
%               vector with the reaction-rate exponential constant for
%               Arrhenius-type surface reactions. It is zero for
%               non-Arrhenius reactions
%           theta_GSr   [K]                             (N_GSreac x 1)
%               vector with the reaction-rate activation temperatures for
%               Arrhenius-type surface reactions. It is zero for
%               non-Arrhenius reactions
%           sublSpec_bool [-]                           (N_spec x 1)
%               vector of booleans keeping track of whether each of the
%               species in the mixture will sublimate from the surface.
%           oxidPark76_idx [-]                          (N_GSreac x 1)
%               vector of positions corresponding to the oxidizing species
%               for each of the Park76 reactions.
%           nuCat_reac  [-]                             (N_spec x N_Catreac)
%               stoichiometric coefficients of the reactants in catalytic
%               reactions
%           nuCat_prod  [-]                             (N_spec x N_Catreac)
%               stoichiometric coefficients of the products in catalytic
%               reactions
%           catReac_list [-]                            (N_Catreac x 1)
%               cell of strings with the catalytic reactions to consider
%
% The available mixtures are:
%   'Air'
%       non-reacting 1-species air (Air)
%   'Ar'
%       non-reacting 1-species argon
%   'N2'
%       non-reacting 1-species nitrogen
%   'Air-He'
%       non-reacting 2-species air-Helium (Air-He)
%   'Air-Ne'
%       non-reacting 2-species air-Neon (Air-Ne)
%   'Air-Ar'
%       non-reacting 2-species air-Argon (Air-Ar)
%   'Air-CO2'
%       non-reacting 2-species air-CO2 (Air-CO2)
%   'air2'
%       non-reacting 2-species air (O2,N2)
%   'air2-He'
%       non-reacting 3-species air-He (O2,N2,He)
%   'air2-Ne'
%       non-reacting 3-species air-Ne (O2,N2,Ne)
%   'air2-Ar'
%       non-reacting 3-species air-Ar (O2,N2,Ar)
%   'air2-CO2'
%       non-reacting 3-species air-CO2 (O2,N2,CO2)
%   'oxygen2Park01' -------> <a href="matlab:help getMixture_constants_oxygen2Park01">getMixture_constants_oxygen2Park01</a>
%       dissociating oxygen (O,O2)
%   'oxygen2Bortner' ------> <a href="matlab:help getMixture_constants_oxygen2Bortner">getMixture_constants_oxygen2Bortner</a>
%       dissociating oxygen (O,O2)
%   'air5Park85' ----------> <a href="matlab:help getMixture_constants_air5Park85">getMixture_constants_air5Park85</a>
%       5-species air (N,O,NO,N2,O2)
%   'air5Park90' ----------> <a href="matlab:help getMixture_constants_air5Park90">getMixture_constants_air5Park90</a>
%       5-species air (N,O,NO,N2,O2)
%   'air5Stuckert91' ------> <a href="matlab:help getMixture_constants_air5Stuckert91">getMixture_constants_air5Stuckert91</a>
%       5-species air (N,O,NO,N2,O2)
%   'air5Bortner' ---------> <a href="matlab:help getMixture_constants_air5Bortner">getMixture_constants_air5Bortner</a>
%       5-species air (N,O,NO,N2,O2)
%   'air5Park01' ----------> <a href="matlab:help getMixture_constants_air5Park01">getMixture_constants_air5Park01</a>
%       5-species air (N,O,NO,N2,O2) model
%   'air11Park93' ---------> <a href="matlab:help getMixture_constants_air11Park93">getMixture_constants_air11Park93</a>
%       11-species air (N,O,N2,O2,NO,N+,O+,N2+,O2+,NO+,e-)
%   'air11Park91noIoniz' --> <a href="matlab:help getMixture_constants_air11Park91noIoniz">getMixture_constants_air11Park91noIoniz</a>
%       Same as air11Park91, only without the ionization reactions.
%   'air11Park01' ---------> <a href="matlab:help getMixture_constants_air11Park01">getMixture_constants_air11Park01</a>
%       11-species air (e-,N,N+,O,O+,NO,N2,N2+,O2,O2+,NO+)
%   'mars5Park94' ---------> <a href="matlab:help getMixture_constants_mars5Park94">getMixture_constants_mars5Park94</a>
%       5-species mars (O,C,CO,O2,CO2)
%   'airC6Mortensen' ------> <a href="matlab:help getMixture_constants_airC6Mortensen">getMixture_constants_airC6Mortensen</a>
%       6-species air with CO2 (CO2,N,O,NO,N2,O2)
%   'mars5Mortensen' ------> <a href="matlab:help getMixture_constants_mars5Mortensen">getMixture_constants_mars5Mortensen</a>
%       5-species mars atmosphere (CO2,CO,C,O,O2)
%   'mars8Mortensen' ------> <a href="matlab:help getMixture_constants_mars8Mortensen">getMixture_constants_mars8Mortensen</a>
%       8-species mars atmosphere (CO2,CO,C,N,O,NO,N2,O2)
%   'airC11Mortensen' -----> <a href="matlab:help getMixture_constants_airC11Mortensen">getMixture_constants_airC11Mortensen</a>
%       11-species air with carbon species (C3,CO2,C2,CO,CN,C,N,O,NO,N2,O2)
%
% Children and related functions:
% See also setDefaults, ListOfVariablesExplained, generateStoichiometricMatrices
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% DEVELOPERS NOTES:
%   1)  The coefficients for the geometric averaging of the temperatures
%       (qf2T_r, qb2T_r, etc.) are defined inside the switch for all
%       temperatures except for the "bath temperature" (the one obtained
%       from total energy conservation). The remaining coefficient is
%       obtained from doing 1-sum() of all the others.
%   2)  The values for these qf2T_r and qb2T_r coefficients are obtained
%       following table 2.1 (pag. 36) from Scoggins, J. B. (2017).
%       Development of numerical methods and study of coupled flow,
%       radiation, and ablation phenomena for atmospheric entry. Université
%       Paris-Saclay and VKI.

N_out = 16;
% Current list:
%  1. nu_reac
%  2. nu_prod
%  3. delta_nuEq
%  4. A_r
%  5. nT_r
%  6. theta_r
%  7. N_spec
%  8. spec_list
%  9. Keq_struc
% 10. reac_list
% 11. reac_list_eq
% 12. idx_bath
% 13. idx_bathElement
% 14. qf2T_r
% 15. qb2T_r
% 16. gasSurfReac_struct

switch mixture
    case 'Air';                 [varargout{1:N_out}] = getMixture_constants_nonReactive(1);
        % The Air model works with 1 air species
        varargout{8} = {'Air'}; % spec_list
        varargout{12} = 1;      % idx_bath:        Air
        varargout{13} = 1;      % idx_bathElement: Air
    case 'Ar';                  [varargout{1:N_out}] = getMixture_constants_nonReactive(1);
        % The Ar model works with 1 argon species
        varargout{8} = {'Ar'};  % spec_list
        varargout{12} = 1;      % idx_bath:        Ar
        varargout{13} = 1;      % idx_bathElement: Ar
    case 'N2';                  [varargout{1:N_out}] = getMixture_constants_nonReactive(1);
        % The N2 model works with 1 nitrogen species
        varargout{8} = {'N2'};  % spec_list
        varargout{12} = 1;      % idx_bath:        N2
        varargout{13} = 1;      % idx_bathElement: N
    case 'air2';                [varargout{1:N_out}] = getMixture_constants_nonReactive(2);
        % The air2 model works with 2 air species (N2,O2)
        varargout{8} = {'O2','N2'}; % spec_list
        varargout{12} = 2;          % idx_bath:        N2
        varargout{13} = 1;          % idx_bathElement: N
    case 'air2-He';             [varargout{1:N_out}] = getMixture_constants_nonReactive(3);
        % The air2-He model works with 3 species (N2,O2,He)
        varargout{8} = {'O2','N2','He'};    % spec_list
        varargout{12} = 2;                  % idx_bath:        N2
        varargout{13} = 2;                  % idx_bathElement: N
    case 'air2-Ne';             [varargout{1:N_out}] = getMixture_constants_nonReactive(3);
        % The air2-Ne model works with 3 species (N2,O2,Ne)
        varargout{8} = {'O2','N2','Ne'};    % spec_list
        varargout{12} = 2;                  % idx_bath:        N2
        varargout{13} = 1;                  % idx_bathElement: N
    case 'air2-Ar';             [varargout{1:N_out}] = getMixture_constants_nonReactive(3);
        % The air2-Ar model works with 3 species (N2,O2,Ar)
        varargout{8} = {'O2','N2','Ar'};    % spec_list
        varargout{12} = 2;                  % idx_bath:        N2
        varargout{13} = 2;                  % idx_bathElement: N
    case 'air2-CO2';            [varargout{1:N_out}] = getMixture_constants_nonReactive(3);
        % The air2-CO2 model works with 3 species (N2,O2,CO2)
        varargout{8} = {'O2','N2','CO2'};   % spec_list
        varargout{12} = 2;                  % idx_bath:        N2
        varargout{13} = 2;                  % idx_bathElement: N
    case 'Air-He';              [varargout{1:N_out}] = getMixture_constants_nonReactive(2);
        % The Air-He model works with 2 air species (Air,He)
        varargout{8} = {'Air','He'};    % spec_list
        varargout{12} = 1;              % idx_bath:        Air
        varargout{13} = 1;              % idx_bathElement: Air
    case 'Air-Ne';              [varargout{1:N_out}] = getMixture_constants_nonReactive(2);
        % The Air-Ne model works with 2 air species (Air,Ne)
        varargout{8} = {'Air','Ne'};    % spec_list
        varargout{12} = 1;              % idx_bath:        Air
        varargout{13} = 1;              % idx_bathElement: Air
    case 'Air-Ar';              [varargout{1:N_out}] = getMixture_constants_nonReactive(2);
        % The Air-Ar model works with 2 air species (Air,Ar)
        varargout{8} = {'Air','Ar'};    % spec_list
        varargout{12} = 1;              % idx_bath:        Air
        varargout{13} = 1;              % idx_bathElement: Air
    case 'Air-CO2';             [varargout{1:N_out}] = getMixture_constants_nonReactive(2);
        % The Air-CO2 model works with 2 air species (Air,CO2)
        varargout{8} = {'Air','CO2'};   % spec_list
        varargout{12} = 1;              % idx_bath:        Air
        varargout{13} = 1;              % idx_bathElement: Air
    case 'oxygen2Park01';       [varargout{1:N_out}] = getMixture_constants_oxygen2Park01();
    case 'oxygen2Bortner';      [varargout{1:N_out}] = getMixture_constants_oxygen2Bortner();
    case 'air5Park85';          [varargout{1:N_out}] = getMixture_constants_air5Park85();
    case 'air5Park90';          [varargout{1:N_out}] = getMixture_constants_air5Park90();
    case 'air5Bortner';         [varargout{1:N_out}] = getMixture_constants_air5Bortner();
    case 'air5Stuckert91';      [varargout{1:N_out}] = getMixture_constants_air5Stuckert91();
    case 'air5Park01';          [varargout{1:N_out}] = getMixture_constants_air5Park01();
    case 'air11Park93';         [varargout{1:N_out}] = getMixture_constants_air11Park93();
    case 'air11Park93noIoniz';  [varargout{1:N_out}] = getMixture_constants_air11Park93noIoniz();
    case 'air11Park93Bose';     [varargout{1:N_out}] = getMixture_constants_air11Park93Bose();
    case 'air11Park01';         [varargout{1:N_out}] = getMixture_constants_air11Park01();
    case 'mars5Park94';         [varargout{1:N_out}] = getMixture_constants_mars5Park94();
    case 'airC6Mortensen';      [varargout{1:N_out}] = getMixture_constants_airC6Mortensen();
    case 'mars8Mortensen';      [varargout{1:N_out}] = getMixture_constants_mars8Mortensen();
    case 'mars5Mortensen';      [varargout{1:N_out}] = getMixture_constants_mars5Mortensen();
    case 'airC11Mortensen';     [varargout{1:N_out}] = getMixture_constants_airC11Mortensen(options.numberOfTemp);
    otherwise
        error(['the chosen mixture ''',mixture,''' is not a valid option.']);
end

% applying enhancement factor for the reaction rates
varargout{4} = varargout{4}(:) .* options.enhanceChemistry(:);  % A_r
varargout{9}.pAtm = pAtm;                                       % Keq_struc
varargout{9}.R0 = R0;

% Computing exponential of the "bath temperature" for the geometric average
switch options.numberOfTemp
    case '1T'
        varargout{14} = ones(size(varargout{14}));              % qb2T_r
        varargout{15} = ones(size(varargout{15}));              % qf2T_r
    case '2T'
        varargout{14} = [ones(size(varargout{14})) - varargout{14} ; varargout{14}]; % qb2T_r
        varargout{15} = [ones(size(varargout{15})) - varargout{15} ; varargout{15}]; % qf2T_r
    otherwise
        error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
end

end % getMixture_constants
