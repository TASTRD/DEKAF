function [hSens298_solid,solid_spec,gas_spec] = getSpecies_solids(formReac_s)
% getSpecies_solids gets the sensible enthalpy of solid species at 298K
% (needed to compute the heat of formation of certain species at 0K)
%
% Examples:
%  (1) hSens298_solid = getSpecies_solids(formReac_s)
%   |-> returns formation enthalpies for the solid species appearing in the
%   formation reactions passed by formReac_s
%
%  (2) [hSens298_solid,solid_spec] = getSpecies_solids(formReac_s)
%   |-> returns also a list of strings with the names of the solid species
%   corresponding to the different enthalpies
%
%  (3) [hSens298_solid,solid_spec,gas_spec] = getSpecies_solids(formReac_s)
%   |-> returns also a list of strings with the gaseous species with the
%   same molar mass as the solid species in solid_spec. gas_spec is
%   therefore the sublimated version of the different solid_spec.
%
% References:
% - Picard 2007. Determination of the specific heat capacity of a graphite
% sample using absolute and differential methods
%
% See also: getSpecies_HForm0K
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% List of all solid species currently considered by DEKAF
solid_list = {'Cgr',...         % carbon graphite
    };

solid_spec = {}; % initializing
gas_spec = {};
hSens298_solid = [];
for ii = 1:length(solid_list)
    if nnz(~cellfun(@isempty,regexp(formReac_s,solid_list,'once'))) % if the solid species is in any of the formation reactions
        switch solid_list{ii}                                           % depending on the species it is, we will define its sensible enthalpy at 298K
            case 'Cgr'                                                      % graphite
                cp = 716.8;                                                     % [J/kg-K] assuming constant heat capacity
                hSens298_Cgr = cp*298;                                          % computing sensible enthalpy
                hSens298_solid = [hSens298_solid;hSens298_Cgr];                 % attatching sensible enthalpy
                gas_spec = [gas_spec,'C'];                                      % Graphite corresponds to atomic carbon
            otherwise
                error(['the chosen solid species ''',solid_list{ii},''' is not supported']);
        end
        solid_spec = [solid_spec,solid_list{ii}];
    end
end