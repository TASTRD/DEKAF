function varargout = getMixture_kappa_EuckenWilke(y_s,mu_s,TTrans,Tel,cvTrans_s,cvRot_s,cvVib_s,cvElec_s,Mm_s,R0,bElectron,varargin)
% getMixture_kappa_EuckenWilke Calculate the mixture thermal conductivity
%
% Usage:
%   (1)
%       kappa = getMixture_kappa_EuckenWilke(y_s,mu_s,TTrans,Tel,cvTrans_s,...
%                      cvRot_s,cvVib_s,cvElec_s,Mm_s,R0,bElectron)
%
%   (2)
%       [kappa,kappav] = getMixture_kappa_EuckenWilke(...,'2T',bMol,options)
%       |--> obtains conductivities for a two-temperature model
%
% Inputs:
%   y_s         -   species mass fractions, matrix of size (N_eta x N_spec)
%   mu_s        -   species dynamic viscosity, matrix of size (N_eta x N_spec)
%   TTrans      -   translational temperature, vector of size (N_eta x 1)
%   Tel         -   electron temperature, vector of size (N_eta x 1)
%   cvTrans_s   -   translation specific heat at constant volume [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   cvRot_s     -   rotational specific heat at constant volume  [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   cvVib_s     -   vibrational specific heat at constant volume [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   cvElec_s    -   electronic specific heat at constant volume [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   R_s         -   species gas constant, matrix of size (N_eta x N_spec)
%   Mm_s        -   species mixture molar mass, matrix of size
%                   (N_eta x N_spec)
%   nAtoms_s    -   species number of atoms, matrix of size
%                   (N_eta x N_spec)
%   R0          -   universal gas constant
%   bElectron   -   electron boolean (N_spec x 1)
%
% Notes:
%   Apply Wilke's mixing rule and Eucken's relation
%
% References:
%   Wilke, C., "A Viscosity Equation for Gas Mixtures" The Journal of
%       Chemical Physics, Vol. 18, No. 4, 1950
%   A. Eucken, "�ber das W�rmeleitverm�gen, die spezifische W�rme und die
%       innere Reibung der Gase," Phys. Z, 1913
%
% Children and related functions:
%   See also getMixture_Mm_R.m, getSpecies_X.m, getSpecies_mu.m,
%            getSpecies_cv.m, getSpecies_kappa.m, applyWilkes.m
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

if isempty(varargin)
    numberOfTemp = '1T';
else
    numberOfTemp = varargin{1};
end

% mixture molar mass and mixture gas constant
Mm = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);

% species mole fraction
X_s = getSpecies_X(y_s,Mm,Mm_s);

% species thermal conductivity
[kappas_all{1:nargout}] = getSpecies_kappa_Eucken(mu_s,cvTrans_s,cvRot_s,cvVib_s,cvElec_s,numberOfTemp,bElectron);

% mixture thermal conductivity given by Wilke's mixing rule
bHeavy = ~bElectron; % heavy particles
switch numberOfTemp
    case '1T'
%         varargout{1}   = applyWilkes(X_s(:,bHeavy),kappas_all{1}(:,bHeavy),Mm_s(bHeavy));
        varargout{1} = applyWilkes(X_s,kappas_all{1},mu_s,Mm_s);
        varargout{2} = [];
    case '2T'
        options = varargin{3};
                                        varargout{1} = applyWilkes(X_s(:,bHeavy),kappas_all{1}(:,bHeavy),mu_s(:,bHeavy),Mm_s(bHeavy));
        if options.neglect_kappaEl;     varargout{2} = applyWilkes(X_s(:,bHeavy),kappas_all{2}(:,bHeavy),mu_s(:,bHeavy),Mm_s(bHeavy));
        else;                           varargout{2} = applyWilkes(X_s,          kappas_all{2},          mu_s,          Mm_s);
        end
    case '5T'
        bMol = varargin{2};
        options = varargin{3};
        varargout{1} = applyWilkes(X_s(:,bHeavy),kappas_all{1}(:,bHeavy),mu_s(:,bHeavy),Mm_s(bHeavy));     % kappaTrans
        varargout{2} = applyWilkes(X_s(:,bMol),  kappas_all{2}(:,bMol),  mu_s(:,bMol),  Mm_s(bMol));       % kappaRot
        varargout{3} = applyWilkes(X_s(:,bMol),  kappas_all{3}(:,bMol),  mu_s(:,bMol),  Mm_s(bMol));       % kappaVib
        varargout{4} = applyWilkes(X_s(:,bHeavy),kappas_all{4}(:,bHeavy),mu_s(:,bHeavy),Mm_s(bHeavy));     % kappaElec
        if options.neglect_kappaEl
            varargout{5} = zeros(size(TTrans));                                                 % kappael
        else
            varargout{5} = kappas_all{5}(:,bElectron);                                          % kappael
        end
    otherwise
        error(['the chosen number of temperatures ''',numberOfTemp,''' is not supported']);
end

end % getMixture_kappa_EuckenWilke