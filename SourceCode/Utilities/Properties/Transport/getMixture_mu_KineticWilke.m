function [mu,mu_s] = getMixture_mu_KineticWilke(TTrans,Tel,y_s,rho,p,Mm_s,bElectron,kBoltz,R0,nAvogadro,qEl,epsilon0,consts_Omega22,bPos)
% getMixture_mu_KineticWilke computes the mixture viscosity using Wilke's
% mixing rule on the species shear viscosity coefficients coming from
% kinetic theory.
%
% Usage:
%   (1)
%       [mu,mu_s] = getMixture_mu_KineticWilke(TTrans,Tel,y_s,rho,p,Mm_s,...
%                                   bElectron,kBoltz,R0,nAvogadro,qEl,...
%                                   epsilon0,consts_Omega22,bPos)
%
% Inputs and outputs:
%       TTrans          size N_eta x 1
%       Tel             size N_eta x 1
%       y_s             size N_eta x N_spec
%       rho             size N_eta x 1
%       p               size N_eta x 1
%       Mm_s            size N_spec x 1
%       bElectron       size N_spec x 1
%       kBoltz          size 1 x 1
%       R0              size 1 x 1
%       nAvogadro       size 1 x 1
%       qEl             size 1 x 1
%       epsilon0        size 1 x 1
%       Tred            size N_eta x N_spec x N_spec
%       consts_Omega22  struct with fields .A, .B, .C, .D, .E for the
%                       collision integral curve fit
%       bPos            size N_spec x 1
%       mu              size N_eta x 1
%       mu_s            size N_eta x N_spec
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s); % sizes
bHeavy = ~bElectron; % heavy particles

% Collision integrals
lnOmega22_sl   = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega22,bPos);
lnOmega22_sl2D = reshape(lnOmega22_sl,[N_eta,N_spec^2]);
lnOmega22_ss = lnOmega22_sl2D(:,1:(N_spec+1):N_spec^2); % taking the diagonal

% species shear viscosity coefficients
mu_s = getSpecies_muKinetic(TTrans,lnOmega22_ss,Mm_s,kBoltz,nAvogadro);
mu_s(:,bElectron) = zeros(N_eta,nnz(bElectron)); % removing electron viscosities

% mixture molar mass and mixture gas constant
Mm = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);

% mole fraction of each species
X_s = getSpecies_X(y_s,Mm,Mm_s);

% Wilke's mixing rule
mu = applyWilkes(X_s(:,bHeavy),mu_s(:,bHeavy),mu_s(:,bHeavy),Mm_s(bHeavy));