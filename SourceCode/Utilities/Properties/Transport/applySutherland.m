function [val,val_T,val_TT] = applySutherland(T,prop_ref,T_ref,Su)
% APPLYSUTHERLAND(T,prop_ref,T_ref,Su) computes the required transport
% property (viscosity or thermal conductivity) using Sutherland's law.
%
% Usage:
%   (1)
%       prop = applySutherland(T,prop_ref,T_ref,Su)
%
%   (2)
%       [prop,dprop_dT,dprop_dT2] = applySutherland(T,prop_ref,T_ref,Su)
%       |--> returns also the temperature derivatives
%
% The inputs must be:
%   - T: temperature at which the property must be evaluated (N_eta x 1)
%   - prop_ref: reference value of the property to evaluate (N_spec x 1)
%   - T_ref: reference temperature (N_spec x 1)
%   - Su: coefficient in sutherland's formula (N_spec x 1)
%   - prop: computed property  (N_eta x N_spec)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_spec = length(prop_ref);
N_eta = length(T);

prop_ref    = repmat(prop_ref(:).'  ,[N_eta,1]);
T_ref       = repmat(T_ref(:).'     ,[N_eta,1]);
Su          = repmat(Su(:).'        ,[N_eta,1]);
T           = repmat(T              ,[1,N_spec]);

cst = prop_ref .* (T_ref + Su)./(T_ref.^(3/2));

val = cst .* T.^(3/2)./(T+Su);
if nargout>1
    val_T = cst.*(3/2*T.^(1/2) .* 1./(T+Su) - T.^(3/2)./((T+Su).^2));
end
if nargout>2
    df = 3/2.*(1/2*T.^(-1/2) .* 1./(T+Su) - T.^(1/2)./((T+Su).^2));
    dg = -(3/2.*T.^(1/2) .* 1./(T+Su).^2 - 2.*T.^(3/2)./((T+Su).^3));
    val_TT = cst.*(df+dg);
end