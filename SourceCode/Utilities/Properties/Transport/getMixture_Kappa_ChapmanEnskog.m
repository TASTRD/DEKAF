function [varargout] = getMixture_Kappa_ChapmanEnskog(y_s,TTrans,TRot,TVib,TElec,Tel,rho,p,...
                        Mm_s,Mm,R0,consts_Omega11,consts_Omega22,nAvogadro,...
                        kBoltz,bElectron,qEl,epsilon0,bPos,consts_Bstar,consts_Cstar,...
                        consts_Estar,consts_Omega14,consts_Omega15,consts_Omega24,...
                        nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,hForm_s,options,varargin)
% getMixture_Kappa_ChapmanEnskog computes the mixture frozen thermal
% conductivity using the best approximation for each of its components.
%
% Usage:
%   (1)
%       kappa =  getMixture_Kappa_ChapmanEnskog(y_s,TTrans,TRot,TVib,TElec,Tel,rho,...
%               Mm_s,Mm,R0,consts_Omega11,consts_Omega22,nAvogadro,...
%               kBoltz,bElectron,qEl,epsilon0,bPos,consts_Bstar,consts_Cstar,...
%               consts_Estar,consts_Omega14,consts_Omega15,consts_Omega24,...
%               nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,hForm_s,options);
%
%   (2)
%       [kappa,kappaHeavy,kappaInt,kappaEl,kappaRot,kappaVib,kappaElec] =
%                                                       getMixture_Kappa_ChapmanEnskog(...);
%
%   (3)
%       [...] = getMixture_Kappa_ChapmanEnskog(...,AThermFuncs_s);
%       |--> usage for polynomial thermal models ('NASA7' or
%       'customPoly7'), where the coefficients of the polynomials must be
%       passed.
%
% Inputs:
%   y_s            mass fractions of species s                          [-]
%                  matrix of size (N_eta x N_spec)
%   TTrans         translational temperature                            [K]
%                  vector of size (N_eta x 1)
%   TRot           rotational temperature                               [K]
%                  vector of size (N_eta x 1)
%   TVib           vibrational temperature                              [K]
%                  vector of size (N_eta x 1)
%   TElec          electronic temperature                               [K]
%                  vector of size (N_eta x 1)
%   Tel            electron temperature                                 [K]
%                  vector of size (N_eta x 1)
%   rho            mixture density                                  [kg/m3]
%                  vector of size (N_eta x 1)
%   p              static pressure                                  [kg/m3]
%                  vector of size (N_eta x 1)
%   Mm_s           species molecular molar mass.                   [kg/mol]
%                  vector of size (N_spec x 1)
%   Mm             mixture molecular molar mass.                   [kg/mol]
%                  vector of size (N_eta x 1)
%   R0             Universal gas constant.                      [J/(K*mol)]
%
%   consts_[a-Z]**  Curve fit coefficients to calculate collision data.
%                   Struct with fields .A, .B, .C, .D where each field is a
%                   matrix of size (N_s x N_l).
%   nAvogadro       Avogradro's constant.                        [part/mol]
%   kBoltz          Boltzmann's constant
%   bElectron       electron / non-electron boolean size (N_spec x 1)
%   qEl             elementary charge (1 x 1)
%   epsilon0        permittivity of vacuum (1 x 1)
%   bPos            boolean for positively charged particles (N_spec x 1)
%   consts_Bstar    Curve fit coefficients to
%                   calculate Bstar for a given species pair (s,l).
%                   struct with fields .A, .B, .C, .D where each
%                   field is a matrix of size (N_s x N_l).
%   consts_Cstar    Curve fit coefficients to
%                   calculate Cstar for a given species pair (s,l).
%                   struct with fields .A, .B, .C, .D where each
%                   field is a matrix of size (N_s x N_l).
%   options         necessary fields
%           .modelThermal
%           .flow
%           .numberOfTemp
%           .neglect_hRot
%           .neglect_hVib
%           .neglect_hElec
%   AThermFuncs_s      (N_spec x 1)                                    [SI]
%       structured cell array with the anonymous functions for each
%       species. Each cell-array position contains N_coef anonymous
%       functions:
%           .A1, .A2, etc
%
% Outputs:
%
%   kappa           N_eta x 1
%   kappaHeavy      N_eta x 1
%   kappaInt        N_eta x 1
%   kappaEl         N_eta x 1
%   kappaRot        N_eta x 1
%   kappaVib        N_eta x 1
%   kappaElec       N_eta x 1
%
%
% See also: applyChapmanEnskog, getMixture_kappaInt_EuckenCorrection
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% Computing heavy-particle translational thermal conductivity with the
% second approximation to Chapman-Enskog
kappaHeavy = applyChapmanEnskog(22,y_s,TTrans,Tel,rho,p,Mm_s,Mm,consts_Omega11,...
                              consts_Omega22,nAvogadro,R0,kBoltz,bElectron,...
                               qEl,epsilon0,bPos,consts_Bstar,options);


% Computing internal thermal conductivities with Eucken's approximation
[kappaInt,kappaRot,kappaVib,kappaElec] = getMixture_kappaInt_EuckenCorrection(TTrans,TRot,...
                                            TVib,TElec,Tel,y_s,rho,p,R0,nAtoms_s,thetaVib_s,thetaElec_s,...
                                            gDegen_s,bElectron,bPos,hForm_s,nAvogadro,...
                                            consts_Omega11,Mm_s,kBoltz,qEl,epsilon0,options,varargin{:});

% Computing electron thermal conductivity with the third approximation to
% Chapman-Enskog
if nnz(bElectron)
    kappaEl = getElectron_kappa(3,TTrans,Tel,y_s,rho,p,Mm_s,bElectron,bPos,kBoltz,R0,nAvogadro,qEl,...
                    epsilon0,consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,consts_Estar,...
                    consts_Omega14,consts_Omega15,consts_Omega24);
else
    kappaEl = zeros(size(kappaHeavy));
end

% Assembling it all
kappa = kappaHeavy + kappaInt + kappaEl;

% preparing outputs
oc = 1;
varargout{oc} = kappa;      oc=oc+1;
varargout{oc} = kappaHeavy; oc=oc+1;
varargout{oc} = kappaInt;   oc=oc+1;
varargout{oc} = kappaEl;    oc=oc+1;
varargout{oc} = kappaRot;   oc=oc+1;
varargout{oc} = kappaVib;   oc=oc+1;
varargout{oc} = kappaElec; %oc=oc+1;
