function varargout = getElectron_Lambdaij(X_s,Tel,bElectron,Mm_s,kBoltz,nAvogadro,varargin)
% getElectron_Lambdaij computes the Lambdaij_ee terms needed in the Sonine
% expansion of the Chapman-Enskog expression for the electron thermal
% conductivity.
%
% Examples:
%   Lambda11_ee = getElectron_Lambdaij(X_s,Tel,bElectron,Mm_s,kBoltz,nAvogadro,...
%                                       Omega11_sl,Omega12_sl,Omega13_sl,Omega22_sl)
%
%   [Lambda11_ee,Lambda12_ee] = getElectron_Lambdaij(X_s,Tel,bElectron,Mm_s,...
%               kBoltz,nAvogadro,Omega11_sl,Omega12_sl,Omega13_sl,Omega14_sl,...
%               Omega22_sl,Omega23_sl)
%
%   [Lambda11_ee,Lambda12_ee,Lambda22_ee] = getElectron_Lambdaij(X_s,Tel,bElectron,...
%               Mm_s,kBoltz,nAvogadro,Omega11_sl,Omega12_sl,Omega13_sl,Omega14_sl,...
%               Omega15_sl,Omega22_sl,Omega23_sl,Omega24_sl)
%
% Inputs:
%   X_s             N_eta x N_spec
%   Tel             N_eta x 1
%   Omega11_sl      N_eta x N_spec x N_spec
%   Omega12_sl      N_eta x N_spec x N_spec
%   Omega13_sl      N_eta x N_spec x N_spec
%   Omega14_sl      N_eta x N_spec x N_spec
%   Omega15_sl      N_eta x N_spec x N_spec
%   Omega22_sl      N_eta x N_spec x N_spec
%   Omega23_sl      N_eta x N_spec x N_spec
%   Omega24_sl      N_eta x N_spec x N_spec
%   bElectron       N_spec x 1
%   Mm_s            N_spec x 1
%   kBoltz          N_spec x 1
%   nAvogadro       N_spec x 1
%
% References:
% Magin, T. E. and Degrez, G., "Transport of partially ionized and
%   unmagnetized plasmas," Physical Review E, Vol. 70, 2004.
% Scoggins, J. (2017). Development of numerical methods and study of
%   coupled flow, radiation, and ablation phenomena for atmospheric entry.
%   von Karman Institute.
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% obtaining inputs
switch length(varargin)
    case 4 % only asking for Lambda11_ee
        flag = 1;
        Omega11_sl = varargin{1};
        Omega12_sl = varargin{2};
        Omega13_sl = varargin{3};
        Omega22_sl = varargin{4};
    case 6 % asking for Lambda11_ee and Lambda12_ee
        flag = 2;
        Omega11_sl = varargin{1};
        Omega12_sl = varargin{2};
        Omega13_sl = varargin{3};
        Omega14_sl = varargin{4};
        Omega22_sl = varargin{5};
        Omega23_sl = varargin{6};
    case 8 % asking for Lambda11_ee, Lambda12_ee and Lambda22_ee
        flag = 3;
        Omega11_sl = varargin{1};
        Omega12_sl = varargin{2};
        Omega13_sl = varargin{3};
        Omega14_sl = varargin{4};
        Omega15_sl = varargin{5};
        Omega22_sl = varargin{6};
        Omega23_sl = varargin{7};
        Omega24_sl = varargin{8};
    otherwise
        error('wrong number of inputs');
end

% sizes
[N_eta,N_spec] = size(X_s);
N_heavy = (~bElectron') * ones(N_spec,1); % number of heavy particles

% passing from Omega11_sl to Q11_eH (see Magin 2004)
Q11_eH = reshape(Omega11_sl(:,bElectron,~bElectron)*pi,[N_eta,N_heavy]); % keeping only heavy particles
Q12_eH = reshape(Omega12_sl(:,bElectron,~bElectron)*pi,[N_eta,N_heavy]);
Q13_eH = reshape(Omega13_sl(:,bElectron,~bElectron)*pi,[N_eta,N_heavy]);
Q22_ee = Omega22_sl(:,bElectron,bElectron)*pi;
if flag>=2
    Q14_eH = reshape(Omega14_sl(:,bElectron,~bElectron)*pi,[N_eta,N_heavy]);
    Q23_ee = Omega23_sl(:,bElectron,bElectron)*pi;
end
if flag>=3
    Q15_eH = reshape(Omega15_sl(:,bElectron,~bElectron)*pi,[N_eta,N_heavy]);
    Q24_ee = Omega24_sl(:,bElectron,bElectron)*pi;
end

% other intermediate variables required
X_e = X_s(:,bElectron);             % electron mole fraction
X_sH = X_s(:,~bElectron);           % heavy-particle mole fraction
m_e = Mm_s(bElectron)/nAvogadro;    % electron weight

% computing lambdas (Magin 2004, Appendix Equation C.3c)
sumand = X_sH.*(25/4*Q11_eH - 15*Q12_eH + 12*Q13_eH);
Lambda11_ee = 64*X_e/(75*kBoltz) .* sqrt(m_e./(2*pi*kBoltz*Tel)) .* (sumand*ones(N_heavy,1) + sqrt(2)*X_e.*Q22_ee);
varargout{1} = Lambda11_ee;

if flag>=2
    %  (Magin 2004, Appendix Equation C.3e)
    sumand = X_sH.*(175/16*Q11_eH - 315/8*Q12_eH + 57*Q13_eH - 30*Q14_eH);
    Lambda12_ee = 64*X_e/(75*kBoltz) .* sqrt(m_e./(2*pi*kBoltz*Tel)) .* (sumand*ones(N_heavy,1) + sqrt(2)*X_e.*(7/4*Q22_ee - 2*Q23_ee));
    varargout{2} = Lambda12_ee;
end

if flag>=3
    % (Magin 2004, Appendix Equation C.3f)
    sumand = X_sH.*(1225/64*Q11_eH - 735/8*Q12_eH + 399/2*Q13_eH - 210*Q14_eH + 90*Q15_eH);
    Lambda22_ee = 64*X_e/(75*kBoltz) .* sqrt(m_e./(2*pi*kBoltz*Tel)) .* (sumand*ones(N_heavy,1) + sqrt(2)*X_e.*(77/16*Q22_ee - 7*Q23_ee + 5*Q24_ee));
    varargout{3} = Lambda22_ee;
end
