function [mu,dmu_dT] = ...
    getMixture_mu_Brokaw58(TTrans,Tel,y_s,rho,p,Mm_s,R0,nAvogadro,consts_Omega22,...
    bElectron,kBoltz,qEl,epsilon0,bPos)
%getMixture_mu_Brokaw58 Calculate the mixture dynamic viscosity with the
% approximation given by Gupta Yos et al, 1990.
%
% Usage:
% 1. Calculate the mixture dynamic viscosity, mu
%   mu = ...
%       getMixture_mu_Brokaw58(TTrans,Tel,y_s,rho,p,Mm_s,R0,nAvogadro,...
%           consts_Omega22,bElectron,kBoltz,qEl,...
%           epsilon0,bPos);
%
%
% Inputs:
%   TTrans              translational temperature [K] (N_eta x 1)
%   Tel                 electron temperature [K] (N_eta x 1)
%   y_s                 mass fractions (N_eta x N_spec)
%   rho                 mixture density [kg/m3] (N_eta x 1)
%   p                   static pressure [Pa] (N_eta x 1)
%   Mm_s                molecular molar mass [kg/mol] (N_spec x 1)
%   R0                  universal gas constant [J/mol-K]
%   nAvogadro           avogadro's constant
%   consts_Omega22      struct whose fields define the curve fit
%                       coefficients for the calculation of Omega22
%                       each field is (N_eta x N_s x N_l)
%   bElectron           electron / non-electron boolean size (N_spec x 1)
%   qEl                 elementary charge (1 x 1)
%   epsilon0            permittivity of vacuum (1 x 1)
%   bPos                boolean for positively charged species (N_spec x 1)
%
% Outputs:
%   mu                  dynamic viscosity [kg/m-s](N_eta x 1)
%
% Notes:
%
%   Comments in this function refer to equations in the paper written by
%   Gupta, Yos et al., 1990. The citation is given below under the section,
%   "References".
%
% References:
% Gupta,  R. N.,  Yos,  J. M.,  Thompson,  R. A.,  and Lee,  K.-P.,  "A
%   Review of Reaction Rates and Thermodynamic and Transport Properties for
%   an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%   Calculations to 30000 K," Nasa-rp-1232, National Aeronautics and Space
%   Administration, 1990.
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);

% computing mole fraction
[Mm,~] = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);
X_s = getSpecies_X(y_s,Mm,Mm_s);

% reshaping
Mm_s2D = repmat(Mm_s',[N_eta,1,1]);     % N_eta x N_s
X_l3D  = repmat(reshape(X_s,[N_eta,1,N_spec]),[1,N_spec,1]);

% Get (2,2) collision integrals to calculate Delta2, as defined by Gupta,
% Yos et al., 1990.
lnOmega22_sl = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega22,bPos);
lnDelta2_sl  = ...
    getPair_Collision_Delta2(TTrans,Mm_s,R0,lnOmega22_sl);

Delta2_sl = exp(lnDelta2_sl);

% Equation (39)
lSumXDelta2 = zeros(N_eta,N_spec);
for l=1:N_spec
    lSumXDelta2 = lSumXDelta2 + X_l3D(:,:,l).*Delta2_sl(:,:,l);
end
mu = (Mm_s2D/nAvogadro.*X_s ./ lSumXDelta2) * ones(N_spec,1);

end % getMixture_mu_Brokaw58.m
