function varargout = getSpecies_kappa_Eucken(mu_s,cvTrans_s,cvRot_s,cvVib_s,cvElec_s,varargin)
%GETSPECIES_KAPPA_EUCKEN Calculate species thermal conductivity using
% Eucken's theory
%
% Usage:
%   (1)
%       kappa_s = getSpecies_kappa_Eucken(mu_s,cvTrans_s,cvRot_s,cvVib_s,...
%                                                                  cvElec_s)
%
%   (2)
%       [kappa_s,kappav_s] = getSpecies_kappa_Eucken(...,'2T',bElectron)
%       |--> obtains conductivities for a two-temperature model
%
%   (3)
%       [kappaTrans_s,kappaRot_s,kappaVib_s,kappaElec_s,kappael_s] = getSpecies_kappa_Eucken(...,'5T',bElectron)
%       |--> obtains conductivities for a five-temperature model
%
% Inputs:
%   mu_s        -   species dynamic viscosity,                   [kg/(m*s)]
%                   matrix of size (N_eta x N_spec)
%   cvTrans_s   -   translation specific heat at constant volume [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   cvRot_s     -   rotational specific heat at constant volume  [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   cvVib_s     -   vibrational specific heat at constant volume [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   cvElec_s    -   electronic specific heat at constant volume [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   bElectron   -   boolean vector true for electron species, false otherwise
%                   matrix of size (N_spec x 1)
%
% Outputs:
%   kappa_s     -   species thermal conductivity                  [W/(m*K)]
%   kappav_s    -   species vibrational-electronic thermal conductivity [W/(m*K)]
%   kappaTrans_s, kappaRot_s, kappaVib_s, kappaElec_s, kappael_s
%                   species thermal conductivity due to each energy mode [W/(m*K)]
%
% Notes:
% Eucken's relation, rooted in Chapman-Enskog theory, is used
%
% References:
%   A. Eucken, "�ber das W�rmeleitverm�gen, die spezifische W�rme and die
% innere Reibung der Gase", Physik Z., 14 (1913), pp. 324-332
%
% Children and related functions:
%   See also getMixture_kappa.m, getSpecies_cv.m listOfVariablesExplained
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

if isempty(varargin)
    numberOfTemp = '1T';
else
    numberOfTemp = varargin{1};
end

switch numberOfTemp
    case '1T'
        varargout{1} = mu_s.*(5/2*cvTrans_s + cvRot_s + cvVib_s + cvElec_s); % Eucken's relation
        varargout{2} = [];
    case '2T'
        N_eta = size(mu_s,1);
        bElectron = varargin{2};
        bHeavy2D    = repmat(~bElectron.',[N_eta,1]);
        bElectron2D = repmat( bElectron.',[N_eta,1]);
        varargout{1}  = mu_s.*(5/2*cvTrans_s.*bHeavy2D    + cvRot_s);            % translational-rotational conductivity
        varargout{2}  = mu_s.*(5/2*cvTrans_s.*bElectron2D + cvVib_s + cvElec_s); % vibrational-electronic-electron conductivity
    case '5T'
        N_eta = size(mu_s,1);
        bElectron = varargin{2};
        bHeavy2D    = repmat(~bElectron.',[N_eta,1]);
        bElectron2D = repmat( bElectron.',[N_eta,1]);
        varargout{1} = mu_s.*5/2.*cvTrans_s.*bHeavy2D;
        varargout{2} = mu_s.*cvRot_s;
        varargout{3} = mu_s.*cvVib_s;
        varargout{4} = mu_s.*cvElec_s;
        varargout{5} = mu_s.*5/2.*cvTrans_s.*bElectron2D;
    otherwise
        error(['the chosen number of temperatures ''',numberOfTemp,''' is not supported']);
end

end % getSpecies_kappa_Eucken