function eta_s = getSpecies_muKinetic(TTrans,lnOmega22_ss,Mm_s,kBoltz,nAvogadro)
% getSpecies_muKinetic computes the species shear viscosity coefficients.
%
% Usage:
%   (1)
%       eta_s = getSpecies_muKinetic(TTrans,lnOmega22_ss,Mm_s,kBoltz,nAvogadro)
%
% Inputs and outputs:
%   TTrans          - N_eta x 1
%   lnOmega22_ss    - N_eta x N_spec
%   Mm_s            - N_spec x 1
%   kBoltz          - 1 x 1
%   nAvogadro       - 1 x 1
%   eta_s           - N_eta x N_spec
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(lnOmega22_ss);    % obtaining sizes
TTrans = repmat(TTrans,[1,N_spec]);     % making all sizes the same
Mm_s = repmat(Mm_s.',[N_eta,1]);

eta_s = 5/16*sqrt(pi*kBoltz*TTrans.*Mm_s/nAvogadro)./(pi*exp(lnOmega22_ss)); % computing viscosities