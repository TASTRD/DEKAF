function [kappaInt,varargout] = getMixture_kappaInt_GuptaYos(X_s,...
    cpRot_s,cpVib_s,cpElec_s,Mm_s,R0,kBoltz,Delta1_sl)
%GETMIXTURE_KAPPAINTERAL_GUPTAYOS Calculate the internal energy thermal
% conductivity for distinct internal modes: rotational, vibrational, and
% electronic.
%
% FIXME: add derivative calculation to backend
%
% Usage:
% 1. Calculate the internal energy thermal conductivity of all three modes
%
%   kappaInt = ...
%       getMixture_kappaInt_GuptaYos(X_s,cpRot_s,cpVib_s,cpElec_s,...
%           Mm_s,R0,kBoltz,Delta1_sl);
%
% 2. Calculate the distinct components' thermal conductivities, as well
%    as the total internal energy thermal conductivity.
%
%   [kappaInt,kappaRot,kappaVib,kappaElec] = ...
%       getMixture_kappaInt_GuptaYos(X_s,cpRot_s,cpVib_s,cpElec_s,...
%           Mm_s,R0,kBoltz,Delta1_sl);
%
% Inputs:
%   X_s             -   mole fractions of species s                     [-]
%                       matrix of size (N_eta x N_s)
%   cpInt_s         -   internal mode component of cp of species s [J/kg-K]
%                       matrix of size (N_eta x N_s)
%   Mm_s            -   molecular molar mass of species s          [J/kg-K]
%                       matrix of size (N_s x 1)
%                       ...to be reshaped to (N_eta x N_s)
%   R0              -   Universal gas constant                    [J/mol-K]
%   kBoltz          -   Boltzmann's constant                          [J/K]
%   Delta1_sl       -   defined by Gupta 1990, Equation (34)          [m-s]
%                       matrix of size (N_eta x N_s x N_l)
%   bElectron       -   electron / non-electron boolean
%                       vector of size (N_spec x 1)
%
% Outputs:
%   kappaInt        -   internal mode component of thermal conductivity
%                       of species s                                [W/m-K]
%
% References:
% Gupta,  R. N.,  Yos,  J. M.,  Thompson,  R. A.,  and Lee,  K.-P.,  "A
%   Review of Reaction Rates and Thermodynamic and Transport Properties for
%   an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%   Calculations to 30000 K," Nasa-rp-1232, National Aeronautics and Space
%   Administration, 1990.
%
% Related functions:
%   See also applyGuptaYos.m, getMixture_R.m, getMixture_Mm_R.m
%            getSpecies_X.m
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(cpRot_s);

R_s = getSpecies_R(R0,Mm_s);        % N_s x 1
R_s2D = repmat(R_s',[N_eta,1]);     % N_eta x N_s

X_l = repmat(reshape(X_s,[N_eta,1,N_spec]),[1,N_spec,1]);

% Gupta 1990 Equation (38b), modified with R_univ replaced by R_s
% because in DEKAF, mass cp is used as opposed to molar cp.
lSum = zeros(N_eta,N_spec);
for l=1:N_spec
    lSum = lSum + X_l(:,:,l).*Delta1_sl(:,:,l);
end
kappaRot    = kBoltz * (cpRot_s.* X_s./R_s2D./lSum) * ones(N_spec,1);
kappaVib    = kBoltz * (cpVib_s.* X_s./R_s2D./lSum) * ones(N_spec,1);
kappaElec   = kBoltz * (cpElec_s.*X_s./R_s2D./lSum) * ones(N_spec,1);
kappaInt    = kappaRot + kappaVib + kappaElec;

if nargout>1
    oc = 1;                   % output counter
    varargout{oc} = kappaRot;   oc = oc+1;
    varargout{oc} = kappaVib;   oc = oc+1;
    varargout{oc} = kappaElec; %oc = oc+1;
end

end % getMixture_kappaInt_GuptaYos.m
