function [mu,varargout] = getTransport_mu_kappa(y_s,TTrans,Tel,rho,p,...
    Mm,options,mixCnst,varargin)
%GETTRANSPORT_MU_KAPPA Calculate transport properties of dynamic viscosity
%and thermal conductivity for any transport model implemented in DEKAF.
%
% Usage:
% (1)
%   mu = getTransport_mu_kappa(y_s,TTrans,Tel,rho,p,Mm,options,mixCnst);
%
% (2)
%   [mu,kappa] = getTransport_mu_kappa(y_s,TTrans,Tel,rho,p,Mm,options,mixCnst,TRot,TVib,TElec,cp)
%
% (3)
%   [mu,kappa,kappav] = getTransport_mu_kappa(...)
%
% Inputs and Outputs:
%
%   y_s             N_eta x N_spec
%   TTrans          N_eta x 1
%   Tel             N_eta x 1
%   rho             N_eta x 1
%   p               N_eta x 1
%   Mm              N_eta x 1
%   TRot            N_eta x 1
%   TVib            N_eta x 1
%   TElec           N_eta x 1
%   cp              N_eta x 1
%   options         necessary fields
%                       .flow
%                       .mixture
%                       .modelTransport
%                       .cstPr
%                       .CEOrderMu
%                       .CEOrderKappaHeavy
%                       .CEOrderKappaEl
%   mixCnst         output from getAll_constants.m
%
%   mu              N_eta x 1
%   kappa           N_eta x 1
%   kappav          N_eta x 1
%
% Related functions:
% See also getAll_constants
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

if nargout>1
    if isempty(varargin)
        error('Missing inputs to calculate kappa! See Usage (2)');
    end
    ic = 1;
    TRot    = varargin{ic}; ic = ic + 1;
    TVib    = varargin{ic}; ic = ic + 1;
    TElec   = varargin{ic}; ic = ic + 1;
    cp      = varargin{ic}; %ic = ic + 1;
end
numberOfTemp = options.numberOfTemp;

if isempty(Mm)
    Mm = getMixture_Mm_R(y_s,[],[],mixCnst.Mm_s,[],[]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% DYNAMIC VISCOSITY %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch options.modelTransport
    case 'powerLaw'             % power law
        m = mixCnst.mPowerLaw;
        mu = mixCnst.mu_ref * (TTrans/mixCnst.T_ref).^m;
    case 'Sutherland'
        protectedvalue(options,'flow',{'CPG','TPG'},'the Sutherland transport model is only available for homogeneous mixtures (flow = CPG or TPG)');
        % extracting coefficients for sutherland's law
        mu_ref  = mixCnst.mu_ref;
        T_ref   = mixCnst.T_ref;
        Su_mu   = mixCnst.Su_mu;

        % computing viscosity
        mu = applySutherland(TTrans,mu_ref,T_ref,Su_mu);

    case {'SutherlandWilke','SutherlandEuckenWilke'}
        % extracting coefficients for sutherland's law
        Mm_s        = mixCnst.Mm_s;
        R0          = mixCnst.R0;
        bElectron   = mixCnst.bElectron;
        muRef_s     = mixCnst.muRef_s;
        TmuRef_s    = mixCnst.TmuRef_s;
        Smu_s       = mixCnst.Smu_s;

        % computing viscosity
        [mu,mu_s] = getMixture_mu_SutherlandWilke(y_s,TTrans,Tel,Mm_s,muRef_s,TmuRef_s,Smu_s,R0,bElectron);

    case {'BlottnerEucken','GuptaWilke'}
        % mixture dynamic viscosity using Wilke's mixing rule
        Mm_s    = mixCnst.Mm_s;
        Amu_s   = mixCnst.Amu_s;
        Bmu_s   = mixCnst.Bmu_s;
        Cmu_s   = mixCnst.Cmu_s;
        R0      = mixCnst.R0;
        bElectron = mixCnst.bElectron;

        % computing viscosity
        [mu,mu_s] = getMixture_mu_BlottnerWilke(y_s,TTrans,Tel,Mm_s,Amu_s,...
            Bmu_s,Cmu_s,R0,bElectron);
    case 'KineticEuckenWilke'
        Mm_s            = mixCnst.Mm_s;
        consts_Omega22  = mixCnst.consts_Omega22;
        nAvogadro       = mixCnst.nAvogadro;
        R0              = mixCnst.R0;
        bElectron       = mixCnst.bElectron;
        kBoltz          = mixCnst.kBoltz;
        qEl             = mixCnst.qEl;
        epsilon0        = mixCnst.epsilon0;
        bPos            = mixCnst.bPos;

        [mu,mu_s] = getMixture_mu_KineticWilke(TTrans,Tel,y_s,rho,p,Mm_s,bElectron,...
                                kBoltz,R0,nAvogadro,qEl,epsilon0,consts_Omega22,bPos);

    case {'CE_11','CE_12','CE_110','CE_120','CE_112','CE_113','CE_122','CE_123'}
        % Apply first approximation of Chapman-Enskog theory to calculate
        % mixture dynamic viscosity
        iOrder = 10 + options.CEOrderMu;
        Mm_s            = mixCnst.Mm_s;
        consts_Omega11  = mixCnst.consts_Omega11;
        consts_Omega22  = mixCnst.consts_Omega22;
        nAvogadro       = mixCnst.nAvogadro;
        R0              = mixCnst.R0;
        kBoltz          = mixCnst.kBoltz;
        bElectron       = mixCnst.bElectron;
        qEl             = mixCnst.qEl;
        epsilon0        = mixCnst.epsilon0;
        bPos            = mixCnst.bPos;

        % computing viscosity
        mu = applyChapmanEnskog(iOrder,y_s,TTrans,Tel,rho,p,Mm_s,Mm,...
            consts_Omega11,consts_Omega22,nAvogadro,R0,kBoltz,...
            bElectron,qEl,epsilon0,bPos);

    case 'Yos67'
        % Apply Yos' approximation of Chapman-Enskog theory to calculate
        % mixture dynamic viscosity
        Mm_s            = mixCnst.Mm_s;
        consts_Omega11  = mixCnst.consts_Omega11;
        consts_Omega22  = mixCnst.consts_Omega22;
        nAvogadro       = mixCnst.nAvogadro;
        R0              = mixCnst.R0;
        kBoltz          = mixCnst.kBoltz;
        bElectron       = mixCnst.bElectron;
        qEl             = mixCnst.qEl;
        epsilon0        = mixCnst.epsilon0;
        bPos            = mixCnst.bPos;

        % computing viscosity
        mu = applyYos67(11,y_s,TTrans,Tel,rho,p,Mm_s,Mm,R0,...
            consts_Omega11,consts_Omega22,nAvogadro,kBoltz,...
            bElectron,qEl,epsilon0,bPos,options);

    case 'Brokaw58'
        % Apply Brokaw's approximation of Chapman-Enskog theory to calculate
        % mixture dynamic viscosity
        Mm_s            = mixCnst.Mm_s;
        consts_Omega22  = mixCnst.consts_Omega22;
        nAvogadro       = mixCnst.nAvogadro;
        R0              = mixCnst.R0;
        kBoltz          = mixCnst.kBoltz;
        bElectron       = mixCnst.bElectron;
        qEl             = mixCnst.qEl;
        epsilon0        = mixCnst.epsilon0;
        bPos            = mixCnst.bPos;

        % computing viscosity
        mu = getMixture_mu_Brokaw58(TTrans,Tel,y_s,rho,p,Mm_s,...
            R0,nAvogadro,consts_Omega22,bElectron,kBoltz,qEl,epsilon0,bPos);

    otherwise
        error(['the chosen transport model ''',options.modelTransport,''' is not a valid option.']);
end

if nnz(isnan(mu))
    warning('mu has nan''d! Check the field of options.modelTransport!');
end

% If the user only wants dynamic viscosity, we're done here.
if nargout==1
    return
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%% THERMAL CONDUCTIVITY %%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if options.cstPr % thermal conductivity will be fixed by the prandtl number
    Pr = mixCnst.Pr;
    kappa = cp.*mu/Pr;
    kappav = zeros(size(kappa));
    kappaMod = struct;
else % it will come from the chosen transport model
    switch options.modelTransport
        case 'Sutherland'
            % extracting coefficients for sutherland's law
            kappa_ref   = mixCnst.kappa_ref;
            T_ref       = mixCnst.T_ref;
            Su_kappa    = mixCnst.Su_kappa;

            % computing thermal conductivity
            [kappa,~] = applySutherland(TTrans,kappa_ref,T_ref,Su_kappa);
            kappaMod = struct;
            kappav = zeros(size(kappa));

        case 'SutherlandWilke'
            % extracting coefficients for sutherland's law
            Mm_s        = mixCnst.Mm_s;
            R0          = mixCnst.R0;
            bElectron   = mixCnst.bElectron;
            kappaRef_s     = mixCnst.kappaRef_s;
            TkappaRef_s    = mixCnst.TkappaRef_s;
            Skappa_s       = mixCnst.Skappa_s;

            % computing viscosity
            [kappa,~] = getMixture_mu_SutherlandWilke(y_s,TTrans,Tel,Mm_s,muRef_s,TmuRef_s,Smu_s,R0,bElectron,'kappa',kappaRef_s,TkappaRef_s,Skappa_s);
            kappav = NaN*kappa;
            kappaMod = struct;

        case {'BlottnerEucken','KineticEuckenWilke','SutherlandEuckenWilke'}
            % mixture thermal conductivity using Eucken's relation and
            % Wilke's mixing rule

            [~,~,~,~,~,~,cvMod_s] = getMixture_h_cp_noMat(y_s,TTrans,TRot,TVib,TElec,Tel,mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,options,mixCnst.AThermFuncs_s);
            cvTrans_s   = cvMod_s.cvTrans_s;
            cvRot_s     = cvMod_s.cvRot_s;
            cvVib_s     = cvMod_s.cvVib_s;
            cvElec_s    = cvMod_s.cvElec_s;
            bMol = mixCnst.bMol;

            % computing thermal conductivity
            [kappa_all{1:max(2,nargout-1)}] = getMixture_kappa_EuckenWilke(y_s,mu_s,TTrans,Tel,...
                cvTrans_s,cvRot_s,cvVib_s,cvElec_s,Mm_s,R0,bElectron,options.numberOfTemp,bMol,options);

            kappa  = kappa_all{1};
            kappav = kappa_all{2};
            kappaMod = struct;

        case 'GuptaWilke'

            % mixture dynamic viscosity using Wilke's mixing rule
            Mm_s        = mixCnst.Mm_s;
            Akappa_s    = mixCnst.Akappa_s;
            Bkappa_s    = mixCnst.Bkappa_s;
            Ckappa_s    = mixCnst.Ckappa_s;
            Dkappa_s    = mixCnst.Dkappa_s;
            Ekappa_s    = mixCnst.Ekappa_s;
            R0          = mixCnst.R0;
            bElectron   = mixCnst.bElectron;

            % computing viscosity
            [kappa,kappav] = getMixture_mu_BlottnerWilke(y_s,TTrans,Tel,Mm_s,Amu_s,Bmu_s,Cmu_s,R0,bElectron,...
                                                            'kappa',Akappa_s,Bkappa_s,Ckappa_s,Dkappa_s,Ekappa_s);

            kappaMod = struct; % placeholder

        case {'CE_11','CE_12','CE_110','CE_120','CE_112','CE_113','CE_122','CE_123'}
            % Apply first approximation of Chapman-Enskog theory to calculate
            % mixture thermal conductivity
            iOrder = 20 + options.CEOrderKappaHeavy;
            consts_Bstar = mixCnst.consts_Bstar;

            % computing thermal conductivity of heavy species
            kappaHeavy = applyChapmanEnskog(iOrder,y_s,TTrans,Tel,rho,p,...
                Mm_s,Mm,consts_Omega11,consts_Omega22,nAvogadro,R0,kBoltz,...
                bElectron,qEl,epsilon0,bPos,consts_Bstar);

            nAtoms_s    = mixCnst.nAtoms_s;
            thetaVib_s  = mixCnst.thetaVib_s;
            thetaElec_s = mixCnst.thetaElec_s;
            gDegen_s    = mixCnst.gDegen_s;
            hForm_s     = mixCnst.hForm_s;

            % computing thermal conductivity of the internal modes
            [kappaInt,kappaRot,kappaVib,kappaElec] = getMixture_kappaInt_EuckenCorrection(TTrans,TRot,...
                TVib,TElec,Tel,y_s,rho,p,R0,nAtoms_s,thetaVib_s,thetaElec_s,...
                gDegen_s,bElectron,bPos,hForm_s,nAvogadro,consts_Omega11,...
                Mm_s,kBoltz,qEl,epsilon0,options,mixCnst.AThermFuncs_s);

            % computing electron thermal conductivity
            if nnz(bElectron) && options.CEOrderKappaEl>1
                consts_Cstar = mixCnst.consts_Cstar;
                consts_Estar = mixCnst.consts_Estar;
                consts_Omega14 = mixCnst.consts_Omega14;
                consts_Omega15 = mixCnst.consts_Omega15;
                consts_Omega24 = mixCnst.consts_Omega24;

                kappaEl = getElectron_kappa(options.CEOrderKappaEl,TTrans,...
                    Tel,y_s,rho,p,Mm_s,bElectron,bPos,kBoltz,R0,nAvogadro,...
                    qEl,epsilon0,consts_Omega11,consts_Omega22,...
                    consts_Bstar,consts_Cstar,consts_Estar,consts_Omega14,...
                    consts_Omega15,consts_Omega24);
            else
                kappaEl = zeros(size(kappaHeavy));
            end

            % Add all separate components of mixture thermal conductivity
            switch numberOfTemp
                case '1T'
                    kappa = kappaHeavy + kappaInt + kappaEl;
                    kappav = [];
                case '2T'
                    kappa = kappaHeavy + kappaRot;
                    kappav = kappaVib + kappaElec + kappaEl;
                otherwise
                    error(['the chosen number of temperatures ''',numberOfTemp,''' is not supported']);
            end

            kappaMod.kappaHeavy = kappaHeavy;
            kappaMod.kappaRot = kappaRot;
            kappaMod.kappaVib = kappaVib;
            kappaMod.kappaElec = kappaElec;
            kappaMod.kappaEl = kappaEl;

        case 'Yos67'
            % Apply first approximation of Chapman-Enskog theory to calculate
            % mixture thermal conductivity
            consts_Bstar = mixCnst.consts_Bstar;

            % computing thermal conductivity of heavy species
            kappaFroz = applyYos67(21,y_s,TTrans,Tel,rho,p,Mm_s,Mm,R0,...
                consts_Omega11,consts_Omega22,nAvogadro,kBoltz,bElectron,...
                qEl,epsilon0,bPos,consts_Bstar,options);

            nAtoms_s    = mixCnst.nAtoms_s;
            thetaVib_s  = mixCnst.thetaVib_s;
            thetaElec_s = mixCnst.thetaElec_s;
            gDegen_s    = mixCnst.gDegen_s;
            hForm_s     = mixCnst.hForm_s;

            % computing thermal conductivity of the internal modes
            [kappaInt,kappaRot,kappaVib,kappaElec] = getMixture_kappaInt_EuckenCorrection(TTrans,TRot,...
                TVib,TElec,Tel,y_s,rho,p,R0,nAtoms_s,...
                thetaVib_s,thetaElec_s,gDegen_s,bElectron,bPos,...
                hForm_s,nAvogadro,consts_Omega11,Mm_s,...
                kBoltz,qEl,epsilon0,options);

                % Add all separate components of mixture thermal conductivity
                switch numberOfTemp
                    case '1T'
                        kappa = kappaFroz + kappaInt;
                        kappav = [];
                    case '2T'
                        kappa = kappaFroz + kappaRot;
                        kappav = kappaVib + kappaElec;
                    otherwise
                        error(['the chosen number of temperatures ''',numberOfTemp,''' is not supported']);
                end
            kappaMod.kappaFroz = kappaFroz;
            kappaMod.kappaRot = kappaRot;
            kappaMod.kappaVib = kappaVib;
            kappaMod.kappaElec = kappaElec;

        case 'Brokaw58'
            % Apply Brokaw's approximation of Chapman-Enskog theory to calculate
            % mixture thermal conductivity
            [~,~,~,~,~,cpMod_s,~] = getMixture_h_cp_noMat(y_s,TTrans,TRot,TVib,TElec,Tel,mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,options,mixCnst.AThermFuncs_s);
            cpRot_s         = cpMod_s.cpRot_s;
            cpVib_s         = cpMod_s.cpVib_s;
            cpElec_s        = cpMod_s.cpElec_s;
            consts_Omega11  = mixCnst.consts_Omega11;

            % computing thermal conductivity
            [kappa,kappav,kappaInt] = ...
                getMixture_kappa_Brokaw58(y_s,TTrans,Tel,rho,p,...
                consts_Omega11,consts_Omega22,cpRot_s,cpVib_s,cpElec_s,...
                Mm_s,R0,kBoltz,bElectron,nAvogadro,qEl,epsilon0,bPos);

            kappaMod.kappaInt = kappaInt;

        otherwise
            error(['the chosen transport model ''',options.modelTransport,''' is not a valid option.']);
    end
end

if strcmp(numberOfTemp,'5T') % only supported for BlottnerEucken right now
    varargout = kappa_all;
else
    varargout{1} = kappa;
    varargout{2} = kappav;
    varargout{3} = kappaMod;
end

end % getTransport_mu_kappa
