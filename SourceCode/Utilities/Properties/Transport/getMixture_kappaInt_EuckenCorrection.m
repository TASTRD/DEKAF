function [kappaInt,varargout] = getMixture_kappaInt_EuckenCorrection(TTrans,TRot,TVib,TElec,Tel,...
                                    y_s,rho,p,R0,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,...
                                    bPos,hForm_s,nAvogadro,consts_Omega11,Mm_s,...
                                    kBoltz,qEl,epsilon0,options,varargin)
%GETMIXTURE_KAPPAINT_EUCKENCORRECTION Calculate the internal energy thermal
% conductivity of a mixture accounting for distinct rotational,
% vibrational, and electronic modes using Eucken's Correction.
%
% Usage:
%   (1)
%       kappaInt = getMixture_kappaInt_EuckenCorrection(TTrans,TRot,TVib,TElec,Tel,...
%               y_s,rho,p,R0,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,bPos,...
%               hForm_s,nAvogadro,consts_Omega11,Mm_s,kBoltz,qEl,...
%               epsilon0,options);
%   (2)
%       [kappaInt,kappaRot,kappaVib,kappaElec] = getMixture_kappaInt_EuckenCorrection(...);
%       |--> returns also the components of each energy mode
%
%   (3)
%       [...] = getMixture_kappaInt_EuckenCorrection(...,AThermFuncs_s);
%       |--> usage for polynomial thermal models ('NASA7' or
%       'customPoly7'), where the coefficients of the polynomials must be
%       passed.
%
% Inputs:
%   TTrans              N_eta x 1
%   TRot                N_eta x 1
%   TVib                N_eta x 1
%   TElec               N_eta x 1
%   Tel                 N_eta x 1
%   y_s                 N_eta x N_spec
%   rho                 N_eta x 1
%   p                   N_eta x 1
%   R0                  1 x 1
%   nAtoms_s            N_spec x 1
%   thetaVib_s          N_spec x 1
%   thetaElec_s         N_spec x N_stat
%   gDegen_s            N_spec x N_stat
%   bElectron           N_spec x 1
%   bPos                N_spec x 1
%   hForm_s             N_spec x 1
%   nAvogadro           1 x 1
%   consts_Omega11      N_spec x N_spec
%   kBoltz              1 x 1
%   qEl                 1 x 1
%   epsilon0            1 x 1
%   options             necessary fields
%                           neglect_hElec
%                           flow
%
% Output(s):
%   kappaInt    internal energy thermal conductivity of the mixture
%               (N_eta x 1)
%
% Notes:
%   "It can be shown that the Eucken correction is in fact exact when the
% internal energy modes can be split according to rotational, vibrational,
% and electronic degrees of freedom."
%
% References:
% Scoggins, J. B., & Magin, T. E. (2014). Development of Mutation++:
%   MUlticomponent Thermodynamics And Transport properties for IONized
%   gases library in C++. In 2014 AIAA Aviation Forum.
%   https://doi.org/10.2514/MTPHT14
% Magin, T. E. (2004). A Model for Inductive Plasma Wind Tunnels.
%   Thesis. ULB and VKI.
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% Mole fraction
R_s = getSpecies_R(R0,Mm_s);
[Mm,~] = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);
X_s = getSpecies_X(y_s,Mm,Mm_s);

% calculation of nD_sl_b
[~,nD_sl_b] = getPair_nD_binary_FOCE(TTrans,Tel,y_s,rho,consts_Omega11,...
               Mm_s,kBoltz,nAvogadro,bElectron,qEl,epsilon0,bPos,options);

% Computing heat capacities
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
         bElectron_mat,hForm_s_mat,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat] = reshape_inputs4enthalpy(R_s,...
         nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,TTrans,TRot,TVib,TElec,Tel);

[~,~,~,~,~,cvMod_s] = getSpecies_h_cp(TTrans_mat,TRot_mat,TVib_mat,TElec_mat,...
                                Tel_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,...
                                thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,varargin{:});
cvRot_s = cvMod_s.cvRot_s;
cvVib_s = cvMod_s.cvVib_s;
cvElec_s = cvMod_s.cvElec_s;

[N_eta,N_spec] = size(cvRot_s);

% Reshape inputs to size (N_eta x N_s x N_l)
X_l3D       = repmat(reshape(X_s,[N_eta,1,N_spec]),[1,N_spec,1]);

% Convert specific heat to specific heat per particle
Mm_s2D          = repmat(Mm_s',[N_eta,1]);
cpRot_s_part    = cvRot_s .* Mm_s2D / nAvogadro;
cpVib_s_part    = cvVib_s .* Mm_s2D / nAvogadro;
cpElec_s_part   = cvElec_s.* Mm_s2D / nAvogadro;

bHeavy = ~bElectron;

% Apply Eucken's Correction for the internal energies thermal conductivity
kappaRot    = applyEuckenCorrection(cpRot_s_part ,X_s,X_l3D,nD_sl_b,bHeavy);
kappaVib    = applyEuckenCorrection(cpVib_s_part ,X_s,X_l3D,nD_sl_b,bHeavy);
kappaElec   = applyEuckenCorrection(cpElec_s_part,X_s,X_l3D,nD_sl_b,bHeavy);
kappaInt    = kappaElec + kappaVib + kappaRot;

if nargout>1
    oc = 1; % output counter
    varargout{oc} = kappaRot;   oc = oc + 1;
    varargout{oc} = kappaVib;   oc = oc + 1;
    varargout{oc} = kappaElec; %oc = oc + 1;
end

end % getMixture_kappaInt_EuckenCorrection.m


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function kappaMod = applyEuckenCorrection(cvMod_s,X_s,X_l,nD_bin_sl,bHeavy)
% Equations (25a-c) in Magin 2004, p. 435

X_l = X_l(:,bHeavy,bHeavy);
X_s = X_s(:,bHeavy);
nD_bin_sl = nD_bin_sl(:,bHeavy,bHeavy);
cvMod_s = cvMod_s(:,bHeavy);

N_heavy = nnz(bHeavy);

denom = zeros(size(cvMod_s)); % (N_eta x N_s)
for l = 1:N_heavy
    denom = denom + X_l(:,:,l)./nD_bin_sl(:,:,l);
end

kappaMod = (X_s.*cvMod_s ./ denom) * ones(N_heavy,1);

end % applyEuckenCorrection subfunction
