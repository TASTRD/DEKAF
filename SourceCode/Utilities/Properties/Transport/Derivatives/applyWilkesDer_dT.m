function dZ_dT = applyWilkesDer_dT(X_s,Z_s,dZs_dT,Mm_s)
% applyWilkesDer_dT Use Wilke's Mixing Rule to calculate the temperature
% derivative of a mixture transport property.
%
% This function is commonly applied to calculating a mixture dynamic
% viscosity or thermal conductivity
%
% Inputs:
%   X_s     -   Species molar fractions [-]
%               Matrix of size (N_eta by N_spec)
%   Z_s     -   Species transport property
%               Matrix of size (N_eta by N_spec)
%   dZs_dT  -   Species transport property
%               Matrix of size (N_eta by N_spec)
%   Mm_s    -   Species molar masses [kg/mol]
%               Matrix of size (N_spec x 1)
%
% Outputs
%   dZ_dT   -   Mixture transport property
%               Matrix of size (N_eta by N_spec)
%
% Notes:
%   N_eta is the number of elements in the spatial domain
%   N_spec is the number of species in the mixture
%
% References:
%   Wilke, C. R., �A Viscosity Equation for Gas Mixtures,�
%   Journal of Chemical Physics, Vol. 18 No. 4, 1950, pp. 517�519.
%
% See also: listOfVariablesExplained
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(X_s); % getting sizes

% getting 3D matrices varying over s and l (2nd and 3rd dimension)
Z3D_s       = repmat(Z_s,[1,1,N_spec]);
Z3D_l       = repmat(reshape(Z_s,[N_eta,1,N_spec]),[1,N_spec,1]);

dZsdT3D     = repmat(dZs_dT,[1,1,N_spec]);
dZldT3D     = repmat(reshape(dZs_dT,[N_eta,1,N_spec]),[1,N_spec,1]);

X3D_l       = repmat(reshape(X_s,[N_eta,1,N_spec]),[1,N_spec,1]);

Mm_s = repmat(Mm_s',[N_eta,1]);
Mm3D_s = repmat(Mm_s,[1,1,N_spec]);
Mm3D_l = repmat(reshape(Mm_s,[N_eta,1,N_spec]),[1,N_spec,1]);

% obtaining wilke's mixing coefficient
phi_sl = X3D_l .* (1+(Z3D_s./Z3D_l).^(1/2) .* (Mm3D_l./Mm3D_s).^(1/4)).^2 ./ (8*(1+Mm3D_s./Mm3D_l)).^(1/2);
dphisl_dT = X3D_l .* (((Z3D_l./Z3D_s).^(1/2).*(Mm3D_l./Mm3D_s).^(1/4) + (Mm3D_l./Mm3D_s).^(1/2)) .* (1./Z3D_l.*dZsdT3D - Z3D_s./Z3D_l.^2.*dZldT3D)) ./ (8*(1+Mm3D_s./Mm3D_l)).^(1/2);

% summing over l (third dimension) to obtain a 2D phi_s
phi_s = zeros(N_eta,N_spec);
dphis_dT = zeros(N_eta,N_spec);
for l=1:N_spec % summing over l
    phi_s       = phi_s     + phi_sl(:,:,l);
    dphis_dT    = dphis_dT  + dphisl_dT(:,:,l);
end

% summing in matrix form to obtain the property
dZ_dT = (X_s.*(1./phi_s.*dZs_dT - Z_s./phi_s.^2.*dphis_dT)) * ones(N_spec,1); % by multiplying by a column of ones, we are effectively summing over the columns (species

end