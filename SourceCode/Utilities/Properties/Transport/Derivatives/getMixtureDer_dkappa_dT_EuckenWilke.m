function dkappa_dT = getMixtureDer_dkappa_dT_EuckenWilke(y_s,mu_s,dmus_dT,TTrans,Tel,cvTrans_s,cvRot_s,cvVib_s,cvElec_s,dcvVibs_dT,dcvElecs_dT,Mm_s,R0,bElectron)
% getMixtureDer_dkappa_dT_EuckenWilke Calculates the mixture thermal conductivity
%
% kappa = getMixtureDer_dkappa_dT_EuckenWilke(y_s,mu_s,TTrans,Tel,cvTrans_s,...
%               cvRot_s,cvVib_s,cvElec_s,dcvVibs_dT,dcvElecs_dT,Mm_s,R0,bElectron)
%
% Inputs:
%   y_s         -   species mass fractions, matrix of size (N_eta x N_spec)
%   mu_s        -   species dynamic viscosity, matrix of size (N_eta x N_spec)
%   dmus_dT     -   T deriv. of the species dynamic viscosity,   [kg/(m*s*K)]
%                   matrix of size (N_eta x N_spec)
%   TTrans      -   translational temperature, vector of size (N_eta x 1)
%   Tel         -   electron temperature, vector of size (N_eta x 1)
%   cvTrans_s   -   translation specific heat at constant volume [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   cvRot_s     -   rotational specific heat at constant volume  [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   cvVib_s     -   vibrational specific heat at constant volume [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   cvElec_s    -   electronic specific heat at constant volume  [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   dcvVibs_dT  -   T. deriv. of cvVib_s                         [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   dcvElecs_dT -   T. deriv of cvElec_s                         [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   Mm_s        -   species mixture molar mass, matrix of size   [kg/mol]
%                   (N_spec x 1)
%   nAtoms_s    -   species number of atoms, matrix of size      [-]
%                   (N_spec x 1)
%   R0          -   universal gas constant                       [J/mol-K]
%   bElectron   -   electron boolean (N_spec x 1)                [-]
%
% Notes:
%   Apply Wilke's mixing rule and Eucken's relation
%
% References:
%   Wilke, C., "A Viscosity Equation for Gas Mixtures" The Journal of
%       Chemical Physics, Vol. 18, No. 4, 1950
%   A. Eucken, "�ber das W�rmeleitverm�gen, die spezifische W�rme und die
%       innere Reibung der Gase," Phys. Z, 1913
%
% Children and related functions:
%   See also getMixture_Mm_R.m, getSpecies_X.m, getSpecies_mu.m,
%            getSpecies_cv.m, getSpecies_kappa.m, applyWilkes.m
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% mixture molar mass and mixture gas constant
Mm = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);

% species mole fraction
X_s = getSpecies_X(y_s,Mm,Mm_s);

% species thermal conductivity
kappa_s = getSpecies_kappa_Eucken(mu_s,cvTrans_s,cvRot_s,cvVib_s,cvElec_s);
dkappas_dT = getSpeciesDer_dkappa_dT_Eucken(mu_s,dmus_dT,cvTrans_s,cvRot_s,cvVib_s,cvElec_s,dcvVibs_dT,dcvElecs_dT);

% mixture thermal conductivity given by Wilke's mixing rule
dkappa_dT = applyWilkesDer_dT(X_s,kappa_s,dkappas_dT,Mm_s);

end
