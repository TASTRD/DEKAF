function dkappas_dT = getSpeciesDer_dkappa_dT_Eucken(mu_s,dmus_dT,cvTrans_s,cvRot_s,cvVib_s,cvElec_s,dcvVibs_dT,dcvElecs_dT)
%GETSPECIES_KAPPA_EUCKEN Calculate species thermal conductivity using
% Eucken's theory
%
% dkappas_dT = getSpeciesDer_dkappa_dT_Eucken(mu_s,dmus_dT,cvTrans_s,cvRot_s,...
%                                       cvVib_s,cvElec_s,dcvVibs_dT,dcvElecs_dT)
%
% Inputs:
%   mu_s        -   species dynamic viscosity,                   [kg/(m*s)]
%                   matrix of size (N_eta x N_spec)
%   dmus_dT     -   T deriv. of the species dynamic viscosity,   [kg/(m*s*K)]
%                   matrix of size (N_eta x N_spec)
%   cvTrans_s   -   translation specific heat at constant volume [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   cvRot_s     -   rotational specific heat at constant volume  [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   cvVib_s     -   vibrational specific heat at constant volume [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   cvElec_s    -   electronic specific heat at constant volume  [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   dcvVibs_dT  -   T. deriv. of cvVib_s                         [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%   dcvElecs_dT -   T. deriv of cvElec_s                         [J/(kg*K)]
%                   matrix of size (N_eta x N_spec)
%
% Outputs:
%   kappa_s     -   species thermal conductivity                  [W/(m*K)]
%
% Notes:
% Eucken's relation, rooted in Chapman-Enskog theory, is used
%
% References:
%   A. Eucken, "�ber das W�rmeleitverm�gen, die spezifische W�rme and die
% innere Reibung der Gase", Physik Z., 14 (1913), pp. 324-332
%
% Children and related functions:
%   See also getMixture_kappa.m, getMixtureDer_dkappa_dT,
%   listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

dkappas_dT = mu_s.*(dcvVibs_dT + dcvElecs_dT) + dmus_dT.*(5/2*cvTrans_s + cvRot_s + cvVib_s + cvElec_s); % Eucken's relation

end

