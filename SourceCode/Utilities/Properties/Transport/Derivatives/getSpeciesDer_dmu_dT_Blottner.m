function dmus_dT = getSpeciesDer_dmu_dT_Blottner(T,Amu_s,Bmu_s,Cmu_s,N_spec)
% getSpeciesDer_dmu_dT_Blottner Calculate the temperature derivative of the
% species dynamic viscosity with Blottner's 1971 curve fit
%
% Inputs:
%   T               -   temperature, vector of size (N_eta x 1)         [K]
%   Amu_s, Bmu_s,   -   coefficients in the Blottner curve fit for the
%   & Cmu_s             dynamic viscosity [-].
%                       matrices of size (N_spec x 1)
%   N_spec          -   number of species considered in the reacting
%                       mixture. scalar
%
% Notes:
% Assume Blottner's curve fit for species dynamic viscosity
%
% References:
%   Blottner, F.G., Johnson, M. and Ellis, M., �Chemically Reacting Viscous
%       Flow Program for Multi-Component Gas Mixtures,� Report NO.
%       SC-RR-70-754, Sandia Laboratories, Albuquerque, New Mexico, 1971.
%
% Children and related functions:
%   See also getSpecies_constants, getSpecies_mu_Blottner,
%   listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

% Reshape to N_eta x N_spec
N_eta = length(T);
T = repmat(T,[1,N_spec]);
Amu_s = repmat(Amu_s',[N_eta,1]);
Bmu_s = repmat(Bmu_s',[N_eta,1]);
Cmu_s = repmat(Cmu_s',[N_eta,1]);

% Species viscosity
dmus_dT = 0.1*exp( (Amu_s.*log(T) + Bmu_s).*log(T) + Cmu_s ) .* ((Amu_s.*log(T)+Bmu_s)./T + Amu_s./T.*log(T));

end % getSpecies_mu_Blottner.m
