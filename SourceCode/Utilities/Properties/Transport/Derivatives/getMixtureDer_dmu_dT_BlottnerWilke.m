function [dmu_dT,dmus_dT] = getMixtureDer_dmu_dT_BlottnerWilke(y_s,TTrans,Tel,Mm_s,Amu_s,Bmu_s,Cmu_s,N_spec,R0,bElectron)
% getMixtureDer_dmu_dT_BlottnerWilke Calculates the temperature derivative
% of the mixture dynamic viscosity using Blottner's 1971 curve fit and
% Wilke's mixing rule.
%
% [dmu_dT,dmus_dT] = getMixtureDer_dmu_dT_BlottnerWilke(y_s,TTrans,Tel,Mm_s,Amu_s,Bmu_s,Cmu_s,N_spec,R0,bElectron);
%
% Inputs:
%   y_s         -   species mass fractions, matrix of size (N_eta x N_spec)
%   TTrans      -   translational temperature, vector of size (N_eta x 1)
%   Tel         -   electron temperature, vector of size (N_eta x 1)
%   Mm_s        -   species mixture molar mass, matrix of size
%                   (N_eta x N_spec)
%   Amu_s       -   coefficients in the Blottner curve fit for the
%   Bmu_s       -   dynamic viscosity.
%   Cmu_s       -   vectors of size (N_spec x 1)
%   N_spec      -   number of species in mixture
%   R0          -   universal gas constant
%   bElectron   -   electron boolean (N_spec x 1)
%
% Outputs:
%   dmu_dT      -   temperature derivative of the mixture dynamic viscosity
%                   (N_eta x 1)
%   dmus_dT     -   temperature derivative of the species dynamic viscosity
%                   (N_eta x N_spec)
%
% Notes:
%   Apply Wilke's mixing rule and Blottner's curve fit
%
% References:
%   Wilke, C., "A Viscosity Equation for Gas Mixtures" The Journal of
%       Chemical Physics, Vol. 18, No. 4, 1950
%   Blottner, F.G., Johnson, M. and Ellis, M., "Chemically Reacting Viscous
%       Flow Program for Multi-Component Gas Mixtures," Report NO.
%       SC-RR-70-754, Sandia Laboratories, Albuquerque, New Mexico, 1971.
%
% Children and related functions:
%   See also getMixture_Mm_R.m, getSpecies_X.m,
%            getSpecies_mu_Blottner.m, getSpeciesDer_dmu_dT_Blottner,
%            eval_func_Heavy_el.m, applyWilkes.m, applyWilkesDer_dT,
%            listOfVariablesExplained
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% reshaping bElectron
N_eta = length(TTrans);
bElectron_mat = repmat(bElectron',[N_eta,1]);

% mixture molar mass and mixture gas constant
Mm = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);

% mole fraction of each species
X_s = getSpecies_X(y_s,Mm,Mm_s);

% species dynamic viscosity using Blottner's curve fit
mu_s = eval_func_Heavy_el(@(T) getSpecies_mu_Blottner(T,Amu_s,Bmu_s,Cmu_s,N_spec),TTrans,Tel,bElectron_mat);
dmus_dT = eval_func_Heavy_el(@(T) getSpeciesDer_dmu_dT_Blottner(T,Amu_s,Bmu_s,Cmu_s,N_spec),TTrans,Tel,bElectron_mat);

% Wilke's mixing rule
dmu_dT = applyWilkesDer_dT(X_s,mu_s,dmus_dT,Mm_s);
end
