function kappaEl = getElectron_kappa(Order,TTrans,Tel,y_s,rho,p,Mm_s,bElectron,bPos,kBoltz,R0,nAvogadro,qEl,...
                    epsilon0,consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,consts_Estar,...
                    consts_Omega14,consts_Omega15,consts_Omega24,varargin)
% getElectron_kappa computes the electron thermal conductivity using the
% second or third approximation to Chapman-Enskog.
%
% Examples:
%   1
%   kappaEl = getElectron_kappa(Order,TTrans,Tel,y_s,rho,p,Mm_s,bElectron,bPos,kBoltz,R0,nAvogadro,qEl,...
%                     epsilon0,consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,consts_Estar,...
%                    consts_Omega14,consts_Omega15,consts_Omega24);
%
% Inputs:
%   FIXME
%
% References:
% Magin, T. E. and Degrez, G., "Transport of partially ionized and
%   unmagnetized plasmas," Physical Review E, Vol. 70, 2004.
% Scoggins, J. (2017). Development of numerical methods and study of
%   coupled flow, radiation, and ablation phenomena for atmospheric entry.
%   von Karman Institute.
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

N_eta = length(TTrans);

% Mole fraction
[Mm,~] = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);
X_s = getSpecies_X(y_s,Mm,Mm_s);

% Obtaining collision integrals and friends
% Note: the derivatives of the collision integrals are always wrt T_sl i.e.
% the temperature "governing" each collision
[lnOmega11_sl,Tstar] = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega11,bPos);
lnOmega22_sl         = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega22,bPos);

% Calculate Bstar and Cstar, as well as their derivatives wrt Tsl
lnBstar_sl = getPair_ABCstar(Tstar,consts_Bstar);
lnCstar_sl = getPair_ABCstar(Tstar,consts_Cstar);

Omega11_sl  = exp(lnOmega11_sl); % undoing exponential
Omega22_sl  = exp(lnOmega22_sl);
Bstar_sl    = exp(lnBstar_sl);
Cstar_sl    = exp(lnCstar_sl);

[Omega12_sl,Omega13_sl] = getPair_Omega12_Omega13(Omega11_sl,Bstar_sl,Cstar_sl);% obtaining 12, and 13 collision integrals from Bstar and Cstar
switch Order
    case 2
        % Obtaining lambda functions of the 2nd-order sonine expansion
        Lambda11_ee = getElectron_Lambdaij(X_s,Tel,bElectron,Mm_s,kBoltz,nAvogadro,...
                                        Omega11_sl,Omega12_sl,Omega13_sl,Omega22_sl);
        % computing kappa (Magin 2004: Equation 10a)
        kappaEl = X_s(:,bElectron).^2 ./ Lambda11_ee;
    case 3
        %%%% OLD CALLS (using recurrence relation)
        %{
%         % Obtaining temperature governing the different collisions
%         T_el3D = repmat(Tel,[1,N_spec,N_spec]);
%         T_sl = repmat(TTrans,[1,N_spec,N_spec]);
%         T_sl(:,bElectron,:) = T_el3D(:,bElectron,:);
%         T_sl(:,:,bElectron) = T_el3D(:,:,bElectron);
%         % Obtaining Omega14_sl, Omega15_sl, Omega23_sl & Omega24_sl using
%         % the recurrence relation of the collision integrals
%         [Omega14_sl,Omega15_sl] = getPair_Omegaijplus_Recurrence(T_sl,Omega13_sl,3,dOmega13sl_dT,dOmega13sl_dT2);
%         [Omega23_sl,Omega24_sl] = getPair_Omegaijplus_Recurrence(T_sl,Omega22_sl,2,dOmega22sl_dT,dOmega22sl_dT2);
        %}
        %%%% UNTIL HERE

        % Obtaining Omega14_sl, Omega15_sl & Omega24_sl from their
        % curve-fits
        lnOmega14_sl = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega14,bPos);
        lnOmega15_sl = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega15,bPos);
        lnOmega24_sl = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega24,bPos);
        % Obtaining Estar_sl from the curve fits (Estar=Omega23_sl/Omega22_sl)
        lnEstar_sl = getPair_ABCstar(Tstar,consts_Estar);
        % computing Omega23_sl from Estar
        lnOmega23_sl = lnEstar_sl + lnOmega22_sl;

        % undoing exponential
        Omega14_sl = exp(lnOmega14_sl);
        Omega15_sl = exp(lnOmega15_sl);
        Omega23_sl = exp(lnOmega23_sl);
        Omega24_sl = exp(lnOmega24_sl);

        % Enforcing that for the electron-Neutral collisions, Omega14 = Omega15 = Omega13
        bCharged = getPair_bCharged(bPos,bElectron);
        bCharged3D = repmat(permute(bCharged,[3,1,2]),[N_eta,1,1]); % (s,l) --> (eta,s,l)
        Omega14_sl(~bCharged3D) = Omega13_sl(~bCharged3D); % we apply it on all non-charged, but we only use the electron-neutral afterwards
        Omega15_sl(~bCharged3D) = Omega13_sl(~bCharged3D);

        % Obtaining lambda functions of the 3rd-order sonine expansion
        [Lambda11_ee,Lambda12_ee,Lambda22_ee] = getElectron_Lambdaij(X_s,Tel,bElectron,...
                Mm_s,kBoltz,nAvogadro,Omega11_sl,Omega12_sl,Omega13_sl,Omega14_sl,...
                Omega15_sl,Omega22_sl,Omega23_sl,Omega24_sl);
        % computing kappa (Magin 2004: Equation 10b)
        kappaEl = X_s(:,bElectron).^2 .*Lambda22_ee ./ (Lambda11_ee.*Lambda22_ee - Lambda12_ee.^2);
    otherwise
        error(['the chosen order for the Chapman-Enskog approximation to be used on the electron thermal conductivity (',...
            num2str(Order),') is not supported.']);
end