function [Q,Q_s] = getMixture_mu_BlottnerWilke(y_s,TTrans,Tel,Mm_s,Amu_s,Bmu_s,Cmu_s,R0,bElectron,varargin)
% GETMIXTURE_MU_BLOTTNER Calculate the mixture dynamic viscosity using
% Blottner's 1971 curve fit and Wilke's mixing rule
%
% Usage:
%   (1)
%       [mu,mu_s] = getMixture_mu_BlottnerWilke(y_s,TTrans,Tel,Mm_s,Amu_s,Bmu_s,Cmu_s,R0,bElectron);
%
%   (2)
%       [kappa,kappa_s] = getMixture_mu_BlottnerWilke(y_s,TTrans,Tel,Mm_s,Amu_s,Bmu_s,Cmu_s,R0,bElectron,...
%                                                   'kappa',Akappa_s,Bkappa_s,Ckappa_s,Dkappa_s,Ekappa_s);
%       |--> computes also the mixture thermal conductivity using the
%       Blottner-like curve fits proposed by Gupta et al.
%
% Inputs:
%   y_s         -   species mass fractions, matrix of size (N_eta x N_spec)
%   TTrans      -   translational temperature, vector of size (N_eta x 1)
%   Tel         -   electron temperature, vector of size (N_eta x 1)
%   Mm_s        -   species mixture molar mass, matrix of size
%                   (N_spec x 1)
%   Amu_s       -   coefficients in the Blottner curve fit for the
%   Bmu_s       -   dynamic viscosity.
%   Cmu_s       -   vectors of size (N_spec x 1)
%   R0          -   universal gas constant
%   bElectron   -   electron boolean (N_spec x 1)
%   Akappa_s, Bkappa_s, Ckappa_s, Dkappa_s, Ekappa_s - same story but
%                   for the thermal conductivity  (N_spec x 1)
%
% Outputs:
%   mu          -   mixture dynamic viscosity (N_eta x 1)
%   mu_s        -   species dynamic viscosity (N_eta x N_spec)
%   kappa       -   mixture thermal conductivity (N_eta x 1)
%   kappa_s     -   species thermal conductivity (N_eta x N_spec)
%
% Notes:
%   Apply Wilke's mixing rule and Blottner's curve fit
%
% References:
%   Wilke, C., "A Viscosity Equation for Gas Mixtures" The Journal of
%       Chemical Physics, Vol. 18, No. 4, 1950
%   Blottner, F.G., Johnson, M. and Ellis, M., "Chemically Reacting Viscous
%       Flow Program for Multi-Component Gas Mixtures," Report NO.
%       SC-RR-70-754, Sandia Laboratories, Albuquerque, New Mexico, 1971.
%   Gupta, R. N., Yos, J. M., Thompson, R. A., & Lee, K.-P. (1990). A
%       review of Reaction Rates and Thermodynamic and Transport Properties
%       for an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%       Calculations to 30000K.
%
% Children and related functions:
%   See also getMixture_Mm_R.m, getSpecies_X.m, getSpecies_mu_Blottner.m,
%            eval_func_Heavy_el.m, applyWilkes.m, listOfVariablesExplained
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

if ~isempty(varargin) && strcmp(varargin{1},'kappa');   bKappa = true;
else;                                                   bKappa = false;
end

% reshaping bElectron
bHeavy = ~bElectron; % heavy particles
[N_eta,N_spec] = size(y_s);
bElectron_mat = repmat(bElectron.',[N_eta,1]);

% mixture molar mass and mixture gas constant
Mm = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);

% mole fraction of each species
X_s = getSpecies_X(y_s,Mm,Mm_s);

% species dynamic viscosity using Blottner's curve fit
mu_s = eval_func_Heavy_el(@(T) getSpecies_mu_Blottner(T,Amu_s,Bmu_s,Cmu_s,N_spec),TTrans,Tel,bElectron_mat);

% Wilke's mixing rule
if bKappa
    ic=2;
    Akappa_s = varargin{ic}; ic=ic+1;
    Bkappa_s = varargin{ic}; ic=ic+1;
    Ckappa_s = varargin{ic}; ic=ic+1;
    Dkappa_s = varargin{ic}; ic=ic+1;
    Ekappa_s = varargin{ic}; %ic=ic+1;
    kappa_s = eval_func_Heavy_el(@(T) getSpecies_mu_Blottner(T,Akappa_s,Bkappa_s,Ckappa_s,N_spec,Dkappa_s,Ekappa_s),TTrans,Tel,bElectron_mat);
    Q = applyWilkes(X_s,          kappa_s,       mu_s,          Mm_s);
    Q_s = kappa_s;
else
    Q = applyWilkes(X_s(:,bHeavy),mu_s(:,bHeavy),mu_s(:,bHeavy),Mm_s(bHeavy));
    Q_s = mu_s;
end
