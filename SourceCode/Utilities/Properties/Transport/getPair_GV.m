function GV_sl = getPair_GV(TTrans,Tel,y_s,Mm_s,bElectron,D_sl_b)
% getPair_GV computes the coefficients of the matrix needed for the
% Stefan-Maxwell diffusion model.
%
% Usage:
%   (1)
%       GV_sl = getPair_GV(TTrans,Tel,y_s,Mm_s,bElectron,D_sl_b)
%
% Inputs and outputs:
%   TTrans
%       translational temperature (N_eta x 1)
%   Tel
%       electron temperature (N_eta x 1)
%   y_s
%       species mass fractions (N_eta x N_spec)
%   Mm_s
%       species molar mass (N_spec x 1)
%   bElectron
%       species electron boolean (N_spec x 1)
%   D_sl_b
%       species-pair binary diffusion coefficient (N_eta x N_spec(s) x N_spec(l))
%   GV_sl
%       coefficients in the Stefan-Maxwell matrix (N_eta x N_spec(s) x N_spec(l))
%
% References:
%   () Magin, T. E., & Degrez, G. (2004). Transport algorithms for
%   partially ionized and unmagnetized plasmas. Journal of Computational
%   Physics, 198(2), 424–449.
%
% Author(s): Fernando Miro Miro
% Date: May 2019
% GNU Lesser General Public License 3.0

[~,N_spec] = size(y_s);

% computing auxiliary properties
Mm = getMixture_Mm_R(y_s,[],[],Mm_s,[],[]);                                 % mixture molar mass
X_s = getSpecies_X(y_s,Mm,Mm_s);                                            % mole fractions
T_s = getSpecies_T(TTrans,Tel,bElectron);                                   % species temperature

% reshaping
Xs_3D       = repmat(        X_s         ,[1,1,N_spec]);                    % (eta,s) --> (eta,s,l)
Xl_3D       = repmat(permute(X_s,[1,3,2]),[1,N_spec,1]);                    % (eta,l) --> (eta,s,l)
Ts_3D       = repmat(        T_s         ,[1,1,N_spec]);                    % (eta,s) --> (eta,s,l)
Tl_3D       = repmat(permute(T_s,[1,3,2]),[1,N_spec,1]);                    % (eta,l) --> (eta,s,l)
TTrans_3D   = repmat(       TTrans,  [1,N_spec,N_spec]);                    % (eta,1) --> (eta,s,l)

% off-diagonal terms
GV_sl           = -(Ts_3D.*Tl_3D./TTrans_3D.^2) .* Xs_3D.*Xl_3D./D_sl_b;    % Eq. C.1g and C.2c in Magin 2004
% diagonal terms
GVdiag2sum_sl   =  (Tl_3D.*Tl_3D./TTrans_3D.^2) .* Xs_3D.*Xl_3D./D_sl_b;    % term going in the summation in Eq. C.1h and C.3g in Magin 2004
GVsum_s         = sum(GVdiag2sum_sl,3);                                     % summing over l (sumation in Eq. C.1h and C.3g in Magin 2004)
for s=1:N_spec                                                              % looping species diagonal
    GV_sl(:,s,s) = GVsum_s(:,s) - GVdiag2sum_sl(:,s,s);                         % Eq. C.1h and C.3g in Magin 2004
end

end % getPair_GV