function Q = applyWilkes(X_s,Q_s,Z_s,Mm_s)
% APPLYWILKES Use Wilke's Mixing Rule to calculate a mixture
% transport property.
%
% This function is commonly applied to calculating a mixture dynamic
% viscosity or thermal conductivity.
%
% Inputs:
%   X_s     -   Species molar fractions [-]
%               Matrix of size (N_eta by N_spec)
%   Z_s     -   Species transport property to be mixed
%               Matrix of size (N_eta by N_spec)
%   Q_s     -   Species transport property to be used for the mixing
%               coefficients. Typically one uses mu_s here always.
%               Mortensen used Z_s=1.
%               Matrix of size (N_eta by N_spec)
%   mu_s    -   Species viscosities
%               Matrix of size (N_eta by N_spec)
%   Mm_s    -   Species molar masses [kg/mol]
%               Matrix of size (N_spec x 1)
%
% Outputs
%   Z       -   Mixture transport property
%               Matrix of size (N_eta by N_spec)
%
% Notes:
%   N_eta is the number of elements in the spatial domain
%   N_spec is the number of species in the mixture
%
% References:
%   Wilke, C. R., �A Viscosity Equation for Gas Mixtures,�
%   Journal of Chemical Physics, Vol. 18 No. 4, 1950, pp. 517�519.
%
% See also: listOfVariablesExplained
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(X_s); % getting sizes

% getting 3D matrices varying over s and l (2nd and 3rd dimension)
% Z3D_s = repmat(Q_s,[1,1,N_spec]);                               %% Using kappa_s as weighing property (when computing kappa)
% Z3D_l = repmat(reshape(Q_s,[N_eta,1,N_spec]),[1,N_spec,1]);
Z3D_s = repmat(Z_s,[1,1,N_spec]);                               %% Using mu_s as weighing property (when computing kappa)
Z3D_l = repmat(reshape(Z_s,[N_eta,1,N_spec]),[1,N_spec,1]);

X3D_l = repmat(reshape(X_s,[N_eta,1,N_spec]),[1,N_spec,1]);

Mm_s = repmat(Mm_s',[N_eta,1]);
Mm3D_s = repmat(Mm_s,[1,1,N_spec]);
Mm3D_l = repmat(reshape(Mm_s,[N_eta,1,N_spec]),[1,N_spec,1]);

% obtaining wilke's mixing coefficient
phi_sl = X3D_l .* (1+(Z3D_s./Z3D_l).^(1/2) .* (Mm3D_l./Mm3D_s).^(1/4)).^2 ./ (8*(1+Mm3D_s./Mm3D_l)).^(1/2);

% summing over l (third dimension) to obtain a 2D phi_s
phi_s = zeros(N_eta,N_spec);
% Wilke 1950
%{
all_l = 1:N_spec;
for s=1:N_spec % obtaining sumation for each s
    lNotS = all_l(all_l~=s); % all species except for s
    phi_s(:,s) = reshape(phi_sl(:,s,lNotS),[N_eta,N_spec-1]) * ones(N_spec-1,1); % summing over all l different than s
end
%}
% Equivalent simplification
%{%
for l=1:N_spec % summing over l
    phi_s = phi_s + phi_sl(:,:,l);
end
%}

% summing in matrix form to obtain the property
% Z = (Z_s./(1+1./X_s.*phi_s)) * ones(N_spec,1); % Wilke 1950
Q = (X_s.*Q_s./phi_s) * ones(N_spec,1); % Equivalent simplification

end