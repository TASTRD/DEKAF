function [kappa,kappav,varargout] = ...
    getMixture_kappa_Brokaw58(y_s,TTrans,Tel,rho,p,...
        consts_Omega11,consts_Omega22,...
        cpRot_s,cpVib_s,cpElec_s,Mm_s,R0,kBoltz,bElectron,nAvogadro,qEl,epsilon0,bPos,varargin)
%getMixture_kappa_Brokaw58 Calculate mixture frozen thermal conductivity
% using the formulae outlined by Gupta, Yos, et al. 1990
%
% Usage:
% 1. Calculate frozen thermal conductivity
%
%   kappa = ...
%       getMixture_kappa_Brokaw58(y_s,TTrans,Tel,rho,p...
%           consts_Omega11,consts_Omega22,...
%           cpRot_s,cpVib_s,cpElec_s,Mm_s,R0,kBoltz,bElectron,...
%           nAvogadro,qEl,epsilon0,bPos);
%
% 1.1 ...using equilibrium composition to calculate reduced temperature
%      for the collision integrals.
%   kappa = ...
%       getMixture_kappa_Brokaw58(y_s,TTrans,Tel,rho,p...
%           consts_Omega11,consts_Omega22,...
%           cpRot_s,cpVib_s,cpElec_s,Mm_s,R0,kBoltz,bElectron,...
%           nAvogadro,qEl,epsilon0,bPos,options,dXs_dT,dXs_dT2);
%
% 2. Calculate frozen thermal conductivity and its components
%
%   [kappa,~,kappaTrans,kappaInt,kappaRot,kappaVib,kappaElec] = ...
%       getMixture_kappa_Brokaw58(...); <--| same inputs as Usage 1
%
% 2.1 ...using equilibrium composition to calculate reduced temperature
%      for the collision integrals.
%   [...] = ...
%       getMixture_kappa_Brokaw58(y_s,TTrans,Tel,rho,p...
%           consts_Omega11,consts_Omega22,...
%           cpRot_s,cpVib_s,cpElec_s,Mm_s,R0,kBoltz,bElectron,...
%           nAvogadro,qEl,epsilon0,bPos,options,dXs_dT,dXs_dT2);
%       ...same inputs as Usage 1.1
%
% 3. Calculate translational-rotational and vibrational-electronic thermal
% conductivity
%
%   [kappa,kappav,...] = getMixture_kappa_Brokaw58(...,'2T');
%
% References:
%
% Brokaw, R. S. (1958). Approximate formulas for the viscosity and thermal
%   conductivity of gas mixtures. The Journal of Chemical Physics, 29(2),
%   391–397.
%
% Gupta,  R. N.,  Yos,  J. M.,  Thompson,  R. A.,  and Lee,  K.-P.,  "A
%   Review of Reaction Rates and Thermodynamic and Transport Properties for
%   an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%   Calculations to 30000 K," Nasa-rp-1232, National Aeronautics and Space
%   Administration, 1990.
%
% See also: getPair_lnOmegaij, getMixture_kappaTrans_Brokaw58, getMixture_kappaInt_GuptaYos
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

if isempty(varargin)
    numberOfTemp = '1T';
else
    numberOfTemp = varargin{1};
end

% Get (2,2) collision integrals
lnOmega11_sl = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega11,bPos);
lnOmega22_sl = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega22,bPos);

% Equation (35)
lnDelta1_sl  = ...
    getPair_Collision_Delta1(TTrans,Mm_s,R0,lnOmega11_sl);
lnDelta2_sl  = ...
    getPair_Collision_Delta2(TTrans,Mm_s,R0,lnOmega22_sl);

% Exponentiate -- logarithmic form is not needed for the GuptaYos90 approx.
Delta1_sl = exp(lnDelta1_sl);
Delta2_sl = exp(lnDelta2_sl);

% Calculate mole fractions
Mm  = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);
X_s = getSpecies_X(y_s,Mm,Mm_s);

% Calculate translational component of thermal conductivity
kappaTrans = ...
    getMixture_kappaTrans_Brokaw58(X_s,Delta2_sl,Mm_s,kBoltz);

% Calculate internal energy components of thermal conductivity
[kappaInt,kappaRot,kappaVib,kappaElec] = ...
    getMixture_kappaInt_GuptaYos(X_s,cpRot_s,cpVib_s,cpElec_s,...
    Mm_s,R0,kBoltz,Delta1_sl);

% Frozen thermal conductivity of the mixture
switch numberOfTemp
    case '1T'
        kappa = kappaTrans + kappaInt;
        kappav = [];
    case '2T'
        kappa = kappaTrans + kappaRot;
        kappav = kappaVib + kappaElec;
    otherwise
        error(['the chosen number of temperatures ''',numberOfTemp,''' is not supported']);
end

% Variable outputs
if nargout>2
    oc = 1;                   % output counter
    varargout{oc} = kappaTrans; oc = oc+1;
    varargout{oc} = kappaInt;   oc = oc+1;
    varargout{oc} = kappaRot;   oc = oc+1;
    varargout{oc} = kappaVib;   oc = oc+1;
    varargout{oc} = kappaElec; %oc = oc+1;
end

end % getMixture_kappa_Brokaw58.m
