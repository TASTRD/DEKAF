function [prop] = applyChapmanEnskog(choiceTransport,varargin)
% applyChapmanEnskog computes transport properties solving the
% Chapman-Enskog matrix system.
%
% Examples:
%   (1) mu = applyChapmanEnskog(11,y_s,TTrans,Tel,rho,p,Mm_s,Mm,consts_Omega11,...
%       |           consts_Omega22,nAvogadro,R0,kBoltz,bElectron,...
%       |           qEl,epsilon0,bPos);
%       |--> computes the first approximation of the dynamic viscosity
%
%   (2) kappaHeavy = applyChapmanEnskog(22,y_s,TTrans,Tel,rho,pMm_s,Mm,consts_Omega11,...
%       |                      consts_Omega22,nAvogadro,R0,kBoltz,bElectron,...
%       |                      qEl,epsilon0,bPos,consts_Bstar);
%       |--> computes the second approximation of the heavy-particle
%       thermal conductivity
%
% Inputs:
%   choiceTransport     integer to specify which transport property will be
%                       evaluated. given below are the possibilities:
%                       11    -   dynamic viscosity 1st approx
%                       21    -   frozen heavy particle translational
%                             thermal conductivity 1st approx
%                       22    -   frozen heavy particle translational
%                             thermal conductivity 2nd approx
%
%  The following inputs are needed for all choiceTransport
%
%   y_s            mass fractions of species s                          [-]
%                  matrix of size (N_eta x N_spec)
%   TTrans         translational temperature                            [K]
%                  vector of size (N_eta x 1)
%   Tel            electron temperature                                 [K]
%                  vector of size (N_eta x 1)
%   rho            mixture density                                  [kg/m3]
%                  vector of size (N_eta x 1)
%   p              static pressure                                     [Pa]
%                  vector of size (N_eta x 1)
%   Mm_s           species molecular molar mass.                   [kg/mol]
%                  vector of size (N_spec x 1)
%   Mm             mixture molecular molar mass.                   [kg/mol]
%                  vector of size (N_eta x 1)
%
%   consts_Omega11         Curve fit coefficients to
%                           calculate Omega11_sl, the average collision
%                           cross-sectional area of two-body pair, species
%                           s & l, order (1,1).
%                           struct with fields .A, .B, .C, .D where each
%                           field is a matrix of size (N_s x N_l).
%   consts_Omega22         Curve fit coefficients to
%                           calculate Omega22_sl, the average collision
%                           cross-sectional area of two-body pair, species
%                           s & l, order (2,2).
%                           struct with fields .A, .B, .C, .D where each
%                           field is a matrix of size (N_s x N_l).
%   nAvogadro       Avogradro's constant.                        [part/mol]
%   R0              universal gas constant
%   kBoltz          Boltzmann's constant
%   bElectron       electron / non-electron boolean size (N_spec x 1)
%   qEl             elementary charge (1 x 1)
%   epsilon0        permittivity of vacuum (1 x 1)
%   bPos            boolean for positively charged particles (N_spec x 1)
%   options         necessary fields
%                       flow
%   dXs_dT          N_eta x N_spec
%   dXs_dT2         N_eta x N_spec
%
%  Additional inputs are needed for 21 & 22 only (thermal conductivity)
%
%   consts_Bstar   Curve fit coefficients to
%                  calculate Bstar for a given species pair (s,l).
%                  See eval_Fit_PolyLogLog.m
%                  struct with fields .A, .B, .C, .D where each
%                  field is a matrix of size (N_s x N_l).
%
% Output:
%   mu/kappa       transport property values, as specified by
%                  choiceTransport
%                  vector of size (N_eta x 1)
%
% References:
% Gupta,  R. N.,  Yos,  J. M.,  Thompson,  R. A.,  and Lee,  K.-P.,  "A
%   Review of Reaction Rates and Thermodynamic and Transport Properties for
%   an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%   Calculations to 30000 K," Nasa-rp-1232, National Aeronautics and Space
%   Administration, 1990.
% Magin, T. E. and Degrez, G., "Transport of partially ionized and
%   unmagnetized plasmas," Physical Review E, Vol. 70, 2004.
% Scoggins, J. (2017). Development of numerical methods and study of
%   coupled flow, radiation, and ablation phenomena for atmospheric entry.
%   von Karman Institute.
%
% Children and related functions:
%   See also getPair_lnOmegaij, getPair_Collision_Delta1.m,
%            getPair_Collision_Delta2.m,
%            getMixture_R.m, getPair_Collision_constants.m,
%            eval_Fit.m getUnits_convStrings.m
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

ic = 1; % Input counter from varargin struct
% Extract inputs
y_s             = varargin{ic}; ic = ic + 1;
TTrans          = varargin{ic}; ic = ic + 1;
Tel             = varargin{ic}; ic = ic + 1;
rho             = varargin{ic}; ic = ic + 1;
p               = varargin{ic}; ic = ic + 1;
Mm_s            = varargin{ic}; ic = ic + 1;
Mm              = varargin{ic}; ic = ic + 1;
consts_Omega11  = varargin{ic}; ic = ic + 1;
consts_Omega22  = varargin{ic}; ic = ic + 1;
nAvogadro       = varargin{ic}; ic = ic + 1;
R0              = varargin{ic}; ic = ic + 1;
kBoltz          = varargin{ic}; ic = ic + 1;
bElectron       = varargin{ic}; ic = ic + 1;
qEl             = varargin{ic}; ic = ic + 1;
epsilon0        = varargin{ic}; ic = ic + 1;
bPos            = varargin{ic};ic = ic + 1;
% DEVELOPER'S NOTE: Whichever new variables are passed in must be before
% flow (see Usage 1.1 & 2.1 and the call to getPair_lnOmegaij below)

% Get mole fractions of species s
X_s = getSpecies_X(y_s,Mm,Mm_s);

% repmatting and reshaping
[N_eta,N_spec] = size(y_s);
Mm_s2D = repmat(reshape(Mm_s,[1,N_spec]),[N_eta,1]);
Mm_s3D = repmat(reshape(Mm_s,[1,N_spec,1]),[N_eta,1,N_spec]);
Mm_l3D = repmat(reshape(Mm_s,[1,1,N_spec]),[N_eta,N_spec,1]);
X_s3D = repmat(X_s,[1,1,N_spec]);
X_l3D = repmat(reshape(X_s,[N_eta,1,N_spec]),[1,N_spec,1]);

% Collision integrals
[lnOmega11_sl,Tred]   = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega11,bPos);
lnOmega22_sl          = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega22,bPos);
lnOmega22_sl2D = reshape(lnOmega22_sl,[N_eta,N_spec^2]);                    % (eta,s,l) --> (eta,s*l)
lnOmega22_ss   = lnOmega22_sl2D(:, 1:(N_spec+1):N_spec^2);                  % obtaining only the diagonal (positions 1, N_s+1, 2N_s+2, ... , N_s^2)
Astar_sl = exp(lnOmega22_sl - lnOmega11_sl);                                % (Magin 2004 eq. A.4a)

% calculating shear viscosity coefficients (Magin 2004 eq. A.5)
eta_s = getSpecies_muKinetic(TTrans,lnOmega22_ss,Mm_s,kBoltz,nAvogadro);

% calculating binary diffusion coefficients
[~,nDb_sl] = getPair_nD_binary_FOCE(TTrans,Tel,y_s,rho,consts_Omega11,...
    Mm_s,kBoltz,nAvogadro,bElectron,qEl,epsilon0,bPos);

% making sure we work only with heavy particles
bHeavy = ~bElectron;
idxHeavy = find(bHeavy);
N_heavy = sum(bHeavy);

switch choiceTransport
    case 11 % viscosity first approximation
        % building G (Magin 2004 eq. C.1a & C.1b)
        fac = X_s3D.*X_l3D*nAvogadro./(nDb_sl.*(Mm_s3D+Mm_l3D));                    % factor common to the diagonal and off-diagonal terms
        GDiag = zeros(N_eta,N_spec);                                               % allocating
        for ii=1:N_heavy                                                            % looping species
            s = idxHeavy(ii);                                                           % index in the rest of the terms
            lNotS = idxHeavy;                                                           % building vector of heavy species excluding the looped
            lNotS(ii) = [];
            term3D = fac(:,s,lNotS).*(6/5*Mm_l3D(:,s,lNotS)./Mm_s3D(:,s,lNotS)...
                .*Astar_sl(:,s,lNotS)+2);                                               % term in the sumation over l \neq s
            term2D = permute(term3D,[1,3,2]);                                           % (eta,1,l) --> (eta,l)
            GDiag(:,s) = term2D*ones(N_heavy-1,1);                                      % summing over l \neq s
        end
        GDiag = GDiag+X_s.^2./eta_s;                                                % adding missing term to the diagonal
        GOffDiag = fac.*(6/5*Astar_sl-2);                                           % defining off-diagonal terms
    case 22 % heavy-particle thermal conductivity
        % Obtaining Bstar
        consts_Bstar = varargin{ic};%    ic = ic + 1;
        lnBstar_sl = getPair_ABCstar(Tred,consts_Bstar);
        Bstar_sl = exp(lnBstar_sl);
        % Building G (Magin 2004 eq. C.1c & C.1d)
        fac = X_s3D.*X_l3D./(25*kBoltz.*nDb_sl).*Mm_s3D.*Mm_l3D./(Mm_s3D+Mm_l3D).^2; % factor common to the diagonal and off diagonal terms
        bracket = 30*Mm_s3D./Mm_l3D + 25*Mm_l3D./Mm_s3D - 12*Mm_l3D./Mm_s3D.*Bstar_sl + 16*Astar_sl;
        GDiag = zeros(N_eta,N_spec);                                               % allocating
        for ii=1:N_heavy                                                            % looping species
            s = idxHeavy(ii);                                                           % index in the rest of the terms
            lNotS = idxHeavy;                                                           % building vector of heavy species excluding the looped
            lNotS(ii) = [];
            term3D = fac(:,s,lNotS).*bracket(:,s,lNotS);                                % term in the sumation over l \neq s
            term2D = permute(term3D,[1,3,2]);                                           % (eta,1,l) --> (eta,l)
            GDiag(:,s) = term2D*ones(N_heavy-1,1);                                      % summing over l \neq s
        end
        GDiag = GDiag + 4/(15*kBoltz)*X_s.^2.*Mm_s2D./(nAvogadro*eta_s);
        GOffDiag = fac.*(16*Astar_sl+12*Bstar_sl-55);
    otherwise
        error(['the chosen transport property identified with ''',num2str(choiceTransport),''' is not supported']);
end
GDiag = GDiag(:,bHeavy);                                                    % keeping only the heavies
GOffDiag = GOffDiag(:,bHeavy,bHeavy);
GOffDiag2D = reshape(GOffDiag,[N_eta,N_heavy^2]);                           % (eta,s,l) --> (eta,s*l)
GOffDiag2D(:,1:(N_heavy+1):N_heavy^2) = GDiag;                              % assigning diagonal values
G = reshape(GOffDiag2D,[N_eta,N_heavy,N_heavy]);                            % (eta,s*l) --> (eta,s,l)

% solving G (Scoggins Thesis eq. 4.44)
G_prmt = permute(G,[2,3,1]);                                                % (eta,s,l) --> (s,l,eta)
Xs_prmt = permute(X_s(:,bHeavy),[2,3,1]);                                   % (eta,s) --> (s,1,eta)
XsT_prmt = permute(X_s(:,bHeavy),[3,2,1]);                                  % (eta,s) --> (1,s,eta)
O_prmt = zeros(1,1,N_eta);
GX_prmt = [[G_prmt,Xs_prmt];[XsT_prmt,O_prmt]];                             % concatenating matrices

%%%% BETTER ALGORITHM GOES HERE
prop = zeros(N_eta,1); % allocating
for it = 1:N_eta
    prop(it) = -det(GX_prmt(:,:,it))/det(G_prmt(:,:,it));
end
%%%% UNTIL HERE
end