function [transport] = applyYos67(choiceTransport,varargin)
% applyYos67 Use the approximation of Chapman Enskog proposed by Yos to
% calculate a mixture transport property, either the mixture dynamic
% viscosity or the heavy-particle frozen thermal conductivity.
%
% Usage:
% 11. Dynamic viscosity first approx.
% mu = ...
%     applyYos67(11,y_s,TTrans,Tel,rho,p,Mm_s,Mm,R0,consts_Omega11,...
%       consts_Omega22,nAvogadro,kBoltz,bElectron,qEl,epsilon0,bPos);
%
% 21. Frozen thermal conductivity first approx.
% kappa = ...
%     applyYos67(21,y_s,TTrans,Tel,rho,Mm_s,Mm,R0,consts_Omega11,...
%       consts_Omega22,nAvogadro,kBoltz,bElectron,qEl,epsilon0,bPos,consts_Bstar);
%
% 22. Frozen thermal conductivity second approx.
% kappa = ...
%     applyYos67(22,y_s,TTrans,Tel,rho,Mm_s,Mm,R0,consts_Omega11,...
%       consts_Omega22,nAvogadro,kBoltz,bElectron,qEl,epsilon0,bPos,consts_Bstar);
%
%
%  [...] = applyYos67(...,options);
%   pass in an options struct as the input after bPos if 11 usage, or after
%   consts_Bstar if 21|22 usage
%
% Inputs:
%   choiceTransport     integer to specify which transport property will be
%                       evaluated. given below are the possibilities:
%                       11    -   dynamic viscosity 1st approx
%                       21    -   frozen heavy particle translational
%                             thermal conductivity 1st approx
%                       22    -   frozen heavy particle translational
%                             thermal conductivity 2nd approx
%
%  The following inputs are needed for all choiceTransport
%
%   y_s            mass fractions of species s                          [-]
%                  matrix of size (N_eta x N_spec)
%   TTrans         translational temperature                            [K]
%                  vector of size (N_eta x 1)
%   Tel            electron temperature                                 [K]
%                  vector of size (N_eta x 1)
%   rho            mixture density                                  [kg/m3]
%                  vector of size (N_eta x 1)
%   p              static pressure                                     [Pa]
%                  vector of size (N_eta x 1)
%   Mm_s           species molecular molar mass.                   [kg/mol]
%                  vector of size (N_spec x 1)
%   Mm             mixture molecular molar mass.                   [kg/mol]
%                  vector of size (N_eta x 1)
%   R0             Universal gas constant.                      [J/(K*mol)]
%
%   consts_Omega11         Curve fit coefficients to
%                           calculate Omega11_sl, the average collision
%                           cross-sectional area of two-body pair, species
%                           s & l, order (1,1).
%                           struct with fields .A, .B, .C, .D where each
%                           field is a matrix of size (N_s x N_l).
%   consts_Omega22         Curve fit coefficients to
%                           calculate Omega22_sl, the average collision
%                           cross-sectional area of two-body pair, species
%                           s & l, order (2,2).
%                           struct with fields .A, .B, .C, .D where each
%                           field is a matrix of size (N_s x N_l).
%   nAvogadro       Avogradro's constant.                        [part/mol]
%   kBoltz          Boltzmann's constant
%   bElectron       electron / non-electron boolean size (N_spec x 1)
%   qEl             elementary charge (1 x 1)
%   epsilon0        permittivity of vacuum (1 x 1)
%   bPos            boolean for positively charged particles (N_spec x 1)
%   options         necessary fields
%                       Yos67_sumHeavy
%
%  Additional inputs are needed for 21 & 22 only (thermal conductivity)
%
%   consts_Bstar   Curve fit coefficients to
%                  calculate Bstar for a given species pair (s,l).
%                  See eval_Fit.m
%                  struct with fields .A, .B, .C, .D where each
%                  field is a matrix of size (N_s x N_l).
%
% Output:
%   transport      transport property values, as specified by
%                  choiceTransport
%                  vector of size (N_eta x 1)
%
% Notes:
%   Transport properties are the approximations of the first approximation
%   of the Chapman-Enskog equations.
%
%   Comments in this function refer to equations in the paper written by
%   Gupta et al., 1990. The citation is given below under the section,
%   "References".
%
%   N_eta is the number of elements in the spatial domain.
%   N_spec, N_s, N_l are the numbers of species in the mixture. All of
%   these variables have the same numerical value.
%
%   The natural logarithms of several quantities are computed to avoid
%   MATLAB's integer overflow
%
% References:
% Gupta,  R. N.,  Yos,  J. M.,  Thompson,  R. A.,  and Lee,  K.-P.,  "A
%   Review of Reaction Rates and Thermodynamic and Transport Properties for
%   an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%   Calculations to 30000 K," Nasa-rp-1232, National Aeronautics and Space
%   Administration, 1990.
% Magin, T. E. and Degrez, G., "Transport of partially ionized and
%   unmagnetized plasmas," Physical Review E, Vol. 70, 2004.
% Scoggins, J. (2017). Development of numerical methods and study of
%   coupled flow, radiation, and ablation phenomena for atmospheric entry.
%   von Karman Institute.
%
% Children and related functions:
%   See also getPair_lnOmegaij, getPair_Collision_Delta1.m,
%            getPair_Collision_Delta2.m,
%            getMixture_R.m, getPair_Collision_constants.m,
%            eval_Fit.m getUnits_convStrings.m
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

ic = 1; % Input counter from varargin struct
% Extract inputs
y_s             = varargin{ic}; ic = ic + 1;
TTrans          = varargin{ic}; ic = ic + 1;
Tel             = varargin{ic}; ic = ic + 1;
rho             = varargin{ic}; ic = ic + 1;
p               = varargin{ic}; ic = ic + 1;
Mm_s            = varargin{ic}; ic = ic + 1;
Mm              = varargin{ic}; ic = ic + 1;
R0              = varargin{ic}; ic = ic + 1;
consts_Omega11  = varargin{ic}; ic = ic + 1;
consts_Omega22  = varargin{ic}; ic = ic + 1;
nAvogadro       = varargin{ic}; ic = ic + 1;
kBoltz          = varargin{ic}; ic = ic + 1;
bElectron       = varargin{ic}; ic = ic + 1;
qEl             = varargin{ic}; ic = ic + 1;
epsilon0        = varargin{ic}; ic = ic + 1;
bPos            = varargin{ic}; ic = ic + 1;

% Get mole fractions of species s
X_s = getSpecies_X(y_s,Mm,Mm_s);

% Continue reshaping to (N_eta x N_s x N_l)
[N_eta,N_spec] = size(y_s);
Mm_s3D = repmat(reshape(Mm_s,[1,N_spec,1]),[N_eta,1,N_spec]);
Mm_l3D = repmat(reshape(Mm_s,[1,1,N_spec]),[N_eta,N_spec,1]);

% Get (1,1) and (2,2) collision integrals to calculate the Deltas, as
% defined by Gupta Yos et al., 1990.
[lnOmega11_sl,Tred]  = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega11,bPos);
lnOmega22_sl         = getPair_lnOmegaij(TTrans,Tel,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega22,bPos);
lnDelta1_sl  = getPair_Collision_Delta1(TTrans,Mm_s,R0,lnOmega11_sl);
lnDelta2_sl  = getPair_Collision_Delta2(TTrans,Mm_s,R0,lnOmega22_sl);

 switch choiceTransport
    case 11 % mu, dynamic viscosity 1st approx

        % Equations (32a,b)
        % B_sl and a_sl are size (N_eta x N_s x N_l)
        a_sl = nAvogadro*(2*exp(lnDelta1_sl)-exp(lnDelta2_sl))./(Mm_s3D+Mm_l3D);
        B_sl = nAvogadro*exp(lnDelta2_sl)./Mm_s3D;

    case 21 % kappaHeavy, translational thermal conductivity of heavy particles 1st approx

        % Extract components from input struct unique to thermal conductivity
        consts_Bstar = varargin{ic};    ic = ic + 1;

        % Calculate Bstar, the ratio of collision cross-sectional area mixed-order terms.
        lnBstar_sl = getPair_ABCstar(Tred,consts_Bstar);
        % lnBstar_sl is initialized as size N_eta x N_s x N_l

        % Equations (33a,b)
        a_sl = 2/(15*kBoltz)*(Mm_s3D.*Mm_l3D)./(Mm_s3D + Mm_l3D).^2 .* ...
            ((33/2 - 18/5*exp(lnBstar_sl)).*exp(lnDelta1_sl) - 4*exp(lnDelta2_sl));
        B_sl = 2./(15*kBoltz*(Mm_s3D + Mm_l3D).^2) .* ...
            (8*Mm_s3D.*Mm_l3D.*exp(lnDelta2_sl) + (Mm_s3D - Mm_l3D).* ...
            (9*Mm_s3D - 15/2*Mm_l3D + 18/5*exp(lnBstar_sl).*Mm_l3D).*exp(lnDelta1_sl));

    case 22 % kappaHeavy, translational thermal conductivity of heavy particles 2nd approx

        % Extract components from input struct unique to thermal conductivity
        consts_Bstar = varargin{ic};    ic = ic + 1;

        % Calculate Bstar and Astar, the ratio of collision cross-sectional area mixed-order terms.
        lnBstar_sl = getPair_ABCstar(Tred,consts_Bstar);
        Bstar_sl = exp(lnBstar_sl);
        Astar_sl = exp(lnOmega22_sl - lnOmega11_sl);

        % calculation of nD_sl_b
        [~,nD_sl_b] = getPair_nD_binary_FOCE(TTrans,Tel,y_s,rho,consts_Omega11,...
               Mm_s,kBoltz,nAvogadro,bElectron,qEl,epsilon0,bPos);

        % reshaping etc.
        Mm_s2D = repmat(Mm_s',[N_eta,1]);                           % (s,1) --> (eta,s)
        TTrans_2D = repmat(TTrans,[1,N_spec]);                      % (eta,1) --> (eta,s)
        lnOmega22_sl2D = reshape(lnOmega22_sl,[N_eta,N_spec^2]);    % (eta,s,l) --> (eta,s*l)
        lnOmega22_ss    = lnOmega22_sl2D(:, 1:(N_spec+1):N_spec^2); % obtaining only the diagonal (positions 1, N_s+1, 2N_s+2, ... , N_s^2)
        delta_sl3D = repmat(reshape(eye(N_spec),[1,N_spec,N_spec]),[N_eta,1,1]); % (s,l) --> (eta,s,l)

        % calculating shear viscosity coefficients
        eta_s = 5/16*sqrt(pi*kBoltz*TTrans_2D.*Mm_s2D/nAvogadro)./(pi*exp(lnOmega22_ss));
        eta_s3D = repmat(eta_s,[1,1,N_spec]); % (eta,s) --> (eta,s,l)

        % Derived by us from the expressions in Magin 2004
        m_frac = Mm_s3D.*Mm_l3D./(Mm_s3D+Mm_l3D).^2;
        a_sl = 1./(25*kBoltz.*nD_sl_b) .* m_frac .* (55-12*Bstar_sl-16*Astar_sl);
        B_sl = 1/(25*kBoltz) * (delta_sl3D.*(20/3*Mm_s3D./(nAvogadro*eta_s3D) - 8*Astar_sl./nD_sl_b) + ...
            1./nD_sl_b.*m_frac.*(30*(Mm_s3D./Mm_l3D-1) + 25*(Mm_l3D./Mm_s3D-1) - 12*(Mm_l3D./Mm_s3D-1).*Bstar_sl + 32*Astar_sl));
    otherwise
        error(['the input choiceTransport=',num2str(choiceTransport),' is not supported.']);
 end

if length(varargin)>=ic
    options = varargin{ic};   %ic = ic + 1;
    Yos67_sumHeavy = options.Yos67_sumHeavy;
else
    Yos67_sumHeavy = false;
end


% check if the user wants to sum over the heavies or all species
if Yos67_sumHeavy
    bHeavy = ~bElectron;
else
    bHeavy = true(N_spec,1);
    % variable name is incorrect. we are summing over all species with this
    % choice.
end
N_heavy = nnz(bHeavy);
B_slH = B_sl(:,bHeavy,bHeavy);
a_slH = a_sl(:,bHeavy,bHeavy);
X_sH = X_s(:,bHeavy);
X_lH3D = repmat(reshape(X_sH,[N_eta,1,N_heavy]),[1,N_heavy,1]);

% Equation (31b)
A_s = zeros(N_eta,N_heavy);
for l=1:N_heavy
    A_s = A_s + X_lH3D(:,:,l).*B_slH(:,:,l);
end
% A_s is initialized to N_eta x N_heavy
A_l_mat = repmat(reshape(A_s,[N_eta,1,N_heavy]),[1,N_heavy,1]);

% Equation (31a)
num_s = zeros(N_eta,N_heavy);
denom_s = zeros(N_eta,N_heavy);
for l=1:N_heavy
    tmp = X_sH.*X_lH3D(:,:,l).*(1./A_s - 1./A_l_mat(:,:,l)).^2;
    denom_s = denom_s + tmp;
    num_s = num_s + tmp.*a_slH(:,:,l);
end
denom   = denom_s * ones(N_heavy,1);
num     = num_s * ones(N_heavy,1);
a_av    = num./denom; % size N_eta x 1
a_av2D  = repmat(a_av,[1,N_heavy]); % N_eta x N_heavy

% Equation (30)
mySum       = (X_sH./(A_s+a_av2D)) * ones(N_heavy,1); % N_eta x 1
transport   = mySum./(1-a_av.*mySum);

end % applyYos67
