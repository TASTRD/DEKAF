function mu_s = getSpecies_mu_Blottner(T,Amu_s,Bmu_s,Cmu_s,N_spec,varargin)
% GETSPECIES_MU_BLOTTNER Calculate species dynamic viscosity with
% Blottner's 1971 curve fit
%
% Usage:
%   (1)
%       mu_s = getSpecies_mu_Blottner(T,Amu_s,Bmu_s,Cmu_s,N_spec)
%       |--> Blottner's 3-coefficient curve fits
%
%   (2)
%       mu_s = getSpecies_mu_Blottner(T,Amu_s,Bmu_s,Cmu_s,N_spec,Dmu_s,Emu_s)
%       |--> Gupta's 5-coefficient extension to Blottner's curve fits
%
% Inputs:
%   T               -   temperature, vector of size (N_eta x 1)         [K]
%   Amu_s, Bmu_s,   -   coefficients in the Blottner curve fit for the
%   Cmu_s, Dmu_s,       dynamic viscosity [SI].
%   Emu_s               matrices of size (N_spec x 1)
%   N_spec          -   number of species considered in the reacting
%                       mixture. scalar
%
% Notes:
%   - Assume Blottner's curve fit for species dynamic viscosity
%   - Blottner proposed the curve fit with 0.1*exp(...), howecer, here it
%   is implemented with the 0.1* absorbed into the Cmu_s coefficient, which
%   is Blottner's Cmu_s plus log(0.1)
%
% References:
%   Blottner, F.G., Johnson, M. and Ellis, M., �Chemically Reacting Viscous
%       Flow Program for Multi-Component Gas Mixtures,� Report NO.
%       SC-RR-70-754, Sandia Laboratories, Albuquerque, New Mexico, 1971.
%   Gupta, R. N., Yos, J. M., Thompson, R. A., & Lee, K.-P. (1990). A
%       review of Reaction Rates and Thermodynamic and Transport Properties
%       for an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%       Calculations to 30000K.
%
% Children and related functions:
%   See also getSpecies_constants.m, listOfVariablesExplained
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

% Reshape to N_eta x N_spec
N_eta = length(T);
lnT   = repmat(log(T),[1,N_spec]);
Amu_s = repmat(Amu_s',[N_eta,1]);
Bmu_s = repmat(Bmu_s',[N_eta,1]);
Cmu_s = repmat(Cmu_s',[N_eta,1]);

% Species viscosity
if isempty(varargin)
    mu_s = exp(Amu_s.*lnT.^2 + Bmu_s.*lnT + Cmu_s);
else
    Dmu_s = varargin{1};
    Emu_s = varargin{2};
    Dmu_s = repmat(Dmu_s',[N_eta,1]);
    Emu_s = repmat(Emu_s',[N_eta,1]);
    mu_s = exp(Amu_s.*lnT.^4 + Bmu_s.*lnT.^3 + Cmu_s.*lnT.^2 + Dmu_s.*lnT + Emu_s);
end

end % getSpecies_mu_Blottner.m
