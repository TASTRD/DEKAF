function [Q,Q_s] = getMixture_mu_SutherlandWilke(y_s,TTrans,Tel,Mm_s,muRef_s,TmuRef_s,Smu_s,R0,bElectron,varargin)
% getMixture_mu_SutherlandWilke Calculate the mixture dynamic viscosity using
% Blottner's 1971 curve fit and Wilke's mixing rule
%
% Usage:
%   (1)
%       [mu,mu_s] = getMixture_mu_SutherlandWilke(y_s,TTrans,Tel,Mm_s,muRef_s,TmuRef_s,Smu_s,R0,bElectron);
%
%   (2)
%       [kappa,kappa_s] = getMixture_mu_SutherlandWilke(y_s,TTrans,Tel,Mm_s,muRef_s,TmuRef_s,Smu_s,R0,bElectron,...
%                                                           'kappa',kappaRef_s,TkappaRef_s,Skappa_s);
%       |--> computes also the mixture thermal conductivity using the
%       Blottner-like curve fits proposed by Gupta et al.
%
% Inputs:
%   y_s
%       species mass fractions, matrix of size (N_eta x N_spec)
%   TTrans
%       translational temperature, vector of size (N_eta x 1)
%   Tel
%       electron temperature, vector of size (N_eta x 1)
%   Mm_s
%       species mixture molar mass, matrix of size (N_spec x 1)
%   muRef_s, TmuRef_s, Smu_s
%       coefficients in Sutherland's law for the dynamic viscosity (N_spec x 1)
%   kappaRef_s, TkappaRef_s, Skappa_s
%       same story but for the thermal conductivity (N_spec x 1)
%   R0          -   universal gas constant
%   bElectron   -   electron boolean (N_spec x 1)
%
% Outputs:
%   mu          -   mixture dynamic viscosity (N_eta x 1)
%   mu_s        -   species dynamic viscosity (N_eta x N_spec)
%   kappa       -   mixture thermal conductivity (N_eta x 1)
%   kappa_s     -   species thermal conductivity (N_eta x N_spec)
%
% Notes:
%   Apply Wilke's mixing rule and Blottner's curve fit
%
% References:
%   Wilke, C., "A Viscosity Equation for Gas Mixtures" The Journal of
%       Chemical Physics, Vol. 18, No. 4, 1950
%
% Children and related functions:
%   See also getMixture_Mm_R.m, getSpecies_X.m, applySutherland.m,
%            eval_func_Heavy_el.m, applyWilkes.m, listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

if ~isempty(varargin) && strcmp(varargin{1},'kappa');   bKappa = true;
else;                                                   bKappa = false;
end

% reshaping bElectron
bHeavy = ~bElectron; % heavy particles

% mixture molar mass and mixture gas constant
Mm = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);

% mole fraction of each species
X_s = getSpecies_X(y_s,Mm,Mm_s);

% species dynamic viscosity using Blottner's curve fit
mu_s = applySutherland(TTrans,muRef_s,TmuRef_s,Smu_s);

% Wilke's mixing rule
if bKappa
    ic=2;
    kappaRef_s  = varargin{ic}; ic=ic+1;
    TkappaRef_s = varargin{ic}; ic=ic+1;
    Skappa_s    = varargin{ic}; %ic=ic+1;
    kappa_s = applySutherland(TTrans,kappaRef_s,TkappaRef_s,Skappa_s);
    Q = applyWilkes(X_s,          kappa_s,       mu_s,          Mm_s);
    Q_s = kappa_s;
else
    Q = applyWilkes(X_s(:,bHeavy),mu_s(:,bHeavy),mu_s(:,bHeavy),Mm_s(bHeavy));
    Q_s = mu_s;
end

end % getMixture_mu_SutherlandWilke
