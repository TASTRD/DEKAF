function [kappaTrans,dkappaTrans_dT] = ...
    getMixture_kappaTrans_Brokaw58(X_s,Delta2_sl,Mm_s,kBoltz,varargin)
%getMixture_kappaTrans_Brokaw58 Calculate the translational component of
% frozen thermal conductivity with the approximation given by Gupta Yos et
% al, 1990.
%
% FIXME: COMMENTS
%
% Usage:
% 1. Calculate the translational component of frozen thermal conductivity
%
%   kappaTrans = ...
%       getMixture_kappaTrans_Brokaw58(X_s,Delta2_sl,Mm_s,kBoltz);
%
% 2. Calculate the translational component of frozen thermal conductivity
%    and its derivative with respect to temperature.
%
%   [kappaTrans,dkappaTrans_dT] = ...
%       getMixture_kappaTrans_Brokaw58(X_s,Delta2_sl,Mm_s,kBoltz,...
%           dXs_dT,dlnDelta2sl_dT);
%
% Inputs:
%   X_s                 mole fractions (N_eta x N_spec)
%   Delta2_sl           Reduced collision integral delta2 term, eqn. (35)
%                       (N_eta x N_s x N_l)
%   Mm_s                molar mass of species (N_eta x N_spec)
%   kBoltz              boltzmann constant
%   varargin:
%       dXs_dT          derivative of mole fraction with respect to
%                       temperature (N_eta x N_s)
%       dlnDelta2sl_dT  derivative of lnDelta2 with respect to
%                       temperature (N_eta x N_s x N_l)
%
% Outputs:
%   kappaTrans          translational component of mixture frozen thermal
%                       conductivity [W/m-K](N_eta x 1)
%   dkappaTrans_dT      derivative of translational component of mixture
%                       frozen thermal conductivity with respect to
%                       translational temperature [W/m-K-K]
%
% Notes:
%
%   Comments in this function refer to equations in the paper written by
%   Gupta, Yos et al., 1990. The citation is given below under the section,
%   "References".
%
% References:
% Gupta,  R. N.,  Yos,  J. M.,  Thompson,  R. A.,  and Lee,  K.-P.,  "A
%   Review of Reaction Rates and Thermodynamic and Transport Properties for
%   an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%   Calculations to 30000 K," Nasa-rp-1232, National Aeronautics and Space
%   Administration, 1990.
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(X_s);

Mm_s3D = repmat(reshape(Mm_s,[1,N_spec,1]),[N_eta,1,N_spec]);
Mm_l3D = repmat(reshape(Mm_s,[1,1,N_spec]),[N_eta,N_spec,1]);

% Continue reshaping to (N_eta x N_s x N_l)
X_l3D  = repmat(reshape(X_s,[N_eta,1,N_spec]),[1,N_spec,1]);

% Equation (40b)
Mm_ratio = Mm_s3D./Mm_l3D;
alfa_sl = 1 + ((1-Mm_ratio).*(0.45-2.54*Mm_ratio)) ./ (1+Mm_ratio).^2;

% Equation (40a)
lSumAlfaXDelta2 = zeros(N_eta,N_spec);
for l=1:N_spec
    lSumAlfaXDelta2 = lSumAlfaXDelta2 + ...
        alfa_sl(:,:,l).*X_l3D(:,:,l).*Delta2_sl(:,:,l);
end

kappaTrans = 15/4*kBoltz * (X_s ./ lSumAlfaXDelta2) * ones(N_spec,1);

if nargout>1
    ic = 1;                         %input counter
    dXs_dT          = varargin{ic};  ic=ic+1;
    dlnDelta2sl_dT  = varargin{ic}; %ic=ic+1;

    dDelta2sl_dT = Delta2_sl .* dlnDelta2sl_dT;
    dXl_dT = repmat(reshape(dXs_dT,[N_eta,1,N_spec]),[1,N_spec,1]);

    % Calculate the derivative of Equation (40a)
    rhprodsum = zeros(N_eta,N_spec);
    for l=1:N_spec
        rhprodsum = rhprodsum + alfa_sl(:,:,l).*( ...
            dXl_dT(:,:,l).*Delta2_sl(:,:,l) + ...
            X_l3D(:,:,l).*dDelta2sl_dT(:,:,l));
    end
    lhterm = dXs_dT ./ lSumAlfaXDelta2;
    rhterm = -X_s ./ (lSumAlfaXDelta2).^2 .* rhprodsum;
    dkappaTrans_dT = ...
        15/4*kBoltz * (lhterm + rhterm) * ones(N_spec,1);
end

end % getMixture_kappaTrans_Brokaw58.m
