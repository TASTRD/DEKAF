function R_s = getSpecies_R(R0,Mm_s)
%getSpecies_R Calculate specific gas constant
%
% R_s = getSpecies_R(R0,Mm_s)
%
% Inputs:
%   Mm_s        -   molar mass of gas                              [kg/mol]
%   R0          -   universal gas constant                        [J/mol-K]
%
% Outputs:
%   R_s         -   specific gas constant                          [J/kg-K]
%
% Notes:
% This relation applies to both a mixture of gases or a single gas species,
%   provided that the Mm input corresponds to either a mixture or a
%   single gas species, respectively.
%
% Related functions:
%   See also getMixture_Mm_R.m
%
% GNU Lesser General Public License 3.0

R_s = R0./Mm_s;

end % getSpecies_R.m
