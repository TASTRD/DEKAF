function X_E = getElement_XE_from_ys(y_s,Mm_s,elemStoich_mat,varargin)
% getElement_XE_from_ys computes the elemental mole fractions from the
% species mass fractions.
%
% Usage:
%   (1)
%       X_E = getElement_XE_from_ys(y_s,Mm_s,elemStoich_mat)
%
%   (2)
%       X_E = getElement_XE_from_ys(y_s,Mm_s,elemStoich_mat,bChargeNeutrality,elem_list)
%
% Inputs and outputs:
%   y_s                 [-]         N_eta x N_spec
%       species mass fractions
%   elemStoich_mat      [-]         N_elem x N_spec
%       elemental stoichiometric matrix
%   Mm_s                [kg/mol]    N_spec x 1
%       species molar mass
%   bChargeNeutrality
%       boolean determining whether charge neutrality should be imposed or
%       not. This effectively means that the elemental fraction of e- must
%       be identically zero.
%   elem_list                       N_elem x 1
%       cell of strings with the names of the different elements
%   X_E                 [-]         N_eta x N_elem
%       species mole fractions
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if isempty(varargin);   bChargeNeutrality = false;
else;                   bChargeNeutrality = varargin{1};
end 

N_elem = size(elemStoich_mat,1);
N_eta = size(y_s,1);

Mm  = getMixture_Mm_R(y_s,[],[],Mm_s,[],[]);    % obtaining mixture molar mass
X_s = getSpecies_X(y_s,Mm,Mm_s);                % mole fraction

X_s3D            = repmat(permute(X_s,           [1,3,2]),[1,    N_elem,1]);    % (eta,s) --> (eta,E,s)
elemStoich_mat3D = repmat(permute(elemStoich_mat,[3,1,2]),[N_eta,1,     1]);    % (E,s) ----> (eta,E,s)

X_E_prov = sum(X_s3D.*elemStoich_mat3D , 3);    % summing over species

Xnorm = X_E_prov*ones(N_elem,1);
X_E   = X_E_prov./repmat(Xnorm,[1,N_elem]);

if bChargeNeutrality
    X_E = enforce_chargeNeutralityCond_elements(X_E,varargin{2});
end

end % getElement_XE_from_ys