function [p_e,rho_e] = getInviscid_p_rho_Bernouilli_fromMach(M_e,gam_e,TTrans_e,Rstar_e,Dx,Ix,p_e0,EoS_params,options)
% getInviscid_p_rho_Bernouilli_fromMach computes the mixture pressure and
% density at the boundary-layer edge by integrating the inviscid Bernouilli
% equation in terms of the Mach number.
%
% Usage:
%   (1)
%       [p_e,rho_e] = getInviscid_p_rho_Bernouilli_fromMach(M_e,gam_e,TTrans_e,Rstar_e,Dx,Ix,p_e0,EoS_params,options)
%
% Inputs:
%   M_e         [-]                 (Nx x 1)
%       Mach number at the boundary-layer edge
%   gam_e       [-]                 (Nx x 1)
%       ratio of specific heats at the BL edge
%   TTrans_e    [K]                 (Nx x 1)
%       static translational temperature at the boundary-layer edge
%   Rstar_e     [J/kg-K]            (Nx x 1)
%       gas constant at the boundary-layer edge
%   Dx          [1/m]               (Nx x Nx)
%       streamwise differentiation matrix
%   Ix          [1/m]               (Nx x Nx)
%       integration matrix in the streamwise direction
%   p_e0        [Pa]                (1 x 1)
%       mixture static pressure at the initial streamwise station
%   EoS_params
%       structure containing any additional parameters necessary for the
%       evaluation of the equation of state
%   options
%       classic DEKAF options structure
%
% Outputs:
%   p_e         [Pa]                (Nx x 1)
%       mixture static pressure at the boundary-layer edge
%   rho_e       [kg/m^3]            (Nx x 1)
%       mixture density at the boundary-layer edge
%
% IMPORTANT! This function is compatible with non-ideal EoS
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% initial guess assuming ideal-gas equation of state
U_e = sqrt(gam_e.*Rstar_e.*TTrans_e).*M_e;
bernoulliInt = -U_e./(Rstar_e.*TTrans_e) .* (Dx*U_e);
p_e = p_e0 .* exp(Ix*bernoulliInt);
rho_e    = getMixture_rho([],p_e,TTrans_e,[],[],[],[],'specify_R',Rstar_e,EoS_params,options);  % final computation of rho_e

% initializing iterative process
convPRho = 1;                                                 % equal to zero for ideal gas (no need to iterate)
itPRho = 0;
itPRhoMax = 5000;           % HARDCODE ALERT!!
tolPRho = options.tol;
switch options.EoS
    case 'idealGas' % end of the story
    case {'vanDerWaals0','vanDerWaals'}
        pCrit_s = EoS_params.pCrit_s;
        TCrit_s = EoS_params.TCrit_s;
        if length(pCrit_s)>1
            error('The van der Waals equation of state is only available for mono-species mixtures');
        end
        [aEoS,bEoS] = getVanDerWaals_ab(pCrit_s,TCrit_s,Rstar_e);
        if strcmp(options.EoS,'vanDerWaals0')
            aEoS = 0;
        end
        rho_e0 = rho_e(1);
        while convPRho>tolPRho && itPRho<itPRhoMax                                                      % until convergence is reached
            itPRho = itPRho+1;                                                                              % increase counter
            rho_e_prev = rho_e;                                                                             % storing previous value
            A_rho = M_e.^2.*gam_e./2 - (1+M_e.^2.*gam_e./2) .* (Rstar_e.*TTrans_e./(1-bEoS.*rho_e).^2 - 2*aEoS.*rho_e) ./ (Rstar_e.*TTrans_e./(1-bEoS.*rho_e) - aEoS.*rho_e);
            dM2gam_dx = Dx*(M_e.^2.*gam_e./2);
            dRT_dx = Dx*(Rstar_e.*TTrans_e);
            B_rho = (1+M_e.^2.*gam_e/2) ./ (Rstar_e.*TTrans_e - aEoS.*rho_e + aEoS.*bEoS.*rho_e.^2);
            bernoulliInt = (dM2gam_dx + B_rho.*dRT_dx)./A_rho;
            rho_e = rho_e0.*exp(Ix*bernoulliInt);
            convPRho = max(abs(1-rho_e(:)./rho_e_prev(:)));                                                 % convergence criterion
        end
        p_e = getMixture_p([],rho_e,TTrans_e,[],[],[],[],'specify_R',Rstar_e,EoS_params,options);
    otherwise
        error(['The chosen EoS ''',options.EoS,''' is not supported']);
end

if itPRho==itPRhoMax
    warning(['Reached the maximum iteration count (',num2str(itPRhoMax),') with a convergence of ',num2str(convPRho),'>tol=',num2str(tolPRho)]);
elseif any(isnan(p_e))
    error('The computation of p_e from M_e NaN''ed');
end

end % getInviscid_p_rho_Bernouilli_fromMach