function Z = eval_func_Heavy_el(func,TTrans,Tel,bElectron)
%EVAL_FUNC_HEAVY_EL Calculate a property of a mixture of heavy particles
% and an electron given the property's function handle.
%
% Inputs:
%   func        -   property function_handle
%   TTrans      -   translational temperature. it should come in the
%                   appropriate size for the function being evaluated
%   Tel         -   electron temperature.  it should come in the
%                   appropriate size for the function being evaluated
%   bElectron   -   electron boolean. size (N_eta x N_spec). an entry is 0 if
%                   the particle is heavy; 1 if an electron.
%
% Outputs:
%   Z           -   value of function_handle, func for both heavy and
%                   particles
%
% Notes:
% N_eta is the number of points in the domain
% N_spec is the number of species in the mixture
%
% Example(s):
% 1. Calculate species viscosity using Blottner's curve fit
%    See getSpecies_mu_Blottner.m
%
%    fun_Blottner = @(T) getSpecies_mu_Blottner(T,Amu_s,Bmu_s,Cmu_s,N_spec)
%    mu_s = eval_func_Heavy_el(fun_Blottner,TTrans,Tel,bElectron)
%
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

% Calculate property Z for heavy particles with translational temperature
Z_Heavy = func(TTrans);

% Calculate property Z for electrons with their respective temperature
if sum(bElectron(:))
    Z_el = func(Tel);
else
    Z_el = zeros(size(Z_Heavy));
end

% Blend the properties together into one vector
Z = (bElectron).*Z_el + (~bElectron).*Z_Heavy;

end % eval_func_Heavy_el.m
