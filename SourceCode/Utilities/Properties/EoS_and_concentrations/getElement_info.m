function [elem_list,elemStoich_mat] = getElement_info(spec_list)
% getElement_info returns the list of elements in a series of species.
%
% Usage:
%   (1)
%       [elem_list,elemStoich_mat] = getElement_info(spec_list)
%
% Inputs and outputs:
%   spec_list
%       cell of strings with the various species conforming the mixture
%   elem_list
%       cell of strings with the various elements conforming the various
%       species in the mixture
%   elemStoich_mat
%       matrix of size N_elem x N_spec with the stoichiometric coefficients
%       of each element in each species.
%
% Examples:
%   (1)
%       spec_list = {'CO2','Ar','Air','H','CH3Fe2','H+','H++','e-'}
%       [elem_list,idx_fict,elemStoich_mat] = getElement_info(spec_list)
%           elem_list =
%               {'e-','Air','Ar','C','Fe','H','O'}
%           elemStoich_mat =
%                   [ 0     0     0    0      0    -2    -1    1   ] % e-
%                   [ 0     0     1    0      0     0     0    0   ] % Air
%                   [ 0     1     0    0      0     0     0    0   ] % Ar
%                   [ 1     0     0    0      1     0     0    0   ] % C
%                   [ 0     0     0    0      2     0     0    0   ] % Fe
%                   [ 0     0     0    1      3     1     1    0   ] % H
%                   [ 2     0     0    0      0     0     0    0   ] % O
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_spec = length(spec_list);                                             % number of species
elem_list = {};                                                         % allocating
for s=1:N_spec                                                          % looping species
    elem = regexp(spec_list{s},'([A-Z][a-z]*|\+|\-)','match');              % searching for species
    % regexp breakdown: either a capital letter [A-Z] followed by a
    % lowercase letter [a-z] 0 or more times, or | a plus sign \+, or | a
    % minus sign \-
    elem_list = [elem_list , elem];                                         %#ok<AGROW> % appending to element list
end
elem_list = unique(elem_list);                                          % removing repeated elements
idx_pos = ~cellfun(@isempty,regexp(elem_list,'\+','once'));             % positively-charged element
idx_neg = ~cellfun(@isempty,regexp(elem_list,'\-','once'));             % negatively-charged element
idx_char = idx_pos|idx_neg;                                             % positive or negatively-charged elements

N_elem = length(elem_list);
for ie=N_elem:-1:1                                                      % looping elements
    N_ie_inList(ie) = nnz(~cellfun(@isempty,regexp(elem_list,str2regexp(elem_list{ie})))); % checking that no element is part of another
end
if any(N_ie_inList>1)                                                   % if they are, iit means that the stoichiometric matrix could be wrongly determined
    error(['two of the elements in the element list (',strjoin(elem_list,','),') are contained within each other. The identification of the elemental stoichiometric matrix therefore cannot be computed. The internal logic of build_ElemStoichCoeffs must be changed before it can.']);
end

for ie=N_elem:-1:1                                                      % looping elements
    elemStoich_mat(ie,:) = build_ElemStoichCoeffs(elem_list{ie},spec_list); % populating stoichiometric matrix
end

if any(idx_char)                                                        % we have fictitious elements
    elem_list = [{'e-'},elem_list(~idx_char)];                              % swaping + and - for e- in the list
    elemStoich_mat = [ elemStoich_mat(idx_neg,:) - elemStoich_mat(idx_pos,:) ; elemStoich_mat(~idx_char,:)]; % unifying positive and negative row
end

end % getElement_info