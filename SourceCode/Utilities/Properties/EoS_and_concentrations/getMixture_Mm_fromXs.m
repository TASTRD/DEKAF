function Mm = getMixture_Mm_fromXs(X_s,Mm_s)
% getMixture_Mm_fromXs computes the mixture molar mass from the mole
% fractions.
% 
% Usage:
%   (1)
%       Mm = getMixture_Mm_fromXs(X_s,Mm_s)
%
% Inputs and outputs:
%   X_s             [-]         (N_eta x N_spec)
%       species mole fraction.
%   Mm_s            [kg/mol]    (N_spec x 1)
%       species molar mass.
%   Mm             [kg/mol]     (N_eta x 1)
%       mixture molar mass
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(X_s);

Mm_s2D  = repmat(Mm_s.',[N_eta,1]);     % (s,1) .--> (eta,s)
Mm      = (Mm_s2D.*X_s)*ones(N_spec,1); % (eta,1)

end % getEquilibriumDer_dXs_dyE