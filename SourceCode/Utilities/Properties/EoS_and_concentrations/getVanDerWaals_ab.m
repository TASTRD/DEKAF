function [aEoS,bEoS] = getVanDerWaals_ab(pCrit,TCrit,R)
% getVanDerWaals_ab returns the a and b constants needed to evaluate the
% van der Waals equation of state.
%
% Usage:
%   (1)
%       [aEoS,bEoS] = getVanDerWaals_ab(pCrit,TCrit,R)
%
% Inputs and outputs:
%   pCrit       [Pa]
%       critical pressure
%   TCrit       [K]
%       critical temperature
%   R           [J/kg-K]
%       gas-specific constant
%   aEoS        [m^6/kg^2-Pa]
%       a constant
%   bEoS        [m^3/kg]
%       b constant
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

aEoS = 0.421875*(R*TCrit).^2/pCrit;
bEoS = 0.125 *   R*TCrit   ./pCrit;

end % getVanDerWaals_ab