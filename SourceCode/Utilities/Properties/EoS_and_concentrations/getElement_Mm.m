function Mm_E = getElement_Mm(elem)
% getElement_Mm returns the molar mass (in kg/mol) of an element provided a
% string with its symbol.
%
% One can provide: either a single string, or a cell of strings.
%
% Usage:
%   (1)
%       Mm_E = getElement_Mm(elem)
%
% Available elements:
%   'Air'       - average air molecule
%   'Ar'        - argon
%   'He'        - helium
%   'Ne'        - neon
%   'C'         - carbon
%   'N'         - nitrogen
%   'O'         - oxygen
%   'e-'        - electrons
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

if iscell(elem)                             % if the user passed a cell
    Mm_E = cellfun(@getElement_Mm,elem(:));     % we call the function recursively within cellfun
else                                        % if the user passed a string
    switch elem                                 % depending on the string
        case 'Air';     Mm_E = 28.8558e-3;          % mass of average air molecule
        case 'Ar';      Mm_E = 39.94e-3;            % mass of Argon atoms
        case 'He';      Mm_E =  4.003e-3;           % mass of Helium atoms
        case 'Ne';      Mm_E = 20.18e-3;            % mass of Neon atoms
        case 'C';       Mm_E = 12.011e-3;           % mass of Carbon atoms
        case 'N';       Mm_E = 14.0067e-3;          % mass of Nitrogen atoms
        case 'O';       Mm_E = 15.9994e-3;          % mass of Oxygen atoms
        case 'e-';      Mm_E =  0.00055e-3;         % mass of electrons
        otherwise;      error(['The chosen element ''',elem,''' is not supported']);
    end
end

end % getElement_Mm