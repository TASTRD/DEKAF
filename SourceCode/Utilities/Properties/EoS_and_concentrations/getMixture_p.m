function p = getMixture_p(y_s,rho,TTrans,Tel,R0,Mm_s,bElectron,varargin)
% getMixture_p Calculate the mass density of a gas with electrons
%
% Usage:
%   (1)
%       p = getMixture_p(y_s,rho,TTrans,Tel,R0,Mm_s,bElectron);
%       |--> ideal-gas equation of state
%
%   (2)
%       p = getMixture_p([],rho,TTrans,[],[],[],[],'specify_R',R);
%       |--> allows to manually specify the gas constant, 
%
%   (3)
%       p = getMixture_p(...,EoS_params,options);
%       |--> other equations of state
%
% Inputs:
%   y_s         -   species mass fractions                 (N_eta x N_spec)
%   rho         -   mixture density                             (N_eta x 1)
%   TTrans      -   translational temperature                   (N_eta x 1)
%   Tel         -   electron temperature                        (N_eta x 1)
%   R0          -   universal gas constant                         (scalar)
%   Mm_s        -   species molecular mass                     (N_spec x 1)
%   bElectron   -   electron boolean                           (N_spec x 1)
%   EoS_params  -   structure containing any additional parameters needed
%                   for the evaluation of the equation of state. The
%                   specific fields it must contain are:
%               'idealGas'      - none
%               'vanDerWaals0'  - pCrit_s and TCrit_s (scalar)
%               'vanDerWaals'   - pCrit_s and TCrit_s (scalar)
%   options     -   classic DEKAF options structure
%
% Outputs:
%   p           -   static mixture pressure                     (N_eta x 1)
%
% Notes:
%   This function assumes the fluid is an ideal gas
%
% Related functions:
%   See also getMixture_Mm_R, getMixture_rho, eval_func_Heavy_el
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[Rstar,varargin] = parse_optional_input(varargin,'specify_R',NaN);  % checking if the user specified R

if isempty(varargin)                                                % checking for additional EoS-related inputs
    options.EoS = 'idealGas';
else
    EoS_params  = varargin{1};
    options     = varargin{2};
end

if isnan(Rstar)
[~,~,~,Rstar] = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);
end

switch options.EoS
    case 'idealGas'
        p = rho.*Rstar.*TTrans;
    case 'vanDerWaals0'
        if ~ismember(options.flow,{'CPG','TPG'}) || ~ismember(options.numberOfTemp,{'1T'})
            error('The van der Waals equation of state is only available for flow=''CPG'' or ''TPG'', and numberOfTemp=''1T''.');
        end
        pCrit_s = EoS_params.pCrit_s;
        TCrit_s = EoS_params.TCrit_s;
        if length(pCrit_s)>1
            error('The van der Waals equation of state is only available for mono-species mixtures');
        end
        [~,bEoS] = getVanDerWaals_ab(pCrit_s,TCrit_s,Rstar);
        p = Rstar.*TTrans./(1./rho - bEoS);
    case 'vanDerWaals'
        if ~ismember(options.flow,{'CPG','TPG'}) || ~ismember(options.numberOfTemp,{'1T'})
            error('The van der Waals equation of state is only available for flow=''CPG'' or ''TPG'', and numberOfTemp=''1T''.');
        end
        pCrit_s = EoS_params.pCrit_s;
        TCrit_s = EoS_params.TCrit_s;
        if length(pCrit_s)>1
            error('The van der Waals equation of state is only available for mono-species mixtures');
        end
        [aEoS,bEoS] = getVanDerWaals_ab(pCrit_s,TCrit_s,Rstar);
        p = Rstar.*TTrans./(1./rho - bEoS) - aEoS.*rho.^2;
    otherwise
        error(['The chosen EoS ''',options.EoS,''' is not supported']);
end

end % getMixture_p.m
