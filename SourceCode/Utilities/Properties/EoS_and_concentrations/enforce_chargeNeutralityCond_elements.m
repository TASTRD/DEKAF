function X_E_out = enforce_chargeNeutralityCond_elements(X_E_in,elem_list)
% enforce_chargeNeutralityCond_elements applies the charge neutrality
% condition to elemental mole fractions. 
%
% Usage:
%   (1)
%       X_E = enforce_chargeNeutralityCond_elements(X_E,elem_list)
%
% Inputs and outputs:
%   elem_list                       N_elem x 1
%       cell of strings with the names of the different elements
%   X_E                 [-]         N_eta x N_elem
%       species mole fractions
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_elem = length(elem_list);

X_E_in(:,strcmp(elem_list,'e-')) = 0;       % fixing elemental fraction of electrons to zero

Xtot    = X_E_in*ones(N_elem,1);            % summing elemental mole fractions
X_E_out = X_E_in./repmat(Xtot,[1,N_elem]);  % renormalizing wrt it

end % enforce_chargeNeutralityCond_elements