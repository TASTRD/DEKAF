function [spec_list,idx_ghostSpec] = get_additionalGhostSpecies(spec_list)
% get_additionalGhostSpecies obtains a series of additional ghost species
% to account for whichever fundamental species (needed for the formation
% reactions) is not in the species list.
%
% Usage:
%   (1)
%       spec_list = get_additionalGhostSpecies(spec_list)
%
%   (2)
%       [spec_list,idx_ghostSpec] = get_additionalGhostSpecies(spec_list)
%       |--> returns also the index of where these new ghost species are in
%       spec_list
%
% Examples:
%   (1)
%       spec_list = {'N','NO','O2'};   % N and NO are missing their fundamental species N2
%       get_additionalGhostSpecies(spec_list)
%
%       ans = {'N','NO','O2','N2'}
%
%   (2)
%       spec_list = {'CO2','O','O2'};   % CO2 is missing its fundamental species C
%       [a,b] = get_additionalGhostSpecies(spec_list)
%
%       a = {'CO2','O','O2','C'}
%       b = 4;
%
% Author: Fernando Miro Miro
% Date: November 2018
% GNU Lesser General Public License 3.0

% For now we only have three fundamental species - N2, O2 and C
idx_N = ~cellfun(@isempty,regexp(spec_list,'(.*)N+(.*)'));  % those species containing N somewhere
idx_O = ~cellfun(@isempty,regexp(spec_list,'(.*)O+(.*)'));  % those species containing O somewhere
idx_C = ~cellfun(@isempty,regexp(spec_list,'(.*)C+(.*)'));  % those species containing C somewhere
idx_Air = ~cellfun(@isempty,regexp(spec_list,'Air'));       % Air species

idx_ghostSpec = []; % initializing
if (nnz(idx_N)>0 || nnz(idx_Air)) && ~ismember('N2',spec_list) % we must add N2
    idx_ghostSpec = [idx_ghostSpec,length(spec_list)+1];
    spec_list = [spec_list,{'N2'}];
end
if (nnz(idx_O)>0 || nnz(idx_Air)) && ~ismember('O2',spec_list) % we must add O2
    idx_ghostSpec = [idx_ghostSpec,length(spec_list)+1];
    spec_list = [spec_list,{'O2'}];
end
if nnz(idx_C)>0 && ~ismember('C',spec_list) % we must add C
    idx_ghostSpec = [idx_ghostSpec,length(spec_list)+1];
    spec_list = [spec_list,{'C'}];
end
