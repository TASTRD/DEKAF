function X_s = getSpecies_X(y_s,Mm,Mm_s)
%GETSPECIES_X Calculate species mole fraction
%
% Usage:
%   (1) 
%       X_s = getSpecies_X(y_s,Mm,Mm_s)
%
%   (2) 
%       X_s = getSpecies_X(y_s,[],Mm_s)
%       |--> uses getMixture_Mm_R to compute Mm (slower)
%
% Inputs:
%   y_s         -   species mass fractions [-].
%                   Matrix of size (N_eta x N_spec)
%   Mm          -   mixture molecular molar mass [kg/mol]
%                   Vector of size (N_eta x 1)
%   Mm_s        -   species molecular molar mass [kg/mol]
%                   Matrix of size (N_spec x 1)
%
% Outputs:
%   X_s         -   species mole fraction [-]
%                   Matrix of size (N_eta x N_spec)
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0
if isempty(Mm)
    Mm = getMixture_Mm_R(y_s,[],[],Mm_s,[],[]);
end

% Reshape to size (N_eta x N_spec)
[N_eta,N_spec] = size(y_s);
Mm_s = repmat(Mm_s',[N_eta,1]);
Mm = repmat(Mm,[1,N_spec]);

X_s = Mm.*y_s./Mm_s;

end % getSpecies_X.m
