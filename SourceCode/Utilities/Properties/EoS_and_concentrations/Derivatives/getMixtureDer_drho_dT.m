function drho_dT = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,TGroup,varargin)
% getMixtureDer_drho_dT returns the derivative with each temperature of the
% mixture density for constant pressure.
%
% Usage:
%   (1)
%       drho_dT = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,TTrans)
%
%   (2)
%       drho_dT = getMixtureDer_drho_dT(p,y_s,R0,Mm_s,bElectron,[TTrans,Tel],'2T')
%       |--> computes the derivatives with TTrans and Tel (drho_dT will
%       have size N_eta x 2)
%
% Inputs and outputs:
%  - p          [N_eta x 1] (to be repeated over dim. 2)
%  - y_s        [N_eta x N_spec]
%  - R0         [1]
%  - Mm_s       [N_spec x 1] (to be reshaped to dim. 2 and repeated over dim. 1)
%  - bElectron  [N_spec x 1] (to be reshaped to dim. 2 and repeated over dim. 1)
%  - TTrans     [N_eta x 1] (to be repeated over dim. 2)
%  - Tel        [N_eta x 1] (to be repeated over dim. 2)
%  - drho_dT    [N_eta x N_T]
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

if isempty(varargin) % we assume the temperature for electrons to be the same as for heavies
    numberOfTemp = '1T';
else
    numberOfTemp = varargin{1};
end

switch numberOfTemp
    case '1T'
        TTrans = TGroup(:,1);
        Tel = TGroup(:,1);
    case '2T'
        TTrans = TGroup(:,1);
        Tel = TGroup(:,2);
    otherwise
        error(['the selected number of temperatures ''',numberOfTemp,''' is not supported']);
end

% sizes and booleans
[N_eta,N_spec] = size(y_s);
bHeavy = ~bElectron;
N_heavy = nnz(bHeavy);
Mm_s = repmat(Mm_s',[N_eta,1]);             % defined to keep clear the subindex notation

% Defining species temperature
T_s = repmat(TTrans,[1,N_spec]);
Tel_mat = repmat(Tel,[1,N_spec]);
T_s(:,bElectron) = Tel_mat(:,bElectron);

% computing derivatives
SumTysOverMm_s = (y_s./Mm_s.*T_s) * ones(N_spec,1);

drho_dTTrans = - p./(R0*SumTysOverMm_s.^2) .* ((y_s(:,bHeavy)./Mm_s(:,bHeavy))*ones(N_heavy,1));
drho_dTel = y_s(:,bElectron)./Mm_s(:,bElectron);
if isempty(drho_dTel)
    drho_dTel = zeros(N_eta,1);
end                                     % we use these strange recursive definitions to make sure that there are no size conflicts
drho_dTel = -drho_dTel.*p./(R0*SumTysOverMm_s.^2);

% Assembling outputs depending on the number of temperatures
switch numberOfTemp
    case '1T'
    drho_dT = drho_dTTrans + drho_dTel;
    case '2T'
    drho_dT = [drho_dTTrans,drho_dTel];
    otherwise
        error(['the selected thermal model ''',numberOfTemp,''' is not supported']);
end

end