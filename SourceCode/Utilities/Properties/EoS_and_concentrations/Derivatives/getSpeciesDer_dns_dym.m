function dns_dym = getSpeciesDer_dns_dym(y_s,drho_dym,rho,nAvogadro,Mm_s)
% getSpeciesDer_dns_dym computes the derivatives of the species number
% densities wrt all species mass fractions
%
% Usage:
%   (1)
%       dns_dym = getSpeciesDer_dns_dym(y_s,drho_dym,rho,nAvogadro,Mm_s)
%
% Inputs and outputs:
%   y_s             N_eta x N_spec
%   drho_dym        N_eta x N_spec
%   rho             N_eta x 1
%   nAvogadro       1 x 1
%   Mm_s            N_spec x 1
%   dns_dym         N_eta x N_spec(m) x N_spec(s)
%
% See also: getSpeciesDer_dns_dT, listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);

y_s         = repmat(permute(y_s,        [1,3,2]),[1,N_spec,1]);        % (eta,s) --> (eta,m,s)
drho_dym    = repmat(drho_dym,                    [1,1,N_spec]);        % (eta,m) --> (eta,m,s)
rho         = repmat(rho,                         [1,N_spec,N_spec]);   % (eta,1) --> (eta,m,s)
delta_ms    = repmat(permute(eye(N_spec),[3,1,2]),[N_eta,1,1]);         % (m,s) ----> (eta,m,s)
Mm_s        = repmat(permute(Mm_s,       [2,3,1]),[N_eta,N_spec,1]);    % (s,1) ----> (eta,m,s)

dns_dym = nAvogadro./Mm_s.*(delta_ms.*rho + y_s.*drho_dym);     % eta,m,s)