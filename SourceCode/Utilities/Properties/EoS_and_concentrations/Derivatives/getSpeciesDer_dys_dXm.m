function dys_dXm = getSpeciesDer_dys_dXm(y_s,Mm_s)
% getSpeciesDer_dXs_dym computes the derivatives of all mass fractions wrt.
% all mole fractions.
%
% It can also work with elemental quantities.
%
% Usage:
%   (1)
%       dys_dXm = getSpeciesDer_dys_dXm(y_s,Mm_s)
%
% Inputs & outputs:
%   y_s         N_eta x N_spec
%   Mm_s        N_spec x 1
%   dys_dXm     N_eta x N_spec x N_spec (eta,m,s)
%
% See also: listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);

% Computing auxiliary quantities (mixture molar mass and molar fractions) 
Mm  = getMixture_Mm_R(y_s,[],[],Mm_s,[],[]);                                % (eta,1)
X_s = getSpecies_X(y_s,Mm,Mm_s);                                            % (eta,s)

% Repmatting
delta_ms = repmat(permute(eye(N_spec),[3,1,2]),[N_eta,1,1]);                % (m,s) ----> (eta,m,s)
Mm_3D    = repmat(        Mm,                  [1,N_spec,N_spec]);          % (eta,1) --> (eta,m,s)
Mm_m3D   = repmat(        Mm_s.',              [N_eta,1,N_spec]);           % (m,1) ----> (eta,m,s)
Mm_s3D   = repmat(permute(Mm_s,       [2,3,1]),[N_eta,N_spec,1]);           % (s,1) ----> (eta,m,s)
X_s3D    = repmat(permute(X_s,        [1,3,2]),[1,N_spec,1]);               % (eta,s) --> (eta,m,s)

dys_dXm = Mm_s3D.*(delta_ms./Mm_3D + X_s3D./Mm_m3D);                        % (eta,m,s)

end % getSpeciesDer_dys_dXm