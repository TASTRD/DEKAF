function drhol_dym = getSpeciesDer_drhol_dym(rho,drho_dym,y_l)
% getSpeciesDer_drhol_dym returns the derivative of the species density
% with all of the species mass fractions for constant pressure.
%
% Usage:
%   (1)
%       drhol_dym = getSpeciesDer_drhol_dym(rho,drho_dym,y_l)
%
% Inputs and outputs:
%  - rho        [N_eta x 1] (to be repeated over dim. 2 & 3)
%  - drho_dym   [N_eta x N_spec] (to be repeated over dim. 3)
%  - y_l        [N_eta x N_spec] (to be reshaped and repeated over dim. 2)
%  - drhol_dym  [N_eta x N_spec(m) x N_spec(l)]
%
% The output will have size N_eta x N_spec x N_spec, corresponding to the
% subindices eta, m, l. For example, the derivative of rho_2 with respect
% to the mass fraction of species 4 will be in drhol_dym(:,4,1)
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_l);
delta_ml = eye(N_spec); % dirac delta matrix

% reshaping
delta_ml    = repmat(permute(delta_ml,[3,1,2]),[N_eta,1,1]);    % (m,l) --> (eta,m,l)
rho         = repmat(rho,[1,N_spec,N_spec]);                    % (eta,1) --> (eta,m,l)
y_l         = repmat(permute(y_l,[1,3,2]),[1,N_spec,1]);        % (eta,l) --> (eta,m,l)
drho_dym    = repmat(drho_dym,[1,1,N_spec]);                    % (eta,m) --> (eta,m,l)

drhol_dym   = rho.*delta_ml + y_l.*drho_dym;                    % (eta,m,l)

end