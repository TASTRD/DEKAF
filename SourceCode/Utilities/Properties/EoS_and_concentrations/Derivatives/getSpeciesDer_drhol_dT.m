function drhol_dT = getSpeciesDer_drhol_dT(drho_dT,y_l)
% GETSPECIESDER_DRHOl_DT(drho_dT,y_l) returns the derivative with
% temperature of all species densities for constant pressure. Inputs:
%
%  - drho_dT [N_eta x N_T] (to be repeated over dim. 2)
%  - y_l [N_eta x N_spec]
%  - drhol_dT [N_eta x N_spec x N_T]
%
% GNU Lesser General Public License 3.0

N_spec = size(y_l,2);
N_T = size(drho_dT,2);
drho_dT = repmat(permute(drho_dT,[1,3,2]),[1,N_spec,1]);    % (eta,iT) --> (eta,l,iT)
y_l = repmat(y_l,[1,1,N_T]);                                % (eta,l) --> (eta,l,iT)

drhol_dT = y_l.*drho_dT;

end