function dp_dT = getMixtureDer_dp_dT(rho,y_s,TTrans,Tel,Mm_s,R0,bElectron,drho_dT)
% GETMIXTUREDER_DP_DT Derivative of mixture pressure wrt temperature
%
% Inputs:
%   rho         -   mixture mass density
%                   [N_eta x 1]
%   y_s         -   species mass fractions, matrix of size (N_eta x N_spec)
%   TTrans      -   Temperature for the translational mode (N_eta x 1)
%   Tel         -   Temperature for the electrons (N_eta x 1)
%   Mm_s        -   species mixture molar mass, vector of size
%                   (N_spec x 1)
%   R0          -   universal gas constant
%   bElectron   -   electron boolean (N_spec x 1)
%   drho_dT     -   mixture mass density derivative wrt temperature
%                   [N_eta x 1]
%
% Outputs:
%   dp_dT       -   mixture pressure derivative wrt temperature
%                   [N_eta x 1]
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

[~,~,~,Rstar] = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron);

dp_dT       = rho.*Rstar + drho_dT.*Rstar.*TTrans;

end % getMixtureDer_dp_dT.m
