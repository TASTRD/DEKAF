function dps_dym = getSpeciesDer_dps_dym(rho,R_s,TTrans,y_s,drho_dym,varargin)
%GETSPECIESDER_DPS_DYM Derivative of species partial pressure wrt
% species mass fractions
%
% Usage:
%   (1)
%       dps_dym = getSpeciesDer_dps_dym(rho,R_s,TTrans,y_s,drho_dym)
%
%   (2)
%       dps_dym = getSpeciesDer_dps_dym(rho,R_s,TTrans,y_s,drho_dym,'2T',bElectron,Tel)
%       |--> for two-temperature models
%
% Inputs:
%   rho         -   mixture mass density
%                   [N_eta x 1]
%   R_s         -   species gas constant
%                   [N_spec x 1]
%   bElectron   -   electron boolean
%                   [N_spec x 1]
%   TTrans      -   translational temperature
%                   [N_eta x 1]
%   Tel         -   electron temperature
%                   [N_eta x 1]
%   y_s         -   species mass fractions
%                   [N_eta x N_spec]
%   drho_dym    -   mixture mass density derivative wrt species mass
%                   fractions
%                   [N_eta x N_spec]
%
% Outputs:
%   dps_dym     -   species partial pressure derivative wrt species mass
%                   fractions
%                   [N_eta x N_spec x N_spec]
%
% Notes:
%   m is the first species index, i.e., the second dimension of dps_dym
%   s is the second species index, i.e., the third dimension of dps_dym
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

if isempty(varargin)
    numberOfTemp    = '1T';
else
    numberOfTemp    = varargin{1};
    bElectron       = varargin{2};
    Tel             = varargin{3};
end

[N_eta,N_spec] = size(drho_dym);
rho         = repmat(rho,[1,N_spec,N_spec]);                                % (eta,1) --> (eta,m,s)
R_s         = repmat(reshape(R_s,[1,1,N_spec]),[N_eta,N_spec,1]);           % (s,1) --> (eta,m,s)
y_s         = repmat(reshape(y_s,[N_eta,1,N_spec]),[1,N_spec,1]);           % (eta,s) --> (eta,m,s)
drho_dym    = repmat(drho_dym,[1,1,N_spec]);                                % (eta,m) --> (eta,m,s)
dirac_ms    = repmat(reshape(eye(N_spec),[1,N_spec,N_spec]),[N_eta,1,1]);   % (m,s) --> (eta,m,s)

T_s         = repmat(TTrans,[1,N_spec]);                                    % (eta,1) --> (eta,s)
switch numberOfTemp
    case '1T'
        % nothing to modify
    case '2T'
        T_s(:,bElectron) = Tel;
    otherwise
        error(['the chosen number of temperatures ''',numberOfTemp,''' is not supported']);
end
T_s     = repmat(permute(T_s,[1,3,2]),[1,N_spec,1]);                        % (eta,s) --> (eta,m,s)
dps_dym = dirac_ms.*rho.*R_s.*T_s + y_s.*R_s.*T_s.*drho_dym;

end % getSpeciesDer_dps_dym.m
