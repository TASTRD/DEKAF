function dps_dT = getSpeciesDer_dps_dT(rho,R_s,TGroup,y_s,drho_dT,varargin)
%GETSPECIESDER_DPS_DT Derivative of species partial pressure wrt
% temperature
%
% Usage:
%   (1)
%       dps_dT = getSpeciesDer_dps_dT(rho,R_s,TGroup,y_s,drho_dT)
%
%   (2)
%       dps_dT = getSpeciesDer_dps_dT(rho,R_s,TGroup,y_s,drho_dT,'2T',bElectron)
%       |--> how the function should be called for a two-T model
%
% Inputs:
%   rho         -   mixture mass density
%                   [N_eta x 1]
%   R_s         -   species gas constant
%                   [N_spec x 1]
%   TGroup      -   temperatures
%                   [N_eta x N_T]
%   y_s         -   species mass fractions
%                   [N_eta x N_spec]
%   drho_dT     -   mixture mass density derivative wrt temperature
%                   [N_eta x N_T]
%   bElectron   -   electron boolean
%                   [N_spec x 1]
%
% Outputs:
%   dps_dT      -   species partial pressure derivative wrt temperature
%                   [N_eta x N_spec x N_T]
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

if isempty(varargin)
    numberOfTemp = '1T';
else
    numberOfTemp = varargin{1};
    bElectron = varargin{2};
end

[N_eta,N_spec] = size(y_s);
N_T = size(drho_dT,2);
rho         = repmat(rho                        ,[1,N_spec,N_T]);   % (eta,1) --> (eta,s,T)
R_s         = repmat(R_s'                       ,[N_eta,1,N_T]);    % (s,1)   --> (eta,s,T)
y_s         = repmat(y_s                        ,[1,1,N_T]);        % (eta,s) --> (eta,s,T)
drho_dT     = repmat(permute(drho_dT,[1,3,2])   ,[1,N_spec,1]);     % (eta,T) --> (eta,s,T)

switch numberOfTemp
    case '1T'
        T_s = repmat(TGroup(:,1),[1,N_spec,N_T]);   % (eta,1) --> (eta,s,T)
    case '2T'
        T_s = repmat(permute(TGroup,[1,3,2]),[1,N_spec,1]); % (eta,T) --> (eta,s,T)
        T_s(:,bElectron,1) = 0;             % clearing trans-rot temperature for electrons
        T_s(:,~bElectron,2) = 0;            % clearing vib-elec-el temperature for heavy particles
    otherwise
        error(['the chosen number of temperatures ''',numberOfTemp,''' is not supported']);
end
dps_dT = y_s.*rho.*R_s + y_s.*R_s.*T_s.*drho_dT;

end % getSpeciesDer_dps_dT.m
