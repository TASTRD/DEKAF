function drho_dym = getMixtureDer_drho_dym(p,y_s,R0,Mm_s,bElectron,TTrans,varargin)
% getMixtureDer_drho_dym returns the derivative with each mass fraction y_m
% of the mixture density for constant pressure.
%
% Usage:
%   (1)
%       drho_dym = getMixtureDer_drho_dym(p,y_s,R0,Mm_s,bElectron,TTrans)
%
%   (2)
%       drho_dym = getMixtureDer_drho_dym(p,y_s,R0,Mm_s,bElectron,TTrans,Tel)
%
% Inputs and outputs:
%  - p          [N_eta x 1] (to be repeated over dim. 2)
%  - y_s        [N_eta x N_spec]
%  - R0         [1]
%  - Mm_s       [N_spec x 1] (to be reshaped to dim. 2 and repeated over dim. 1)
%  - bElectron  [N_spec x 1] (to be reshaped to dim. 2 and repeated over dim. 1)
%  - TTrans     [N_eta x 1] (to be repeated over dim. 2)
%  - Tel        [N_eta x 1] (to be repeated over dim. 2)
%  - drho_dT    [N_eta x N_T]
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0

if isempty(varargin)
    Tel = TTrans;
else
    Tel = varargin{1};
end

% sizes and booleans
[N_eta,N_spec] = size(y_s);
Mm_s = repmat(Mm_s',[N_eta,1]);             % defined to keep clear the subindex notation

% Defining species temperature
T_s = repmat(TTrans,[1,N_spec]);
Tel_mat = repmat(Tel,[1,N_spec]);
T_s(:,bElectron) = Tel_mat(:,bElectron);

% computing derivatives
SumTysOverMm_s = (y_s./Mm_s.*T_s) * ones(N_spec,1);

drho_dym = -p/R0 ./ SumTysOverMm_s.^2 .* T_s./Mm_s;

end