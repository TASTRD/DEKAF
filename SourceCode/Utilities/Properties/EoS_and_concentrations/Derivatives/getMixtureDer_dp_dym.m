function dp_dym = getMixtureDer_dp_dym(rho,y_s,TTrans,Tel,Mm_s,R0,bElectron,drho_dym)
% GETMIXTUREDER_DP_DYM Derivative of mixture pressure wrt mass fractions
%
% Inputs:
%   rho         -   mixture mass density
%                   [N_eta x 1]
%   y_s         -   species mass fractions, matrix of size (N_eta x N_spec)
%   TTrans      -   Temperature for the translational mode (N_eta x 1)
%   Tel         -   Temperature for the electrons (N_eta x 1)
%   Mm_s        -   species mixture molar mass, vector of size
%                   (N_spec x 1)
%   R0          -   universal gas constant
%   bElectron   -   electron boolean (N_spec x 1)
%   drho_dym    -   mixture mass density derivative wrt mass fraction
%                   [N_eta x N_m]
%
% Outputs:
%   dp_dym     -   mixture pressure derivative wrt mth mass fraction
%                   [N_eta x N_m]
%
% Notes:
%   m is the first species index, i.e., the second dimension of dp_dym
%   s is the second species index, i.e., the third dimension of dp_dym
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

[N_eta,N_spec]  = size(y_s);
y_s         = repmat(reshape(y_s,[N_eta,1,N_spec]),[1,N_spec,1]);   % size (N_eta x N_m x N_s)
rho         = repmat(rho,[1,N_spec,N_spec]);                        % size (N_eta x N_m x N_s)
Mm_s        = repmat(reshape(Mm_s,[1,1,N_spec]),[N_eta,N_spec,1]);  % size (N_eta x N_m x N_s)
TTrans      = repmat(TTrans,[1,N_spec,N_spec]);                     % size (N_eta x N_m x N_s)
Tel         = repmat(Tel,[1,N_spec,N_spec]);                        % size (N_eta x N_m x N_s)
bElectron   = repmat(reshape(bElectron,[1,1,N_spec]),[N_eta,N_spec,1]);
drho_dym    = repmat(drho_dym,[1,1,N_spec]);                        % size (N_eta x N_m x N_s)
dirac_ms    = repmat(reshape(eye(N_spec),[1,N_spec,N_spec]),[N_eta,1,1]);% (N_eta x N_m x N_s)

% Define for 3D matrix inputs
dp_dym_func = @(T) (R0./Mm_s).*T.*(dirac_ms.*rho + drho_dym.*y_s);
summand_dp_dym_s = eval_func_Heavy_el(dp_dym_func,TTrans,Tel,bElectron);% (N_eta x N_m x N_s)

dp_dym = zeros(N_eta,N_spec); % N_eta x N_m
for s=1:N_spec
    dp_dym = dp_dym + summand_dp_dym_s(:,:,s);
end

end % getMixtureDer_dp_dym.m
