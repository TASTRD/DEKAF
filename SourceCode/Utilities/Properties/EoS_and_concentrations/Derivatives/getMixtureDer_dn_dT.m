function dn_dT = getMixtureDer_dn_dT(y_s,rho,dys_dT,drho_dT,Mm_s,nAvogadro)
%GETMIXTUREDER_DN_DT Calculate the temperature-derivative of mixture number
% density [1/m^3-K]
%
% Usage
%   dn_dT = getMixtureDer_dn_dT(y_s,rho,dys_dT,drho_dT,Mm_s,nAvogadro);
%
% Inputs and Outputs
%   y_s         N_eta x N_spec
%   rho         N_eta x 1
%   dys_dT      N_eta x N_spec (only nonzero for LTE)
%   drho_dT     N_eta x 1
%   Mm_s        N_spec x 1
%   nAvogadro   1 x 1
%
%   dn_dT       N_eta x 1
%
% Children and related functions
% See also getMixtureDer_drho_dT.m,
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);
rho2D = repmat(rho,[1,N_spec]);
Mm_s2D = repmat(Mm_s',[N_eta,1]);
drho_dT2D = repmat(drho_dT,[1,N_spec]);

dn_dT = (nAvogadro./Mm_s2D .* (y_s.*drho_dT2D + dys_dT.*rho2D)) * ones(N_spec,1);

end % getMixtureDer_dn_dT.m
