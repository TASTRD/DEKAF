function [dMm_dT,dR_dT] = getMixtureDer_dMm_dT(y_s,dys_dT,Mm_s,R0)
%GETMIXTUREDER_DMM_DT Calculate the derivative of mixture molar mass and
% the specific gas constant with respect to temperature. This quantity is
% only nonzero in LTE.
%
% Mixture molar mass (when flow is in equilibrium) is defined as:
%
%   Mm = [sum y_s(s)/Mm_s(s)]^-1
%
% Usage:
%
% Inputs:
%   y_s             N_eta x N_spec
%   dys_dT          N_eta x N_spec
%   Mm_s            N_spec x 1
%   R0              1 x 1
%   bElectron       N_spec x 1
%
% Outputs:
%   dMm_dT          N_eta x 1
%   dR_dT           N_eta x 1
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);

Mm_s2D  = repmat(Mm_s',[N_eta,1]);

summand     = (y_s./Mm_s2D)*ones(N_spec,1);     % summing vectorially
summandDer  = (dys_dT./Mm_s2D)*ones(N_spec,1);

Mm      = (summand).^(-1);

dMm_dT  = -1*(summand).^(-2) .* summandDer;
dR_dT   = -R0 ./ Mm.^2 .* dMm_dT;

end % getMixtureDer_dMm_dT.m

