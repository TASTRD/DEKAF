function [dp_drho,dp_dys,dp_dT,dp_dTv] = getMixtureDer_dp_dall(rho,TTrans,Tel,y_s,R0,Mm_s,bElectron)
% getMixtureDer_dp_dall computes all the derivatives of the static pressure
% needed for the evaluation of the 1D Euler equation in euler1D_ODE.
%
% Usage:
%   (1)
%       [dp_drho,dp_dys,dp_dT,dp_dTv] = getMixtureDer_dp_dall(rho,TTrans,Tel,y_s,R0,Mm_s,bElectron)
%
% Inputs and outputs:
%   rho
%       mixture density [kg/m^3]
%   TTrans
%       translational temperature [K]
%   Tel
%       electron temperature [K]
%   y_s
%       species mass fraction [-]
%   R0
%       universal gas constant [J/kg-K]
%   Mm_s
%       species molar mass [kg/mol]
%   bElectron
%       species electron boolean [-]
%   dp_drho, dp_dys, dp_dT, dp_dTv
%       partial derivatives of pressure
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% sizes
N_spec = length(Mm_s);
N_heav = nnz(~bElectron);
N_eta = length(TTrans);

% resizing
Mm_s        = repmat(Mm_s.',        [N_eta,1]);
bElectron   = repmat(bElectron.',   [N_eta,1]);
T_s         = repmat(TTrans,        [1,N_spec]);
Tel2D       = repmat(Tel,           [1,N_spec]);
rho2D       = repmat(rho,           [1,N_spec]);
T_s(bElectron) = Tel2D(bElectron);

% computing derivatives
dp_dT   = R0*rho.*(y_s(~bElectron)./Mm_s(~bElectron))*ones(N_heav,1);
dp_drho = R0*(y_s./Mm_s.*T_s)*ones(N_spec,1);
dp_dys  = R0*rho2D./Mm_s.*T_s;

if nnz(bElectron)>1;    dp_dTv = R0*rho.*y_s(bElectron)./Mm_s(bElectron);
else;                   dp_dTv = zeros(N_eta,1);
end

end % getMixtureDer_dp_dall