function dns_dT = getSpeciesDer_dns_dT(y_s,drho_dT,nAvogadro,Mm_s)
% getSpeciesDer_dns_dT computes the derivative of the species number
% density with temperature
%
% Usage:
%   (1)
%       dns_dT = getSpeciesDer_dns_dT(y_s,drho_dT,nAvogadro,Mm_s)
%
% Inputs and outputs:
%   y_s             N_eta x N_spec
%   drho_dT         N_eta x N_T
%   nAvogadro       1 x 1
%   Mm_s            N_spec x 1
%   dns_dT          N_eta x N_spec x N_T
%
% See also: getSpeciesDer_dns_dym, listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);
N_T = size(drho_dT,2);

y_s = repmat(y_s,[1,1,N_T]);                                % (eta,s) --> (eta,s,iT)
drho_dT = repmat(permute(drho_dT,[1,3,2]),[1,N_spec,1]);    % (eta,iT) --> (eta,s,iT)
Mm_s = repmat(Mm_s.',[N_eta,1,N_T]);                        % (s,1) --> (eta,s,iT)

dns_dT = nAvogadro./Mm_s .* y_s .* drho_dT;                 % (eta,s,iT)