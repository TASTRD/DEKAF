function ys_out = enforce_concCond(ys_in,idx_bath,varargin)
% enforce_concentrationCondition enforces the concentration condition on
% the mass fractions:
%
%     sum(y_s) = 1
%
% It does so, by making the mass fraction of a certain bath species equal
% to one minus the sum of all other mass fractions.
%
% Examples:
%   (1) y_s = enforce_concCond(y_s,idx_bath)
%
%   (2) y_s = enforce_concCond(y_s,idx_bath,'ys_lim',ys_lim)
%   |-->  applies a minimum to the mass fractions
%
%   (3) X_s = enforce_concCond(X_s,idx_bath,'ys_lim',Xs_lim)
%   |-->  same story for the mole fractions
%
%   (4) y_s = enforce_concCond(y_s,'varyingBath',...)
%   |-->  varies the bath species to be the one with the highest mole/mass
%   fraction
%
% Inputs & outputs:
%   y_s         cell or matrix of mass fractions arranged in columns [-]
%   idx_bath    index identifying the mass fraction corresponding to the
%               bath species.
%   ys_lim      computational lower limit to the mass fractions
%
% See also: setDefaults, enforce_chargeNeutralityCond
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% Checking inputs
if ~isempty(varargin) && ischar(varargin{1}) && strcmp(varargin{1},'ys_lim')
    lim_flag = true;
    ys_lim = varargin{2};
else
    lim_flag = false;
end

% transforming if cell input
if iscell(ys_in)
    cell_flag = true;
    y_s = cell1D2matrix2D(ys_in);
else
    cell_flag = false;
    y_s = ys_in;
end

% applying ys limit
if lim_flag
    y_s(y_s<ys_lim) = ys_lim;
    y_s(y_s>1) = 1;
end

% applying concentration condition
N_spec = size(y_s,2);
if ~ischar(idx_bath) && length(idx_bath(:)) == 1 % same idx_bath for all points
    y_s(:,idx_bath) = 1-y_s(:,(1:N_spec)~=idx_bath)*ones(N_spec-1,1);
elseif ischar(idx_bath) && strcmp(idx_bath,'varyingBath') % idx_bath has to be computed and may vary
    bIdxBath = ismax(y_s,2);                % looking for the position of the species with the largest mole fraction (the bath species)
    ys_withoutBath = y_s;                   % we want a matrix with the same size as X_s
    ys_withoutBath(bIdxBath) = 0;           % only that we will make the the bath species zero
    ys_bath = 1 - sum(ys_withoutBath,2);    % that way we can simply sum them and substract them from 1 to get the bath
    ys_bath = repmat(ys_bath,[1,N_spec]);   % we repeat over N_spec so that the logical indexing in X_s is the same as that in Xs_bath
    y_s(bIdxBath) = ys_bath(bIdxBath);      % we then replug it into X_s in the appropriate positions
    % NOTE: the way this works is the following; posBath is a 2D logical matrix
    % of the same size as X_s, containing true in the position of the bath
    % species for every p and T, and false everywhere else. Using logical
    % indexing, we redefine X_s of these bath species, as one minus the sum of
    % all other species (those with false in the posBath matrix).
else
    error('not supported entry for idx_bath');
end

% transforming back to the original input
if cell_flag
    ys_out = matrix2D2cell1D(y_s);
else
    ys_out = y_s;
end