function [p_e,rho_e,U_e] = getInviscid_p_rho_U_fromMachRe(M_e,Re1_e,mu_e,gam_e,Rstar_e,TTrans_e,EoS_params,options)
% getInviscid_p_rho_fromMachRe1 computes the freestream pressure, density,
% and velocity from the Mach number and unit Reynolds number.
%
% Usage:
%   (1)
%       [p_e,rho_e,U_e] = getInviscid_p_rho_U_fromMachRe(M_e,Re1_e,mu_e,gam_e,Rstar_e,TTrans_e,EoS_params,options)
%
% Inputs:
%   M_e         [-]                 (Nx x 1)
%       Mach number at the boundary-layer edge 
%   Re1_e       [1/m]               (Nx x 1)
%       unit Reynolds number at the boundary-layer edge
%   mu_e        [kg/m-s]            (Nx x 1)
%       dynamic viscosity
%   gam_e       [-]                 (Nx x 1)
%       ratio of specific heats at the BL edge 
%   TTrans_e    [K]                 (Nx x 1)
%       static translational temperature at the boundary-layer edge 
%   Rstar_e     [J/kg-K]            (Nx x 1)
%       gas constant at the boundary-layer edge
%   EoS_params
%       structure containing any additional parameters necessary for the
%       evaluation of the equation of state
%   options
%       classic DEKAF options structure
%
% Outputs:
%   p_e         [Pa]                (Nx x 1)
%       static pressure
%   rho_e       [kg/m^3]            (Nx x 1)
%       mixture density
%   U_e         [m/s]               (Nx x 1)
%       streamwise velocity
%
% IMPORTANT! This function is compatible with non-ideal EoS
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

a_e = sqrt(gam_e.*Rstar_e.*TTrans_e);               % ideal gas speed of sound
conv = 1;           it=0;                           % initializing
tol=options.tol;    itMax=50;                       % HARDCODE ALERT!!
while conv>tol || it<itMax                          % until convergence
    it=it+1;                                            % increasing counter
    a_e_prev = a_e;                                     % speed of sound at the previous run
    U_e = M_e .*a_e;                                    % compute velocity from Mach and speed of sound
    rho_e = mu_e .* Re1_e / U_e;                        % compute density from Re and U_e
    p_e = getMixture_p([],rho_e,TTrans_e,[],[],[],[],'specify_R',Rstar_e,EoS_params,options); % compute pressure from EoS
    a_e = sqrt(gam_e.*p_e./rho_e);                      % real gas speed of sound
    conv = abs(1-a_e/a_e_prev);                         % compute convergence
end

end % getInviscid_p_rho_U_fromMachRe