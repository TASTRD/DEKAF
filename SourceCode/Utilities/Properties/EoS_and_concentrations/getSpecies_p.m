function p_s = getSpecies_p(y_s,TTrans,Tel,R_s,rho,bElectron)
% GETSPECIES_P(y_s,TTrans,Tel,R_s,rho,bElectro) returns the matrix of
% species partial pressures using the ideal gas law.
%
% The inputs are
%   y_s         -   species mass fractions, matrix of size (N_eta x N_spec)
%   TTrans      -   Temperature for the translational mode (N_eta x 1)
%   Tel         -   Temperature for the electrons (N_eta x 1)
%   R_s         -   species gas constant, vector of size (N_spec x 1)
%   rho         -   mixture density (N_eta x 1)
%   bElectron   -   electron boolean (N_spec x 1)
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec]  = size(y_s);
TTrans          = repmat(TTrans,[1,N_spec]);
Tel             = repmat(Tel,[1,N_spec]);
R_s             = repmat(R_s',[N_eta,1]);
rho             = repmat(rho,[1,N_spec]);
bElectron       = repmat(bElectron',[N_eta,1]);

p_s = eval_func_Heavy_el(@(T) y_s.*R_s.*T.*rho ,TTrans,Tel,bElectron);