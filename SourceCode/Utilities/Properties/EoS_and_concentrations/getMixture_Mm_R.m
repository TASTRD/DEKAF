function [Mm,R,Mm_star,R_star] = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron)
% GETMIXTURE_MM_R Calculate the mixture molar mass and gas constant it is
% defined as:
%
%   Mm      = (sum y_s/Mm_s)^-1
%   R       = R0/Mm
%
%   Mm_star = [sum_sH y_s(s)/Mm_s(s) + y_s(e-)/Mm_s(e-) * Tel/TTrans]^-1
%   R_star  = R0/Mm_star
%
% where sH is the set of all heavy particles and e- represents electrons
%
%   The advantage of these "star" quantities is that, in thermal
%   nonequilibrium, the ideal gas law may be used directly. That is,
%   rho = p/(R_star*TTrans)
%
% Usage:
%   (1)
%       Mm = getMixture_Mm_R(y_s,[],[],Mm_s,[],[]);
%
%   (2)
%       [Mm,R] = getMixture_Mm_R(y_s,[],[],Mm_s,R0,[]);
%
%   (3)
%       [Mm,R,Mm_star,R_star] = getMixture_Mm_R(y_s,TTrans,Tel,Mm_s,R0,bElectron)
%
% Inputs:
%   y_s         -   species mass fractions, matrix of size (N_eta x N_spec)
%   TTrans      -   Temperature for the translational mode (N_eta x 1)
%   Tel         -   Temperature for the electrons (N_eta x 1)
%   Mm_s        -   species mixture molar mass, vector of size
%                   (N_spec x 1)
%   R0          -   universal gas constant
%   bElectron   -   electron boolean (N_spec x 1)
%   dys_dT      -   derivative of mass fraction with respect to
%                   equilibrium temperature. Note that this is only
%                   nonzero in chemical equilibrium.
%
% Outputs:
%   Mm          -   mixture molar mass, matrix of size (N_eta x 1)
%   R           -   mixture gas constant, matrix of size (N_eta x 1)
%   Mm_star     -   Mm correcting for the various species temperatures, matrix of size (N_eta x 1)
%   R_star      -   R correcting for the various species temperatures, matrix of size (N_eta x 1)
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);
Mm_s        = repmat(Mm_s',[N_eta,1]);

% Standard definition of mixture molar mass and the gas constant
sumand      = y_s./Mm_s;
Mm          = (sumand*ones(N_spec,1)).^(-1); % summing vectorially
if nargout>1 % compute specific gas constant
    R       = R0./Mm;
end

% Alternate definitions to ease the computation of ideal gas properties
% in thermal nonequilibrium
if nargout>2
    bElectron   = repmat(bElectron',[N_eta,1]);
    TTrans      = repmat(TTrans,[1,N_spec]);
    Tel         = repmat(Tel,[1,N_spec]);
    sumand_star = eval_func_Heavy_el(@(T) y_s./Mm_s .* T./TTrans,TTrans,Tel,bElectron);
    Mm_star     = (sumand_star*ones(N_spec,1)).^(-1);
    R_star      = R0./Mm_star;
end

end % getMixture_Mm_R.m
