function ys_out = enforce_chargeNeutralityCond(ys_in,bElectron,bPos,Mm_s,R0,varargin)
% enforce_chargeNeutralityCond enforces the charge-neutrality condition on
% a given set of mass fractions or mole fractions.
%
% The charge-neutrality condition states that the net charge must be zero
% in all points. In other words, that the sum of positive and negative
% charges must be the same.
%
% IMPORTANT: this function does not preserve the concentration condition
% (sum of mole/mass fractions equals to one), and it should therefore be
% followed with the use of enforce_concCond on a DIFFERENT BATH SPECIES,
% such that both the concentration and the charge-neutrality assumption are
% simultaneously satisfied.
%   It is therefore good practice to let the bath species for the
% conentration condition be always different from the electron species, and
% let the electron species be the one on which the charge-neutrality
% condition is enforced.
%
% NOTE: this is only valid for mixtures without double ionization (E.g.:
% O++) and without anions (E.g.: H-). In other words, all charged particles
% must be either electrons or +1 cations.
%
% Examples:
%   (1) X_s         =         [0.1,0.2,0.4,0.05,0.01,0.04,0.2]
%       bElectron   = logical([1,  0,  0,  0,   0,   0,   0  ])
%       bPos        = logical([0,  1,  0,  1,   0,   1,   0  ])
%       Mm_s = []; R0 = []; % not used for X_s without ys_lim
%       X_s = enforce_chargeNeutralityCond(X_s,bElectron,bPos,Mm_s,R0)
%           --->              [0.29,0.2,0.4,0.05,0.01,0.04,0.2]
%
%   (2) X_s = enforce_chargeNeutralityCond(X_s,bElectron,bPos,Mm_s,R0,idxBath)
%       |--> allows to specify which species should be used as the bath
%       (obtained as one minus the others). By default it is the electron
%       species.
%
%   (3) y_s = enforce_chargeNeutralityCond(...,'massFrac')
%       |--> same story but for mass fractions
%
%   (4) y_s = enforce_chargeNeutralityCond(...,'ys_lim',ys_lim)
%       |--> allows to set a lower limit on the mass fraction
%
% Inputs & outputs:
%   y_s         cell or matrix of mass fractions arranged in columns [-]
%   X_s         cell or matrix of mole fractions arranged in columns [-]
%   bElectron   electron boolean (size N_spec x 1)
%   bPos        positive boolean (size N_spec x 1)
%   TTrans      translational temperature (size N_eta x 1)
%   Tel         electron temperature (size N_eta x 1)
%   Mm_s        species molar mass (size N_spec x 1)
%   R0          ideal gas constant (size 1 x 1)
%
% See also: setDefaults, enforce_concCond
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% checking inputs
[varargin,bMassFrac] = find_flagAndRemove('massFrac',varargin); % checking for the massFrac flag
[varargin,bYLim,idx] = find_flagAndRemove('ys_lim',varargin); % checking for the ys_lim flag
if bYLim
    ys_lim = varargin{idx};
    varargin = varargin([1:idx-1,idx+1:end]);
end
if isempty(varargin) % checking for the bath index
    idxBath = find(bElectron); % default
else
    idxBath = varargin{1};
end

% transforming if cell input
if iscell(ys_in)
    cell_flag = true;
    y_s = cell1D2matrix2D(ys_in);
else
    cell_flag = false;
    y_s = ys_in;
end

% if we did not receive the mole fraction
if bMassFrac                                    % if the user input mass fraction
    if bYLim                                        % if we must limit the mass fraction
        y_s(y_s<ys_lim) = ys_lim;
        y_s(y_s>1) = 1;
    end
    [Mm,~] = getMixture_Mm_R(y_s,[],[],Mm_s,R0,[]);
    X_s = getSpecies_X(y_s,Mm,Mm_s);
else                                            % if the user input mole fraction
    X_s = y_s;
    if bYLim                                        % if we must limit the mass fraction
        y_s = getEquilibrium_ys(X_s,Mm_s);              % computing mass fraction from mole fraction
        y_s(y_s<ys_lim) = ys_lim;                       % limiting mass fraction
        [Mm,~] = getMixture_Mm_R(y_s,[],[],Mm_s,R0,[]); % computing mixture molar mass
        X_s = getSpecies_X(y_s,Mm,Mm_s);                % computing mole fraction from mass fraction
    end
end

% Imposing condition
if nnz(bElectron) % it must only be applied if there are indeed charged species
    N_pos = nnz(bPos);
    if bElectron(idxBath)                               % electron as bath species
        X_s(:,idxBath) = X_s(:,bPos)*ones(N_pos,1);         % making the electron species equal to the sum of the ions
    elseif bPos(idxBath)                                % ion as bath species
        X_s(:,idxBath) = X_s(:,bElectron) - X_s(:,and(bPos,((1:length(bPos)).'~=idxBath)))*ones(N_pos-1,1);
    else
        error('the bath species must be a charged species (e-, N+, O+, etc.)');
    end
end

% If we receive mass fraction, we transform back to it
if bMassFrac
    y_s = getEquilibrium_ys(X_s,Mm_s);
else
    y_s = X_s; % if the user input mole fraction
end

% transforming back to the original input
if cell_flag
    ys_out = matrix2D2cell1D(y_s);
else
    ys_out = y_s;
end

end % enforce_chargeNeutralityCond