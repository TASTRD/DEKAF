function n_s = getSpecies_n(rho,y_s,Mm_s,nAvogadro)
% getSpecies_n computes the species number density
%
%   Examples:
%       (1)     n_s = getSpecies_n(rho,y_s,Mm_s,nAvogadro)
%
%   Inputs and Outputs:
%       rho             size N_eta x 1
%       y_s             size N_eta x N_spec
%       Mm_s            size N_spec x 1
%       nAvogadro       size 1 x 1
%       n_s             size N_eta x N_spec
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

% sizes
[N_eta,N_spec] = size(y_s);
rho = repmat(rho,[1,N_spec]);
Mm_s = repmat(Mm_s',[N_eta,1]);

% expression
n_s = nAvogadro.*rho.*y_s./Mm_s;