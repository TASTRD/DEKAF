function n = getMixture_n(y_s,rho,Mm_s,nAvogadro)
%GETMIXTURE_N Calculate the mixture number density [1/m^3]
%
% Usage
%   n = getMixture_n(y_s,rho,Mm_s,nAvogadro);
%
% Note: this is also valid for multitemperature models as long as rho is
% computed accounting for these multiple temperatures
%
% Inputs and Outputs
%   y_s         N_eta x N_spec
%   rho         N_eta x 1
%   Mm_s        N_spec x 1
%   nAvogadro   1 x 1
%
%   n           N_eta x 1
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);
rho2D = repmat(rho,[1,N_spec]);
Mm_s2D = repmat(Mm_s',[N_eta,1]);

n = (y_s.*rho2D*nAvogadro./Mm_s2D) * ones(N_spec,1);

end % getMixture_n.m
