function [J,F] = build_LTEJacobianF(flag,delta_nuEq,elConsCoeffEq,o_3D,lnKpeq,p,varargin)
%BUILD_LTEJACOBIANF Generate the LTE analytical jacobian matrix and the
% Newton-Raphson vector to be iterated on to zero.
%
% For cases 'doubleLoop', 'mtimesx'
% [J,F] = build_LTEJacobianF('mtimesx',delta_nuEq,elConsCoeffEq,o_3D,lnKpeq,p)
%
% For case 'mtimesxh'
% [J,F] = build_LTEJacobianF('mtimesxh',delta_nuEq,elConsCoeffEq,o_3D,lnKpeq,p,...
%         hLoop,dh_dlnT,dh_dos,dlnKpeq_dlnT,h);
%
% For case 'mtimesxs'
% [J,F] = build_LTEJacobianF('mtimesxs',delta_nuEq,elConsCoeffEq,o_3D,lnKpeq,p,...
%         sLoop,ds_dlnT,ds_dos,dlnKpeq_dlnT,s);
%
% Required sizes for inputs
%   delta_nuEq      : N_spec x N_reac
%   elConsCoeffEq   : N_el-1 x N_spec x N_Tp
%   o_3D            : N_spec x 1 x N_Tp
%   lnKpeq          : N_reac x 1 x N_Tp
%   p               : N_Tp x 1
%   hLoop           : 1 x 1 x N_Tp
%   dh_dlnT         : 1 x 1 x N_Tp
%           NOTE: dh_dlnT corresponds to cp*T, since dh/dT = cp, and
%           dT = 1/T * dlnT
%   dh_dos          : N_spec x 1 x N_Tp
%   h               : N_reac x 1 x N_Tp
%   dlnKpeq_dlnT    : N_reac x 1 x N_Tp
%   sLoop           : 1 x 1 x N_Tp
%   ds_dlnT         : 1 x 1 x N_Tp
%   ds_dos          : N_spec x 1 x N_Tp
%   s               : N_reac x 1 x N_Tp
%
% Outputs:
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_spec,~,N_Tp] = size(o_3D);

switch flag
    case 'mtimesx'
        % no extra inputs
    case {'mtimesxh','mtimesxs'} % 'mtimesxh' method in getEquilibrium_X.m
        QLoop           = varargin{1};
        dQ_dlnT         = varargin{2};
        dQ_dos          = varargin{3};
        dlnKpeq_dlnT    = varargin{4};
        Q               = varargin{5};
    otherwise
        error(['the chosen flag ''',flag,''' is not supported']);
end

N_reac = size(lnKpeq,1);
N_el = N_spec - N_reac;

% Reshape inputs for 4D calculations
unity_1D        = ones(1,1,N_Tp);                        % (1,1,N_Tp)
ones_3D         = ones(N_spec,1,N_Tp);                   % (N_spec,1,N_Tp)
delta_nuEq_3D   = repmat(delta_nuEq',[1,1,N_Tp]);        % (N_spec,N_reac) --> (N_reac,N_spec,N_Tp)
o_3D            = permute(o_3D,[2,1,3]);                  % (N_spec,1,N_Tp) --> (1,N_spec,N_Tp)
o_3D_mat        = repmat(o_3D,[N_reac,1,1]);              % (1,N_spec,N_Tp) --> (N_reac,N_spec,N_Tp)
o_3D_elmat      = repmat(o_3D,[N_el-1,1,1]);              % (1,N_spec,N_Tp) --> (N_el-1,N_spec,N_Tp)
p_3D            = repmat(reshape(p,[1,1,N_Tp]),[N_reac,1,1]);  % (N_reac,1,N_Tp)

J = [   delta_nuEq_3D                       ;   % constitutive reactions (dissociation, ionization, etc.)
        elConsCoeffEq.*exp(o_3D_elmat)      ;   % elemental fraction conservation equations
        exp(o_3D)                           ;   % molar balance
    ];


F = [   mtimesxIf(o_3D_mat.*delta_nuEq_3D,ones_3D) - lnKpeq + mtimesxIf(delta_nuEq_3D,ones_3D).*log(p_3D);  % constitutive reactions
        mtimesxIf(elConsCoeffEq.*exp(o_3D_elmat),ones_3D);        % elemental fraction balance equations
        mtimesxIf(exp(o_3D),ones_3D) - unity_1D;                  % molar balance
    ];

if ismember(flag,{'mtimesxh','mtimesxs'})
    % append additional row and column to J
    right_J_col = [ -dlnKpeq_dlnT;              % Chain rule wrt temperature
                    zeros(N_el-1,1,N_Tp);    % Elemental fraction balance equations are unaffected when iterating on enthalpy
                    zeros(1,1,N_Tp);         % molar balance
                    ];

    dQ_dos = permute(dQ_dos,[2,1,3]);
    h_3D = repmat(Q,[1,N_spec,1]);
    J = [   J           , right_J_col ;
          dQ_dos./h_3D  ,   dQ_dlnT./Q    ];     % Final row is the enthalpy condition, with dh/dt appended on.

    % append additional row to F
    F = [     F    ;
          QLoop./Q - 1  ];        % we normalize the enthalpy expression with the objective enthalpy
end