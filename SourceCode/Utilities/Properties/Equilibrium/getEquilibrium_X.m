function [X_s,varargout] = getEquilibrium_X(varargin)
% getEquilibrium_X computes the equilibrium composition for all possible
% combiniations of pressure and temperature.
%
% Usage:
%   (1)
%       X_s = getEquilibrium_X(p,T,lnKpeq,X_E,mixCnst)
%           |--> computes the composition with the default method
%           'doubleLoop'
%   (2)
%       X_s = getEquilibrium_X(p,T,lnKpeq,X_E,mixCnst,method)
%           |--> allows to pass a string with the method to be used
%   (3)
%       X_s = getEquilibrium_X(p,T,lnKpeq,X_E,mixCnst,method,options)
%           |--> allows to pass a structure with various options
%   (4)
%       [X_s,T] = getEquilibrium_X(p,h,X_E,mixCnst,'mtimesxh',mixture,options)
%           |--> solves the equilibrium system including also the
%           expression of the mixture enthalpy
%   (5)
%       [X_s,T,cp] = getEquilibrium_X(p,h,X_E,mixCnst,'mtimesxh',mixture,options)
%           |--> returns also the FROZEN heat capacity at cnst. press.
%   (6)
%       [X_s,T] = getEquilibrium_X(p,s,X_E,mixCnst,'mtimesxs',mixture,options)
%           |--> solves the equilibrium system including also the
%           expression of the mixture entropy
%   (7)
%       [...] = getEquilibrium_X(...,'Xslim',Xslim)
%           |--> allows to pass a lower limit on the mole fractions. By
%           default it is 1e-35
%
% Inputs:
%   p       [Pa]            (N_p x 1) or (N_Tp x 1) if options.pTEqVecs = true
%       vector of pressures 
%   T       [K]             (N_T x 1) or (N_Tp x 1) if options.pTEqVecs = true
%       vector of temperatures
%   h       [J/kg]          (N_Tp x 1)
%       vector of mixture enthalpies size (N_Tp)
%   s       [J/kg-K]        (N_Tp x 1)
%       vector of mixture entropies
%   lnKpeq  [log(Pa)]       (N_T x N_reac) or (N_Tp x 1) if options.pTEqVecs = true
%       vector of natural logarithms of the equilibrium constants for the reactions to be considered
%   X_E     [-]             (N_Tp x N_elem) or (1 x N_elem)
%       matrix with the atomic fractions to be used for the obtention of
%       the equilibrium composition 
%   method
%       string identifying the method to obtain the equilibrium
%       composition. Supported values are:
%           'doubleLoop'
%               makes a double loop over pressure and temperature, using
%               the previous composition as the initial guess for the next
%               Newton-Raphson iteration.
%           'mtimesx'
%               uses the package 'mtimesx' to avoid loops and perform the
%               matrix multiplication all at once. NOT RECOMMENDED for
%               ionized mixtures at low temperatures
%           'mtimesxh'
%               uses the package 'mtimesx' and solves the equilibrium
%               system of equations including also the expression for the
%               mixture enthalpy from the temperature.  
%           'mtimesxs'
%               uses the package 'mtimesx' and solves the equilibrium
%               system of equations including also the expression for the
%               mixture entropy from the temperature.  
%   options
%       structure with several options, such as:
%           .display
%               boolean to display messages to the user
%               default: false
%           .pTEqVecs
%               boolean that indicates that p and T are equally-sized
%               vectors of size N_Tp x 1 (true) or if they have different
%               sizes Np and NT (false)
%               default: false
%           .T_tol          [-]
%               tolerance to be used in the NR method
%               default: 1e-13
%           .T_itMax        [-]
%               maximum iteration count in the NR method
%               default: 50
%           .X_mat0         [-]                     (N_Tp x N_spec)
%               matrix to be used as the initial guess for the molar
%               fractions 
%               default: 1/N_spec*ones(N_Tp,N_spec)
%           .T0             [K]
%               vector to be used as the initial guess for the temperature
%
% Outputs:
%   X_s     [-]             (N_Tp x N_spec)
%       vector of pressures 
%   T       [K]             (N_Tp x 1)
%       vector of temperatures
%   cp      [J/kg-K]        (N_Tp x 1)
%       vector of heat capacities
%
% See also: mtimesx, multiInv
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

[idxE,varargin] = parse_optional_input(varargin,'idxE',1);
[varargin,~,idx] = find_flagAndRemove('Xslim',varargin);
if isempty(idx);    Xmin = 1e-35;
else;               Xmin = varargin{idx};
end
Xmax = 1;

switch length(varargin)
    case 5
        p       = varargin{1};
        T       = varargin{2};
        lnKpeq  = varargin{3};
        X_E     = varargin{4};
        mixCnst = varargin{5};
        method  = 'doubleLoop';
        options = struct;
    case 6
        if ~ischar(varargin{5}) % it means that the user is inputting T
            p       = varargin{1};
            T       = varargin{2};
            lnKpeq  = varargin{3};
            X_E     = varargin{4};
            mixCnst = varargin{5};
            method  = varargin{6};
            options = struct;
        else
            method  = varargin{5};
            error(['the number of inputs provided is not appropriate for the chosen method ''',method,'''']);
        end
    case 7
        if ~ischar(varargin{5}) % it means that the user is inputting T
            p       = varargin{1};
            T       = varargin{2};
            lnKpeq  = varargin{3};
            X_E     = varargin{4};
            mixCnst = varargin{5};
            method  = varargin{6};
            options = varargin{7};
        elseif ismember(varargin{5},{'mtimesxh','mtimesxs'}) % it means the user is inputting h or s (not defined for the moment)
            p       = varargin{1};
            Q       = varargin{2}; % h or s, depending on the case
            X_E     = varargin{3};
            mixCnst = varargin{4};
            method  = varargin{5};
            mixture = varargin{6};
            options = varargin{7};
            T = Q; % T and lnKpeq are defined just as place holders, such that the blocks before the switch work also for the mtimesxh option
            lnKpeq = mixCnst.reac_list_eq(:)';
        else
            method  = varargin{5};
            error(['the number of inputs provided is not appropriate for the chosen method ''',method,'''']);
        end
    otherwise
        error('wrong number of inputs');
end

% analyzing inputs
if isfield(options,'EoS') && ~ismember(options.EoS,{'idealGas'})
    error(['The chosen equation of state ''',options.EoS,''' is not supported']);
end
options = standardvalue(options,'display',false);
options = standardvalue(options,'pTEqVecs',true);
options = standardvalue(options,'T_tol',1e-13);
options = standardvalue(options,'T_itMax',200);
if ~options.pTEqVecs    % p and T are not equally sized vectors
    N_T = length(T);            % number of temperature/enthalpy values
    N_p = length(p);            % number of pressure values
    N_Tp = N_T*N_p;             % total number of values
    p = reshape(repmat(p(:).',[N_T,1]),[N_Tp,1]); % (p,1) --> (p*T,1)
    T = reshape(repmat(T(:)  ,[1,N_p]),[N_Tp,1]); % (T,1) --> (p*T,1)
else                    % they are
    if length(p)~=length(T) % check that they are
        error(['options.pTEqVecs was set to true (p & T equally sized vectors), yet p has a length of ',num2str(length(p)),' and T has a length of ',num2str(length(p))]);
    end
    N_Tp = length(p);       % total number of values
end
if size(X_E,1)==1           % constant elemental fractions
    X_E = repmat(X_E,[N_Tp,1]); % repeating over pT, to make the rest of the code consistent
end

N_el = size(X_E,2);           % number of elements
N_reac = size(lnKpeq,2);    % number of reactions
N_spec = N_reac + N_el;     % number of species

% Extract fields from mixCnst struct
delta_nuEq      = mixCnst.delta_nuEq;      % stoichiometry of equilibrium reactions    [N_reac x N_spec]
spec_list       = mixCnst.spec_list;        % list of species                           [N_spec x 1]
elemStoich_mat  = mixCnst.elemStoich_mat;   % elemental stoichiometric matrix           [N_el x N_spec]
options = standardvalue(options,'X_mat0',1/N_spec*ones(N_Tp,N_spec)); % initial guess for the molar fractions

% making p and T into column vectors (in case)
p = p(:);
T = T(:);

% Construct elemental stoichiometric matrices
elConsCoeffEq = build_LTEElemCoeffs(elemStoich_mat,X_E,spec_list,'elID',idxE);

% HARDCODE WARNING!
% We introduce a relaxation factor to be used when converging the mtimesxh
% method for mixtures containing electrons.
%  To compensate it, we also pump up the maximum temperature iterations to
% T_itmax_Multiplier times whatever the user chose (thus accounting for the
% extra iteration count needed due to the relaxation).
switch options.LTE_NR_relax
    case 0  % no relaxation
        relaxFactor0 = 1;           % minimum relaxation factor
        OoM_conv0 = 4;              % irrelevant
        OoM_conv1 = 16;             % irrelevant
        T_itmax_Multiplier = 1;     % factor by which the maximum number of T iterations must be increased due to the relaxation
    case 1  % light relaxation
        relaxFactor0 = 0.1;         % minimum relaxation factor
        OoM_conv0 = 5;              % order of magnitude of the convergence criterion after which the relaxation factor will start to increase
        OoM_conv1 = 10;             % order of magnitude of the convergence criterion when the relaxation factor will reach one
        T_itmax_Multiplier = 5;     % factor by which the maximum number of T iterations must be increased due to the relaxation
    case 2  % moderate relaxation
        relaxFactor0 = 0.1;         % minimum relaxation factor
        OoM_conv0 = 6;              % order of magnitude of the convergence criterion after which the relaxation factor will start to increase
        OoM_conv1 = 16;             % order of magnitude of the convergence criterion when the relaxation factor will reach one
        T_itmax_Multiplier = 5;     % factor by which the maximum number of T iterations must be increased due to the relaxation
    case 3  % strictest relaxation
        relaxFactor0 = 0.1;         % minimum relaxation factor
        OoM_conv0 = 16;             % order of magnitude of the convergence criterion after which the relaxation factor will start to increase
        OoM_conv1 = 16;             % order of magnitude of the convergence criterion when the relaxation factor will reach one
        T_itmax_Multiplier = 10;    % factor by which the maximum number of T iterations must be increased due to the relaxation
    otherwise
        error(['value of ',options.LTE_NR_relax,' for options.LTE_NR_relax is currently unsupported!'])
end

switch method
    case 'doubleLoop'
        % Looping T and p and computing equilibrium composition with the
        % Newton-Raphson algorithm
        % We will work with the logarithm of the molar fraction, which we call o
        o_3D = log(options.X_mat0(1,:));                % obtaining initial guess from the user
        o_3D = reshape(o_3D,[N_spec,1]);                % (1,N_spec) --> (N_spec,1)
        tol = options.T_tol;                            % convergence criterion
        it_max = options.T_itMax;                       % maximum number of iterations
        o_out = zeros(N_Tp,N_spec);                     % initializing output

        % Prepare inputs for NR J & F construction
        if ~options.pTEqVecs && size(lnKpeq,1)==N_T
            lnKpeq = permute(repmat(lnKpeq,[N_p,1]),[2,3,1]); % (N_T,N_reac) --> (N_reac,1,Ntot)
        else
            lnKpeq = permute(lnKpeq,[2,3,1]); % (Ntot,N_reac) --> (N_reac,1,Ntot)
        end

        for iTp = 1:N_Tp
            it = 0;                                                         % initializing counter
            conv = 1;                                                       % initializing convergence tracker
            if iTp>1                                                        % we look for the previous p-T combination that is closest to ours
                dist = sqrt((T(1:iTp-1)/T(iTp)-1).^2 + (p(1:iTp-1)/p(iTp)-1).^2);   % "distance" in the T-p space
                [~,idx] = min(dist);                                                % index of the point to take as a reference
                o_3D = reshape(o_out(idx,:),[N_spec,1]);                            % obtaining initial guess
            end
            while conv>tol && it<it_max
                it = it+1;
                [J,F] = build_LTEJacobianF('mtimesx',delta_nuEq,elConsCoeffEq(:,:,iTp),o_3D,lnKpeq(:,:,iTp),p(iTp));
                do = -J\F;
                o_3D = o_3D+do;
                if it~=1
                    conv = norm(o_3D-Z_prev,inf);
                end
                Z_prev = o_3D;
            end
            o_out(iTp,:) = reshape(o_3D,[1,N_spec]);
            dispif(['p = ',num2str(p(iTp)),' Pa & T = ',num2str(T(iTp)),' K with err. ',num2str(conv),' after ',num2str(it),' it. Completed ',num2str(iTp/N_Tp * 100,'%0.1f'),'%'],options.display);
        end
        X_s = exp(o_out); % molar fraction
    case 'mtimesx'
        % obtaining initial guess for the molar fractions
        X_0 = options.X_mat0;                                                   % input must be N_pT x N_spec
        % reshaping and repmatting to obtain the jacobian 4D matrix
        if ~options.pTEqVecs && size(lnKpeq,1)==N_T
            lnKpeq_3D = permute(repmat(lnKpeq,[N_p,1]),[2,3,1]); % (N_T,N_reac) --> (N_reac,1,Ntot)
        else
            lnKpeq_3D = permute(lnKpeq,[2,3,1]); % (Ntot,N_reac) --> (N_reac,1,Ntot)
        end
        X_0 = permute(X_0,[2,3,1]);                                           % (Tp,s) -> (s,1,Tp)
        o_3D = log(X_0);                                                        % obtaining initial guess for the natural logarithm of the molar fraction (o)

        % looping and running Newton-Raphson iteration
        it = 0;                         % initializing counter
        conv = 1;                       % initializing convergence tracker
        tol = options.T_tol;            % convergence criterion
        it_max = options.T_itMax;       % maximum number of iterations
        while conv>tol && it<it_max*T_itmax_Multiplier
            it = it+1;
            relaxFactor = getRelaxationFactor(conv,relaxFactor0,OoM_conv0,OoM_conv1); % relaxation factor to obtain better convergence when electrons are present

            [J,F] = build_LTEJacobianF(method,delta_nuEq,elConsCoeffEq,o_3D,lnKpeq_3D,p);
            Jinv = multiInv(J);          % computing inverse of the jacobian vectorially
            do = -mtimesxIf(Jinv,F);      % computing matrix product vectorially
            o_3D = o_3D+relaxFactor*do;

            % imposing bounds on the mole fractions
            o_3D(o_3D<log(Xmin)) = log(Xmin);
            o_3D(o_3D>log(Xmax)) = log(Xmax);

            % Studying convergence
            if it~=1
                conv = norm(exp(o_3D(:))-exp(Z_prev(:)),inf);
            end
            Z_prev = o_3D;
            dispif(['It. ',num2str(it),' with error ',num2str(conv)],options.display);
        end
        % displaying warning message if maximum iteration count is reached
        if it == it_max
            warning(['Maximum iteration count (',num2str(it_max),') reached with a convergence of ',num2str(conv),', larger than the fixed tolerance ',...
                num2str(tol),'. These can be changed through options.T_itMax and options.T_tol respectively.']);
        end
        % Obtaining mass fractions
        X_3D = exp(o_3D);                           % computing the molar fraction from the fraction of the partial pressure to the total
        X_s = permute(X_3D,[3,1,2]);              % transforming output to have it in size N_Tp x N_spec
    case {'mtimesxh','mtimesxs'} % includes expression of the mixture enthalpy or entropy again
        % DEVELOPERS NOTE: this case has a particularly large amount of dimension
        % changes, swaps and reshapings, which can very easily become incredibly
        % confusing. One may distinguish two main matrix arrangements:
        %
        %   1st) The 2D matrix arrangement in which inputs and initial guesses are
        %   received.
        %   In it, the 1st dimension corresponds to the
        %   enthalpy/temperature - pressure points to be solved, and the
        %   2nd is for species identifiers.  The 2nd dimension may
        %   eventually also be used to account for other identifiers, such
        %   as the reactions.
        %
        %   2nd) The 3D matrix arrangement used to resolve the problem in a
        %   vectorial form using mtimesx and multiInv.
        %   In it, the first two dimensions are for NR variable and equation
        %   identifier. The first dimension will be populated with the different
        %   variables to solve for: natural logs of the mole fractions and
        %   temperature, as well as the functions to be zeroed for that purpose:
        %   equilibrium conditions, atomic fraction conservation and dalton's law.
        %   The second dimension is used to build the jacobian of the functions to
        %   be zeroed.
        %   The third dimension is for the different enthalpy/temperature -
        %   pressure points to be solved, and any additional dimensions are
        %   used for reactions, or any other needed indicator.
        %
        %   3rd) The 2D matrix arrangement required by getMixture_h_cp
        %   In it, the first dimension is for all computational points, i.e., those
        %   enthalpy/temperature and pressure points to be solved.
        %   The second dimension is for species identifiers, and any additional
        %   dimension may be used, either for additional species identifiers (like
        %   in the case of the dyl_dXs jacobian, which needs two species
        %   identifiers s & l), or for additional information (like the different
        %   electronic states in gDegen_s and thetaElec_s).
        %
        % Whenever a switch between these two arrangements is done, a comment such
        % as (s,1,Tp) --> (Tp,s) will indicate what change is being done. The
        % previous example would denote a switch from the 3D nomenclature, with
        % species indicators in the 1st dimension, a unit 2nd dim., and
        % enthalpy/temperature - pressure points in the 3rd; to the 2D
        % nomenclature, with enthalpy/temperature points in the 1st dimension,
        % and species indicators in the 2nd.

        clear T; % to avoid confusion, T, that was used ambiguously before the switch, is now cleaned

        % obtaining initial guess for the molar fractions
        X_0 = options.X_mat0;                                                   % input must be N_Tp x N_spec
        neededvalue(options,'T0','For the ''mtimesxh'' method, an initial guess for the temperature should be passed through options.T0');
        Tn  = options.T0;                                                       % input must be N_Tp

        % reshaping and repmatting to obtain the jacobian 4D matrix
        Tn_3D = reshape(Tn,[1,1,N_Tp]);                                                 % (Tp) --> (1,1,Tp)
        X_0 = permute(X_0,[2,3,1]);                                                     % (Tp,s) --> (s,1,Tp)
        o_3D = log(X_0);                                                                % obtaining initial guess for the natural logarithm of the molar fraction (o)
        Q_3D = permute(Q,[2,3,1]);                                                      % (Tp) --> (1,1,Tp)
        Z = [o_3D;log(Tn_3D)];                                                          % creating vector of unknowns (logarithms of mass fractions + temperature)

        % extracting everything from mixCnst and preparing for the
        % evaluation of the equilibrium constants and their temp. derivat.
        R_s = mixCnst.R_s;                      % dimensions (s,1)
        Mm_s = mixCnst.Mm_s;                    % dimensions (s,1)
        nAtoms_s = mixCnst.nAtoms_s;            % dimensions (s,1)
        thetaVib_s = mixCnst.thetaVib_s;        % dimensions (s,1)
        hForm_s = mixCnst.hForm_s;              % dimensions (s,1)
        bElectron = mixCnst.bElectron;          % dimensions (s,1)
        thetaElec_s = mixCnst.thetaElec_s;      % dimensions (s,el)
        gDegen_s = mixCnst.gDegen_s;            % dimensions (s,el)
        R0 = mixCnst.R0;                        % dimensions (1,1)
        nAvogadro = mixCnst.nAvogadro;          % dimensions (1,1)
        hPlanck = mixCnst.hPlanck;              % dimensions (1,1)
        kBoltz = mixCnst.kBoltz;                % dimensions (1,1)
        L_s = mixCnst.L_s;                      % dimensions (s,1)
        sigma_s = mixCnst.sigma_s;              % dimensions (s,1)
        thetaRot_s = mixCnst.thetaRot_s;        % dimensions (s,1)
        Keq_struc = mixCnst.Keq_struc;

        % Preparing enthalpy-specific properties
        [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,~] = ... (s,1) --> (Tp,s)
            reshape_inputs4enthalpy(R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,Tn);
        Mm_s_mat = repmat(Mm_s',[N_Tp,1]);                                       % (s,1) --> (Tp,s)
        Mm_l_3D = repmat(reshape(Mm_s,[1,1,N_spec]),[N_Tp,N_spec,1]);            % (l,1) --> (Tp,s,l)
        Mm_s_3D = repmat(reshape(Mm_s,[1,N_spec,1]),[N_Tp,1,N_spec]);            % (s,1) --> (Tp,s,l)
        delta_nuEq = mixCnst.delta_nuEq;                                            % difference between products and reactants in eq. cond.

        % dirac-delta
        delta_sl = eye(N_spec);                                                     % 2-Dimensional identity matrix
        delta_sl_3D = repmat(reshape(delta_sl,[1,N_spec,N_spec]),[N_Tp,1,1]);    % (s,l) --> (Tp,s,l)

        % looping and running Newton-Raphson iteration
        it = 0;                                         % initializing counter
        conv = 1;                                       % initializing convergence tracker
        tol = options.T_tol;                            % convergence criterion
        it_max = options.T_itMax*T_itmax_Multiplier;    % maximum number of iterations
        while conv>tol && it<it_max
            it = it+1;
            relaxFactor = getRelaxationFactor(conv,relaxFactor0,OoM_conv0,OoM_conv1); % relaxation factor to obtain better convergence when electrons are present

            % computing y_s with results from previous step
            o_mat = reshape(permute(o_3D,[2,3,1]),[N_Tp,N_spec]);           % (s,1,Tp) --> (Tp,s)
            denom_y = (exp(o_mat).*Mm_s_mat) * ones(N_spec,1);              % denominator in the expression for y_s, dimensions (Tp,1)
            denom_y = repmat(denom_y,[1,N_spec]);                           % (Tp,1) --> (Tp,s)
            y_s = exp(o_mat).*Mm_s_mat./denom_y;                            % computing mass fractions from mole fractions, dimensions (Tp,s)

            % computing T with results from previous step
            Tn = reshape(Tn_3D,[N_Tp,1]);                                   % (1,1,Tp) --> (Tp,1)
            Tn_mat = repmat(Tn,[1,N_spec]);                                 % (Tp,1) --> (Tp,s)

            % computing derivative of y_l with respect to each X_s
            denom_y_3D = repmat(denom_y,[1,1,N_spec]);                                           % (Tp,s) --> (Tp,s,l)
            X_l_3D = repmat(reshape(exp(o_mat),[N_Tp,1,N_spec]),[1,N_spec,1]);                   % (Tp,l) --> (Tp,s,l)
            dyl_dXs = Mm_l_3D.*delta_sl_3D./denom_y_3D - X_l_3D.*Mm_l_3D.*Mm_s_3D./denom_y_3D.^2;% computing mass fraction jacobian with mole fraction, dimensions (Tp,s,l)

            % computing equilibrium constants and their derivatives with temperature
            T_r = repmat(Tn,[1,size(delta_nuEq,2)]);                                            % dimensions (Tp,r)
            lnKeq_r     = getReaction_lnKeq(T_r,Tn,Tn,Tn,Tn,Tn,delta_nuEq,Keq_struc,options);   % dimensions (Tp,r)
            dlnKeqr_dT  = getReactionDer_dlnKeqr_dT(y_s,Tn,delta_nuEq,Keq_struc,options);       % dimensions (Tp,r)
            lnKpeq      = getEquilibrium_lnKpeq(lnKeq_r,R0,Tn,sum(delta_nuEq,1)');              % converting from lnKeq  to lnKpeq
            dlnKpeq_dT  = getEquilibriumDer_dlnKpeq_dT(dlnKeqr_dT,Tn,sum(delta_nuEq,1)');       % converting from dlnKeq_dT to dlnKpeq_dT
            dlnKpeq_dlnT  = repmat(Tn,[1,N_reac]) .* dlnKpeq_dT;                                % converting from d/dT to d/dlnT

            switch method
                case 'mtimesxh'
                    % computing enthalpy and derivatives
                    [Qn,cpFroz,~,dQ_dys]  = getMixture_h_cp(y_s,Tn_mat,Tn_mat,Tn_mat,Tn_mat,Tn_mat,R_s_mat,...
                                            nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
                                            bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);
                    dQ_dT = cpFroz;
                case 'mtimesxs'
                    % computing auxiliary variables (rho and cp_s)
                    rho = getMixture_rho(y_s,p,Tn,Tn,R0,Mm_s,bElectron);
                    [~,cp_s] = getSpecies_h_cp(Tn_mat,Tn_mat,Tn_mat,Tn_mat,Tn_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);
                    % computing entropy and derivatives
                    Qn = getMixture_s(Tn,Tn,Tn,Tn,Tn,rho,y_s,R_s,nAtoms_s,...
                        thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,Mm_s,...
                        nAvogadro,hPlanck,kBoltz,L_s,sigma_s,thetaRot_s,options);
                    dQ_dys = getMixtureDer_ds_dys(y_s,Tn,p,R0,Mm_s,bElectron,nAvogadro,nAtoms_s,...
                        thetaVib_s,thetaElec_s,gDegen_s,hForm_s,hPlanck,...
                        kBoltz,L_s,sigma_s,thetaRot_s,options);
                    dQ_dT = getMixtureDer_ds_dT(y_s,Tn,Tn,p,cp_s,R0,Mm_s,bElectron,nAvogadro);
                otherwise
                    error(['the chosen method ''',method,''' is not supported']);
            end

            % applying chain rule to obtain dQ_dos and dQ_dlnT
            dQ_dyl = repmat(reshape(dQ_dys,[N_Tp,1,N_spec]),[1,N_spec,1]);                        % (Tp,l) --> (Tp,s,l)
            dXs_dos = exp(o_mat);                                                               % this derivative is identical to X_s, dimensions (Tp,s)
            dQ_dos_mat = zeros(N_Tp,N_spec);                                                    % initialising, dimensions (Tp,s)
            for l = 1:N_spec                                                                    % looping species
                dQ_dos_mat = dQ_dos_mat + dQ_dyl(:,:,l) .* dyl_dXs(:,:,l) .* dXs_dos;              % summing over l
            end
            dQ_dlnT = dQ_dT.*Tn;                                                               % we compute dQ_dlnT = T*dQ/dT

            % reshaping to the multiInv/mtimesx shapes
            QLoop = reshape(Qn,[1,1,N_Tp]);                                                     % (Tp,1) --> (1,1,Tp)
            dQ_dlnTLoop = reshape(dQ_dlnT,[1,1,N_Tp]);                                          % (Tp,1) --> (1,1,Tp)
            dQ_dos = permute(reshape(dQ_dos_mat,[N_Tp,N_spec]),[2,3,1]);                        % (Tp,s) --> (s,1,Tp)
            lnKpeq_3D = permute(lnKpeq,[2,3,1]);                                                % (Tp,r) --> (r,1,Tp)
            dlnKpeq_dlnT_3D = permute(dlnKpeq_dlnT,[2,3,1]);                                    % (Tp,r) --> (r,1,Tp)

            [J,F] = build_LTEJacobianF(method,delta_nuEq,elConsCoeffEq,o_3D,lnKpeq_3D,p,QLoop,dQ_dlnTLoop,dQ_dos,dlnKpeq_dlnT_3D,Q_3D);

            Jinv = multiInv(J);          % computing inverse of the jacobian vectorially
            dZ = -mtimesxIf(Jinv,F);      % computing matrix product vectorially
            Z = Z+relaxFactor*dZ;       % applying NR increase

            o_3D = Z(1:N_spec,:,:,:);   % extracting logarithms of mole fractions from variable vector
            Tn_3D = exp(Z(end,:,:,:));  % extracting temperatures from variable vector

            Z_err = exp(Z);             % computing error objective variable
            if it~=1
                Z_ref = [ones(N_spec,1,N_Tp);Tn_3D]; % vector of references with respect to which each solution should be normalized (1 for o and T(n+1) for T)
                conv = norm((Z_err(:)-Z_prev(:))./Z_ref(:),inf);
            end
            Z_prev = Z_err;
            dispif(['It. ',num2str(it),' with error ',num2str(conv)],options.display);

            % imposing bounds on the mole fractions
            o_3D(o_3D<log(Xmin)) = log(Xmin);
            o_3D(o_3D>log(Xmax)) = log(Xmax);
        end
        % displaying warning message if maximum iteration count is reached
        if it == it_max
            warning(['Maximum iteration count (',num2str(it_max),') reached with a convergence of ',num2str(conv),', larger than the fixed tolerance ',...
                num2str(tol),'. These can be changed through options.T_itMax and options.T_tol respectively.']);
        end
        % Obtaining mole fractions
        X_3D = exp(o_3D);                           % computing the molar fraction
        X_s = permute(X_3D,[2,3,1]);              % (s,1,Tp) --> (Tp,s)
        % extracting temperature
        T = reshape(Tn_3D,[N_Tp,1]);                % (1,1,Tp) --> (Tp,1)
        varargout{1} = T;                           % preparing temperature output
        if strcmp(method,'mtimesxh')
        varargout{2} = reshape(cpFroz,[N_Tp,1]);    % preparing cpFroz output (1,1,Tp) --> (Tp,1)
        end
    otherwise
        error(['non-identified method ''',method,'''']);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function relaxFactor = getRelaxationFactor(conv,relaxFactor0,OoM_conv0,OoM_conv1)
% getRelaxationFactor computes the relaxation factor letting it decrease
% linearly with the order of magnitude of conv. After the order of
% magnitude of conv reaches OoM_conv0, the relaxation factor goes linearly
% from relaxFactor0 to 1 (when the order of magnitude of conv reaches
% OoM_conv1).
%
% It is embedded within getEquilibrium_X.
%
% See also: getEquilibrium_X
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

OoM_conv = -log10(conv);                                                    % obtaining order of magnitude of conv
relaxFactor = relaxFactor0 + (OoM_conv-OoM_conv0)/(OoM_conv1-OoM_conv0) * (1-relaxFactor0);  % linear variation of the relaxation factor
relaxFactor(relaxFactor<relaxFactor0)   = relaxFactor0;                     % setting minimum
relaxFactor(relaxFactor>1)              = 1;                                % setting maximum
