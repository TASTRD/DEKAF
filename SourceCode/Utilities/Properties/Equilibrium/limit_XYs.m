function XYs = limit_XYs(XYs,varargin)
%LIMIT_XYS Redefine the mole fraction or mass fraction matrix by enforcing
% a minimum value while obeying Dalton's law / continuity.
%
% Usage:
%   0. Xs = limit_XYs(Xs);
%
%   1. Xs = limit_XYs(Xs,lim);
%
%   2. Xs = limit_XYs(Xs,lim,idx);
%
% Note that this call works also with mass fractions as well, as continuity
% behaves similarly to Dalton's law.
%
%   3. y_s = limit_XYs(y_s);
%
%   4. y_s = limit_XYs(y_s,lim);
%
%   5. y_s = limit_XYs(y_s,lim,idx);
%
% Inputs and Outputs
%   Xs || y_s       mole fraction or mass fraction matrix
%                   N_eta x N_spec
%   lim             double, lower limit for mole / mass fractions
%                   default |--> 1e-35
%   idx             integer, index specifying which species's mole
%                   fraction will be overwritten by enforcing Dalton's law
%                   of partial pressures (sum of mole fractions equal one)
%                   or continuity (sum of mass fractions equal to one)
%                   default |--> 1
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(XYs);

% HARDCODE WARNING Set default values
idx = 1;
lim = 1e-35;

% Read in variable inputs
if nargin>1
    lim = varargin{1};
end
if nargin>2
    idx = varargin{2};
end
if length(idx)==1
    idx = idx*ones(N_eta,1);
end
idx2D = sub2ind([N_eta,N_spec],(1:N_eta).',idx);

% Enforce minimum mole / mass fraction condition
XYs(XYs>1-lim)  = 1-lim;        % no mass fraction can ever be larger than 1
XYs(XYs<lim)    = lim;          % no mass fraction can ever be smaller than lim

% Enforce sum of the partial pressures must equal the total pressure.
% (...or continuity for the mass fractions)
XYs_rest = XYs;                                     % allocating the rest of the mass/mole fractions
XYs_rest(idx2D) = [];                               % removing bath species
XYs_rest = reshape(XYs_rest,[N_eta,N_spec-1]);      % reshaping back to matrix form
XYs(idx2D) = 1 - XYs_rest*ones(N_spec-1,1);         % defining bath species

end % limit_XYs
