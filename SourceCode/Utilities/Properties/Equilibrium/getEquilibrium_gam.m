function gam_eq = getEquilibrium_gam(T,y_s,p,mixCnst,options)
% getEquilibrium_gam returns the mixture's ratio of specific heats in
% equilibrium.
%
% Usage:
%   (1)
%       gam_eq = getEquilibrium_gam(T,y_s,p,mixCnst,options)
%
% Inputs:
%   T           [K]             N_eta x 1
%       temperature
%   y_s         [-]             N_eta x N_spec
%       equilibrium species mass fractions
%   p           [Pa]            N_eta x 1
%       pressure
%   mixCnst
%       structure with the necessary mixture constants
%   options
%       classic DEKAF options structure
%
% Outputs:
%   gam_eq      [-]             N_eta x 1
%       mixture's ratio of specific heats in equilibrium
%
% Notes:
%   N_spec is the number of species in the mixture
%   N_eta is the number of points in the domain
%   N_stat is the number of electronic states for the given species
%   N_vib is the number of vibrational modes for the given species
%
% Children and relation functions:
%   See also getEquilibriumDer_drho_dp, getMixture_gam, getMixture_rho,
%   listOfVariablesExplained 
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);

R0              = mixCnst.R0;                                   % extracting from mixCnstt
Mm_s            = mixCnst.Mm_s;
R_s             = mixCnst.R_s;
bElectron       = mixCnst.bElectron;
nAtoms_s        = mixCnst.nAtoms_s;
thetaVib_s      = mixCnst.thetaVib_s;
thetaElec_s     = mixCnst.thetaElec_s;
gDegen_s        = mixCnst.gDegen_s;
hForm_s         = mixCnst.hForm_s;
AThermFuncs_s   = mixCnst.AThermFuncs_s;
elemStoich_mat  = mixCnst.elemStoich_mat;
delta_nuEq      = mixCnst.delta_nuEq;
qf_r            = ones(1,size(delta_nuEq,2));
qb_r            = qf_r;
Keq_struc       = mixCnst.Keq_struc;
sumDelta_nu     = delta_nuEq.'*ones(N_spec,1);

dlnKeqr_dT = getReactionDerMultiT_dlnKeqr_dT(T,T,T,T,T,delta_nuEq,qf_r,qb_r,Keq_struc,options);
dlnKpeqr_dT = getEquilibriumDer_dlnKpeq_dT(dlnKeqr_dT,T,sumDelta_nu);

X_E     = getElement_XE_from_ys(y_s,Mm_s,elemStoich_mat);       % elemental mole fractions
X_s     = getSpecies_X(y_s,[],Mm_s);                            % species mole fractions
dXs_dp  = getEquilibriumDer_dX_dp_general(X_s,X_E,p,mixCnst);   % gradient of the mole fractions wrt pressure
dXs_dT  = getEquilibriumDer_dX_dT_general(X_s,X_E,dlnKpeqr_dT,mixCnst);% gradient of the mole fractions wrt temperature
dys_dT  = getEquilibriumDer_dys_dq(X_s,dXs_dT,Mm_s);            % gradient of the mass fractions wrt temperature

[~,cp,cv,h_s] = getMixture_h_cp_noMat(y_s,T,T,T,T,T,R_s,nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,options,AThermFuncs_s);

R_s2D   = repmat(R_s.',[N_eta,1]);                              % (s,1) ----> (eta,s)
T2D     = repmat(T,    [1,N_spec]);                             % (eta,1) --> (eta,s)
e_s     = h_s - R_s2D.*T2D;                                     % species internal energies
cp_reac = getEquilibrium_cpreac(h_s,dys_dT);                    % reactive heat capacity at cst pressure
cv_reac = getEquilibrium_cpreac(e_s,dys_dT);                    % reactive heat capacity at cst volume

rho     = getMixture_rho(y_s,p,T,T,R0,Mm_s,bElectron);          % density
drho_dp = getEquilibriumDer_drho_dp(dXs_dp,y_s,p,T,Mm_s,R0);    % density gradient with pressure

gam_eq  = (cp+cp_reac)./(cv+cv_reac) .* rho./(p.*drho_dp);      % equilibrium ratio

end % getEquilibrium_gam
