function kappa_reac = getEquilibrium_kappareac(varargin)
% getEquilibrium_kappareac computes the reacting thermal conductivity in
% equilibrium.
%
% Examples:
%   (1)     kappa_reac = getEquilibrium_kappareac('spec-mix',h_s,rho,D_s,dXs_dT)
%           |--> for species-mixture diffusion
%   (2)     kappa_reac = getEquilibrium_kappareac('spec-spec',h_s,rho_s,D_sl,dXs_dT)
%           |--> for species-species diffusion
%   (3)     kappa_reac = getEquilibrium_kappareac('spec-spec',h_s,rho_s,D_sl,dXs_dT,...
%                                   bElectron,TTrans,rho,y_s,Mm_s,bPos,qEl,nAvogadro,kBoltz)
%           |--> for species-species diffusion of an ionized mixture
%           (includes electric field contribution
%
%   Inputs:
%      h_s          size (N_eta x N_spec)
%      rho_s        size (N_eta x N_spec)
%      rho          size (N_eta x 1)
%      D_sl         size (N_eta x N_spec x N_spec)
%      D_s          size (N_eta x N_spec)
%      dXs_dT       size (N_eta x N_spec)
%
% If the diffusion coefficients correspond to mass-fraction-gradient-driven
% diffusion, then dXs_dT should be changed for dys_dT
%
% For detailed information on the different variables see also:
% listOfVariablesExplained, setDefaults
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

switch varargin{1}
    case 'spec-mix'
        h_s     = varargin{2};
        rho     = varargin{3};
        D_s     = varargin{4};
        dXs_dT  = varargin{5};

        % reshaping and repmatting
        N_spec = size(h_s,2);
        rho = repmat(rho,[1,N_spec]);

        kappa_reac = (h_s.*rho.*D_s.*dXs_dT) * ones(N_spec,1); % summing over s
    case 'spec-spec'
        h_s     = varargin{2};
        rho_s   = varargin{3};
        D_sl    = varargin{4};
        dXs_dT  = varargin{5};

        [N_eta,N_spec] = size(h_s);

        if length(varargin)>5 && any(varargin{6}) % ionized mixtures
            ic=6;
            bElectron   = varargin{ic}; ic=ic+1;
            TTrans      = varargin{ic}; ic=ic+1;
            rho         = varargin{ic}; ic=ic+1;
            y_s         = varargin{ic}; ic=ic+1;
            Mm_s        = varargin{ic}; ic=ic+1;
            bPos        = varargin{ic}; ic=ic+1;
            qEl         = varargin{ic}; ic=ic+1;
            nAvogadro   = varargin{ic}; ic=ic+1;
            kBoltz      = varargin{ic}; %ic=ic+1;
            dE_dT       = getEquilibriumDer_dE_dT(TTrans,rho,y_s,dXs_dT,D_sl,Mm_s,bPos,bElectron,qEl,nAvogadro,kBoltz);
            kCharge_s   = getSpecies_kCharge(TTrans,y_s,Mm_s,bPos,bElectron,qEl,kBoltz);
            ksdEdT      = kCharge_s .* repmat(dE_dT,[1,N_spec]);
            kldEdT_3D   = repmat(permute(ksdEdT,[1,3,2]),[1,N_spec,1]); % (eta,l) --> (eta,s,l)

        else
            kldEdT_3D   = zeros(N_eta,N_spec,N_spec);
        end

        % reshaping and repmatting
        dXl_dT = repmat(reshape(dXs_dT,[N_eta,1,N_spec]),[1,N_spec,1]);
        sumand_l = zeros(N_eta,N_spec); % allocating
        for l=1:N_spec
            sumand_l = sumand_l + D_sl(:,:,l).*(dXl_dT(:,:,l) - kldEdT_3D(:,:,l)); % summing over l
        end
        kappa_reac = (h_s.*rho_s.*sumand_l) * ones(N_spec,1); % summing over s
    otherwise
        error(['non-identified flag passed in first position ''',varargin{1},'''']);
end
