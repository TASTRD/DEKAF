function [savepath,savepath_pref] = getLTE_tablePath(mixture,X_e,elem_list,varargin)
% getLTE_tablePath returns the path to the file where the tables for the
% initial guess of equilibrium compositions are stored.
%
%   Examples:
%       (1)     mixture = 'air5mutation';
%               X_e = [0.21,0.79];
%               elem_list = {'O','N'};
%               N_T = 1000;
%               T_min = 5;
%               T_max = 30000;
%               N_p = 57;
%               p_min = 1;
%               p_max = 10e6;
%               getLTE_tablePath(mixture,X_e,elem_list,N_T,T_min,T_max, ...
%                                                           N_p,p_min,p_max)
%
%         'LTETables_air5_N79O21_1000T5-30000_57p1-10000000.mat'
%
%       (2)     getLTE_tablePath(mixture,XO,XN)
%           |       'LTETables_air5mutation_O21N79_'
%           |--> returns only the initial part of the name (needed to
%           search any files with this composition and mixture.
%
%       (3)     [savepath,savepath_pref] = getLTE_tablePath(...)
%           |--> returns also the prefix of the savepath
%           'LTETables_air5mutation_' for the previous examples
%
%       (4)     [...] = getLTE_tablePath(...,'extraName','blablabla')
%           |--> appends the extra name 'blablabla' to the file name
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% Checking inputs
[varargin,~,idx_flag] = find_flagAndRemove('extraName',varargin);     % looking for the extraName flag
if ~isempty(idx_flag)                                       % if the flag appears
    extraName = varargin{idx_flag};                             % read what follows (the extra name)
    varargin(idx_flag) = [];                                    % and clear it from varargin
else                                                        % otherwise
    extraName = '';                                             % no extra name
end

[X_e,idx_Xe] = sort(X_e,'descend'); % sorting in descending order
elem_list = elem_list(idx_Xe);

% grab abbreviated mixture name (mixture + number of species, not source)
[tokens,~] = regexp(mixture,'(^[a-zA-Z]+[0-9]+)','tokens','once');
mixture_abbrev = tokens{1}; % abbreviated mixture string

savepath_pref = ['LTETables_',mixture_abbrev,'_'];
savepath = savepath_pref;
for e = 1:length(elem_list)
    if X_e(e)>=1e-3
        X_e(e) = round(X_e(e)*1e3)/1e3;
        savepath = [savepath,elem_list{e},num2str(X_e(e)*100,3)];
    else
        warning(['Element ',elem_list{e},' had an elemental fraction less than 0.1%, so it was not included in the path name.']);
    end
end
savepath = [savepath,'_'];
if ~isempty(varargin)
    N_T = varargin{1};
    T_min = varargin{2};
    T_max = varargin{3};
    N_p = varargin{4};
    p_min = varargin{5};
    p_max = varargin{6};
    savepath = [savepath,num2str(N_T),'T',num2str(T_min),'-',num2str(T_max),...
    '_',num2str(N_p),'p',num2str(p_min),'-',num2str(p_max),extraName,'.mat'];
end