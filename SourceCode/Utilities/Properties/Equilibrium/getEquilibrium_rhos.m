function rho_s = getEquilibrium_rhos(X_s,p,T,R_s)
% getEquilibrium_rhos computes the equilibrium species partial densities
%
%       X_s     -   size (N_eta x N_spec)
%       p       -   size (N_eta x 1)
%       T       -   size (N_eta x 1)
%       R_s     -   size (N_spec x 1)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% converting sizes
[N_eta,N_spec] = size(X_s);
p = repmat(p,[1,N_spec]);
T = repmat(T,[1,N_spec]);
R_s = repmat(R_s',[N_eta,1]);

rho_s = p.*X_s./(R_s.*T);       % computing partial densities
