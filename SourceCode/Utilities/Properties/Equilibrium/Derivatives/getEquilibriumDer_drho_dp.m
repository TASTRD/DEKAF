function drho_dp = getEquilibriumDer_drho_dp(dXs_dp,y_s,p,T,Mm_s,R0)
% getEquilibriumDer_drho_dp computes the derivative of density with respect
% to pressure in equilibrium.
%
% Usage:
%   (1)
%       drho_dp = getEquilibriumDer_drho_dp(dXs_dp,y_s,p,T,Mm_s,R0)
%
% Inputs:
%   dXs_dp      [1/Pa]          N_eta x N_spec
%       gradient of the equilibrium composition wrt pressure
%   y_s         [-]             N_eta x N_spec
%       equilibrium species mass fractions
%   p           [Pa]            N_eta x 1
%       pressure
%   T           [K]             N_eta x 1
%       temperature
%   Mm_s        [kg/mol]        N_spec x 1
%       species molar masses
%   R0          [J/kg-mol]      1 x 1
%       ideal gas constant
%
% Outputs:
%   drho_dp     [kg/J]          N_eta x 1
%       derivative of density wrt pressure in equilibrium.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);                             % number of points and species

Mm     = getMixture_Mm_R(y_s,[],[],Mm_s);               % mixture molar mass
X_s    = getSpecies_X(y_s,Mm,Mm_s);                     % mole fractions
dys_dp = getEquilibriumDer_dys_dq(X_s,dXs_dp,Mm_s);     % concentration derivative
Mm_s2D = repmat(Mm_s.',[N_eta,1]);                      % repmatting

sum_dysdpMms = (1./Mm_s2D.*dys_dp)*ones(N_spec,1);      % summing the various gradients of concentration weighed with their molar masses

drho_dp = Mm./(R0.*T) .* (1 - p.*Mm.*sum_dysdpMms);     % computing gradient

end % getEquilibriumDer_drho_dp