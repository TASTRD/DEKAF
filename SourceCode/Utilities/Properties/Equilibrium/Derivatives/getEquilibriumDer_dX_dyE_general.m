function [dXs_dyF] = getEquilibriumDer_dX_dyE_general(X_s,X_E,idx_F,mixCnst,varargin)
% getEquilibriumDer_dX_dyE_general computes the derivatives of the
% equilibrium composition with respect to the elemental mole fractions
% using a general algorithm valid for whatever mixture.
%
% Usage:
%   (1)
%       dXs_dyF = getEquilibriumDer_dX_dyE_general(X_s,X_E,idx_F,mixCnst)
%
%   (2)
%       ... = getEquilibriumDer_dX_dyE_general(...,'solveOnXs')
%       |--> solves the matrix system on dXs_dyF, rather than on dlnXs_dXF
%
% Inputs and outputs:
%   X_s         [-]
%       species mole fractions in equilibrium (N_eta x N_spec)
%   X_E         [-]
%       elemental atomic fraction (N_eta x N_el)
%   idx_F
%       index corresponding to the element with respect to which
%       derivatives are to be obtained
%   mixCnst
%       structure with the necessary mixture constants and properties
%   dXs_dyF     [-]
%       derivative of the equilibrium species mole fractions wrt the
%       elemental mass fraction X_F=X_E(idx_F) (N_eta x N_spec)
%
% See also: getEquilibriumDer_X_Map
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,bSolveOnXs] = find_flagAndRemove('solveOnXs',varargin);

% Obtaining sizes
[~,N_elem] = size(X_E);         % number of elements
[N_eta,N_spec] = size(X_s);     % number of species
N_reac = N_spec - N_elem;       % number of reactions

% Auxiliary fields
y_E = getEquilibrium_ys(X_E,mixCnst.Mm_E);
dXE_dyF = getSpeciesDer_dXs_dym(y_E,mixCnst.Mm_E); % (eta,F,E)

% Extract fields from mixCnst struct
delta_nuEq      = mixCnst.delta_nuEq;       % stoichiometry of equilibrium reactions    [N_reac x N_spec]
spec_list       = mixCnst.spec_list;        % list of species                           [N_spec x 1]
elemStoich_mat  = mixCnst.elemStoich_mat;   % elemental stoichiometric matrix           [N_el x N_spec]

% Obtain coefficients for the elemental conservation equations
[elConsCoeffEq,~,el_vec] = build_LTEElemCoeffs(elemStoich_mat,X_E,spec_list);   % (E,s,eta)
elemStoich_mat3D    = repmat(elemStoich_mat,        [1,1,N_eta]);               % (E,s) ------> (E,s,eta)
X_s3D_0             = repmat(permute(X_s,[3,2,1]),  [N_elem,1,1]);              % (eta,s) ----> (E,s,eta)
sumEXs              = sum(sum(elemStoich_mat3D.*X_s3D_0 , 1) , 2);              % (1,1,eta)
sumEXs_3D           = repmat(sumEXs , [N_elem-1,1,1]);                          % (1,1,eta) --> (E,1,eta)
dXE_dyF_3D          = permute(dXE_dyF(:,idx_F,el_vec),[3,2,1]);                 % (eta,F,E) --> (E,1,eta)

% Reshape inputs for 3D calculations
delta_nuEq_3D    = repmat(delta_nuEq.', [1,1,N_eta]);     % (s,r) ----> (r,s,eta)

b = [   zeros(N_reac,1,N_eta)   ;   % constitutive reactions (dissociation, ionization, etc.)
       -sumEXs_3D.*dXE_dyF_3D   ;   % elemental fraction conservation equations
        zeros(1,1,N_eta)        ;   % molar balance
     ]; % N_spec x 1 x N_eta

if bSolveOnXs                                                   % solving equation system on dXs_dXE
    X_s3D = repmat(permute(X_s,[3,2,1]),[N_reac,1,1]);                  % (eta,s) --> (r,s,eta)

    % Building the matrices composing the equation system A*dlnXdT=b
    A = [   delta_nuEq_3D./X_s3D    ;   % constitutive reactions (dissociation, ionization, etc.)
            elConsCoeffEq           ;   % elemental fraction conservation equations
            ones(1,N_spec,N_eta)    ;   % molar balance
        ]; % N_spec x N_spec x N_eta

    % Solving system at each location
    %%%% BETTER ALGORITHM GOES HERE
    for ii=N_eta:-1:1
        dXs_dyF(:,ii) = A(:,:,ii)\b(:,:,ii);
    end
    %%%%
    dXs_dyF = dXs_dyF.';
else                                                            % solving equation system on dlnXs_dXE
    X_s3D_1 = repmat(permute(X_s,[3,2,1]),[N_elem-1,1,1]);              % (eta,s) --> (E,s,eta)
    X_s3D_2 = permute(X_s,[3,2,1]);                                     % (eta,s) --> (1,s,eta)

    % Building the matrices composing the equation system A*dlnXdT=b
    A = [   delta_nuEq_3D               ;   % constitutive reactions (dissociation, ionization, etc.)
            elConsCoeffEq.*X_s3D_1      ;   % elemental fraction conservation equations
            X_s3D_2                     ;   % molar balance
        ]; % N_spec x N_spec x N_eta

    % Solving system at each location
    %%%% BETTER ALGORITHM GOES HERE
    for ii=N_eta:-1:1
        dlnXs_dyF(:,ii) = A(:,:,ii)\b(:,:,ii);
    end
    %%%%
    dXs_dyF = X_s .* dlnXs_dyF.';
end

end % getEquilibriumDer_dX_dyE_general