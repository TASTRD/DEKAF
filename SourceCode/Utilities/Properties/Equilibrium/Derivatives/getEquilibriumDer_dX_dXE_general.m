function [dXs_dXF] = getEquilibriumDer_dX_dXE_general(X_s,X_E,idx_F,mixCnst,varargin)
% getEquilibriumDer_dX_dXE_general computes the derivatives of the
% equilibrium composition with respect to the elemental mole fractions
% using a general algorithm valid for whatever mixture.
%
% Usage:
%   (1)
%       dXs_dXF = getEquilibriumDer_dX_dXE_general(X_s,X_E,idx_F,mixCnst)
%
%   (2)
%       ... = getEquilibriumDer_dX_dXE_general(...,'solveOnXs')
%       |--> solves the matrix system on dXs_dXF, rather than on dlnXs_dXF
%
% IMPORTANT! this function presumably gives a WRONG result. See issue #70
% on gitlab for a full disclosure of the problem. 
%    The correct approach to obtain both dXs_dyE and dXs_dXE is:
%       - compute dXs_dyE with getEquilibriumDer_dX_dyE_general
%       - compute dXs_dXE with getEquilibriumDer_dXs_dXE
%
% Inputs and outputs:
%   X_s         [-]
%       species mole fractions in equilibrium (N_eta x N_spec)
%   X_E         [-]
%       elemental atomic fraction (N_eta x N_el)
%   idx_F
%       index corresponding to the element with respect to which
%       derivatives are to be obtained
%   mixCnst
%       structure with the necessary mixture constants and properties
%   dXs_dXF     [-]
%       derivative of the equilibrium species mole fractions wrt the
%       elemental mole fraction X_F=X_E(idx_F) (N_eta x N_spec)
%
% See also: getEquilibriumDer_X_Map
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,bSolveOnXs] = find_flagAndRemove('solveOnXs',varargin);

% Obtaining sizes
[~,N_elem] = size(X_E);         % number of elements
[N_eta,N_spec] = size(X_s);     % number of species
N_reac = N_spec - N_elem;       % number of reactions

% Extract fields from mixCnst struct
delta_nuEq      = mixCnst.delta_nuEq;       % stoichiometry of equilibrium reactions    [N_reac x N_spec]
spec_list       = mixCnst.spec_list;        % list of species                           [N_spec x 1]
elemStoich_mat  = mixCnst.elemStoich_mat;   % elemental stoichiometric matrix           [N_el x N_spec]

% Obtain coefficients for the elemental conservation equations
[elConsCoeffEq,idxFinMat] = build_LTEElemCoeffs(elemStoich_mat,X_E,spec_list,'elID',idx_F); %  [N_el-1 x N_spec x N_eta]
elemStoich_mat3D    = repmat(elemStoich_mat,        [1,1,N_eta]);               % (el,s) --> (el,s,eta)
X_s3D_0             = repmat(permute(X_s,[3,2,1]),  [N_elem,1,1]);              % (eta,s) --> (el,s,eta)
sumEXs              = sum(sum(elemStoich_mat3D.*X_s3D_0 , 1) , 2);              % (1,1,eta)
deltaEF_sumEXs      = zeros(N_elem-1,1,N_eta);                                  % populating with zeros
deltaEF_sumEXs(idxFinMat,:,:) = sumEXs;                                         % evaluating Dirac delta function at E=F

% Reshape inputs for 3D calculations
delta_nuEq_3D    = repmat(delta_nuEq.', [1,1,N_eta]);     % (s,r) ----> (r,s,eta)

b = [   zeros(N_reac,1,N_eta)   ;   % constitutive reactions (dissociation, ionization, etc.)
       -deltaEF_sumEXs          ;   % elemental fraction conservation equations
        zeros(1,1,N_eta)        ;   % molar balance
     ]; % N_spec x 1 x N_eta

if bSolveOnXs                                                   % solving equation system on dXs_dXE
    X_s3D = repmat(permute(X_s,[3,2,1]),[N_reac,1,1]);                  % (eta,s) --> (r,s,eta)

    % Building the matrices composing the equation system A*dlnXdT=b
    A = [   delta_nuEq_3D./X_s3D    ;   % constitutive reactions (dissociation, ionization, etc.)
            elConsCoeffEq           ;   % elemental fraction conservation equations
            ones(1,N_spec,N_eta)    ;   % molar balance
        ]; % N_spec x N_spec x N_eta

    % Solving system at each location
    %%%% BETTER ALGORITHM GOES HERE
    for ii=N_eta:-1:1
        dXs_dXF(:,ii) = A(:,:,ii)\b(:,:,ii);
    end
    %%%%
    dXs_dXF = dXs_dXF.';
else                                                            % solving equation system on dlnXs_dXE
    X_s3D_1 = repmat(permute(X_s,[3,2,1]),[N_elem-1,1,1]);              % (eta,s) --> (el,s,eta)
    X_s3D_2 = permute(X_s,[3,2,1]);                                     % (eta,s) --> (1,s,eta)

    % Building the matrices composing the equation system A*dlnXdT=b
    A = [   delta_nuEq_3D               ;   % constitutive reactions (dissociation, ionization, etc.)
            elConsCoeffEq.*X_s3D_1      ;   % elemental fraction conservation equations
            X_s3D_2                     ;   % molar balance
        ]; % N_spec x N_spec x N_eta

    % Solving system at each location
    %%%% BETTER ALGORITHM GOES HERE
    for ii=N_eta:-1:1
        dlnXs_dXF(:,ii) = A(:,:,ii)\b(:,:,ii);
    end
    %%%%
    dXs_dXF = X_s .* dlnXs_dXF.';
end

end % getEquilibriumDer_dX_dXE_general