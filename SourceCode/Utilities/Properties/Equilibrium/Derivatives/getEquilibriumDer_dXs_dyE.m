function dXs_dyF = getEquilibriumDer_dXs_dyE(X_E,dXs_dXE,Mm_E)
% getEquilibriumDer_dXs_dyE computes the derivative of the equilibrium mole
% fraction with respect to the elemental mass fraction.
% 
% Usage:
%   (1)
%       dXs_dyE = getEquilibriumDer_dXs_dyE(X_E,dXs_dXE,Mm_E)
%
% IMPORTANT! This function computes dXs_dyE provided dXs_dXE. If these
% dXs_dXE derivatives were obtained with getEquilibriumDer_dX_dXE_general,
% they are WRONG! See issue #70 on gitlab for a full disclosure of the
% problem.
%    The correct approach to obtain both dXs_dyE and dXs_dXE is:
%       - compute dXs_dyE with getEquilibriumDer_dX_dyE_general
%       - compute dXs_dXE with getEquilibriumDer_dXs_dXE
%
% Inputs and outputs:
%   X_E             [-]         (N_eta x N_elem)
%       elemental mole fractions
%   dXs_dXE         [-]         (N_eta x N_spec x N_elem)
%       derivative of the equilibrium species mole fractions with respect to
%       the elemental mole fractions.
%   Mm_E            [kg/mol]    (N_elem x 1)
%       elemental molar mass
%   dXs_dyE         [-]         (N_eta x N_spec x N_elem)
%       derivative of the equilibrium mole fractions with respect to the
%       elemental mass fractions.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,N_spec,N_elem] = size(dXs_dXE);

y_E     = getEquilibrium_ys(X_E,Mm_E);      % (eta,E) NOTE that getEquilibrium_ys can also provide y_E from X_E and Mm_E
dXE_dyF = getSpeciesDer_dXs_dym(y_E,Mm_E);  % (eta,F,E)

dXE_dyF_4D = repmat(permute(dXE_dyF,[1,4,2,3]),[1,N_spec,1,1]); % (eta,F,E) --> (eta,s,F,E)
dXs_dXE_4D = repmat(permute(dXs_dXE,[1,2,4,3]),[1,1,N_elem,1]); % (eta,s,E) --> (eta,s,F,E)

dXs_dyF = sum(dXs_dXE_4D.*dXE_dyF_4D,4);

end % getEquilibriumDer_dXs_dyE