function [y_s,varargout] = getEquilibriumDer_dys_dall(X_s,Mm_s,varargin)
% getEquilibriumDer_dys_dall computes all equilibrium pressure and
% temperature derivatives of the species mass fractions.
%
%   Examples:
%       (1)     [y_s,dys_dT,dys_dp] = getEquilibriumDer_dys_dall(...
%                                               X_s,Mm_s,dXs_dT,dXs_dp);
%
%       (2)     [y_s,dys_dT,dys_dp,dys_dT2,dys_dpdT,dys_dp2] = ...
%               getEquilibriumDer_dys_dall(X_s,Mm_s,dXs_dT,dXs_dp, ...
%                                                   dXs_dT2,dXs_dpdT,dXs_dp2);
%
%       (3)     [y_s,dys_dT,dys_dp,dys_dT2,dys_dpdT,dys_dp2, ...
%               dys_dT3,dys_dpdT2,dys_dp2dT,dys_dp3] = ...
%               getEquilibriumDer_dys_dall(X_s,Mm_s,dXs_dT,dXs_dp, ...
%               dXs_dT2,dXs_dpdT,dXs_dp2,dXs_dT3,dXs_dpdT2,dXs_dp2dT,dXs_dp3);
%
%   Inputs:
%       - X_s           size (N_spec x N_eta)
%       - Mm_s          size (N_spec x 1)
%       - dXs_dT,etc.   size (N_spec x N_eta)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

switch length(varargin)
    case 2
        dXs_dT = varargin{1};
        dXs_dp = varargin{2};
        Oder = 1; % order of the derivatives
    case 5
        dXs_dT = varargin{1};
        dXs_dp = varargin{2};
        dXs_dT2 = varargin{3};
        dXs_dpdT = varargin{4};
        dXs_dp2 = varargin{5};
        Oder = 2; % order of the derivatives
    case 9
        dXs_dT = varargin{1};
        dXs_dp = varargin{2};
        dXs_dT2 = varargin{3};
        dXs_dpdT = varargin{4};
        dXs_dp2 = varargin{5};
        dXs_dT3 = varargin{6};
        dXs_dpdT2 = varargin{7};
        dXs_dp2dT = varargin{8};
        dXs_dp3 = varargin{9};
        Oder = 3; % order of the derivatives
    otherwise
        error('wrong number of inputs');
end

% reshaping and repmatting
[N_eta,N_spec] = size(X_s);
Mm_s = repmat(Mm_s',[N_eta,1]);

% computing MM (denominator of y_s) and its derivatives
MM          = repmat((X_s.*Mm_s)        *ones(N_spec,1),[1,N_spec]);

dMM_dT      = repmat((dXs_dT.*Mm_s)     *ones(N_spec,1),[1,N_spec]);
dMM_dp      = repmat((dXs_dp.*Mm_s)     *ones(N_spec,1),[1,N_spec]);

dMM_dT2     = repmat((dXs_dT2.*Mm_s)    *ones(N_spec,1),[1,N_spec]);
dMM_dpdT    = repmat((dXs_dpdT.*Mm_s)   *ones(N_spec,1),[1,N_spec]);
dMM_dp2     = repmat((dXs_dp2.*Mm_s)    *ones(N_spec,1),[1,N_spec]);

dMM_dT3     = repmat((dXs_dT3.*Mm_s)    *ones(N_spec,1),[1,N_spec]);
dMM_dpdT2   = repmat((dXs_dpdT2.*Mm_s)  *ones(N_spec,1),[1,N_spec]);
dMM_dp2dT   = repmat((dXs_dp2dT.*Mm_s)  *ones(N_spec,1),[1,N_spec]);
dMM_dp3     = repmat((dXs_dp3.*Mm_s)    *ones(N_spec,1),[1,N_spec]);

% Computing rho_s
y_s = X_s.*Mm_s./MM;

% Computing 1st-order derivatives
dys_dT = (Mm_s.*dXs_dT)./MM-(dMM_dT.*Mm_s.*X_s)./MM.^2;
dys_dp = (Mm_s.*dXs_dp)./MM-(dMM_dp.*Mm_s.*X_s)./MM.^2;
varargout{1} = dys_dT;
varargout{2} = dys_dp;

% Computing 2nd-order derivatives
if Oder>1
dys_dT2 = (Mm_s.*dXs_dT2)./MM-(2.*dMM_dT.*Mm_s.*dXs_dT)./MM.^2-(dMM_dT2.*Mm_s.*X_s)./MM.^2+(2.*(dMM_dT).^2.*Mm_s.*X_s)./MM.^3;
dys_dpdT = (-(dMM_dT.*Mm_s.*dXs_dp)./MM.^2)+(Mm_s.*dXs_dpdT)./MM-(dMM_dp.*Mm_s.*dXs_dT)./MM.^2+(2.*dMM_dT.*dMM_dp.*Mm_s.*X_s)./MM.^3-(dMM_dpdT.*Mm_s.*X_s)./MM.^2;
dys_dp2 = (Mm_s.*dXs_dp2)./MM-(2.*dMM_dp.*Mm_s.*dXs_dp)./MM.^2-(dMM_dp2.*Mm_s.*X_s)./MM.^2+(2.*(dMM_dp).^2.*Mm_s.*X_s)./MM.^3;
varargout{3} = dys_dT2;
varargout{4} = dys_dpdT;
varargout{5} = dys_dp2;
end

% Computing 3rd-order derivatives
if Oder>2
dys_dT3 = (Mm_s.*dXs_dT3)./MM-(3.*dMM_dT.*Mm_s.*dXs_dT2)./MM.^2-(3.*dMM_dT2.*Mm_s.*dXs_dT)./MM.^2+(6.*(dMM_dT).^2.*Mm_s.*dXs_dT)./MM.^3-(dMM_dT3.*Mm_s.*X_s)./MM.^2+(6.*dMM_dT.*dMM_dT2.*Mm_s.*X_s)./MM.^3-(6.*(dMM_dT).^3.*Mm_s.*X_s)./MM.^4;
dys_dpdT2 = (-(dMM_dT2.*Mm_s.*dXs_dp)./MM.^2)+(2.*(dMM_dT).^2.*Mm_s.*dXs_dp)./MM.^3+(Mm_s.*dXs_dpdT2)./MM-(dMM_dp.*Mm_s.*dXs_dT2)./MM.^2-(2.*dMM_dT.*Mm_s.*dXs_dpdT)./MM.^2+(4.*dMM_dT.*dMM_dp.*Mm_s.*dXs_dT)./MM.^3-(2.*dMM_dpdT.*Mm_s.*dXs_dT)./MM.^2+(2.*dMM_dT2.*dMM_dp.*Mm_s.*X_s)./MM.^3-(6.*(dMM_dT).^2.*dMM_dp.*Mm_s.*X_s)./MM.^4-(dMM_dpdT2.*Mm_s.*X_s)./MM.^2+(4.*dMM_dT.*dMM_dpdT.*Mm_s.*X_s)./MM.^3;
dys_dp2dT = (-(dMM_dT.*Mm_s.*dXs_dp2)./MM.^2)+(4.*dMM_dT.*dMM_dp.*Mm_s.*dXs_dp)./MM.^3-(2.*dMM_dpdT.*Mm_s.*dXs_dp)./MM.^2+(Mm_s.*dXs_dp2dT)./MM-(2.*dMM_dp.*Mm_s.*dXs_dpdT)./MM.^2-(dMM_dp2.*Mm_s.*dXs_dT)./MM.^2+(2.*(dMM_dp).^2.*Mm_s.*dXs_dT)./MM.^3+(2.*dMM_dT.*dMM_dp2.*Mm_s.*X_s)./MM.^3-(6.*dMM_dT.*(dMM_dp).^2.*Mm_s.*X_s)./MM.^4+(4.*dMM_dpdT.*dMM_dp.*Mm_s.*X_s)./MM.^3-(dMM_dp2dT.*Mm_s.*X_s)./MM.^2;
dys_dp3 = (Mm_s.*dXs_dp3)./MM-(3.*dMM_dp.*Mm_s.*dXs_dp2)./MM.^2-(3.*dMM_dp2.*Mm_s.*dXs_dp)./MM.^2+(6.*(dMM_dp).^2.*Mm_s.*dXs_dp)./MM.^3-(dMM_dp3.*Mm_s.*X_s)./MM.^2+(6.*dMM_dp.*dMM_dp2.*Mm_s.*X_s)./MM.^3-(6.*(dMM_dp).^3.*Mm_s.*X_s)./MM.^4;
varargout{6} = dys_dT3;
varargout{7} = dys_dpdT2;
varargout{8} = dys_dp2dT;
varargout{9} = dys_dp3;
end