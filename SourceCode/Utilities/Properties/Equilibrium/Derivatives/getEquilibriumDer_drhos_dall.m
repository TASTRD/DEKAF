function [rho_s,varargout] = getEquilibriumDer_drhos_dall(p,T,R_s,X_s,varargin)
% getEquilibriumDer_drhos_dall computes all equilibrium pressure and temperature
% derivatives of the species partial densities.
%
%   Examples:
%       (1)     [rho_s,drhos_dT,drhos_dp] = getEquilibriumDer_drhos_dall(...
%                                               p,T,R_s,X_s,dXs_dT,dXs_dp);
%
%       (2)     [rho_s,drhos_dT,drhos_dp,drhos_dT2,drhos_dpdT,drhos_dp2] = ...
%               getEquilibriumDer_drhos_dall(p,T,R_s,X_s,dXs_dT,dXs_dp, ...
%                                                   dXs_dT2,dXs_dpdT,dXs_dp2);
%
%       (3)     [rho_s,drhos_dT,drhos_dp,drhos_dT2,drhos_dpdT,drhos_dp2, ...
%               drhos_dT3,drhos_dpdT2,drhos_dp2dT,drhos_dp3] = ...
%               getEquilibriumDer_drhos_dall(p,T,R_s,X_s,dXs_dT,dXs_dp, ...
%               dXs_dT2,dXs_dpdT,dXs_dp2,dXs_dT3,dXs_dpdT2,dXs_dp2dT,dXs_dp3);
%
%   Inputs:
%       - p             size (N_eta x 1)
%       - T             size (N_eta x 1)
%       - R_s           size (N_spec x 1)
%       - X_s           size (N_spec x N_eta)
%       - dXs_dT,etc.   size (N_spec x N_eta)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

switch nargin
    case 6
        dXs_dT = varargin{1};
        dXs_dp = varargin{2};
        Oder = 1; % order of the derivatives
    case 9
        dXs_dT = varargin{1};
        dXs_dp = varargin{2};
        dXs_dT2 = varargin{3};
        dXs_dpdT = varargin{4};
        dXs_dp2 = varargin{5};
        Oder = 2; % order of the derivatives
    case 13
        dXs_dT = varargin{1};
        dXs_dp = varargin{2};
        dXs_dT2 = varargin{3};
        dXs_dpdT = varargin{4};
        dXs_dp2 = varargin{5};
        dXs_dT3 = varargin{6};
        dXs_dpdT2 = varargin{7};
        dXs_dp2dT = varargin{8};
        dXs_dp3 = varargin{9};
        Oder = 3; % order of the derivatives
    otherwise
        error('wrong number of inputs');
end

% reshaping and repmatting
[N_eta,N_spec] = size(X_s);
R_s = repmat(R_s',[N_eta,1]);
T = repmat(T,[1,N_spec]);
p = repmat(p,[1,N_spec]);

% Computing rho_s
rho_s = p.*X_s./(R_s.*T);

% Computing 1st-order derivatives
drhos_dp = X_s./(R_s.*T) + p./(R_s.*T) .* dXs_dp;
drhos_dT = -X_s.*p./(R_s.*T.^2) + p./(R_s.*T) .* dXs_dT;
varargout{1} = drhos_dT;
varargout{2} = drhos_dp;

% Computing 2nd-order derivatives
if Oder>1
drhos_dp2 = 2./(R_s.*T).*dXs_dp + p./(R_s.*T).*dXs_dp2;
drhos_dpdT = -X_s./(R_s.*T.^2) + 1./(R_s.*T).*dXs_dT - p./(R_s.*T.^2).*dXs_dp + p./(R_s.*T).*dXs_dpdT;
drhos_dT2 = 2*X_s.*p./(R_s.*T.^3) - 2*p./(R_s.*T.^2).*dXs_dT + p./(R_s.*T).*dXs_dT2;
varargout{3} = drhos_dT2;
varargout{4} = drhos_dpdT;
varargout{5} = drhos_dp2;
end

% Computing 3rd-order derivatives
if Oder>2
drhos_dp3 = 3./(R_s.*T).*dXs_dp2 + p./(R_s.*T).*dXs_dp3;
drhos_dp2dT = -2./(R_s.*T.^2).*dXs_dp + 2./(R_s.*T).*dXs_dpdT - p./(R_s.*T.^2).*dXs_dp2 + p./(R_s.*T).*dXs_dp2dT;
drhos_dpdT2 = 2*X_s./(R_s.*T.^3) + 2*p./(R_s.*T.^3).*dXs_dp - 2./(R_s.*T.^2).*dXs_dT - 2*p./(R_s.*T.^2).*dXs_dpdT + 1./(R_s.*T).*dXs_dT2 + p./(R_s.*T).*dXs_dpdT2;
drhos_dT3 = -6*X_s.*p./(R_s.*T.^4) + 6*p./(R_s.*T.^3).*dXs_dT - 3*p./(R_s.*T.^2).*dXs_dT2 + p./(R_s.*T).*dXs_dT3;
varargout{6} = drhos_dT3;
varargout{7} = drhos_dpdT2;
varargout{8} = drhos_dp2dT;
varargout{9} = drhos_dp3;
end