function drhos_dT = getEquilibriumDer_drhos_dT(X_s,dXs_dT,p,T,R_s)
% getEquilibriumDer_drhos_dT computes the first temperature derivative of
% the equilibrium species partial densities fractions.
%
%       X_s     -   size (N_eta x N_spec)
%       dXs_dT  -   size (N_eta x N_spec)
%       p       -   size (N_eta x 1)
%       T       -   size (N_eta x 1)
%       R_s     -   size (N_spec x 1)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% converting sizes
[N_eta,N_spec] = size(X_s);
p = repmat(p,[1,N_spec]);
T = repmat(T,[1,N_spec]);
R_s = repmat(R_s',[N_eta,1]);

drhos_dT = -X_s.*p./(R_s.*T.^2) + p./(R_s.*T).*dXs_dT;