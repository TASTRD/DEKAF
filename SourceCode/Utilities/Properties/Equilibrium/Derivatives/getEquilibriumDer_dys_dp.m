function dys_dp = getEquilibriumDer_dys_dp(X_s,dXs_dp,p,T,R_s)
% getEquilibriumDer_dys_dp computes the first pressure derivative of the
% equilibrium mass fractions.
%
%       X_s     -   size (N_eta x N_spec)
%       dXs_dp  -   size (N_eta x N_spec)
%       p       -   size (N_eta x 1)
%       T       -   size (N_eta x 1)
%       R_s     -   size (N_spec x 1)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% computing species densities and derivatives
rho_s = getEquilibrium_rhos(X_s,p,T,R_s);
drhos_dp = getEquilibriumDer_drhos_dp(X_s,dXs_dp,p,T,R_s);

% converting sizes
[~,N_spec] = size(X_s);

drho_dp = drhos_dp * ones(N_spec,1);    % mixture density derivative
drho_dp = repmat(drho_dp,[1,N_spec]);   % repmatting

rho = rho_s*ones(N_spec,1);         % summing all partial densities
rho = repmat(rho,[1,N_spec]);       % repeating over all species

dys_dp = 1./rho.*drhos_dp - rho_s./rho.^2 .* drho_dp;
