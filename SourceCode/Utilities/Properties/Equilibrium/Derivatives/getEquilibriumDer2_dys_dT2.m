function dys_dT2 = getEquilibriumDer2_dys_dT2(dXs_dT2,Mm_s,Mm)
%GETEQUILIBRIUMDER2_DYS_DT2 Calculate the second derivative of the mass
% fraction with respect to temperature in terms of the mole fraction
% gradient
%
% Inputs:
%   dXs_dT2     -   species mole fraction 2nd derivative with respect to
%                   temperature twice [1/K-K]
%                   Matrix of size (N_eta x N_spec)
%   Mm          -   mixture molecular molar mass [kg/mol]
%                   Vector of size (N_eta x 1)
%   Mm_s        -   species molecular molar mass [kg/mol]
%                   Matrix of size (N_spec x 1)
%
% Outputs:
%   dys_dT2     -   species mass fractions 2nd derivative with respect to
%                   temperature twice [1/K-K]
%                   Matrix of size (N_eta x N_spec)
%
% Notes:
%   This function is currently incorrect. Mm is assumed to be constant,
%   however this is false: mixture molar mass depends on mole fractions,
%   which are dependent on temperature.
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(dXs_dT2);
Mm_s2D  = repmat(Mm_s',[N_eta,1]);
Mm2D    = repmat(Mm,[1,N_spec]);
dys_dT2 = dXs_dT2 .* Mm_s2D ./ Mm2D;

end % getEquilibriumDer2_dys_dT2
