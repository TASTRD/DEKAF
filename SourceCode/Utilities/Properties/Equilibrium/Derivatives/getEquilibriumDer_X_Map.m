function varargout = getEquilibriumDer_X_Map(Oder,X_s,p,X_E,mixture,mixCnst,varargin)
% getEquilibriumDer_X_Map computes the derivatives of the equilibrium mole
% fractions with respect to temperature and pressure, mapping the program
% to the correct mixture derivative calculations.
%
%  Usage:
%    (1)
%       [dXs_dT,dXs_dp] = getEquilibriumDer_X_Map(1,X_s,p,X_E,mixture,mixCnst,dlnKp_dT)
%       |--> employs a general algorithm -- see <a href="matlab:help getEquilibriumDer_dX_dT_general">getEquilibriumDer_dX_dT_general</a>
%
%    
%    (2)
%       [dXs_dT,dXs_dp,dXs_dyF] = getEquilibriumDer_X_Map(...,'computeXFgrads')
%       |--> computes also the gradients wrt the elemental mole fractions
%
%    (3)
%       [dXs_dT,dXs_dp,dXs_dT2,dXs_dpdT,dXs_dp2] = ...
%                   getEquilibriumDer_X_Map(2,X_s,p,X_E,mixture,mixCnst,dlnKp_dT,dlnKp_dT2)
%       |--> employs analytical expressions for the available mixtures (see
%       below)
%
%    (4)
%       [dXs_dT,dXs_dp,dXs_dT2,dXs_dpdT,dXs_dp2,dXs_dT3,dXs_dpdT2,dXs_dp2dT,dXs_dp3] = ...
%                   getEquilibriumDer_X_Map(3,X_s,p,X_E,mixture,mixCnst,dlnKp_dT,dlnKp_dT2,dlnKp_dT3)
%       |--> employs analytical expressions for the available mixtures (see
%       below)
%
% The user must specify the maximum order of the derivatives to be computed
% through derOrder:
%       derOrder = 1    -   dXs_dT & dXs_dp
%       derOrder = 2    -   dXs_dT2, dXs_dpdT & dXs_dp2 (also)
%       derOrder = 3    -   dXs_dT3, dXs_dpdT2, dXs_dp2dT & dXs_dp3 (also)
%
% Inputs:
%   X_s         [-]             (N_eta x N_spec)
%       mole fractions
%   p           [Pa]            (N_eta x 1)
%       pressure
%   X_E         [-]             (N_eta x N_elem)
%       elemental fractions
%   mixture
%       string identifying the mixture. Supported inputs for analytical
%       derivatives are: 
%           'oxygen2*'
%           'air5*'
%           'air11*'
%        numerical derivatives of dXs_dT, dXs_dp and dXs_dXF are available
%        for all mixtures
%   mixCnst
%       structure with the necessary mixture constants and properties
%   dlnKp_dT    [log(Pa)/K]     (N_eta x 3)
%       temperature gradient of the natural logarithms of the equilibrium
%       constants
%   dlnKp_dT2   [log(Pa)/K^2]   (N_eta x 3)
%       second temperature gradient of the natural logarithms of the
%       equilibrium constants 
%   dlnKp_dT3   [log(Pa)/K^3]   (N_eta x 3)
%       third temperature gradient of the natural logarithms of the
%       equilibrium constants
%
% Outputs:
%   dXs_dT      [1/K]           (N_eta x N_spec)
%       equilibrium composition gradient wrt temperature
%   dXs_dp      [1/Pa]          (N_eta x N_spec)
%       equilibrium composition gradient wrt pressure
%   dXs_dyF     [-]             (N_eta x N_spec x N_elem)
%       equilibrium composition gradient wrt each elemental mass fraction
%
% See also: getEquilibriumDer_X_air5, getEquilibriumDer_X_air11
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[varargin,bXF] = find_flagAndRemove('computeXFgrads',varargin);

if nargout<=2 || (nargout==3 && bXF)        % only we compute analytical derivatives
    ic=1;
    if nargout>=ic                              % dXs_dT
        varargout{ic} = getEquilibriumDer_dX_dT_general(X_s,X_E,varargin{1},mixCnst);
    end
    ic=ic+1;
    if nargout>=ic                              % dXs_dp
        varargout{ic} = getEquilibriumDer_dX_dp_general(X_s,X_E,p,mixCnst);
    end
    ic=ic+1;
    if nargout>=ic                              % dXs_dyF
        N_elem = size(X_E,2);
        for iF=N_elem:-1:1
%             varargout{ic}(:,:,iF) = getEquilibriumDer_dX_dXE_general(X_s,X_E,iF,mixCnst);
            varargout{ic}(:,:,iF) = getEquilibriumDer_dX_dyE_general(X_s,X_E,iF,mixCnst);
        end
    end
else                                        % we require higher-order derivatives, so we must use the analytical functions
    mixtureCut = regexp(mixture,'^[a-zA-Z]+\d+','match');
    mixtureCut = mixtureCut{1};
    spec_list    = mixCnst.spec_list;
    reac_list_eq = mixCnst.reac_list_eq;
    X_EMean = repmat(mean(X_E,1),[size(X_E,1),1]);
    if max(abs(X_E(:)-X_EMean(:)))>eps*10*size(X_E,1) % the deviation of some of the X_E from the average is unacceptable
        error('The analytical functions for the thermodynamic derivatives of the equilibrium compositions only support zero elemental demixing (X_E(eta)=cst)');
    end
    switch mixtureCut
        case 'oxygen2'
            [varargout{1:nargout}] = getEquilibriumDer_X_oxygen2(Oder,X_s,p,spec_list,varargin{:}); % computing derivatives of mole fractions
        case 'air5'
            XEq_old = X_E(1,strcmp(mixCnst.elem_list,'O')) / X_E(1,strcmp(mixCnst.elem_list,'N')); % used for the old equation arrangement
            [varargout{1:nargout}] = getEquilibriumDer_X_air5( Oder,X_s,p,XEq_old,spec_list,reac_list_eq,varargin{:}); % computing derivatives of mole fractions
        case 'air11'
            XEq_old = X_E(1,strcmp(mixCnst.elem_list,'O')) / X_E(1,strcmp(mixCnst.elem_list,'N'));
            [varargout{1:nargout}] = getEquilibriumDer_X_air11(Oder,X_s,p,XEq_old,spec_list,reac_list_eq,varargin{:}); % computing derivatives of mole fractions
        otherwise
            error(['There are no analytical functions computing the high-order equilibrium thermodynamic derivatives of the chosen mixture ''',mixture,'''. ',...
                'Somebody is going to have to get his hands dirty and develop a function like getEquilibriumDer_X_air5 or ',...
                'getEquilibriumDer_X_air11 but for this mixture. Look at SourceCode/Derivation/LTE for useful tools.',newline,...
                ' Alternatively, one can also develop general functions like getEquilibriumDer_dX_dT_general but for higher-order derivatives.']);
    end
end