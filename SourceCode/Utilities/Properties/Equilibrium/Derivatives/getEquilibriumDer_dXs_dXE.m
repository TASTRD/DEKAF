function dXs_dXF = getEquilibriumDer_dXs_dXE(X_E,dXs_dyE,Mm_E)
% getEquilibriumDer_dXs_dXE computes the derivative of the equilibrium mole
% fraction with respect to the elemental mass fraction.
% 
% Usage:
%   (1)
%       dXs_dXF = getEquilibriumDer_dXs_dXE(X_E,dXs_dyE,Mm_E)
%
% Inputs and outputs:
%   X_E             [-]         (N_eta x N_elem)
%       elemental mole fractions
%   dXs_dyE         [-]         (N_eta x N_spec x N_elem)
%       derivative of the equilibrium mole fractions with respect to the
%       elemental mass fractions.
%   Mm_E            [kg/mol]    (N_elem x 1)
%       elemental molar mass
%   dXs_dXE         [-]         (N_eta x N_spec x N_elem)
%       derivative of the equilibrium species mole fractions with respect to
%       the elemental mole fractions.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,N_spec,N_elem] = size(dXs_dyE);

y_E     = getEquilibrium_ys(X_E,Mm_E);      % (eta,E) NOTE that getEquilibrium_ys can also provide y_E from X_E and Mm_E
dyE_dXF = getSpeciesDer_dys_dXm(y_E,Mm_E);  % (eta,F,E)

dyE_dXF_4D = repmat(permute(dyE_dXF,[1,4,2,3]),[1,N_spec,1,1]); % (eta,F,E) --> (eta,s,F,E)
dXs_dyE_4D = repmat(permute(dXs_dyE,[1,2,4,3]),[1,1,N_elem,1]); % (eta,s,E) --> (eta,s,F,E)

dXs_dXF = sum(dXs_dyE_4D.*dyE_dXF_4D,4);

end % getEquilibriumDer_dXs_dXE