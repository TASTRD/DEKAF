function varargout = getEquilibriumDer_X(T,X_s,p,X_E,mixCnst,mixture,Oder,options,varargin)
%GETEQUILIBRIUMDER_X Calculate the derivatives of the mole fraction to a
% specified order of differentiation
%
% Usage:
%   (1)
%       [dXs_dT,dXs_dp] = ...
%           getEquilibriumDer_X(T,X_s,p,X_E,mixCnst,mixture,1,options)
%       |--> computes first order derivatives with T and p of the
%       mole fractions
%
%   (2)
%       [dXs_dT,dXs_dp,dXs_dXF] = getEquilibriumDer_X(...,'computeXFgrads')
%       |--> computes first order derivatives also wrt the elemental mole
%       fractions
%
%   (3)
%       [...,dXs_dT,dXs_dp,dXs_dT2,dXs_dpdT,dXs_dp2] = ...
%           getEquilibriumDer_X(T,X_s,p,X_E,mixCnst,mixture,2,options)
%       |--> computes also second order derivatives with T and p of the
%       mole fractions
%
%   (4)
%       [...,dXs_dT,dXs_dp,dXs_dT2,dXs_dpdT,dXs_dp2,dXs_dT3,dXs_dpdT2,dXs_dp2dT,dXs_dp3] = ...
%           getEquilibriumDer_X(T,X_s,p,X_E,mixCnst,mixture,3,options)
%       |--> computes also third order derivatives with T and p of the
%       mole fractions
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
%
% GNU Lesser General Public License 3.0

% computing derivatives of the equilibrium constants, required to compute
% the derivatives of the mole fractions
if Oder>0 % preparing inputs to compute first order derivatives
    % N_out variable is redundant -- it is dependent on nargout. However,
    % we use it here to reduce obscurity of the code (it's gnarly enough as
    % it is).
    y_s = getEquilibrium_ys(X_s,mixCnst.Mm_s);                                  % computing mass fraction from mole fractions
    dlnKeqr_dT  = getReactionDer_dlnKeqr_dT(y_s,T,mixCnst.delta_nuEq,mixCnst.Keq_struc,options); % computing temperature gradient of the equilibrium constant
    dlnKpeq_dT  = getEquilibriumDer_dlnKpeq_dT(dlnKeqr_dT,T,sum(mixCnst.delta_nuEq,1)');    % converting from dlnKeq_dT to dlnKpeq_dT
    list_dlnKpeq = {dlnKpeq_dT}; % cell to be inputted to getEquilibrium_X_air5
end
if Oder>1 % preparing inputs to compute also second derivatives
    dlnKeqr_dT2  = getReactionDer2_dlnKeqr_dT2(T,mixCnst.delta_nuEq,mixCnst.Keq_struc,options); % computing second temperature gradient of the equilibrium constant
    dlnKpeq_dT2  = getEquilibriumDer2_dlnKpeq_dT2(dlnKeqr_dT2,T,sum(mixCnst.delta_nuEq,1)');    % converting from dlnKeq_dT2 to dlnKpeq_dT2
    list_dlnKpeq = {dlnKpeq_dT,dlnKpeq_dT2}; % cell to be inputted to getEquilibrium_X_air5
end
if Oder>2 % preparing inputs to compute also third derivatives
    dlnKeqr_dT3  = getReactionDer3_dlnKeqr_dT3(T,mixCnst.delta_nuEq,mixCnst.Keq_struc,options); % computing third temperature gradient of the equilibrium constant
    dlnKpeq_dT3  = getEquilibriumDer3_dlnKpeq_dT3(dlnKeqr_dT3,T,sum(mixCnst.delta_nuEq,1)');    % converting from dlnKeq_dT3 to dlnKpeq_dT3
    list_dlnKpeq = {dlnKpeq_dT,dlnKpeq_dT2,dlnKpeq_dT3}; % cell to be inputted to getEquilibrium_X_air5
end

% computing derivatives of the mole fractions (if necessary)
if Oder>0
[varargout{1:nargout}] = getEquilibriumDer_X_Map(Oder,X_s,p,X_E,mixture,mixCnst,list_dlnKpeq{:},varargin{:}); % computing derivatives of mole fractions
end

end