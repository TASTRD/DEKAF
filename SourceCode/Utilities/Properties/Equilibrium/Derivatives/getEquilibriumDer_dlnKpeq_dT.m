function dlnKpeqr_dT = getEquilibriumDer_dlnKpeq_dT(dlnKeqr_dT,T,delta_nu)
% getEquilibriumDer_dlnKpeq_dT computes the temperature derivative of the
% natural logs of the equilibrium p constants.
%
%   Examples:
%      (1) dlnKpeqr_dT = getEquilibriumDer_dlnKpeq_dT(dlnKeqr_dT,T,delta_nu)
%
%   Inputs:
%      dlnKeqr_dT   size (N_eta x N_reac)
%      T            size (N_eta x 1)
%      delta_nu     size (N_reac x 1)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_eta,N_reac] = size(dlnKeqr_dT);
T = repmat(T,[1,N_reac]);
delta_nu = repmat(delta_nu',[N_eta,1]);

dlnKpeqr_dT = dlnKeqr_dT + delta_nu./T;