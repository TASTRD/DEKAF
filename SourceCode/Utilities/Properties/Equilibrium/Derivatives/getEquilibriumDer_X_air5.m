function varargout = getEquilibriumDer_X_air5(derOrder,X_s,p,X,spec_list,reac_list,varargin)
% getEquilibriumDer_X_air5 computes the derivatives of the equilibrium mole
% fractions with pressure and temperature for an air5 mixture.
%
%  Examples:
%    (1)    [dXs_dT,dXs_dp] = getEquilibriumDer_X_air5(1,X_s,p,X,spec_list,...
%                                                           reac_list,dlnKp_dT)
%    (2)    [dXs_dT,dXs_dp,dXs_dT2,dXs_dpdT,dXs_dp2] = ...
%                   getEquilibriumDer_X_air5(2,X_s,p,X,spec_list,reac_list,...
%                                                           dlnKp_dT,dlnKp_dT2)
%    (3)    [dXs_dT,dXs_dp,dXs_dT2,dXs_dpdT,dXs_dp2,dXs_dT3,dXs_dpdT2, ...
%            dXs_dp2dT,dXs_dp3] = getEquilibriumDer_X_air5(3,X_s,p,X, ...
%                               spec_list,reac_list,dlnKp_dT,dlnKp_dT2,dlnKp_dT3)
%
% The user must specify the maximum order of the derivatives to be computed
% through derOrder:
%       derOrder = 1    -   dXs_dT & dXs_dp
%       derOrder = 2    -   dXs_dT2, dXs_dpdT & dXs_dp2 (also)
%       derOrder = 3    -   dXs_dT3, dXs_dpdT2, dXs_dp2dT & dXs_dp3 (also)
%
% The other required inputs are:
%       X_s         -   mole fractions                  [-]  size (N x 5)
%       p           -   pressure                        [Pa] size (N x 1)
%       X           -   ratio of O-N atomic fractions   [-]  size (1 x 1)
%       spec_list   -   list of species                 [-]  size (5 x 1)
%       reac_list   -   list of equilibrium reactions   [-]  size (3 x 1)
%       dlnKp_dT    -   temperature gradient of the natural logarithms of
%                       the equilibrium constants       [Pa] size (N x 3)
%       dlnKp_dT2   -   second temperature gradient of the natural logarithms
%                       of the equilibrium constants    [Pa] size (N x 3)
%       dlnKp_dT3   -   third temperature gradient of the natural logarithms
%                       of the equilibrium constants    [Pa] size (N x 3)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

%% Extracting inputs
XN = X_s(:,strcmp(spec_list,'N'));
XO = X_s(:,strcmp(spec_list,'O'));
XNO = X_s(:,strcmp(spec_list,'NO'));
XN2 = X_s(:,strcmp(spec_list,'N2'));
XO2 = X_s(:,strcmp(spec_list,'O2'));

pos_NDiss  = ~cellfun(@isempty,regexp(reac_list,'N2')); % identifying the position of the N dissociation reaction
pos_ODiss  = ~cellfun(@isempty,regexp(reac_list,'O2')); % identifying the position of the O dissociation reaction
pos_NODiss = ~cellfun(@isempty,regexp(reac_list,'NO')); % identifying the position of the NO dissociation reaction
switch derOrder
    case 1
        dlnKN_dT    = varargin{1}(:,pos_NDiss);
        dlnKO_dT    = varargin{1}(:,pos_ODiss);
        dlnKNO_dT   = varargin{1}(:,pos_NODiss);
    case 2
        dlnKN_dT    = varargin{1}(:,pos_NDiss);
        dlnKO_dT    = varargin{1}(:,pos_ODiss);
        dlnKNO_dT   = varargin{1}(:,pos_NODiss);

        dlnKN_dT2   = varargin{2}(:,pos_NDiss);
        dlnKO_dT2   = varargin{2}(:,pos_ODiss);
        dlnKNO_dT2  = varargin{2}(:,pos_NODiss);
    case 3
        dlnKN_dT    = varargin{1}(:,pos_NDiss);
        dlnKO_dT    = varargin{1}(:,pos_ODiss);
        dlnKNO_dT   = varargin{1}(:,pos_NODiss);

        dlnKN_dT2   = varargin{2}(:,pos_NDiss);
        dlnKO_dT2   = varargin{2}(:,pos_ODiss);
        dlnKNO_dT2  = varargin{2}(:,pos_NODiss);

        dlnKN_dT3   = varargin{3}(:,pos_NDiss);
        dlnKO_dT3   = varargin{3}(:,pos_ODiss);
        dlnKNO_dT3  = varargin{3}(:,pos_NODiss);
    otherwise
        error('wrong input derOrder');
end

%% Computing derivatives (expressions obtained from maxima using derivatives.wxm)

if derOrder>0
    % Pressure 1st-order derivatives ;

    dlnXN_dp = ((XO+(-X-1).*XNO+(-4.*X-4).*XN2).*XO2+((-2.*X-1).*XN2-X.*XNO).*XO+(-X-1).*XN2.*XNO)./(p.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dlnXO_dp = -(((X+1).*XNO+(4.*X+4).*XN2+(X+2).*XN).*XO2+((X+1).*XN2+XN).*XNO-X.*XN.*XN2)./(p.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dlnXNO_dp = ((XO+(X+2).*XN).*XO2+((2.*X+1).*XN2+(X+1).*XN).*XO+X.*XN.*XN2)./(p.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dlnXN2_dp = ((2.*XO+(2.*X+4).*XN).*XO2+((X+1).*XN-X.*XNO).*XO+XN.*XNO)./(p.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dlnXO2_dp = ((X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO-XN.*XNO+2.*X.*XN.*XN2)./(p.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dXN_dp = dlnXN_dp.*XN ;

    dXO_dp = dlnXO_dp.*XO ;

    dXNO_dp = dlnXNO_dp.*XNO ;

    dXN2_dp = dlnXN2_dp.*XN2 ;

    dXO2_dp = dlnXO2_dp.*XO2 ;

    % Temperature 1st-order derivatives ;

    dlnXN_dT = -(dlnKNO_dT.*((-2.*X-2).*XNO.*XO2-X.*XNO.*XO)+dlnKN_dT.*((-4.*X-4).*XN2.*XO2+(-2.*X-1).*XN2.*XO+(-X-1).*XN2.*XNO)+dlnKO_dT.*(XO+(X+1).*XNO).*XO2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXO_dT = (dlnKO_dT.*((X+1).*XNO+(4.*X+4).*XN2+(X+2).*XN).*XO2+dlnKN_dT.*((-X-1).*XN2.*XNO-X.*XN.*XN2)+dlnKNO_dT.*((2.*X+2).*XN2+XN).*XNO)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXNO_dT = -(dlnKNO_dT.*(((8.*X+8).*XN2+(2.*X+4).*XN).*XO2+((4.*X+2).*XN2+(X+1).*XN).*XO)+dlnKN_dT.*((-4.*X-4).*XN2.*XO2+(-2.*X-1).*XN2.*XO+X.*XN.*XN2)+dlnKO_dT.*(XO+(-4.*X-4).*XN2+(-X-2).*XN).*XO2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXN2_dT = -(dlnKN_dT.*(((2.*X+2).*XNO+(2.*X+4).*XN).*XO2+(X.*XNO+(X+1).*XN).*XO+XN.*XNO)+dlnKNO_dT.*((-4.*X-4).*XNO.*XO2-2.*X.*XNO.*XO)+dlnKO_dT.*(2.*XO+(2.*X+2).*XNO).*XO2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXO2_dT = -(dlnKO_dT.*((X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)+dlnKN_dT.*((2.*X+2).*XN2.*XNO+2.*X.*XN.*XN2)+dlnKNO_dT.*((-4.*X-4).*XN2-2.*XN).*XNO)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dXN_dT = dlnXN_dT.*XN ;

    dXO_dT = dlnXO_dT.*XO ;

    dXNO_dT = dlnXNO_dT.*XNO ;

    dXN2_dT = dlnXN2_dT.*XN2 ;

    dXO2_dT = dlnXO2_dT.*XO2 ;
end

if derOrder>1
    % Pressure 2nd-order derivatives ;

    dlnXN_dp2 = -(p.^2.*(((2.*dlnXO_dp.^2-dlnXO2_dp.^2).*XO+(dlnXNO_dp.^2.*(2.*X+2)+dlnXO2_dp.^2.*(-X-1)).*XNO+dlnXN2_dp.^2.*(4.*X+4).*XN2+dlnXN_dp.^2.*(2.*X+4).*XN).*XO2+((dlnXNO_dp.^2.*X-dlnXO_dp.^2.*X).*XNO+dlnXN2_dp.^2.*(2.*X+1).*XN2+dlnXN_dp.^2.*(X+1).*XN).*XO+(dlnXN2_dp.^2.*(X+1).*XN2+dlnXN_dp.^2.*XN).*XNO)+(XO+(-X-1).*XNO+(-4.*X-4).*XN2).*XO2+((-2.*X-1).*XN2-X.*XNO).*XO+(-X-1).*XN2.*XNO)./(p.^2.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dlnXO_dp2 = -(p.^2.*((dlnXO2_dp.^2.*(X+1).*XNO+dlnXO2_dp.^2.*(4.*X+4).*XN2+dlnXO2_dp.^2.*(X+2).*XN).*XO2+(dlnXO_dp.^2.*X.*XNO+dlnXO_dp.^2.*(4.*X+2).*XN2+dlnXO_dp.^2.*(X+1).*XN).*XO+((dlnXNO_dp.^2.*(2.*X+2)+dlnXN2_dp.^2.*(-X-1)).*XN2+(dlnXNO_dp.^2-dlnXN_dp.^2).*XN).*XNO+(2.*dlnXN_dp.^2.*X-dlnXN2_dp.^2.*X).*XN.*XN2)+((-X-1).*XNO+(-4.*X-4).*XN2+(-X-2).*XN).*XO2+((-X-1).*XN2-XN).*XNO+X.*XN.*XN2)./(p.^2.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dlnXNO_dp2 = -(p.^2.*(((2.*dlnXO_dp.^2-dlnXO2_dp.^2).*XO+dlnXNO_dp.^2.*(2.*X+2).*XNO+(dlnXO2_dp.^2.*(4.*X+4)+dlnXN2_dp.^2.*(4.*X+4)).*XN2+(dlnXN_dp.^2.*(2.*X+4)+dlnXO2_dp.^2.*(X+2)).*XN).*XO2+(dlnXNO_dp.^2.*X.*XNO+(dlnXO_dp.^2.*(4.*X+2)+dlnXN2_dp.^2.*(2.*X+1)).*XN2+(dlnXO_dp.^2.*(X+1)+dlnXN_dp.^2.*(X+1)).*XN).*XO+(dlnXNO_dp.^2.*(2.*X+2).*XN2+dlnXNO_dp.^2.*XN).*XNO+(2.*dlnXN_dp.^2.*X-dlnXN2_dp.^2.*X).*XN.*XN2)+(XO+(X+2).*XN).*XO2+((2.*X+1).*XN2+(X+1).*XN).*XO+X.*XN.*XN2)./(p.^2.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dlnXN2_dp2 = -(p.^2.*(((4.*dlnXO_dp.^2-2.*dlnXO2_dp.^2).*XO+(dlnXNO_dp.^2.*(4.*X+4)+dlnXO2_dp.^2.*(-2.*X-2)).*XNO+dlnXN2_dp.^2.*(8.*X+8).*XN2+dlnXN_dp.^2.*(4.*X+8).*XN).*XO2+((2.*dlnXNO_dp.^2.*X-2.*dlnXO_dp.^2.*X).*XNO+dlnXN2_dp.^2.*(4.*X+2).*XN2+dlnXN_dp.^2.*(2.*X+2).*XN).*XO+(dlnXN2_dp.^2.*(2.*X+2).*XN2+2.*dlnXN_dp.^2.*XN).*XNO)+(2.*XO+(2.*X+4).*XN).*XO2+((X+1).*XN-X.*XNO).*XO+XN.*XNO)./(p.^2.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dlnXO2_dp2 = -(p.^2.*((dlnXO2_dp.^2.*(2.*X+2).*XNO+dlnXO2_dp.^2.*(8.*X+8).*XN2+dlnXO2_dp.^2.*(2.*X+4).*XN).*XO2+(2.*dlnXO_dp.^2.*X.*XNO+dlnXO_dp.^2.*(8.*X+4).*XN2+dlnXO_dp.^2.*(2.*X+2).*XN).*XO+((dlnXNO_dp.^2.*(4.*X+4)+dlnXN2_dp.^2.*(-2.*X-2)).*XN2+(2.*dlnXNO_dp.^2-2.*dlnXN_dp.^2).*XN).*XNO+(4.*dlnXN_dp.^2.*X-2.*dlnXN2_dp.^2.*X).*XN.*XN2)+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO-XN.*XNO+2.*X.*XN.*XN2)./(p.^2.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dXN_dp2 = dlnXN_dp2.*XN+dXN_dp.^2./XN ;

    dXO_dp2 = dlnXO_dp2.*XO+dXO_dp.^2./XO ;

    dXNO_dp2 = dlnXNO_dp2.*XNO+dXNO_dp.^2./XNO ;

    dXN2_dp2 = dlnXN2_dp2.*XN2+dXN2_dp.^2./XN2 ;

    dXO2_dp2 = dlnXO2_dp2.*XO2+dXO2_dp.^2./XO2 ;

    % Temperature 2nd-order derivatives ;

    dlnXN_dT2 = -(dlnKNO_dT2.*((-2.*X-2).*XNO.*XO2-X.*XNO.*XO)+dlnKN_dT2.*((-4.*X-4).*XN2.*XO2+(-2.*X-1).*XN2.*XO+(-X-1).*XN2.*XNO)+((2.*dlnXO_dT.^2-dlnXO2_dT.^2).*XO+(dlnXNO_dT.^2.*(2.*X+2)+dlnXO2_dT.^2.*(-X-1)).*XNO+dlnXN2_dT.^2.*(4.*X+4).*XN2+dlnXN_dT.^2.*(2.*X+4).*XN).*XO2+dlnKO_dT2.*(XO+(X+1).*XNO).*XO2+((dlnXNO_dT.^2.*X-dlnXO_dT.^2.*X).*XNO+dlnXN2_dT.^2.*(2.*X+1).*XN2+dlnXN_dT.^2.*(X+1).*XN).*XO+(dlnXN2_dT.^2.*(X+1).*XN2+dlnXN_dT.^2.*XN).*XNO)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXO_dT2 = (dlnKO_dT2.*((X+1).*XNO+(4.*X+4).*XN2+(X+2).*XN).*XO2+(dlnXO2_dT.^2.*(-X-1).*XNO+dlnXO2_dT.^2.*(-4.*X-4).*XN2+dlnXO2_dT.^2.*(-X-2).*XN).*XO2+(-dlnXO_dT.^2.*X.*XNO+dlnXO_dT.^2.*(-4.*X-2).*XN2+dlnXO_dT.^2.*(-X-1).*XN).*XO+dlnKN_dT2.*((-X-1).*XN2.*XNO-X.*XN.*XN2)+((dlnXN2_dT.^2.*(X+1)+dlnXNO_dT.^2.*(-2.*X-2)).*XN2+(dlnXN_dT.^2-dlnXNO_dT.^2).*XN).*XNO+dlnKNO_dT2.*((2.*X+2).*XN2+XN).*XNO+(dlnXN2_dT.^2.*X-2.*dlnXN_dT.^2.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXNO_dT2 = -(dlnKNO_dT2.*(((8.*X+8).*XN2+(2.*X+4).*XN).*XO2+((4.*X+2).*XN2+(X+1).*XN).*XO)+dlnKN_dT2.*((-4.*X-4).*XN2.*XO2+(-2.*X-1).*XN2.*XO+X.*XN.*XN2)+((2.*dlnXO_dT.^2-dlnXO2_dT.^2).*XO+dlnXNO_dT.^2.*(2.*X+2).*XNO+(dlnXO2_dT.^2.*(4.*X+4)+dlnXN2_dT.^2.*(4.*X+4)).*XN2+(dlnXN_dT.^2.*(2.*X+4)+dlnXO2_dT.^2.*(X+2)).*XN).*XO2+dlnKO_dT2.*(XO+(-4.*X-4).*XN2+(-X-2).*XN).*XO2+(dlnXNO_dT.^2.*X.*XNO+(dlnXO_dT.^2.*(4.*X+2)+dlnXN2_dT.^2.*(2.*X+1)).*XN2+(dlnXO_dT.^2.*(X+1)+dlnXN_dT.^2.*(X+1)).*XN).*XO+(dlnXNO_dT.^2.*(2.*X+2).*XN2+dlnXNO_dT.^2.*XN).*XNO+(2.*dlnXN_dT.^2.*X-dlnXN2_dT.^2.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXN2_dT2 = -(dlnKN_dT2.*(((2.*X+2).*XNO+(2.*X+4).*XN).*XO2+(X.*XNO+(X+1).*XN).*XO+XN.*XNO)+dlnKNO_dT2.*((-4.*X-4).*XNO.*XO2-2.*X.*XNO.*XO)+((4.*dlnXO_dT.^2-2.*dlnXO2_dT.^2).*XO+(dlnXNO_dT.^2.*(4.*X+4)+dlnXO2_dT.^2.*(-2.*X-2)).*XNO+dlnXN2_dT.^2.*(8.*X+8).*XN2+dlnXN_dT.^2.*(4.*X+8).*XN).*XO2+dlnKO_dT2.*(2.*XO+(2.*X+2).*XNO).*XO2+((2.*dlnXNO_dT.^2.*X-2.*dlnXO_dT.^2.*X).*XNO+dlnXN2_dT.^2.*(4.*X+2).*XN2+dlnXN_dT.^2.*(2.*X+2).*XN).*XO+(dlnXN2_dT.^2.*(2.*X+2).*XN2+2.*dlnXN_dT.^2.*XN).*XNO)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXO2_dT2 = -((dlnXO2_dT.^2.*(2.*X+2).*XNO+dlnXO2_dT.^2.*(8.*X+8).*XN2+dlnXO2_dT.^2.*(2.*X+4).*XN).*XO2+dlnKO_dT2.*((X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)+(2.*dlnXO_dT.^2.*X.*XNO+dlnXO_dT.^2.*(8.*X+4).*XN2+dlnXO_dT.^2.*(2.*X+2).*XN).*XO+dlnKN_dT2.*((2.*X+2).*XN2.*XNO+2.*X.*XN.*XN2)+((dlnXNO_dT.^2.*(4.*X+4)+dlnXN2_dT.^2.*(-2.*X-2)).*XN2+(2.*dlnXNO_dT.^2-2.*dlnXN_dT.^2).*XN).*XNO+dlnKNO_dT2.*((-4.*X-4).*XN2-2.*XN).*XNO+(4.*dlnXN_dT.^2.*X-2.*dlnXN2_dT.^2.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dXN_dT2 = dlnXN_dT2.*XN+dXN_dT.^2./XN ;

    dXO_dT2 = dlnXO_dT2.*XO+dXO_dT.^2./XO ;

    dXNO_dT2 = dlnXNO_dT2.*XNO+dXNO_dT.^2./XNO ;

    dXN2_dT2 = dlnXN2_dT2.*XN2+dXN2_dT.^2./XN2 ;

    dXO2_dT2 = dlnXO2_dT2.*XO2+dXO2_dT.^2./XO2 ;

    % Pressure-temperature crossed derivatives ;

    dlnXN_dpdT = -(((2.*dlnXO_dT.*dlnXO_dp-dlnXO2_dT.*dlnXO2_dp).*XO+(dlnXNO_dT.*dlnXNO_dp.*(2.*X+2)+dlnXO2_dT.*dlnXO2_dp.*(-X-1)).*XNO+dlnXN2_dT.*dlnXN2_dp.*(4.*X+4).*XN2+dlnXN_dT.*dlnXN_dp.*(2.*X+4).*XN).*XO2+((dlnXNO_dT.*dlnXNO_dp.*X-dlnXO_dT.*dlnXO_dp.*X).*XNO+dlnXN2_dT.*dlnXN2_dp.*(2.*X+1).*XN2+dlnXN_dT.*dlnXN_dp.*(X+1).*XN).*XO+(dlnXN2_dT.*dlnXN2_dp.*(X+1).*XN2+dlnXN_dT.*dlnXN_dp.*XN).*XNO)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXO_dpdT = -((dlnXO2_dT.*dlnXO2_dp.*(X+1).*XNO+dlnXO2_dT.*dlnXO2_dp.*(4.*X+4).*XN2+dlnXO2_dT.*dlnXO2_dp.*(X+2).*XN).*XO2+(dlnXO_dT.*dlnXO_dp.*X.*XNO+dlnXO_dT.*dlnXO_dp.*(4.*X+2).*XN2+dlnXO_dT.*dlnXO_dp.*(X+1).*XN).*XO+((dlnXNO_dT.*dlnXNO_dp.*(2.*X+2)+dlnXN2_dT.*dlnXN2_dp.*(-X-1)).*XN2+(dlnXNO_dT.*dlnXNO_dp-dlnXN_dT.*dlnXN_dp).*XN).*XNO+(2.*dlnXN_dT.*dlnXN_dp.*X-dlnXN2_dT.*dlnXN2_dp.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXNO_dpdT = -(((2.*dlnXO_dT.*dlnXO_dp-dlnXO2_dT.*dlnXO2_dp).*XO+dlnXNO_dT.*dlnXNO_dp.*(2.*X+2).*XNO+(dlnXO2_dT.*dlnXO2_dp.*(4.*X+4)+dlnXN2_dT.*dlnXN2_dp.*(4.*X+4)).*XN2+(dlnXN_dT.*dlnXN_dp.*(2.*X+4)+dlnXO2_dT.*dlnXO2_dp.*(X+2)).*XN).*XO2+(dlnXNO_dT.*dlnXNO_dp.*X.*XNO+(dlnXO_dT.*dlnXO_dp.*(4.*X+2)+dlnXN2_dT.*dlnXN2_dp.*(2.*X+1)).*XN2+(dlnXO_dT.*dlnXO_dp.*(X+1)+dlnXN_dT.*dlnXN_dp.*(X+1)).*XN).*XO+(dlnXNO_dT.*dlnXNO_dp.*(2.*X+2).*XN2+dlnXNO_dT.*dlnXNO_dp.*XN).*XNO+(2.*dlnXN_dT.*dlnXN_dp.*X-dlnXN2_dT.*dlnXN2_dp.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXN2_dpdT = -(((4.*dlnXO_dT.*dlnXO_dp-2.*dlnXO2_dT.*dlnXO2_dp).*XO+(dlnXNO_dT.*dlnXNO_dp.*(4.*X+4)+dlnXO2_dT.*dlnXO2_dp.*(-2.*X-2)).*XNO+dlnXN2_dT.*dlnXN2_dp.*(8.*X+8).*XN2+dlnXN_dT.*dlnXN_dp.*(4.*X+8).*XN).*XO2+((2.*dlnXNO_dT.*dlnXNO_dp.*X-2.*dlnXO_dT.*dlnXO_dp.*X).*XNO+dlnXN2_dT.*dlnXN2_dp.*(4.*X+2).*XN2+dlnXN_dT.*dlnXN_dp.*(2.*X+2).*XN).*XO+(dlnXN2_dT.*dlnXN2_dp.*(2.*X+2).*XN2+2.*dlnXN_dT.*dlnXN_dp.*XN).*XNO)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXO2_dpdT = -((dlnXO2_dT.*dlnXO2_dp.*(2.*X+2).*XNO+dlnXO2_dT.*dlnXO2_dp.*(8.*X+8).*XN2+dlnXO2_dT.*dlnXO2_dp.*(2.*X+4).*XN).*XO2+(2.*dlnXO_dT.*dlnXO_dp.*X.*XNO+dlnXO_dT.*dlnXO_dp.*(8.*X+4).*XN2+dlnXO_dT.*dlnXO_dp.*(2.*X+2).*XN).*XO+((dlnXNO_dT.*dlnXNO_dp.*(4.*X+4)+dlnXN2_dT.*dlnXN2_dp.*(-2.*X-2)).*XN2+(2.*dlnXNO_dT.*dlnXNO_dp-2.*dlnXN_dT.*dlnXN_dp).*XN).*XNO+(4.*dlnXN_dT.*dlnXN_dp.*X-2.*dlnXN2_dT.*dlnXN2_dp.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dXN_dpdT = dlnXN_dpdT.*XN+dXN_dT.*dXN_dp./XN ;

    dXO_dpdT = dlnXO_dpdT.*XO+dXO_dT.*dXO_dp./XO ;

    dXNO_dpdT = dlnXNO_dpdT.*XNO+dXNO_dT.*dXNO_dp./XNO ;

    dXN2_dpdT = dlnXN2_dpdT.*XN2+dXN2_dT.*dXN2_dp./XN2 ;

    dXO2_dpdT = dlnXO2_dpdT.*XO2+dXO2_dT.*dXO2_dp./XO2 ;

end

if derOrder>2
    % Pressure 3rd-order derivatives ;

    dlnXN_dp3 = -(p.^3.*(((6.*dlnXO_dp.*dlnXO_dp2+2.*dlnXO_dp.^3-3.*dlnXO2_dp.*dlnXO2_dp2-dlnXO2_dp.^3).*XO+(dlnXNO_dp.*dlnXNO_dp2.*(6.*X+6)+dlnXNO_dp.^3.*(2.*X+2)+dlnXO2_dp.^3.*(-X-1)+dlnXO2_dp.*dlnXO2_dp2.*(-3.*X-3)).*XNO+(dlnXN2_dp.*dlnXN2_dp2.*(12.*X+12)+dlnXN2_dp.^3.*(4.*X+4)).*XN2+(dlnXN_dp.*dlnXN_dp2.*(6.*X+12)+dlnXN_dp.^3.*(2.*X+4)).*XN).*XO2+((-3.*dlnXO_dp.*dlnXO_dp2.*X-dlnXO_dp.^3.*X+3.*dlnXNO_dp.*dlnXNO_dp2.*X+dlnXNO_dp.^3.*X).*XNO+(dlnXN2_dp.*dlnXN2_dp2.*(6.*X+3)+dlnXN2_dp.^3.*(2.*X+1)).*XN2+(dlnXN_dp.*dlnXN_dp2.*(3.*X+3)+dlnXN_dp.^3.*(X+1)).*XN).*XO+((dlnXN2_dp.*dlnXN2_dp2.*(3.*X+3)+dlnXN2_dp.^3.*(X+1)).*XN2+(3.*dlnXN_dp.*dlnXN_dp2+dlnXN_dp.^3).*XN).*XNO)+(-2.*XO+(2.*X+2).*XNO+(8.*X+8).*XN2).*XO2+(2.*X.*XNO+(4.*X+2).*XN2).*XO+(2.*X+2).*XN2.*XNO)./(p.^3.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dlnXO_dp3 = -(p.^3.*(((dlnXO2_dp.*dlnXO2_dp2.*(3.*X+3)+dlnXO2_dp.^3.*(X+1)).*XNO+(dlnXO2_dp.*dlnXO2_dp2.*(12.*X+12)+dlnXO2_dp.^3.*(4.*X+4)).*XN2+(dlnXO2_dp.*dlnXO2_dp2.*(3.*X+6)+dlnXO2_dp.^3.*(X+2)).*XN).*XO2+((3.*dlnXO_dp.*dlnXO_dp2.*X+dlnXO_dp.^3.*X).*XNO+(dlnXO_dp.*dlnXO_dp2.*(12.*X+6)+dlnXO_dp.^3.*(4.*X+2)).*XN2+(dlnXO_dp.*dlnXO_dp2.*(3.*X+3)+dlnXO_dp.^3.*(X+1)).*XN).*XO+((dlnXNO_dp.*dlnXNO_dp2.*(6.*X+6)+dlnXNO_dp.^3.*(2.*X+2)+dlnXN2_dp.^3.*(-X-1)+dlnXN2_dp.*dlnXN2_dp2.*(-3.*X-3)).*XN2+(-3.*dlnXN_dp.*dlnXN_dp2-dlnXN_dp.^3+3.*dlnXNO_dp.*dlnXNO_dp2+dlnXNO_dp.^3).*XN).*XNO+(6.*dlnXN_dp.*dlnXN_dp2.*X+2.*dlnXN_dp.^3.*X-3.*dlnXN2_dp.*dlnXN2_dp2.*X-dlnXN2_dp.^3.*X).*XN.*XN2)+((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+((2.*X+2).*XN2+2.*XN).*XNO-2.*X.*XN.*XN2)./(p.^3.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dlnXNO_dp3 = -(p.^3.*(((6.*dlnXO_dp.*dlnXO_dp2+2.*dlnXO_dp.^3-3.*dlnXO2_dp.*dlnXO2_dp2-dlnXO2_dp.^3).*XO+(dlnXNO_dp.*dlnXNO_dp2.*(6.*X+6)+dlnXNO_dp.^3.*(2.*X+2)).*XNO+(dlnXO2_dp.*dlnXO2_dp2.*(12.*X+12)+dlnXN2_dp.*dlnXN2_dp2.*(12.*X+12)+dlnXO2_dp.^3.*(4.*X+4)+dlnXN2_dp.^3.*(4.*X+4)).*XN2+(dlnXN_dp.*dlnXN_dp2.*(6.*X+12)+dlnXO2_dp.*dlnXO2_dp2.*(3.*X+6)+dlnXN_dp.^3.*(2.*X+4)+dlnXO2_dp.^3.*(X+2)).*XN).*XO2+((3.*dlnXNO_dp.*dlnXNO_dp2.*X+dlnXNO_dp.^3.*X).*XNO+(dlnXO_dp.*dlnXO_dp2.*(12.*X+6)+dlnXN2_dp.*dlnXN2_dp2.*(6.*X+3)+dlnXO_dp.^3.*(4.*X+2)+dlnXN2_dp.^3.*(2.*X+1)).*XN2+(dlnXO_dp.*dlnXO_dp2.*(3.*X+3)+dlnXN_dp.*dlnXN_dp2.*(3.*X+3)+dlnXO_dp.^3.*(X+1)+dlnXN_dp.^3.*(X+1)).*XN).*XO+((dlnXNO_dp.*dlnXNO_dp2.*(6.*X+6)+dlnXNO_dp.^3.*(2.*X+2)).*XN2+(3.*dlnXNO_dp.*dlnXNO_dp2+dlnXNO_dp.^3).*XN).*XNO+(6.*dlnXN_dp.*dlnXN_dp2.*X+2.*dlnXN_dp.^3.*X-3.*dlnXN2_dp.*dlnXN2_dp2.*X-dlnXN2_dp.^3.*X).*XN.*XN2)+((-2.*X-4).*XN-2.*XO).*XO2+((-4.*X-2).*XN2+(-2.*X-2).*XN).*XO-2.*X.*XN.*XN2)./(p.^3.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dlnXN2_dp3 = -(p.^3.*(((12.*dlnXO_dp.*dlnXO_dp2+4.*dlnXO_dp.^3-6.*dlnXO2_dp.*dlnXO2_dp2-2.*dlnXO2_dp.^3).*XO+(dlnXNO_dp.*dlnXNO_dp2.*(12.*X+12)+dlnXNO_dp.^3.*(4.*X+4)+dlnXO2_dp.^3.*(-2.*X-2)+dlnXO2_dp.*dlnXO2_dp2.*(-6.*X-6)).*XNO+(dlnXN2_dp.*dlnXN2_dp2.*(24.*X+24)+dlnXN2_dp.^3.*(8.*X+8)).*XN2+(dlnXN_dp.*dlnXN_dp2.*(12.*X+24)+dlnXN_dp.^3.*(4.*X+8)).*XN).*XO2+((-6.*dlnXO_dp.*dlnXO_dp2.*X-2.*dlnXO_dp.^3.*X+6.*dlnXNO_dp.*dlnXNO_dp2.*X+2.*dlnXNO_dp.^3.*X).*XNO+(dlnXN2_dp.*dlnXN2_dp2.*(12.*X+6)+dlnXN2_dp.^3.*(4.*X+2)).*XN2+(dlnXN_dp.*dlnXN_dp2.*(6.*X+6)+dlnXN_dp.^3.*(2.*X+2)).*XN).*XO+((dlnXN2_dp.*dlnXN2_dp2.*(6.*X+6)+dlnXN2_dp.^3.*(2.*X+2)).*XN2+(6.*dlnXN_dp.*dlnXN_dp2+2.*dlnXN_dp.^3).*XN).*XNO)+((-4.*X-8).*XN-4.*XO).*XO2+(2.*X.*XNO+(-2.*X-2).*XN).*XO-2.*XN.*XNO)./(p.^3.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dlnXO2_dp3 = -(p.^3.*(((dlnXO2_dp.*dlnXO2_dp2.*(6.*X+6)+dlnXO2_dp.^3.*(2.*X+2)).*XNO+(dlnXO2_dp.*dlnXO2_dp2.*(24.*X+24)+dlnXO2_dp.^3.*(8.*X+8)).*XN2+(dlnXO2_dp.*dlnXO2_dp2.*(6.*X+12)+dlnXO2_dp.^3.*(2.*X+4)).*XN).*XO2+((6.*dlnXO_dp.*dlnXO_dp2.*X+2.*dlnXO_dp.^3.*X).*XNO+(dlnXO_dp.*dlnXO_dp2.*(24.*X+12)+dlnXO_dp.^3.*(8.*X+4)).*XN2+(dlnXO_dp.*dlnXO_dp2.*(6.*X+6)+dlnXO_dp.^3.*(2.*X+2)).*XN).*XO+((dlnXNO_dp.*dlnXNO_dp2.*(12.*X+12)+dlnXNO_dp.^3.*(4.*X+4)+dlnXN2_dp.^3.*(-2.*X-2)+dlnXN2_dp.*dlnXN2_dp2.*(-6.*X-6)).*XN2+(-6.*dlnXN_dp.*dlnXN_dp2-2.*dlnXN_dp.^3+6.*dlnXNO_dp.*dlnXNO_dp2+2.*dlnXNO_dp.^3).*XN).*XNO+(12.*dlnXN_dp.*dlnXN_dp2.*X+4.*dlnXN_dp.^3.*X-6.*dlnXN2_dp.*dlnXN2_dp2.*X-2.*dlnXN2_dp.^3.*X).*XN.*XN2)+(-2.*X.*XNO+(-8.*X-4).*XN2+(-2.*X-2).*XN).*XO+2.*XN.*XNO-4.*X.*XN.*XN2)./(p.^3.*(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)) ;

    dXN_dp3 = dlnXN_dp3.*XN+3.*dXN_dp.*dXN_dp2./XN-2.*dXN_dp.^3./XN.^2 ;

    dXO_dp3 = dlnXO_dp3.*XO+3.*dXO_dp.*dXO_dp2./XO-2.*dXO_dp.^3./XO.^2 ;

    dXNO_dp3 = dlnXNO_dp3.*XNO+3.*dXNO_dp.*dXNO_dp2./XNO-2.*dXNO_dp.^3./XNO.^2 ;

    dXN2_dp3 = dlnXN2_dp3.*XN2+3.*dXN2_dp.*dXN2_dp2./XN2-2.*dXN2_dp.^3./XN2.^2 ;

    dXO2_dp3 = dlnXO2_dp3.*XO2+3.*dXO2_dp.*dXO2_dp2./XO2-2.*dXO2_dp.^3./XO2.^2 ;

    % Temperature 3rd-order derivatives ;

    dlnXN_dT3 = -(dlnKNO_dT3.*((-2.*X-2).*XNO.*XO2-X.*XNO.*XO)+dlnKN_dT3.*((-4.*X-4).*XN2.*XO2+(-2.*X-1).*XN2.*XO+(-X-1).*XN2.*XNO)+((6.*dlnXO_dT.*dlnXO_dT2+2.*dlnXO_dT.^3-3.*dlnXO2_dT.*dlnXO2_dT2-dlnXO2_dT.^3).*XO+(dlnXNO_dT.*dlnXNO_dT2.*(6.*X+6)+dlnXNO_dT.^3.*(2.*X+2)+dlnXO2_dT.^3.*(-X-1)+dlnXO2_dT.*dlnXO2_dT2.*(-3.*X-3)).*XNO+(dlnXN2_dT.*dlnXN2_dT2.*(12.*X+12)+dlnXN2_dT.^3.*(4.*X+4)).*XN2+(dlnXN_dT.*dlnXN_dT2.*(6.*X+12)+dlnXN_dT.^3.*(2.*X+4)).*XN).*XO2+dlnKO_dT3.*(XO+(X+1).*XNO).*XO2+((-3.*dlnXO_dT.*dlnXO_dT2.*X-dlnXO_dT.^3.*X+3.*dlnXNO_dT.*dlnXNO_dT2.*X+dlnXNO_dT.^3.*X).*XNO+(dlnXN2_dT.*dlnXN2_dT2.*(6.*X+3)+dlnXN2_dT.^3.*(2.*X+1)).*XN2+(dlnXN_dT.*dlnXN_dT2.*(3.*X+3)+dlnXN_dT.^3.*(X+1)).*XN).*XO+((dlnXN2_dT.*dlnXN2_dT2.*(3.*X+3)+dlnXN2_dT.^3.*(X+1)).*XN2+(3.*dlnXN_dT.*dlnXN_dT2+dlnXN_dT.^3).*XN).*XNO)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXO_dT3 = (dlnKO_dT3.*((X+1).*XNO+(4.*X+4).*XN2+(X+2).*XN).*XO2+((dlnXO2_dT.^3.*(-X-1)+dlnXO2_dT.*dlnXO2_dT2.*(-3.*X-3)).*XNO+(dlnXO2_dT.^3.*(-4.*X-4)+dlnXO2_dT.*dlnXO2_dT2.*(-12.*X-12)).*XN2+(dlnXO2_dT.^3.*(-X-2)+dlnXO2_dT.*dlnXO2_dT2.*(-3.*X-6)).*XN).*XO2+((-3.*dlnXO_dT.*dlnXO_dT2.*X-dlnXO_dT.^3.*X).*XNO+(dlnXO_dT.^3.*(-4.*X-2)+dlnXO_dT.*dlnXO_dT2.*(-12.*X-6)).*XN2+(dlnXO_dT.^3.*(-X-1)+dlnXO_dT.*dlnXO_dT2.*(-3.*X-3)).*XN).*XO+dlnKN_dT3.*((-X-1).*XN2.*XNO-X.*XN.*XN2)+((dlnXN2_dT.*dlnXN2_dT2.*(3.*X+3)+dlnXN2_dT.^3.*(X+1)+dlnXNO_dT.^3.*(-2.*X-2)+dlnXNO_dT.*dlnXNO_dT2.*(-6.*X-6)).*XN2+(3.*dlnXN_dT.*dlnXN_dT2+dlnXN_dT.^3-3.*dlnXNO_dT.*dlnXNO_dT2-dlnXNO_dT.^3).*XN).*XNO+dlnKNO_dT3.*((2.*X+2).*XN2+XN).*XNO+(-6.*dlnXN_dT.*dlnXN_dT2.*X-2.*dlnXN_dT.^3.*X+3.*dlnXN2_dT.*dlnXN2_dT2.*X+dlnXN2_dT.^3.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXNO_dT3 = -(dlnKNO_dT3.*(((8.*X+8).*XN2+(2.*X+4).*XN).*XO2+((4.*X+2).*XN2+(X+1).*XN).*XO)+dlnKN_dT3.*((-4.*X-4).*XN2.*XO2+(-2.*X-1).*XN2.*XO+X.*XN.*XN2)+((6.*dlnXO_dT.*dlnXO_dT2+2.*dlnXO_dT.^3-3.*dlnXO2_dT.*dlnXO2_dT2-dlnXO2_dT.^3).*XO+(dlnXNO_dT.*dlnXNO_dT2.*(6.*X+6)+dlnXNO_dT.^3.*(2.*X+2)).*XNO+(dlnXO2_dT.*dlnXO2_dT2.*(12.*X+12)+dlnXN2_dT.*dlnXN2_dT2.*(12.*X+12)+dlnXO2_dT.^3.*(4.*X+4)+dlnXN2_dT.^3.*(4.*X+4)).*XN2+(dlnXN_dT.*dlnXN_dT2.*(6.*X+12)+dlnXO2_dT.*dlnXO2_dT2.*(3.*X+6)+dlnXN_dT.^3.*(2.*X+4)+dlnXO2_dT.^3.*(X+2)).*XN).*XO2+dlnKO_dT3.*(XO+(-4.*X-4).*XN2+(-X-2).*XN).*XO2+((3.*dlnXNO_dT.*dlnXNO_dT2.*X+dlnXNO_dT.^3.*X).*XNO+(dlnXO_dT.*dlnXO_dT2.*(12.*X+6)+dlnXN2_dT.*dlnXN2_dT2.*(6.*X+3)+dlnXO_dT.^3.*(4.*X+2)+dlnXN2_dT.^3.*(2.*X+1)).*XN2+(dlnXO_dT.*dlnXO_dT2.*(3.*X+3)+dlnXN_dT.*dlnXN_dT2.*(3.*X+3)+dlnXO_dT.^3.*(X+1)+dlnXN_dT.^3.*(X+1)).*XN).*XO+((dlnXNO_dT.*dlnXNO_dT2.*(6.*X+6)+dlnXNO_dT.^3.*(2.*X+2)).*XN2+(3.*dlnXNO_dT.*dlnXNO_dT2+dlnXNO_dT.^3).*XN).*XNO+(6.*dlnXN_dT.*dlnXN_dT2.*X+2.*dlnXN_dT.^3.*X-3.*dlnXN2_dT.*dlnXN2_dT2.*X-dlnXN2_dT.^3.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXN2_dT3 = -(dlnKN_dT3.*(((2.*X+2).*XNO+(2.*X+4).*XN).*XO2+(X.*XNO+(X+1).*XN).*XO+XN.*XNO)+dlnKNO_dT3.*((-4.*X-4).*XNO.*XO2-2.*X.*XNO.*XO)+((12.*dlnXO_dT.*dlnXO_dT2+4.*dlnXO_dT.^3-6.*dlnXO2_dT.*dlnXO2_dT2-2.*dlnXO2_dT.^3).*XO+(dlnXNO_dT.*dlnXNO_dT2.*(12.*X+12)+dlnXNO_dT.^3.*(4.*X+4)+dlnXO2_dT.^3.*(-2.*X-2)+dlnXO2_dT.*dlnXO2_dT2.*(-6.*X-6)).*XNO+(dlnXN2_dT.*dlnXN2_dT2.*(24.*X+24)+dlnXN2_dT.^3.*(8.*X+8)).*XN2+(dlnXN_dT.*dlnXN_dT2.*(12.*X+24)+dlnXN_dT.^3.*(4.*X+8)).*XN).*XO2+dlnKO_dT3.*(2.*XO+(2.*X+2).*XNO).*XO2+((-6.*dlnXO_dT.*dlnXO_dT2.*X-2.*dlnXO_dT.^3.*X+6.*dlnXNO_dT.*dlnXNO_dT2.*X+2.*dlnXNO_dT.^3.*X).*XNO+(dlnXN2_dT.*dlnXN2_dT2.*(12.*X+6)+dlnXN2_dT.^3.*(4.*X+2)).*XN2+(dlnXN_dT.*dlnXN_dT2.*(6.*X+6)+dlnXN_dT.^3.*(2.*X+2)).*XN).*XO+((dlnXN2_dT.*dlnXN2_dT2.*(6.*X+6)+dlnXN2_dT.^3.*(2.*X+2)).*XN2+(6.*dlnXN_dT.*dlnXN_dT2+2.*dlnXN_dT.^3).*XN).*XNO)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXO2_dT3 = -(((dlnXO2_dT.*dlnXO2_dT2.*(6.*X+6)+dlnXO2_dT.^3.*(2.*X+2)).*XNO+(dlnXO2_dT.*dlnXO2_dT2.*(24.*X+24)+dlnXO2_dT.^3.*(8.*X+8)).*XN2+(dlnXO2_dT.*dlnXO2_dT2.*(6.*X+12)+dlnXO2_dT.^3.*(2.*X+4)).*XN).*XO2+dlnKO_dT3.*((X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO)+((6.*dlnXO_dT.*dlnXO_dT2.*X+2.*dlnXO_dT.^3.*X).*XNO+(dlnXO_dT.*dlnXO_dT2.*(24.*X+12)+dlnXO_dT.^3.*(8.*X+4)).*XN2+(dlnXO_dT.*dlnXO_dT2.*(6.*X+6)+dlnXO_dT.^3.*(2.*X+2)).*XN).*XO+dlnKN_dT3.*((2.*X+2).*XN2.*XNO+2.*X.*XN.*XN2)+((dlnXNO_dT.*dlnXNO_dT2.*(12.*X+12)+dlnXNO_dT.^3.*(4.*X+4)+dlnXN2_dT.^3.*(-2.*X-2)+dlnXN2_dT.*dlnXN2_dT2.*(-6.*X-6)).*XN2+(-6.*dlnXN_dT.*dlnXN_dT2-2.*dlnXN_dT.^3+6.*dlnXNO_dT.*dlnXNO_dT2+2.*dlnXNO_dT.^3).*XN).*XNO+dlnKNO_dT3.*((-4.*X-4).*XN2-2.*XN).*XNO+(12.*dlnXN_dT.*dlnXN_dT2.*X+4.*dlnXN_dT.^3.*X-6.*dlnXN2_dT.*dlnXN2_dT2.*X-2.*dlnXN2_dT.^3.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dXN_dT3 = dlnXN_dT3.*XN+3.*dXN_dT.*dXN_dT2./XN-2.*dXN_dT.^3./XN.^2 ;

    dXO_dT3 = dlnXO_dT3.*XO+3.*dXO_dT.*dXO_dT2./XO-2.*dXO_dT.^3./XO.^2 ;

    dXNO_dT3 = dlnXNO_dT3.*XNO+3.*dXNO_dT.*dXNO_dT2./XNO-2.*dXNO_dT.^3./XNO.^2 ;

    dXN2_dT3 = dlnXN2_dT3.*XN2+3.*dXN2_dT.*dXN2_dT2./XN2-2.*dXN2_dT.^3./XN2.^2 ;

    dXO2_dT3 = dlnXO2_dT3.*XO2+3.*dXO2_dT.*dXO2_dT2./XO2-2.*dXO2_dT.^3./XO2.^2 ;

    % Pressure-temperature(x2) crossed derivatives ;

    dlnXN_dpdT2 = -(((4.*dlnXO_dT.*dlnXO_dpdT+(2.*dlnXO_dT2+2.*dlnXO_dT.^2).*dlnXO_dp-2.*dlnXO2_dT.*dlnXO2_dpdT+(-dlnXO2_dT2-dlnXO2_dT.^2).*dlnXO2_dp).*XO+(dlnXNO_dp.*(dlnXNO_dT2.*(2.*X+2)+dlnXNO_dT.^2.*(2.*X+2))+dlnXNO_dT.*dlnXNO_dpdT.*(4.*X+4)+dlnXO2_dT.*dlnXO2_dpdT.*(-2.*X-2)+dlnXO2_dp.*(dlnXO2_dT2.*(-X-1)+dlnXO2_dT.^2.*(-X-1))).*XNO+(dlnXN2_dp.*(dlnXN2_dT2.*(4.*X+4)+dlnXN2_dT.^2.*(4.*X+4))+dlnXN2_dT.*dlnXN2_dpdT.*(8.*X+8)).*XN2+(dlnXN_dp.*(dlnXN_dT2.*(2.*X+4)+dlnXN_dT.^2.*(2.*X+4))+dlnXN_dT.*dlnXN_dpdT.*(4.*X+8)).*XN).*XO2+((dlnXO_dp.*(-dlnXO_dT2.*X-dlnXO_dT.^2.*X)+dlnXNO_dp.*(dlnXNO_dT2.*X+dlnXNO_dT.^2.*X)-2.*dlnXO_dT.*dlnXO_dpdT.*X+2.*dlnXNO_dT.*dlnXNO_dpdT.*X).*XNO+(dlnXN2_dp.*(dlnXN2_dT2.*(2.*X+1)+dlnXN2_dT.^2.*(2.*X+1))+dlnXN2_dT.*dlnXN2_dpdT.*(4.*X+2)).*XN2+(dlnXN_dp.*(dlnXN_dT2.*(X+1)+dlnXN_dT.^2.*(X+1))+dlnXN_dT.*dlnXN_dpdT.*(2.*X+2)).*XN).*XO+((dlnXN2_dp.*(dlnXN2_dT2.*(X+1)+dlnXN2_dT.^2.*(X+1))+dlnXN2_dT.*dlnXN2_dpdT.*(2.*X+2)).*XN2+(2.*dlnXN_dT.*dlnXN_dpdT+(dlnXN_dT2+dlnXN_dT.^2).*dlnXN_dp).*XN).*XNO)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXO_dpdT2 = -(((dlnXO2_dp.*(dlnXO2_dT2.*(X+1)+dlnXO2_dT.^2.*(X+1))+dlnXO2_dT.*dlnXO2_dpdT.*(2.*X+2)).*XNO+(dlnXO2_dp.*(dlnXO2_dT2.*(4.*X+4)+dlnXO2_dT.^2.*(4.*X+4))+dlnXO2_dT.*dlnXO2_dpdT.*(8.*X+8)).*XN2+(dlnXO2_dp.*(dlnXO2_dT2.*(X+2)+dlnXO2_dT.^2.*(X+2))+dlnXO2_dT.*dlnXO2_dpdT.*(2.*X+4)).*XN).*XO2+((dlnXO_dp.*(dlnXO_dT2.*X+dlnXO_dT.^2.*X)+2.*dlnXO_dT.*dlnXO_dpdT.*X).*XNO+(dlnXO_dp.*(dlnXO_dT2.*(4.*X+2)+dlnXO_dT.^2.*(4.*X+2))+dlnXO_dT.*dlnXO_dpdT.*(8.*X+4)).*XN2+(dlnXO_dp.*(dlnXO_dT2.*(X+1)+dlnXO_dT.^2.*(X+1))+dlnXO_dT.*dlnXO_dpdT.*(2.*X+2)).*XN).*XO+((dlnXNO_dp.*(dlnXNO_dT2.*(2.*X+2)+dlnXNO_dT.^2.*(2.*X+2))+dlnXNO_dT.*dlnXNO_dpdT.*(4.*X+4)+dlnXN2_dT.*dlnXN2_dpdT.*(-2.*X-2)+dlnXN2_dp.*(dlnXN2_dT2.*(-X-1)+dlnXN2_dT.^2.*(-X-1))).*XN2+(-2.*dlnXN_dT.*dlnXN_dpdT+(-dlnXN_dT2-dlnXN_dT.^2).*dlnXN_dp+2.*dlnXNO_dT.*dlnXNO_dpdT+(dlnXNO_dT2+dlnXNO_dT.^2).*dlnXNO_dp).*XN).*XNO+(dlnXN_dp.*(2.*dlnXN_dT2.*X+2.*dlnXN_dT.^2.*X)+dlnXN2_dp.*(-dlnXN2_dT2.*X-dlnXN2_dT.^2.*X)+4.*dlnXN_dT.*dlnXN_dpdT.*X-2.*dlnXN2_dT.*dlnXN2_dpdT.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXNO_dpdT2 = -(((4.*dlnXO_dT.*dlnXO_dpdT+(2.*dlnXO_dT2+2.*dlnXO_dT.^2).*dlnXO_dp-2.*dlnXO2_dT.*dlnXO2_dpdT+(-dlnXO2_dT2-dlnXO2_dT.^2).*dlnXO2_dp).*XO+(dlnXNO_dp.*(dlnXNO_dT2.*(2.*X+2)+dlnXNO_dT.^2.*(2.*X+2))+dlnXNO_dT.*dlnXNO_dpdT.*(4.*X+4)).*XNO+(dlnXO2_dp.*(dlnXO2_dT2.*(4.*X+4)+dlnXO2_dT.^2.*(4.*X+4))+dlnXN2_dp.*(dlnXN2_dT2.*(4.*X+4)+dlnXN2_dT.^2.*(4.*X+4))+dlnXO2_dT.*dlnXO2_dpdT.*(8.*X+8)+dlnXN2_dT.*dlnXN2_dpdT.*(8.*X+8)).*XN2+(dlnXN_dp.*(dlnXN_dT2.*(2.*X+4)+dlnXN_dT.^2.*(2.*X+4))+dlnXO2_dp.*(dlnXO2_dT2.*(X+2)+dlnXO2_dT.^2.*(X+2))+dlnXN_dT.*dlnXN_dpdT.*(4.*X+8)+dlnXO2_dT.*dlnXO2_dpdT.*(2.*X+4)).*XN).*XO2+((dlnXNO_dp.*(dlnXNO_dT2.*X+dlnXNO_dT.^2.*X)+2.*dlnXNO_dT.*dlnXNO_dpdT.*X).*XNO+(dlnXO_dp.*(dlnXO_dT2.*(4.*X+2)+dlnXO_dT.^2.*(4.*X+2))+dlnXN2_dp.*(dlnXN2_dT2.*(2.*X+1)+dlnXN2_dT.^2.*(2.*X+1))+dlnXO_dT.*dlnXO_dpdT.*(8.*X+4)+dlnXN2_dT.*dlnXN2_dpdT.*(4.*X+2)).*XN2+(dlnXO_dp.*(dlnXO_dT2.*(X+1)+dlnXO_dT.^2.*(X+1))+dlnXN_dp.*(dlnXN_dT2.*(X+1)+dlnXN_dT.^2.*(X+1))+dlnXO_dT.*dlnXO_dpdT.*(2.*X+2)+dlnXN_dT.*dlnXN_dpdT.*(2.*X+2)).*XN).*XO+((dlnXNO_dp.*(dlnXNO_dT2.*(2.*X+2)+dlnXNO_dT.^2.*(2.*X+2))+dlnXNO_dT.*dlnXNO_dpdT.*(4.*X+4)).*XN2+(2.*dlnXNO_dT.*dlnXNO_dpdT+(dlnXNO_dT2+dlnXNO_dT.^2).*dlnXNO_dp).*XN).*XNO+(dlnXN_dp.*(2.*dlnXN_dT2.*X+2.*dlnXN_dT.^2.*X)+dlnXN2_dp.*(-dlnXN2_dT2.*X-dlnXN2_dT.^2.*X)+4.*dlnXN_dT.*dlnXN_dpdT.*X-2.*dlnXN2_dT.*dlnXN2_dpdT.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXN2_dpdT2 = -(((8.*dlnXO_dT.*dlnXO_dpdT+(4.*dlnXO_dT2+4.*dlnXO_dT.^2).*dlnXO_dp-4.*dlnXO2_dT.*dlnXO2_dpdT+(-2.*dlnXO2_dT2-2.*dlnXO2_dT.^2).*dlnXO2_dp).*XO+(dlnXNO_dp.*(dlnXNO_dT2.*(4.*X+4)+dlnXNO_dT.^2.*(4.*X+4))+dlnXNO_dT.*dlnXNO_dpdT.*(8.*X+8)+dlnXO2_dT.*dlnXO2_dpdT.*(-4.*X-4)+dlnXO2_dp.*(dlnXO2_dT2.*(-2.*X-2)+dlnXO2_dT.^2.*(-2.*X-2))).*XNO+(dlnXN2_dp.*(dlnXN2_dT2.*(8.*X+8)+dlnXN2_dT.^2.*(8.*X+8))+dlnXN2_dT.*dlnXN2_dpdT.*(16.*X+16)).*XN2+(dlnXN_dp.*(dlnXN_dT2.*(4.*X+8)+dlnXN_dT.^2.*(4.*X+8))+dlnXN_dT.*dlnXN_dpdT.*(8.*X+16)).*XN).*XO2+((dlnXO_dp.*(-2.*dlnXO_dT2.*X-2.*dlnXO_dT.^2.*X)+dlnXNO_dp.*(2.*dlnXNO_dT2.*X+2.*dlnXNO_dT.^2.*X)-4.*dlnXO_dT.*dlnXO_dpdT.*X+4.*dlnXNO_dT.*dlnXNO_dpdT.*X).*XNO+(dlnXN2_dp.*(dlnXN2_dT2.*(4.*X+2)+dlnXN2_dT.^2.*(4.*X+2))+dlnXN2_dT.*dlnXN2_dpdT.*(8.*X+4)).*XN2+(dlnXN_dp.*(dlnXN_dT2.*(2.*X+2)+dlnXN_dT.^2.*(2.*X+2))+dlnXN_dT.*dlnXN_dpdT.*(4.*X+4)).*XN).*XO+((dlnXN2_dp.*(dlnXN2_dT2.*(2.*X+2)+dlnXN2_dT.^2.*(2.*X+2))+dlnXN2_dT.*dlnXN2_dpdT.*(4.*X+4)).*XN2+(4.*dlnXN_dT.*dlnXN_dpdT+(2.*dlnXN_dT2+2.*dlnXN_dT.^2).*dlnXN_dp).*XN).*XNO)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXO2_dpdT2 = -(((dlnXO2_dp.*(dlnXO2_dT2.*(2.*X+2)+dlnXO2_dT.^2.*(2.*X+2))+dlnXO2_dT.*dlnXO2_dpdT.*(4.*X+4)).*XNO+(dlnXO2_dp.*(dlnXO2_dT2.*(8.*X+8)+dlnXO2_dT.^2.*(8.*X+8))+dlnXO2_dT.*dlnXO2_dpdT.*(16.*X+16)).*XN2+(dlnXO2_dp.*(dlnXO2_dT2.*(2.*X+4)+dlnXO2_dT.^2.*(2.*X+4))+dlnXO2_dT.*dlnXO2_dpdT.*(4.*X+8)).*XN).*XO2+((dlnXO_dp.*(2.*dlnXO_dT2.*X+2.*dlnXO_dT.^2.*X)+4.*dlnXO_dT.*dlnXO_dpdT.*X).*XNO+(dlnXO_dp.*(dlnXO_dT2.*(8.*X+4)+dlnXO_dT.^2.*(8.*X+4))+dlnXO_dT.*dlnXO_dpdT.*(16.*X+8)).*XN2+(dlnXO_dp.*(dlnXO_dT2.*(2.*X+2)+dlnXO_dT.^2.*(2.*X+2))+dlnXO_dT.*dlnXO_dpdT.*(4.*X+4)).*XN).*XO+((dlnXNO_dp.*(dlnXNO_dT2.*(4.*X+4)+dlnXNO_dT.^2.*(4.*X+4))+dlnXNO_dT.*dlnXNO_dpdT.*(8.*X+8)+dlnXN2_dT.*dlnXN2_dpdT.*(-4.*X-4)+dlnXN2_dp.*(dlnXN2_dT2.*(-2.*X-2)+dlnXN2_dT.^2.*(-2.*X-2))).*XN2+(-4.*dlnXN_dT.*dlnXN_dpdT+(-2.*dlnXN_dT2-2.*dlnXN_dT.^2).*dlnXN_dp+4.*dlnXNO_dT.*dlnXNO_dpdT+(2.*dlnXNO_dT2+2.*dlnXNO_dT.^2).*dlnXNO_dp).*XN).*XNO+(dlnXN_dp.*(4.*dlnXN_dT2.*X+4.*dlnXN_dT.^2.*X)+dlnXN2_dp.*(-2.*dlnXN2_dT2.*X-2.*dlnXN2_dT.^2.*X)+8.*dlnXN_dT.*dlnXN_dpdT.*X-4.*dlnXN2_dT.*dlnXN2_dpdT.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dXN_dpdT2 = dlnXN_dpdT2.*XN+2.*dXN_dT.*dXN_dpdT./XN+dXN_dT2.*dXN_dp./XN-2.*dXN_dT.^2.*dXN_dp./XN.^2 ;

    dXO_dpdT2 = dlnXO_dpdT2.*XO+2.*dXO_dT.*dXO_dpdT./XO+dXO_dT2.*dXO_dp./XO-2.*dXO_dT.^2.*dXO_dp./XO.^2 ;

    dXNO_dpdT2 = dlnXNO_dpdT2.*XNO+2.*dXNO_dT.*dXNO_dpdT./XNO+dXNO_dT2.*dXNO_dp./XNO-2.*dXNO_dT.^2.*dXNO_dp./XNO.^2 ;

    dXN2_dpdT2 = dlnXN2_dpdT2.*XN2+2.*dXN2_dT.*dXN2_dpdT./XN2+dXN2_dT2.*dXN2_dp./XN2-2.*dXN2_dT.^2.*dXN2_dp./XN2.^2 ;

    dXO2_dpdT2 = dlnXO2_dpdT2.*XO2+2.*dXO2_dT.*dXO2_dpdT./XO2+dXO2_dT2.*dXO2_dp./XO2-2.*dXO2_dT.^2.*dXO2_dp./XO2.^2 ;

    % Pressure(x2)-temperature crossed derivatives ;

    dlnXN_dp2dT = -(((4.*dlnXO_dp.*dlnXO_dpdT+2.*dlnXO_dT.*dlnXO_dp2+2.*dlnXO_dT.*dlnXO_dp.^2-2.*dlnXO2_dp.*dlnXO2_dpdT-dlnXO2_dT.*dlnXO2_dp2-dlnXO2_dT.*dlnXO2_dp.^2).*XO+(dlnXNO_dp.*dlnXNO_dpdT.*(4.*X+4)+dlnXNO_dT.*dlnXNO_dp2.*(2.*X+2)+dlnXNO_dT.*dlnXNO_dp.^2.*(2.*X+2)+dlnXO2_dT.*dlnXO2_dp2.*(-X-1)+dlnXO2_dT.*dlnXO2_dp.^2.*(-X-1)+dlnXO2_dp.*dlnXO2_dpdT.*(-2.*X-2)).*XNO+(dlnXN2_dp.*dlnXN2_dpdT.*(8.*X+8)+dlnXN2_dT.*dlnXN2_dp2.*(4.*X+4)+dlnXN2_dT.*dlnXN2_dp.^2.*(4.*X+4)).*XN2+(dlnXN_dp.*dlnXN_dpdT.*(4.*X+8)+dlnXN_dT.*dlnXN_dp2.*(2.*X+4)+dlnXN_dT.*dlnXN_dp.^2.*(2.*X+4)).*XN).*XO2+((-2.*dlnXO_dp.*dlnXO_dpdT.*X-dlnXO_dT.*dlnXO_dp2.*X-dlnXO_dT.*dlnXO_dp.^2.*X+2.*dlnXNO_dp.*dlnXNO_dpdT.*X+dlnXNO_dT.*dlnXNO_dp2.*X+dlnXNO_dT.*dlnXNO_dp.^2.*X).*XNO+(dlnXN2_dp.*dlnXN2_dpdT.*(4.*X+2)+dlnXN2_dT.*dlnXN2_dp2.*(2.*X+1)+dlnXN2_dT.*dlnXN2_dp.^2.*(2.*X+1)).*XN2+(dlnXN_dp.*dlnXN_dpdT.*(2.*X+2)+dlnXN_dT.*dlnXN_dp2.*(X+1)+dlnXN_dT.*dlnXN_dp.^2.*(X+1)).*XN).*XO+((dlnXN2_dp.*dlnXN2_dpdT.*(2.*X+2)+dlnXN2_dT.*dlnXN2_dp2.*(X+1)+dlnXN2_dT.*dlnXN2_dp.^2.*(X+1)).*XN2+(2.*dlnXN_dp.*dlnXN_dpdT+dlnXN_dT.*dlnXN_dp2+dlnXN_dT.*dlnXN_dp.^2).*XN).*XNO)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXO_dp2dT = -(((dlnXO2_dp.*dlnXO2_dpdT.*(2.*X+2)+dlnXO2_dT.*dlnXO2_dp2.*(X+1)+dlnXO2_dT.*dlnXO2_dp.^2.*(X+1)).*XNO+(dlnXO2_dp.*dlnXO2_dpdT.*(8.*X+8)+dlnXO2_dT.*dlnXO2_dp2.*(4.*X+4)+dlnXO2_dT.*dlnXO2_dp.^2.*(4.*X+4)).*XN2+(dlnXO2_dp.*dlnXO2_dpdT.*(2.*X+4)+dlnXO2_dT.*dlnXO2_dp2.*(X+2)+dlnXO2_dT.*dlnXO2_dp.^2.*(X+2)).*XN).*XO2+((2.*dlnXO_dp.*dlnXO_dpdT.*X+dlnXO_dT.*dlnXO_dp2.*X+dlnXO_dT.*dlnXO_dp.^2.*X).*XNO+(dlnXO_dp.*dlnXO_dpdT.*(8.*X+4)+dlnXO_dT.*dlnXO_dp2.*(4.*X+2)+dlnXO_dT.*dlnXO_dp.^2.*(4.*X+2)).*XN2+(dlnXO_dp.*dlnXO_dpdT.*(2.*X+2)+dlnXO_dT.*dlnXO_dp2.*(X+1)+dlnXO_dT.*dlnXO_dp.^2.*(X+1)).*XN).*XO+((dlnXNO_dp.*dlnXNO_dpdT.*(4.*X+4)+dlnXNO_dT.*dlnXNO_dp2.*(2.*X+2)+dlnXNO_dT.*dlnXNO_dp.^2.*(2.*X+2)+dlnXN2_dT.*dlnXN2_dp2.*(-X-1)+dlnXN2_dT.*dlnXN2_dp.^2.*(-X-1)+dlnXN2_dp.*dlnXN2_dpdT.*(-2.*X-2)).*XN2+(-2.*dlnXN_dp.*dlnXN_dpdT-dlnXN_dT.*dlnXN_dp2-dlnXN_dT.*dlnXN_dp.^2+2.*dlnXNO_dp.*dlnXNO_dpdT+dlnXNO_dT.*dlnXNO_dp2+dlnXNO_dT.*dlnXNO_dp.^2).*XN).*XNO+(4.*dlnXN_dp.*dlnXN_dpdT.*X+2.*dlnXN_dT.*dlnXN_dp2.*X+2.*dlnXN_dT.*dlnXN_dp.^2.*X-2.*dlnXN2_dp.*dlnXN2_dpdT.*X-dlnXN2_dT.*dlnXN2_dp2.*X-dlnXN2_dT.*dlnXN2_dp.^2.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXNO_dp2dT = -(((4.*dlnXO_dp.*dlnXO_dpdT+2.*dlnXO_dT.*dlnXO_dp2+2.*dlnXO_dT.*dlnXO_dp.^2-2.*dlnXO2_dp.*dlnXO2_dpdT-dlnXO2_dT.*dlnXO2_dp2-dlnXO2_dT.*dlnXO2_dp.^2).*XO+(dlnXNO_dp.*dlnXNO_dpdT.*(4.*X+4)+dlnXNO_dT.*dlnXNO_dp2.*(2.*X+2)+dlnXNO_dT.*dlnXNO_dp.^2.*(2.*X+2)).*XNO+(dlnXO2_dp.*dlnXO2_dpdT.*(8.*X+8)+dlnXN2_dp.*dlnXN2_dpdT.*(8.*X+8)+dlnXO2_dT.*dlnXO2_dp2.*(4.*X+4)+dlnXO2_dT.*dlnXO2_dp.^2.*(4.*X+4)+dlnXN2_dT.*dlnXN2_dp2.*(4.*X+4)+dlnXN2_dT.*dlnXN2_dp.^2.*(4.*X+4)).*XN2+(dlnXN_dp.*dlnXN_dpdT.*(4.*X+8)+dlnXO2_dp.*dlnXO2_dpdT.*(2.*X+4)+dlnXN_dT.*dlnXN_dp2.*(2.*X+4)+dlnXN_dT.*dlnXN_dp.^2.*(2.*X+4)+dlnXO2_dT.*dlnXO2_dp2.*(X+2)+dlnXO2_dT.*dlnXO2_dp.^2.*(X+2)).*XN).*XO2+((2.*dlnXNO_dp.*dlnXNO_dpdT.*X+dlnXNO_dT.*dlnXNO_dp2.*X+dlnXNO_dT.*dlnXNO_dp.^2.*X).*XNO+(dlnXO_dp.*dlnXO_dpdT.*(8.*X+4)+dlnXO_dT.*dlnXO_dp2.*(4.*X+2)+dlnXO_dT.*dlnXO_dp.^2.*(4.*X+2)+dlnXN2_dp.*dlnXN2_dpdT.*(4.*X+2)+dlnXN2_dT.*dlnXN2_dp2.*(2.*X+1)+dlnXN2_dT.*dlnXN2_dp.^2.*(2.*X+1)).*XN2+(dlnXO_dp.*dlnXO_dpdT.*(2.*X+2)+dlnXN_dp.*dlnXN_dpdT.*(2.*X+2)+dlnXO_dT.*dlnXO_dp2.*(X+1)+dlnXO_dT.*dlnXO_dp.^2.*(X+1)+dlnXN_dT.*dlnXN_dp2.*(X+1)+dlnXN_dT.*dlnXN_dp.^2.*(X+1)).*XN).*XO+((dlnXNO_dp.*dlnXNO_dpdT.*(4.*X+4)+dlnXNO_dT.*dlnXNO_dp2.*(2.*X+2)+dlnXNO_dT.*dlnXNO_dp.^2.*(2.*X+2)).*XN2+(2.*dlnXNO_dp.*dlnXNO_dpdT+dlnXNO_dT.*dlnXNO_dp2+dlnXNO_dT.*dlnXNO_dp.^2).*XN).*XNO+(4.*dlnXN_dp.*dlnXN_dpdT.*X+2.*dlnXN_dT.*dlnXN_dp2.*X+2.*dlnXN_dT.*dlnXN_dp.^2.*X-2.*dlnXN2_dp.*dlnXN2_dpdT.*X-dlnXN2_dT.*dlnXN2_dp2.*X-dlnXN2_dT.*dlnXN2_dp.^2.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXN2_dp2dT = -(((8.*dlnXO_dp.*dlnXO_dpdT+4.*dlnXO_dT.*dlnXO_dp2+4.*dlnXO_dT.*dlnXO_dp.^2-4.*dlnXO2_dp.*dlnXO2_dpdT-2.*dlnXO2_dT.*dlnXO2_dp2-2.*dlnXO2_dT.*dlnXO2_dp.^2).*XO+(dlnXNO_dp.*dlnXNO_dpdT.*(8.*X+8)+dlnXNO_dT.*dlnXNO_dp2.*(4.*X+4)+dlnXNO_dT.*dlnXNO_dp.^2.*(4.*X+4)+dlnXO2_dT.*dlnXO2_dp2.*(-2.*X-2)+dlnXO2_dT.*dlnXO2_dp.^2.*(-2.*X-2)+dlnXO2_dp.*dlnXO2_dpdT.*(-4.*X-4)).*XNO+(dlnXN2_dp.*dlnXN2_dpdT.*(16.*X+16)+dlnXN2_dT.*dlnXN2_dp2.*(8.*X+8)+dlnXN2_dT.*dlnXN2_dp.^2.*(8.*X+8)).*XN2+(dlnXN_dp.*dlnXN_dpdT.*(8.*X+16)+dlnXN_dT.*dlnXN_dp2.*(4.*X+8)+dlnXN_dT.*dlnXN_dp.^2.*(4.*X+8)).*XN).*XO2+((-4.*dlnXO_dp.*dlnXO_dpdT.*X-2.*dlnXO_dT.*dlnXO_dp2.*X-2.*dlnXO_dT.*dlnXO_dp.^2.*X+4.*dlnXNO_dp.*dlnXNO_dpdT.*X+2.*dlnXNO_dT.*dlnXNO_dp2.*X+2.*dlnXNO_dT.*dlnXNO_dp.^2.*X).*XNO+(dlnXN2_dp.*dlnXN2_dpdT.*(8.*X+4)+dlnXN2_dT.*dlnXN2_dp2.*(4.*X+2)+dlnXN2_dT.*dlnXN2_dp.^2.*(4.*X+2)).*XN2+(dlnXN_dp.*dlnXN_dpdT.*(4.*X+4)+dlnXN_dT.*dlnXN_dp2.*(2.*X+2)+dlnXN_dT.*dlnXN_dp.^2.*(2.*X+2)).*XN).*XO+((dlnXN2_dp.*dlnXN2_dpdT.*(4.*X+4)+dlnXN2_dT.*dlnXN2_dp2.*(2.*X+2)+dlnXN2_dT.*dlnXN2_dp.^2.*(2.*X+2)).*XN2+(4.*dlnXN_dp.*dlnXN_dpdT+2.*dlnXN_dT.*dlnXN_dp2+2.*dlnXN_dT.*dlnXN_dp.^2).*XN).*XNO)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dlnXO2_dp2dT = -(((dlnXO2_dp.*dlnXO2_dpdT.*(4.*X+4)+dlnXO2_dT.*dlnXO2_dp2.*(2.*X+2)+dlnXO2_dT.*dlnXO2_dp.^2.*(2.*X+2)).*XNO+(dlnXO2_dp.*dlnXO2_dpdT.*(16.*X+16)+dlnXO2_dT.*dlnXO2_dp2.*(8.*X+8)+dlnXO2_dT.*dlnXO2_dp.^2.*(8.*X+8)).*XN2+(dlnXO2_dp.*dlnXO2_dpdT.*(4.*X+8)+dlnXO2_dT.*dlnXO2_dp2.*(2.*X+4)+dlnXO2_dT.*dlnXO2_dp.^2.*(2.*X+4)).*XN).*XO2+((4.*dlnXO_dp.*dlnXO_dpdT.*X+2.*dlnXO_dT.*dlnXO_dp2.*X+2.*dlnXO_dT.*dlnXO_dp.^2.*X).*XNO+(dlnXO_dp.*dlnXO_dpdT.*(16.*X+8)+dlnXO_dT.*dlnXO_dp2.*(8.*X+4)+dlnXO_dT.*dlnXO_dp.^2.*(8.*X+4)).*XN2+(dlnXO_dp.*dlnXO_dpdT.*(4.*X+4)+dlnXO_dT.*dlnXO_dp2.*(2.*X+2)+dlnXO_dT.*dlnXO_dp.^2.*(2.*X+2)).*XN).*XO+((dlnXNO_dp.*dlnXNO_dpdT.*(8.*X+8)+dlnXNO_dT.*dlnXNO_dp2.*(4.*X+4)+dlnXNO_dT.*dlnXNO_dp.^2.*(4.*X+4)+dlnXN2_dT.*dlnXN2_dp2.*(-2.*X-2)+dlnXN2_dT.*dlnXN2_dp.^2.*(-2.*X-2)+dlnXN2_dp.*dlnXN2_dpdT.*(-4.*X-4)).*XN2+(-4.*dlnXN_dp.*dlnXN_dpdT-2.*dlnXN_dT.*dlnXN_dp2-2.*dlnXN_dT.*dlnXN_dp.^2+4.*dlnXNO_dp.*dlnXNO_dpdT+2.*dlnXNO_dT.*dlnXNO_dp2+2.*dlnXNO_dT.*dlnXNO_dp.^2).*XN).*XNO+(8.*dlnXN_dp.*dlnXN_dpdT.*X+4.*dlnXN_dT.*dlnXN_dp2.*X+4.*dlnXN_dT.*dlnXN_dp.^2.*X-4.*dlnXN2_dp.*dlnXN2_dpdT.*X-2.*dlnXN2_dT.*dlnXN2_dp2.*X-2.*dlnXN2_dT.*dlnXN2_dp.^2.*X).*XN.*XN2)./(((2.*X+2).*XNO+(8.*X+8).*XN2+(2.*X+4).*XN).*XO2+(X.*XNO+(4.*X+2).*XN2+(X+1).*XN).*XO+((2.*X+2).*XN2+XN).*XNO) ;

    dXN_dp2dT = dlnXN_dp2dT.*XN+2.*dXN_dp.*dXN_dpdT./XN+dXN_dT.*dXN_dp2./XN-2.*dXN_dT.*dXN_dp.^2./XN.^2 ;

    dXO_dp2dT = dlnXO_dp2dT.*XO+2.*dXO_dp.*dXO_dpdT./XO+dXO_dT.*dXO_dp2./XO-2.*dXO_dT.*dXO_dp.^2./XO.^2 ;

    dXNO_dp2dT = dlnXNO_dp2dT.*XNO+2.*dXNO_dp.*dXNO_dpdT./XNO+dXNO_dT.*dXNO_dp2./XNO-2.*dXNO_dT.*dXNO_dp.^2./XNO.^2 ;

    dXN2_dp2dT = dlnXN2_dp2dT.*XN2+2.*dXN2_dp.*dXN2_dpdT./XN2+dXN2_dT.*dXN2_dp2./XN2-2.*dXN2_dT.*dXN2_dp.^2./XN2.^2 ;

    dXO2_dp2dT = dlnXO2_dp2dT.*XO2+2.*dXO2_dp.*dXO2_dpdT./XO2+dXO2_dT.*dXO2_dp2./XO2-2.*dXO2_dT.*dXO2_dp.^2./XO2.^2 ;
end

%% Preparing outputs
switch derOrder
    case 1
        varargout{1} = [dXN_dT,dXO_dT,dXNO_dT,dXN2_dT,dXO2_dT]; % dXs_dT
        varargout{2} = [dXN_dp,dXO_dp,dXNO_dp,dXN2_dp,dXO2_dp]; % dXs_dp
    case 2
        varargout{1} = [dXN_dT,dXO_dT,dXNO_dT,dXN2_dT,dXO2_dT];             % dXs_dT
        varargout{2} = [dXN_dp,dXO_dp,dXNO_dp,dXN2_dp,dXO2_dp];             % dXs_dp
        varargout{3} = [dXN_dT2,dXO_dT2,dXNO_dT2,dXN2_dT2,dXO2_dT2];        % dXs_dT2
        varargout{4} = [dXN_dpdT,dXO_dpdT,dXNO_dpdT,dXN2_dpdT,dXO2_dpdT];   % dXs_dpdT
        varargout{5} = [dXN_dp2,dXO_dp2,dXNO_dp2,dXN2_dp2,dXO2_dp2];        % dXs_dp2
    case 3
        varargout{1} = [dXN_dT,dXO_dT,dXNO_dT,dXN2_dT,dXO2_dT];                 % dXs_dT
        varargout{2} = [dXN_dp,dXO_dp,dXNO_dp,dXN2_dp,dXO2_dp];                 % dXs_dp
        varargout{3} = [dXN_dT2,dXO_dT2,dXNO_dT2,dXN2_dT2,dXO2_dT2];            % dXs_dT2
        varargout{4} = [dXN_dpdT,dXO_dpdT,dXNO_dpdT,dXN2_dpdT,dXO2_dpdT];       % dXs_dpdT
        varargout{5} = [dXN_dp2,dXO_dp2,dXNO_dp2,dXN2_dp2,dXO2_dp2];            % dXs_dp2
        varargout{6} = [dXN_dT3,dXO_dT3,dXNO_dT3,dXN2_dT3,dXO2_dT3];            % dXs_dT3
        varargout{7} = [dXN_dpdT2,dXO_dpdT2,dXNO_dpdT2,dXN2_dpdT2,dXO2_dpdT2];  % dXs_dpdT2
        varargout{8} = [dXN_dp2dT,dXO_dp2dT,dXNO_dp2dT,dXN2_dp2dT,dXO2_dp2dT];  % dXs_dp2dT
        varargout{9} = [dXN_dp3,dXO_dp3,dXNO_dp3,dXN2_dp3,dXO2_dp3];            % dXs_dp3
end
