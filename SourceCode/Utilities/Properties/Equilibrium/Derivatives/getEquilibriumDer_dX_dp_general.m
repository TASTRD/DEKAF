function [dXs_dp] = getEquilibriumDer_dX_dp_general(X_s,X_E,p,mixCnst,varargin)
% getEquilibriumDer_dX_dp_general computes the pressure derivatives of
% the equilibrium composition using a general algorithm valid for whatever
% mixture.
%
% It differs from getEquilibriumDer_X_air5, getEquilibriumDer_X_air11, or
% getEquilibriumDer_X_oxygen2, in that it does not solve the system
% analytically, but rather numerically with simple matrix operations.
%
% Usage:
%   (1)
%       dXs_dp = getEquilibriumDer_dX_dp_general(X_s,X_E,p,mixCnst)
%
%   (2)
%       ... = getEquilibriumDer_dX_dp_general(...,'solveOnXs')
%       |--> solves the matrix system on dXs_dp, rather than on dlnXs_dp
%
% Inputs and outputs:
%   X_s         [-]
%       species mole fractions in equilibrium (N_eta x N_spec)
%   X_E         [-]
%       elemental atomic fraction (N_eta x N_el)
%   p           [Pa]
%       static pressure (N_eta x 1)
%   mixCnst
%       structure with the necessary mixture constants and properties
%   dXs_dp      [1/Pa]
%       pressure derivative of the eq. mole fractions (N_eta x N_spec)
%
% See also: getEquilibriumDer_X_air5, getEquilibriumDer_X_air11,
% getEquilibriumDer_X_oxygen2, getEquilibriumDer_X_Map
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,bSolveOnXs] = find_flagAndRemove('solveOnXs',varargin);

% Obtaining sizes
[~,N_el] = size(X_E);           % number of elements
[N_eta,N_spec] = size(X_s);     % number of species
N_reac = N_spec - N_el;         % number of reactions

% Extract fields from mixCnst struct
delta_nuEq      = mixCnst.delta_nuEq;       % stoichiometry of equilibrium reactions    [N_reac x N_spec]
spec_list       = mixCnst.spec_list;        % list of species                           [N_spec x 1]
elemStoich_mat  = mixCnst.elemStoich_mat;   % elemental stoichiometric matrix           [N_el x N_spec]

% Obtain coefficients for the elemental conservation equations
elConsCoeffEq = build_LTEElemCoeffs(elemStoich_mat,X_E,spec_list);          % [N_el-1 x N_spec x N_eta]
sum_delta_nuEq = delta_nuEq.' * ones(N_spec,1); % (r,1)

% Reshape inputs for 3D calculations
delta_nuEq_3D       = repmat(delta_nuEq.',    [1,1,N_eta]);     % (s,r) ----> (r,s,eta)
sum_delta_nuEq_3D   = repmat(sum_delta_nuEq, [1,1,N_eta]);      % (r,1) ----> (r,1,eta)
p_3D                = repmat(permute(p,[2,3,1]),[N_reac,1,1]);  % (eta,1) --> (r,1,eta)

b = [  -sum_delta_nuEq_3D./p_3D ;   % constitutive reactions (dissociation, ionization, etc.)
        zeros(N_el-1,1,N_eta)   ;   % elemental fraction conservation equations
        zeros(1,1,N_eta)        ;   % molar balance
     ]; % N_spec x 1 x N_eta

if bSolveOnXs                                                   % solving equation system on dXs_dp
    X_s3D = repmat(permute(X_s,[3,2,1]),[N_reac,1,1]);                  % (eta,s) --> (r,s,eta)

    % Building the matrices composing the equation system A*dlnXdT=b
    A = [   delta_nuEq_3D./X_s3D    ;   % constitutive reactions (dissociation, ionization, etc.)
            elConsCoeffEq           ;   % elemental fraction conservation equations
            ones(1,N_spec,N_eta)    ;   % molar balance
        ]; % N_spec x N_spec x N_eta

    % Solving system at each location
    %%%% BETTER ALGORITHM GOES HERE
    for ii=N_eta:-1:1
        dXs_dp(:,ii) = A(:,:,ii)\b(:,:,ii);
    end
    %%%%
    dXs_dp = dXs_dp.';
else                                                            % solving equation system on dlnXs_dp
    X_s3D_1 = repmat(permute(X_s,[3,2,1]),[N_el-1,1,1]);                % (eta,s) --> (el,s,eta)
    X_s3D_2 = permute(X_s,[3,2,1]);                                     % (eta,s) --> (1,s,eta)

    % Building the matrices composing the equation system A*dlnXdT=b
    A = [   delta_nuEq_3D               ;   % constitutive reactions (dissociation, ionization, etc.)
            elConsCoeffEq.*X_s3D_1      ;   % elemental fraction conservation equations
            X_s3D_2                     ;   % molar balance
        ]; % N_spec x N_spec x N_eta

    % Solving system at each location
    %%%% BETTER ALGORITHM GOES HERE
    for ii=N_eta:-1:1
        dlnXs_dp(:,ii) = A(:,:,ii)\b(:,:,ii);
    end
    %%%%
    dXs_dp = X_s .* dlnXs_dp.';
end

end % getEquilibriumDer_dX_dp_general