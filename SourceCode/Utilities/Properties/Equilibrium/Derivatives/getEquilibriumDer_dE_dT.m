function dE_dT = getEquilibriumDer_dE_dT(TTrans,rho,y_s,dXs_dT,D_sl,Mm_s,bPos,bElectron,qEl,nAvogadro,kBoltz)
% getEquilibriumDer_dE_dT computes the temperature gradient of the
% ambipolar electric field in equilibrium.
%
% Usage:
%   (1)
%       dE_dT = getEquilibriumDer_dE_dT(TTrans,rho,y_s,dXs_dT,D_sl,Mm_s,bPos,bElectron,qEl,nAvogadro,kBoltz)
%
% Inputs and outputs:
%   TTrans
%       translational temperature (N_eta x 1)
%   rho
%       mixture density (N_eta x 1)
%   dXs_dT
%       temperature variation of the equilibrium mole fractions (N_eta x N_spec)
%   y_s
%       mass fractions (N_eta x N_spec)
%   D_sl
%       multicomponent diffusion coefficients (N_eta x N_spec(s) x N_spec(l))
%   Mm_s
%       mixture molar mass (N_spec x 1)
%   bPos
%       positively-charged-species boolean (N_spec x 1)
%   bElectron
%       electron boolean (N_spec x 1)
%   qEl
%       charge of an electron (1 x 1)
%   nAvogadro
%       Avogadro's number (1 x 1)
%   kBoltz
%       Boltzmann constant (1 x 1)
%   dE_dT
%       Ambipolar electric field temperature gradient (N_eta x 1)
%
% References:
%   () Scoggins, J. B. (2017). Development of numerical methods and study
%   of coupled flow, radiation, and ablation phenomena for atmospheric
%   entry. Université Paris-Saclay and VKI.
%
% Author(s): Fernando Miro Miro
% Date: May 2019
% GNU Lesser General Public License 3.0

[N_eta,N_spec] = size(y_s);

% computing auxiliary properties
kCharge_s   = getSpecies_kCharge(TTrans,y_s,Mm_s,bPos,bElectron,qEl,kBoltz);
n_s         = getSpecies_n(rho,y_s,Mm_s,nAvogadro);
ZCharge     = getSpecies_ZCharge(bPos,bElectron);

% reshaping
dXl_dT_3D    = repmat(permute(dXs_dT   ,[1,3,2]),[1,N_spec,1]); % (eta,s) --> (eta,s,l)
kCharge_l_3D = repmat(permute(kCharge_s,[1,3,2]),[1,N_spec,1]); % (eta,l) --> (eta,s,l)
ZCharge_s_2D = repmat(ZCharge.',[N_eta,1]);                     % (s,1) --> (eta,s)

% computing sums and assembling
sum1_s = sum(D_sl.*dXl_dT_3D,3);                                % suming over l (eta,s)
sum2_s = sum(D_sl.*kCharge_l_3D,3);                             % suming over l (eta,s)

numer = (n_s .* ZCharge_s_2D .* sum1_s) * ones(N_spec,1);       % summing over s (eta,1)
denom = (n_s .* ZCharge_s_2D .* sum2_s) * ones(N_spec,1);       % summing over s (eta,1)

dE_dT = numer./denom;                                           % assembling

end % getEquilibriumDer_dE_dT