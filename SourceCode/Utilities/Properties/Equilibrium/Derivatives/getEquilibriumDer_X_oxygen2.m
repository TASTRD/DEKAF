function varargout = getEquilibriumDer_X_oxygen2(derOrder,X_s,p,spec_list,varargin)
% getEquilibriumDer_X_oxygen2 computes the derivatives of the equilibrium mole
% fractions with pressure and temperature for an oxygen2 mixture.
%
%  Examples:
%    (1)    [dXs_dT,dXs_dp] = getEquilibriumDer_X_oxygen2(1,X_s,p,spec_list,...
%                                                           reac_list,dlnKp_dT)
%    (2)    [dXs_dT,dXs_dp,dXs_dT2,dXs_dpdT,dXs_dp2] = ...
%                   getEquilibriumDer_X_oxygen2(2,X_s,p,spec_list,reac_list,...
%                                                           dlnKp_dT,dlnKp_dT2)
%    (3)    [dXs_dT,dXs_dp,dXs_dT2,dXs_dpdT,dXs_dp2,dXs_dT3,dXs_dpdT2, ...
%            dXs_dp2dT,dXs_dp3] = getEquilibriumDer_X_oxygen2(3,X_s,p, ...
%                               spec_list,reac_list,dlnKp_dT,dlnKp_dT2,dlnKp_dT3)
%
% The user must specify the maximum order of the derivatives to be computed
% through derOrder:
%       derOrder = 1    -   dXs_dT & dXs_dp
%       derOrder = 2    -   dXs_dT2, dXs_dpdT & dXs_dp2 (also)
%       derOrder = 3    -   dXs_dT3, dXs_dpdT2, dXs_dp2dT & dXs_dp3 (also)
%
% The other required inputs are:
%       X_s         -   mole fractions                  [-]  size (N x 2)
%       p           -   pressure                        [Pa] size (N x 1)
%       spec_list   -   list of species                 [-]  size (2 x 1)
%       dlnKp_dT    -   temperature gradient of the natural logarithms of
%                       the equilibrium constants       [Pa] size (N x 1)
%       dlnKp_dT2   -   second temperature gradient of the natural logarithms
%                       of the equilibrium constants    [Pa] size (N x 1)
%       dlnKp_dT3   -   third temperature gradient of the natural logarithms
%                       of the equilibrium constants    [Pa] size (N x 1)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

%% Extracting inputs
XO = X_s(:,strcmp(spec_list,'O'));
XO2 = X_s(:,strcmp(spec_list,'O2'));

switch derOrder
    case 1
        dlnKO_dT    = varargin{1};
    case 2
        dlnKO_dT    = varargin{1};
        dlnKO_dT2   = varargin{2};
    case 3
        dlnKO_dT    = varargin{1};
        dlnKO_dT2   = varargin{2};
        dlnKO_dT3   = varargin{3};
    otherwise
        error('wrong input derOrder');
end

%% Computing derivatives (expressions obtained from maxima using derivatives.wxm)

if derOrder>0

    % Pressure 1st-order derivatives ;

    dlnXO_dp = -XO2./(p.*(2.*XO2+XO)) ;

    dlnXO2_dp = XO./(p.*(2.*XO2+XO)) ;

    dXO_dp = dlnXO_dp.*XO ;

    dXO2_dp = dlnXO2_dp.*XO2 ;

    % Temperature 1st-order derivatives ;

    dlnXO_dT = dlnKO_dT.*XO2./(2.*XO2+XO) ;

    dlnXO2_dT = -dlnKO_dT.*XO./(2.*XO2+XO) ;

    dXO_dT = dlnXO_dT.*XO ;

    dXO2_dT = dlnXO2_dT.*XO2 ;
end

if derOrder>1

    % Pressure 2nd-order derivatives ;

    dlnXO_dp2 = -(p.^2.*(dlnXO2_dp.^2.*XO2+dlnXO_dp.^2.*XO)-XO2)./(p.^2.*(2.*XO2+XO)) ;

    dlnXO2_dp2 = -(p.^2.*(2.*dlnXO2_dp.^2.*XO2+2.*dlnXO_dp.^2.*XO)+XO)./(p.^2.*(2.*XO2+XO)) ;

    dXO_dp2 = dlnXO_dp2.*XO ;

    dXO2_dp2 = dlnXO2_dp2.*XO2 ;

    % Temperature 2nd-order derivatives ;

    dlnXO_dT2 = (-dlnXO2_dT.^2.*XO2+dlnKO_dT2.*XO2-dlnXO_dT.^2.*XO)./(2.*XO2+XO) ;

    dlnXO2_dT2 = -(2.*dlnXO2_dT.^2.*XO2+2.*dlnXO_dT.^2.*XO+dlnKO_dT2.*XO)./(2.*XO2+XO) ;

    dXO_dT2 = dlnXO_dT2.*XO ;

    dXO2_dT2 = dlnXO2_dT2.*XO2 ;

    % Pressure-temperature crossed derivatives ;

    dlnXO_dpdT = -(dlnXO2_dT.*dlnXO2_dp.*XO2+dlnXO_dT.*dlnXO_dp.*XO)./(2.*XO2+XO) ;

    dlnXO2_dpdT = -(2.*dlnXO2_dT.*dlnXO2_dp.*XO2+2.*dlnXO_dT.*dlnXO_dp.*XO)./(2.*XO2+XO) ;

    dXO_dpdT = dlnXO_dpdT.*XO ;

    dXO2_dpdT = dlnXO2_dpdT.*XO2 ;
end

if derOrder>2

    % Pressure 3rd-order derivatives ;

    dlnXO_dp3 = -(p.^3.*((3.*dlnXO2_dp.*dlnXO2_dp2+dlnXO2_dp.^3).*XO2+(3.*dlnXO_dp.*dlnXO_dp2+dlnXO_dp.^3).*XO)+2.*XO2)./(p.^3.*(2.*XO2+XO)) ;

    dlnXO2_dp3 = -(p.^3.*((6.*dlnXO2_dp.*dlnXO2_dp2+2.*dlnXO2_dp.^3).*XO2+(6.*dlnXO_dp.*dlnXO_dp2+2.*dlnXO_dp.^3).*XO)-2.*XO)./(p.^3.*(2.*XO2+XO)) ;

    dXO_dp3 = dlnXO_dp3.*XO ;

    dXO2_dp3 = dlnXO2_dp3.*XO2 ;

    % Temperature 3rd-order derivatives ;

    dlnXO_dT3 = ((-3.*dlnXO2_dT.*dlnXO2_dT2-dlnXO2_dT.^3).*XO2+dlnKO_dT3.*XO2+(-3.*dlnXO_dT.*dlnXO_dT2-dlnXO_dT.^3).*XO)./(2.*XO2+XO) ;

    dlnXO2_dT3 = -((6.*dlnXO2_dT.*dlnXO2_dT2+2.*dlnXO2_dT.^3).*XO2+(6.*dlnXO_dT.*dlnXO_dT2+2.*dlnXO_dT.^3).*XO+dlnKO_dT3.*XO)./(2.*XO2+XO) ;

    dXO_dT3 = dlnXO_dT3.*XO ;

    dXO2_dT3 = dlnXO2_dT3.*XO2 ;

    % Pressure-temperature(x2) crossed derivatives ;

    dlnXO_dpdT2 = -((2.*dlnXO2_dT.*dlnXO2_dpdT+(dlnXO2_dT2+dlnXO2_dT.^2).*dlnXO2_dp).*XO2+(2.*dlnXO_dT.*dlnXO_dpdT+(dlnXO_dT2+dlnXO_dT.^2).*dlnXO_dp).*XO)./(2.*XO2+XO) ;

    dlnXO2_dpdT2 = -((4.*dlnXO2_dT.*dlnXO2_dpdT+(2.*dlnXO2_dT2+2.*dlnXO2_dT.^2).*dlnXO2_dp).*XO2+(4.*dlnXO_dT.*dlnXO_dpdT+(2.*dlnXO_dT2+2.*dlnXO_dT.^2).*dlnXO_dp).*XO)./(2.*XO2+XO) ;

    dXO_dpdT2 = dlnXO_dpdT2.*XO ;

    dXO2_dpdT2 = dlnXO2_dpdT2.*XO2 ;

    % Pressure(x2)-temperature crossed derivatives ;

    dlnXO_dp2dT = -((2.*dlnXO2_dp.*dlnXO2_dpdT+dlnXO2_dT.*dlnXO2_dp2+dlnXO2_dT.*dlnXO2_dp.^2).*XO2+(2.*dlnXO_dp.*dlnXO_dpdT+dlnXO_dT.*dlnXO_dp2+dlnXO_dT.*dlnXO_dp.^2).*XO)./(2.*XO2+XO) ;

    dlnXO2_dp2dT = -((4.*dlnXO2_dp.*dlnXO2_dpdT+2.*dlnXO2_dT.*dlnXO2_dp2+2.*dlnXO2_dT.*dlnXO2_dp.^2).*XO2+(4.*dlnXO_dp.*dlnXO_dpdT+2.*dlnXO_dT.*dlnXO_dp2+2.*dlnXO_dT.*dlnXO_dp.^2).*XO)./(2.*XO2+XO) ;

    dXO_dp2dT = dlnXO_dp2dT.*XO ;

    dXO2_dp2dT = dlnXO2_dp2dT.*XO2 ;
end

%% Preparing outputs
switch derOrder
    case 1
        varargout{1} = [dXO_dT,dXO2_dT];        % dXs_dT
        varargout{2} = [dXO_dp,dXO2_dp];        % dXs_dp
    case 2
        varargout{1} = [dXO_dT,dXO2_dT];        % dXs_dT
        varargout{2} = [dXO_dp,dXO2_dp];        % dXs_dp
        varargout{3} = [dXO_dT2,dXO2_dT2];      % dXs_dT2
        varargout{4} = [dXO_dpdT,dXO2_dpdT];    % dXs_dpdT
        varargout{5} = [dXO_dp2,dXO2_dp2];      % dXs_dp2
    case 3
        varargout{1} = [dXO_dT,dXO2_dT];        % dXs_dT
        varargout{2} = [dXO_dp,dXO2_dp];        % dXs_dp
        varargout{3} = [dXO_dT2,dXO2_dT2];      % dXs_dT2
        varargout{4} = [dXO_dpdT,dXO2_dpdT];    % dXs_dpdT
        varargout{5} = [dXO_dp2,dXO2_dp2];      % dXs_dp2
        varargout{6} = [dXO_dT3,dXO2_dT3];      % dXs_dT3
        varargout{7} = [dXO_dpdT2,dXO2_dpdT2];  % dXs_dpdT2
        varargout{8} = [dXO_dp2dT,dXO2_dp2dT];  % dXs_dp2dT
        varargout{9} = [dXO_dp3,dXO2_dp3];      % dXs_dp3
end
