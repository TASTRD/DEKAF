function dys_dq = getEquilibriumDer_dys_dq(X_s,dXs_dq,Mm_s)
% getEquilibriumDer_dys_dq computes the first derivative of the equilibrium
% mass fractions with respect to an arbitrary variable q.
%
% Usage:
%   (1)
%       dys_dq = getEquilibriumDer_dys_dq(X_s,dXs_dq,Mm_s)
%
%   X_s     -   size (N_eta x N_spec)
%   dXs_dq  -   size (N_eta x N_spec x N_q)
%   Mm_s    -   size (N_spec x 1)
%   dys_dq  -   size (N_eta x N_spec x N_q)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_eta,N_spec,N_q] = size(dXs_dq);

Mm      = getMixture_Mm_fromXs(X_s,Mm_s);   % (eta,1)

Mm_s    = repmat(Mm_s.',[N_eta,1,N_q]);     % (s,1) ------> (eta,s,q)

dMm_dq  = sum(Mm_s.*dXs_dq,2);              % (eta,1,q)

X_s     = repmat(X_s,   [1,1,N_q]);         % (eta,s) ----> (eta,s,q)
Mm      = repmat(Mm,    [1,N_spec,N_q]);    % (eta,1) ----> (eta,s,q)
dMm_dq  = repmat(dMm_dq,[1,N_spec,1]);      % (eta,1,q) --> (eta,s,q)

dys_dq = -Mm_s./Mm.^2.*X_s.*dMm_dq + Mm_s./Mm .* dXs_dq;

end % getEquilibriumDer_dys_dq