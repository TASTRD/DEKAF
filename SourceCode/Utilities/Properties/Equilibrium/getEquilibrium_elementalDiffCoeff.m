function [A_E,C_EF] = getEquilibrium_elementalDiffCoeff(bPairDiff,dXYs_dT,dXYs_dyE,D_s,rho,y_s,Mm_s,Mm_E,elemStoich_mat)
% getEquilibrium_elementalDiffCoeff computes the elemental diffusion
% coefficients for a flow in LTEED.
%
% Usage:
%   (1) ---> for species-to-species molar diffusion
%       [A_E,C_EF] = getEquilibrium_elementalDiffCoeff(1,dXs_dT,dXs_dyE,D_sl,rho,y_s,Mm_s,Mm_E,elemStoich_mat)
%
%   (2) ---> for species-to-species mass diffusion
%       [A_E,C_EF] = getEquilibrium_elementalDiffCoeff(1,dys_dT,dys_dyE,D_sl,rho,y_s,Mm_s,Mm_E,elemStoich_mat)
%
%   (3) ---> for species-to-mixture molar diffusion
%       [A_E,C_EF] = getEquilibrium_elementalDiffCoeff(0,dXs_dT,dXs_dyE,D_s, rho,[], Mm_s,Mm_E,elemStoich_mat)
%
%   (4) ---> for species-to-mixture mass diffusion
%       [A_E,C_EF] = getEquilibrium_elementalDiffCoeff(0,dys_dT,dys_dyE,D_s, rho,[], Mm_s,Mm_E,elemStoich_mat)
%
% Inputs and outputs:
%   dXs_dT          [1/K]                   N_eta x N_spec
%       gradient of the equilibrium mole fractions of each species (s) with
%       temperature
%   dys_dT          [1/K]                   N_eta x N_spec
%       gradient of the equilibrium mass fractions of each species (s) with
%       temperature
%   dXs_dyE         [-]                     N_eta x N_spec x N_elem
%       gradient of the equilibrium mole fraction of each species (s) with
%       each elemental mass fracion (E)
%   dys_dyE         [-]                     N_eta x N_spec x N_elem
%       gradient of the equilibrium mass fraction of each species (s) with
%       each elemental mass fracion (E)
%   D_s             [m^2/s]                 N_eta x N_spec
%       effective diffusion coefficient of species s in the mixture
%   D_sl            [m^2/s]                 N_eta x N_spec x N_spec
%       multicomponent diffusion coefficient of species s in species l
%   rho             [kg/m^3]                N_eta x 1
%       mixture density
%   y_s             [-]                     N_eta x N_spec
%       species mass fractions
%   Mm_s            [kg/mol]                N_spec x 1
%       species molar mass
%   Mm_E            [kg/mol]                N_elem x 1
%       elemental molar mass
%   elemStoich_mat  [-]                     N_elem x N_spec
%       elemental stoichiometric matrix (number of each element E contained
%       in species s
%   A_E             [kg/m-s-K]              N_eta x N_elem
%       elemental thermodiffusion coefficient of each element E
%   C_EF            [kg/m-s]                N_eta x N_elem(E) x N_elem(F)
%       elemental molar diffusion coefficient of each element E into
%       element F.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_eta,N_spec,N_elem] = size(dXYs_dyE);

% Computing corrected elemental stoichiometric matrix
Mm_s2D = repmat(Mm_s.',[N_elem,1]);             % (s,1) --> (E,s)
Mm_E2D = repmat(Mm_E,  [1,N_spec]);             % (E,1) --> (E,s)
Ecorr_Es = elemStoich_mat .* Mm_E2D./Mm_s2D;    % (E,s)

% repmatt'ing density
rho_2D = repmat(rho,[1,N_elem]);        % (eta,1) ----> (eta,E)
rho_3D = repmat(rho,[1,N_elem,N_elem]); % (eta,1) ----> (eta,E,F)

if bPairDiff % spec-spec diffusion
    % Thermodiffusion coefficient
    y_s_4D      = repmat(permute(y_s,       [1,3,2,4]),[1,    N_elem,1,     N_spec]); % (eta,s) ----> (eta,E,s,l)
    D_sl_4D     = repmat(permute(D_s,       [1,4,2,3]),[1,    N_elem,1,     1]);      % (eta,s,l) --> (eta,E,s,l)
    dXYl_dT_4D  = repmat(permute(dXYs_dT,   [1,3,4,2]),[1,    N_elem,N_spec,1]);      % (eta,l) ----> (eta,E,s,l)
    Ecorr_Es_4D = repmat(permute(Ecorr_Es,  [3,1,2,4]),[N_eta,1,     1,     N_spec]); % (E,s) ------> (eta,E,s,l)
    
    A_E = rho_2D .* sum(sum(Ecorr_Es_4D.*y_s_4D.*D_sl_4D.*dXYl_dT_4D , 3) , 4);
    
    % Molar-diffusion coefficient
    y_s_5D      = repmat(permute(y_s,       [1,3,5,2,4]),[1,    N_elem,N_elem,1,     N_spec]); % (eta,s) ----> (eta,E,F,s,l)
    D_sl_5D     = repmat(permute(D_s,       [1,4,5,2,3]),[1,    N_elem,N_elem,1,     1]);      % (eta,s,l) --> (eta,E,F,s,l)
    dXYl_dyF_5D = repmat(permute(dXYs_dyE,  [1,4,3,5,2]),[1,    N_elem,1,     N_spec,1]);      % (eta,l,F) --> (eta,E,F,s,l)
    Ecorr_Es_5D = repmat(permute(Ecorr_Es,  [3,1,5,2,4]),[N_eta,1,     N_elem,1,     N_spec]); % (E,s) ------> (eta,E,F,s,l)
    
    C_EF = rho_3D .* sum(sum(Ecorr_Es_5D.*y_s_5D.*D_sl_5D.*dXYl_dyF_5D , 4) , 5);
else % spec-mix diffusion
    % Thermodiffusion coefficient
    D_s_3D      = repmat(permute(D_s,       [1,3,2]),[1,    N_elem,1]);  % (eta,s) ----> (eta,E,s)
    dXYs_dT_3D  = repmat(permute(dXYs_dT,   [1,3,2]),[1,    N_elem,1]);  % (eta,s) ----> (eta,E,s)
    Ecorr_Es_3D = repmat(permute(Ecorr_Es,  [3,1,2]),[N_eta,1,     1]);  % (E,s) ------> (eta,E,s)
    
    A_E = rho_2D .* sum(Ecorr_Es_3D.*D_s_3D.*dXYs_dT_3D , 3);
    
    % Molar-diffusion coefficient
    D_sl_4D     = repmat(permute(D_s,       [1,3,4,2]),[1,    N_elem,N_elem,1,    ]); % (eta,s) ----> (eta,E,F,s)
    dXYs_dyF_4D = repmat(permute(dXYs_dyE,  [1,4,3,2]),[1,    N_elem,1,     1     ]); % (eta,s,F) --> (eta,E,F,s)
    Ecorr_Es_4D = repmat(permute(Ecorr_Es,  [3,1,4,2]),[N_eta,1,     N_elem,1     ]); % (E,s) ------> (eta,E,F,s)
    
    C_EF = rho_3D .* sum(Ecorr_Es_4D.*D_sl_4D.*dXYs_dyF_4D , 4);
end

end % getEquilibrium_elementalDiffCoeff