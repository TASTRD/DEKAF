function a_eq = getEquilibrium_a(T,y_s,p,mixCnst,options)
% getEquilibrium_a returns the equilibrium speed of sound.
%
% Usage:
%   (1)
%       gam_eq = getEquilibrium_gam(T,y_s,p,mixCnst,options)
%
% Inputs:
%   T           [K]             N_eta x 1
%       temperature
%   y_s         [-]             N_eta x N_spec
%       equilibrium species mass fractions
%   p           [Pa]            N_eta x 1
%       pressure
%   mixCnst
%       structure with the necessary mixture constants
%   options
%       classic DEKAF options structure
%
% Outputs:
%   a_eq        [m/s]           N_eta x 1
%       equilibrium speed of sound
%
% Children and relation functions:
%   See also getEquilibrium_gam, listOfVariablesExplained 
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

Mm_s = mixCnst.Mm_s;
R0   = mixCnst.R0;

% obtaining specific heat ratio
gam_eq = getEquilibrium_gam(T,y_s,p,mixCnst,options);
% obtaining gas constant
[~,R] = getMixture_Mm_R(y_s,T,T,Mm_s,R0,zeros(size(Mm_s)));
% speed of sound
a_eq = sqrt(gam_eq.*R.*T);

end % getEquilibrium_a
