function y_s = getEquilibrium_ys(X_s,Mm_s)
% getEquilibrium_ys computes the equilibrium mass fractions given the mole
% fractions and the molar weights
%
% It can also work with elemental quantities.
%
% Examples:
%   y_s = getEquilibrium_ys(X_s,Mm_s)
%
%       X_s     -   size (N_eta x N_spec)
%       Mm_s    -   size (N_spec x 1)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% converting sizes
[N_eta,N_spec] = size(X_s);
Mm_s = repmat(Mm_s',[N_eta,1]);

denom = (Mm_s.*X_s) * ones(N_spec,1);   % denominator of the expression of y_s
denom = repmat(denom,[1,N_spec]);       % repeating over s
y_s = Mm_s.*X_s./denom;                 % computing mass fractions