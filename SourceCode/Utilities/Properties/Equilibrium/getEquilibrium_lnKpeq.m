function lnKpeq_r = getEquilibrium_lnKpeq(lnKeq_r,R0,T,delta_nu)
% getEquilibrium_lnKpeq computes the natural logs of the equilibrium p
% constants.
%
%   Examples:
%      (1) lnKpeq_r = getEquilibrium_lnKpeq(lnKeq_r,R0,T,delta_nu)
%
%   Inputs:
%      lnKeq_r      size (N_eta x N_reac)
%      R0           size 1
%      T            size (N_eta x 1)
%      delta_nu     size (N_reac x 1)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[N_eta,N_reac] = size(lnKeq_r);
T = repmat(T,[1,N_reac]);
delta_nu = repmat(delta_nu',[N_eta,1]);

lnKpeq_r = lnKeq_r + delta_nu.*log(R0*T);