function [LTEtablePath,XEq] = checkCompositionLTE(ys_e,Mm_s,R0,mixture,options)
% checkCompositionLTE is a function to check if the chosen mixture
% composition is currently available in DEKAF, and if not it prompts an
% error.
%
% This function assumes that the freestream is in thermal equilibrium, and
% that therefore the mixture molar mass can be obtained without the
% translational and electron temperatures.
%
% It also takes the average (along x) of the edge elemental fractions. A
% varying edge elemental composition is not supported by DEKAF.
%
%   Usage:
%       (1)     LTEtablePath = checkCompositionLTE(ys_e,Mm_s,R0,mixture,options)
%
%       (2)     [LTEtablePath,XEq] = checkCompositionLTE(ys_e,Mm_s,R0,mixture,options)
%           |--> returns also the ratios of elemental compositions as
%           required for the chosen mixture
%
%   Inputs & outputs:
%       - ys_e          cell size N_spec x 1, with fields N_x x 1
%                       or matrix size N_x x N_spec
%       - Mm_s          vector size N_spec x 1
%       - R0            single value size 1 x 1
%       - mixture       string
%       - LTETablePath  path to the file with the LTE tables needed to run
%                       this composition
%       - XEq           ratios of elemental fractions size N_elem - 1
%
% Examples
%   air11:
%   [LTEtablePath,XEq] = ...
%     checkCompositionLTE({0,0,0,0,0,0,0.767,0,0.232,0,0},Mm_s,...
%     R0,mixture,options);
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if iscell(ys_e)
    y_se = cell1D2matrix2D(ys_e);
else
    y_se = ys_e;
end

% obtaining mixture properties
[~,~,~,~,~,~,~,spec_list,~,~,~] = getMixture_constants(mixture,101325,R0,options);
[elem_list,elemStoich_mat] = getElement_info(spec_list);
idx_notEl = ~strcmp(elem_list,'e-'); % elements excluding electrons (not included in the LTE tablepath search
elem_list_notEl = elem_list(idx_notEl); % list of non-fictitious elements
N_elem = length(elem_list_notEl);

% obtaining elemental mole fractions
X_E = getElement_XE_from_ys(y_se,Mm_s,elemStoich_mat,options.bChargeNeutrality,elem_list); % elemental fractions
X_ee = mean(X_E(:,idx_notEl),1);                                            % taking the mean over all x locations, but excluding electrons
XEq = mean(X_E,1);

% building string of elemental compositions (used for warning and error
% messages)
elem_str = getElement_stringFancy(X_ee,elem_list_notEl);

% defining table path and searching it
[LTEtablePathPrefix,LTEtablePathPrePrefix] = getLTE_tablePath(mixture,X_ee,elem_list_notEl); % obtaining required file path
folderInfo = what('LTETablesEtc');                                          % getting information of the LTETablesEtc folder
if length(folderInfo)>2
    error('there is more than one LTETablesEtc in matlabs working paths. Check by running ''path'' in the workspace which one does not correspond to DEKAF and remove it.');
end
matFiles = folderInfo.mat;                                                  % list of mat files in LTETablesEtc
matchesFull = ~cellfun(@isempty,regexp(matFiles,['^',LTEtablePathPrefix,'*'],'once')); % looking for files starting with the composition we need
matchesMix = ~cellfun(@isempty,regexp(matFiles,['^',LTEtablePathPrePrefix,'*'],'once')); % looking for files starting with the mixture we need
matchedFilesFull = matFiles(matchesFull );                                  % list of matched files (full match)
matchedFilesMix = matFiles(matchesMix );                                    % list of matched files (mixture match)
if isempty(matchedFilesFull) && ~isempty(matchedFilesMix)                   % if there aren't any files with the exact composition we want
    % obtaining the elemental fractions used for each file
    N_match = length(matchedFilesMix);                                          % number of matched files
    Xe_match = zeros(length(matchedFilesMix),N_elem);                           % allocating matched elemental compositions
    for ii=1:N_match                                                            % looping the files with the same mixture
        noPrefix = matchedFilesMix{ii}(length(LTEtablePathPrePrefix)+1:end);        % removing the preprefix
        posLowerbar = strfind(noPrefix,'_');                                        % locating lower bars
        elemPercent{ii} = noPrefix(1:posLowerbar(1));                               % cutting off the end of the file name (pressures, temperatures, etc.)
        [elem_match,idx_elem] = regexp(elemPercent{ii},strjoin(elem_list_notEl,'|'),'match'); % positions where the element names appear
        N_elemPercent = length(elemPercent{ii});                                    % length of the element string with the percentages
        idx_elem = [idx_elem,N_elemPercent];                                        % appending the length of the element string to the locations of the elements in it
        for jj=1:length(elem_match)                                                 % looping matched elements and storing elemental fractions after them
            Xe_match(ii,strcmp(elem_list_notEl,elem_match{jj})) = str2double(elemPercent{ii}(idx_elem(jj)+length(elem_match{jj}):idx_elem(jj+1)-1))/100;
        end                                                                         % closing elment loop
    end                                                                         % closing file loop
    X_ee_mat = repmat(X_ee,[N_match,1]);                                        % repmatting to make it the same size as Xe_match
    diff_X = sum((X_ee_mat - Xe_match).^2,2);                                   % difference between all matched files and the required composition
    [~,idx_best] = min(diff_X);                                                 % keeping the file with the minimum difference
    LTEtablePath = matchedFilesMix{idx_best};
    elem_str_taken = getElement_stringFancy(Xe_match(idx_best,:),elem_list_notEl); % building string for the warning message
    warning(['The exact chosen elemental composition - ',elem_str,' was not found. The closest was taken (',elem_str_taken(1:end-1),...
        ') for the LTE initial guesses. If it fails to converge, consider generating a new table with SourceCode/Utilities/LTETablesEtc/buildLTETables.']);
elseif isempty(matchedFilesMix)                                             % if there aren't any files with the mixture we want, break
    errorMsg = ['the needed LTE table file for the chosen elemental composition ',elem_str,...;
    'was not found. Neither was another file with the same list of elements.',newline,...
    'Run SourceCode/Utilities/LTETablesEtc/build_LTETables with this composition.'];
    error(errorMsg);
else
    LTEtablePath = matchedFilesFull{1};
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function elem_str = getElement_stringFancy(X_ee,elem_list)
% getElement_string returns a fancy expression for the elemental
% composition.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

elem_str = '';
for e=1:length(elem_list)
    elem_str = [elem_str,num2str(X_ee(e)*100,3),'% ',elem_list{e},' '];
end