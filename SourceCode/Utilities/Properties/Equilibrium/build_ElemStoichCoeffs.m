function stoichCoeffs = build_ElemStoichCoeffs(elem,spec_list)
%build_ElemStoichCoeffs Generate a row of stoichiometric coefficients of an
% element with respect to a list of species in a chemical mixture.
%
% Inputs
%   elem            string representing a species in the chemical mixture
%   spec_list       cell array of strings of all the species in the mixture
%
% Output
%   stoichCoeffs    vector of integers representing the stoichiometric
%                   coefficients of the spec within the spec_list, making
%                   this vector's length = length(spec_list)
%
% Examples
%
% |---> Count the stoichiometry of atomic nitrogen in a mixture of N2 & O2
%       products with N, NO singly-ionized.
%
%   elem = 'N';
%   spec_list = {'N','O','NO','N2','O2','N+','NO+','e-'};
%   stoichCoeffs = build_ElemStoichCoeffs(elem,spec_list)
%       stoichCoeffs = [1 0 1 2 0 1 1 0];
%
% |---> Count the stoichiometry of ionized species in a mixture of N2 & O2
%       products with N, NO singly-ionized, and NO doubly-ionized.
%
%   elem = '+';
%   spec_list = {'N','O','NO','N2','O2','N+','NO+','NO+2','e-'};
%   stoichCoeffs = build_ElemStoichCoeffs(elem,spec_list)
%       stoichCoeffs = [0 0 0 0 0 1 1 2 0];
%
% |---> Count the stoichiometry of free eletrons in a mixture of N2 & O2
%       products with N, NO singly-ionized.
%
%   elem = '-';
%   spec_list = {'N','O','NO','N2','O2','N+','NO+','e-'};
%   stoichCoeffs = build_ElemStoichCoeffs(elem,spec_list)
%       stoichCoeffs = [0 0 0 0 0 0 0 1];
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
% GNU Lesser General Public License 3.0

% Escape any + characters to accomodate regular expression syntax
elem            = regexprep(elem,'+','\\+');
spec_list       = regexprep(spec_list,'+','\\+');

% If another letter, or \, set to one.
% Positive look behind
% a-zA-Z matches any letters after spec
% \\ matches any backslashes after spec (see the regexprep command above)
eCharSlash   = ['(?<=',elem,')[a-zA-Z\\]'];
sCharSlash   = regexp(spec_list,eCharSlash,'once');

% If end of string, set to one.
% Use terminating anchor $
eTermAnchor  = [elem,'$'];
sTermAnchor  = regexp(spec_list,eTermAnchor,'once');

% Match number suffix of spec.
% Positive look behind
% expr_num matches 1 or more digits following spec
eNumSuffix   = ['(?<=',elem,')\d+'];
sNumSuffix   = regexp(spec_list,eNumSuffix,'match');

% Set regular expression matches to appropriate integers
% If matched with another letter, \, or ending with spec, set to one.
% If matched with another number, set to that number.
stcCharSlash  = ~cellfun('isempty',sCharSlash);
stcTermAnchor = ~cellfun('isempty',sTermAnchor);
stcNumSuffix = cellfun(@str2double,sNumSuffix,'UniformOutput',false);
stcNumSuffix(cellfun('isempty',stcNumSuffix)) = {0};
stcNumSuffix = cell2mat(stcNumSuffix);

% Take the union of all three matches
stoichCoeffs = stcCharSlash + stcTermAnchor + stcNumSuffix;

end % build_ElemStoichCoeffs.m
