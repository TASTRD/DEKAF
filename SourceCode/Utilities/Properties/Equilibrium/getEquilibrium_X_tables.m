function [X_s,varargout] = getEquilibrium_X_tables(p,T,LTEtableData,varargin)
% getEquilibrium_X_tables returns the equilibrium mole fractions
% interpolating from tabled values.
%
% A spline interpolation is used on the logarithms of the pressure, whilst
% a chebyshev interpolation is used on the temperatures.
%
%   Examples:
%     (1)     X_s = getEquilibrium_X_tables(p,T,LTEtableData,options)
%     (2)     X_s = getEquilibrium_X_tables(p,T,LTEtableData,Xs_lim,options)
%               |--> allows to fix the minimum limit for the mole fractions
%               (by default 1e-200)
%     (3)     [X_s,T] = getEquilibrium_X_tables(p,h,LTEtableData,Xs_lim,'hInput',options)
%               |--> allows to pass the equilibrium static enthalpy as an
%               input, rather than the temperature, and returns also the
%               temperature profile.
%     (4)     [X_s,T] = getEquilibrium_X_tables(p,s,LTEtableData,Xs_lim,'sInput',options)
%               |--> allows to pass the equilibrium mixture entropy as an
%               input, rather than the temperature, and returns also the
%               temperature profile.
%     (5)     [X_s,T] = getEquilibrium_X_tables(p,T,LTEtableData,[],...)
%               |--> fixes the default 1e-200 minimum limit for the mole
%               fractions
%     (6)     [...,spec_list] = getEquilibrium_X_tables(...)
%               |--> returns also the list of species making up the
%               mixture in question
%
% Inputs:
%    p                  [Pa]
%       vector of pressures size (N_eta x 1)
%    T                  [K]
%       vector of temperatures size (N_eta x 1)
%    LTEtableData
%       structure containing the necessary tabled data for the
%       interpolation:
%           T_i         [K]
%               intermediate temperature used in Malik's mapping
%           T_vec       [K]
%               vector of temperatures in the tables
%           p_vec       [Pa]
%               vector of pressures in the tables
%           X_mat       [-]
%               matrix of mole fractions
%           h_mat       [J/kg]
%               matrix of static enthalpies
%           h_vec       [J/kg]
%               equispaced vector of enthalpies to be used for the hinput
%               case (looking for T as a function of h and p)
%           T_hp        [K]
%               matrix of temperatures as a function of enthalpy and
%               pressure
%           spec_list
%               list of strings with the species order corresponding to
%               X_mat
%    h                  [J/kg]
%       matrix of static enthalpies size (N_eta x 1)
%    s                  [J/kg-K]
%       matrix of static entropies size (N_eta x 1)
%
% Outputs:
%    X_s                [-]
%       matrix of mole fractions size (N_eta x N_spec)
%
% See also: getEquilibrium_X, eval_thermo
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

options = varargin{end};
varargin = varargin(1:end-1);

if ~isempty(varargin) && length(varargin)>1 && strcmp(varargin{2},'hInput')
    input = 'h';
    h = T; % the static enthalpy is passed through the input of the temperature
elseif ~isempty(varargin) && length(varargin)>1 && strcmp(varargin{2},'sInput')
    input = 's';
    s = T; % the static enthalpy is passed through the input of the temperature
else
    input = 'T';
end

% analyzing inputs
if length(p)~=length(T) % check that the sizes are the same
    error(['options.pTEqVecs was set to true (p & T equally sized vectors), yet p has a length of ',num2str(length(p)),' and T has a length of ',num2str(length(T))]);
end
N_eta = length(p); % number of points

% extracting from structure
T_i     = LTEtableData.T_i;
T_vec   = LTEtableData.T_vec;
p_vec   = LTEtableData.p_vec;
X_mat   = LTEtableData.X_mat;
h_mat   = LTEtableData.h_mat;
h_vec   = LTEtableData.h_vec;
s_vec   = LTEtableData.s_vec;
T_hp    = LTEtableData.T_hp;
T_sp    = LTEtableData.T_sp;
spec_list = LTEtableData.spec_list;

% obtaining information from loaded variables
N_spec = size(X_mat,3);     % number of species
N_Ttb = length(T_vec);      % number of temperatures in tables
T_max = max(T_vec);         % maximum temperature in tables
T_min = min(T_vec);         % minimum temperature in tables
logp_vec = log10(p_vec);    % vector of logarithms of pressures in tables

switch options.LTETableInterpMethod
    case 'cheb-interp1'
        % We will first loop the required pressure values. For each of them, we
        % will interpolate to get the mole fractions as a function of temperature.
        % Then, we will interpolate the desired temperatures using chebyshev
        % interpolation.
        X_s = zeros(N_eta,N_spec);                            % allocating
        if ismember(input,{'s','h'})                          % when inputting enthalpy
            T = zeros(N_eta,1);                                     % allocating temperature
        end
        for iTp = 1:N_eta                                       % looping pressure values
            logp = log10(p(iTp));                                   % logarithm of the pressure
            switch input                                            % depending on the input
                case 'T'                                                % if it is the temperature
                    % nothing else is to be done
                case 'h'                                                % if it is the enthalpy
                    h_pT = permute(h_mat,[2,1]);                            % making pressure the first dimension
                    h_T = interp1(logp_vec,h_pT,logp,'spline');             % interpolating enthalpy on pressure
                    T(iTp) = interp1(h_T',T_vec,h(iTp),'spline');           % interpolating temperature on enthalpy
                case 's'                                                % if it is the entropy
                    s_pT = permute(s_mat,[2,1]);                            %#ok<NODEF> % making pressure the first dimension
                    s_T = interp1(logp_vec,s_pT,logp,'spline');             % interpolating enthalpy on pressure
                    T(iTp) = interp1(s_T',T_vec,h(iTp),'spline');           % interpolating temperature on enthalpy
                otherwise
                    error(['the chosen input ''',input,''' is not supported']);
            end
            if hInput                                               % when inputting enthalpy we must first obtain the temperature for the inputted enthalpy
            end
            t = kilamMapping(T_max,T_i,T(iTp),T_min);               % inverse malik mapping
            X_pTs = permute(X_mat,[2,1,3]);                         % making pressure be the first dimension so that we can interpolate
            X_T = interp1(logp_vec,X_pTs,logp,'spline');            % interpolating mole fractions on pressure
            X_T = reshape(X_T,[N_Ttb,N_spec]);                      % making into a 2D matrix
            for s = 1:N_spec                                        % looping species
                X_s(iTp,s) = chebinterp(X_T(:,s),t);                    % interpolating on the values of temperature using chebyshev interpolation
            end
            disp(['Completed T-p position ',num2str(iTp),'/',num2str(N_eta),' --> ',num2str(iTp/N_eta*100,'%0.2f'),'%']);
        end
    case 'interp2'
        [p_mat,T_mat] = meshgrid(p_vec,T_vec);
        switch input                                            % depending on the input
            case 'T'                                                % if it is the temperature
                % nothing else is to be done
            case 'h'                                                % if it is the enthalpy
                [p_mat2,h_mat] = meshgrid(p_vec,h_vec);
                if max(p)>max(p_vec) || min(p)<min(p_vec) || max(h)>max(h_vec) || min(h)<min(h_vec)  % checking that we aren't extrapolating by accident
                    error(['range of (p,h) = (',num2str(min(p),'%0.5e'),'-',num2str(max(p),'%0.5e'),',',num2str(min(h),'%0.5e'),'-',num2str(max(h),'%0.5e'),') out of the range available in tables ',...
                        '(',num2str(min(p_vec),'%0.5e'),'-',num2str(max(p_vec),'%0.5e'),',',num2str(min(h_vec),'%0.5e'),'-',num2str(max(h_vec),'%0.5e'),')']);
                else
                    T = interp2(p_mat2,h_mat,T_hp,real(p),real(h),'spline'); % interpolating
                end
            case 's'                                                % if it is the entropy
                [p_mat2,s_mat] = meshgrid(p_vec,s_vec);
                if max(p)>max(p_vec) || min(p)<min(p_vec) || max(s)>max(s_vec) || min(s)<min(s_vec)  % checking that we aren't extrapolating by accident
                    error(['range of (p,s) = (',num2str(min(p),'%0.5e'),'-',num2str(max(p),'%0.5e'),',',num2str(min(s),'%0.5e'),'-',num2str(max(s),'%0.5e'),') out of the range available in tables ',...
                        '(',num2str(min(p_vec),'%0.5e'),'-',num2str(max(p_vec),'%0.5e'),',',num2str(min(s_vec),'%0.5e'),'-',num2str(max(s_vec),'%0.5e'),')']);
                else
                    T = interp2(p_mat2,s_mat,T_sp,real(p),real(s),'spline'); % interpolating
                end
            otherwise
                error(['the chosen input ''',input,''' is not supported']);
        end
        X_s = zeros(N_eta,N_spec);
        for s = 1:N_spec                                        % looping species
            X_s(:,s) = interp2(p_mat,T_mat,X_mat(:,:,s),real(p),real(T),'spline');
        end
    otherwise
        error(['the chosen interpolation ''',options.LTETableInterpMethod,''' is not supported']);
end

% outputting temperature (if necessary)
ic = 0; % index counter
if ismember(input,{'h','s'})
    ic = ic+1; varargout{ic} = T;
end
ic = ic+1; varargout{ic} = spec_list; % coming from the loaded file