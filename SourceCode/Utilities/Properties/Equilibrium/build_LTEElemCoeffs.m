function [elConsCoeffEq,idx_el,el_vec] = build_LTEElemCoeffs(elemStoich_mat,X_E,spec_list,varargin)
%build_LTEElemCoeffs Generate the coefficients for an elemental fraction
% conservation equation found in the LTE system
%
% Usage:
%   (1)
%       elConsCoeffEq = build_LTEElemCoeffs(elemStoich_mat,X_E,spec_list)
%
%   (2)
%       [...,idx_el,el_vec] = build_LTEElemCoeffs(...,'elID',elID)
%       |---> allows to specify an element ID that must have a dedicated
%       conservation equation in the final system
%
% Inputs
%   elemStoich_mat
%       matrix of size N_elem x N_spec with the stoichiometric coefficients
%       of each element in each species.
%   X_E
%       matrix of size N_eta x N_elem of elemental fractions
%   spec_list
%       cell array of strings of all species in the mixture
%   elID
%       number identifying the element that must have a dedicated
%       conservatioin equation
%
% Output
%   elConsCoeffEq
%       matrix of size N_elem-1 x N_spec x N_eta with the coefficients of the
%       elemental fraction conservation equations
%   idx_el
%       identifier of the row in elConsCoeffEq containing the dedicated
%       equation for the element chosen through elID
%   el_vec
%       list of element identifiers for which a specific conservation
%       equation is used.
%
% Examples
%
% |---> Generate the elemental fraction conservation coefficients for an
% air5 mixture with a O to N mole fraction of 21% to 79%.
%
%   spec_list = {'N','O','NO','N2','O2'};
%   [elem_list,elemStoich_mat] = getElement_info(spec_list)
%       elem_list =
%               {'N','O'}
%       elem_stoich_mat =
%               [   1     0     1     2     0
%                   0     1     1     0     2   ]
%   XEq =          [0.79; 0.21];
%   elConsCoeffEq = build_LTEElemCoeffs(elemStoich_mat,XEq,spec_list)
%       elConsCoeffEq =
%           [ -0.2100    0.7900    0.5800   -0.4200    1.5800 ]
%
%
% |---> Generate the elemental fraction conservation coefficients for an
% air11 mixture with a O to N mole fraction of 21% to 79% and a
% singly-ionized species to free electron fraction of 1.
%
%   spec_list = {'N','O','NO','N2','O2','N+','O+','NO+','N2+','O2+','e-'};
%   [elem_list,elemStoich_mat] = getElement_info(spec_list)
%       elem_list = {'e-','N','O'}
%       elem_stoich_mat =
%               [      0     0     0     0     0    -1    -1    -1    -1    -1     1
%                      1     0     1     2     0     1     0     1     2     0     0
%                      0     1     1     0     2     0     1     1     0     2     0 ]
%   XEq =          [  0;0.79; 0.21];
%   elConsCoeffEq = build_LTEElemCoeffs(elemStoich_mat,XEq,spec_list)
%
% elConsCoeffEq =
%         [      0       0       0       0       0    1.00    1.00    1.00    1.00    1.00   -1.00
%            -0.21    0.79    0.58   -0.42    1.58   -1.00       0   -0.21   -1.21    0.79    0.79  ]
%
%
% See also build_ElemStoichCoeffs.m
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
% GNU Lesser General Public License 3.0

elID = parse_optional_input(varargin,'elID',1);     % optinal input - the element identifier that must stay

N_eta            = size(X_E,1);                     % number of mesh points
[N_elem,N_spec]  = size(elemStoich_mat);            % number of elements and species

if N_elem<1                                         % for the case of mono-elemental mixtures, we don't have to do any of this
    elConsCoeffEq = zeros(0,length(spec_list));
else                                                % for species with more than one element, there will be conditions to satisfy
    if elID==N_elem && N_elem>2;        el_vec = [1,3:N_elem];  % we need the last element in the list and there are more than two elements (we leave out the second)
    elseif elID==N_elem && N_elem<=2;   el_vec = 2;             % we need the last element in the list and there are only two elements
    else;                               el_vec = 1:N_elem-1;    % we don't need the last element in the list (we leave it out)
    end
    idx_el = find(el_vec==elID);                        % position of the wanted element in the final list
    elemStoich_sum   = sum(elemStoich_mat,1);           % suming over elements
    X_E_3D           = repmat(permute(X_E,[2,3,1]) , [1,N_spec,1]);     % (eta,el) --> (el,s,eta)
    elemStoich_mat3D = repmat(elemStoich_mat,        [1,1,N_eta]);      % (el,s) ----> (el,s,eta)
    elemStoich_sum3D = repmat(elemStoich_sum,        [N_elem,1,N_eta]); % (1,s) -----> (el,s,eta)
    
    elConsCoeffEq    = X_E_3D .* elemStoich_sum3D - elemStoich_mat3D;   % coefficients multiplying X_s
    elConsCoeffEq    = elConsCoeffEq(el_vec,:,:);                       % keeping only the elements of interest
end

end % build_LTEElemCoeffs.m
