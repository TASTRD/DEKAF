function cp_reac = getEquilibrium_cpreac(h_s,dys_dT)
% getEquilibrium_cpreac computes the reactive part of the heat capacity.
%
%   Examples:
%       (1) cp_reac = getEquilibrium_cpreac(h_s,dys_dT)
%
%   Inputs:
%       h_s         size N_eta x N_spec
%       dys_dT      size N_eta x N_spec
%
% For detailed information on the different variables see also:
% listOfVariablesExplained, setDefaults
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_spec = size(h_s,2);
cp_reac = (h_s.*dys_dT) * ones(N_spec,1);
