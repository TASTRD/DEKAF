function varargout = getEquilibrium_X_complete(p,Ths,method,mixture,mixCnst,modelEquilibrium,Oder,varargin)
% getEquilibrium_X_complete computes the equilibrium molar fractions,
% directly from simple inputs.
%
% Usage:
%   (1)
%       X_s = getEquilibrium_X_complete(p,T,'mtimesx',mixture,mixCnst,modelEquilibrium,0,options)
%         |--> computes mole fractions given pressure and temperatures
%
%   (2)
%       [X_s,T] = getEquilibrium_X_complete(p,h,'mtimesxh',mixture,mixCnst,modelEquilibrium,0,options)
%         |--> computes mole fractions and temperatures given pressure and
%         enthalpies
%
%   (3)
%       [X_s,T] = getEquilibrium_X_complete(p,s,'mtimesxs',mixture,mixCnst,modelEquilibrium,0,options)
%         |--> computes mole fractions and temperatures given pressure and
%         entropies
%
%   (4)
%       [X_s,T,cp] = getEquilibrium_X_complete(p,h,'mtimesxh',mixture,mixCnst,modelEquilibrium,0,options)
%         |--> returns also the specific heat at constant pressure
%
%   (5)
%       [...,dXs_dT,dXs_dp] = getEquilibrium_X_complete(p,Th,method,mixture,mixCnst,modelEquilibrium,1,options)
%         |--> computes also first order derivatives with T and p of the
%         mole fractions
%
%   (6)
%       [...,dXs_dT,dXs_dp,dXs_dXF] = getEquilibrium_X_complete(...,'computeXFgrads')
%       |--> computes first order derivatives also wrt the elemental mole
%       fractions
%
%   (7)
%       [...,dXs_dT,dXs_dp,dXs_dT2,dXs_dpdT,dXs_dp2] = ...
%                   getEquilibrium_X_complete(p,Th,method,mixCnst,mixture,modelEquilibrium,2,options)
%         |--> computes also second order derivatives with T and p of the
%         mole fractions
%
%   (8)
%       [...,dXs_dT,dXs_dp,dXs_dT2,dXs_dpdT,dXs_dp2,dXs_dT3,dXs_dpdT2,dXs_dp2dT,dXs_dp3] = ...
%                   getEquilibrium_X_complete(p,Th,method,mixture,mixCnst,modelEquilibrium,3,options)
%         |--> computes also third order derivatives with T and p of the
%         mole fractions
%
%   (9)
%       [...] = getEquilibrium_X_complete(...,'Xslim',Xslim)
%           |--> allows to pass a lower limit on the mole fractions. By
%           default it is 1e-35
%
%   (10)
%       [...] = getEquilibrium_X_complete(...,'variable_XE',X_E)
%           |--> instead of employing the fixed elemental fractions in
%           mixCnst, it uses a custom variable X_E
%
%   (11.1)    method = 'mtimesx' only
%       [...] = getEquilibrium_X_complete(...,'guessXs',X_s0)
%           |--> allows to provide an initial guess for X_s. Otherwise it
%           is taken from the precomputed tables. (see 
%           <a href="matlab:help getEquilibrium_X_tables">getEquilibrium_X_tables</a>). X_s0 must have the same size as X_s.
%
%   (11.2)    method = 'mtimesxh' or 'mtimesxs'
%       [...] = getEquilibrium_X_complete(...,'guessXs',X_s0,T0)
%           |--> allows to provide an initial guess for X_s and T.
%           Otherwise it is taken from the precomputed tables (see 
%           <a href="matlab:help getEquilibrium_X_tables">getEquilibrium_X_tables</a>). X_s0 and T0 must have the same size 
%           as X_s and T.
%
% Inputs:
%   p       [Pa]            (N_Tp x 1)
%       vector of pressures 
%   T       [K]             (N_Tp x 1)
%       vector of temperatures
%   h       [J/kg]          (N_Tp x 1)
%       vector of mixture enthalpies
%   s       [J/kg-K]        (N_Tp x 1)
%       vector of mixture entropies
%   X_E     [-]             (N_Tp x N_elem)
%       matrix with the atomic fractions to be used for the obtention of
%       the equilibrium composition 
%   method
%       string identifying the method to obtain the equilibrium
%       composition.
%   mixCnst
%       structure with the necessary mixture constants
%   modelEquilibrium
%       string identifying the equilibrium model
%   options
%       classic DEKAF options structure
%
% Outputs:
%   X_s     [-]             (N_Tp x N_spec)
%       vector of pressures 
%   T       [K]             (N_Tp x 1)
%       vector of temperatures
%   cp      [J/kg-K]        (N_Tp x 1)
%       vector of heat capacities
%
% See also: getEquilibrium_X, getEquilibriumDer_X_air5,
% getEquilibriumDer_X_air11, getEquilibriumDer_X_oxygen2
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% checking for the option structure and setting defaults
N_eta = length(Ths); % number of temperature values
[X_E, varargin] = parse_optional_input(varargin,'variable_XE',repmat(mixCnst.XEq,[N_eta,1]));
[varargin,bGuessXs,idxXs] = find_flagAndRemove('guessXs',varargin);
if bGuessXs                                             % initial guesses
    X_s0 = varargin{idxXs};                                 % guess for the equilibrium concentrations
    switch method                                           % depending on the method
        case 'mtimesx'                                          % nothing needed
            varargin(idxXs) = [];                                   % clearing varargin
        case {'mtimesxh','mtimesxs'}                            % we need the guess for T also
            T0 = varargin{idxXs+1};                                 % guess for the temperature
            varargin(idxXs:idxXs+1) = [];                           % clearing varargin
        otherwise;                      error(['The chosen method ''',method,''' is not supported by getEquilibrium_X_complete. Try with getEquilibrium_X']);
    end
end
[varargin,bXF] = find_flagAndRemove('computeXFgrads',varargin);
if bXF;     flags4der = {'computeXFgrads'};
else;       flags4der = {};
end
if isempty(varargin)
    options = struct;
else
    options = varargin{1};
    varargin(1) = [];
end

options.modelEquilibrium = modelEquilibrium; % defining equilibrium model
N_spec = length(mixCnst.spec_list); % number of species

switch method                                                                       % depending on the method
    case 'mtimesx'                                                                      % we input temperature
        T = Ths;                                                                            % defining from ambiguous input
        if ~bGuessXs                                                                        % If no initial guess was provided
            [X_s0,spec_list_table] = getEquilibrium_X_tables(p,T,mixCnst.LTEtableData,options); % mole fractions from tables
            X_s0 = switchOrder(X_s0,2,spec_list_table,mixCnst.spec_list);                       % switching to the order we want
            X_s0 = enforce_chargeNeutralityCond(X_s0,mixCnst.bElectron,mixCnst.bPos,mixCnst.Mm_s,mixCnst.R0,'ys_lim',options.ys_lim); % enforcing charge-neutrality condition
            X_s0 = enforce_concCond(X_s0,mixCnst.idx_bath);                                     % enforcing concentration condition
        end
        options.X_mat0 = X_s0;                                                              % populating initial guess
        T_r = repmat(T,[1,size(mixCnst.delta_nuEq,2)]);
        lnKeq_r = getReaction_lnKeq(T_r,T,T,T,T,T,mixCnst.delta_nuEq,mixCnst.Keq_struc,options); % computing equilibrium constants
        lnKpeq_r = getEquilibrium_lnKpeq(lnKeq_r,mixCnst.R0,T,sum(mixCnst.delta_nuEq,1)');  % computing equilibrium-p constant
        X_mat = getEquilibrium_X(p,T,lnKpeq_r,X_E,mixCnst,'mtimesx',options,varargin{:});   % exact mole fractions
        N_out1 = 1;                                                                         % number of initial outputs
    case 'mtimesxh'                                                                     % or we input enthalpy
        h = Ths;                                                                            % defining from ambiguous input
        if ~bGuessXs                                                                        % If no initial guess was provided
            [X_s0,T0,spec_list_table] = getEquilibrium_X_tables(p,h,mixCnst.LTEtableData,[],'hInput',options); % mole fractions from tables (guessing also T from h)
            X_s0 = switchOrder(X_s0,2,spec_list_table,mixCnst.spec_list);                       % switching to the order we want
            X_s0 = enforce_chargeNeutralityCond(X_s0,mixCnst.bElectron,mixCnst.bPos,mixCnst.Mm_s,mixCnst.R0,'ys_lim',options.ys_lim); % enforcing charge-neutrality condition
            X_s0 = enforce_concCond(X_s0,mixCnst.idx_bath);                                     % enforcing concentration condition
        end
        options.X_mat0 = X_s0;                                                              % populating initial guess
        options.T0     = T0;
        [X_mat,T,cp] = getEquilibrium_X(p,h,X_E,mixCnst,'mtimesxh',mixture,options,varargin{:}); % exact mole fractions
        varargout{2} = T;                                                                   % defining outputs
        varargout{3} = cp;
        N_out1 = 3;                                                                         % number of initial outputs
    case 'mtimesxs'                                                                     % or we input enthalpy
        s = Ths;                                                                            % defining from ambiguous input
        if ~bGuessXs                                                                        % If no initial guess was provided
            [X_s0,T0,spec_list_table] = getEquilibrium_X_tables(p,s,mixCnst.LTEtableData,[],'sInput',options); % mole fractions from tables (guessing also T from s)
            X_s0 = switchOrder(X_s0,2,spec_list_table,mixCnst.spec_list);                       % switching to the order we want
            X_s0 = enforce_chargeNeutralityCond(X_s0,mixCnst.bElectron,mixCnst.bPos,mixCnst.Mm_s,mixCnst.R0,'ys_lim',options.ys_lim); % enforcing charge-neutrality condition
            X_s0 = enforce_concCond(X_s0,mixCnst.idx_bath);                                     % enforcing concentration condition
        end
        options.X_mat0 = X_s0;                                                              % populating initial guess
        options.T0     = T0;
        [X_mat,T] = getEquilibrium_X(p,s,X_E,mixCnst,'mtimesxs',mixture,options,varargin{:}); % exact mole fractions
        varargout{2} = T;                                                                   % defining outputs
        N_out1 = 2;                                                                         % number of initial outputs
    otherwise
        error(['The chosen method ''',method,''' is not supported by getEquilibrium_X_complete. Try with getEquilibrium_X']);
end
X_s = reshape(X_mat,[N_eta,N_spec]);                                        % making the output into a two-dimensional matrix
varargout{1} = X_s;

if nargout>N_out1
[varargout{N_out1+1:nargout}] = getEquilibriumDer_X(T,X_s,p,X_E,mixCnst,mixture,Oder,options,flags4der{:});
end

end