function [mixCnst,options] = getAll_constants(mixture,varargin)
% getAll_constants gets all the necessary constants given a mixture name.
%
%   Examples:
%       (1)     mixCnst = getAll_constants(mixture)
%
%       (2)     mixCnst = getAll_constants(mixture,options)
%           |--> allows to pass an options structure
%
%       (3)     [mixCnst,options] = getAll_constants(...)
%           |--> returns also the options structure with the defaults
%
% The required options are:
% --> elecStates, modelCollisionChargeCharge, modelCollisionNeutral,
%     modelDiffusion, collisionRefExceptions
%
% Description of options in see also setDefaults, listOfVariablesExplained
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
%
% GNU Lesser General Public License 3.0

if nargin==1
    options.mixture = mixture;
elseif nargin==2
    options = varargin{1};
else
    error('wrong number of inputs');
end
options = setDefaults(options);

% Getting universal constants
[R0,hPlanck,cLight,kBoltz,nAvogadro,pAtm,qEl,epsilon0,sigmaStefBoltz] = getUniversal_constants();

% define all experimental constants associated with the mixture and its reactions
[nu_reac,nu_prod,delta_nuEq,A_r,nT_r,theta_r,N_spec,spec_list,Keq_struc,...
    reac_list,reac_list_eq,dflt_idx_bath,dflt_idx_bathElement,qf2T_r,qb2T_r,gasSurfReac_struct] = getMixture_constants(mixture,pAtm,R0,options);
[bIonelim,bIonHeavyim,bDissel,bIonAssoc] = identifyReac_allioniz(reac_list,spec_list);

% elemental arrangements
[elem_list,elemStoich_mat] = getElement_info(spec_list);
Mm_E = getElement_Mm(elem_list);
options = standardvalue(options,'idx_bath',         dflt_idx_bath);
options = standardvalue(options,'idx_bathElement',  dflt_idx_bathElement);

% define all experimental constants associated with the species e.g., A_s, B_s, C_s in Blottner's
[Mm_s,Amu_s,Bmu_s,Cmu_s,Akappa_s,Bkappa_s,Ckappa_s,Dkappa_s,Ekappa_s,muRef_s,TmuRef_s,Smu_s,kappaRef_s,TkappaRef_s,Skappa_s,R_s,...
    nAtoms_s,thetaVib_s,pCrit_s,TCrit_s,Keq_struc,thetaRot_s,L_s,sigma_s,thetaElec_s,gDegen_s,bElectron,hForm_s,HForm298K_s,hIon_s,bPos,bMol,...
    gasSurfReac_struct,AThermFuncs_s] = ...
    getSpecies_constants(spec_list,R0,hPlanck,cLight,kBoltz,nAvogadro,Keq_struc,gasSurfReac_struct,options);

% define all experimental constants associated with the collision cross-sectional areas
[consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,consts_Estar,consts_Omega14,...
    consts_Omega15,consts_Omega24,consts_Dbar_binary,stats_fits,~] = ...
    getPair_Collision_constants(spec_list,options);

% define all experimental constants associated with the vibrational relaxation times
if strcmp(options.numberOfTemp,'2T')
    [aVib_sl,bVib_sl,sigmaVib_s] = getPair_Vibrational_constants(spec_list,bMol,bElectron,Mm_s,thetaVib_s,nAvogadro,options);
else
    aVib_sl = NaN*ones(N_spec,N_spec);    bVib_sl = NaN*ones(N_spec,N_spec);    sigmaVib_s = NaN*ones(N_spec,1);
end

% Set Sutherland's defaults (mono-species mixtures)
[mu_ref,T_ref,Su_mu,kappa_ref,Su_kappa] = getDefault_Sutherland(options,muRef_s,TmuRef_s,Smu_s,kappaRef_s,TkappaRef_s,Skappa_s);

% Set constant Prandtl defaults
Pr = getDefault_Pr(options);

% saving into structure format
mixCnst.mu_ref                  = mu_ref;
mixCnst.T_ref                   = T_ref;
mixCnst.Su_mu                   = Su_mu;
mixCnst.kappa_ref               = kappa_ref;
mixCnst.Su_kappa                = Su_kappa;
mixCnst.Pr                      = Pr;
mixCnst.nu_reac                 = nu_reac;
mixCnst.nu_prod                 = nu_prod;
mixCnst.delta_nuEq              = delta_nuEq;
mixCnst.elem_list               = elem_list;
mixCnst.elemStoich_mat          = elemStoich_mat;
mixCnst.Mm_E                    = Mm_E;
mixCnst.N_elem                  = length(elem_list);
mixCnst.pAtm                    = pAtm;
mixCnst.R0                      = R0;
mixCnst.nAvogadro               = nAvogadro;
mixCnst.kBoltz                  = kBoltz;
mixCnst.hPlanck                 = hPlanck;
mixCnst.pAtm                    = pAtm;
mixCnst.sigmaStefBoltz          = sigmaStefBoltz;
mixCnst.qEl                     = qEl;
mixCnst.epsilon0                = epsilon0;
mixCnst.R_s                     = R_s;
mixCnst.A_r                     = A_r;
mixCnst.nT_r                    = nT_r;
mixCnst.theta_r                 = theta_r;
mixCnst.N_spec                  = N_spec;
mixCnst.reac_list               = reac_list;
mixCnst.reac_list_eq            = reac_list_eq;
mixCnst.idx_bath                = options.idx_bath;
mixCnst.idx_bathElement         = options.idx_bathElement;
mixCnst.qf2T_r                  = qf2T_r;
mixCnst.qb2T_r                  = qb2T_r;
mixCnst.spec_list               = spec_list;
mixCnst.bIonelim                = bIonelim;
mixCnst.bIonHeavyim             = bIonHeavyim;
mixCnst.bDissel                 = bDissel;
mixCnst.bIonAssoc               = bIonAssoc;
mixCnst.Mm_s                    = Mm_s;
mixCnst.thetaRot_s              = thetaRot_s;
mixCnst.L_s                     = L_s;
mixCnst.sigma_s                 = sigma_s;
mixCnst.Amu_s                   = Amu_s;
mixCnst.Bmu_s                   = Bmu_s;
mixCnst.Bmu_s                   = Bmu_s;
mixCnst.Cmu_s                   = Cmu_s;
mixCnst.Akappa_s                = Akappa_s;
mixCnst.Bkappa_s                = Bkappa_s;
mixCnst.Ckappa_s                = Ckappa_s;
mixCnst.Dkappa_s                = Dkappa_s;
mixCnst.Ekappa_s                = Ekappa_s;
mixCnst.muRef_s                 = muRef_s;
mixCnst.TmuRef_s                = TmuRef_s;
mixCnst.Smu_s                   = Smu_s;
mixCnst.kappaRef_s              = kappaRef_s;
mixCnst.TkappaRef_s             = TkappaRef_s;
mixCnst.Skappa_s                = Skappa_s;
mixCnst.Keq_struc               = Keq_struc;
mixCnst.nAtoms_s                = nAtoms_s;
mixCnst.thetaVib_s              = thetaVib_s;
mixCnst.thetaElec_s             = thetaElec_s;
mixCnst.gDegen_s                = gDegen_s;
mixCnst.bElectron               = bElectron;
mixCnst.bPos                    = bPos;
mixCnst.bMol                    = bMol;
mixCnst.hForm_s                 = hForm_s;
mixCnst.HForm298K_s             = HForm298K_s;
mixCnst.hIon_s                  = hIon_s;
mixCnst.consts_Omega11          = consts_Omega11;
mixCnst.consts_Omega22          = consts_Omega22;
mixCnst.consts_Bstar            = consts_Bstar;
mixCnst.consts_Cstar            = consts_Cstar;
mixCnst.consts_Estar            = consts_Estar;
mixCnst.consts_Omega14          = consts_Omega14;
mixCnst.consts_Omega15          = consts_Omega15;
mixCnst.consts_Omega24          = consts_Omega24;
mixCnst.consts_Dbar_binary      = consts_Dbar_binary;
mixCnst.stats_fits              = stats_fits;
mixCnst.aVib_sl                 = aVib_sl;
mixCnst.bVib_sl                 = bVib_sl;
mixCnst.sigmaVib_s              = sigmaVib_s;
mixCnst.gasSurfReac_struct      = gasSurfReac_struct;
mixCnst.EoS_params.pCrit_s      = pCrit_s;
mixCnst.EoS_params.TCrit_s      = TCrit_s;
mixCnst.AThermFuncs_s           = AThermFuncs_s;

end % getAll_constants
