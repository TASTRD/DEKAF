function [] = compareGradientY(T,yl,F,dF_dyl_anal)
dyl = yl(end)-yl(1);
T = T(1:end/2);
dF_dyl_anal = dF_dyl_anal(1:end/2,1);
dF_dyl = (F(1:end/2) - F(end/2+1:end)) / dyl;
plot(T,dF_dyl,T,dF_dyl_anal,'--');