function [] = compareGradientT(ym,F,dF_dym_anal)
dF_dym = diff(F)./diff(ym);
T_diff = (ym(1:end-1)+ym(2:end))/2;
figure
plot(T_diff,dF_dym,ym,dF_dym_anal,'--');