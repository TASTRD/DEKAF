function H_e = getEnthalpy_semitotal(h_e, U_e)
%GETENTHALPY_SEMITOTAL Calculate the semi-total enthalpy H_e based off of a
%static enthalpy h_e and a velocity component perpendicular to the leading edge

H_e = h_e + 0.5*U_e.^2;

end % getEnthalpy_semitotal