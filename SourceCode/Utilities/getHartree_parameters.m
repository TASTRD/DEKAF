function [theta,Theta,beta,varargout] = getHartree_parameters(dxi_dx,xi_e,U_e,H_e,Tv_e,Dxe,varargin)
% getHartree_parameters returns the three Hartree parameters used to
% accound for the streamwise variation of the different edge quantities.
%
% Usage:
%   (1)
%       [theta,Theta,beta] = getHartree_parameters(dxi_dx,xi_e,U_e,H_e,Tv_e,Dxe)
%
%   (2)
%       [...,ThetaQ1,ThetaQ2,...] = getHartree_parameters(...,Q1,Q2,...)
%       |--> allows to compute additional Hartree parameters based on other
%       quantities Q1, Q2, etc.
%
% Inputs and outputs:
%   dxi_dx
%       variation of the marching variable wrt the physical (or
%       Probstein-Elliot transformed) x. (size N_x x 1)
%   xi_e
%       streamwise marching variable. (size N_x x 1)
%   U_e
%       streamwise edge velocity. (size N_x x 1)
%   H_e
%       total edge enthalpy. (size N_x x 1)
%   Tv_e
%       vib.-elec.-el. temperature. (size N_x x 1)
%   Dxe
%       differentiation matrix wrt the physical (or PE-transformed x).
%       (size N_x x N_x)
%   theta
%       temperature Hartree parameter. (size N_x x 1)
%   Theta
%       total enthalpy Hartree parameter. (size N_x x 1)
%   beta
%       pressure/velocity Hartree parameter. (size N_x x 1)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

Nextra = length(varargin);

if length(xi_e) > 1 % if there are more than one xi positions, then we compute this
    % we can now calculate the derivatives of U_e & h_e with respect to xi,
    % which are needed for the evaluation of beta and theta. We benefit
    % from chain rule, having that Dxi * U_e = dx_dxi * Dx * U_e
    dx_dxi  = 1./dxi_dx;
    beta    = 2*xi_e./U_e  .* dx_dxi .* (Dxe*U_e);     % Hartree velocity parameter
    Theta   = 2*xi_e./H_e  .* dx_dxi .* (Dxe*H_e);     % Hartree total enthalpy parameter
    theta   = 2*xi_e./Tv_e .* dx_dxi .* (Dxe*Tv_e);    % Hartree temperature parameter
    for ii=Nextra:-1:1
        Q_e = varargin{ii};
        varargout{ii} = 2*xi_e./Q_e .* dx_dxi .* (Dxe*Q_e); % additional Hartree parameter
    end

    % For stagnation flow, xi_e(1) and U_e(1) both equal identically zero.
    % This leads to NaN floating point results. Treat this case separately.
    if xi_e(1) == 0 && U_e(1) == 0
        beta(1) = 1;
        Theta(1) = 0; % H_e  is nonzero, yet xi_e is zero.
        theta(1) = 0; % Tv_e is nonzero, yet xi_e is zero.
    end

else % if there is only one xi position
    beta = 0; % then they must all be constants
    Theta = 0;
    theta = 0;
    for ii=Nextra:-1:1
        varargout{ii} = 0;
    end
end

end % getHartree_parameters
