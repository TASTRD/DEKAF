function varargout = find_closestND(M,varargin)
% find_closestND, given a set of N vectors, obtains the positions of the M
% points of these vectors that are closest to each other.
%
% Syntax:
%       [idx1,idx2,...] = find_closestND(M,vec1,vec2,...)
%
% Example:
%   X1 = [1,3,5,10,16,24,32,53,67,73]
%   X2 = [1,6,17,23,36,40,43,48,53,59,66,71]
%   X3 = [1,2,4,7,11,16,22,29,37,46,56,67]
%
%       [idx1,idx2,idx3] = find_closestND(6,X1,X2,X3)
%
%   idx1 = [1 9 5 1 2 3];
%   idx2 = [1 11 3 1 1 2];
%   idx3 = [1 12 6 2 2 3];
%
% Visualization of the example:
% X       10        20        30        40        50        60        70
% 1234567890123456789012345678901234567890123456789012345678901234567890123
% X1:
% o-o-o----o-----o-------o-------o--------------------o-------------o-----o
% X2:
% x----x----------x-----x------------x---x--x----x----x-----x------x----x
% X3:
% **-*--*---*----*-----*------*-------*--------*---------*----------*
% closest 6 in order
% |1st           |3rd
% |4th                                                              |2nd
%  |5th
%     |6th
%
%
%       [...,dist] = find_closestND(...)  --> returns also the distances
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

vecs = varargin; % inputted vectors
N = length(vecs); % number of vectors
N_N = zeros(N,1); % number of vectors
for ii=1:N % looping vectors
    N_N(ii) = length(vecs{ii}); % computing length of each vector
    idxAll1D{ii} = 1:N_N(ii); % position index of each vector
end
N_Tot = prod(N_N); % total number of combinations (N1*N2*N3*...)
jReshape = ones(1,N); % general vector to be used to reshape
jRepmat = N_N'; % general vector to be used to repmat
vecsND = zeros(N_Tot,N); % allocating compressed N-D vector of values
idxAllND = zeros(N_Tot,N); % allocating compressed N-D vectors of indices
for ii=1:N % looping vectors
    jReshape_ii = jReshape;
    jReshape_ii(ii) = N_N(ii); % we want to have it span over its ii dimension
    jRepmat_ii = jRepmat;
    jRepmat_ii(ii) = 1; % and repeat it over all the others
    vecsND(:,ii)   = reshape(repmat(reshape(vecs{ii},    jReshape_ii),jRepmat_ii),[N_Tot,1]); % building N-D matrix of vector values and compressing into 1D
    idxAllND(:,ii) = reshape(repmat(reshape(idxAll1D{ii},jReshape_ii),jRepmat_ii),[N_Tot,1]); % building N-D matrix of indices and compressing into 1D
end
centerOfMass = vecsND*ones(N,1)/N; % computing center of mass (average)
centerOfMass2D = repmat(centerOfMass,[1,N]); % repeating to have the same size as vecsND
distTotND = abs(centerOfMass2D-vecsND)*ones(N,1)/N; % computing distance from the center (norm-1)
[distMins,idxMins] = sort(distTotND); % sorting from smaller to higher
idxMins = idxMins(1:M); % keeping M smallest
distMins = distMins(1:M); % keeping the distances of these M smallest
idxAllMins = idxAllND(idxMins,:); % indices in each vec where the minimum will be
for ii=1:N
    varargout{ii} = idxAllMins(:,ii); % outputting where these minimums are
end
varargout{N+1} = distMins;