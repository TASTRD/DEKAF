function [xi,dxi_dXI,dxi_dXI2] = getXi_tanhMapping(N_xi,XI_i,frac_XIi,deltaxi_start,xi_end,xi_start,varargin)
% getXi_tanhMapping builds a vector xi using the tanh mapping.
%
% Usage:
%   (1)
%       xi = getXi_tanhMapping(N_xi,XI_i,frac_XIi,deltaxi_start,xi_end,xi_start)
%       |--> with xi(XI) = C + A*XI + B*log(cosh(a*XI))
%               ^
%              /|\  deltaxi/deltaxi(end)
%               |
%            1 _|_ _ _ _ _ _ _______________|______
%              _| _ _ _ _ __/-
%     frac_XIi  |       _/|                 |
%               |      /
%               |     /   |                 |
%               |    /
%               |   /     |                 |
%               |  /
%               | /       |                 |
%               |/________________________________\
%               0         |                 |     /   XI
%                       XI_i               1.0
%
%   (2)
%       [xi,dxi_dXI,dxi_dXI2] = getXi_tanhMapping(N_xi,XI_i,frac_XIi,deltaxi_start,xi_end,xi_start)
%       |--> returns also the gradients
%
%   (3)
%       [...] = getXi_tanhMapping(...,'exact')
%       |--> uses a newton-Raphson method to get the values of the A, B and
%       a parameters such that the initial step is exactly the desired one.
%       Otherwise, the assumption deltaxi_start/deltaXI = dxi/dXI is
%       implied.
%
% Inputs and outputs:
%     xi_start
%       first value of xi in the marching
%     xi_end
%       final value of xi in the marching
%     deltaxi_start
%       first value of the marching step
%     N_xi
%       number of xi points
%     XI_i
%       intermediate value of XI, where the step will have reached a
%       certain fraction of the final asymptotic value
%     frac_XIi
%       fraction of the final asymptotic value to be reached at XI_i
%     xi
%       streamwise position vector (size N_xi x 1)
%     dxi_dXI, dxi_dXI2
%       first and second derivatives of the position vector wrt the
%       computational variable XI (size N_xi x 1)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,exact] = find_flagAndRemove('exact',varargin);

XI = linspace(0,1,N_xi);            % equispaced computational domain
a = 1/XI_i * atanh(frac_XIi);
A = deltaxi_start * (N_xi-1);
B = (xi_end-xi_start-deltaxi_start*(N_xi-1)) / log(cosh(a));
C = xi_start;

if exact % applying correction to match the first step exactly
    disp('--- Performing Newton-Raphson for a machine-accurate ''tanh'' mapping ...');
    it=0;
    vec = [A,B,a].';
    while true
        A = vec(1);
        B = vec(2);
        a = vec(3);
        [F,J] = getXi_jacobian(xi_start,xi_end,deltaxi_start,XI_i,N_xi,frac_XIi,A,B,a);
        vec = vec - J\F;
        conv = norm(F,Inf);
        if isnan(conv)
            error('getXi_tanhMapping Newton-Raphson has nan''d!');
        end
        it=it+1;
        disp(['--- Iteration ',num2str(it),': Converged with error: ',num2str(conv)]);
        if conv<=eps, break; end
        if it>=1000, break; end
    end
    A = vec(1);
    B = vec(2);
    a = vec(3);
    disp('--- Completed Newton-Raphson algorithm for ''tanh'' mapping');
end

xi = C + A*XI + B*log(cosh(a*XI));
dxi_dXI = A + a*B*tanh(a*XI);       % computing dxi_dXI
dxi_dXI2 = a^2*B./(cosh(a*XI).^2);

% Fix xi and its derivatives to remove rounding error (where possible)
xi(1) = xi_start;
dxi_dXI(1) = A;
dxi_dXI2(1) = a^2*B;
xi(end) = xi_end;
% Due to inevitable rounding errors in evaluation of cosh() and tanh(),
% it is not possible to correct the derivatives at XI = 1 (indexing on end)

end % getXi_tanhMapping

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [F,J] = getXi_jacobian(xi_start,xi_end,deltaxi_start,XI_i,N_xi,frac_XIi,A,B,a)
% getXi_jacobian returns the jacobian and function to be zeroed in order to
% obtain the parameters of the tanh fitting that match exactly the chosen
% deltaxi_start.
%
% Usage:
%   (1)
%      [F,J] = getXi_jacobian(xi_start,xi_end,deltaxi_start,XI_i,N_xi,frac_XIi,A,B,a)
%
%
% Inputs and outputs:
%   xi_start, xi_end
%       initial and final xi position
%   deltaxi_start
%       initial xi step
%   XI_i
%       intermediate value of XI, where the step will have reached a
%       certain fraction of the final asymptotic value
%   N_xi
%       number of points in the xi vector
%   frac_XIi
%       fraction of the final asymptotic value to be reached at XI_i
%   A, B, a
%       value of the different parameters in the tanh mapping
%   F
%       function to be zeroed during the NR iterations
%   J
%       jacobian of F with respect to A, B and a
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

deltaXI = 1/(N_xi-1);

F = [A + B*log(cosh(a)) - xi_end - xi_start ; ...
     A*deltaXI + B*log(cosh(a*deltaXI)) - deltaxi_start ; ...
     A*(frac_XIi - 1) + B*a*(tanh(a) - tanh(a*XI_i)) ];

J = [1          , log(cosh(a))              , B*tanh(a) ; ...
     deltaXI    , log(cosh(a*deltaXI))      , B*deltaXI*tanh(a*deltaXI) ; ...
     frac_XIi-1 , a*(tanh(a) - tanh(a*XI_i)), B*(tanh(a) - tanh(a*XI_i)) + B*a*(1/sinh(a)^2 - XI_i/sinh(a*XI_i)^2) ];

end % getXi_jacobian
