function [ ynd,D1y,D2y,D3y ] = MappingMalikFD_uptoD3( y_max,y_i,eta,D1eta,D2eta )
% This function maps the FD grid on the domain [0,1] to
% [0,y_i,y_max]. Furthermore, it delivers the
% differentiation matrices that correspond to this transformation. This
% function transforms the derivative matrices up to third order
%
% K.J. Groot, Oct. 2015

% Transform equally spaced to physical grid
a = (y_max*y_i)/(y_max-2*y_i);
b = 1+a/y_max;
ynd = ((a*eta)./(b-eta))';

% Compute scale factors w.r.t. differentiation
detady   = (a*b)./((a+ynd).^2);
d2etady2 =  (-2*a*b)./((a+ynd).^3);
% please note that for this particular mapping the following identity
% holds: d^n eta / dy^n = (-comb)^(n-1) * n! * (deta/dy)^n

D1y = diag(detady)                             * D1eta;
D2y = diag(d2etady2)                           * D1eta ...
    + diag(detady.^2)                          * D2eta;
D3y = zeros(size(D1y));

end % MappingMalikFD_uptoD3.m
