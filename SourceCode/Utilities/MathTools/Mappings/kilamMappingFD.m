function [ ynd] = kilamMappingFD(y_max,y_i,y)
% This function maps back from malik's domain [0,y_i,y_max] to the the
% Chebyshev grid on the FD equispaced domain [0,1].
%
% K.J. Groot, Oct. 2015

% Transform equally spaced to physical grid
a = (y_max*y_i)/(y_max-2*y_i);
b = 1+a/y_max;
ynd = (b*y./(a+y))';

end % kilamMappingFD.m
