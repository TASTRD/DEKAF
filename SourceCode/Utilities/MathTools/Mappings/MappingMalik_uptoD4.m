function [ ystar,D1y,D2y,D3y,D4y ] = MappingMalik_uptoD4( y_max,y_i,eta,D1eta,D2eta,D3eta,D4eta,varargin)
% This function maps the Chebyshev grid on the domain [-1,0,1] to
% [0,y_i,y_max]. Furthermore, it delivers the pseudo-spectral
% differentiation matrices that correspond to this transformation. This
% function transforms the derivative matrices up to fourth order
%
% If an additional input is passed (y_min) then it account for a chebyshev
% distribution of points not starting at y=0
%
% K.J. Groot, Oct. 2015

if ~isempty(varargin)
    y_min = varargin{1};
else
    y_min = 0;
end

% displacing the maximum and intermediate ys (y_max, y_i) to have them
% start at zero
y_max = y_max-y_min;
y_i = y_i-y_min;

% Transform Chebyshev to physical grid
ystar = (y_i*y_max*(1+eta)./(y_max - eta*(y_max - 2*y_i)))';
ystar = ystar + y_min; % undoing the movement to zero done before.

% Recurring combination in the scaling factor expressions
comb = (y_max - 2*y_i)./(y_max - eta*(y_max - 2*y_i));

% Compute scale factors w.r.t. differentiation
detady   = (y_max - eta*(y_max - 2*y_i)).^2/(2*y_i*y_max*(y_max - y_i));
d2etady2 =  -2*detady.^2.*comb;
d3etady3 =   6*detady.^3.*comb.^2;
d4etady4 = -24*detady.^4.*comb.^3;
% please note that for this particular mapping the following identity
% holds: d^n eta / dy^n = (-comb)^(n-1) * n! * (deta/dy)^n

D1y = diag(detady)                             * D1eta;
D2y = diag(d2etady2)                           * D1eta ...
    + diag(detady.^2)                          * D2eta;
D3y = diag(d3etady3)                           * D1eta ...
    + diag(3*detady.*d2etady2)                 * D2eta ...
    + diag(detady.^3)                          * D3eta;
D4y = diag(d4etady4)                           * D1eta ...
    + diag(4*detady.*d3etady3 + 3*d2etady2.^2) * D2eta ...
    + diag(6*detady.^2.*d2etady2)              * D3eta ...
    + diag(detady.^4)                          * D4eta;

end % MappingMalik_uptoD4.m
