function [ ystar] = kilamMapping(y_max,y_i,y,varargin)
% kilamMapping maps back from malik's domain [0,y_i,y_max] to the the
% Chebyshev grid on the Chebyshev domain [-1,1].
%
% Examples:
%   (1)     ystar = killamMapping(y_max,y_i,y)
%           |--> assumes the minimum y to be 0
%   (2)     ystar = killamMapping(y_max,y_i,y,y_min)
%           |--> allows to pass a minimum y
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% checking if y_min was passed
if ~isempty(varargin)
    y_min = varargin{1};
else
    y_min = 0;
end

% correcting for non-zero y_min
y_max = y_max - y_min;
y_i = y_i - y_min;
y = y - y_min;

% Transform equally spaced to physical grid
a = (y_max*y_i)/(y_max-2*y_i);
b = 1+2*a/y_max;
ystar = ((b*y-a)./(a+y))';

end % kilamMapping.m
