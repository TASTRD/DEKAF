function varargout = xMappings(meth,varargin)
% xMappings can return the jacobians of the different mappings available
% for x, as well as the x distribution.
%
% In other words, if we want a x spacing given by whatever function XI(x),
% xMappings retruns the first and second derivatives of XI with x.
%
% Examples:
%   (1)     [dXI_dx,dXI_dx2] = xMappings('linear',x)
%       |--> uses a linear spacing
%
%   (2)     [dXI_dx,dXI_dx2] = xMappings('log',x)
%       |--> uses a logarithmic spacing
%
%   (3)     [dXI_dx,dXI_dx2] = xMappings('tanh',x,mapOpts)
%       |--> uses a tanh spacing, with x(XI) = C + A*XI + B*log(cosh(a*XI))
%       and requires a mapping options structure with:
%               - x_start           first value of x in the marching
%               - x_end             final value of x in the marching
%               - deltax_start      first value of the marching step
%               - N_x               number of x points
%               - XI_i              intermediate value of XI, where the
%                                   step will have reached a certain
%                                   fraction of the final asymptotic value
%               - frac_XIi          fraction of the final asymptotic value
%                                   to be reached at XI_i
%               ^
%              /|\  deltax/deltax(end)
%               |
%            1 _|_ _ _ _ _ _ _______________|______
%              _| _ _ _ _ __/-
%     frac_XIi  |       _/|                 |
%               |      /
%               |     /   |                 |
%               |    /
%               |   /     |                 |
%               |  /
%               | /       |                 |
%               |/________________________________\
%               0         |                 |     /   XI
%                       XI_i               1.0
%
%   (3)     [x,dXI] = xMappings(meth,'get_x',mapOpts);
%       |--> returns the x and the dXI (step in the compuational variable)
%       according to the chosen method, initial & final points, and number
%       of points, which must be passed through mapOpts.x_start, .x_end
%       and .N_x.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

errmsgID = 'DEKAF:xMappings';

if ischar(varargin{1}) && strcmp(varargin{1},'get_x')  % it means we want the x and dXI
    bGetX = true;
else                                                    % it means we want the derivatives
    bGetX = false;
end
mapOpts = varargin{2};
x_start    = mapOpts.x_start;
x_end      = mapOpts.x_end;
N_x        = mapOpts.N_x;


if bGetX  % we want the x spacing
    XI = linspace(0,1,N_x);
    dXI = 1/(N_x-1);
    switch meth
        case 'linear'                       % x(XI) = xMin + (xMax-xMin)*XI
            x = x_start + (x_end-x_start)*XI;
        case 'log'                          % x(XI) = exp(log(xMin) + (log(xMax)-log(xMin))*XI)
            ln_x = log(x_start) + (log(x_end)-log(x_start))*XI;
            x = exp(ln_x);
        case 'tanh'                         % x(XI) = C + A*XI + B*log(cosh(a*XI))
            deltax_start   = mapOpts.deltax_start; % extracting from mapOpts
            XI_i           = mapOpts.XI_i;
            frac_XIi       = mapOpts.frac_XIi;

            x = getXi_tanhMapping(N_x,XI_i,frac_XIi,deltax_start,x_end,x_start);
        otherwise
            error(errmsgID,['the chosen spacing in the x direction ''',meth,''' is not a valid option.']);
    end
    varargout{1} = x;
    varargout{2} = dXI;
else    % we want the derivatives of the x spacing
    x = varargin{1};
    switch meth
        case 'linear'                       % x(XI) = xMin + (xMax-xMin)*XI
            % no modifications needed
            dXI_dx = ones(size(x)) * 1/(x_end-x_start);
            dXI_dx2 = zeros(size(x));
        case 'log'                          % x(XI) = exp(log(xMin) + (log(xMax)-log(xMin))*XI)
            dXI_dx  =  1/(log(x_end)-log(x_start)) * 1./x;
            dXI_dx2 = -1/(log(x_end)-log(x_start)) * 1./x.^2;
        case 'tanh'                         % x(XI) = C + A*XI + B*log(cosh(a*XI))
            deltax_start   = mapOpts.deltax_start; % extracting from mapOpts
            XI_i           = mapOpts.XI_i;
            frac_XIi       = mapOpts.frac_XIi;

            [x_vec,dx_dXI_vec,dx_dXI2_vec] = getXi_tanhMapping(N_x,XI_i,frac_XIi,deltax_start,x_end,x_start);

            % Attempt to find the required index/indices aligning the input
            % x with the calculated x.
            i_x_req = x_vec == x;
            if ~nnz(i_x_req)
                % It appears there is no exact match with the input x and
                % any of the calculated x. Shame.
                % As of now, it's only been required to think of this
                % edge case for x as a scalar (e.g. attachment line within build_mat)
                % We can just take the min(abs()) to find the
                % nearest appropriate index.
                if length(x) == 1
                    [~,i_x_req] = min(abs(x_vec-x));
                else
                    % As a developer, if your calculated x does not
                    % exactly match any of the input x array, you will
                    % have to figure out an appropriate way to handle this.
                    error(errmsgID,['The vector of x''s calculated by getXi_tanhMapping()', ...
                        'has no exact matches with the input vector x\n.', ...
                        'This requires a new development for this edge case.']);
                end
            end
            dx_dXI = dx_dXI_vec(i_x_req);
            dx_dXI2 = dx_dXI2_vec(i_x_req);

            dXI_dx = 1./dx_dXI;
            dXI_dx2 = -1./(dx_dXI).^3 .* dx_dXI2;
        otherwise
            error(errmsgID,['the chosen spacing in the x direction ''',meth,''' is not a valid option.']);
    end
    varargout{1} = dXI_dx;
    varargout{2} = dXI_dx2;
end
