function c = mtimesxIf(a,b)
% mtimesxIf makes the product of the 2D slices of a 3D matrix using (if
% possible) mtimesx, otherwise looping over the third dimension.
%
% Usage:
%   (1)
%       c = mtimesxIf(a,b)
%
% See also: mtimesx, loop3D
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

try
    mtimesx('matlab');
    c = mtimesx(a,b);
catch
    warning('mtimesx was not successfully compiled, so the (slower) looping method will be used.');
    c = loop3D_prod(a,b);
end