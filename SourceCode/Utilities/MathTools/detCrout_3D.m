function [dt,dt_minors] = detCrout_3D(A)
% detCrout_3D computes the determinants of an 3-dimensional matrix where
% the 2D slices span over the 2nd and 3rd.
%
%   Examples:
%       (1)     [dt,dt_minors] = detCrout_3D(A)
%
%   Inputs and Outputs:
%
%       A:          3D matrix  (size N_eta x N_spec x N_spec)
%       dt:         determinant of the full matrix (size N_eta x 1)
%       dt_minors:  determinant of each minor of the matrix (size N_eta x N_spec x N_spec)
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

% extract sizes
[N_eta,N_spec,~] = size(A);

% building minors 5D
A_5D = permute(A,[2,3,4,5,1]); % (eta,i,j) -> (i,j,s,l,eta)
a = zeros(N_spec-1,N_spec-1,N_spec,N_spec,N_eta); % allocating minor matrices
for s=1:N_spec % looping species rows
    iNotS = 1:N_spec;
    iNotS(s) = []; % rows to be kept in the minor
    for l=1:N_spec % looping species columns
        jNotL = 1:N_spec;
        jNotL(l) = []; % columns to be kept in the minor
        a(:,:,s,l,:) = A_5D(iNotS,jNotL,1,1,:); % populating minors
    end
end

clear A_5D

% Crout algorithm for the minors
alfa = repmat(eye(N_spec-1),[1,1,N_spec,N_spec,N_eta]); % allocating coefficients in the lower diagonal matrix making alfa(i,i,...) = 1
beta = zeros(N_spec-1,N_spec-1,N_spec,N_spec,N_eta); % allocating coefficients in the upper diagonal matrix
dt_minors_5D = ones(1,1,N_spec,N_spec,N_eta);
for jj=1:N_spec-1
    for ii=1:jj
        k=1:ii-1;
        beta(ii,jj,:,:,:) = a(ii,jj,:,:,:) - mtimesx(alfa(ii,k,:,:,:),beta(k,jj,:,:,:));
    end
    for ii=jj+1:N_spec-1
        k=1:jj-1;
        alfa(ii,jj,:,:,:) = 1./beta(jj,jj,:,:,:) .* (a(ii,jj,:,:,:) - mtimesx(alfa(ii,k,:,:,:),beta(k,jj,:,:,:)));
    end
    dt_minors_5D = dt_minors_5D .* beta(jj,jj,:,:,:); % computing determinants of the minors
end
clear alfa beta a
dt_minors_abs = permute(dt_minors_5D,[5,3,4,1,2]); % (i,j,s,l,eta) -> (eta,s,l)
clear dt_minors_5D
ii = repmat((1:N_spec)',[1,N_spec]); % row position
jj = repmat(1:N_spec,   [N_spec,1]); % column position
checkerboard_2D = (-1).^(ii+jj); % defining checkerboard matrix [[1,-1,1,...],[-1,1,-1,...],...]
checkerboard = repmat(reshape(checkerboard_2D,[1,N_spec,N_spec]),[N_eta,1,1]); % (s,l) --> (eta,s,l)
dt_minors = dt_minors_abs.*checkerboard;

% computing the determinant of the full matrix
dt = (A(:,:,1).*dt_minors(:,:,1)) * ones(N_spec,1); % computing determinant of the full matrix
