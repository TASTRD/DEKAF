function [varargout] = roots4thOrderPoly(a,b,c,d,e,varargin)
% [x1,x2,x3,x4] = roots4thOrderPoly(a,b,c,d,e) returns the 4 roots of the
% fourth-order polynomial:
%
%           a*x^4 + b*x^3 + c*x^2 + d*x + e = 0;
%
% [x1,x2,x3,x4] = roots4thOrderPoly(a,b,c,d,e,'log') does the same, but
% evaluating the expressions with the logarithms of the coefficients a, b,
% c, d and e, such that the order of magnitude is preserved. This is
% recommended for polynomials with very different values in their
% coefficients.
%
% [x1,x2,x3,x4] = roots4thOrderPoly(a,b,c,d,e,'log','multiOrder') activates
% the option to make all sumations with a the multiOrderSum function.
%
% [x1,x2,x3,x4] = roots4thOrderPoly(a,b,c,d,e,'log','multiOrder','precision',prec)
% allows to specify with what precision the multi-order sum must be
% performed.
%
% See also: multiOrderSum
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if nargin>5 && ischar(varargin{1}) && strcmp(varargin{1},'log') % we want to evaluate the coefficients with their logarithms
    % computing logarithms of the polynomial coefficients
    lna = log(a);
    lnb = log(b);
    lnc = log(c);
    lnd = log(d);
    lne = log(e);
    % computing intermediate terms for the roots of the 4th order
    % polynomial
    if nargin>6 && ischar(varargin{2}) && strcmp(varargin{2},'multiOrder')
        p = exp(lnc-lna) - 3/8*exp(2*lnb-2*lna);
        lnq = log(multiOrderSum(1/8*exp(3*lnb-3*lna), - 1/2*exp(lnb+lnc-2*lna), + exp(lnd-lna),varargin{3:end}));
        dlt = multiOrderSum(256*exp(3*lna+3*lne), - 192*exp(2*lna+lnb+lnd+2*lne), - 128*exp(2*lna+2*lnc+2*lne), + 144*exp(2*lna+lnc+2*lnd+lne), - 27*exp(2*lna+4*lnd), ...
            + 144*exp(lna+2*lnb+lnc+2*lne), - 6*exp(lna+2*lnb+2*lnd+lne), - 80*exp(lna+lnb+2*lnc+lnd+lne), + 18*exp(lna+lnb+lnc+3*lnd), + 16*exp(lna+4*lnc+lne), ...
            - 4*exp(lna+3*lnc+2*lnd), - 27*exp(4*lnb+2*lne), + 18*exp(3*lnb+lnc+lnd+lne), - 4*exp(3*lnb+3*lnd), - 4*exp(2*lnb+3*lnc+lne), + exp(2*lnb+2*lnc+2*lnd),varargin{3:end});
        dlt1 = multiOrderSum(2*exp(3*lnc), - 9*exp(lnb+lnc+lnd), + 27*exp(2*lnb+lne), + 27*exp(lna+2*lnd), - 72*exp(lna+lnc+lne),varargin{3:end});
        lnQ = 1/3*(log(dlt1 + sqrt(-27*dlt)) - log(2));
        Sp2x4 = multiOrderSum(-2/3*p, + 1/3*exp(lnQ-lna), + 1/3*exp(2*lnc-lna-lnQ), - exp(lnb+lnd-lna-lnQ), + 4*exp(lne-lnQ),varargin{3:end}); % S to the power 2 times 4
        lnS = 1/2*log(Sp2x4) - log(2);
        S = 1/2*sqrt(Sp2x4);
        % computing roots
        if nargout>0
            inSqrt1 = multiOrderSum(2/3*p, - 1/3*exp(lnQ-lna), - 1/3*exp(2*lnc-lna-lnQ), + exp(lnb+lnd-lna-lnQ), - 4*exp(lne-lnQ), - 2*p, + exp(lnq-lnS),varargin{3:end});
            varargout{1} = multiOrderSum(-1/4*exp(lnb-lna), - S, + 1/2*sqrt(inSqrt1),varargin{3:end});
        end
        if nargout>1
            varargout{2} = multiOrderSum(-1/4*exp(lnb-lna), - S, - 1/2*sqrt(inSqrt1),varargin{3:end});
        end
        if nargout>2
            inSqrt2 = multiOrderSum(2/3*p, - 1/3*exp(lnQ-lna), - 1/3*exp(2*lnc-lna-lnQ), + exp(lnb+lnd-lna-lnQ), - 4*exp(lne-lnQ), - 2*p, - exp(lnq-lnS),varargin{3:end});
            varargout{3} = multiOrderSum(-1/4*exp(lnb-lna), + S, + 1/2*sqrt(inSqrt2),varargin{3:end});
        end
        if nargout>3
            varargout{4} = multiOrderSum(-1/4*exp(lnb-lna), + S, - 1/2*sqrt(inSqrt2),varargin{3:end});
        end
    else
        p = exp(lnc-lna) - 3/8*exp(2*lnb-2*lna);
        lnq = log(1/8*exp(3*lnb-3*lna) - 1/2*exp(lnb+lnc-2*lna) + exp(lnd-lna));
        dlt = 256*exp(3*lna+3*lne) - 192*exp(2*lna+lnb+lnd+2*lne) - 128*exp(2*lna+2*lnc+2*lne) + 144*exp(2*lna+lnc+2*lnd+lne) - 27*exp(2*lna+4*lnd) ...
            + 144*exp(lna+2*lnb+lnc+2*lne) - 6*exp(lna+2*lnb+2*lnd+lne) - 80*exp(lna+lnb+2*lnc+lnd+lne) + 18*exp(lna+lnb+lnc+3*lnd) + 16*exp(lna+4*lnc+lne) ...
            - 4*exp(lna+3*lnc+2*lnd) - 27*exp(4*lnb+2*lne) + 18*exp(3*lnb+lnc+lnd+lne) - 4*exp(3*lnb+3*lnd) - 4*exp(2*lnb+3*lnc+lne) + exp(2*lnb+2*lnc+2*lnd);
        dlt1 = 2*exp(3*lnc) - 9*exp(lnb+lnc+lnd) + 27*exp(2*lnb+lne) + 27*exp(lna+2*lnd) - 72*exp(lna+lnc+lne);
        lnQ = 1/3*(log(dlt1 + sqrt(-27*dlt)) - log(2));
        Sp2x4 = -2/3*p + 1/3*exp(lnQ-lna) + 1/3*exp(2*lnc-lna-lnQ) - exp(lnb+lnd-lna-lnQ) + 4*exp(lne-lnQ); % S to the power 2 times 4
        lnS = 1/2*log(Sp2x4) - log(2);
        S = 1/2*sqrt(Sp2x4);
        % computing roots
        if nargout>0
            inSqrt1 = 2/3*p - 1/3*exp(lnQ-lna) - 1/3*exp(2*lnc-lna-lnQ) + exp(lnb+lnd-lna-lnQ) - 4*exp(lne-lnQ) - 2*p + exp(lnq-lnS);
            varargout{1} = -1/4*exp(lnb-lna) - S + 1/2*sqrt(inSqrt1);
        end
        if nargout>1
            varargout{2} = -1/4*exp(lnb-lna) - S - 1/2*sqrt(inSqrt1);
        end
        if nargout>2
            inSqrt2 = 2/3*p - 1/3*exp(lnQ-lna) - 1/3*exp(2*lnc-lna-lnQ) + exp(lnb+lnd-lna-lnQ) - 4*exp(lne-lnQ) - 2*p - exp(lnq-lnS);
            varargout{3} = -1/4*exp(lnb-lna) + S + 1/2*sqrt(inSqrt2);
        end
        if nargout>3
            varargout{4} = -1/4*exp(lnb-lna) + S - 1/2*sqrt(inSqrt2);
        end
    end
    varargout{5} = p;
    varargout{6} = exp(lnq);
    varargout{7} = dlt1;
    varargout{8} = exp(lnQ);
    varargout{9} = S;
else % we do it normally
    p = (8.*a.*c-3.*b.^2)./(8.*a.^2);
    q = (b.^3-4.*a.*b.*c+8.*a.^2.*d)./(8.*a.^3);
    dlt0 = c.^2-3.*b.*d+12.*a.*e;
    dlt1 = 2.*c.^3 - 9.*b.*c.*d + 27.*b.^2.*e + 27.*a.*d.^2 - 72.*a.*c.*e;
    Q = ((dlt1 + sqrt(dlt1.^2-4.*dlt0.^3))./2).^(1./3);
    S = 1./2.*sqrt(-2./3.*p + 1./(3.*a).*(Q+dlt0./Q));

    if nargout>0
        varargout{1} = -b./(4.*a) - S + 1./2.*sqrt(-4.*S.^2-2.*p+q./S);
    end
    if nargout>1
        varargout{2} = -b./(4.*a) - S - 1./2.*sqrt(-4.*S.^2-2.*p+q./S);
    end
    if nargout>2
        varargout{3} = -b./(4.*a) + S + 1./2.*sqrt(-4.*S.^2-2.*p-q./S);
    end
    if nargout>3
        varargout{4} = -b./(4.*a) + S - 1./2.*sqrt(-4.*S.^2-2.*p-q./S);
    end
    varargout{5} = p;
    varargout{6} = q;
    varargout{7} = dlt1;
    varargout{8} = Q;
    varargout{9} = S;
end