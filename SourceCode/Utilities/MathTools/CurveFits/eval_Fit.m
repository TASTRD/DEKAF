function varargout = eval_Fit(X,consts)
% eval_Fit evaluates the appropriate fitting according to the fitting type
% passed through consts.fitType.
%
% Examples:
%   (1)     lnf = eval_Fit(X,consts)
%
%   (2)     [lnf,dlnf_dX,dlnf_dX2] = eval_Fit(X,consts)
%       |--> returns also derivatives of the logarithm of the function
%
% Available values of consts.fitType are:
%   'polyLog'           uses eval_Fit_PolyLog.m
%   'polyLogLog'        uses eval_Fit_PolyLogLog.m
%
% It is important to note that even though these functions return different
% quantities (lnf or f), eval_Fit ALWAYS returns the logarithmic values and
% derivatives.
%
% Inputs have to be compliant with:
%
% See also: eval_Fit_PolyLogLog and eval_Fit_PolyLog
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

fitType = consts.fitType;

switch fitType
    case 'polyLogLog'
[varargout{1:nargout}] = eval_Fit_PolyLogLog(X,consts);
    case 'polyLog'
[varargout{1:nargout}] = eval_Fit_PolyLog(X,consts);
[varargout{1:nargout}] = dfdx_2_dlnfdx(varargout{:});
    otherwise
        error(['fit type ''',fitType,''' not supported']);
end
