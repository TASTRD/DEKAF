function [p,varargout] = generateFit_PolyLogLog(x,y,nOrder)
%GENERATEFIT_POLYLOGLOG Create a curve fit for a function expressed as a
% polynomial for the log-log plot.
%
% Calls:
% 1. p                      = generateFit_PolyLogLog(x,y,nOrder);
% 2. [p,errorRes,Rsquared]  = generateFit_PolyLogLog(x,y,nOrder);
%
% Inputs:
%   x           domain of function, y
%   y           range of function, y(x)
%   nOrder      order of polynomial fit
%
% Outputs:
%   p           vector whose entries correspond to the coefficients for
%               the polynomial log-log fit, in the typical built-in MATLAB
%               polyfit syntax. See below for
%               their mathematical significance.
%   varargout   variable arugment output struct
%               1st entry: residual error of the fit
%               2nd entry: R-squared value (coefficient of determination)
%
% An example of nOrder of 3: the following coefficients A,B,C,D are
% returned whose definition is given below:
%   y = exp(D)*x^(A*(lnx)^2 + B*(lnx) + C)
% or, written in its logarithmic form...
%   ln(y) = A*(lnx)^3 + B*(lnx)^2 + C*(lnx) + D
%
% The output p then would be the row vector, [A,B,C,D].
%
% Note if an alternate order of polynomial order is specified, say nOrder=2
% then the function would fit the following curve:
%   ln(y) = A*(lnx)^2 + B*(lnx) + C
%
% The output p then would be the row vector, [A,B,C].
%
% Examples:
% 1.    Extract the coefficients for a cubic log-log fit to collision
%       integral data.
%
%   T = [300 500 600 1000 2000 4000 5000 6000 8000 10000 15000];
%   Omega11 = [11.12 9.88 9.53 8.69 7.60 6.52 6.22 5.99 5.64 5.39 4.94];
%   p = generateFit_PolyLogLog(T,Omega11,3)
%
% 2.    Fit a quadratic log-log fit to collision integral data. Return the
%       residual error and the "R-squared" value of the fit
%       (i.e., coefficient of determination). Plot the original data
%       against the fitted data.
%
%     %% Calculating the fit
%     T.O_O   = [300 500 1000 2000 4000 5000 6000 8000 10000 15000 20000];
%     Q11.O_O = [8.53 7.28 5.89 4.84 4.00 3.76 3.57 3.27 3.05 2.65 2.39];
%     nOrder  = 3;
%     [p.O_O,eR.O_O,Rsq.O_O] = generateFit_PolyLogLog(T.O_O,Q11.O_O,nOrder);
%     Q11.fit.O_O = exp(polyval(p.O_O,log(T.O_O)));
%     %% Plotting
%     figure
%     subplot(2,1,1)
%     plot(T.O_O,Q11.O_O,'k+-'); hold on
%     plot(T.O_O,Q11.fit.O_O,'b-.');
%     title(sprintf('Polynomial Order %i Log-Log Curve Fit Comparison',nOrder))
%     ylabel('\Omega^{1,1} [A^2]');
%     grid minor
%     subplot(2,1,2)
%     loglog(T.O_O,Q11.O_O,'k+-'); hold on
%     loglog(T.O_O,Q11.fit.O_O,'b-.');
%     ylabel('\Omega^{1,1} [A^2]')
%     xlabel('T [K]')
%     grid minor
%     legend({'Original Data','Fitted Data'},'location','best');
%     hold off;
%
%     fprintf('Residual error of the curve fit: %.3f\n',eR.O_O);
%     fprintf('R-squared value of the curve fit: %.3f\n',Rsq.O_O);
%
% See also polyfit.m, corrcoef.m
%
% This function uses MATLAB's built-in polyfit to do so
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

lnx = log(x);
lny = log(y);

[p,S] = polyfit(lnx,lny,nOrder);

if nargout>1
%     R = corrcoef(log(x),log(y))^2;    % correlation coefficients
    Rsq = 1-sum((lny - polyval(p,lnx)).^2)/sum((lny - mean(lny)).^2); % correlation coefficients
    varargout{1} = S.normr;         % residual error of the fit
    varargout{2} = Rsq;        % R squared value
end

end % generateFit_PolyLogLog.m

