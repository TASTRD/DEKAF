function [lnProp_sl,dlnPropsl_dT,dlnPropsl_dT2] = eval_Fit_PolyLogLog(T_pair,consts)
%EVAL_FIT_POLYLOGLOG Evaluate a polynomial log-log curve fit
% structured for all pairs of species in a gaseous mixture. This is
% commonly used to calculate collision integrals.
%
% Examples:
%   (1)     lnProp_sl = eval_Fit_PolyLogLog(T,consts);
%
%   (1)     [lnProp_sl,dlnPropsl_dT,dlnPropsl_dT2] = eval_Fit_PolyLogLog(T,consts);
%
% Inputs:
%   T_pair      -   temperature corresponding to each pair (N_eta x N_spec x N_spec)
%                   measured in Kelvin
%   consts      -   struct with fields .A, .B, .C, .D, .E. These fields
%   .A              contain curve fit coefficients to calculate
%   .B              a property (e.g., OmegaXX_sl, the average collision
%   .C              cross-sectional area of two-body pair, species s & l,
%   .D              order (m,n)); matrix of size (N_s x N_l). The different
%   .E              fields correspond to different coefficients in the
%   .F              polynomial evaluation.
%   .G              Note that this input works for any of the following
%   .H              polynomial forms:
%
%   0:  Zeroth order polynomial
%
%       lnProp_sl   =   A;
%
%   1:  First order polynomial
%
%       lnProp_sl   =   A + B*lnT;
%
%   2:  Second order polynomial
%
%       lnProp_sl   =   A + B*lnT + C*(lnT)^2;
%
%   3:  Third order polynomial
%
%       lnProp_sl   =   A + B*lnT + C*(lnT)^2 + D*(lnT)^3;
%
%   4:  Fourth order polynomial
%
%       lnProp_sl   =   A + B*lnT + C*(lnT)^2 + D*(lnT)^3 + E*(lnT)^4;
%
%   5:  Fifth order polynomial
%
%       lnProp_sl   =   A + B*lnT + C*(lnT)^2 + D*(lnT)^3 + E*(lnT)^4 + F*(lnT)^5;
%
% Outputs:
%   lnProp_sl   -   natural logarithm of a property for all collision pairs
%                   Depending on the input curve fit values, this may be
%                   the average collision cross-sectional area or the
%                   binary diffusion coefficient multiplied by pressure
%                   matrix of size (N_eta x N_s x N_l)
%   dlnPropsl_dT    First derivative with temperature (N_eta x N_s x N_l)
%   dlnPropsl_dT2   Second derivative with temperature (N_eta x N_s x N_l)
%
% References:
%
% Gupta,  R. N.,  Yos,  J. M.,  Thompson,  R. A.,  and Lee,  K.-P.,  "A
%   Review of Reaction Rates and Thermodynamic and Transport Properties for
%   an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%   Calculations to 30000 K," Nasa-rp-1232, National Aeronautics and Space
%   Administration, 1990.
%
% Michael J. Wright, Deepak Bose, Grant E. Palmer, and Eugene Levin.
%   "Recommended Collision Integrals for Transport Property Computations
%    Part 1: Air Species", AIAA Journal, Vol. 43, No. 12 (2005), pp.
%    2558-2564. https://doi.org/10.2514/1.16713
%
% See also getPair_Collision_constants.m, eval_Fit
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

[N_eta,N_spec,~]   = size(T_pair);
N_flds = length(fieldnames(consts)); % number of fields (order of the polynomial)
A = zeros(N_eta,N_spec,N_spec); % allocating
B = A; C = A; D = A; E = A; F = A; G = A; H = A;
switch N_flds % note: there is an additional field due to the fitType
    case 2
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 3
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 4
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
        C = repmat(reshape(consts.C,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 5
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
        C = repmat(reshape(consts.C,[1,N_spec,N_spec]),[N_eta,1,1]);
        D = repmat(reshape(consts.D,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 6
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
        C = repmat(reshape(consts.C,[1,N_spec,N_spec]),[N_eta,1,1]);
        D = repmat(reshape(consts.D,[1,N_spec,N_spec]),[N_eta,1,1]);
        E = repmat(reshape(consts.E,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 7
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
        C = repmat(reshape(consts.C,[1,N_spec,N_spec]),[N_eta,1,1]);
        D = repmat(reshape(consts.D,[1,N_spec,N_spec]),[N_eta,1,1]);
        E = repmat(reshape(consts.E,[1,N_spec,N_spec]),[N_eta,1,1]);
        F = repmat(reshape(consts.F,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 8
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
        C = repmat(reshape(consts.C,[1,N_spec,N_spec]),[N_eta,1,1]);
        D = repmat(reshape(consts.D,[1,N_spec,N_spec]),[N_eta,1,1]);
        E = repmat(reshape(consts.E,[1,N_spec,N_spec]),[N_eta,1,1]);
        F = repmat(reshape(consts.F,[1,N_spec,N_spec]),[N_eta,1,1]);
        G = repmat(reshape(consts.G,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 9
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
        C = repmat(reshape(consts.C,[1,N_spec,N_spec]),[N_eta,1,1]);
        D = repmat(reshape(consts.D,[1,N_spec,N_spec]),[N_eta,1,1]);
        E = repmat(reshape(consts.E,[1,N_spec,N_spec]),[N_eta,1,1]);
        F = repmat(reshape(consts.F,[1,N_spec,N_spec]),[N_eta,1,1]);
        G = repmat(reshape(consts.G,[1,N_spec,N_spec]),[N_eta,1,1]);
        H = repmat(reshape(consts.H,[1,N_spec,N_spec]),[N_eta,1,1]);
    otherwise
        error(['the number of fields in consts ',num2str(N_flds),' is not supported.']);
end

lnT = log(T_pair);
lnProp_sl = A + B.*lnT + C.*lnT.^2 + D.*lnT.^3 + E.*lnT.^4 + F.*lnT.^5 + G.*lnT.^6 + H.*lnT.^7;
if nargout>1
    dlnPropsl_dlnT = B + 2*C.*lnT + 3*D.*lnT.^2 + 4*E.*lnT.^3 + 5*F.*lnT.^4 + 6*G.*lnT.^5 + 7*H.*lnT.^6;
    dlnPropsl_dT = dfdlnx_2_dfdx(T_pair,dlnPropsl_dlnT);
end
if nargout>2
    dlnPropsl_dlnT2 = 2*C + 6*D.*lnT + 12*E.*lnT.^2 + 20*F.*lnT.^3 + 30*G.*lnT.^4 + 42*H.*lnT.^5;
    [~,dlnPropsl_dT2] = dfdlnx_2_dfdx(T_pair,dlnPropsl_dlnT,dlnPropsl_dlnT2);
end

end % eval_Fit_PolyLogLog.m
