function [x_new,y_new,y_extrap] = extrap_data_prior_fit(x,y,x_extrap,edge_extrap,type_extrap)
%EXTRAP_DATA_PRIOR_FIT Given data of x and y, extrapolate and append a
% point onto the data so that curve fits generated do not diverge at
% the specified coordinate
%
% Usage:
%   [x_new,y_new,y_extrap] = ...
%       extrap_data_prior_fit(x,y,x_extrap,edge_extrap,type_extrap);
%
% Inputs and Outputs
%   x               independent variable of data
%                   1 x N
%   y               dependent variable of data
%                   1 x N
%   x_extrap        double, specified x coordinate of the artificial point
%                   can be a single value, or a vector of points
%   edge_extrap     string, specify which edge of data from which to
%                   extrapolate
%                   if 'left', extrapolate to the left of x(1)
%                   if 'right, extrapolate to the right of x(end)
%   type_extrap     string, type of extrapolation
%                   if 'linear', linearly extrapolate from edge
%                   if 'cutoff', cutoff the data (flat line)
%
%   x_new           independent variable of data with x_extrap appended on
%                   1 x (N + 1)
%   y_new           dependent variable of data with y_extrap appended on
%                   1 x (N + 1)
%   y_extrap        y coordinate whose x coord. is x_extrap and whose
%                   value is determined by type_extrap
%
% Notes:
%   Be aware that if multiple values are input for x_extrap, then the
%   quality of the fit over the original set of data will degrade.
%
% Examples
%   1. Linearly extrapolate at the end of a data set in a log-log domain,
%       specifying a point at x coordinate 10^11.
%
%         % Import the data
%         T = [0.1 0.2 0.3 0.4 0.6 0.8 1.0 2.0 3.0 4.0...
%             6.0 8.0 10.0 20.0 30.0 40.0 60.0 80.0...
%             100.0 200.0 300.0 400.0 600.0 800.0...
%             10^3 10^4 10^5 10^6 10^7 10^8];
%         O11 = [0.0630 0.1364 0.1961 0.2480 0.3297...
%             0.3962 0.4519 0.6467 0.7746 0.8719 1.0173...
%             1.1259 1.2130 1.4972 1.6716 1.7984 1.9807...
%             2.1123 2.2156 2.5427 2.7380 2.8780 3.0767...
%             3.2185 3.3289 4.4759 5.6273 6.7786 7.9299...
%             9.0812];
%
%         [logT_new,logO11_new,logO11_extrap] = ...
%             extrap_data_prior_fit(log(T),log(O11),log(10^(11)),'right','linear');
%
%         % Generate fit for original data
%         p_fit = generateFit_PolyLogLog(T,O11,5);
%
%         % Generate fit for new data
%         p_newfit = generateFit_PolyLogLog(exp(logT_new),exp(logO11_new),5);
%
%         % Resample the domain at a greater sample rate for the fits
%         T_new = exp(logT_new);
%         T_r = logspace(log10(T(1)),log10(T(end)),300);
%         T_new_r = logspace(log10(T_new(1)),log10(T_new(end)),300);
%
%         % Evaluate fits in finely sampled domain
%         logO11_fit = polyval(p_fit,log(T_r));
%         logO11_newfit = polyval(p_newfit,log(T_new_r));
%
%         % Plot the comparison
%         figure
%         loglog(T,O11,'-','color',viridis(1,4),'linewidth',1.5); hold on;
%         loglog(T_r,exp(logO11_fit),'-.','color',viridis(2,4),'linewidth',1.5);
%         loglog(T_new,exp(logO11_new),'-','color',viridis(3,4),'linewidth',1.5);
%         loglog(T_new_r,exp(logO11_newfit),'-.','color',viridis(4,4),'linewidth',1.5);
%         xlabel('log(T*)'); ylabel('log(\Omega^{1,1})');
%         grid minor
%         legend('original data','original fit','new data','new fit','location','best');
%
% Related functions:
% See also generateFit_PolyLogLog
%
% Author(s): Ethan Beyak
%
% GNU Lesser General Public License 3.0

% Determine from which edge to extrapolate
switch edge_extrap
    case 'left' % left edge
        i1 = 1;                 % x1 index
        ib = 1;                 % xboundary index
    case 'right' % right edge
        i1 = length(x) - 1;
        ib = i1+1;
    otherwise
        error(['The edge_extrap value of ',edge_extrap,' is not currently supported!']);
end
i2 = i1+1;

% Extract the y coordinate of the specified extrapolation type
switch type_extrap
    case 'linear' % linear extrapolation from two boundary data points
        y_extrap = y(i1) + (y(i2)-y(i1))/(x(i2)-x(i1))*(x_extrap - x(i1));
    case 'cutoff' % cutoff
        y_extrap = y(ib);
    otherwise
        error(['The type_extrap value of ',type_extrap,' is not currently supported!']);
end

% Append on the extrapolated points to the original data
switch edge_extrap
    case 'left' % left edge
        x_new = [x_extrap,x];
        y_new = [y_extrap,y];
    case 'right' % right edge
        x_new = [x,x_extrap];
        y_new = [y,y_extrap];
end

end % extrap_data_prior_fit
