function [Prop_sl,dPropsl_dT,dPropsl_dT2] = eval_Fit_PolyLog(T_pair,consts)
% eval_Fit_PolyLog Evaluates a polynomial log curve fit structured for all
% pairs of species in a gaseous mixture.
%
% This is commonly used to calculate collision integrals. In particular, it
% is the one chosen by Stuckert.
%
% Examples:
%   (1)     Prop_sl = eval_Fit_PolyLog(T,consts);
%
% Inputs:
%   T_pair      -   temperature corresponding to each pair (N_eta x N_spec x N_spec)
%                   measured in Kelvin
%   consts      -   struct with fields .A, .B, .C, .D, .E. These fields
%   .A              contain curve fit coefficients to calculate
%   .B              a property (e.g., OmegaXX_sl, the average collision
%   .C              cross-sectional area of two-body pair, species s & l,
%   .D              order (m,n)); matrix of size (N_s x N_l). The different
%   .E              fields correspond to different coefficients in the
%   .F              polynomial evaluation.
%                   Note that this input works for any of the following
%                   polynomial forms:
%
%   0:  Zeroth order polynomial
%
%       Prop_sl   =   A;
%
%   1:  First order polynomial
%
%       Prop_sl   =   A + B*lnT;
%
%   2:  Second order polynomial
%
%       Prop_sl   =   A + B*lnT + C*(lnT)^2;
%
%   3:  Third order polynomial
%
%       Prop_sl   =   A + B*lnT + C*(lnT)^2 + D*(lnT)^3;
%
%   4:  Fourth order polynomial
%
%       Prop_sl   =   A + B*lnT + C*(lnT)^2 + D*(lnT)^3 + E*(lnT)^4;
%
%   5:  Fifth order polynomial
%
%       Prop_sl   =   A + B*lnT + C*(lnT)^2 + D*(lnT)^3 + E*(lnT)^4 + F*(lnT)^5;
%
% Outputs:
%   Prop_sl   -   natural logarithm of a property for all collision pairs
%                   Depending on the input curve fit values, this may be
%                   the average collision cross-sectional area or the
%                   binary diffusion coefficient multiplied by pressure
%                   matrix of size (N_eta x N_s x N_l)
%   dPropsl_dT    First derivative with temperature (N_eta x N_s x N_l)
%   dPropsl_dT2   Second derivative with temperature (N_eta x N_s x N_l)
%
% References:
%
% G. Stuckert 1991, PhD. Thesis. Linear Stability Theory of Hypersonic
% Chemically Reacting Viscous Flows
%
% See also getPair_Collision_constants.m
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0


[N_eta,N_spec,~]   = size(T_pair);
N_flds = length(fieldnames(consts)); % number of fields (order of the polynomial)
A = zeros(N_eta,N_spec,N_spec); % allocating
B = A; C = A; D = A; E = A; F = A; G = A; H = A;
switch N_flds % note: there is an additional field due to the fitType
    case 2
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 3
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 4
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
        C = repmat(reshape(consts.C,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 5
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
        C = repmat(reshape(consts.C,[1,N_spec,N_spec]),[N_eta,1,1]);
        D = repmat(reshape(consts.D,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 6
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
        C = repmat(reshape(consts.C,[1,N_spec,N_spec]),[N_eta,1,1]);
        D = repmat(reshape(consts.D,[1,N_spec,N_spec]),[N_eta,1,1]);
        E = repmat(reshape(consts.E,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 7
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
        C = repmat(reshape(consts.C,[1,N_spec,N_spec]),[N_eta,1,1]);
        D = repmat(reshape(consts.D,[1,N_spec,N_spec]),[N_eta,1,1]);
        E = repmat(reshape(consts.E,[1,N_spec,N_spec]),[N_eta,1,1]);
        F = repmat(reshape(consts.F,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 8
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
        C = repmat(reshape(consts.C,[1,N_spec,N_spec]),[N_eta,1,1]);
        D = repmat(reshape(consts.D,[1,N_spec,N_spec]),[N_eta,1,1]);
        E = repmat(reshape(consts.E,[1,N_spec,N_spec]),[N_eta,1,1]);
        F = repmat(reshape(consts.F,[1,N_spec,N_spec]),[N_eta,1,1]);
        G = repmat(reshape(consts.G,[1,N_spec,N_spec]),[N_eta,1,1]);
    case 9
        A = repmat(reshape(consts.A,[1,N_spec,N_spec]),[N_eta,1,1]);
        B = repmat(reshape(consts.B,[1,N_spec,N_spec]),[N_eta,1,1]);
        C = repmat(reshape(consts.C,[1,N_spec,N_spec]),[N_eta,1,1]);
        D = repmat(reshape(consts.D,[1,N_spec,N_spec]),[N_eta,1,1]);
        E = repmat(reshape(consts.E,[1,N_spec,N_spec]),[N_eta,1,1]);
        F = repmat(reshape(consts.F,[1,N_spec,N_spec]),[N_eta,1,1]);
        G = repmat(reshape(consts.G,[1,N_spec,N_spec]),[N_eta,1,1]);
        H = repmat(reshape(consts.H,[1,N_spec,N_spec]),[N_eta,1,1]);
    otherwise
        error(['the number of fields in consts ',num2str(N_flds),' is not supported.']);
end

lnT = log(T_pair);
Prop_sl = A + B.*lnT + C.*lnT.^2 + D.*lnT.^3 + E.*lnT.^4 + F.*lnT.^5 + G.*lnT.^6 + H.*lnT.^7;
if nargout>1
    dPropsl_dlnT = B + 2*C.*lnT + 3*D.*lnT.^2 + 4*E.*lnT.^3 + 5*F.*lnT.^4 + 6*G.*lnT.^5 + 7*H.*lnT.^6;
    dPropsl_dT = dfdlnx_2_dfdx(T_pair,dPropsl_dlnT);
end
if nargout>2
    dPropsl_dlnT2 = 2*C + 6*D.*lnT + 12*E.*lnT.^2 + 20*F.*lnT.^3 + 30*G.*lnT.^4 + 42*H.*lnT.^5;
    [~,dPropsl_dT2] = dfdlnx_2_dfdx(T_pair,dPropsl_dlnT,dPropsl_dlnT2);
end

end % eval_Fit_PolyLog.m
