function lMax = ismax(x,varargin)
% ISMAX(x) returns a logical matrix with the position of the maximum
%
% ISMAX(x,dim) allows to fix in which dimension the maximum is to be
% computed
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if isempty(varargin)
    dim = 1;
else
    dim = varargin{1};
end

[~,b] = max(x, [], dim);                                    % position of maximums
sizes = size(x);                                            % sizes of x in each dimension
Ndim = length(sizes);                                       % number of dimensions
dim_vec = 1:Ndim;                                           % vector of indices for every dimension
nonMaxDims = dim_vec(dim_vec~=dim);                         % vector with the dimensions over which the maximum is not computed
sizes_nonMaxDim = sizes(dim_vec~=dim);                      % sizes of the dimensions over which the maximum is not computed
onesDim = ones(1,Ndim-1);                                   % vector with a 1 for every dimension over which the maximum is not computed
input_sub2ind = cell(Ndim,1);                               % allocating cell
for ii=1:Ndim-1                                             % looping non-maximum dimensions
    vecii = 1:sizes_nonMaxDim(ii);                              % position vector for dimension ii
    index_reshape = onesDim;                                    % vector that will have all 1s except for the dimension ii
    index_reshape(ii) = sizes_nonMaxDim(ii);                    % in the dimension ii it will have the size of x in that dimension
    if length(index_reshape)==1
        index_reshape = [index_reshape,1];                          % making sure index_reshape is 2D otherwise it cannot be used for reshape
    end
    index_repmat = sizes_nonMaxDim;                             % vector that will have all the dimension's sizes except for in the dimension ii
    index_repmat(ii) = 1;                                       % in the dimension ii it will have a 1
    if length(index_repmat)==1
        index_repmat = [index_repmat,1];                            % making sure index_repmat is 2D otherwise it cannot be used for repmat
    end
    matii = repmat(reshape(vecii,index_reshape),index_repmat);  % we now make a matrix by repeating the indices of dimension ii over all the others
    input_sub2ind{nonMaxDims(ii)} = matii(:);                   % we make a cell with all the matrices, but in column form
end                                                             % this cell will be the input to sub2ind
input_sub2ind{dim} = b(:);                                  % in the dimension over which the maximum is computed, we put the position of the maximum in column form

lMax = false(sizes);                                        % allocating logical matrix
lMax(sub2ind(sizes,input_sub2ind{:})) = true;               % making the position of all maximums true