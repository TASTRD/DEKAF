/* [wxMaxima batch file version 1] [ DO NOT EDIT BY HAND! ]*/
/* [ Created with wxMaxima version 14.12.1 ] */

/* [wxMaxima: input   start ] */
/* zebraGaryGoat is a wxMaxima script to derive the expressions to be used for the  */
/* initial guess on the non-dimensional enthalpy profile in DEKAF. The expression   */
/* is a second-order polynomial times an exponential with a second order polynomial.*/
/* The coefficient on the highest-order polynomial term in the exponential is fixed */
/* to be positive by defining it as a square.                                       */
/*   Only the gw and dg/deta|_w conditions are satisfied at the wall - not the      */
/* d2g/deta2|_w, since it was giving problems when using the lsqfit function.       */
/*                                                                                  */
/* Author(s): Fernando Miro Miro & Ethan Beyak                                      */
/* Date: December 2017                                                              */

gfit(x,alfa,bravo,charlie,delta,echo,foxtrot) := (alfa*x^3 + bravo*x^2 + charlie*x + delta)*exp(- echo^2*x^2 + foxtrot*x)+1;

/* We impose the wall g and dg/deta conditions */
solut: expand(solve([   gfit(0,alfa,bravo,charlie,delta,echo,foxtrot) = gw,
                        subst(0,x,diff(gfit(x,alfa,bravo,charlie,delta,echo,foxtrot),x,1)) = gary,
                        subst(0,x,diff(gfit(x,alfa,bravo,charlie,delta,echo,foxtrot),x,2)) = goat],[delta,foxtrot,bravo]));

/* extract the solution */
delta_good: part(solut[1],1,2);
foxtrot_good: part(solut[1],2,2);
bravo_good: part(solut[1],3,2);

/* replug them into gfit and its derivatives, to get the final expressions to       */
/* implement in matlab (which we transform to matlab-readable - strings)            */
string(gfit(x,alfa,bravo_good,charlie,delta_good,echo,foxtrot_good));
string(ev(factor(diff(gfit(x,alfa,bravo_good,charlie,delta_good,echo,foxtrot_good),x,1)),ev));
string(ev(factor(diff(gfit(x,alfa,bravo_good,charlie,delta_good,echo,foxtrot_good),x,2)),ev));
/* [wxMaxima: input   end   ] */

/* Maxima can't load/batch files which end with a comment! */
"Created with wxMaxima"$
