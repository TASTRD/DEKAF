% Sample launch script to compute the compressible self-similar boundary
% layer profile using VESTA's self-similar BL solver
%
% Author: Fernando Miro Miro
% Date: December 2015

clear;
close all;
addpath(genpath('/home/miromiro/workspace/VESTA/'));
addpath(genpath('/home/miromiro/workspace/DEKAF/SourceCode/'));
M_e = 3;
T_e = 300;
gam = 1.4;
Ec = (gam-1)*M_e^2;
% The first run is going to be adiabatic, and then we will ramp down
% the G_bc until we reach the equivalent to gw of 0.2
options.H_F = true;
options.G_bc = 0;
options.tol = 1e-15;
options.L = 40;
options.N = 10001;
options.s_new = 1.0;
options.beta = 0;
options.import_sol = false;
options.FileName = 'M12T300p4000gw0.0067114fw0.mat';
options.fluid = 'air';
options.display = false;
%options.M_e = 5;
options.M_e = M_e;
options.T_e = T_e;
options.p_e = 4000;
options.save = false;
compr_profile = CBL_profile (options);

%load('DEKAF_M0p5.mat');
%%
eta = linspace(0,options.L,options.N);
figure
plot(compr_profile.g,eta,intel.g,intel.eta,'--');