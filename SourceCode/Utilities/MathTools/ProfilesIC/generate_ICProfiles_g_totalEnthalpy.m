% For a given input data, get an least mean square fit
% Author(s):    Fernando Miro Miro
%{
close all;
clear;

addpath(genpath('/home/miromiro/workspace/DEKAF/SourceCode/'));

% FIXME: Comments: point to the zebraGaryGoat.wxm for the derivation of
% these beautiful expressions

% Define curve fit expression as (ax^3 + bx^2 + cx + d)*exp(-e^2*x^2 + fx) + 1
% where d & f are obtained from satisfying the gw and gary (dg/deta|_w) conditions
g_func = @(A,eta,gw,gary,goat) poly3exp2_g_func(A,eta,gw,gary,goat);

dg_deta_func = @(A,eta,gw,gary,goat) poly3exp2_dg_deta_func(A,eta,gw,gary,goat);

dg_deta2_func = @(A,eta,gw,gary,goat) poly3exp2_dg_deta2_func(A,eta,gw,gary,goat);

% Load the data
dataStruc = load('Profiles4initialGuess_VESTA.mat');
%}
%%
% g and each of its derivatives is a 3d matrix: dimensioned (value,M_e,T_e)
% That is:
% 1st dim g, dg_deta, dg_deta2 (for g_mat, dg_deta_mat, dg_deta2_mat)
% 2nd dim Mach edge
% 3rd dim Temperature edge
% FIXME: Comments on M_mat and gw_mat

eta             = dataStruc.eta';           % size (N_eta x 1)
g_mat           = dataStruc.g_mat;          % size (N_eta x N_M x N_gw)
dg_deta_mat     = dataStruc.dg_deta_mat;    % size (N_eta x N_M x N_gw)
dg_deta2_mat    = dataStruc.dg_deta2_mat;   % size (N_eta x N_M x N_gw)
f_mat           = dataStruc.f_mat;          % size (N_eta x N_M x N_gw)
df_deta_mat     = dataStruc.df_deta_mat;    % size (N_eta x N_M x N_gw)
df_deta2_mat    = dataStruc.df_deta2_mat;   % size (N_eta x N_M x N_gw)
df_deta3_mat    = dataStruc.df_deta3_mat;   % size (N_eta x N_M x N_gw)
M_mat           = dataStruc.M_mat;          % size (N_M x N_gw), repeated in 2nd dimension
gw_mat          = dataStruc.gw_mat;         % size (N_M x N_gw)

N_eta = length(eta);

normOrder = 6;

% initial guess for coefficients
%A0_0 = [0 1 1 0]; % no goat
A0_0 = [0 1 1]; % with goat
A0 = A0_0;
A = zeros([length(A0) size(g_mat,2) size(g_mat,3)]);
B = zeros([length(A0) size(g_mat,2) size(g_mat,3)]);
cnt = 0;
loop_cnt = 0;

% Plotting sizes and options
plotsYesNo = false;
total = size(g_mat,2)*size(g_mat,3);
N = 1; Nx = 6; Ny = 9;
opts = optimset('Display','off');
N_gw = size(g_mat,3);
if plotsYesNo
    figure(1);figure(2);figure(3);
    figure(4);figure(5);figure(6);
end
% Loop over the data
for idx_M=1:size(g_mat,2)
    if plotsYesNo
        clf(1);clf(2);clf(3);
        clf(4);clf(5);clf(6);
    end
    for idx_gw=N_gw:-1:1
        % intial guesses and counters
        A0 = A0_0;
        %cnt = cnt + 1;
        loop_cnt = loop_cnt + 1;

        if M_mat(idx_M,idx_gw)<7 % we observed that the initial guess for A should change with the mach number
            lim_exp = 0.5;
        else
            lim_exp = 0.75;
        end

        % Extract VESTA solution
        g = g_mat(:,idx_M,idx_gw);
        dg_deta = dg_deta_mat(:,idx_M,idx_gw);
        dg_deta2 = dg_deta2_mat(:,idx_M,idx_gw);
        f = f_mat(:,idx_M,idx_gw);
        df_deta = df_deta_mat(:,idx_M,idx_gw);
        df_deta2 = df_deta2_mat(:,idx_M,idx_gw);
        df_deta3 = df_deta3_mat(:,idx_M,idx_gw);

        % Perform nonlinear least squares curve fit
        gw = g(1);
        gary = dg_deta_mat(1,idx_M,idx_gw);
        goat = dg_deta2_mat(1,idx_M,idx_gw);
        fw = f(1);
        felix = df_deta_mat(1,idx_M,idx_gw);
        fox = df_deta2_mat(1,idx_M,idx_gw);
        frog = df_deta3_mat(1,idx_M,idx_gw);

        % is the coefficient on the eta greater than the coefficient on the
        % eta squared? (could lead to exponential growth)
        if (gary/(gw-1)-A0(2)/(gw-1)) > lim_exp*A0(3)^2
            A0(2) = lim_exp*(gary - A0(3)^2*(gw-1));
            %A0(3) = ceil(sqrt(gary./(gw-1)-A0(2)./(gw-1)));
        end
        %{%
        tol = 1e-3;
        if ~(abs(g(1)-1)<tol)
            A(:,idx_M,idx_gw) = lsqcurvefit(@(A,x)g_func(A,x,gw,gary,goat),A0,eta,g,[],[],opts)'; % column of coefficients
        else
            % Set quadratic multiplier to zero
            A(:,idx_M,idx_gw)=0;
            gw = g(1)+eps;
            gary = 0;
            goat = 0;
        end
        B(:,idx_M,idx_gw) = lsqcurvefit(@(A,x)g_func(A,x,felix,fox,frog),A0_0,eta,df_deta,[],[],opts)'; % column of coefficients
        %}
        % evaluating fitted functions to compare against original
        g_fit = g_func(A(:,idx_M,idx_gw),eta,gw,gary,goat);
        dg_deta_fit = dg_deta_func(A(:,idx_M,idx_gw),eta,gw,gary,goat);
        dg_deta2_fit = dg_deta2_func(A(:,idx_M,idx_gw),eta,gw,gary,goat);

        df_deta_fit = g_func(B(:,idx_M,idx_gw),eta,felix,fox,frog);
        df_deta2_fit = dg_deta_func(B(:,idx_M,idx_gw),eta,felix,fox,frog);
        df_deta3_fit = dg_deta2_func(B(:,idx_M,idx_gw),eta,felix,fox,frog);

        rms.g(idx_M,idx_gw) = norm((g_fit - g)./max(g),normOrder)./N_eta^(1/normOrder);
        rms.dg_deta(idx_M,idx_gw) = norm((dg_deta_fit - dg_deta)./max(abs(dg_deta)),normOrder)./N_eta^(1/normOrder);
        rms.dg_deta2(idx_M,idx_gw) = norm((dg_deta2_fit - dg_deta2)./max(abs(dg_deta2)),normOrder)./N_eta^(1/normOrder);

        rms.df_deta(idx_M,idx_gw) = norm((df_deta_fit - df_deta)./max(abs(df_deta)),normOrder)./N_eta^(1/normOrder);
        rms.df_deta2(idx_M,idx_gw) = norm((df_deta2_fit - df_deta2)./max(abs(df_deta2)),normOrder)./N_eta^(1/normOrder);
        rms.df_deta3(idx_M,idx_gw) = norm((df_deta3_fit - df_deta3)./max(abs(df_deta3)),normOrder)./N_eta^(1/normOrder);

        % saving gary and goat for the database
        gary_mat(idx_M,idx_gw)  = gary;
        goat_mat(idx_M,idx_gw)  = goat;
        fw_mat(idx_M,idx_gw)  = fw;
        felix_mat(idx_M,idx_gw)  = felix;
        fox_mat(idx_M,idx_gw)  = fox;
        frog_mat(idx_M,idx_gw)  = frog;

        %A0 = A(:,idx_M,idx_gw); %OLD

        % Plot results
        cnt = idx_gw;
        if mod(cnt,N)==0 && plotsYesNo
            figure(1)
            %             figure(3*(idx_M-1)+1)
            subplot(Nx,Ny,cnt/N);
            plot(dg_deta2,eta,dg_deta2_fit,eta,'-.'); ylim([0 6]);
            hold on
            title(sprintf('M = %.2f, gw = %.4f',M_mat(idx_M,idx_gw),gw_mat(idx_M,idx_gw)));
            if cnt/N == 30
                legend('VESTA','Fitted','location','best');
            end

            figure(2)
            %             figure(3*(idx_M-1)+2)
            subplot(Nx,Ny,cnt/N);
            plot(dg_deta,eta,dg_deta_fit,eta,'-.'); ylim([0 6]);
            hold on
            title(sprintf('M = %.2f, gw = %.4f',M_mat(idx_M,idx_gw),gw_mat(idx_M,idx_gw)));
            if cnt/N == 30
                legend('VESTA','Fitted','location','best');
            end

            figure(4)
            %             figure(3*(idx_M-1)+1)
            subplot(Nx,Ny,cnt/N);
            plot(df_deta3,eta,df_deta3_fit,eta,'-.'); ylim([0 6]);
            hold on
            title(sprintf('M = %.2f, gw = %.4f',M_mat(idx_M,idx_gw),gw_mat(idx_M,idx_gw)));
            if cnt/N == 30
                legend('VESTA','Fitted','location','best');
            end

            figure(5)
            %             figure(3*(idx_M-1)+2)
            subplot(Nx,Ny,cnt/N);
            plot(df_deta2,eta,df_deta2_fit,eta,'-.'); ylim([0 6]);
            hold on
            title(sprintf('M = %.2f, gw = %.4f',M_mat(idx_M,idx_gw),gw_mat(idx_M,idx_gw)));
            if cnt/N == 30
                legend('VESTA','Fitted','location','best');
            end

            figure(6)
            %             figure(3*(idx_M-1)+3)
            subplot(Nx,Ny,cnt/N);
            plot(df_deta,eta,df_deta_fit,eta,'-.'); ylim([0 6]);
            hold on
            title(sprintf('M = %.2f, gw = %.4f',M_mat(idx_M,idx_gw),gw_mat(idx_M,idx_gw)));
            if cnt/N == 30
                legend('VESTA','Fitted','location','best');
            end

            figure(3)
            %             figure(3*(idx_M-1)+3)
            subplot(Nx,Ny,cnt/N);
            plot(g,eta,g_fit,eta,'-.'); ylim([0 6]);
            hold on
            title(sprintf('M = %.2f, gw = %.4f',M_mat(idx_M,idx_gw),gw_mat(idx_M,idx_gw)));
            if cnt/N == 30
                legend('VESTA','Fitted','location','best');
            end

            pause(0.5)
        end
        %A0 = A(:,idx_M,1); %OLD
        fprintf('Percent done: %.4f\n',loop_cnt/total*100);
    end
end
%%
%{%
A_mat = permute(A,[2,3,1]); % A(3,idx_M,idx_gw) -> (N_M x N_gw x 3)
B_mat = permute(B,[2,3,1]); % B(3,idx_M,idx_gw) -> (N_M x N_gw x 3)
save('polyexp_db_goatVersion_totalEnthalpy.mat','M_mat','gw_mat','A_mat','goat_mat','gary_mat','B_mat','fw_mat','felix_mat','fox_mat','frog_mat','rms');
%save('polyexp_db.mat','M_mat','gw_mat','A_mat','gary_mat','rms');

figure; surf(M_mat,gw_mat,log(rms.g)); xlabel('M'); ylabel('g_w'); zlabel('log(g)');
figure; surf(M_mat,gw_mat,log(rms.dg_deta)); xlabel('M'); ylabel('g_w'); zlabel('log(dg/deta)');
figure; surf(M_mat,gw_mat,log(rms.dg_deta2)); xlabel('M'); ylabel('g_w'); zlabel('log(d2g/deta2)');

figure; surf(M_mat,gw_mat,log(rms.df_deta)); xlabel('M'); ylabel('g_w'); zlabel('log(df/deta)');
figure; surf(M_mat,gw_mat,log(rms.df_deta2)); xlabel('M'); ylabel('g_w'); zlabel('log(d2f/deta2)');
figure; surf(M_mat,gw_mat,log(rms.df_deta3)); xlabel('M'); ylabel('g_w'); zlabel('log(d3f/deta3)');
%}