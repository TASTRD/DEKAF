function dg_deta = poly3exp2_dg_deta_func(A,eta,gw,gary,goat)
% poly3exp2_dg_deta_func(A,eta,gw,gary) computes the initial guess for the
% first derivative with eta of the non-dimensional enthalpy profile, for a
% given eta distribution, non-dimensional wall enthalpy (gw), and non-
% dimensional wall-enthalpy-derivative (gary). A must be a vector of size 4
% containing the different coefficients in the equation.
%
% Author(s): Fernando Miro Miro & Ethan Beyak
% GNU Lesser General Public License 3.0

%dg_deta = -(2.*A(1).*A(4).^2.*gw.*eta.^4-2.*A(1).*A(4).^2.*eta.^4+2.*A(2).*A(4).^2.*gw.*eta.^3-A(1).*gary.*eta.^3-2.*A(2).*A(4).^2.*eta.^3+A(1).*A(3).*eta.^3+2.*A(3).*A(4).^2.*gw.*eta.^2-3.*A(1).*gw.*eta.^2-A(2).*gary.*eta.^2-2.*A(3).*A(4).^2.*eta.^2+A(2).*A(3).*eta.^2+3.*A(1).*eta.^2+2.*A(4).^2.*gw.^2.*eta-4.*A(4).^2.*gw.*eta-2.*A(2).*gw.*eta-A(3).*gary.*eta+2.*A(4).^2.*eta+A(3).^2.*eta+2.*A(2).*eta-gary.*gw+gary).*exp(1).^(-A(4).^2.*eta.^2+gary.*eta./(gw-1)-A(3).*eta./(gw-1))./(gw-1);

dg_deta =  -(4.*A(1).*A(3).^2.*gw.^2.*eta.^4-8.*A(1).*A(3).^2.*gw.*eta.^4+4.*A(1).*A(3).^2.*eta.^4+4.*A(3).^4.*gw.^3.*eta.^3+2.*A(3).^2.*goat.*gw.^2.*eta.^3-12.*A(3).^4.*gw.^2.*eta.^3-4.*A(3).^2.*goat.*gw.*eta.^3-2.*A(3).^2.*gary.^2.*gw.*eta.^3-2.*A(1).*gary.*gw.*eta.^3+12.*A(3).^4.*gw.*eta.^3+2.*A(2).^2.*A(3).^2.*gw.*eta.^3+2.*A(1).*A(2).*gw.*eta.^3+2.*A(3).^2.*goat.*eta.^3+2.*A(3).^2.*gary.^2.*eta.^3+2.*A(1).*gary.*eta.^3-4.*A(3).^4.*eta.^3-2.*A(2).^2.*A(3).^2.*eta.^3-2.*A(1).*A(2).*eta.^3-2.*A(3).^2.*gary.*gw.^2.*eta.^2+6.*A(2).*A(3).^2.*gw.^2.*eta.^2-6.*A(1).*gw.^2.*eta.^2-gary.*goat.*gw.*eta.^2+A(2).*goat.*gw.*eta.^2+4.*A(3).^2.*gary.*gw.*eta.^2-12.*A(2).*A(3).^2.*gw.*eta.^2+12.*A(1).*gw.*eta.^2+gary.*goat.*eta.^2-A(2).*goat.*eta.^2+gary.^3.*eta.^2-A(2).*gary.^2.*eta.^2-2.*A(3).^2.*gary.*eta.^2-A(2).^2.*gary.*eta.^2+6.*A(2).*A(3).^2.*eta.^2+A(2).^3.*eta.^2-6.*A(1).*eta.^2-2.*goat.*gw.^2.*eta+4.*goat.*gw.*eta+2.*gary.^2.*gw.*eta-2.*A(2).*gary.*gw.*eta-2.*goat.*eta-2.*gary.^2.*eta+2.*A(2).*gary.*eta-2.*gary.*gw.^2+4.*gary.*gw-2.*gary).*exp(1).^(-A(3).^2.*eta.^2+gary.*eta./(gw-1)-A(2).*eta./(gw-1))./(2.*(gw-1).^2) ;
