function g = poly3exp2_g_func(A,eta,gw,gary,goat)
% poly3exp2_g_func(A,eta,gw,gary) computes the initial guess for the non-
% dimensional enthalpy profile, for a given eta distribution,
% non-dimensional wall enthalpy (gw), and non-dimensional wall-enthalpy-
% derivative (gary). A must be a vector of size 4 containing the different
% coefficients in the equation.
%
% Author(s): Fernando Miro Miro & Ethan Beyak
% GNU Lesser General Public License 3.0

%g = (A(1).*eta.^3+A(2).*eta.^2+A(3).*eta+gw-1).*exp(1).^((gary./(gw-1)-A(3)./(gw-1)).*eta-A(4).^2.*eta.^2)+1 ;
g = (A(1).*eta.^3+(2.*A(3).^2.*gw.^2./(2.*gw-2)+goat.*gw./(2.*gw-2)-4.*A(3).^2.*gw./(2.*gw-2)-goat./(2.*gw-2)-gary.^2./(2.*gw-2)+2.*A(3).^2./(2.*gw-2)+A(2).^2./(2.*gw-2)).*eta.^2+A(2).*eta+gw-1).*exp(1).^((gary./(gw-1)-A(2)./(gw-1)).*eta-A(3).^2.*eta.^2)+1 ;
