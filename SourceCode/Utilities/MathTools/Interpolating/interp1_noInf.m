function y_out = interp1_noInf(x,y,x_out,varargin)
% interp1_noInf works the same as matlab's interp1 yet avoiding infinite
% and NaN values.
%
% Usage:
%   (1)
%       y_out = interp1_noInf(x,y,x_out)
%
%   (2)
%       y_out = interp1_noInf(..,in1,in2,...)
%       |--> allows multiple inputs like in matlab's <a href="matlab:help interp1"> interp1 </a>
%
% Author: Fernando Miro Miro
% Date: June 2019
% GNU Lesser General Public License 3.0

bIdx = isnan(y) | isinf(y);
Nx = length(x);
if length(y(:))==1 && length(x(:))==1;                  y_out = y;          % only one position, so we take it
else                                                                        % more than one position
    if      size(y,1)==Nx;      bIdx_x = any(bIdx,2);       y = y(~bIdx_x,:);   % choosing non-Inf and non-NaN values accross dim. 2
    elseif  size(y,2)==Nx;      bIdx_x = any(bIdx,1);       y = y(:,~bIdx_x);   % choosing non-Inf and non-NaN values accross dim. 1
    end
    y_out = interp1(x(~bIdx_x),y,x_out,varargin{:});
end

end % interp1_noInf