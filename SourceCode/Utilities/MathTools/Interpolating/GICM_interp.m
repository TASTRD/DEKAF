function [intel,eta2,varargout] = GICM_interp(eta_i,eta_max,N,fields2interp,U_e,xi,intel,varargin)
% GICM_interp(eta_i,eta_max,N,fields2interp,U_e,xi,intel,y2,D_y2) performs
% the Groot-Illingworth-Chebyshev-Malik interpolation.
%
% It interpolates all fields in fields2interp from the eta domain, defined
% by Chebyshev collocation points mapped with Malik's mapping, onto a
% physical domain defined by y2 and D_y2 (the derivation matrix) after
% undoing the Illingworth transformation, using Groot's iterative method.
%
% The way it works is the following:
% We have a solution (several fields) as a function of the self-similar
% variable eta. This eta is a distribution of N Chebyshev collocation
% points X1 which have been mapped using Malik's mapping (defined by eta_i
% & eta_max). This eta1 can also be corresponded, through the Illingsworth
% transformation, onto a physical domain y1. This correspondance is
% determined by the density rho obtained from solving the boundary layer,
% and a few edge quantities (U_e and xi). rho will therefore be known on
% these two grids: the computational X1, the self-similar eta1 and the
% physical y1. In fact, the rho vector will be the same, changing only the
% X1-y1-eta1.
%
% What we want is the solution on a new set of points on the physical
% domain y2, which will have to be inputted by the user.
%
% The tricky part of this is that to perform the Chebyshev interpolation,
% we need the input grid y2 in the computational Chebyshev domain (X2).
% We need X2, so that we can interpolate from X1 to X2 using the Chebyshev
% polynomials, but passing from y2 to X2 requires having rho on this y2
% grid, let's call it rho2. We therefore initially interpolate rho1 onto
% y2 using 'spline', we use it to get X2 by inverting the Illingworth and
% Malik transformation, and then we interpolate rho1 from X1 to X2 using the
% Chebyshev interpolation. Now this is the crucial bit: even though the
% nodes of X2 are completely different from the nodes of y2, the rho values
% on these nodes have not changed. That is, rho2 on X2 is identical to rho2
% on y2. We then use this new rho2 to get a new X2, and continue looping.
% The iterative process finishes when the X2 grid converges.
%
% GICM_interp(eta_i,eta_max,N,fields2interp,U_e,xi,intel), without a
% specified grid onto which the interpolation should be done, interpolates
% onto the Chebyshev-Malik grid that would be generated using the y_i and
% y_max coming from eta_i and eta_max after undoing the Illingworth
% transformation. One might think that these two meshes would be the same,
% but the fact that the original domain y1 has gone through the undoing of
% the Illingworth transformation and the new domain y2 has not, makes the
% points be scaled differently. Namely, it is not the same to do:
%
%    X1  --Malik-->  eta1  --Illingworth-->  y1
%    X1  --Malik-->   y2
%
% even if the y_i and y_max are the same for y1 and y2.
%
% GICM_interp(...,options) allows to pass a structure with several options
% such as:
%      .GICMresmult: multiplier on the output resolution with respect to
%       the input resolution: N.
%       default: 1
%      .GICMtol: tolerance for the convergence of the Chebyshev grid
%       default: eps
%      .GICMitmax: maximum iteration after which the iteration should stop
%       default: 50
%      .GICMsuffix: string with the suffix that the interpolated variables
%       should have.
%       default '_GICM'
%       default: 50
%      .GICMfirstInterp: string identifying which method to use for the
%       first (non-spectral) interpolation. It must be a string that can
%       be understood by matlab's interp1 function
%       default: 'spline'
%      .display: boolean to determine if information should be displayed by
%       the function
%       default: true
%
% The outputs are [intel,eta2,conv] - the interpolated fields, the new eta
% domain, and the convergence reached during the GICM iterative
% interpolation. The last output is optional.
%
% The Probstein-Elliot transformation may now also be performed, making
% GICM effectively into GIPECM. The workflow in that case is:
%
%    X1  --Malik-->  eta1  --Illingworth-->  yBar1  --ProbsteinElliot--> y1
%    X1  --Malik-->   y2
%
% Usage:
%   (1)
%       [intel,eta2] = GICM_interp(eta_i,eta_max,N,fields2interp,U_e,xi,intel)
%       |--> uses grid with y_min, y_max and y_i from DEKAF
%
%   (2)
%       [intel,eta2] = GICM_interp(eta_i,eta_max,N,fields2interp,U_e,xi,intel,y2,D_y2)
%       |--> allows to manually specify the grid onto which the
%       interpolation must be done (through the point distribution y2 and
%       the derivation matrix along it D_y2).
%       IMPORTANT: the derivation matrix being passed must allow for an
%       integration matrix to be obtained from it using integral_mat.
%       Centered difference schemes for instance do not allow it. If a
%       centered difference method is to be ultimately used in the
%       stability solver, it is recommended to nevertheless use a forward
%       or backward FD differentiation matrix for the GICM.
%
%   (3)
%       [intel,eta2] = GICM_interp(...,options)
%       |--> allows to pass an options structure
%
%   (4)
%       [intel,eta2,conv] = GICM_interp(...)
%       |--> returns a convergence boolean also
%
%   (5)
%       [...] = GICM_interp(...,'coordsys','cone',cylCoordFuncs,xPhys)
%       |--> chooses a conical coordinate system (the Probstein-Elliot
%       transformation will also be undone), making GICM into GIPECM.
%       The additional cylCoordFuncs input is a structure containing
%       various functions to compute various geometrical quantities related
%       to cylindrical coordinates:
%           .rc(xc)             [m]
%               spanwise radius of curvature
%           .alphac(xc)         [deg]
%               streamwise inclination angle
%           .Ixc_rc2(xc)        [m]
%               integral of (rc/L)^2 wrt xc
%           .Dxc_rc(xc)         [-]
%               derivative of rc wrt xc
%           .Dxc_alphac(xc)     [1/m]
%               derivative of alphac wrt xc
%       If the DEKAF solution was produced before this generalization was
%       introduced, simply run:
%           cylCoordFuncs = populateProbsteinElliotFunctions(intel,options);
%       to generate it.
%
%   (6)
%       [...] = GICM_interp(...,'coordsys','cartesian2D')
%       |--> does the same as GICM_interp(...)
%
%   (7)
%       [...] = GICM_interp(...,'passDCheb',D1,Xcheb)
%       |--> pass the Cheb matrices and points
%
%   (7)
%       [...] = GICM_interp(...,'clipExtrap')
%       |--> performs a clipped extrapolation, in the sense that whatever
%       point outside of the domain, is taken to have the last value.
%
%   (8)
%       [...] = GICM_interp(...,'slowInterp')
%       |--> uses the "slow" interpolation method, without creating a
%       unified matrix with all the fields, but looping through them one at
%       a time.
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

% Checking inputs
[varargin,bCoordsys,idx_match] = find_flagAndRemove('coordsys',varargin);
if bCoordsys
    coordsys = varargin{idx_match};
    switch coordsys
        case 'cartesian2D'
            % nothing else to extract
        case {'cone','cylindricalExternal','cylindricalInternal'}
            cylCoordFuncs = varargin{idx_match+1};
            xPhys = varargin{idx_match+2};
            if ~isstruct(cylCoordFuncs)
                error(['Error: the input to GICM_interp (cylCoordFuncs) is not a structure. ',newline,...
                    'This is most-likely because the DEKAF solution was generated before modifying the coordinate-system transformations. ',newline,...
                    'In order to generate it, one must simply run:',newline,newline,...
                    '  cylCoordFuncs = populateProbsteinElliotFunctions(intel,options);']);
            end
        otherwise
            error(['the chosen coordinate system ''',coordsys,''' is currently unsupported.']);
    end
    varargin(idx_match:idx_match+2) = []; % clearing varargin
else
    coordsys = 'cartesian2D'; % default
end

[varargin,bDCheb,idx_match] = find_flagAndRemove('passDCheb',varargin);
if bDCheb
    DM = varargin{idx_match};
    X1 = varargin{idx_match+1};
    varargin(idx_match:idx_match+1) = []; % clearing varargin
else
    [X1,DM] = chebDif(N,1);
end
[varargin,bClipExtrap] = find_flagAndRemove('clipExtrap',varargin);
[varargin,bSlowInterp] = find_flagAndRemove('slowInterp',varargin);

switch length(varargin)
    case 0
        custom_grid = false;
        options = struct;
    case 1
        custom_grid = false;
        options = varargin{1};
    case 2 % in this case the user is inputing the domain it wants to
        % interpolate the fields onto, together with the integration matrix
        custom_grid = true;
        y2 = varargin{1};
        D_y2 = varargin{2};
        options = struct;
    case 3
        custom_grid = true;
        y2 = varargin{1};
        D_y2 = varargin{2};
        options = varargin{3};
    otherwise
        error('wrong number of inputs or incorrect varargin usage!');
end
options = standardvalue(options,'GICMfirstInterp','spline');
options = standardvalue(options,'GICMresmult',1);
options = standardvalue(options,'GICMtol',eps);
options = standardvalue(options,'GICMitmax',50);
options = standardvalue(options,'display',true);
if (~isfield(options,'GICMsuffix'))
    options.GICMsuffix = '_GICM';
end

if U_e ~= 0
    Ue_sqrt2xi = U_e./sqrt(2*xi);
else
    if ~isfield(intel, 'Ue_sqrt2xi0')
        error(['Stagnation flow has been detected in your boundary layer\n',...
            'In order to perform GICM, the field ''Ue_sqrt2xi0'' must be supplied to intel!']);
    end
    % This term Ue_sqrt2xi represents the limiting case of U_e divided by
    % sqrt(2*xi) for stagnation flow only (when U_e = K*x)
    Ue_sqrt2xi = intel.Ue_sqrt2xi0;
end
% computational domain
[eta1,D_eta] = MappingMalik_uptoD4( eta_max,eta_i,X1',DM,0*DM,0*DM,0*DM);

% we want to now pass to the physical domain
rho1 = intel.rho;
I_eta  = integral_mat(D_eta,'du');
deta_dy = Ue_sqrt2xi.*rho1;
y1 = I_eta*(1./deta_dy);

% accounting for Probstein-Elliot transformation for conical flows
switch coordsys                                                     % depending on the coordinate system
    case 'cartesian2D'                                                  % for cartesian
    case {'cone','cylindricalExternal','cylindricalInternal'}           % for cylindrical coordinates
    [xBar] = get_xyTransformed(   xPhys,[],coordsys,cylCoordFuncs);   % obtaining xBar
    [~,y1] = get_xyInvTransformed(xBar, y1,coordsys,cylCoordFuncs);   % obtaining y1 from yBar
    otherwise                                                           % for anything else break
        error(['the chosen coordsys ''',coordsys,''' is not supported']);
end

% we now obtain the grid we want to end up with
%{%
if ~custom_grid % if we didn't input a grid, then we must generate it from the

    if options.GICMresmult > 1
        % computational domain (of possibly higher resolution)
        [Xr,DMr] = chebDif(N*options.GICMresmult,1);
    else
        % do not apply any resolution scaling
        Xr = X1; DMr = DM;
    end

    % once in the physical domain, we can obtain the value of y_i corresponding
    % to eta_i
    y_i = interp1(eta1,y1,eta_i,'spline');
    y_max = max(y1);

    [y2,D_y2] = MappingMalik_uptoD4( y_max,y_i,Xr',DMr,0*DMr,0*DMr,0*DMr);
end

% accounting for Probstein-Elliot transformation for conical flows
switch coordsys                                                     % depending on the coordinate system
    case 'cartesian2D'                                                  % for cartesian
    case {'cone','cylindricalExternal','cylindricalInternal'}           % for cylindrical coordinates
    [~,~,~,~,~,dy2Bar_dy2] = get_xyTransformed(xPhys,y2,coordsys,cylCoordFuncs);    % obtaining obtaining dy2Bar_dy2
    D_y2 = diag(1./dy2Bar_dy2) * D_y2;                                              % obtaining D_yBar2, which is what we actually need
    otherwise                                                           % for anything else break
        error(['the chosen coordsys ''',coordsys,''' is not supported']);
end

% obtaining integration matrix from the differentiation one
[~,idx_0] = min(y2); % position of the wall in the new grid
if idx_0==1 % first point is the zero
    intMethod = 'ud'; % up-down integration
elseif idx_0==length(y2) % last point is the zero
    intMethod = 'du'; % down-up integration
else
    error(['the minimum y was not located neither at the beginning nor at the end of the y vector (pos ',num2str(idx_0),'/',num2str(length(y2)),'. Such point distributions are not supported.']);
end
I_y2 = integral_mat(D_y2,intMethod); % obtaining integration matrix
%}

% checking if our domain is too big
%{%
if max(y2)>max(y1)+options.tol % the chosen domain is too big
    delta_eta = Ue_sqrt2xi * rho1(1) * (max(y2)-max(y1));
    needed_eta = eta_max + delta_eta;
    warningMsg = ['the chosen domain is too large and will therefore need extrapolation. It has a maximum y=', ...
        num2str(max(y2)),' whereas the flowfield''s maximum y position is ',num2str(max(y1)), ...
        '. If you want a solution that can be interpolated onto the chosen grid, you should fix an L = eta_max = ', ...
        num2str(needed_eta)];
    if bClipExtrap
        warningMsg = [warningMsg,newline,'However, since ''clipExtrap'' was chosen, all external values will be fixed to those at y=',num2str(max(y1)),','...
            newline,'with the exception of v, which properly follows linear extrapolation according to the boundary-layer assumptions.'];
    end
    if ~bClipExtrap && options.display
        warning(warningMsg);
    end
end
%}

% we interpolate rho onto it dirtyly
rho2              = zeros(size(y2));                            % allocating
idx4interp        = get_idx4interp(y1,y2,bClipExtrap);          % indices of the points to be interpolated
rho2( idx4interp) = interp1(y1,rho1,y2(idx4interp),options.GICMfirstInterp);   % interpolating
rho2(~idx4interp) = rho1(1)*ones(nnz(~idx4interp),1);           % clipping remainder
N2   = length(y2);               % position-vector length

conv = 1;
it = 0;
X2_prev = 0;
while conv>options.GICMtol && it<options.GICMitmax % we now iterate
    it = it+1;
    % with it, we can undo the illingworth transformation
    eta2 = I_y2*(Ue_sqrt2xi*rho2);

    % we undo the malik mapping kilam
    X2 = kilamMapping(eta_max,eta_i,eta2);

    % we interpolate rho from X1 to X2
    rho2(idx4interp) = chebinterp(rho1,X2(idx4interp));
    if it~=1
        conv = norm(X2-X2_prev,inf);
        X2_prev = X2;
        dispif(['--- Iteration ',num2str(it),' on the Chebyshev interpolation with error: ',num2str(conv)],options.display);
    end
    if isnan(conv)
        error('GICM NaNed');
    end
end

% Now that we have the correct X2 grid, we can interpolate all fields onto
% it, and since the values at the nodes will be identical to those in Y2,
% we effectively have interpollated the fields onto Y2 using Chebyshev
% polynomials.
dispif('--- Performing Chebyshev interpolation...', options.display);
Nflds       = length(fields2interp);                        % number of fields to interpolate
idx4interp  = get_idx4interp(y1,y2,bClipExtrap);            % indices of the points to be interpolated
fieldsPostInterp = cellfun(@(cll)[cll,options.GICMsuffix],fields2interp,'UniformOutput',false); % names for the output structure
ivfld = strcmp('v',fields2interp);
if bSlowInterp
    for ii=1:Nflds
        intel.(fieldsPostInterp{ii})( idx4interp) = chebinterp(intel.(fields2interp{ii}),X2(idx4interp));
        if ivfld(ii)
            % For v from the boundary-layer equations, the asymptote goes linearly: extrapolation isn't nearest neighbor.
            vnoextrap = intel.(fieldsPostInterp{ii})(idx4interp);
            intel.(fieldsPostInterp{ii})(~idx4interp) = interp1(y2(idx4interp),vnoextrap,y2(~idx4interp),'linear','extrap');
        else
            intel.(fieldsPostInterp{ii})(~idx4interp) = repmat(intel.(fields2interp{ii})(1),[nnz(~idx4interp),1]);
        end
    end
else
    intel_mat = struct2mat(intel,fields2interp);                                    % making the chosen fields into a matrix
    intel_mat_interp                = zeros(N2,Nflds);                              % allocating interpolated matrix
    intel_mat_interp( idx4interp,:) = chebinterp(intel_mat,X2(idx4interp));         % interpolating matrix
    intel_mat_interp(~idx4interp,~ivfld) = repmat(intel_mat(1,~ivfld),[nnz(~idx4interp),1]);  % filling values that are clipped off for all fields but v
    % GICM_interp may not have any field that is v, e.g. df_deta, df_deta2 only.
    % In this case, skip these lines to avoid empty indexing.
    if any(ivfld)
        vnoextrap = intel_mat_interp(idx4interp,ivfld);
        intel_mat_interp(~idx4interp,ivfld) = interp1(y2(idx4interp),vnoextrap,y2(~idx4interp),'linear','extrap'); % linearly extrpolate v
    end
    intel = mat2struct(intel_mat_interp,fieldsPostInterp,intel);                       % setting back into a structure
end

% preparing optional outputs.
if nargout==3
    varargout{1} = conv;
end
intel.(['y',options.GICMsuffix]) = y2; % also redefining what the new y is

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function idx4interp = get_idx4interp(y1,y2,bClipExtrap)
% get_idx4interp returns the indices of the points that must be
% interpolated. The others will be clipped.
%
% Usage:
%   (1)
%       idx4interp = get_idx4interp(y1,y2,bClipExtrap)
%
% Inputs and outputs:
%   y1
%       original dimensional y grid
%   y2
%       objective dimensional y grid
%   bClipExtrap
%       boolean determining whether a clipped extrapolation must be
%       performed or not
%   idx4interp
%       indices of the positions in y2 to be used for the interpolation
%       from y1.
%
% This function is embedded to GICM_interp
%
% Date: June 2019
% GNU Lesser General Public License 3.0

if bClipExtrap;         idx4interp = (y2<=max(y1));                             % we want to make a clipped extrapolation
else;                   idx4interp = true(size(y2));                            % we don't want to extrapolate
end

end % get_idx4interp
