function F = matLinearInterp(X0,X1,F0,F1,X)
% matLinearInterp performs a linear interpolation on a series of
% coefficients provided in matrix form.
%
% It essentially does:
%                   F = (1-a)*F0 + a*F1
% where:
%                   a = (X-X0)/(X1-X0)
%
% X0, X1, F0, and F1 can be vectors, such that the interpolation is
% simultaneously performed on various ranges and F values.
%
% Usage:
%   (1)
%       F = matLinearInterp(X0,X1,F0,F1,X)
%
% Inputs and outputs:
%   X0, X1                  (1 x Nrange)
%       limits of the interpolation region.
%   F0, F1                  (1 x Nrange)
%       values of the function to interpolate at the limits of the
%       interpolation region (paired with X0 and X1)
%   X                       (Nx x 1)
%       where the interpolation is to be evaluated
%   F                       (Nx x Nrange)
%       interpolated matrix
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

a = (X-X0)./(X1-X0);
F = (1-a).*F0 + a.*F1;

end % matLinearInterp