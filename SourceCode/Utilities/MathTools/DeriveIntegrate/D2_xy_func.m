function df_dxdy = D2_xy_func(f,D1_eta,D1_xi,D2_eta,deta_dx,dxi_dx,deta_dy)
%D2_XY_FUNC Calculate the mixed partial derivative of a variable with
% respect to x and y by using the chain rule
%
% Let f = f(x,y), where x = x(xi,eta) and y = y(eta)
%
% Inputs:
%   f       -   Variable over 2D domain for which the derivative will be
%               taken. Matrix of size (N_eta x N_x).
%   D1_eta  -   Differentiation matrix of first order with respect to eta.
%               Matrix of size (N_eta x N_eta).
%   D1_xi   -   Differentiation matrix of first order with respect to xi.
%               Matrix of size (N_x x N_x).
%   deta_dx -   Rate of change of eta with respect to x.
%               Matrix of size (N_eta x N_x).
%   dxi_dx  -   Rate of change of xi with respect to x.
%               Matrix of size (N_eta x N_x).
%   deta_dy -   Rate of change of eta with respect to y.
%               Matrix of size (N_eta x N_x).
%
% Outputs:
%   df_dxdy  -  Mixed partial derivative of f with respect to x and y.
%               Matrix of size (N_eta x N_x).
%
% See also D1_x_func.m, D2_x_func.m, eval_dimVars.m
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
%            Koen Groot
% GNU Lesser General Public License 3.0

deta_dxdy   =   D1_x_func(deta_dy,D1_eta,D1_xi,deta_dx,dxi_dx);

df_dxdy     =   deta_dxdy.*(D1_eta*f) + deta_dx.*deta_dy.*(D2_eta*f) + ...
                deta_dy.*dxi_dx.*(D1_xi*(D1_eta*f)')';

end % D2_xy_func.m
