function [A] = fdcoeff_1d(x,x0)
%% *****************************************************************************
% fdcoeff_1d:
%   Computes coefficients of one-dimensional Finite Difference schemes for
%   a point x0 on an uneven stencil using the stencil points in the vector x
%
% Syntax:
%   [A] = fdcoeff_1d(x,x0)
%
% Input:
%   x     : vector with stencil points in ascending order
%   x0    : base point, where derivatives are computed.
%
% Output:
%   A    : r*r array with coefficients of FD schemes on even stencil for the
%          derivatives m = 0...r-1. Note that powers of dx are included in A
%             [ phi_i^(0)   ]         [ phi_{i-sl} ]
%             [       .     ]         [     .      ]
%             [       .     ]         [ phi_{i-1}  ]
%             [ phi_i^(m)   ]   = A * [ phi_i      ]
%             [       .     ]         [ phi_{i+1}  ]
%             [       .     ]         [     .      ]
%             [ phi_i^(r-1) ]         [ phi_{i+sr} ]
%
% Author:   Ludovico Zanus
% Date    : Sep-20-2013
% Version : Final
%*******************************************************************************

%% Initlaize
r   = length(x);     % number of stencil points
dx0 = min(diff(x));  % minimum stencil spacing
x   = x/dx0;         % normalize x w.r.t. dx0
x0  = x0/dx0;        % normalize x0 w.r.t. dx0
M   = zeros(r);      % inverse of A (to be assembled)

%% Assemble M
% Each i-th row of the M matrix contains the coefficients of the Taylor
% series evaluated at the i-th point of the stencil. The M matrix has been
% built going first by rows and then by columns. The x coordinate has been
% normalized by the minimum stencil spacing dx0.
% N.B.: power of dx are included in M

% The calculation of the factorial is expensive. It is more efficient to save it before the double loop
fact = factorial((1:r)-1);
for i=1:r
    for j=1:r
        M(i,j)=1/fact(j)*(x(i)-x0)^(j-1);
    end
end

%% Invert M to compute A
% The A matrix containing the coefficients of one-dimensional Finite
% Difference schemes is found inverting the M matrix calculated before.
% N.B.: the powers of dx are included in A
A=M\eye(size(M));

%% Rescale rows with 1/dx0^(i-1)
% Scale the rows of A with 1/dx0^(i-1) because the x coordinates have been
% normalized before
for i=1:r
    A(i,:)=A(i,:)./dx0^(i-1);
end
