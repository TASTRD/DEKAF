function [idx_x,jj] = getIdxBackwardDiff(ii,n,varargin)
% getIdxBackwardDiff(ii,n) returns the positions of the vector that compose
% the FD stencil for an n-th-order backward-difference method at node ii.
%
% getIdxBackwardDiff(...,'noRepeat') avoids the repetition of the first
% line for backward differences. Instead, a forward stencil is taken at the
% first location.
% IMPORTANT! The differentiation matrix will remain singular despite this.
% It is just that the point where the discrete differentiation is redundant
% is "hidden" within the matrix at another mesh point.
%
% Usage:
%   (1)
%       [idx_x,jj] = getIdxBackwardDiff(ii,n,...)
%       |--> it returns also the point jj where the derivative must be
%       evaluated
%
% See also: getIdxForwardDiff, getIdxCentralDiff
%
% Author: Fernando Miro Miro, based on the work of Ludovico Zanus
% Date: July 2019
% GNU Lesser General Public License 3.0


[varargin,bNoRpt]   = find_flagAndRemove('noRepeat',varargin);      % to avoid repetition near the edges
[derOrder,~]        = parse_optional_input(varargin,'derOrder',1);  % to specify a different differntiation order

jj=ii;
switch derOrder
    case 1                                              % FIRST-ORDER DERIVATIVES
        if bNoRpt && ii<=n+1;       idx_x = 1:n+1;          % if we want to avoid repetition, the first n+1 locations will take the first n+1 grid points to compute the (hybrid-method) derivative
        elseif ii<=2;               idx_x = 1:2; jj=2;      % (repeating) for the two first locations we simply take first-order differences
        elseif ii<=n+1;             idx_x = 1:ii;           % (repeating) if the number of available locations is less than the desired order+1, we take from 1 until ii (all available locations)
        else;                       idx_x = (ii-n):ii;      % for other locations we take as many backward points as the required order+1
        end
    case 2                                              % SECOND-ORDER DERIVATIVES
        if bNoRpt && ii<=n+2;       idx_x = 1:n+2;          % if we want to avoid repetition, the first n+2 locations will take the first n+2 grid points to compute the (hybrid-method) derivative
        elseif ii<=3;               idx_x = 1:3; jj=3;      % (repeating) for the three first locations we simply take first-order differences
        elseif ii<=n+2;             idx_x = 1:ii;           % (repeating) if the number of available locations is less than the desired order+2, we take from 1 until ii (all available locations)
        else;                       idx_x = (ii-n-1):ii;    % for other locations we take as many backward points as the required order+2
        end
    otherwise
        error(['the chosen derOrder (',num2str(derOrder),') is not supported']);
end

end % getIdxBackwardDiff