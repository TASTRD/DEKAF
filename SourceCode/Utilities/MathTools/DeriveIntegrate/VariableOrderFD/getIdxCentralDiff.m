function idx_x = getIdxCentralDiff(ii,n,Nx,varargin)
% getIdxCentralDiff(ii,n,Nx) returns the positions of the vector that
% compose the FD stencil for an n-th-order central-difference method at
% node ii out of Nx.
%
% getIdxCentralDiff(...,'derOrder',m) allows to specify the differentiation
% order of the scheme.
%
% See also: getIdxBackwardDiff, getIdxForwardDiff
%
% Author: Fernando Miro Miro, based on the work of Ludovico Zanus
% Date: July 2019
% GNU Lesser General Public License 3.0


[derOrder,~]        = parse_optional_input(varargin,'derOrder',1);  % to specify a different differntiation order


half_stenc  = n/2;                      % only used for centered differences
switch derOrder
    case 1                                              % FIRST-ORDER DERRIVATIVES
        if ii<=half_stenc;          idx_x = 1:ii+half_stenc+(half_stenc-ii)+1;      % for the first n locations we take forward differences
        elseif ii>=Nx-half_stenc;   idx_x = ii-half_stenc-(half_stenc-(Nx-ii)):Nx;  % for the last n locations we take backward differences
        else;                       idx_x = ii-half_stenc:ii+half_stenc;            % for the middle ones, we take central
        end
    case 2                                              % SECOND-ORDER DERRIVATIVES
        if ii<=half_stenc;          idx_x = 1:ii+half_stenc+(half_stenc-ii)+1;      % for the first n locations we take forward differences
        elseif ii>=Nx-half_stenc;   idx_x = ii-half_stenc-(half_stenc-(Nx-ii)):Nx;  % for the last n locations we take backward differences
        else;                       idx_x = ii-half_stenc:ii+half_stenc;            % for the middle ones, we take central
        end
    otherwise
        error(['the chosen derOrder (',num2str(derOrder),') is not supported']);
end

end % getIdxCentralDiff