function idx_x = getIdxForwardDiff(ii,n,Nx,varargin)
% getIdxForwardDiff(ii,n,Nx) returns the positions of the vector that
% compose the FD stencil for an n-th-order forward-difference method at
% node ii out of Nx.
%
% getIdxForwardDiff(...,'noRepeat') avoids the repetition of the last
% line for forward differences. Instead, a backward stencil is taken at the
% last location.
% IMPORTANT! The differentiation matrix will remain singular despite this.
% It is just that the point where the discrete differentiation is redundant
% is "hidden" within the matrix at another mesh point.
%
% See also: getIdxBackwardDiff, getIdxCentralDiff
%
% Author: Fernando Miro Miro, based on the work of Ludovico Zanus
% Date: July 2019
% GNU Lesser General Public License 3.0


[varargin,bNoRpt]   = find_flagAndRemove('noRepeat',varargin);      % to avoid repetition near the edges
[derOrder,~]        = parse_optional_input(varargin,'derOrder',1);  % to specify a different differntiation order

switch derOrder
    case 1                                              % FIRST-ORDER DERRIVATIVES
        if bNoRpt && ii>=Nx-n;      idx_x = Nx-n:Nx;        % if we want to avoid repetition, the last n+1 locations will take the last n+1 grid points to compute the (hybrid-method) derivative
        elseif ii>=Nx-1;            idx_x = (Nx-1):Nx;      % (repeating) for the two last locations we simply take first-order differences (identical)
        elseif ii>=Nx-n;            idx_x = ii:Nx;          % (repeating) if the distance from the final point is less than the desired order+1, we take from ii until Nx (all final locations)
        else;                       idx_x = ii:(ii+n);      % for other locations we take as many backward points as the required order+1
        end
    case 2                                              % SECOND-ORDER DERRIVATIVES
        if bNoRpt && ii>=Nx-n-1;    idx_x = (Nx-n-1):Nx;    % if we want to avoid repetition, the last n+1 locations will take the last n+1 grid points to compute the (hybrid-method) derivative
        elseif ii>=Nx-2;            idx_x = (Nx-2):Nx;      % (repeating) for the three last locations we simply take first-order differences (identical)
        elseif ii>=Nx-n-1;          idx_x = ii:Nx;          % (repeating) if the distance from the final point is less than the desired order+2, we take from ii until Nx (all final locations)
        else;                       idx_x = ii:(ii+n+1);    % for other locations we take as many backward points as the required order+2
        end
    otherwise
        error(['the chosen derOrder (',num2str(derOrder),') is not supported']);
end

end % getIdxForwardDiff