function [FD,scaling] = FDstencil(order)
%FDSTENCIL Compute backward finite difference stencil of specified order
%
% Inputs:
%   order   -   derivative order, scalar
%               currently handles integers 1 to 6
%
% Outputs:
%   FD      -   row of coefficients of finite difference operator
%               multiplied such that all entries are integers
%               this form is useful to avoid numerical round-off error
%   scaling -   integer, least common denominator of all coefficients
%               so that all entries in FD are integers
%
% Note:
%   The true finite difference operator is FD/scaling, not solely FD
%
% Example usage:
%   Let f_new and f_old be column vectors where f_new is evaluated at the
%   downstream value of xi = xi_new = dxi + xi_old; (dxi > 0)
%   f_old is evaluated at xi = xi_old;
%
%   >> df_dxi = [f_new, f_old]*FDc')/(dxi*sclfactor);
%
% TO-DO:
% GET THE CODE WORKING WITH 2ND DERIVATIVES DIFFERENCE OPERATORS
%
% Blend new code with FDstencil.m
%
% FD21 = [   1    -2     1];
% FD22 = [   2    -5     4    -1];
% FD23 = [  35  -104   114   -56   11] / 12;
% FD24 = [  45  -154   214  -156   61   -10] / 12;
% FD25 = [ 812 -3132  5265 -5080  2970  -972  137] / 180;
% FD26 = [ 938 -4014  7911 -9490  7380 -3618 1019  -126] / 180;
% CD11 = [-1/2 2 -3/2]/ds; % n = 1
% D12 = [3/2 -2 1/2]/ds; % n = n
% CD13 = [1/2 0 -1/2]/ds; % n = 2, n-1
% CD14 = [-1/12 2/3 0 -2/3 1/12]/ds;
% CD21 = [-1 4 -5 2]/(ds*ds); % n = 1
% CD22 = [2 -5 4 -1]/(ds*ds); % n = n
% CD23 = [1 -2 1]/(ds*ds); % n = 2, n-1
% CD24 = [-1/12 4/3 -5/2 4/3 -1/12]/(ds*ds);
%
% Author(s):    Koen Groot
% GNU Lesser General Public License 3.0

switch order
    case 1
        FD = [  1   -1];
        scaling = 1;
    case 2
        FD = [  3   -4   1];
        scaling = 2;
    case 3
        FD = [ 11  -18   9   -2];
        scaling = 6;
    case 4
        FD = [ 25  -48  36  -16   3];
        scaling = 12;
    case 5
        FD = [137 -300 300 -200  75 -12];
        scaling = 60;
    case 6
        FD = [147 -360 450 -400 225 -72 10];
        scaling = 60;
end

end % FDstencil.m
