function df_dx2 = D2_x_func(f,D1_eta,D1_xi,D2_eta,D2_xi,deta_dx,dxi_dx)
%D2_X_FUNC Calculate the second derivative of a variable with
% respect to x twice by using the chain rule
%
% Let f = f(x,y), where x = x(xi,eta) and y = y(eta)
%
% Inputs:
%   f       -   Variable over 2D domain for which the derivative will be
%               taken. Matrix of size (N_eta x N_x).
%   D1_eta  -   Differentiation matrix of first order with respect to eta.
%               Matrix of size (N_eta x N_eta).
%   D1_xi   -   Differentiation matrix of first order with respect to xi.
%               Matrix of size (N_x x N_x).
%   D2_eta  -   Differentiation matrix of second order with respect to eta.
%               Matrix of size (N_eta x N_eta).
%   D1_xi   -   Differentiation matrix of second order with respect to xi.
%               Matrix of size (N_x x N_x).
%   deta_dx -   Rate of change of eta with respect to x.
%               Matrix of size (N_eta x N_x).
%   dxi_dx  -   Rate of change of xi with respect to x.
%               Matrix of size (N_eta x N_x).
%
% Outputs:
%   df_dx2  -   Second derivative of f with respect to x twice.
%               Matrix of size (N_eta x N_x).
%
% See also D1_x_func.m, D2_xy_func.m, eval_dimVars.m
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
%            Koen Groot
% GNU Lesser General Public License 3.0

deta_dx2    =   D1_x_func(deta_dx,D1_eta,D1_xi,deta_dx,dxi_dx);
dxi_dx2     =   D1_x_func(dxi_dx, D1_eta,D1_xi,dxi_dx, dxi_dx);

df_dx2      =   deta_dx2.*(D1_eta*f) + (deta_dx).^2.*(D2_eta*f) + ...
                dxi_dx2.*(D1_xi*f')' + (dxi_dx).^2.*(D2_xi*f')' + ...
                2*deta_dx.*dxi_dx.*(D1_xi*(D1_eta*f)')';

end % D2_x_func.m
