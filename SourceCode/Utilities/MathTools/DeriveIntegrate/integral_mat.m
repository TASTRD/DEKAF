function I = integral_mat(D,position)
% INTEGRAL_MAT(D) returns the integration matrix corresponding to the
%   derivation matrix D.
%
% Inputs:
%   D           : Differentiation matrix (e.g., finite-difference, chebyshev)
%   position    : String specifying start of domain in matrix
%                 'du' - Integrate "down-up" so the bottom row corresponds
%                 to the start of the domain (like for chebyshev matrices)
%                 'ud' - Integrate "up-down" so the top row corresponds to
%                 the start of the domain (like for FD matrices)
%
% Two matrices are returned, depending on how the physical integrand varies
% with the vector position. If going from down to up (Idu), or from up to
% down.
%
% Author(s): Koen Groot
% GNU Lesser General Public License 3.0

N=length(D);
switch position
    case 'du'
        % This example illustrates how to integrate with the Chebyshev matrices, in the typical down-up direction.
        Ddu = D; % D down-up
        Ddu(end,:) = [zeros(1,N-1) 1];
        I = inv(Ddu); % I down-up
        I(:,end) = I(:,end) - ones(N,1); % Set bottom right value to zero, setting the starting value of zero.
        % I1du_0tol = I1du(1,:); % extract first row of the integral matrix, representing the total integral over the complete domain
    case 'ud'
        % This example illustrates how to integrate with FD matrices, in the other up-down direction.
        Dud = D; % D up-down
        Dud(1,:) = [1 zeros(1,N-1)];
        I = inv(Dud); % I up-down
        I(:,1) = I(:,1) - ones(N,1);
        % I1ud_0tol = I1ud(end,:); % extract first row of the integral matrix, representing the total integral over the complete domain
end

end % integral_mat.m