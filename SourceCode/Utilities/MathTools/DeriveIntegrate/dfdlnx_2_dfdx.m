function varargout = dfdlnx_2_dfdx(varargin)
% DFDLNX_2_DFDX passes from derivatives of a function with respect to the
% natural logarithm of a variable, to derivatives with respect to the
% variable.
%
% Examples:
%   (1)     df_dx = DFDLNX_2_DFDX(x,df_dlnx)
%
%   (2)     [df_dx,df_dx2] = DFDLNX_2_DFDX(x,df_dlnx,df_dlnx2)
%
% See also: dlnfdx_2_dfdx, dfdx_2_dlnfdx
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

if nargin>=2 % first derivative
    x = varargin{1};
    df_dlnx = varargin{2};
    dlnx_dx = 1./x;
    df_dx = dlnx_dx .* df_dlnx;
    varargout{1} = df_dx;
end

if nargin>=3 % second derivative
    df_dlnx2 = varargin{3};
    dlnx_dx2 = -1./x.^2;
    df_dx2 = dlnx_dx2 .* df_dlnx + dlnx_dx.^2.*df_dlnx2;
    varargout{2} = df_dx2;
end