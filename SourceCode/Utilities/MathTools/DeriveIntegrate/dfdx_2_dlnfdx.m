function varargout = dfdx_2_dlnfdx(varargin)
% DFDX_2_DLNFDX passes from derivatives of a function to derivatives of the
% logarithm of the function.
%
% Examples:
%   (1)     lnf = DFDX_2_DLNFDX(f)
%
%   (2)     [lnf,dlnf_dx] = DFDX_2_DLNFDX(f,df_dx)
%
%   (3)     [lnf,dlnf_dx,dlnf_dx2] = DFDX_2_DLNFDX(f,df_dx,df_dx2)
%
% See also: dlnfdx_2_dfdx, dfdlnx_2_dfdx
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

f = varargin{1};
lnf = log(f);
varargout{1} = lnf;

if nargin>=2 % first derivative
    df_dx = varargin{2};
    dlnf_dx = 1./f.*df_dx;
    varargout{2} = dlnf_dx;
end

if nargin>=3 % second derivative
    df_dx2 = varargin{3};
    dlnf_dx2 = -1./f.^2.*df_dx.^2 + 1./f.*df_dx2;
    varargout{3} = dlnf_dx2;
end