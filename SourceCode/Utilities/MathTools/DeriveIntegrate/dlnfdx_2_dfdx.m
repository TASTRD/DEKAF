function varargout = dlnfdx_2_dfdx(varargin)
% DLNFDX_2_DFDX passes from derivatives of a natural logarithm of a
% function, to derivatives of the function itself.
%
% Examples:
%   (1)     df_dx = DLNFDX_2_DFDX(f,dlnf_dx)
%
%   (2)     [df_dx,df_dx2] = DLNFDX_2_DFDX(f,dlnf_dx,dlnf_dx2)
%
% See also: dfdlnx_2_dfdx, dfdx_2_dlnfdx
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

if nargin>=2 % first derivative
    f = varargin{1};
    dlnf_dx = varargin{2};
    df_dx = f.*dlnf_dx;
    varargout{1} = df_dx;
end

if nargin>=3 % second derivative
    dlnf_dx2 = varargin{3};
    df_dx2 = f.*(dlnf_dx).^2 + f.*dlnf_dx2;
    varargout{2} = df_dx2;
end