function [varargout] = FDdif_nonEquis(x,varargin)
% FDdif_nonEquis(x) returns the differentiation matrix for a 6th order
% backward finite difference scheme on a non-equispaced grid. The two first
% lines are identical.
%
% FDdif_nonEquis(x,'centered#') returns differentiation matrices for #-th
% order centered finite differences.
%
% FDdif_nonEquis(x,'backward#') returns differentiation matrices for #-th
% order backward finite differences. The two first lines are identical.
%
% FDdif_nonEquis(x,'forward#') returns differentiation matrices for #-th
% order forward finite differences.  The two last lines are identical.
%
% FDdif_nonEquis(...,'noRepeat') avoids the repetition of the first/last
% line for backward/forward differences. Instead, a forward/backward
% stencil is taken at the first/last location.
% IMPORTANT! The differentiation matrix will remain singular despite this.
% It is just that the point where the discrete differentiation is redundant
% is "hidden" within the matrix at another mesh point.
%
% Calls:
%
%   [FD_D1] = FDdif_nonEquis(x,...);
%   [FD_D1,FD_D2] = FDdif_nonEquis(x,...);
%
% See also fdcoeff_1d.m
%
% Reference:
%   FMM: (18/7/2019) Ludo, we need some knowledge here
%
% Author: Fernando Miro Miro, based on the work of Ludovico Zanus
% Date: July 2019
% GNU Lesser General Public License 3.0

% Checking inputs
if isempty(varargin);       method = 'backward6';
else;                       method = varargin{1}; varargin(1) = [];
end
regStruct   = regexp(method,'[0-9]+',    'match');      n    = str2double(regStruct{1});    % order of the method
regStruct   = regexp(method,'[a-z|A-Z]+','match');      meth = regStruct{1};                % type of method

if strcmp(meth,'centered') && round(n/2)~=n/2                                               % we cannot do odd-number-ordered central difference
    warning(['Odd-numbered-order central differences are not possible, so order ',num2str(n+1),' will be taken instead of order ',num2str(n)]);
    n = n+1;
end

Nx          = length(x);
Dmat        = zeros(Nx,Nx);
for ii=1:Nx
    %%%%% 1st-order derivatives
    % Determining positions in the x vector that affect the derivative at
    % this location
    switch meth
        case 'centered';    idx_x = getIdxCentralDiff(ii,n,Nx,varargin{:});
        case 'backward';    idx_x = getIdxBackwardDiff(ii,n,  varargin{:});
        case 'forward';     idx_x = getIdxForwardDiff(ii,n,Nx,varargin{:});
        otherwise
            error(['the chosen method ''',meth,''' is not supported']);
    end

    xii = x(idx_x);                                 % stencil
    A = fdcoeff_1d(xii,x(ii));              % obtaining coefficients
    Dmat(ii,idx_x)  = A(2,:);                       % populating matrix
end
switch meth
    case 'centered'                                                        % nothing to take care of
    case 'backward';    Dmat(1 ,:) = Dmat(2,:);     % we repeat the second row twice
    case 'forward';     Dmat(Nx,:) = Dmat(Nx-1,:);  % we repeat the third-last row twice
    otherwise;          error(['the chosen method ''',meth,''' is not supported']);
end
varargout{1} = Dmat;

if nargout>1
    D2mat = zeros(Nx,Nx);
    for ii=1:Nx
        %%%%% 2nd-order derivatives
        % Determining positions in the x vector that affect the derivative at
        % this location
        switch meth
            case 'centered';    idx_x = getIdxCentralDiff(ii,n,Nx,'derOrder',2,varargin{:});
            case 'backward';    idx_x = getIdxBackwardDiff(ii,n,  'derOrder',2,varargin{:});
            case 'forward';     idx_x = getIdxForwardDiff(ii,n,Nx,'derOrder',2,varargin{:});
            otherwise
                error(['the chosen method ''',meth,''' is not supported']);
        end

        xii = x(idx_x);                                 % stencil
        A = fdcoeff_1d(xii,x(ii));              % obtaining coefficients

        if length(xii)>2;   D2mat(ii,idx_x) = A(3,:);   % populating matrix
        else;               D2mat(ii,:) = NaN;          % filling temporarily with NaNs
        end
    end
    switch meth
        case 'centered'                                                             % nothing to take care of
        case 'backward';    D2mat(1:2    ,:) = [D2mat(3,:)    ; D2mat(3,:)   ];     % we repeat the third row thrice
        case 'forward';     D2mat(Nx-1:Nx,:) = [D2mat(Nx-2,:) ; D2mat(Nx-2,:)];     % we repeat the third-last row thrice
        otherwise;          error(['the chosen method ''',meth,''' is not supported']);
    end
    varargout{2} = D2mat;
end


end % FDdif_nonEquis