function [varargout] = FDdif(N,ds,varargin)
% FDdif(N,ds) returns the differentiation matrix for a 6th order backward
% finite difference scheme defined by the N number of points in any
% direction and the stencil step ds. The first two lines are identical.
%
% FDdif(N,ds,'centered') returns the first and second differentiation
% matrices for 4th order centered finite differences with 2nd order
% stencils applied to the boundaries.
%
% FDdif(N,ds,'centered_cst4') returns the first and second differentiation
% matrices for 4th order centered finite differences across the entire
% domain, including the boundaries.
%
% FDdif(N,ds,'backward1') returns the first differentiation matrix for 1st
% order backward finite differences.
%
% FDdif(N,ds,'backward2') returns the second differentiation matrix for 2nd
% order backward finite differences.
%
% FDdif(N,ds,'backward#') returns the second differentiation matrix for #th
% order (maximum is 6)
%
% FDdif(N,ds,'backward2',false) returns the second differentiation matrix
% for 1st order backward finite differences, but now, the last two rows are
% overlapping, repeating a differentiation over the domain.
%
% FDdif(N,ds,'centered_cst4','sparse') returns sparse differentiation
% matrices identical to the full matrices of FDdif(N,ds,'centered_cst4')
% NOTE: This 'sparse' varargin only works for 'centered_cst4' as of
% 2019-04-23 (out of necessity).
%
% etc.
%
% Calls:
%
%   [FD_D1] = FDdif(N,ds,varargin);
%   [FD_D1,FD_D2] = FDdif(N,ds,varargin);
%
% See also FDstencil.m
%
% Reference:
%   http://web.media.mit.edu/~crtaylor/calculator.html
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

% Set defaults
isFDsparse = false;
switch nargin
    case 2
        scheme = 'backward6';
        toprows = true;
    case 3
        scheme = varargin{1};
        toprows = true;
    case 4
        scheme = varargin{1};
        if islogical(varargin{2})
            toprows = varargin{2};
        elseif ischar(varargin{2})
            switch varargin{2}
                case 'sparse'
                    isFDsparse = true;
                otherwise
                    error('Unsupported string for ''varargin{2}''!');
            end
        else
            error('Unsupported data type for ''varargin{2}''!');
        end
    otherwise
        error('wrong number of inputs');
end

vec_ones = ones(1,N); % vector of ones to be used for the evaluation of the diagonals

if N==1                 % if a single vector is chosen, then the derivation matrix is just made of a single 1
    D_FD1 = 1;
    D_FD2 = 1;
else

    %%% Forward/Backward Difference %%%
    % obtaining stencil for different orders of FD
    [FD11,scl1] = FDstencil(1);
    FD11 = FD11/(scl1*ds);
    [FD12,scl2] = FDstencil(2);
    FD12 = FD12/(scl2*ds);
    [FD13,scl3] = FDstencil(3);
    FD13 = FD13/(scl3*ds);
    [FD14,scl4] = FDstencil(4);
    FD14 = FD14/(scl4*ds);
    [FD15,scl5] = FDstencil(5);
    FD15 = FD15/(scl5*ds);
    [FD16,scl6] = FDstencil(6);
    FD16 = FD16/(scl6*ds);

    % second derivatives
    FD21 = [   1    -2     1]/ds^2;
    FD22 = [   2    -5     4    -1]/ds^2;
    FD23 = [  35  -104   114   -56   11] / 12/ds^2;
    FD24 = [  45  -154   214  -156   61   -10] / 12/ds^2;
    FD25 = [ 812 -3132  5265 -5080  2970  -972  137] / 180/ds^2;
    FD26 = [ 938 -4014  7911 -9490  7380 -3618 1019  -126] / 180/ds^2;

    switch scheme
        case 'centered'
            % obtaining stencil for different orders of CD
            CD11 = [1/2 -2 3/2]/ds; % n = 1
            CD12 = [-3/2 2 -1/2]/ds; % n = n
            CD13 = [-1/2 0 1/2]/ds; % n = 2, n-1
            CD14 = [1/12 -2/3 0 2/3 -1/12]/ds;
            CD21 = [2 -5 4 -1]/(ds*ds); % n = 1  (sampled points of 0,1,2,3)
            CD22 = [-1 4 -5 2]/(ds*ds); % n = n  (sampled points of -3,-2,-1,0)
            CD23 = [1 -2 1]/(ds*ds); % n = 2, n-1 (ignore the zero so that it can work for both i = 2 and i = N - 1)
            CD24 = [-1/12 4/3 -5/2 4/3 -1/12]/(ds*ds);

            % Building differentiation matrix
            D_CD1 = zeros(N,N);
            D_CD2 = zeros(N,N);
            for i = 1:N
                if i == 1
                    D_CD1(1,1:3) = CD11;
                    if nargout>1
                        D_CD2(1,1:4) = CD21;
                    end
                elseif i == N
                    D_CD1(N,N-2:N) = CD12;
                    if nargout>1
                        D_CD2(N,N-3:N) = CD22;
                    end
                elseif i == 2 || i == N-1
                    D_CD1(i,i-1:i+1) = CD13;
                    if nargout>1
                        D_CD2(i,i-1:i+1) = CD23;
                    end
                else
                    D_CD1(i,i-2:i+2) = CD14;
                    if nargout>1
                        D_CD2(i,i-2:i+2) = CD24;
                    end
                end
            end
            D_FD1 = D_CD1; % defined for the outputs
            if nargout>1
                D_FD2 = D_CD2;
            end
        case 'centered_cst4'
            % First derivative stencils
            D1_0___p4 = [-25 48 -36 16 -3]/12/ds;
            D1_n1___p3 = [-3 -10 18 -6 1]/12/ds;
            D1_n2___p2 = [1 -8 0 8 -1]/12/ds;
            D1_n3___p1 = -fliplr(D1_n1___p3); % negative flips on first derivative
            D1_n4___0 = -fliplr(D1_0___p4);

            % Second derivative stencils
            if nargout>1
                D2_0___p6 = [812 -3132 5265 -5080 2970 -972 137]/180/ds/ds;
                D2_n1___p5 = [137 -147 -255 470 -285 93 -13]/180/ds/ds;
                D2_n2___p4 = [-13 228 -420 200 15 -12 2]/180/ds/ds;
                D2_n3___p3 = [2 -27 270 -490 270 -27 2]/180/ds/ds;
                D2_n4___p2 = fliplr(D2_n2___p4); % positive flips on second derivative
                D2_n5___p1 = fliplr(D2_n1___p5);
                D2_n6___p0 = fliplr(D2_0___p6);
            end

            % Build main diagonals
            if isFDsparse
                D_FD1 = spdiags(D1_n2___p2.*vec_ones.', -2:2, N, N);
                if nargout>1
                    D_FD2 = spdiags(D2_n3___p3.*vec_ones.', -3:3, N, N);
                end
            else
                error('not ready yet!');
            end

            % Correct the boundaries
            D_FD1(1,1:5) = D1_0___p4;
            D_FD1(2,1:5) = D1_n1___p3;
            D_FD1(N-1,N-4:N) = D1_n3___p1;
            D_FD1(N,N-4:N) = D1_n4___0;
            if nargout>1
                D_FD2(1,1:7) = D2_0___p6;
                D_FD2(2,1:7) = D2_n1___p5;
                D_FD2(3,1:7) = D2_n2___p4;
                D_FD2(N-2,N-6:N) = D2_n4___p2;
                D_FD2(N-1,N-6:N) = D2_n5___p1;
                D_FD2(N,N-6:N) = D2_n6___p0;
            end
        case 'backward1'
            % Building differentiation matrix
            D_FD1 = diag(FD11(1)*vec_ones(1:N  ), 0) + ... % main diagonal
                diag(FD11(2)*vec_ones(1:N-1),-1);% first diagonal
            D_FD1(1,1:2) = flip(FD11);
            % second derivative
            if nargout>1
            D_FD2 = diag(FD21(1)*vec_ones(1:N),0) + ... % main diagonal
                diag(FD21(2)*vec_ones(1:N-1), -1) + ... % first lower diagonal
                diag(FD21(3)*vec_ones(1:N-2), -2);      % second lower diagonal
            D_FD2(1:2,:) = []; % for the moment we eliminate the first two rows, we will take care of it later
            end
        case 'backward2'
            % Building differentiation matrix
            D_FD1 = diag(FD12(1)*vec_ones(1:N  ), 0) + ... % main diagonal
                diag(FD12(2)*vec_ones(1:N-1),-1) + ... % first diagonal
                diag(FD12(3)*vec_ones(1:N-2),-2); % second lower diagonal
            %D_FD(1,:) = zeros(1,N); % filling in the initial lower order derivatives
            D_FD1(1,1:2) = flip(FD11);
            D_FD1(2,1:2) = flip(FD11);
            % second derivative
            if nargout>1
            D_FD2 = diag(FD22(1)*vec_ones(1:N),0) + ... % main diagonal
                diag(FD22(2)*vec_ones(1:N-1), -1) + ... % first lower diagonal
                diag(FD22(3)*vec_ones(1:N-2), -2) + ... % second lower diagonal
                diag(FD22(4)*vec_ones(1:N-3), -3);      % third lower diagonal
            D_FD2(3,1:3) = flip(FD21);
            D_FD2(1:2,:) = []; % for the moment we eliminate the first two rows, we will take care of it later
            end
        case 'backward3'
            % Building differentiation matrix
            D_FD1 = diag(FD13(1)*vec_ones(1:N  ), 0) + ... % main diagonal
                diag(FD13(2)*vec_ones(1:N-1),-1) + ... % first diagonal
                diag(FD13(3)*vec_ones(1:N-2),-2) + ... % second lower diagonal
                diag(FD13(4)*vec_ones(1:N-3),-3); % third lower diagonal
            %D_FD(1,:) = zeros(1,N); % filling in the initial lower order derivatives
            D_FD1(1,1:2) = flip(FD11);
            D_FD1(2,1:2) = flip(FD11);
            D_FD1(3,1:3) = flip(FD12);
            % second derivative
            if nargout>1
            D_FD2 = diag(FD23(1)*vec_ones(1:N),0) + ... % main diagonal
                diag(FD23(2)*vec_ones(1:N-1), -1) + ... % first lower diagonal
                diag(FD23(3)*vec_ones(1:N-2), -2) + ... % second lower diagonal
                diag(FD23(4)*vec_ones(1:N-3), -3) + ... % third lower diagonal
                diag(FD23(5)*vec_ones(1:N-4), -4);      % fourth lower diagonal
            D_FD2(3,1:3) = flip(FD21);
            D_FD2(4,1:4) = flip(FD22);
            D_FD2(1:2,:) = []; % for the moment we eliminate the first two rows, we will take care of it later
            end
        case 'backward4'
            % Building differentiation matrix
            D_FD1 = diag(FD14(1)*vec_ones(1:N  ), 0) + ... % main diagonal
                diag(FD14(2)*vec_ones(1:N-1),-1) + ... % first diagonal
                diag(FD14(3)*vec_ones(1:N-2),-2) + ... % second lower diagonal
                diag(FD14(4)*vec_ones(1:N-3),-3) + ... % third lower diagonal
                diag(FD14(5)*vec_ones(1:N-4),-4); % fourth lower diagonal
            %D_FD(1,:) = zeros(1,N); % filling in the initial lower order derivatives
            D_FD1(1,1:2) = flip(FD11);
            D_FD1(2,1:2) = flip(FD11);
            D_FD1(3,1:3) = flip(FD12);
            D_FD1(4,1:4) = flip(FD13);
            % second derivative
            if nargout>1
            D_FD2 = diag(FD24(1)*vec_ones(1:N),0) + ... % main diagonal
                diag(FD24(2)*vec_ones(1:N-1), -1) + ... % first lower diagonal
                diag(FD24(3)*vec_ones(1:N-2), -2) + ... % second lower diagonal
                diag(FD24(4)*vec_ones(1:N-3), -3) + ... % third lower diagonal
                diag(FD24(5)*vec_ones(1:N-4), -4) + ... % fourth lower diagonal
                diag(FD24(6)*vec_ones(1:N-5), -5);      % fifth lower diagonal
            D_FD2(3,1:3) = flip(FD21);
            D_FD2(4,1:4) = flip(FD22);
            D_FD2(5,1:5) = flip(FD23);
            D_FD2(1:2,:) = []; % for the moment we eliminate the first two rows, we will take care of it later
            end
        case 'backward5'
            % Building differentiation matrix
            D_FD1 = diag(FD15(1)*vec_ones(1:N  ), 0) + ... % main diagonal
                diag(FD15(2)*vec_ones(1:N-1),-1) + ... % first diagonal
                diag(FD15(3)*vec_ones(1:N-2),-2) + ... % second lower diagonal
                diag(FD15(4)*vec_ones(1:N-3),-3) + ... % third lower diagonal
                diag(FD15(5)*vec_ones(1:N-4),-4) + ... % fourth lower diagonal
                diag(FD15(6)*vec_ones(1:N-5),-5); % fifth lower diagonal
            %D_FD(1,:) = zeros(1,N); % filling in the initial lower order derivatives
            D_FD1(1,1:2) = flip(FD11);
            D_FD1(2,1:2) = flip(FD11);
            D_FD1(3,1:3) = flip(FD12);
            D_FD1(4,1:4) = flip(FD13);
            D_FD1(5,1:5) = flip(FD14);
            % second derivative
            if nargout>1
            D_FD2 = diag(FD25(1)*vec_ones(1:N),0) + ... % main diagonal
                diag(FD25(2)*vec_ones(1:N-1), -1) + ... % first lower diagonal
                diag(FD25(3)*vec_ones(1:N-2), -2) + ... % second lower diagonal
                diag(FD25(4)*vec_ones(1:N-3), -3) + ... % third lower diagonal
                diag(FD25(5)*vec_ones(1:N-4), -4) + ... % fourth lower diagonal
                diag(FD25(6)*vec_ones(1:N-5), -5) + ... % fifth lower diagonal
                diag(FD25(7)*vec_ones(1:N-6), -6);      % sixth lower diagonal
            D_FD2(3,1:3) = flip(FD21);
            D_FD2(4,1:4) = flip(FD22);
            D_FD2(5,1:5) = flip(FD23);
            D_FD2(6,1:6) = flip(FD24);
            D_FD2(1:2,:) = []; % for the moment we eliminate the first two rows, we will take care of it later
            end
        case 'backward6'
            % Building differentiation matrix
            D_FD1 = diag(FD16(1)*vec_ones(1:N  ), 0) + ... % main diagonal
                diag(FD16(2)*vec_ones(1:N-1),-1) + ... % first diagonal
                diag(FD16(3)*vec_ones(1:N-2),-2) + ... % second lower diagonal
                diag(FD16(4)*vec_ones(1:N-3),-3) + ... % third lower diagonal
                diag(FD16(5)*vec_ones(1:N-4),-4) + ... % fourth lower diagonal
                diag(FD16(6)*vec_ones(1:N-5),-5) + ... % fifth lower diagonal
                diag(FD16(7)*vec_ones(1:N-6),-6);      % sixth lower diagonal
            %D_FD(1,:) = zeros(1,N); % filling in the initial lower order derivatives
            D_FD1(1,1:2) = flip(FD11);
            D_FD1(2,1:2) = flip(FD11);
            D_FD1(3,1:3) = flip(FD12);
            D_FD1(4,1:4) = flip(FD13);
            D_FD1(5,1:5) = flip(FD14);
            D_FD1(6,1:6) = flip(FD15);
            % second derivative
            if nargout>1
            D_FD2 = diag(FD26(1)*vec_ones(1:N),0) + ... % main diagonal
                diag(FD26(2)*vec_ones(1:N-1), -1) + ... % first lower diagonal
                diag(FD26(3)*vec_ones(1:N-2), -2) + ... % second lower diagonal
                diag(FD26(4)*vec_ones(1:N-3), -3) + ... % third lower diagonal
                diag(FD26(5)*vec_ones(1:N-4), -4) + ... % fourth lower diagonal
                diag(FD26(6)*vec_ones(1:N-5), -5) + ... % fifth lower diagonal
                diag(FD26(7)*vec_ones(1:N-6), -6) + ... % sixth lower diagonal
                diag(FD26(8)*vec_ones(1:N-7), -7);      % seventh lower diagonal
            D_FD2(3,1:3) = flip(FD21);
            D_FD2(4,1:4) = flip(FD22);
            D_FD2(5,1:5) = flip(FD23);
            D_FD2(6,1:6) = flip(FD24);
            D_FD2(7,1:7) = flip(FD25);
            D_FD2(1:2,:) = []; % for the moment we eliminate the first two rows, we will take care of it later
            end
        otherwise
            error('non-identified scheme');
    end

    % fixing the missing rows (force top or bottom derivatives to coincide)
    if nargout>1
        switch scheme
            case {'centered','centered_cst4'}
                % skip! all rows have already been populated
            otherwise
                if toprows
                    D_FD2 = [D_FD2(1,:) ; D_FD2(1,:) ; D_FD2];
                else
                    D_FD2 = [D_FD2 ; D_FD2(end,:) ; D_FD2(end,:)];
                end
        end
    end
end

% preparing outputs
switch nargout
    case {0,1}
        varargout{1} = D_FD1;
    case 2
        varargout{1} = D_FD1;
        varargout{2} = D_FD2;
    otherwise
        error('wrong number of outputs');
end

end % FDdif.m
