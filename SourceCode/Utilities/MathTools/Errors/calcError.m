function err = calcError(q2,q1,errType,opts)
%CALCERROR Calculate desired error of a vector with respect to another vector
%
% Inputs:
%   errType
%     'diff_abs'            :       |q2 - q1|
%                                Note that normType is not needed for this errType of 'diff_abs'
%
%     'diff_weight'         :       (q2 - q1)/max(|q2|)
%                                If the vectors are not equal in size, then choose the the 'diff_interp' option.
%
%     'ratio'               :        q2 ./ q1
%                                Note that normType is not needed for this errType of 'ratio'
%
%     '2n-1'                :     ||(q2(2n-1)-q1(n))||/max(|q2(2n-1)|)
%                                Collocation of nodes method, 2n-1 scheme.
%                                This error evaluation compares identical nodes between two vectors
%                                provided the vectors were constructed with this 2n-1 scheme in mind.
%                                Constraint: length(q2)==2*length(q1)-1
%
%                                   q1              q2
%                                  -----           -----
%                                   -x-             -x-
%                                    |               |
%                                    |              ---
%                                    |               |
%                                   -x-             -x-
%                                    |               |
%                                    |              ---
%                                    |               |
%                                   -x-             -x-
%                                    |               |
%                                    |              ---
%                                    |               |
%                                   -x-             -x-
%
%     '3n-2'                :     ||(q2(3n-2)-q1(n))||/max(|q2(3n-2)|)
%                                Collocation of nodes method, 3n-2 scheme.
%                                This error evaluation compares identical nodes between two vectors
%                                provided the vectors were constructed with this 3n-2 scheme in mind.
%                                Constraint: length(q2)==3*length(q1)-2
%
%                                   q1              q2
%                                  -----           -----
%                                   -x-             -x-
%                                    |               |
%                                    |              ---
%                                    |               |
%                                    |              ---
%                                    |               |
%                                   -x-             -x-
%                                    |               |
%                                    |              ---
%                                    |               |
%                                    |              ---
%                                    |               |
%                                   -x-             -x-
%                                    |               |
%                                    |              ---
%                                    |               |
%                                    |              ---
%                                    |               |
%                                   -x-             -x-
%
%     'diff_interp'         :       (q2 - q1)/max(|q2|)
%                                q1 is interpolated up to size of q2, then the quotient is computed
%                                The fields opts.fine_domain, opts.coarse_domain are required for this errType.
%                            *** opts.fine_domain corresponds to the domain of q2   ***
%                            *** opts.coarse_domain corresponds to the domain of q1 ***
%
%     'diff_interp_cheb'    :       (q2 - q1)/max(|q2|)
%                                q1 is interpolated up to size of q2 using chebyshev distributions
%                                the quotient is then computed.
%                                this errType only applies to vectors whose domains are both chebyshev distributions
%
%     normType
%       p                   : p-norm (p real or infinity)
%                           if p = 1  , then n is the maximum absolute column sum of the matrix.
%                           if p = 2  , then n is approximately max(svd(X)).
%                           if p = Inf, then n is the maximum absolute row sum of the matrix. ... (default)
%
%       'fro'               : Frobenius norm
%
% TO-DO: Add documentation for the opts.fields!
%
% Author(s):    Ethan Beyak
%               Koen Groot
% See also eval_norm.m, chebinterp.m

% Defaults
if nargin<3
    error('Not enough inputs.');
end

% Defaults
if nargin==3 || (~isfield(opts,'normType') || isempty(opts.normType))
    opts.normType = inf;
end
if (~isfield(opts,'interpType') || isempty(opts.interpType))
    opts.interpType = 'spline';
end
if (~isfield(opts,'maxType') || isempty(opts.maxType))
    opts.maxType = 'fine_and_coarse';
end

n   = 1:length(q1);
if size(q1,1) == size(q2,2) || size(q1,2) == size(q2,1)
    q1 = q1';
end

switch opts.maxType
    case 'fine_and_coarse'
        myMaxDenom = max(abs([q2; q1]));
    case 'fine'
        myMaxDenom = max(abs(q2));
    case 'coarse'
        myMaxDenom = max(abs(q1));
end

% Calculate error
switch errType
    case 'diff_abs'
        err = abs(q2 - q1);
    case 'diff_weight'
        err = norm((q2 - q1)/myMaxDenom,opts.normType);
    case 'ratio'
        err = q2 ./ q1;
    case '2n-1'
        err = norm((q2(2*n-1) -q1(n))/max(abs(q2(2*n-1))),opts.normType);
    case '3n-2'
        err = norm((q2(3*n-2) -q1(n))/max(abs(q2(3*n-2))),opts.normType);
    case 'diff_interp'
        q1 = interp1(opts.coarse_domain,q1,opts.fine_domain,opts.interpType);
        err = norm((q2 - q1)/myMaxDenom,opts.normType);
    case 'diff_interp_cheb'
        % Interpolate q1 onto chebyshev nodes associated with fine domain
        M = length(q2);
        q1 = chebinterp(q1, chebDif(M,0));
        err = norm((q2 - q1)/myMaxDenom,opts.normType);
end

end % calcError.m
