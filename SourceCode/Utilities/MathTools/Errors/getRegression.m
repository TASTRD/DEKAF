function [slope,intercept,y_reg,coeff] = getRegression(x,y,plotType,opts)
%GETREGRESSION Returns the slope and intercept of a regression using polyfit
%
% [slope,intercept,y_reg] = getRegression(x,y,plotType,opts)
%
% Inputs:
%   x           - vector, domain of y
%   y           - vector, function over x for which linear regression characteristics are desired
%   plotType    - string, specify plot type
%                   supported: 'no-log','semilogx','semilogy','loglog'
%                     log base 10 is used.
%
% Optional Inputs:
%   opts        - struct, optional inputs
%                   .plot       -- boolean, plots x,y, holds, plots x,regression
%                                    default: false
%                   .order      -- integer >= 0, specify order of regression
%                                    default: 1
%                   .xbounds    -- 2-entry vector, specify domain interval over which regression is calculated
%                                    default: []
%
% Outputs:
%   slope       - double, coefficient of linear term in polynomial regression
%   intercept   - double, coefficient of zeroth order term in polynomial regression
%   y_reg       - vector, regression of y
%
% Examples:
%   x = linspace(-1,0);
%   y = 10.^(3/2*x+1);
%   opts.plot = true;
%   opts.xbounds = [-1/2 -1/4];
%   [slope,intercept,y_reg] = getRegression(x,y,'semilogy',opts);
%
%   x = linspace(0,pi);
%   y = sin(x);
%   opts.plot = true;
%   opts.xbounds = [pi/2 3*pi/4];
%   [slope,intercept,y_reg] = getRegression(x,y,'no-log',opts);
%
% Author: Ethan Beyak
%
% See also polyfit.m

% Set defaults
opts = standardvalue(opts,'plot',false);
opts = standardvalue(opts,'order',1);
opts = standardvalue(opts,'xbounds',[]);

if strcmp(plotType,'no-log')
    fx      = @(x) x;
    fy      = @(y) y;
    fyinv   = @(y) y;
    varplot = @(x,y,s) plot(x,y,s);
elseif strcmp(plotType,'semilogx')
    fx      = @(x) log10(x);
    fy      = @(y) y;
    fyinv   = @(y) y;
    varplot = @(x,y,s) semilogx(x,y,s);
elseif strcmp(plotType,'semilogy')
    fx      = @(x) x;
    fy      = @(y) log10(y);
    fyinv   = @(y) 10.^(y);
    varplot = @(x,y,s) semilogy(x,y,s);
elseif strcmp(plotType,'loglog')
    fx      = @(x) log10(x);
    fy      = @(y) log10(y);
    fyinv   = @(y) 10.^(y);
    varplot = @(x,y,s) loglog(x,y,s);
else
    error('Input string for ''plotType'' is not supported.');
end

% Save original data as new variables
x_original  = x;
y_original  = y;

% Check if user input alternate x bounds for the regression
if ~isempty(opts.xbounds)
    if max(opts.xbounds) > max(x) || min(opts.xbounds) < min(x)
        error('Input bounds for ''opts.xbounds'' extend beyond the input domain x. Extrapolation is not supported.')
    end
    % % Adjust bounds of x and y based on opts.xbounds
    % Extract indices corresponding to user input
    [~,b]   = min(abs(x-opts.xbounds(1)));
    [~,e]   = min(abs(x-opts.xbounds(2)));
    idx     = (x >= x(b)) & (x <= x(e));
    % Redefine x and y corresponding to opts.xbounds
    x       = x(idx);
    y       = y(idx);
end

coeff = polyfit(fx(x),fy(y),opts.order);

intercept = coeff(end);
if opts.order>0
    slope = coeff(end-1);
end

% Construct regression
n = length(coeff)-1;
y_reg = 0;
for ii = 1:length(coeff)
    y_reg = y_reg + coeff(ii)*fx(x).^(n-ii+1);
end
y_reg=fyinv(y_reg);

if opts.plot
    figure
    varplot(x_original,y_original,'k-');
    hold on
    varplot(x,y_reg,'b-.');
    legend({'Data','Regression'},'location','best')
    grid on
    lines = findobj(gcf,'Type','Line');
    for i = 1:numel(lines)
      lines(i).LineWidth = 2;
    end
end

end % getRegression.m
