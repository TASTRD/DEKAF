function sum = multiOrderSum(varargin)
% multiOrderSum(x1,...,xN) preforms the sum of N terms with multiple orders
% of magnitude, reducing the error associated to cut-off. The terms can
% be multi-dimensional matrices.
%
% The way it works, is: first it stores the orders of magnitude of all
% terms, and then it separately sums the terms with the same orders of
% magnitude. Finally, it sums together all the different orders of
% magnitude.
%
% Example:
% x1 = 1e55
% x2 = 3
% x3 = -1
% x4 = -1e55
%               x1 + x2 + x3 + x4           = 0
%               multiOrderSum(x1,x2,x3,x4)  = 2
%
% multiOrderSum(x1,...,xN,'precision',prec) allows to fix what is the
% relative precision of the values being summed. This is used to cut off
% summation residuals smaller than the precision in question.
%
% Example:
% x1 =  1.172561910927e55
% x2 = 3
% x3 = -1
% x4 = -1.172567618286e55
%               multiOrderSum(x1,x2,x3,x4)  = -5.7073590000e+49
%               multiOrderSum(x1,x2,x3,x4,'precision',1e-5)  = 2
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% Looking for flags and analyzing inputs
flagPos = find(cellfun(@ischar,varargin));                  % identifying which inputs are flags
if isempty(flagPos)                                         % it means the user didn't specify any flags
    prec = eps;                                                 % by default, the precision is machine precision
    N_sum = nargin;                                             % the number of terms to sum will be equal to the number of inputs
elseif strcmp(varargin{flagPos},'precision')                % if there is one and it corresponds to 'precision'
    prec = varargin{flagPos+1};                                 % then we take the input coming straight after the flag
    N_sum = nargin-2;                                           % the number of terms to sum will be equal to the number of inputs minus two
else                                                        % otherwise, break
    error(['Non-identified flag ''',varargin{flagPos},''''])
end
Ni_dim = size(varargin{1});                                 % size of the terms in each dimension
N_tot = numel(varargin{1});                                 % total number of points

% Arranging all inputs into a matrix of N_tot x N_sum
allTerms = zeros(N_tot,N_sum);                              % initializing
for ii=1:N_sum                                              % looping over terms to be summed
    allTerms(:,ii) = reshape(varargin{ii},[N_tot,1]);           % appending reshaped inputs into matrix
end

% Obtaining range of orders of magnitude
OoM = round(log10(abs(allTerms)));                          % obtaining the order of magnitude of each term
OoM(OoM<-300) = -300;                                       % cutting off terms with an order of magnitude smaller than -300 (matlab's overflow)
OoM(OoM>300) = 300;                                         % cutting off terms with an order of magnitude larger than 300 (matlab's overflow)
OoMRange = unique(OoM(:));                                  % vector with the possible orders of magnitude
N_OoM = length(OoMRange);                                   % number of different orders of magnitude

% Summing terms with each order of magnitude (double loop)
sumTermsOoM = zeros(N_tot,N_OoM);                           % initializing
for ii=1:N_OoM                                              % looping orders of magnitude
    for jj = 1:N_sum                                            % looping terms to be summed
        pos = (OoM(:,jj)==OoMRange(ii));                            % position of the terms with the corresponding order of magnitude
        sumTermsOoM(pos,ii) = sumTermsOoM(pos,ii) + allTerms(pos,jj); % adding the terms with the looped order of magnitude
    end
end

% Cutting off any residuals in the sum of each order of magnitude, that is below machine precision (numerical perturbations)
OoMRange_mat = repmat(OoMRange',[N_tot,1]);                 % matrix of orders of magnitude in each position in sumTermsOoM
sumOrder0 = sumTermsOoM .* 10.^(-OoMRange_mat);             % making the order of the summing terms zero
sumOrder0 = prec*(round(real(sumOrder0)/prec) + 1i*round(imag(sumOrder0)/prec));% cutting off anything smaller than the number's relative precision
sumTermsOoM_cut = sumOrder0 .* 10.^OoMRange_mat;            % returning the order of magnitude of the summing terms to what it originally was

% Performing the vectorial sum of all orders of magnitude, and reshaping back to the original size
sum1D = sumTermsOoM_cut*ones(N_OoM,1);                      % by multiplying by a row of ones, we are effectively summing
sum = reshape(sum1D,Ni_dim);                                % reshaping back to the original size