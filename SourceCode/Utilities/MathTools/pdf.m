function [X,PX] = pdf(x,varargin)
% PDF computes the probability distribution function of a set of positive
% values.
%
% Examples:
%   (1)     pdf(x)
%
%   (2)     pdf(x,NX)
%   |--> allows to fix the number of x separations to be made for the
%   plotting
%
%   (3)     pdf(x,NX,bLowerLimit)
%   |--> allows to pass a boolean in order to fix a lower limit for x (eps)
%   or not.
%
% Author: Fernando Miro Miro
% Date: February 2018

if isempty(varargin)
NX = 100; % default
bLowerLimit = true;
elseif length(varargin)==1
    NX = varargin{1};
    bLowerLimit = true;
elseif length(varargin)==2
    NX = varargin{1};
    bLowerLimit = varargin{2};
end

Nx = length(x); % number of inputed values
x_max = max(x); % maximum
x_min = min(x); % minimum
if bLowerLimit
x_min = max(x_min,eps); % putting a limit on the minimum
x(x<x_min) = x_min; % setting lower limit
end

logx_min = log10(x_min); % logarithms
logx_max = log10(x_max);
delta_logx = (logx_max - logx_min)/(NX-1); % step in the logarithms

X = logspace(logx_min,logx_max,NX); % building sample vector with a logarithmic spacing
logx = log10(x); % obtaining the logarithm of the inputted vector

idx = ceil((logx-logx_min)/delta_logx); % index in X where each value in x corresponds
idx(idx<1) = 1; % accounting for the lower limit

PX = zeros(size(X)); % allocating
for ii=1:NX % looping sampled vector
    PX(ii) = sum(idx==ii);
end
PX = PX/Nx; % normalizing

if abs(sum(PX)-1)>eps*Nx
    error(['the sum of PX was ',num2str(sum(PX)),' and it should''ve been 1']);
end

figure
semilogx(X,PX);
xlabel('X'); ylabel('P(X)');
grid minor
