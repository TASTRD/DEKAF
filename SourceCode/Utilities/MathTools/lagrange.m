function yp = lagrange(x,y,xp,varargin)
% lagrange performs an interpolation using lagrangian polynomials.
%
% Usage:
%   (1)
%       yp = lagrange(x,y,xp)
%
%   (2)
%       [...] = lagrange(...,'order',Norder)
%       |---> allows to fix an arbitrary order of the interpolation. By
%       default: N (number of x points)
%
%   (3)
%       [...] = lagrange(...,'plot')
%       |---> makes a plot with the original and final variables
%
% Author: Fernando Miro Miro
% Date: August 2019

%%% Initial checks
[varargin,bPlot] = find_flagAndRemove('plot',varargin);                     % checking for rthe plot flag
if ndims(y)>2 %#ok<*ISMAT>
    error('y has more than 2 dimensions, which is not supported');   % break if input has more than 2 dimensions
end
if size(x,1)==1                                                             % making everything into column-matrix form
    x = x(:);
    y = y.';
end
bTranspose = (size(xp,1)==1);                                               % if the input was a row vector, we'll have to transpose at the end
xp = xp(:);                                                                 % making input into a column for now
N  = length(x);                                                             % number of original points
Np = length(xp);                                                            % number of points onto which we must interpolate
Ncol = size(y,2);                                                           % number of columns in the y matrix
if N~=size(y,1)                                                             % break if the length of x and y don't match
    error('the sizes of x and y are not compatible');
end
bFlip = (mean(diff(x))<0);                                                  % if the x vector is decreasing
if bFlip                                                                    % we flip everything
    x = flip(x);
    y = flip(y,1);
end
[varargin,~,idxFlg] = find_flagAndRemove('order',varargin);                 % checking for the order flag
if isempty(idxFlg);     Norder = N;                                         % by default the order is the number of points
else;                   Norder = varargin{idxFlg};                          % otherwise, it's whatever was passed
end
if Norder<2 || Norder>N                                                     % break if the chosen order is out of range
    error(['the chosen Norder = ',num2str(Norder),' is not supported, it must be between 2 and N=',num2str(N)]);
end
idx=1:Norder;                                                               % index of the interpolation stencil

%%% Repmatting
x_NN    = repmat(x,[1,N]);
x_NNp   = repmat(x,[1,Np]);
xp_NpN  = repmat(xp,[1,N]);

%%% Building interpolation stencils (idx_p)
for ixp=Np:-1:1                                                             % looping target points
    idx0 = find(x<=xp(ixp),1,'last');                                           % point in the original vector before the target point
    idx1 = find(x>=xp(ixp),1,'first');                                          % point in the original vector after the target point
    if isempty(idx0)||isempty(idx1);    error(['point ',num2str(ixp),' out of bounds - extrapolation is not supported']);
    elseif idx0<=floor(Norder/2);       idx_p(ixp,:) = 1:Norder;                % close to the front - take Norder first points
    elseif idx1>=N-ceil(Norder/2)+1;    idx_p(ixp,:) = N-Norder+1:N;            % close to the back - take Norder last points
    elseif idx0==idx1;                  idx_p(ixp,:) = idx0-floor(Norder/2):idx0+ceil(Norder/2)-1;
    else;                               idx_p(ixp,:) = idx0-floor(Norder/2)+1:idx1+ceil(Norder/2)-1;
    end
end


%%% Building lagrange-multiplier individual terms (vectorially)
mult_x  = x_NN   - x_NN.';                                                  % (i,j) = x(i) - x(j)
mult_xp = xp_NpN - x_NNp.';                                                 % (i,j) = xp(i) - x(j)

%%% Assembling multipliers (vectorially)
for ix=Norder:-1:1                                                          % looping stencil points
    j_not_i = idx_p(:,idx~=ix);                                             % all points in the stencil except for the looped one
    idx_p1D = sub2ind([Np,N],repmat((1:Np).',   [1,Norder-1]),j_not_i);     % 1D indices for (1:Np               ,  j(not ix) in stencil )
    idx_1D  = sub2ind([N,N], repmat(idx_p(:,ix),[1,Norder-1]),j_not_i);     % 1D indices for (j(ix) in stencil   ,  j(not ix) in stencil )
    lx(:,ix) = prod(mult_xp(idx_p1D),2) ./ prod(mult_x(idx_1D),2);          % lagrange multiplier for stencil position ix
end

if bPlot % stencil plot
    styleS = repmat({'-','-.','--',':'},[1,Np]);
    figure;
    plot(x,zeros(N,1),'kx-'); hold on; grid minor;
    for ip = ceil(Np/2):-1:1
        plot(x(idx_p(ip,:)) , lx(ip,:),styleS{ip},'color',two_colors(ip,Np,'Rbw2'));
        plot(xp(ip),0,'o','color',two_colors(ip,Np,'Rbw2'));
    end
end

%%% Computing output (vectorially)
for iy=Ncol:-1:1                                                            % looping columns
    idx_1D = sub2ind([Np,Ncol], idx_p , iy*ones(Np,Norder));                    % 1D indices for the stencil indices in column iy
    yp(:,iy) = (y(idx_1D).*lx) * ones(Norder,1);                                % multiplying y by the multipliers and summing over the stencil
end

%%% All vectorial
% for ip=Np:-1:1
%     for ix=Norder:-1:1
%         j_not_i = idx(idx~=ix);
%         lx(ip,ix) = prod(mult_xp(ip,idx_p(ip,j_not_i)),2) / prod(mult_x(idx_p(ip,ix),idx_p(ip,j_not_i)),2);
%     end
%     for iy=Ncol:-1:1
%         yp(ip,iy) = (y(idx_p(ip,:),iy).'.*lx(ip,:)) * ones(Norder,1);
%     end
% end

if bTranspose
    yp = yp.';
end

if bPlot
    figure;     plot(x,y,'-',xp,yp,'--'); legend('original','interpolated'); xlabel('x'); ylabel('y');
end

end % lagrange