function c = loop3D_prod(a,b)
% loop3D_prod makes the product of the 2D slices of a 3D matrix by looping
% over the third dimension.
%
% Usage:
%   (1)
%       c = loop3D_prod(a,b)
%
% See also: mtimesx
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% analyzing sizes and ensuring that they are compatible
size_a = size(a);
size_b = size(b);
Ndim_a = length(size_a);
Ndim_b = length(size_a);

if Ndim_a~=Ndim_b || any(size_a(3:end)~=size_b(3:end)) || size_a(2)~=size_b(1)
    size_str_a = strjoin(cellfun(@num2str,num2cell(size_a),'UniformOutput',false),' x ');
    size_str_b = strjoin(cellfun(@num2str,num2cell(size_b),'UniformOutput',false),' x ');
    error(['matrix a and b do not have compatible sizes. Matrix a is of size (',size_str_a,...
        ') and matrix b is of size (',size_str_b,'). Instead, they should have sizes (Ni x Nj x Nl x ...) and (Nj x Nk x Nl x ...) respectively']);
end

if length(size_a)>2
    for ii=size_a(3):-1:1
        c(:,:,ii) = a(:,:,ii)*b(:,:,ii);
    end
else
    c = a*b;
end

end % loop3D_prod