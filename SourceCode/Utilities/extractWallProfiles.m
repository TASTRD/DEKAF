function [x,Tw,ysw,mw,Tcst,fw] = extractWallProfiles(intel,options,varargin)
% extractWallProfiles returns the various wall profiles.
%
% Usage:
%   (1)
%       [x,Tw,ysw,mw,Tcst,fw] = extractWallProfiles(intel,options)
%
%   (2)
%       [...] = extractWallProfiles(...,'enthalpyAverage')
%       |--> computes the average temperature as the cuotient of the
%       integral of the sensible enthalpy and that of the specific heat
%       capacity, rather than simply averaging over x the temperature.
%
%   (3)
%       [...] = extractWallProfiles(...,'avoid1stPoint')
%       |--> avoids the first point of the domain, which was actually
%       obtained with the SS solution (no source terms)
%
% Inputs and outputs:
%   intel
%       classic DEKAF intel structure with all the flow-field quantities
%   options
%       classic DEKAF options structure with all the case options
%   x
%       wall streamwise coordinate [m]
%   Tw
%       wall temperature profile [K]
%   ysw
%       wall concentration profiles [-]
%   mw
%       wall mass-flux profile [kg/s-m^2]
%   Tcst
%       averaged wall temperature [K]
%
% Author: Fernando Miro Miro
% Date: April 2019
% GNU Lesser General Public License 3.0

[varargin,bEnthalpyAverage] = find_flagAndRemove('enthalpyAverage',varargin);
[~,       bAvoid1stPoint]   = find_flagAndRemove('avoid1stPoint',varargin);

Nx = size(intel.x,2);
if bAvoid1stPoint;      idx_x = [2,2:Nx];
else;                   idx_x = 1:Nx;
end

x       = intel.x(end,:);
Tw      = intel.T(end,idx_x);
ysw     = cellfun(@(cll)cll(end,idx_x),intel.ys,'UniformOutput',false);
% NOTE: we make the first station be the same as the second to avoid too
% big concentration gradients that would require large relaxation factors
rhow    = intel.rho(end,idx_x);
vw      = intel.v(end,idx_x);
mw      = rhow.*vw;

U_e   = mean(intel.U_e);
rho_e = mean(intel.rho_e);
mu_e  = mean(intel.mu_e);
Mw = trapz(x,mw); % total surface mass flow
fw = -Mw/(2*sqrt(U_e*rho_e*mu_e*max(x)));

if ~bEnthalpyAverage
    Tcst = trapz(x,Tw)/max(x);
else
    %%% Alternative - with h and cp
    switch options.numberOfTemp                                                 % defining temperatures for each of the energy modes
        case '1T';      TTrans_w = Tw(:);   TRot_w = Tw(:);     TVib_w = Tw(:);             TElec_w = Tw(:);            Tel_w = Tw(:);
        case '2T';      TTrans_w = Tw(:);   TRot_w = Tw(:);     TVib_w = intel.Tv(end,:).'; TElec_w = intel.Tv(end,:).';Tel_w = intel.Tv(end,:).';
        otherwise;      error(['the chosen number of temperatures ''',options.numberOfTemp,''' is not supported']);
    end
    y_sw = cell1D2matrix2D(ysw);                                                   % wall species mass fractions
    mixCnst = intel.mixCnst;                                                    % extracting mixture constants
    [R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...       % reshaping for getMixture_h_cp
                 bElectron_mat,hForm_s_mat,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat] = reshape_inputs4enthalpy(mixCnst.R_s,...
                 mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,TTrans_w,TRot_w,TVib_w,TElec_w,Tel_w);
                                                                                % computing enthalpy and heat capacity
    [h_w,cp_w] = getMixture_h_cp(y_sw,TTrans_mat,TRot_mat,TVib_mat,TElec_mat,Tel_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);
    hForm = (hForm_s_mat.*y_sw)*ones(mixCnst.N_spec,1);                         % computing formation enthalpy
%     hSens_w = h_w - hForm;                                                      % computing sensible enthalpy
    hSens_w = h_w - intel.h_inf;                                                % computing sensible enthalpy
    Tcst = trapz(x,hSens_w) / trapz(x,cp_w);                                    % averaging
    Tcst2 = intel.T_inf + trapz(x,hSens_w) / trapz(x,cp_w);                                   % averaging
    Tcst3 = trapz(x,Tw)/max(x);
    figure;
    subplot(2,2,1);
    plot(x,Tw,'displayname','T_w'); hold on;
    plot(x,Tcst*ones(size(x)),'displayname','Int_h / Int_{cp}');
    plot(x,Tcst2*ones(size(x)),'displayname','Int_h / Int_{cp} + T_{inf}');
    plot(x,Tcst3*ones(size(x)),'displayname','Int_T / L');
    xlabel('x [m]'); ylabel('T [K]'); legend(); grid minor; xlim([0,max(x)]);
    subplot(2,2,2);
    plot(x,hSens_w,'displayname','h_w'); hold on;
    xlabel('x [m]'); ylabel('h-h_\infty [J/kg]'); grid minor; xlim([0,max(x)]);
    subplot(2,2,4);
    plot(x,cp_w,'displayname','cp_w'); hold on;
    xlabel('x [m]'); ylabel('cp [J/kg-K]'); grid minor; xlim([0,max(x)]);
end

end % extractWallProfiles