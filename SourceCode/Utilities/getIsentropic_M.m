function Mn = getIsentropic_M(A_rat,gam,varargin)
% getIsentropic_M returns the Mach number for a series of area ratios
% assuming an isentropic flow in a nozzle.
%
% Usage:
%   (1)
%       M = getIsentropic_M(A_rat,gam)
%
%   (2)
%       M = getIsentropic_M(A_rat,gam,M0)
%       |--> allows to specify an initial guess for M, otherwise
%       initialized with a value of 2.
%
% Inputs and outputs:
%   A_rat           [-]
%       ratio between the nozzle area and the throat area (where the nozzle
%       is choked and sonic). A_rat = A/Astar
%   gam             [-]
%       heat-capacity ratio
%   M               [-]
%       Mach number assuming isentropic expansion
%
% Author: Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

if isempty(varargin);   Mn = 2*ones(size(A_rat));   % default intial guess
else;                   Mn = varargin{1};           % user-specified initial guess
end

a1 = A_rat;
a2 = ((gam+1)/2)^(-(gam+1)/(2*(gam-1)));
a3 = (gam-1)/2;
a4 = (gam+1)/(2*(gam-1));

func = @(M) a1-a2./M.*(1+a3.*M.^2).^a4;
dfunc_dM = @(M) a2./M.^2.*(1+a3.*M.^2).^a4 - a2./M.*a4.*(1+a3.*M.^2).^(a4-1).*2.*a3.*M;

err = 1;    tol = eps;
it=0;       itMax = 1000;
while err>tol && it<itMax
    dM = -func(Mn)./dfunc_dM(Mn);
    Mn = Mn+dM;
    err = max(abs(dM)) / max(abs(Mn));
    it=it+1;
end
disp(['The Mach number assuming an isentropic expansion converged up to ',num2str(err),' (tol=',num2str(tol),') after ',num2str(it),' iterations']);


end % getIsentropic_M