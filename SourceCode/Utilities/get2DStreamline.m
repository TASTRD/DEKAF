function [x_str,y_str] = get2DStreamline(x,y,u,v,x0,y0,varargin)
% get2DStreamline follows the 2D streamline of a flowfield.
%
% Usage:
%   (1)
%       [x_str,y_str] = get2DStreamline(x,y,u,v,x0,y0)
%
%   (2)
%       [x_str,y_str] = get2DStreamline(x,y,u,v,x0,y0,direction)
%       |--> tracks the streamline in a particular direction. Supported:
%               'forward'
%               'backward'
%               'both' (default)
%
% Inputs and outputs
%   x       streamwise position         (size Ny x Nx)
%   y       wall-normal position        (size Ny x Nx)
%   u       streamwise velocity field   (size Ny x Nx)
%   v       wall-normal velocity field  (size Ny x Nx)
%   x0,y0   position from which the streamline must be tracked
%   x_str   streamwise positions of the streamline (size N_str x 1)
%   y_str   wall-normal positions of the streamline (size N_str x 1)
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

if isempty(varargin)
    direction = 'both';
else
    direction = varargin{1};
end

[Ny,Nx] = size(u);

r = sqrt((x-x0).^2 + (y-y0).^2);                % distance from the tracked position
[~,idx] = min(r(:));                            % position of the minimum distance
[idx_i,idx_j] = ind2sub([Ny,Nx],idx);           % position of the minimum distance
switch direction
    case 'backward'                                 % range of x locations to track backward
    ix_range = idx_j-1:-1:1;
    case 'forward'                                  % range of x locations to track forward
    ix_range = idx_j+1:Nx;
    case 'both'                                     % range of x locations to track backward & forward
    ix_range = [idx_j+1:Nx,idx_j-1:-1:1];
    otherwise
        error(['the chosen marching direction ''',direction,''' is not supported.']);
end
N_str = length(ix_range)+1;                     % number of points to march and length of the streamline function
x_str = zeros(N_str,1);                         % allocating
y_str = zeros(N_str,1);
xi = x(idx_i,idx_j);                            % initial point
yi = y(idx_i,idx_j);
ui = u(idx_i,idx_j);                            % initial velocity
vi = v(idx_i,idx_j);
x_str(1) = xi;                                  % populating first point
y_str(1) = yi;
for ix=1:length(ix_range)                       % marching
    dx = x(idx_i,ix_range(ix)) - xi;                % step in x
    % NOTE: for the x steps, we use the same idx_i position in the matrix
    % as the initial point. So, to all effects, we are following one same
    % row in the position matrix to get our x locations
    dy = dx*vi/ui;                                  % step in y
    xi = xi+dx;                                     % new position
    yi = yi+dy;
    ui = interp1(y(:,ix_range(ix)),u(:,ix_range(ix)),yi,'spline'); % interpolating the new streamwise velocity
    vi = interp1(y(:,ix_range(ix)),v(:,ix_range(ix)),yi,'spline'); % interpolating the new wall-normal velocity
    x_str(ix+1) = xi;                               % storing new position
    y_str(ix+1) = yi;
    if ix_range(ix)==Nx && strcmp(direction,'both') % if we reached the last position and are doing both directions
        xi = x_str(1);          yi = y_str(1);      % we use the first position (initial user-specified point) for the next position
        ui = u(idx_i,idx_j);    vi = v(idx_i,idx_j);
    end
end
% sorting indices
[x_str,idx_sorted] = sort(x_str);
y_str = y_str(idx_sorted);