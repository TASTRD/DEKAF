function out = DEKAFversion()
% DEKAFversion is a function to keep track of the DEKAF version number.
% It returns the most recent tag as given by 'git --no-pager tag'.
% If the system call to git fails for whatever reason, the return value of
% the function is set to 'Unknown'.
%
% Usage:
%   (1)
%       DEKAFversion()
%
% Notes:
% It works using persistent variables to prevent repeated system calls.
% If you'd like to clear the persistent variable from MATLAB's memory,
% or in other words, reset the value of the persistent variable to empty,
% simply clear the function:
%
% > clear DEKAFversion
%
% This function is general enough to work for any standard git repository,
% provided one changes the usage of SourceCode to be whatever is appropriate.
%
% Author(s): Fernando Miro Miro, Ethan Beyak
% Date: February 2020
% GNU Lesser General Public License 3.0

persistent ver

if ~isempty(ver)
    % ver has been set already: we don't need to make another system call
    out = ver;
    return
end

try
    % searching in matlabpath
    str = matlabpath;                                                       % storing all matlabpaths into a single string (to be split)
    currentPath = pwd;                                                      % storing current path to go back to it later
    sep = getOSPathSeparator();
    allPaths = regexp(str,sep,'split');
    idx_SC = ~cellfun(@isempty,regexp(allPaths,'^(.*)SourceCode$','match'));% searching for the SourceCode folder
    switch nnz(idx_SC)                                                      % depending on the number of appearances of the SourceCode folder
        case 0                                                                  % break
            error('The folder ''SourceCode'' was not found among the matlabpaths');
        case 1                                                                  % perfect. Let's go to it
            eval(['cd ',allPaths{idx_SC}]);
        otherwise                                                               % break
            error(['More than one folder ''SourceCode'' was found among the matlabpaths:',newline,strjoin(allPaths(idx_SC),newline)]);
    end
    % Doing usual git commands through system calls pipe all input into less, or $PAGER.
    % On most OS's, MATLAB's system is not a fully functional terminal, so it can't pipe.
    % So we have to add the '--no-pager' option to bypass this unneeded functionality.
    [s,cmdout] = system('git --no-pager tag');
    eval(['cd ',currentPath]);                                              % going back to the original folder
    if s
        % system failed. We have no idea what cmdout is.
        % Let's assign ver to unknown and leave.
        ver = fail(s,cmdout);
    else
        % system had no problems! Let's get the final tag in the list.
        % Example:
        % cmdout =
        %     '0.0.0
        %      0.0.1
        %      0.0.2
        %      ';
        % Select the last row by matching whatever is before the last newline.
        verwrk = regexp(cmdout,'([^\n]+)\n$','tokens');
        ver = verwrk{1}{1};
    end
catch ME
    ver = fail(ME);
end

out = ver;

end % DEKAFversion
% ----------------------------------------------------------------------- %
function ver = fail(varargin)
unknown = 'Unknown';
if length(varargin) == 2 % usage: fail(s,cmdout)
    s = varargin{1};
    cmdout = varargin{2};
    w = ['System call returned an error of status (',num2str(s),')\n', ...
        cmdout];
elseif length(varargin) == 1 % usage: fail(ME)
    ME = varargin{1};
    w = [parse_exception(ME), '\n', ...
        'Could not execute system call to git to retrieve the version!\n'];
else
    error('Case should be unreachable! Contact a developer.');
end
w = [w, 'Assigning version to ''',unknown,'''.'];
warning(w, 'msgID');
ver = unknown;
end % fail
