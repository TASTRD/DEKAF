function perturbRelaxation = relaxFactFunction_conv(conv)
% relaxFactFunction_conv is a function to define the relaxation factor that
% should be used dynamically, correlating it to the convergence exclusively
%
% Usage:
%   (1)
%       perturbRelaxation = relaxFactFunction_conv(conv)
%
% Inputs and outputs:
%   conv
%       convergence at the previous iteration station
%   it
%       iteration counter
%   i_xi
%       marching-station counter
%
% Author: Fernando Miro Miro
% Date: April 2019
% GNU Lesser General Public License 3.0

conv_vec = [100,    0,       -1,     -2,     -3,     -4,     -5,     -100];
pert_vec = [0.001,  0.01,  0.05,    0.1,    0.5,    0.7,      1,        1];

perturbRelaxation = interp1(conv_vec,pert_vec,log10(conv),'linear');

end % relaxFactFunction_conv