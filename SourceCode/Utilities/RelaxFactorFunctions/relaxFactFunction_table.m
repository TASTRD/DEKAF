function perturbRelaxation = relaxFactFunction_table(conv,it,i_xi)
% relaxFactFunction_table is a function to define the relaxation factor that
% should be used dynamically, using an it-i_xi table
%
% Usage:
%   (1)
%       perturbRelaxation = relaxFactFunction_table(conv,it,i_xi)
%
% Inputs and outputs:
%   conv
%       convergence at the previous iteration station
%   it
%       iteration counter
%   i_xi
%       marching-station counter
%
% Author: Fernando Miro Miro
% Date: April 2019

itvec    =  [0,10,30,100,1000].'; % row-wise
xivec    =  [0,    10, 20, 50, 100,10000]; % column-wise
itxi_map = [[0.01 ,0.05,0.1,0.5,0.7,1] ; ... % it=0
            [0.05 ,0.1 ,0.4,0.5,0.7,1] ; ... % it=10
            [0.1  ,0.3 ,0.7,0.8,1  ,1] ; ... % it=30
            [0.3  ,0.5 ,0.8,1  ,1  ,1] ; ... % it=100
            [1    ,1   ,1  ,1  ,1  ,1]];     % it=1000

if conv<1e-3        % small enough convergence to assume it won't blow up
    perturbRelaxation = 1;
else                % check the look-up table
    perturbRelaxation = interp2(xivec,itvec,itxi_map,i_xi,it,'linear');
end

end % relaxFactFunction_table