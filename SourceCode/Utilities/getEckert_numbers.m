function [Ecu, Ecw, varargout] = getEckert_numbers(U_e, W_0, h_e, varargin)
%GETECKERT_NUMBERS Calculate the Eckert numbers in the direction perpendicular
% to the leading edge (Ecu) and parallel (Ecw)
%
% Usage:
%   (1)
%       [Ecu, Ecw, Ecp] = getEckert_numbers(U_e, W_0, h_e)
%
%   (2)
%       [Ecu, Ecw, Ecp] = getEckert_numbers(U_e, W_0, h_e, p_e, rho_e)
%
% Inputs and outputs:
%   U_e
%       streamwise velocity [m/s]
%   W_0
%       Spanwise velocity [m/s]
%   h_e
%       static enthalpy [J/kg]
%   p_e
%       static pressure [Pa]
%   rho_e
%       density [kg/m^3]
%   Ecu, Ecw, Ecp
%       Eckert numbers [-]
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

Ecu = U_e.^2./h_e;
Ecw = W_0^2./h_e;
if nargout>2
    p_e     = varargin{1};
    rho_e   = varargin{2};
    Ecp     = p_e./(h_e.*rho_e);
    varargout{1} = Ecp;
end

end % getEckert_numbers