%% Convert DEKAF's output to EPIC format
% Author(s):    Ethan Beyak
clc;
clear;
close all;

%addpath(genpath(getenv('DEKAF_DIRECTORY')));
tic
Nx_EPIC_BF_external = 'nointerpolation';%2000;
Ny_EPIC_BF = 250;

dekafsavepath = '/home/ebeyak/scratch/projects/uli_ee/data/X207o_v2/basic_states/matfiles/';
lside = {'bottom'};
lLam = {'00', '05'};

for ss = 1:length(lside)
    for ll = 1:length(lLam)

        % Clear all variables from previous iterations, except the externals
        keepVars('ss','lside','ll','lLam','dekafsavepath','Nx_EPIC_BF_external','Ny_EPIC_BF');

        dekafsuffix2save = ['D.',lLam{ll},'deg.cl065.',lside{ss},'.mat'];
        dekaffile2save = [dekafsavepath, dekafsuffix2save];
        %%
        if ~exist(dekaffile2save, 'file')
            error(['Unable to read file ''', dekaffile2save, '''. No such file or directory.']);
        end
        if ~exist('intel_out', 'var')
            disp(['(ss, ll) = (',num2str(ss),'/', num2str(length(lside)),...
                ', ', num2str(ll), '/', num2str(length(lLam)), ')']);
            disp(['Loading in saved data: ', dekaffile2save]);
            dekaf = load(dekaffile2save); % intel is named intel_out, options is named options_out.
            disp('Completed loading');
        end

        % For a representative Malik mapping, we use the y_i coming from DEKAF for
        % a decent approximation of EPIC's Malik mapping
        % NOTE: In the past, the exact y_i used within EPIC was created externally, but
        % it had negligible effect compared to DEKAF's y_i value. So for
        % simplicity, DEKAF's is used
        y_i = dekaf.intel_out.y_i;
        y_max = max(dekaf.intel_out.y,[],1);
        y_query = zeros([Ny_EPIC_BF, size(y_i,2)]);

        % Build differentiation matrices for Malik wall-normal domain
        % The orientation of the diff. matrix ultimately gives y = 0 at j = 1
        XI = linspace(0,1,Ny_EPIC_BF).';
        dXI = XI(2) - XI(1);
        [DM1,~] = FDdif(Ny_EPIC_BF,dXI);
        dumD = zeros(size(DM1));

        flds = {'u', 'v', 'w', 'T', 'rho'};
        dekaf.options_out.GICMsuffix = '_GICM';

        % Stride data to deresolve DEKAF's solution
        x_query_dekaf = 1:1:(dekaf.options_out.mapOpts.separation_i_xi-1);

        % Perform GICM interpolation
        for jj = 1:length(x_query_dekaf)

            ii = x_query_dekaf(jj);

            intel_GICM_input{jj}.u = dekaf.intel_out.u(:,ii);
            intel_GICM_input{jj}.v = dekaf.intel_out.v(:,ii);
            intel_GICM_input{jj}.w = dekaf.intel_out.w(:,ii);
            intel_GICM_input{jj}.T = dekaf.intel_out.T(:,ii);
            intel_GICM_input{jj}.rho = dekaf.intel_out.rho(:,ii);
            options4GICM{jj} = dekaf.options_out;
            % If there is stagnation, we need to supply dUe0_dx to avoid the
            % singularity at xi = 0 within GICM
            if intel_GICM_input{jj}.u(1) == 0
                if ~isfield(dekaf.intel_out, 'dUe0_dx')
                    error(['Stagnation flow has been detected in your boundary layer\n',...
                        'In order to perform GICM, the field ''dUe0_dx'' must be supplied to intel_out!']);
                end
                % This'll likely only occur for ii = 1, but ii is kept general here
                dUe0_dx = dekaf.intel_out.dUe0_dx(:,ii);
                % This term Ue_sqrt2xi0 represents the limiting case of U_e divided by
                % sqrt(2*xi) for stagnation flow only (when U_e = K*x)
                intel_GICM_input{jj}.Ue_sqrt2xi0 = sqrt(dUe0_dx/(dekaf.intel_out.rho_e(ii)*dekaf.intel_out.mu_e(ii)));
            end

            % Perform the Malik Mapping for dimensional wall-normal coord. and return
            % the differentiation matrix
            % Note that the y distribution coming out of this function is such that
            % y = 0 (aka wall) is the FIRST index, and the freestream is the LAST
            % index. This agrees with EPIC's implementation.
            [y_query(:,jj),D1,~] = MappingMalikFD_uptoD3(y_max(ii),y_i(ii),XI,DM1,dumD);

            % Choose to show gicm convergence
            options4GICM{jj}.display = false;

            % do the GICM
            intel_GICM_output{jj} = GICM_interp(options4GICM{jj}.eta_i,options4GICM{jj}.L,options4GICM{jj}.N,flds,...
                dekaf.intel_out.U_e(ii),dekaf.intel_out.xi(ii),intel_GICM_input{jj},...
                y_query(:,jj),D1,options4GICM{jj});

            % Extract out the local fields to save in the .mat
            y_GICM(:,jj) = intel_GICM_output{jj}.y_GICM;
            u_GICM(:,jj) = intel_GICM_output{jj}.u_GICM;
            v_GICM(:,jj) = intel_GICM_output{jj}.v_GICM;
            w_GICM(:,jj) = intel_GICM_output{jj}.w_GICM;
            T_GICM(:,jj) = intel_GICM_output{jj}.T_GICM;
            rho_GICM(:,jj) = intel_GICM_output{jj}.rho_GICM;

            if ~mod(jj,50)
                disp(['--- GICM: ',num2str(jj/length(x_query_dekaf)*100,'%.2f'),'% complete']);
            end
        end
        disp('--- GICM: finished');

        %%

        % Reshape to EPIC's Nx by Ny with j = 1 at the wall
        % (instead of GICM's Ny by Nx with j = 1 at the wall
        % These are cropped in the xi-equispaced domain
        y_crop = y_GICM.';
        u_crop = u_GICM.';
        v_crop = v_GICM.';
        w_crop = w_GICM.';
        T_crop = T_GICM.';
        rho_crop = rho_GICM.';
        s_crop = dekaf.intel_out.x(1,x_query_dekaf).';

        switch Nx_EPIC_BF_external
            case 'nointerpolation'
                s = s_crop;
                y = y_crop;
                u = u_crop;
                v = v_crop;
                w = w_crop;
                T = T_crop;
                rho = rho_crop;

                Nx_EPIC_BF = dekaf.options_out.mapOpts.separation_i_xi-1;
                fprintf('No interpolation yields %i Ns points.\n', Nx_EPIC_BF);
            % Deresolve the boundary layer onto an equispaced s domain
            otherwise
                Nx_EPIC_BF = Nx_EPIC_BF_external;
                s = linspace(s_crop(1), s_crop(end), Nx_EPIC_BF)';
                for ii = 1:Ny_EPIC_BF
                    y(:,ii) = interp1(s_crop, y_crop(:,ii), s, 'spline');
                    u(:,ii) = interp1(s_crop, u_crop(:,ii), s, 'spline');
                    v(:,ii) = interp1(s_crop, v_crop(:,ii), s, 'spline');
                    w(:,ii) = interp1(s_crop, w_crop(:,ii), s, 'spline');
                    T(:,ii) = interp1(s_crop, T_crop(:,ii), s, 'spline');
                    rho(:,ii) = interp1(s_crop, rho_crop(:,ii), s, 'spline');
                end
        end

        if any(any(rho<=0)) || any(any(T<=0))
            error('Density and/or temperature is nonpositive! GICM appears to have failed...');
        end

        % lambda_scale and phis are not used in DEKAF's import-usage to EPIC
        % Set placeholder values for these variables
        lambda_scale = ones(Nx_EPIC_BF,1);
        phis = ones(Nx_EPIC_BF,1);

        % Assign radii of curvature
        % Since transverse curvature is not accounted for DEKAF, we will neglect it in EPIC
        R_rev = 1e7*ones(Nx_EPIC_BF,1);
        % New method uses R_mrch directly from the airfoil coordinates (post-interp.)
        R_mrch = interp1(dekaf.intel_out.x(1,:), dekaf.intel_out.airfoil.R_mrch, s, 'spline');
        % Old method ignores R_mrch in DEKAF
        % % % R_mrch = 1e7*ones(Nx_EPIC_BF,1);

        % New method with x and y used directly (post-interp.) from the airfoil coordinates.
        x = interp1(dekaf.intel_out.x(1,:), dekaf.intel_out.airfoil.x, s, 'spline');
        y_geom = interp1(dekaf.intel_out.x(1,:), dekaf.intel_out.airfoil.y, s, 'spline');
        surf_coords = [x, y_geom, zeros(Nx_EPIC_BF,1)]; % "[x, y, z]"

        % Old method with x = s
        % % NOTE: EPIC's streamwise domain variable is 'x', whereas locally we've called this s
        % % Let us switch back to x, still knowing that it indeed is the arc coordinate
        % % % surf_coords = [s, zeros(Nx_EPIC_BF,1), zeros(Nx_EPIC_BF,1)]; % "[x, y, z]"
        % % % x = s;

        if y(1,1) ~= 0
            error('DEVELOPER BUG: incorrectly shaped output to EPIC! double check your manipulations...')
        end

        dekaffile2save_Nx = [dekaffile2save(1:end-4), 'Nx', num2str(Nx_EPIC_BF), 'Ny', num2str(Ny_EPIC_BF), '.mat'];
        epicfile2save = strrep(dekaffile2save_Nx, '.mat', '_EPIC.mat');
        %%

        % Save all required fields
        epicfields = {'x','y','u','v','w','T','rho',...
            'surf_coords','lambda_scale','phis','R_mrch','R_rev'};
        save(epicfile2save,epicfields{:});

    end
end
toc
