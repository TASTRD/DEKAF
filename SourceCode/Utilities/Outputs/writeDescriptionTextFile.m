function writeDescriptionTextFile(intel,options,filename)
%WRITEDESCRIPTIONTEXTFILE Write a text file with the fields of options and
% intel delimited
%
% Example:
%   writeDescriptionTextFile(intel,options,'DEKAF_README_Test_Run.log');
%
% See also setDefaults.m
% Author(s):    Fernando Miro Miro
%               Koen Groot
% GNU Lesser General Public License 3.0

newline = sprintf('\n'); %#ok<SPRINTFN>

str     = [];
fileID  = fopen(filename,'w');
dash    = '-------------------------';
header  = [dash,' DEKAF RUN DESCRIPTION ',dash];
str     = [str,header,newline];
str     = [str,sprintf('--- Output written on %s',datestr(datetime('now'))),newline];
str     = [str,'--- Note that if any values are not shown, default values are used.',newline];
str     = [str,'--- All intel units are SI',newline];
str     = [str,newline];

allvars_intel   = fieldnames(intel);
cnt = 0;
for ii=1:length(allvars_intel)
    myField = intel.(allvars_intel{ii});
    % Check if the field is a number
    if ~isstruct(myField) && nnz(size(myField)==[1,1])==2
        cnt = cnt + 1;
        intelVars{cnt} = allvars_intel{ii};
    end
end

allvars_options    = fieldnames(options);
cnt = 0;
for ii=1:length(allvars_options)
    myField = options.(allvars_options{ii});
    % Check if the field is a number
    if ~isstruct(myField) && nnz(size(myField)==[1,1])==2
        cnt = cnt + 1;
        optionsVars{cnt} = allvars_options{ii};
    end
end

% Align values on nCol column in text file
nCol     = length(header);

str     = [str,'------ INTEL INPUTS',newline];
for ii=1:length(intelVars)
    lineStart   = ['--- intel.',sprintf(intelVars{ii})];
    nSp         = nCol-length(lineStart);
    val         = sprintf('%*s',nSp,num2str(intel.(intelVars{ii})));
    str         = [str,lineStart,val,newline];
end
str     = [str,newline];

str     = [str,'------ OPTIONS INPUTS',newline];
for ii=1:length(optionsVars)
    lineStart   = ['--- options.',sprintf(optionsVars{ii})];
    nSp         = nCol-length(lineStart);
    val         = sprintf('%*s',nSp,num2str(options.(optionsVars{ii})));
    str         = [str,lineStart,val,newline];
end
str     = [str,newline];

tail    = [dash,' END DEKAF DESCRIPTION ',dash];
str     = [str,tail,newline];

fprintf(fileID,str);
fclose(fileID);

end % writeDescriptionTextFile.m
