function [muT,betaK,betaW,sigmaK,sigmaW,alphaW,gammaT] = getTurbulentSSTkomega_all(kT,wT,dkT_deta,dwT_deta,df_deta2,rho,mu,D_eta,U_e,xi,xBar,coordsys,cylCoordFuncs)
% getTurbulentSSTkomega_all computes all properties necessary for the turbulent SST k-omega model.
%
% Usage:
%   (1)
%       [muT,betaK,betaW,sigmaK,sigmaW,alphaW,gammaT] = getTurbulentSSTkomega_all(kT,wT,...
%                    dkT_deta,dwT_deta,df_deta2,rho,mu,D_eta,U_e,xi,xBar,coordsys,cylCoordFuncs)
%
% Inputs:
%   kT              [J/kg]                              (N_eta x 1)
%       turbulent kinetic energy
%   wT              [1/s]                               (N_eta x 1)
%       turbulent vorticity
%   dkT_deta        [J/kg]                              (N_eta x 1)
%       wall-normal (eta) gradient of the turbulent kinetic energy
%   dwT_deta        [1/s]                               (N_eta x 1)
%       wall-normal (eta) gradient of the turbulent vorticity
%   df_deta2        [-]                                 (N_eta x 1)
%       gradient of the non-dimensional stream function f wrt eta twice
%   rho             [kg/m^3]                            (N_eta x 1)
%       mixture density
%   mu              [kg/m-s]                            (N_eta x 1)
%       mixture (laminar) viscosity
%   D_eta           [-]                             (N_eta x N_eta)
%       differentiation matrix in eta
%   rho_e           [kg/m^3]                        (1 x 1)
%       density at the boundary-layer edge
%   U_e             [m/s]                           (1 x 1)
%       velocity at the boundary-layer edge
%   xi              [kg^2/m^2-s^2]                  (1 x 1)
%       transformed streamwise variable (after illingworth and
%       Probstein-Elliot)
%   xBar            [m]                             (1 x 1)
%       transformed streamwise variable (after Probstein-Elliot)
%   coordsys
%       string identifying the coordinate system. Supported values are
%       found in <a href="matlab:help get_xyInvTransformed">get_xyInvTransformed</a>
%   cylCoordFuncs
%       structure containing various functions to compute various
%       geometrical quantities related to cylindrical coordinates:
%           .rc(xc)             [m]
%               spanwise radius of curvature
%           .alphac(xc)         [deg]
%               streamwise inclination angle
%           .Ixc_rc2(xc)        [m]
%               integral of (rc/L)^2 wrt xc
%           .Dxc_rc(xc)         [-]
%               derivative of rc wrt xc
%           .Dxc_alphac(xc)     [1/m]
%               derivative of alphac wrt xc
%           .IxBar_1rc2(xBar)   [m]
%               integral of (L/rc)^2 wrt xBar
%
% Outputs:
%   muT             [kg/m-s]                            (N_eta x 1)
%       turbulent viscosity
%   betaK, betaW, sigmaK, sigmaW, alphaW, gammaT        (N_eta x 1)
%       parameters appearing in the turbulent-kinetic-energy and vorticity
%       equations.
%
% References:
%       Menter, F. R. (1994). Two-equation eddy-viscosity turbulence models
%   for engineering applications. AIAA Journal, 32(8), 1598–1605.
%   https://doi.org/10.2514/3.12149
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,y,~,~,deta_dy] = getDimensional_xy(D_eta,rho,U_e,xi,xBar,coordsys,cylCoordFuncs);    % dimensional y vectors etc
dkT_dy = dkT_deta.*deta_dy;
dwT_dy = dwT_deta.*deta_dy;
du_dy  = df_deta2.*deta_dy*U_e;

F1 = getTurbulentSSTkomega_F1(kT,wT,dkT_dy,dwT_dy,y,rho,mu); % weighing parameter

betaK  = 0.09*ones(size(F1));           % Menter Eq. A5 and A13
betaW  = 0.075  *F1 + 0.0828 *(1-F1);   % Menter Eq. A3, A5 and A13
sigmaK = 0.85   *F1 + 1.0    *(1-F1);   % Menter Eq. A3, A5 and A13
sigmaW = 0.5    *F1 + 0.856  *(1-F1);   % Menter Eq. A3, A5 and A13
alphaW =              1.712  *(1-F1);   % Menter Eq. A3, A5 and A13
gammaT = 0.55317*F1 + 0.44035*(1-F1);   % Menter Eq. A3, A5 and A13

muT = getTurbulentSSTkomega_muT(kT,wT,y,rho,mu,du_dy);

muT_min = 1e-8*rho;
%%%% DEBUGGING
% figure(121); clf;
% plot(muT,y,'-',muT_min,y,'-.'); hold on;
% legend('\mu_T','\mu_{T min}');
% xlabel('\mu_T [kg/m-s]'); ylabel('y [m]');
%%%% END
idx4min = (isnan(muT)|muT<muT_min);
muT(idx4min) = muT_min(idx4min);

%%%% DEBUGGING
% plot(muT,y,'--','displayname','\mu_{T out}');
% pause(0.5);
% if all(idx4min);      disp('all locations took the minimum muT');
% else;                 disp('not all locations took the minimum muT');
% end
% muT = muT_min;
%%%% END

end % getTurbulentSSTkomega_all