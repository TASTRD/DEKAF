function wT_w = getWall_turbulentViscosity(D_eta,rho,mu,df_deta2,U_e,xi,xBar,coordsys,cylCoordFuncs)
% getWall_turbulentViscosity returns the value to be imposed on the
% turbulent viscosity at the wall.
%
% Usage:
%   (1)
%       wT_w = getWall_turbulentViscosity(D_eta,rho,mu,df_deta2,U_e,xi,xBar,coordsys,cylCoordFuncs)
%
% Inputs:
%   D_eta           [-]                             (N_eta x N_eta)
%       differentiation matrix in eta
%   rho             [kg/m^3]                        (N_eta x 1)
%       mixture density
%   mu              [kg/m-s]                        (N_eta x 1)
%       mixture (laminar) viscosity
%   df_deta2        [-]                             (N_eta x 1)
%       derivative of the non-dimensional stream function f wrt eta twice
%   U_e             [m/s]                           (1 x 1)
%       velocity at the boundary-layer edge
%   xi              [kg^2/m^2-s^2]                  (1 x 1)
%       transformed streamwise variable (after illingworth and
%       Probstein-Elliot)
%   xBar            [m]                             (1 x 1)
%       transformed streamwise variable (after Probstein-Elliot)
%   coordsys
%       string identifying the coordinate system. Supported values are
%       found in <a href="matlab:help get_xyInvTransformed">get_xyInvTransformed</a>
%   cylCoordFuncs
%       structure containing various functions to compute various
%       geometrical quantities related to cylindrical coordinates:
%           .rc(xc)             [m]
%               spanwise radius of curvature
%           .alphac(xc)         [deg]
%               streamwise inclination angle
%           .Ixc_rc2(xc)        [m]
%               integral of (rc/L)^2 wrt xc
%           .Dxc_rc(xc)         [-]
%               derivative of rc wrt xc
%           .Dxc_alphac(xc)     [1/m]
%               derivative of alphac wrt xc
%           .IxBar_1rc2(xBar)   [m]
%               integral of (L/rc)^2 wrt xBar
%
% Outputs:
%   wT_w                [1/s]                       (1 x 1)
%       vorticity value at the wall
%
% References:
%       Menter, F. R. (1994). Two-equation eddy-viscosity turbulence models
%   for engineering applications. AIAA Journal, 32(8), 1598–1605.
%   https://doi.org/10.2514/3.12149
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,y,~,~,deta_dy] = getDimensional_xy(D_eta,rho,U_e,xi,xBar,coordsys,cylCoordFuncs);
[yPlus,Lref] = get_yPlus(rho,mu,y,df_deta2,U_e,deta_dy);
if yPlus(end-1)>3
    error(['The value of y+ at the first wall-normal station (',num2str(yPlus(end-1)),') is higher than the required (3). The mesh should be refined further.']);
end
if yPlus(end-1)<1;      dy0 = Lref(end-1);
else;                   dy0 = y(end-1);
end
wT_w = 800.*mu(end)./(rho(end).*dy0^2);     % wall value of the vorticity

end % getWall_turbulentViscosity