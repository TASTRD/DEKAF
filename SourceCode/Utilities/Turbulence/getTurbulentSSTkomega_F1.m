function F1 = getTurbulentSSTkomega_F1(kT,wT,dkT_dy,dwT_dy,y,rho,mu)
% getTurbulentSSTkomega_F1 computes the weighing parameter F1 needed for
% the turbulent SST k-omega model. 
%
% Usage:
%   (1)
%       F1 = getTurbulentSSTkomega_F1(kT,wT,dkT_dy,dwT_dy,y,rho,mu)
%
% Inputs and outputs:
%   kT              [J/kg]                              (N_eta x 1)
%       turbulent kinetic energy
%   wT              [1/s]                               (N_eta x 1)
%       turbulent vorticity
%   dkT_dy          [J/kg-m]                            (N_eta x 1)
%       wall-normal gradient of the turbulent kinetic energy
%   dwT_dy          [1/s-m]                             (N_eta x 1)
%       wall-normal gradient of the turbulent vorticity
%   y               [m]                                 (N_eta x 1)
%       streamwise position in physical units
%   rho             [kg/m^3]                            (N_eta x 1)
%       mixture density
%   mu              [kg/m-s]                            (N_eta x 1)
%       mixture (laminar) viscosity
%   F1              [-]                                 (N_eta x 1)
%       weighing parameter
%
% References:
%       Menter, F. R. (1994). Two-equation eddy-viscosity turbulence models
%   for engineering applications. AIAA Journal, 32(8), 1598–1605.
%   https://doi.org/10.2514/3.12149
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

CDkw = max(1.712*rho./wT .* dkT_dy.*dwT_dy , 1e-10);                        % Menter Eq. A10
% NOTE: Menter actually keeps the limiter to 1e-20, rather than 1e-10, but
% the latter is what is implemented in CFD++

max_bracket = max(sqrt(kT)./(0.09.*wT.*y) , 500.*mu./(rho.*wT.*y.^2));      % Menter Eq. A9
arg1        = min(max_bracket , 3.424.*rho.*kT./(CDkw .* y.^2));

F1 = tanh(arg1.^4);                                                         % Menter Eq. A8

end % getTurbulentSSTkomega_F1