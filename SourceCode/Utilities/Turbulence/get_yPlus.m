function [yPlus,Lref] = get_yPlus(rho,mu,y,df_deta2,U_e,deta_dy)
% get_yPlus computes the non-dimensional wall distance y+
%
% Usage:
%   (1)
%       [yPlus,Lref] = get_yPlus(rho,mu,y,df_deta2,U_e,deta_dy)
%
% Inputs:
%   rho             [kg/m^3]        (N_eta x 1)
%       mixture density
%   mu              [kg/m-s]        (N_eta x 1)
%       dynamic viscosity
%   y               [m]             (N_eta x 1)
%       dimensional wall-normal distance
%   df_deta2        [-]             (N_eta x 1)
%       derivative of the non-dimensional stream function f wrt eta twice
%   U_e             [m/s]           (1 x 1)
%       edge velocity
%   deta_dy         [1/m]           (N_eta x 1)
%       result of undoing the Illingworth and (if necessary) the
%       Probstein-Elliot transformation.
%
% Outputs:
%   yPlus           [-]             (N_eta x 1)
%       non-dimensional wall distance
%   Lref            [m]             (N_eta x 1)
%       reference length used to non-dimensionalize
%
% Author(s): Fernando Miro Miro
%
% GNU Lesser General Public License 3.0

du_dy = df_deta2.*U_e.*deta_dy;             % velocity gradient
tau = mu.*abs(du_dy);                       % stress
Lref = 1 ./ (rho./mu.*sqrt(tau(end)./rho)); % non-dimensionalizing length
yPlus = y./Lref;                            % non-dimensional wall distance

end % get_yPlus 