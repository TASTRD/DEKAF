function muT = getTurbulent_mu_SmithCebeci(rho,mu,df_deta,df_deta2,g,eta,D_eta,rho_e,U_e,xi,xBar,coordsys,cylCoordFuncs)
% getTurbulent_mu_SmithCebeci computes the turbulent eddy viscosity with
% the model proposed by Smith and Cebeci.
%
% Usage:
%   (1)
%       muT = getTurbulent_mu_SmithCebeci(rho,mu,df_deta,df_deta2,g,eta,D_eta,rho_e,U_e,xi,xBar,coordsys,cylCoordFuncs)
%
% Inputs:
%   rho             [kg/m^3]                        (N_eta x 1)
%       mixture density
%   mu              [kg/m-s]                        (N_eta x 1)
%       dynamic viscosity
%   df_deta         [-]                             (N_eta x 1)
%       non-dimensional velocity profile
%   df_deta2        [-]                             (N_eta x 1)
%       derivative of the non-dimensional velocity profile with the
%       computational wall-normal variable eta
%   eta             [-]                             (N_eta x 1)
%       computational wall-normal variable
%   D_eta           [-]                             (N_eta x N_eta)
%       differentiation matrix in eta
%   rho_e           [kg/m^3]                        (1 x 1)
%       density at the boundary-layer edge
%   U_e             [m/s]                           (1 x 1)
%       velocity at the boundary-layer edge
%   xi              [kg^2/m^2-s^2]                  (1 x 1)
%       transformed streamwise variable (after illingworth and
%       Probstein-Elliot)
%   xBar            [m]                             (1 x 1)
%       transformed streamwise variable (after Probstein-Elliot)
%   coordsys
%       string identifying the coordinate system. Supported values are
%       found in <a href="matlab:help get_xyInvTransformed">get_xyInvTransformed</a>
%   cylCoordFuncs
%       structure containing various functions to compute various
%       geometrical quantities related to cylindrical coordinates:
%           .rc(xc)             [m]
%               spanwise radius of curvature
%           .alphac(xc)         [deg]
%               streamwise inclination angle
%           .Ixc_rc2(xc)        [m]
%               integral of (rc/L)^2 wrt xc
%           .Dxc_rc(xc)         [-]
%               derivative of rc wrt xc
%           .Dxc_alphac(xc)     [1/m]
%               derivative of alphac wrt xc
%           .IxBar_1rc2(xBar)   [m]
%               integral of (L/rc)^2 wrt xBar
%
% Outpus:
%   muT             [kg/m-s]                        (N_eta x 1)
%       turbulent eddy viscosity
%
% References:
%   (1) Smith, A. M. O., & Cebeci, T. (1967). Numerical Solution of the
%       Turbulent Boundary Layer Equations. Douglas Aircraft Division
%       Technical report DAC 33735.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

%%% FIXME: use a better algorithm for the BL height

% Computing auxiliary fields
I_eta       = integral_mat(D_eta,'du');                 % integration matrix
[~,y,~,~,deta_dy] = getDimensional_xy(D_eta,rho,U_e,xi,xBar,coordsys,cylCoordFuncs);    % dimensional y vectors etc
deltaStar   = getBL_deltaStar(I_eta,1./deta_dy,df_deta,rho_e./rho,'passIntegralMat');   % displacement thickness
% [~,~,delta] = getBL_delta(df_deta, df_deta2, y, eta, false, g);                         % boundary-layer thickness
delta       = getBL_delta(df_deta, df_deta2, y);                                        % boundary-layer thickness
nu          = mu./rho;                              % kinematic viscosity
du_dy       = U_e * df_deta2.*deta_dy;              % velocity gradient in dimensional units
tauStress_w = mu(end).*du_dy(end);                  % viscous stress at the wall
nu_w        = nu(end);                              % kinematic viscosity at the wall
rho_w       = rho(end);                             % density at the wall
k1          = 0.4;                                  % constants (Smith & Cebeci pag. 22-23)
k2          = 0.0168;

% Outer region
gamma_out   = 0.5*(1-erf(5*(y./delta - 0.78)));     % Intermittency factor (Eq. 6.26 in Smith & Cebeci)
muT_out     = k2*rho.*U_e.*deltaStar.*gamma_out;    % eddy viscosity in the outer region (Eq. 6.27 in Smith & Cebeci)

% Inner region
A = 26 * nu_w * sqrt(rho_w/tauStress_w);            % constant, specified shortly after Eq. 6.24 in Smith & Cebeci
nuBar = nu_w;                                       % initial guess
err=1;      it=0;                                   % allocating
tol=eps;    itMax = 10;                             % convergence limits
while err>tol && it<itMax                           % until convergence
    it=it+1;                                                                            % increase counter
    nuBarOld    = nuBar;                                                                % storing value of the averaged kinematic viscosity in the previous run
    muT_in      = rho.*k1^2.*y.^2.*(1-exp(-sqrt(nu_w/nuBar) .* y./A)).^2 .* abs(du_dy);   % eddy viscosity in the inner layer (Eq. 6.24 in Smith & Cebeci)
    idxCrossPts = find(diff(sign(muT_in - muT_out))~=0,1,'last');                       % finding points where the viscosities of the two regions cross
    L_IO        = y(idxCrossPts);                                                       % height of the inner region
    idx4avg     = idxCrossPts:length(y);                                                % positions to include ini the averaging
    nuBar       = 1/L_IO * I_eta(1,idx4avg) * (nu(idx4avg)./deta_dy(idx4avg));          % averaging the kinematic in the inner region
    % NOTE: we only take the first row of I_eta, because we want the total integrated value, not the cummulative sum for each position 
    err         = abs(1-nuBar/nuBarOld);                                                % normalized difference in the averaged kinematic viscosity
end
muT                 = muT_in;                       % populating inner region
muT(1:idxCrossPts)  = muT_out(1:idxCrossPts);       % populating outer region
%%%% DEBUGGING
%{
figure(101); clf;
plot(muT_in,eta); hold on;
plot(muT_out,eta);
plot(muT,eta,'k--');
%}

end % getTurbulent_mu_SmithCebeci