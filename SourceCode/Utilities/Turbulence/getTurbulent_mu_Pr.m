function [muT,PrT,varargout] = getTurbulent_mu_Pr(modelTurbulence,rho,mu,df_deta,df_deta2,g,eta,D_eta,rho_e,U_e,xi,xBar,coordsys,cylCoordFuncs,varargin)
% getTurbulent_mu_Pr computes the turbulent viscosity and Prandtl number.
%
% Usage:
%   (1)     modelTurbulence = 'none' or 'SmithCebeci1967'
%       [muT,PrT] = getTurbulent_mu_Pr(modelTurbulence,rho,mu,df_deta,df_deta2,g,eta,D_eta,rho_e,U_e,xi,xBar,coordsys,cylCoordFuncs)
%
%   (2)     modelTurbulence = 'SSTkomega'
%       [...,betaK,betaW,sigmaK,sigmaW,alphaW,gammaT] = getTurbulent_mu_Pr(...,kT,wT,dkT_deta,dwT_deta)
%
% Inputs:
%   rho             [kg/m^3]                        (N_eta x 1)
%       mixture density
%   mu              [kg/m-s]                        (N_eta x 1)
%       dynamic viscosity
%   df_deta         [-]                             (N_eta x 1)
%       non-dimensional velocity profile
%   df_deta2        [-]                             (N_eta x 1)
%       derivative of the non-dimensional velocity profile with the
%       computational wall-normal variable eta
%   eta             [-]                             (N_eta x 1)
%       computational wall-normal variable
%   D_eta           [-]                             (N_eta x N_eta)
%       differentiation matrix in eta
%   rho_e           [kg/m^3]                        (1 x 1)
%       density at the boundary-layer edge
%   U_e             [m/s]                           (1 x 1)
%       velocity at the boundary-layer edge
%   xi              [kg^2/m^2-s^2]                  (1 x 1)
%       transformed streamwise variable (after illingworth and
%       Probstein-Elliot)
%   xBar            [m]                             (1 x 1)
%       transformed streamwise variable (after Probstein-Elliot)
%   coordsys
%       string identifying the coordinate system. Supported values are
%       found in <a href="matlab:help get_xyInvTransformed">get_xyInvTransformed</a>
%   cylCoordFuncs
%       structure containing various functions to compute various
%       geometrical quantities related to cylindrical coordinates:
%           .rc(xc)             [m]
%               spanwise radius of curvature
%           .alphac(xc)         [deg]
%               streamwise inclination angle
%           .Ixc_rc2(xc)        [m]
%               integral of (rc/L)^2 wrt xc
%           .Dxc_rc(xc)         [-]
%               derivative of rc wrt xc
%           .Dxc_alphac(xc)     [1/m]
%               derivative of alphac wrt xc
%           .IxBar_1rc2(xBar)   [m]
%               integral of (L/rc)^2 wrt xBar
%   kT              [J/kg]                          (N_eta x 1)
%       turbulent kinetic energy
%   wT              [1/s]                           (N_eta x 1)
%       turbulent vorticity
%   dkT_deta        [J/kg]                          (N_eta x 1)
%       wall-normal (eta) gradient of the turbulent kinetic energy
%   dwT_deta        [1/s]                           (N_eta x 1)
%       wall-normal (eta) gradient of the turbulent vorticity
%
% Outpus:
%   muT             [kg/m-s]                        (N_eta x 1)
%       turbulent eddy viscosity
%   PrT             [-]                             (N_eta x 1)
%       turbulent eddy Prandtl number
%   betaK, betaW, sigmaK, sigmaW, alphaW, gammaT    (N_eta x 1)
%       parameters appearing in the turbulent-kinetic-energy and vorticity
%       equations.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

switch modelTurbulence
    case 'none'
        muT = zeros(size(rho));
        PrT = zeros(size(rho));
        varargout = cell(1,6);
    case 'SmithCebeci1967'
        muT = getTurbulent_mu_SmithCebeci(rho,mu,df_deta,df_deta2,g,eta,D_eta,rho_e,U_e,xi,xBar,coordsys,cylCoordFuncs);
        PrT = ones(size(rho));
        varargout = cell(1,6);
    case 'SSTkomega'
        [muT,varargout{1:6}] = getTurbulentSSTkomega_all(varargin{:},df_deta2,rho,mu,D_eta,U_e,xi,xBar,coordsys,cylCoordFuncs);
        PrT = ones(size(rho));
    otherwise
        error(['The chosen turbulence model ''',modelTurbulence,''' is not supported']);
end

end % getTurbulent_mu_Pr