function muT = getTurbulentSSTkomega_muT(kT,wT,y,rho,mu,du_dy)
% getTurbulentSSTkomega_muT computes the turbulent viscosity using the
% turbulent SST k-omega model.
%
% Usage:
%   (1)
%       muT = getTurbulentSSTkomega_muT(kT,wT,y,rho,mu,du_dy)
%
% Inputs and outputs:
%   kT              [J/kg]                              (N_eta x 1)
%       turbulent kinetic energy
%   wT              [1/s]                               (N_eta x 1)
%       turbulent vorticity
%   y               [m]                                 (N_eta x 1)
%       streamwise position in physical units
%   rho             [kg/m^3]                            (N_eta x 1)
%       mixture density
%   mu              [kg/m-s]                            (N_eta x 1)
%       mixture (laminar) viscosity
%   du_dy           [1/s]                               (N_eta x 1)
%       velocity gradient (vorticity)
%   muT             [kg/m-s]                            (N_eta x 1)
%       turbulent viscosity
%
% References:
%       Menter, F. R. (1994). Two-equation eddy-viscosity turbulence models
%   for engineering applications. AIAA Journal, 32(8), 1598–1605.
%   https://doi.org/10.2514/3.12149
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

F2 = getTurbulentSSTkomega_F2(kT,wT,y,rho,mu);
max_term = max(0.31*wT , F2.*abs(du_dy));
muT = 0.31*rho.*kT./max_term;                   % Menter Eq. A14
%%%% DEBUGGING
% figure(122); clf;
% subplot(1,2,1);
% semilogx(0.31*wT , y, '-', F2.*abs(du_dy),y,'-.',0.31*rho.*kT,y,'--',muT,y,'-');
% legend('0.31*wT','F2.*abs(du_dy)','0.31*rho.*kT','muT');
% ylabel('y [m]');
% subplot(1,2,2);
% plot(muT,y);
% pause(0.5);
%%%% END

end % getTurbulentSSTkomega_F2