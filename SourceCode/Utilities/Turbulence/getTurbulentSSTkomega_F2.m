function F2 = getTurbulentSSTkomega_F2(kT,wT,y,rho,mu)
% getTurbulentSSTkomega_F2 computes the weighing parameter F2 needed for
% the turbulent SST k-omega model. 
%
% Usage:
%   (1)
%       F2 = getTurbulentSSTkomega_F2(kT,wT,y,rho,mu)
%
% Inputs and outputs:
%   kT              [J/kg]                              (N_eta x 1)
%       turbulent kinetic energy
%   wT              [1/s]                               (N_eta x 1)
%       turbulent vorticity
%   y               [m]                                 (N_eta x 1)
%       streamwise position in physical units
%   rho             [kg/m^3]                            (N_eta x 1)
%       mixture density
%   mu              [kg/m-s]                            (N_eta x 1)
%       mixture (laminar) viscosity
%   F2              [-]                                 (N_eta x 1)
%       weighing parameter
%
% References:
%       Menter, F. R. (1994). Two-equation eddy-viscosity turbulence models
%   for engineering applications. AIAA Journal, 32(8), 1598–1605.
%   https://doi.org/10.2514/3.12149
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

arg2 = max(2*sqrt(kT)./(0.09.*wT.*y) , 500.*mu./(rho.*wT.*y.^2)); % Menter Eq. A16
F2   = tanh(arg2.^2);                                       % Menter Eq. A15

end % getTurbulentSSTkomega_F2