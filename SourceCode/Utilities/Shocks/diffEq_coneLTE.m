function dy = diffEq_coneLTE(theta,y,s,X_E,mixCnst,options,flag)
% diffEq_coneLTE builds the system of differential equations describing the
% conical inviscid flow after a shock in LTE.
%
% Usage:
%   (1)
%       dy = diffEq_coneLTE(theta,y,s,X_E,mixCnst,options,flag)
%
% Inputs and outputs:
%   theta = pitch angle [rad]
%   y     = [Vr,          Vtheta,         p]
%   dy    = [dVr/dtheta,  dVtheta/dtheta, dp/dtheta]
%
%   s       entropy (cst)
%   X_E     elemental fractions
%   mixCnst structure with the mixture constants
%           IMPORTANT! for TPG1T, it must also include the initial guess
%           for T (mixCnst.T0) and the preshock mass fractions mixCnst.y_s1
%   options structure with chosen flow options
%   flag    string identifying if it is a 'TPG1T' or an 'LTE' shock
%
% References:
%   [1] Anderson Jr, J. D. (2006). Hypersonic and High Temperature Gas
%   Dynamics (Second). Reston VA: American Institute of Aeronautics and
%   Astronautics. https://doi.org/https://doi.org/10.2514/4.861956
%
% Author: Fernando Miro Miro
% Date: November 2018
% GNU Lesser General Public License 3.0

options.EoS = protectedvalue(options,'EoS','idealGas');

p       = y(3);
Vtheta  = y(2);
Vr      = y(1);

switch flag
    case 'TPG1T'
        y_s = mixCnst.y_s1; % constant and passed through mixCnst
        T = get_NRTfroms(s,y_s,p,mixCnst.T0,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,...
            mixCnst.gDegen_s,mixCnst.hForm_s,mixCnst.nAvogadro,mixCnst.hPlanck,mixCnst.kBoltz,mixCnst.L_s,mixCnst.sigma_s,mixCnst.thetaRot_s,options);
        a = getMixture_a(T,T,T,T,T,y_s,mixCnst.Mm_s,mixCnst.R0,mixCnst.nAtoms_s,...     % Anderson Eq. 14.67
            mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,options,mixCnst.AThermFuncs_s);
    case 'LTE'
        [X_s,T] = getEquilibrium_X_complete(p,s,'mtimesxs',options.mixture,mixCnst,options.modelEquilibrium,0,options,'variable_XE',X_E);
        y_s = getEquilibrium_ys(X_s,mixCnst.Mm_s);
        a = getEquilibrium_a(T,y_s,p,mixCnst,options);                                  % Magin thesis Eq. 2.5
    otherwise
        error(['the chosen flag ''',flag,''' is not supported']);
end
rho = getMixture_rho(y_s,p,T,T,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron);      % Anderson Eq. 14.66

dy(1,1) = Vtheta;                                                                % Anderson Eq. 14.59
dy(2,1) = a^2/(Vtheta^2-a^2) * (2*Vr + Vtheta/tan(theta) - Vr*Vtheta^2/a^2);     % Anderson Eq. 14.64
dy(3,1) = -rho*Vtheta*a^2/(Vtheta^2-a^2) * (Vr+Vtheta/tan(theta));               % Anderson Eq. 14.65

end % diffEq_coneLTE
