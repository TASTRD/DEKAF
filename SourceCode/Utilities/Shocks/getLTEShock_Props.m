function [T2,p2,u2,y_s2,beta,bConv] = getLTEShock_Props(u1,T1,p1,X_E1,mixCnst,theta,options,varargin)
% getLTEShock_Props computes the post-shock conditions in LTE.
%
% Examples:
%   (1) [T2,p2,u2,y_s2,beta,bConv] = getLTEShock_Props(u1,T1,p1,X_E1,mixCnst,theta,options)
%
%   (2) [T2,p2,u2,y_s2,beta,bConv] = getLTEShock_Props(...,beta0)
%   |--> allows to make an initial guess for the shock angle
%
% Inputs and outputs:
%   u1          pre-shock streamwise velocity [m/s]
%   T1          pre-shock temperature [K]
%   p1          pre-shock static pressure [Pa]
%   X_E1        pre-shock elemental mole fractions [-]
%   mixCnst     structure with the mixture's properties
%   theta       wedge angle [deg]
%   options     options structure (NEEDED)
%   beta0       initial guess for the post-shock angle [deg]
%   u2          post-shock streamwise velocity [m/s]
%   T2          post-shock temperature [K]
%   p2          post-shock static pressure [Pa]
%   y_s2        post-shock mass fractions [-]
%   beta        shock angle [deg]
%   bConv       convergence boolean
%
% NOTE: the initial guesses for the rho ratio and beta do not provide
% converged solutions for small wedge angles - low velocities, nor for
% large angles.
%
% See also: getShock_Props
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

options.display = false; % silencing equilibrium NR messages
if isempty(varargin)
    beta0 = theta*1.2; % guess
else
    beta0 = varargin{1};
end

options.EoS = protectedvalue(options,'EoS','idealGas');

% extracting and transforming everything from mixCnst
Mm_s = mixCnst.Mm_s;
R0 = mixCnst.R0;
R_s = mixCnst.R_s;
nAtoms_s = mixCnst.nAtoms_s;
thetaVib_s = mixCnst.thetaVib_s;
thetaElec_s = mixCnst.thetaElec_s;
gDegen_s = mixCnst.gDegen_s;
bElectron = mixCnst.bElectron;
hForm_s = mixCnst.hForm_s;
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
             bElectron_mat,hForm_s_mat,~] = reshape_inputs4enthalpy(R_s,...
             nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,T1);

% compute preshock composition, density and enthalpy
X_s1 = getEquilibrium_X_complete(p1,T1,'mtimesx',options.mixture,...
                    mixCnst,options.modelEquilibrium,0,options,'variable_XE',X_E1);
y_s1 = getEquilibrium_ys(X_s1,Mm_s);
h1 = getMixture_h_cp(y_s1,T1,T1,T1,T1,T1,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,...
                    thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);

rho1 = getMixture_rho(y_s1,p1,T1,T1,R0,Mm_s,bElectron);
% Iterative process to obtain the shock angle
rho12 = 0.1;                                                % initial guess
beta = beta0;
conv = 1;                                                   % initializing loop quantities
it = 0;
it_max = options.shock_itMax;
tol = options.shock_tol;
while it<it_max && conv>tol
    it = it+1;
    rho12_prev = rho12;
    vn21 = rho12;                                               % eq. E
    beta = fzero(@(B)tand(B-theta)-vn21*tand(B),beta);          % eq. F
    vn1 = u1*sind(beta);                                        % eq. G
    p2 = p1+rho1*vn1^2*(1-rho12);                               % eq. C
    h2 = h1+vn1^2/2*(1-rho12^2);                                % eq. D
    [X_s2,T2] = getEquilibrium_X_complete(p2,h2,'mtimesxh',...  % eq. B inverted
                    options.mixture,mixCnst,options.modelEquilibrium,0,options,'variable_XE',X_E1);
    y_s2 = getEquilibrium_ys(X_s2,Mm_s);
    rho2 = getMixture_rho(y_s2,p2,T2,T2,R0,Mm_s,bElectron);     % eq. A
    rho12 = rho1/rho2;
    conv = abs((rho12-rho12_prev)/rho12);                       % convergence criterion (rho variation)
end
u2 = vn21*vn1/sind(beta-theta);                             % eq. G
if conv<tol                                                 % preparing convergence boolean
    bConv = true;
else
    bConv = false;
    warning(['LTE shock position did not converge - difference in the rho ratio of ',num2str(conv)]);
end
