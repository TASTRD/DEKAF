function [T2,p2,u2,y_s2,beta,bConv] = getHighEnth_ConicalShock_Props(flag,u1,T1,p1,y_s1,mixCnst,theta,options,varargin)
% getHighEnth_ConicalShock_Props computes the post-shock conditions after a
% conical shock in high-enthalpy conditions.
%
% Examples:
%   (1) [T2,p2,u2,y_s2,beta,bConv] = getHighEnth_ConicalShock_Props('TPG1T',u1,T1,p1,y_s1,mixCnst,theta,options)
%   |--> shock assuming TPG with one temperature (thermal equilibrium)
%
%   (2) [T2,p2,u2,y_s2,beta,bConv] = getHighEnth_ConicalShock_Props('LTE',u1,T1,p1,X_E1,mixCnst,theta,options)
%   |--> shock assuming LTE
%
%   (3) [T2,p2,u2,y_s2,beta,bConv] = getHighEnth_ConicalShock_Props(...,beta0)
%   |--> allows to make an initial guess for the shock angle
%
% Inputs and outputs:
%   u1          pre-shock streamwise velocity [m/s]
%   T1          pre-shock temperature [K]
%   p1          pre-shock static pressure [Pa]
%   mixCnst     structure with the mixture's properties
%   theta       cone angle [deg]
%   options     options structure (NEEDED)
%   beta0       initial guess for the post-shock angle [deg]
%   u2          post-shock streamwise velocity [m/s]
%   T2          post-shock temperature [K]
%   p2          post-shock static pressure [Pa]
%   y_s2        post-shock mass fractions [-]
%   beta        shock angle [deg]
%   bConv       convergence boolean
%
% NOTE: the initial guesses for the rho ratio and beta do not provide
% converged solutions for small cone angles - low velocities, nor for
% large angles.
%
% See also: getShock_Props, getLTEShock_Props
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

if isempty(varargin)
    beta0 = theta*1.2; % guess
else
    beta0 = varargin{1};
end
options = standardvalue(options,'display',true);
bDisplay = options.display;     % retaining display boolean to be used in the function
options.display = false;        % silencing equilibrium NR messages

options.EoS = protectedvalue(options,'EoS','idealGas');

% Displaying message
dispif(['Starting iteration to solve the inviscid flowfield after a conical shock in ',flag,' conditions...'],bDisplay);

% relaxation factor (convergence speed copied from TaylorMaccoll function by Dr. J. V. Lassaline)
% relax_factor = 0.44; % suggested by Lassaline for M>5
relax_factor = 0.88; % very agressive

% extracting and transforming everything from mixCnst
Mm_s = mixCnst.Mm_s;
R0 = mixCnst.R0;
R_s = mixCnst.R_s;
nAtoms_s = mixCnst.nAtoms_s;
thetaVib_s = mixCnst.thetaVib_s;
thetaElec_s = mixCnst.thetaElec_s;
gDegen_s = mixCnst.gDegen_s;
bElectron = mixCnst.bElectron;
hForm_s = mixCnst.hForm_s;
nAvogadro = mixCnst.nAvogadro;
hPlanck = mixCnst.hPlanck;
kBoltz = mixCnst.kBoltz;
L_s = mixCnst.L_s;
sigma_s = mixCnst.sigma_s;
thetaRot_s = mixCnst.thetaRot_s;

[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,...
    bElectron_mat,hForm_s_mat,~] = reshape_inputs4enthalpy(R_s,...
    nAtoms_s,thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,T1);

% compute preshock composition, density and enthalpy
if strcmp(flag,'LTE')
    X_E1 = y_s1;                                                            % the input actually corresponds to X_E1
    X_s1 = getEquilibrium_X_complete(p1,T1,'mtimesx',options.mixture,...
        mixCnst,options.modelEquilibrium,0,options,'variable_XE',X_E1);
    y_s1 = getEquilibrium_ys(X_s1,Mm_s);
else
    X_E1 = NaN; % placeholder
end
h1 = getMixture_h_cp(y_s1,T1,T1,T1,T1,T1,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,...
    thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);

rho1 = getMixture_rho(y_s1,p1,T1,T1,R0,Mm_s,bElectron);

% converting shock angles to radians
theta = theta*pi/180;
beta  = beta0*pi/180;

% Iterative process to obtain the oblique shock angle
it_max = options.shock_itMax;    itTheta_max = options.shock_itMax;   % defining convergence criteria
tol = options.shock_tol;         tolTheta = options.shock_tol;
itTheta = 0;                                                % initializing loop quantities
dtheta(1) = 1;
while abs(dtheta(end))>=tolTheta && itTheta<itTheta_max
    itTheta = itTheta+1; % increasing theta counter

    % Obtaining oblique-shock relations for a given beta
    vn1 = u1*sin(beta);                                        % eq. G
    vn21 = tan(beta-theta) / tan(beta);                         % initial value of the velocity ratio (guess the post-shock direction is theta)
    rho12 = vn21;                                               % ... which is equal to the density ratio
%     rho12 = 0.1;
    conv = 1;                                                   % initializing loop quantities
    it = 0;
    while it<it_max && conv>tol
        it = it+1;
        p2 = p1+rho1*vn1^2*(1-rho12);                               % eq. C
        h2 = h1+vn1^2/2*(1-rho12^2);                                % eq. D
        switch flag
            case 'TPG1T'
                y_s2 = y_s1;
                T2 = get_NRTfromh(h2,y_s1,T1,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,...
                thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options,mixCnst.AThermFuncs_s);
            case 'LTE'
                [X_s2,T2] = getEquilibrium_X_complete(p2,h2,'mtimesxh',...  % eq. B inverted
                    options.mixture,mixCnst,options.modelEquilibrium,0,options,'variable_XE',X_E1);
                y_s2 = getEquilibrium_ys(X_s2,Mm_s);
            otherwise
                error(['the chosen flag ''',flag,''' is not supported']);
        end
        rho2 = getMixture_rho(y_s2,p2,T2,T2,R0,Mm_s,bElectron);     % eq. A
        rho12_prev = rho12;                                         % storing previous value
        rho12 = rho1/rho2;                                          % updating density ratio
        conv = abs(1-rho12/rho12_prev);                             % convergence criterion
    end
    vn21 = rho12;                                               % converged velocity ratio
    delta = beta-atan(vn21*tan(beta));                          % deflection right after the shock
    u2 = vn21*vn1/sin(beta-delta);                              % eq. G
    s2 = getMixture_s(T2,T2,T2,T2,T2,rho2,y_s2,R_s,nAtoms_s,... % post-shock entropy (constant)
        thetaVib_s,thetaElec_s,gDegen_s,bElectron,hForm_s,Mm_s,...
        nAvogadro,hPlanck,kBoltz,L_s,sigma_s,thetaRot_s,options);

    mixCnst.T0 = T2;
    mixCnst.y_s1 = y_s1; % used for the TPGT1 case

    % Set up ODE solver
    % Quit integration when vtheta=0
    % See: coneEvent.m
    % Set relative error tolerance small enough to handle low M
    odeOpts=odeset('Events',@coneEvent,'RelTol',1e-10);

    % Solve by marching solution away from shock until either 0 degrees or flow
    % flow tangency reached as indicated by y(2)==0.
    % See cone.m
    Vr0         =  u2*cos(beta-delta); % velocities inmediately after the shock
    Vtheta0     = -u2*sin(beta-delta);
    y0          = [Vr0,Vtheta0,p2];
    [sol]=ode45(@(theta,y)diffEq_coneLTE(theta,y,s2,X_E1,mixCnst,options,flag),[beta 1e-10],y0,odeOpts);
%     [sol]=ode15s(@(theta,y)diffEq_coneLTE(theta,y,s2,mixCnst,options,flag),[beta 1e-10],y0,odeOpts);

    % obtain cone theta
    thetaN = sol.xe;

    % correct beta based on thetaN
    dtheta(itTheta) = thetaN - theta;
%     disp(['Iteration ',num2str(itTheta),' lead to dtheta=',num2str(dtheta(itTheta)),'. Goal: ',num2str(tolTheta)]);
    dispif(['Iteration ',num2str(itTheta),' lead to dtheta=',num2str(dtheta(itTheta)*180/pi),' [deg], beta=',num2str(beta*180/pi),' [deg]. Goal: ',num2str(tolTheta*180/pi)],bDisplay);
    beta = beta - relax_factor*dtheta(itTheta);
end

% Computing properties at the wall
switch flag
    case 'TPG1T'
        y_s2 = mixCnst.y_s1; % constant and passed through mixCnst
        T2 = get_NRTfroms(s2,y_s2,p2,mixCnst.T0,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,...
            mixCnst.gDegen_s,mixCnst.hForm_s,mixCnst.nAvogadro,mixCnst.hPlanck,mixCnst.kBoltz,mixCnst.L_s,mixCnst.sigma_s,mixCnst.thetaRot_s,options);
    case 'LTE'
        [X_s2,T2] = getEquilibrium_X_complete(p2,s2,'mtimesxs',options.mixture,mixCnst,options.modelEquilibrium,0,options,'variable_XE',X_E1);
        y_s2 = getEquilibrium_ys(X_s2,mixCnst.Mm_s);
    otherwise
        error(['the chosen flag ''',flag,''' is not supported']);
end
u2 = sol.ye(1);                                 % radial velocity
p2 = sol.ye(3);                                 % pressure
beta = beta*180/pi;

if dtheta(end)<tol                                                 % preparing convergence boolean
    bConv = true;
else
    bConv = false;
    warning([flag,' conical shock position did not converge - difference in the wall angle of ',num2str(conv)]);
end