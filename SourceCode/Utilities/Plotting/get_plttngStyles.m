function [marks,lines,linewidths,styles] = get_plttngStyles(varargin)
% get_plttngStyles returns a list with the 44 unique possibilities for
% markers, line styles, and line widths.
%
% Usage:
%   (1)
%       [marks,lines,linewidths] = get_plttngStyles()
%
%   (2)
%       [marks,lines,linewidths] = get_plttngStyles(N)
%       |--> allows to pass the minimum number of required line styles
%       (default 44)
%
%   (3)
%       [marks,lines,linewidths,styles] = get_plttngStyles(...)
%       |--> returns also the "styles" - combination of marker + line
%
% Author: Fernando Miro Miro
% Date: February 2019

if isempty(varargin)
    N = 44;
else
    N = varargin{1};
end

Ncp = ceil(N/44);

marks       = {'o','x','s','v','p','^','h','d','*','>','+'};
lines       = {'-','-.','--',':'};
linewidths  = [1,1,1,1.5];
Nmarks      = length(marks);
Nlines      = length(lines);
marks       = repmat(marks,     [1,Ncp*Nlines]);
lines       = repmat(lines,     [1,Ncp*Nmarks]);
linewidths  = repmat(linewidths,[1,Ncp*Nmarks]);

styles = cellfun(@(cll1,cll2)[cll1,cll2],marks,lines,'UniformOutput',false);

end % get_plttngStyles