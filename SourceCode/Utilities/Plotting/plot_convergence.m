function [] = plot_convergence(intel,options)
% PLOT_CONVERGENCE(intel,options) plots the convergence of those variables
% or equations that have been tracked throughout the domain. It requires
% options to provide:
%        .Fields4conv
%        .NindFields4conv
%        .FancyFields4conv
%        .plot_options
%
% The defaults of all these options are set in setDefaults.
%
% intel should have a structure norms, with the convergence of the
% different tracked variables.
%
% DEVELOPER's NOTE: there is a block at the bottom that we still don't know
% what it is for. Eventually this could be eliminated and modify the inputs
% such that it only receives norms.
%
% see also: setDefaults, DEKAF_QSS, DEKAF_BL
%
% Author(s):    Koen Groot
% GNU Lesser General Public License 3.0

% extracting norms
norms = intel.norms;

% defining styles and lines
styleS = {'o','s','^','*','p','v','h','<','+','>','d'};
styleS = [styleS,styleS,styleS,styleS];
LineS = {'-','-.','--',':'};
LineS = [LineS,LineS,LineS,LineS,LineS,LineS,LineS,LineS,LineS];


% extracting variables
Fields4conv = options.Fields4conv; % fields on which the convergence was analyzed
NindFields4conv = options.NindFields4conv; % number of indices each variable has (like ys{1}, ys{2} etc.)
FancyFields4conv = options.FancyFields4conv; % fancy names for the convergence fields
NFields4conv = length(Fields4conv);
NtotFields = sum(NindFields4conv);

% plotting
clear plots legends
figure
ylabel('Errors');
xlabel('Iterations');
set(gca,'yscale','log')
for ii=1:NFields4conv
    posField = sum(NindFields4conv(1:(ii-1))); % where the current field is placed inside all_normDiff
    if NindFields4conv(ii)==1 % only one index (like f or momX)
        rainbowVec = color_rainbow(posField+1,NtotFields);
        plots(posField+1) = semilogy(norms.(Fields4conv{ii}),[styleS{posField+1},LineS{posField+1}],'Color',rainbowVec,options.plot_options{:});
        hold on
        legends{posField+1} = FancyFields4conv{ii};
    else % we loop the different indices of the variable
        for s=1:NindFields4conv(ii)
            rainbowVec = color_rainbow(posField+s,NtotFields);
            plots(posField+s) = semilogy(norms.(Fields4conv{ii}){s},[styleS{posField+s},LineS{posField+s}],'Color',rainbowVec,options.plot_options{:});
            hold on
            legends{posField+s} = strrep(FancyFields4conv{ii},'#s',num2str(s));
        end
    end
end
plots(end+1) = fill([0 intel.it intel.it 0],eps*[1 1 1/10^5 1/10^5],[1 1 1]*0.5,'facealpha',0.5,'linestyle','none');
legends{end+1} = '$$\epsilon$$';
legend(plots,legends,'interpreter','latex');
ylim([1e-17 inf])
grid on;


% THIS HAS BEEN LEFT MOMENTANEOUSLY
if isfield(intel,'xi') && length(intel.xi)>1
    figure
    mesh(intel.xi,intel.eta,intel.df_deta)
    hold on
    xlabel('\xi [-]');
    ylabel('\eta [-]');

    ylim([eps/10^5 inf])

    figure
    mesh(intel.xi(2:end),intel.eta,log10(abs(diff(intel.df_deta'))'));
    mesh(intel.xi(2:end),intel.eta,log10(abs(diff(intel.g'))'))
    mesh(intel.xi(2:end),intel.eta,log10(abs(diff(intel.C'))'))
    mesh(intel.xi(2:end),intel.eta,log10(abs(diff(intel.df_deta3'))'))
    mesh(intel.xi(2:end),intel.eta,log10(abs(diff(intel.df_deta2'))'))
    mesh(intel.xi(2:end),intel.eta,log10(abs(diff(intel.f'))'))
    hold on
    xlabel('\xi [-]');
    ylabel('\eta [-]');

    ylim([eps/10^5 inf])
end