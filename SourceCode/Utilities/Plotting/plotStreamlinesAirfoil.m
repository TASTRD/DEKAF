function [plots] = plotStreamlinesAirfoil(xc_geom,yc_geom,x_SL,y_SL,z_SL,varargin)
% plotStreamlinesAirfoil plots a series of inviscid streamlines on an
% airfoil geometry.
%
% Usage:
%   (1)
%       [h] = plotStreamlinesAirfoil(xc_geom,yc_geom,x_SL,y_SL,z_SL)
%
%   (2)
%       [h] = plotStreamlinesAirfoil(xc_geom,yc_geom,x_SL,y_SL,z_SL,legends)
%       |--> allows to pass a legends cell with the labels to be used for
%       each SL.
%
%   (3)
%       [...] = plotStreamlinesAirfoil(...,'topAndBottom')
%       |--> expects the *_SL inputs to be an NL x 2 cell with each pair of
%       top and bottom streamlines
%
%   (4)
%       [...] = plotStreamlinesAirfoil(...,'plotFlag',plotFlag)
%       |--> allows to customize the plot flag to be used by the two_colors
%       function. Otherwise fixed to 'Rbw2'
%
%   (5)
%       [...] = plotStreamlinesAirfoil(...,'zLim',zLim)
%       |--> allows to customize the maximum spanwise stretch to be used in
%       the plotting
%
% Inputs and outputs:
%   xc_geom     [m]
%       airfoil LE-normal geometry in the chord-LE coordinate system
%   yc_geom     [m]
%       airfoil chord-normal geometry in the chord-LE coordinate system
%   x_SL        [m]
%       cell with the LE-normal positions of each inviscid streamline in
%       the chord-LE coordinate system
%   y_SL        [m]
%       cell with the chord-normal positions of each inviscid streamline in
%       the chord-LE coordinate system
%   z_SL        [m]
%       cell with the LE-parallel positions of each inviscid streamline in
%       the chord-LE coordinate systems
%   h
%       plot handles to each curve. If the 'topAndBottom' flag is passed,
%       then only the handles of the top SLs are returned.
%
% Author: Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

[varargin,bBothSides] = find_flagAndRemove('topAndBottom',varargin);       % checking topAndBottom flag
[varargin,~,idx]      = find_flagAndRemove('plotFlag',    varargin);       % checking plotFlag flag
if isempty(idx);        pltFlag = 'Rbw2';
else;                   pltFlag = varargin{idx};    varargin(idx) = [];
end
[varargin,~,idx]      = find_flagAndRemove('zLim',        varargin);       % checking zLim flag
if isempty(idx);        zLim = max(cellfun(@(cll)max(cll(:)),z_SL(:)));
else;                   zLim = varargin{idx};    varargin(idx) = [];
end
if isempty(varargin);   legends = {};                                      % final entry - legend
else;                   legends = varargin{1};
end

if ~bBothSides
    x_SL = x_SL(:);    y_SL = y_SL(:);    z_SL = z_SL(:);
end
NL = size(x_SL,1);                                                          % number of rows (streamlines)
[~,i_xGeomMin]  = min(xc_geom);                                             % minimum value of x_geom, dividing the geometric top and bottom of the airfoil
NZ = 50;                                                                    % HARDCODE ALERT! number of spanwise stations
xgeom3D   = xc_geom(1:i_xGeomMin).';                                        % vector of chord-wise coordinates
zgeom3D   = linspace(-0.1*zLim,zLim,NZ).';                                  % vector of span-wise coordinates
ygeom3D_1 = repmat(interp1(xc_geom(1:i_xGeomMin),  yc_geom(1:i_xGeomMin),  xgeom3D),[NZ,1]);    % bottom surface
ygeom3D_2 = repmat(interp1(xc_geom(i_xGeomMin:end),yc_geom(i_xGeomMin:end),xgeom3D),[NZ,1]);    % top surface
C3D = 0.4*repmat(ones(size(ygeom3D_1)),[1,1,3]);                            % color map (gray)

% plotting
figure;
surf(xgeom3D,zgeom3D,ygeom3D_1,C3D); hold on;                               % plotting geometry
surf(xgeom3D,zgeom3D,ygeom3D_2,C3D);
for iL=1:NL                                                                 % looping streamlines
    plots(iL) = plot3(x_SL{iL,1},z_SL{iL,1},y_SL{iL,1},'-', 'Color',two_colors(iL,NL,pltFlag),'linewidth',2); % first side
    if bBothSides
                plot3(x_SL{iL,2},z_SL{iL,2},y_SL{iL,2},'--','Color',two_colors(iL,NL,pltFlag),'linewidth',2); % second side
    end
end
daspect([1 1 1]);
view([-53 8]) % alter default view to examine the leading edge
xlabel('x [m]'); ylabel('z [m]'); zlabel('y [m]');
if ~isempty(legends)
    legend(plots,legends);
end

end % plotStreamlinesAirfoil