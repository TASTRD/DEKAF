function vec = two_colors(i,N,flag,varargin)
% two_colors returns the RBG vector identifying a position i in a
% vector of length N for the color scheme fixed by flag.
%
% Usage:
%   (1)
%   vec = two_colors(i,N,flag);
%      |--> standard usage to return the ith rgb triplet in a color scheme
%
%   (2)
%   vec = two_colors(...,'one_color',[r g b]);
%      |--> if N == 1, specify the fallback rgb triplet so this function
%      doesn't break. default: [0 0.2 0.8].
%
% The options for flag are:
%   - 'KR'      black-red
%   - 'GB'      green-blue
%   - 'YM'      yellow-magenta
%   - 'Rbw'     rainbow
%   - 'Rbw2'    dark rainbow
%   - 'KRbw1', 'KRbw2', ... 'KRbw11'    black-rainbow 1, 2, ..., 11.
%       NOTE: it passes from a fully black, to each of the 11 RBG rainbow colors.
%   - 'vrds'    viridis
%   - 'mgm'     magma
%   - 'vrdsInv' viridis inverted
%   - 'mgmInv'  magma inverted
%
% See also: color_rainbow, viridis, magma
%
% Author: Fernando Miro Miro
% Date: January 2018

[one_color, ~] = parse_optional_input(varargin,'one_color',[0 0.2 0.8]);

% Many of these functions break if N == 1
% If so, we choose a fallback color specified by one_color's varargin
if N == 1
    vec = one_color;
    return
end

switch flag
    case 'KR'
        vec = [(i-1)/(N-1), 0, 0];
    case makeList('KRbw',1,11)
        RGB_vecR = [1 , 1 , 1 ,0.5, 0 , 0 , 0 , 0 , 0 ,0.5, 1 ];
        RGB_vecG = [0 ,0.5, 1 , 1 , 1 , 1 , 1 ,0.5, 0 , 0 , 0 ];
        RGB_vecB = [0 , 0 , 0 , 0 , 0 ,0.5, 1 , 1 , 1 , 1 , 1 ];
        jj = str2double(flag(5:end));
        vec = [RGB_vecR(jj) , RGB_vecG(jj) , RGB_vecB(jj)] * (i-1)/(N-1);
    case 'GB'
        vec = [0, (i-1)/(N-1), 1-(i-1)/(N-1)];
    case 'YM'
        vec = 0.8 * [1, 1-(i-1)/(N-1), (i-1)/(N-1)]; % we make this one a bit darker (*0.8)
    case 'Rbw'
        vec = color_rainbow(i,N);
    case 'Rbw2'
        vec = 0.8*color_rainbow(i,N);
    case 'vrds'
        vec = viridis(i,N);
    case 'mgm'
        vec = magma(i,N);
    case 'vrdsInv'
        vec = viridis(N-i+1,N);
    case 'mgmInv'
        vec = magma(N-i+1,N);
    otherwise
        error(['non-identified flag ''',flag,'''']);
end
