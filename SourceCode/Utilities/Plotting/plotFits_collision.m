function [] = plotFits_collision(mixCnst,col_ID,varargin)
% plotFits_collision prepares fancy plots with the fitting done on the
% collision integrals.
%
% Usage:
%   (1)
%   plotFits_collision(mixCnst,'Omega11')
%      |--> plots the fits for the (1,1) collision
%
%   (2)
%   plotFits_collision(mixCnst,'Omega11',specs_1,specs_2)
%      |--> allows to pass two lists of species to output only the
%      collisions between them
%
%   (3)
%   plotFits_collision(...,'maximumSubplots',N)
%      |--> allos to fix the maximum number of subplots to be displayed
%
%   (4)
%   dispLatexTable_collision(...,'customNames',names)
%      |--> allows to pass a cell of strings containing customized names
%      for the different pairs. The names cell must have as many positions
%      as unique pairs can be made out of specs_1 and specs_2
%
%   (5)
%   dispLatexTable_collision(...,'savepath',savepath)
%      |--> allows to pass the name of a folder where the figures will be
%      saved.
%
%   (6)
%   dispLatexTable_collision(...,'TRange',TRange)
%      |--> allows to pass the temperature range (in K) to be plotted
%      (default [200,30000])
%
%   (7)
%   dispLatexTable_collision(...,'TStarRange',TStarRange)
%      |--> allows to pass the reduced temperature range (in K) to be
%      plotted (default [200,30000])
%
%   (8)
%   dispLatexTable_collision(...,'plotSize_x',plotSize_x)
%      |--> allows to choose the unitary horizontal plot size in pixels.
%      The final plot will have width plotSize_x*Nj*2/3, where Nj is the
%      number of columns in the subplot. Default: 400
%
%   (9)
%   dispLatexTable_collision(...,'plotSize_y',plotSize_y)
%      |--> allows to choose the unitary vertical plot size in pixels.
%      The final plot will have width plotSize_y*Ni*2/3, where Ni is the
%      number of rows in the subplot. Default: 300
%
%   (10)
%   dispLatexTable_collision(...,'FSAxis',FSAxis)
%      |--> allows to choose the font-size of the axis (in pt). Default: 12
%
%   (11)
%   dispLatexTable_collision(...,'mark_size',mark_size)
%      |--> allows to choose size of the markers. Default: 8
%
%   (12)
%   dispLatexTable_collision(...,'meters')
%      |--> plots the collision integrals in meters, rather than Armstrongs
%      (default)
%
% See also: get_specNameLatex
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% checking for the 'customNames' flag
[varargin,bCustomNames,idx] = find_flagAndRemove('customNames',varargin);
if ~isempty(idx)
    customNames = varargin{idx};
    varargin(idx) = [];
end

% checking other flags
[NsbpltMax, varargin] = parse_optional_input(varargin,'maximumSubplots',18);
[savepath,  varargin] = parse_optional_input(varargin,'savepath','./');
[TStarRange,varargin] = parse_optional_input(varargin,'TStarRange',[0.1,1e11]);
[TRange,    varargin] = parse_optional_input(varargin,'TRange',[200,30000]);
[plotSize_x,varargin] = parse_optional_input(varargin,'plotSize_x',400);
[plotSize_y,varargin] = parse_optional_input(varargin,'plotSize_y',300);
[FSAxis,    varargin] = parse_optional_input(varargin,'FSAxis',12);
[mark_size, varargin] = parse_optional_input(varargin,'mark_size',8);
[varargin,  bMeters]  = find_flagAndRemove('meters',varargin);

% checking for species lists
spec_list = mixCnst.spec_list;                                          % extracting species list from mixCnst
switch length(varargin)
    case 0
        specs_1 = spec_list;
        specs_2 = spec_list;
    otherwise
        specs_1 = varargin{1};
        specs_2 = varargin{2};
end

%%%% Collecting the necessary data
[bPos,bNeg] = getSpecies_bPosNeg(spec_list);                % positive/negatively charged booleans
bCharged    = getPair_bCharged(bPos,bNeg);                  % charged/not charged booleans
consts = mixCnst.(['consts_',col_ID]);                      % extracting collision constants
if strcmp(col_ID(1:5),'Omega')                                    % renaming omegas
    col_name    = regexprep(col_ID,'Omega([0-9])([0-9])','\\Omega\^{($1,$2)}_{s\\ell}');
    nameSimple  = regexprep(col_ID,'Omega([0-9])([0-9])','O$1$2');
elseif strcmp(col_ID(2:5),'star')                                 % renaming Astar and company
    col_name    = regexprep(col_ID,'([a-z]|[A-Z])star','$1\^{\*}_{s\\ell}');
    nameSimple  = col_ID;
end

passed_pairs = {};      it=0;
for s=1:length(specs_1)                                                 % looping species
    spec_s = get_specNameLatex(specs_1{s},'noText');                        % name of species s
    ss = find(ismember(spec_list,specs_1{s}));                              % index of species s in original spec_list
    for l=1:length(specs_2)                                                 % looping species
        spec_l = get_specNameLatex(specs_2{l},'noText');                        % name of species l
        ll = find(ismember(spec_list,specs_2{l}));                              % index of species l in original spec_list
        pair_ID = num2str(sort([ss,ll]));                                       % obtaining string identifying the ss-ll pair
        if ~ismember(pair_ID,passed_pairs) && ...                               % checking if the pair has already been printed
                size(mixCnst.stats_fits.raw_data.T,1)>=ss && size(mixCnst.stats_fits.raw_data.T,2)>=ll && ...
                ~isempty(mixCnst.stats_fits.raw_data.T{ss,ll})                  % ... and making sure that it comes from tabled values
            it=it+1; % increase counter
            passed_pairs = [passed_pairs;pair_ID];                                  % adding it to the list of checked pairs
            if bCustomNames;    str_pairs{it} = customNames{it};                    % if the user asked for customized names
            else;               str_pairs{it} = ['$$',spec_s,'-',spec_l,'$$'];        % we take the name string of the s-l pair
            end
            bChargedVec(it) = bCharged(ss,ll);                                      % storing whether the collisions are charged or not
            % preparing vectors to plot
            if bChargedVec(it);     TLine(:,it) = logspace(log10(TStarRange(1)),log10(TStarRange(2)),1001); % plotting with the reduced temperature
            else;                   TLine(:,it) = logspace(log10(TRange(1))    ,log10(TRange(2))    ,1001); % plotting with the actual temperature
            end
            lnT = log(TLine(:,it));
            colLine(:,it) = exp(consts.A(ss,ll) + consts.B(ss,ll).*lnT + consts.C(ss,ll).*lnT.^2 + consts.D(ss,ll).*lnT.^3 + consts.E(ss,ll).*lnT.^4 + consts.F(ss,ll).*lnT.^5);

            if ismember(col_ID,{'Omega14','Omega15','Omega24'});    Tvec{it} = mixCnst.stats_fits.raw_data.T2{ss,ll};
            else;                                                   Tvec{it} = mixCnst.stats_fits.raw_data.T{ss,ll};
            end
            colData{it} = mixCnst.stats_fits.raw_data.(nameSimple){ss,ll};
            if isfield(mixCnst.stats_fits.raw_data,'Textrap') && ~isempty(mixCnst.stats_fits.raw_data.Textrap{ss,ll})
                bExtrap(it) = true;                                                     % there was indeed extrapolation
                if ismember(col_ID,{'Omega14','Omega15','Omega24'});    Textrap{it} = mixCnst.stats_fits.raw_data.Textrap2{ss,ll}; % extrapolated temperatures
                else;                                                   Textrap{it} = mixCnst.stats_fits.raw_data.Textrap{ss,ll};  % extrapolated temperatures
                end
                idxExtrap = [];
                for jj=length(Textrap{it}):-1:1
                    [~,idxExtrap(jj)] = min(abs(Tvec{it}-Textrap{it}(jj)));             % locating extrapolated temperatures in vector
                end
                colExtrap{it} = colData{it}(idxExtrap);                                 % extracting collisions corresponding to these extrapolations
                colData{it}(idxExtrap) = [];                                            % removing collisions not corresponding to extrapolations
                Tvec{it}(idxExtrap) = [];                                               % non-extrapolated temperatures
            else
                bExtrap(it) = false;
            end

            % Unit change to Armstrongs
            if ~bCharged(it) && ~ismember(col_ID,{'Bstar','Cstar','Estar'}) && ~bMeters
                colLine(:,it) = 1e20 * colLine(:,it);
                colData{it}   = 1e20 * colData{it};
                if bExtrap(it)
                    colExtrap{it} = 1e20 * colExtrap{it};
                end
            end
        end
    end
end

%%% Preparing plotting parameters
NsbpltTot = it;                                                             % total number of subplots
sbplts = divideAndRemainder(NsbpltTot,NsbpltMax);                           % intervals of subplots going into each plot
Nplts = length(sbplts);                                                     % number of plots
abcd = alphabet;                                                            % letters of the alphabet (to be used for the subplot identifier)
it=1;                                                                       % initializing counter
for ipt = 1:Nplts                                                           % looping plots
    idx2plt = it:sbplts(ipt);                                                   % indices of the subplots going into this plot
    it = sbplts(ipt)+1;                                                         % updating counter for next loop count
    Nsbplts = length(idx2plt);                                                  % number of subplots in this plot
    if Nsbplts<3;       Ni=1;               Nj=Nsbplts;                         % one unique subplot
    else;               Ni=ceil(Nsbplts/3); Nj=3;                               % multiple subplots arranged in two columns
    end
    bOccupied = false(Nj,Ni);                                                   % boolean keeping track of the subplots that are occupied
    bOccupied(1:Nsbplts) = true;                                                % populating
    idxOccupied = find(bOccupied(:));                                           % numerical values
    if Ni==1                                                                    % single row
        bXLabel = true(Nj,1);                                                       % all plots with x label
    else                                                                        % multiple rows
        bXLabel = false(Nj,Ni);                                                     % boolean keeping track of which subplots need the xlabel
        bXLabel(:,end) = true;                                                      % last row will have x labels
        bXLabel(~bOccupied(:,end),end-1) = true;                                    % the second-last row, whenever the last is not occupied will also have x labels
        bXLabel(~bOccupied) = false;                                                % those that are not occupied don't have x labels
    end

    % preparing labels
    if any(bCharged(idx2plt)~=bCharged(ipt))                                    % if there is a mix of charged-charged and other collisions, break
        error('the list of collisions has a mix of charged-charged and other collisions. This implies the use of different plotting variables and ranges, so a unified fancy plot is not possible.');
    elseif bCharged(ipt)                                                        % charged collisions
        xLabel = '$$T^*_{s\ell}$ [--]';
        if ismember(col_ID,{'Bstar','Cstar','Estar'});  yLabel = ['$$',col_name,'$$ [--]'];
        else;                                           yLabel = ['$$\big(T_{sl}^*/\lambda_D\big)^2 \, ',col_name,'$$ [--]'];
        end
    else                                                                        % non-charged collisions
        xLabel = '$$T^\mathrm{col}_{s\ell}$ [K]';
        if ismember(col_ID,{'Bstar','Cstar','Estar'});  yLabel = ['$$',col_name,'$$ [--]'];
        elseif bMeters;                                 yLabel = ['$$',col_name,'$$ [m\textsuperscript{2}]'];
        else;                                           yLabel = ['$$',col_name,'$$ [\AA\textsuperscript{2}]'];
        end
    end

    fig_ID = figure;                                                            % opening figure
    set(gcf,'units','points','position',[0,0,Nj*2/3*plotSize_x,Ni*2/3*plotSize_y])
    h_01=tightSubplot(Ni,Nj,[.2/Ni .08/Nj],[.2 .15]/Ni,[.2 .08]/Nj);           % gap (h/w), marg_h, marg_w

    for isb = 1:Nsbplts                                                         % looping subplots
        icol = (ipt-1)*NsbpltMax + isb;                                             % collision identifier
        [columnID,rowID] = ind2sub([Nj,Ni],isb);                                    % obtaining column and row identification in the subplots
        idx_column = columnID:Nj:(Ni-1)*Nj+columnID;                                % indices of all other collisions in the same column
        idx_row = (rowID-1)*Nj+1 : rowID*Nj;                                        % indices of all other collisions in the same row
        idx_column  = idx_column(ismember(idx_column,idxOccupied));                 % removing the values of empty positions
        idx_row     = idx_row(   ismember(idx_row   ,idxOccupied));
        % plotting limits
        ymax = exp(log(max(max(colLine(:,idx2plt(idx_row)))))+0.2);
        ymin = exp(log(min(min(colLine(:,idx2plt(idx_row)))))-0.2);
        xmax = max(max(TLine(:,idx2plt(idx_column))));
        xmin = min(min(TLine(:,idx2plt(idx_column))));

        axes(h_01(isb));                                                            % choosing subplot
        clear plots legends
        ic=1;                                                                       % initializing index counter
        plots(ic) = loglog(TLine(:,icol),colLine(:,icol),'k-'); hold on;            % fit line plot
        legends{ic} = 'polynomial fitting'; ic=ic+1;
        plots(ic) = loglog(Tvec{icol},colData{icol},'bx','markersize',mark_size);   % original data scattered plot
        legends{ic} = 'original data'; ic=ic+1;
        if bExtrap(icol)                                                            % if there is extrapolation
            plots(ic) = loglog(Textrap{icol},colExtrap{icol},'ro','markersize',mark_size); % extrapolated points
            legends{ic} = 'extrapolated data'; ic=ic+1;
        end
        if bXLabel(isb);        xlabel(xLabel,'interpreter','latex','FontSize',FSAxis); % x axis label
        else;                   set(h_01(isb),'XTickLabel','');                         % no x axis
        end
        if columnID==1;         ylabel(yLabel,'interpreter','latex','FontSize',FSAxis); % y axis label
        else;                   set(h_01(isb),'YTickLabel','');                         % no y axis
        end
        if NsbpltTot~=1                                                             % subplot identifier
            text(0.35,1.1,[abcd{icol},') ',str_pairs{icol}],'Units','normalized','interpreter','latex','Fontsize',FSAxis)
        end
        ylim([ymin,ymax]); xlim([xmin,xmax]);                                       % seetting plotting limits
        box on;
        set(gca,'FontSize',FSAxis)                                                  % setting font size
    end
    lgnd1 = legend(plots,legends,'interpreter','latex','FontSize',FSAxis,'location','best'); % legend (for last subplot only)
    set(lgnd1,'box','off');
    delete(h_01(~bOccupied));                                                       % removing empty subplots

    if Nplts==1;    pltStr = '';                                                % subplot identifying string
    else;           pltStr = ['_plot',num2str(ipt)];
    end
    filenameeps = [savepath,'fitting_',col_ID,'_',strjoin(spec_list,'_'),pltStr,'.eps']; % savename
    set(gcf,'name', filenameeps,'numbertitle','off')                            % setting name
    hgexport(fig_ID,filenameeps);                                               % saving
end


end % plotFits_collision