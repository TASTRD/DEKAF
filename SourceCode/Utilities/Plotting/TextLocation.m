function hOut = TextLocation(textString,varargin)
% TextLocation allows instantiation of an annotation object
% abiding by the rules of legend's 'location' attribute.
% Particularly useful is the 'best' value for 'location' instead of
% specifying the coordinates for the annotation's position/dimension.
%
% Acquired from the following link, 2020-03-15
% https://www.mathworks.com/matlabcentral/answers/98082-how-can-i-automatically-specify-a-best-location-property-for-the-textbox-annotation-function
%
% Please note that the use of this function will delete any
% legend currently in the figure window. If you wish to use a legend in
% your figure, please specify it after calling the 'TextLocation' function.
%
% Example:
%   htb = TextLocation('$\Lambda = 30^\circ$','interpreter','latex','edgecolor','none');
%   set(htb, 'fontsize', 12);

l = legend(textString,varargin{:});
t = annotation('textbox',varargin{:});
t.String = textString;
t.Position = l.Position;
delete(l);
t.LineStyle = 'None';

if nargout
    hOut = t;
end

end % TextLocation
