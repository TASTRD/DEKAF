function [N_i,N_j] = optimalPltSize(N)
% [N_i,N_j] = optimalPltSize(N) returns the optimal number of subplots in
% each direction (i and j) for a given number N of required plots.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if N==1
    N_i = 1; N_j = 1;
elseif N==2
    N_i = 1; N_j = 2;
elseif N<=4
    N_i = 2; N_j = 2;
elseif N<=6
    N_i = 2; N_j = 3;
elseif N<=8
    N_i = 2; N_j = 4;
elseif N<=9
    N_i = 3; N_j = 3;
elseif N<=12
    N_i = 3; N_j = 4;
elseif N<=16
    N_i = 4; N_j = 4;
elseif N<=20
    N_i = 4; N_j = 5;
elseif N<=24
    N_i = 4; N_j = 6;
elseif N<=30
    N_i = 5; N_j = 6;
elseif N<=35
    N_i = 5; N_j = 7;
elseif N<=42
    N_i = 6; N_j = 7;
elseif N<=48
    N_i = 6; N_j = 8;
elseif N<=54
    N_i = 6; N_j = 9;
elseif N<=63
    N_i = 7; N_j = 9;
elseif N<=70
    N_i = 7; N_j = 10;
elseif N<=80
    N_i = 8; N_j = 10;
elseif N<=90
    N_i = 9; N_j = 10;
elseif N<=100
    N_i = 10; N_j = 10;
elseif N<=110
    N_i = 10; N_j = 11;
elseif N<=120
    N_i = 10; N_j = 12;
elseif N<=130
    N_i = 10; N_j = 13;
elseif N<=143
    N_i = 11; N_j = 13;
elseif N<=154
    N_i = 11; N_j = 14;
elseif N<=165
    N_i = 12; N_j = 14;
elseif N<=180
    N_i = 12; N_j = 15;
elseif N<=192
    N_i = 12; N_j = 16;
elseif N<=204
    N_i = 12; N_j = 17;
elseif N<=221
    N_i = 13; N_j = 17;
elseif N<=234
    N_i = 13; N_j = 18;
elseif N<=247
    N_i = 13; N_j = 19;
else
    error(['The number of points -',num2str(N),' is too large']);
end