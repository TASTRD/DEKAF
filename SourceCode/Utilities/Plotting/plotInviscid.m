function plotInviscid(intel,options)
% plotInviscid plots the results coming out of the inviscid solver
%
% Examples:
%   (1) plotInviscid(intel)
%
% Author: Fernando Miro Miro
% Date: April 2018
%
% GNU Lesser General Public License 3.0

[eqs,eq_names] = eval_residualsEuler(intel,options);
Neqs = length(eqs);

mixCnst = intel.mixCnst; % extracting mixture constants
StyleS = {'-','-.','--',':'}; StyleS = repmat(StyleS,[1,Neqs]); % we repmat to make sure that we have enough
lineWidths = [1,1,1,1.5];  lineWidths = repmat(lineWidths,[1,Neqs]);
ip = 1;
figure
subplot(3,1,1);
Nplts = 5; % U, T, Tv, h and p
plot(intel.x_e,intel.U_e/intel.U_e(1),StyleS{ip},           'linewidth',lineWidths(ip),'Color',0.8*color_rainbow(ip,Nplts));ip=ip+1; hold on;
plot(intel.x_e,intel.TTrans_e/intel.TTrans_e(1),StyleS{ip}, 'linewidth',lineWidths(ip),'Color',0.8*color_rainbow(ip,Nplts));ip=ip+1;
plot(intel.x_e,intel.TVib_e/intel.TTrans_e(1),StyleS{ip},   'linewidth',lineWidths(ip),'Color',0.8*color_rainbow(ip,Nplts));ip=ip+1;
plot(intel.x_e,intel.h_e/intel.h_e(1),StyleS{ip},           'linewidth',lineWidths(ip),'Color',0.8*color_rainbow(ip,Nplts));ip=ip+1;
plot(intel.x_e,intel.p_e/intel.p_e(1),StyleS{ip},           'linewidth',lineWidths(ip),'Color',0.8*color_rainbow(ip,Nplts));%ip=ip+1;
xlabel('x [m]'); ylabel('q/q_0 [-]'); grid minor;
legend('U','T','T_v','h','p');
subplot(3,1,2);
for ip=1:Neqs
semilogy(intel.x_e,abs(eqs{ip}),               StyleS{ip},           'linewidth',lineWidths(ip),'Color',0.8*color_rainbow(ip,Neqs)); hold on;
end
xlabel('x [m]'); ylabel('\epsilon(eq) [-]'); grid minor;
legend(eq_names{:});
subplot(3,1,3)
for s=1:mixCnst.N_spec
    semilogy(intel.x_e,intel.y_se(:,s),StyleS{s},'linewidth',lineWidths(s),'Color',0.8*color_rainbow(s,mixCnst.N_spec)); hold on;
end
xlabel('x [m]'); ylabel('y_s [-]'); grid minor;
legend(mixCnst.spec_list{:});
pause(0.1);