function [h,hMark,hLine] = plotLessMarkers_VESTA(x,y,N_markers,markerStyle,varargin)
% plotLessMarkers_VESTA(x,y,N_markers,markerStyle,...) does the same as
% plot, only that it reduces the number of markers and puts them equispaced
% along the curve.
%
% plotLessMarkers_VESTA(x,y,N_markers,markerStyle,lineStyle,...) allows to
% fix also the lineStyle ('-','-.','--' or ':'}. If lineStyle is not in the
% second position, it will have to be introduced doing for example:
% plotLessMarkers_VESTA(...,'LineStyle','--') for a discontinuous line
%
% Note: colors must be introduced doing for example:
% plotLessMarkers_VESTA(...,'Color',[0 0 0]) for a black line. If no color
% is chosen, curves will be black [0 0 0].
%
% h = plotLessMarkers_VESTA(x,y,N_markers,markerStyle,lineStyle,...) returns
% the corresponding plot handle.
%
% [h,hMark,hLine] = plotLessMarkers_VESTA(...) returns also the plot handle
% of the marker or the line in isolation.
%
% plotLessMarkers_VESTA(...,'noLine') only plots the markers, without a line
%
% plotLessMarkers_VESTA(...,'semilogy') uses a logarithmic plot in the y axis.
%
% plotLessMarkers_VESTA(...,'semilogx') uses a logarithmic plot in the x axis.
%
% plotLessMarkers_VESTA(...,'loglog') uses a logarithmic plot in both the x
% and the y axis.
%
% plotLessMarkers_VESTA(...,'ylim',ylims) allows to fix the plotting limits
% in the y axis, so that the marker distribution is more coherent
%
% plotLessMarkers_VESTA(...,'xlim',xlims) allows to fix the plotting limits
% in the x axis, so that the marker distribution is more coherent
%
% All the typical arguments passed to plot are also valid here.
%
% Function imported from VESTA
%
% Author: Fernando Miro Miro
% Version 2.1
% Date: October 2019

% looking for flags
[varargin,bNoLine] = find_flagAndRemove('noLine',varargin);
[varargin,bLogy] = find_flagAndRemove('semilogy',varargin);
[varargin,bLogx] = find_flagAndRemove('semilogx',varargin);
[varargin,bLogLog] = find_flagAndRemove('loglog',varargin);
[varargin,~,idx_xlim] = find_flagAndRemove('xlim',varargin);
if ~isempty(idx_xlim)
    xlims = varargin{idx_xlim}; varargin(idx_xlim) = [];
else
    xlims = [min(x)-10*eps*abs(min(x)),max(x)+10*eps*abs(max(x))];
end
[varargin,~,idx_ylim] = find_flagAndRemove('ylim',varargin);
if ~isempty(idx_ylim)
    ylims = varargin{idx_ylim}; varargin(idx_ylim) = [];
else
    ylims = [min(y)-10*eps*abs(min(y)),max(y)+10*eps*abs(max(y))];
end
if nnz([bLogy,bLogx,bLogLog])>1
    error('more than one logarithmic plot flag was passed.');
end

if nargin>4 && ischar(varargin{1}) && sum(strcmp(varargin{1},{'-','-.','--',':'})) % we have lineStyle as the fifth input
    lineStyle = varargin{1};
    varargin = varargin(2:end); % removing lineStyle from varargin
else % we use solid line as a style
    lineStyle = '-';
end

% checking if no color has been passed through varargin, and if so, setting
% a default (black)
char_pos = cellfun(@ischar,varargin); % obtaining the indices of varargin that have strings in it.
if ~sum(strcmp(varargin(char_pos),'Color'))
    varargin = [varargin,{'Color',[0 0 0]}];
end

% building a coarser y vector and interpolating x onto it
x = x(:);                                           % making sure x and y are column vectors
y = y(:);
idx_keep = (x>=xlims(1) & x<=xlims(2) & y>=ylims(1) & y<=ylims(2)); % cutting before and after the limits
x_cut = x(idx_keep);            y_cut = y(idx_keep);
if bLogx || bLogLog
    lgX = log10(x_cut); lgX(lgX<-100) = -100; lgX(lgX>100) = 100;
    dlgX = abs(log10(xlims(2))-log10(xlims(1)));
    if dlgX==0
        dlgX = eps; % placeholder to avoid 0/0 situations. it won't do anything
    end
    x_norm = lgX/dlgX; % normalizing vectors WRT the maximum log variation (plotting range)
else
    dX = abs(xlims(2)-xlims(1));
    if dX==0
        dX = eps; % placeholder to avoid 0/0 situations. it won't do anything
    end
    x_norm = x_cut/dX;                    % normalizing vectors WRT the maximum variation (plotting range)
end
if bLogy || bLogLog
    lgY = log10(y_cut); lgY(lgY<-100) = -100; lgY(lgY>100) = 100;
    dlgY = abs(log10(ylims(2))-log10(ylims(1)));
    if dlgY==0
        dlgY = eps; % placeholder to avoid 0/0 situations. it won't do anything
    end
    y_norm = lgY/dlgY; % normalizing vectors WRT the maximum log variation (plotting range)
else
    dY = abs(ylims(2)-ylims(1));
    if dY==0
        dY = eps; % placeholder to avoid 0/0 situations. it won't do anything
    end
    y_norm = y_cut/dY;                    % This way we make sure that the distance along the curve is proportional to "appreciable" distances
end
x_diff = diff(x_norm);                              % computing increases in x and y
y_diff = diff(y_norm);
s = cumsum(sqrt(x_diff.^2 + y_diff.^2));            % computing the distance along the curve
s = [0;s];                                          % appending a zero at the beginning of s
s_coarse = linspace(min(s),max(s),N_markers);       % making a coarser distance vector
x_coarse = interp1(s,x_cut,s_coarse,'linear','extrap'); % interpolating x and y onto it
y_coarse = interp1(s,y_cut,s_coarse,'linear','extrap'); % not sure if this should be spline or linear

% plotting
if bLogy % semilogy plots
    if ~bNoLine
    hLine = semilogy(x,y,lineStyle,varargin{:});                               % first line (no markers)
    end
    hold on
    hMark = semilogy(x_coarse,y_coarse,markerStyle,varargin{:});               % second line (with markers)
    h = semilogy(x_coarse(1),y_coarse(1),[markerStyle,lineStyle],varargin{:}); % third line used to have the correct handle, actually a point (with markers and lines)
elseif bLogx % semilogx plots
    if ~bNoLine
    hLine = semilogx(x,y,lineStyle,varargin{:});                               % first line (no markers)
    end
    hold on
    hMark = semilogx(x_coarse,y_coarse,markerStyle,varargin{:});               % second line (with markers)
    h = semilogx(x_coarse(1),y_coarse(1),[markerStyle,lineStyle],varargin{:}); % third line used to have the correct handle, actually a point (with markers and lines)
elseif bLogLog % loglog plots
    if ~bNoLine
    hLine = loglog(x,y,lineStyle,varargin{:});                                 % first line (no markers)
    end
    hold on
    hMark = loglog(x_coarse,y_coarse,markerStyle,varargin{:});                 % second line (with markers)
    h = loglog(x_coarse(1),y_coarse(1),[markerStyle,lineStyle],varargin{:});   % third line used to have the correct handle, actually a point (with markers and lines)
else % normal plots
    if ~bNoLine
    hLine = plot(x,y,lineStyle,varargin{:});                                   % first line (no markers)
    end
    hold on
    hMark = plot(x_coarse,y_coarse,markerStyle,varargin{:});                   % second line (with markers)
    h = plot(x_coarse(1),y_coarse(1),[markerStyle,lineStyle],varargin{:});     % third line used to have the correct handle, actually a point (with markers and lines)
end
