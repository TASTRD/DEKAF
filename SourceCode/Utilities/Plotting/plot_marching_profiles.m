function [] = plot_marching_profiles(intel,profiles2plot,varargin)
% PLOT_MARCHING_PROFILES(intel,profiles2plot) plots the profiles obtained
% using DEKAF's marching algorithm.
%
% PLOT_MARCHING_PROFILES(intel,profiles2plot,N_x) allows to specify the
% number of profiles that are to be plotted in the x direction.
%
% Author(s): Fernando Miro Miro
% GNU Lesser General Public License 3.0

% Checking inputs
switch nargin
    case 2
        N_xPlot = 10;
    case 3
        N_xPlot = varargin{1};
    otherwise
        error('wrong number of inputs');
end

% determining arrangements of the subplots
N_subplots = length(profiles2plot);
if N_subplots==1
    Nrows = 1; Ncols = 1;
elseif N_subplots==2
    Nrows = 1; Ncols = 2;
elseif N_subplots==3
    Nrows = 1; Ncols = 3;
elseif N_subplots==4
    Nrows = 2; Ncols = 2;
elseif N_subplots<=6
    Nrows = 2; Ncols = 3;
elseif N_subplots<=8
    Nrows = 2; Ncols = 4;
elseif N_subplots==9
    Nrows = 3; Ncols = 3;
elseif N_subplots<=12
    Nrows = 3; Ncols = 4;
elseif N_subplots<=15
    Nrows = 3; Ncols = 5;
elseif N_subplots==16
    Nrows = 4; Ncols = 4;
elseif N_subplots<=20
    Nrows = 4; Ncols = 5;
end

x = intel.x(1,:);                                                                               % obtaining x positions
x_plot = linspace(min(x),max(x),N_xPlot);                                                       % determining the x locations where the plots are to be done
figure;
for ii=1:N_xPlot                                                                                % looping x locations
    [~,i_x] = min(abs(x_plot(ii)-x));                                                           % determining the position in the matrix corresponding to this x location
    rainbowVec = color_rainbow(ii,N_xPlot);                                                     % obtaining rainbow color corresponding to this x location
    for jj=1:N_subplots                                                                         % looping variables
        subplot(Nrows,Ncols,jj);
        plots(ii) = plot(intel.(profiles2plot{jj})(:,i_x),intel.y(:,i_x),'Color',rainbowVec);
        hold on
        xlabel(profiles2plot{jj});
        ylabel('y');
    end
    legends{ii} = ['x = ',num2str(x_plot(ii)),' m'];                                            % preparing legend
end
legend(plots,legends);