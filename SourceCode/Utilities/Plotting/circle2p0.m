function varargout = circle2p0(x,y,r,varargin)
%CIRCLE2P0 Plot circles provided the center (x,y) and the radius r.
%
% Usage:
%   h = circle2p0(x,y,r);
%   h = circle2p0(x,y,r,varargin);
%
% Inputs
%   x,y         arrays of x and y Cartesian coordinates for the centers of
%               the circles. Must be real.
%   r           array of radii of circles. Must be same length as x and y.
%   varargin    all optional arguments taken by MATLAB's intrinsic plot()
%
% Outputs
%   h           one figure handle of the plot call
%
% Author(s): Ethan Beyak & Koen Groot
%
% GNU Lesser General Public License 3.0

ntheta = 1000;
thetaf = 2*pi;
thetai = 0;
theta = thetai:(thetaf - thetai)/(ntheta-1):thetaf;

x = x(:).'; y = y(:).'; r = r(:).';
hwrk = plot(x + r.*cos(theta.'),y + r.*sin(theta.'),varargin{:});
if nargout
    % kill all handles, but one
    varargout{1} = hwrk(end);
end

end % circle2p0
