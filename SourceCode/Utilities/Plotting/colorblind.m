function varargout=colorblind(varargin)
%COLORBLIND Define a colormap that is reasonable for the color-blind: those
% with deuteranopia, protanopia, or tritanopia.
%
% Usage:
%   1. Get entire colormap
%       cm = colorblind();
%   2. Get colormap for N divisions
%       cm = colorblind(N);
%   3. Get the ith color of the map divided into N colors
%       clr = colorblind(i,N);
%
% Source: https://ux.stackexchange.com/a/94699 (accessed 2020-03-21)
%
% Author(s): Ethan Beyak

cm = [0   0   0  ;
      0   73  73 ;
      0   146 146;
      255 109 182;
      255 182 119;
      73  0   146;
      0   109 219;
      182 109 255;
      109 182 255;
      182 219 255;
      146 0   0  ;
      146 73  0  ;
      219 209 0  ;
      36  255 36 ;
      255 255 109;
      ]/255;

n_levels_cm = size(cm,1);
if nargin < 1
    varargout{1} = cm;
elseif nargin==1
    m = varargin{1};
    hsv=rgb2hsv(cm);
    ihsv = linspace(0,1,n_levels_cm).';
    ihsv_data = linspace(0,1,m).';
    hsv_data=interp1(ihsv, hsv, ihsv_data);
    varargout{1}=hsv2rgb(hsv_data);
elseif nargin==2
    k = 1; % tweaking parameter -- HARDCODE ALERT!
    RGB_vecx = linspace(-1e-8,1+1e-8,length(cm(1:end*k,1)));
    ii = varargin{1};
    N = varargin{2};%ceil(10/9*varargin{2});
    varargout{1} = [interp1(RGB_vecx,cm(1:end*k,1),(ii-1)/(N-1)),...
                    interp1(RGB_vecx,cm(1:end*k,2),(ii-1)/(N-1)),...
                    interp1(RGB_vecx,cm(1:end*k,3),(ii-1)/(N-1))];
                % avoid bright beige by stopping at k of the length
end

end % colorblind
