function [intel] = plot_gifConvergence(intel,options,varargin)
% plot_gifConvergence(intel,options) creates an animated gif with the
% evolution of the selected fluctuation variables with the iteration steps.
%
% It uses fields in intel specified by options fields4gif, and saves in the
% path in options.savePath_gif. Options and defaults detailed in
% setDefaults.m
%
% plot_gifConvergence(intel,options,conv) allows to specify the convergence
% achieved during the run
%
% plot_gifConvergence(intel,options,conv,i_xi) allows to specify also in
% what i_xi it is
%
% plot_gifConvergence(intel,options,conv,i_xi,nameXtra) allows to specify
% also a name suffix for the file.
%
% plot_gifConvergence(intel,options,conv,i_xi,nameXtra,bRestart) allows
% pass a boolean determining if the gif should be restarted
%
% See also: setDefaults
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

conv_str = '';
i_xi = 1;
nameXtra = '';
bRestart = false;

switch length(varargin)
    case 0
    case 1;         conv_str = [' conv = ',num2str(varargin{1},'%0.5e')];
    case 2;         conv_str = [' conv = ',num2str(varargin{1},'%0.5e')];
                    i_xi = varargin{2};
    case 3;         conv_str = [' conv = ',num2str(varargin{1},'%0.5e')];
                    i_xi = varargin{2};
                    nameXtra = varargin{3};
    case 4;         conv_str = [' conv = ',num2str(varargin{1},'%0.5e')];
                    i_xi = varargin{2};
                    nameXtra = varargin{3};
                    bRestart = varargin{4};
    otherwise;      error('wrong number of inputs');
end

options.xlim            =   options.gifxlim;
options.ylim            =   options.gifylim;
options.fields2plot =       options.fields4gif;                     % defining the fields we want to plot so that plotProfiles understands them
options.fields2plotNames =  options.fields4gifNames;                % same story with the fancy names
options.N_subplots =        options.gif_Nsubplots;                  % same story with the number of fields
options.logPlots =          options.gifLogPlot;                     % same story with the log-plot boolean

if ~isfield(intel,'movieID') || isempty(intel.movieID)              % In the first iteration we must start the animation and so on
    intel.gifID = figure;                                           % initializing figure ID
    axes();                                                         % needed for all the ...,'parent',figID.CurrentAxes
    bRestart = true;
end

if bRestart
    intel.movieID = 0;                                              % initializing frame counter
    plotProfiles(intel,options,i_xi,intel.gifID);                  % plotting
    title(['Frame ',num2str(intel.movieID),conv_str],'parent',intel.gifID.CurrentAxes);

    if isfield(intel,'gifMovie')
        intel.gifMovie(:,:,:,2:end) = [];
    end
    f = getframe(intel.gifID);                                      % getting frame
    [~,intel.gifmap] = rgb2ind(f.cdata,256,'nodither');             % storing color map
else                                                                % in all others we just plot
    intel.movieID = intel.movieID + 1;                              % increasing loop count

    plotProfiles(intel,options,i_xi,intel.gifID);                  % plotting
    title(['Frame ',num2str(intel.movieID),conv_str],'parent',intel.gifID.CurrentAxes);

    f=getframe(intel.gifID);                                        % Getting the frames
    intel.gifMovie(:,:,1,intel.movieID) = rgb2ind(f.cdata,intel.gifmap,'nodither'); % storing
    imwrite(intel.gifMovie,intel.gifmap,[options.savePath_gif,nameXtra,'.gif'],'DelayTime',options.gifLapse,'LoopCount',inf); % writing to file
end

pause(0.2);