function [figID] = plotProfiles(intel,options,varargin)
% plotProfiles(intel,options) plots the selected profiles in
% options.fields2plot at several iterations of interest set through
% options.it_plot (defaults set in setDefaults). intel is a structure with
% all the fields of interest: f, df_deta, etc. options will also have
% fields such as: Cooke, L, etc.
%
% If a non-default set of fields is requested, then options N_subplots must
% also be introduced, stating how many subfields it has (ys has N_spec
% fields). options.fieldsNames may also be introduced, in order to display
% a fancy latex name for each field.
%
% On variable input arguments (varargin):
%   varargin{1}     -   xi_loc  -   index corresponding to the xi location
%                                   default: options.mapOpts.N_x (last xi station)
%   varargin{2}     -   figID   -   figure identifier where the plot should
%                                   be done. Otherwise a new figure will be
%                                   created.
%
% see also: DEKAF_QSS
%
% Note that options.plot_transient must be set to true for this function to work
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

% To speed up the code, defaults are set in setDefaults

switch length(varargin)
    case 0
        xi_loc = 1; % Final xi location
        figID = figure;
    case 1
        xi_loc = varargin{1};
        figID = figure;
    case 2
        xi_loc = varargin{1};
        figID = varargin{2};
    otherwise
        error('wrong number of inputs');
end

fields2plot = options.fields2plot;
fieldsNames = options.fields2plotNames;
N_subplots = options.N_subplots;
plot_options = options.plot_options;
styleS = {'o','s','^','*','p','v','h','<','+','>','d'};
styleS = [styleS,styleS,styleS,styleS];
LineS = {'-','-.','--',':'};
LineS = [LineS,LineS,LineS,LineS,LineS,LineS,LineS,LineS,LineS];

N_plots = length(fields2plot); % number of fields to plot
N_plotsTot = sum(N_subplots); % total number of plots

if N_plotsTot<2 % fixing a minimum to N_plotsTot, such that color_rainbow doesn't return NaNs
    N_plotsTot = 2;
end

clear plots legends
clf(figID); axes(figID); % cleaning the figure before redrawing
if ismember(options.flow,{'TPGD','CNE','LTE','LTEED'}) && options.plot_massFractions % we make two suplots - one of them for ys
    if options.subplot_vertical;            subplot(2,1,1,'parent',figID); % initializing subplot
    else;                                   subplot(1,2,1,'parent',figID); % initializing subplot
    end
end
jj = 0;
% plots = gobjects(N_plots,1);
for ii=1:N_plots
    if N_subplots(ii)==1
        jj = jj+1;
        rainbowVec = color_rainbow(jj,N_plotsTot);
        if options.logPlots
            semilogx(real(abs(intel.(fields2plot{ii})(:,xi_loc))),intel.eta,[styleS{jj},LineS{jj}],'Color',rainbowVec,plot_options{:},...
                                'displayname',fieldsNames{ii},...
                                'Parent',figID.CurrentAxes);
        else
            plot(real(intel.(fields2plot{ii})(:,xi_loc)),intel.eta,[styleS{jj},LineS{jj}],'Color',rainbowVec,plot_options{:},...
                                'displayname',fieldsNames{ii},...
                                'Parent',figID.CurrentAxes);
        end
        hold(figID.CurrentAxes,'on');
    else
        for s=1:N_subplots(ii)
            jj = jj+1;
            rainbowVec = color_rainbow(jj,N_plotsTot);
            if options.logPlots
                semilogx(real(abs(intel.(fields2plot{ii}){s}(:,xi_loc))),intel.eta,[styleS{jj},LineS{jj}],'Color',rainbowVec,plot_options{:},...
                                'displayname',strrep(fieldsNames{ii},'#s',num2str(s)),...
                                'Parent',figID.CurrentAxes);
            else
                plot(real(intel.(fields2plot{ii}){s}(:,xi_loc)),intel.eta,[styleS{jj},LineS{jj}],'Color',rainbowVec,plot_options{:},...
                                'displayname',strrep(fieldsNames{ii},'#s',num2str(s)),...
                                'Parent',figID.CurrentAxes);
            end
            hold(figID.CurrentAxes,'on');
        end
    end
end
lgnd = legend(figID.CurrentAxes); set(lgnd,'interpreter','latex');
ylabel('$$\eta$$','interpreter','latex','Parent',figID.CurrentAxes);

% plot for the mass fractions
if ismember(options.flow,{'TPGD','CNE','LTE','LTEED'}) && options.plot_massFractions % we make two suplots - one of them for ys
    spec_list = intel.mixCnst.spec_list;
    clear plotss legendss
    if options.subplot_vertical;        subplot(2,1,2,'parent',figID);
    else;                               subplot(1,2,2,'parent',figID);
    end
    ys_min = zeros(1,options.N_spec); % allocating mass fractions
    for s=1:options.N_spec
        rainbowVec = color_rainbow(s,options.N_spec);
        semilogx(real(intel.ys{s}(:,xi_loc)),intel.eta,[styleS{s},LineS{s}],'Color',rainbowVec,plot_options{:},...
                                'displayname',['$$Y_{',spec_list{s},'}$$'],...
                                'Parent',figID.CurrentAxes);
        hold(figID.CurrentAxes,'on');
        ys_min(s) = min(real(intel.ys{s}(:,xi_loc))); % minimum mass fraction
    end
    lgnd = legend(figID.CurrentAxes); set(lgnd,'interpreter','latex');
    ylabel('$$\eta$$','interpreter','latex','Parent',figID.CurrentAxes);
    xlabel('$$Y_s$$','interpreter','latex','Parent',figID.CurrentAxes);
    figID.Children(2).XLim = [max([1e-20,min(real(ys_min))]),1];
    figID.Children(2).YLim = options.ylim;
    figID.Children(4).XLim = options.xlim;
    figID.Children(4).YLim = options.ylim;
else
    figID.Children(2).XLim = options.xlim;
    figID.Children(2).YLim = options.ylim;
end
pause(0);

end % plotProfiles
