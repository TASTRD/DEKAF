function vec = color_rainbow(ii,N)
% color_rainbow(i,N) returns the RBG vector identifying a position i in a
% rainbow vector of length N.
%
% Author: Fernando Miro Miro
% Date: November 2017
RGB_vecR = [1 , 1 , 1 ,0.5, 0 , 0 , 0 , 0 , 0 ,0.5, 1 ];
RGB_vecG = [0 ,0.5, 1 , 1 , 1 , 1 , 1 ,0.5, 0 , 0 , 0 ];
RGB_vecB = [0 , 0 , 0 , 0 , 0 ,0.5, 1 , 1 , 1 , 1 , 1 ];
RGB_vecx = linspace(-1e-8,1+1e-8,11);

vec = [interp1(RGB_vecx,RGB_vecR,(ii-1)/(N-1)),...
       interp1(RGB_vecx,RGB_vecG,(ii-1)/(N-1)),...
       interp1(RGB_vecx,RGB_vecB,(ii-1)/(N-1))];