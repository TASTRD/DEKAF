function [deltaStar_out] = fit_deltaStar3(xInv,deltaStar,ox)
% fit_deltaStar3 fits an analytical function to the displacement thickness
%
% The function is:
%           K*exp(-x.^2/tau1-x/tau2)+a*x.^3+b*x.^2+c*x+d
% with:
%   K    = deltaStar_0-d
%   tau1 = -x0^2/(log((deltaStar_0-a*x0^3-b*x0^2-c*x0-d)/(K*exp(-x0/tau2))))
%   tau2 = -K/(ddeltaStar_dx_0-c)
%
% Usage:
%   (1)
%       deltaStar_out = fit_deltaStar3(xInv,deltaStar,ox)
%
% Inputs and outputs:
%   xInv                [m]
%       position along the nozzle symmetry axis
%   deltaStar           [m]
%       displacement thickness
%   ox                  [-]
%       differentiation order
%   deltaStar_out       [m]
%       displacement thickness after fitting a function through it
%
% Author: Fernando Miro Miro based on the work of
%         Guillaume Grossir
% GNU Lesser General Public License 3.0

idx1    = find(xInv>=0,1,'first');                      % throat point
x       = xInv(idx1:end);                               % keeping x after the throat
Dx      = FDdif_nonEquis(x,['centered',num2str(ox)]);   % differentiation matrix in x
ddeltaStar_dx = Dx*deltaStar;                           % slope of the displacement thickness

[~,idx2] = max(deltaStar);
coef_fit = polyfit(xInv(idx1:idx2), deltaStar(idx1:idx2), 3);   % linear fitting
a        = coef_fit(1);
b        = coef_fit(2);
c        = coef_fit(3);
d        = coef_fit(4);

if min(xInv)<0                                                          % there are points before the throat
    deltaStar_0     = interp1(xInv, deltaStar,     0,'linear','extrap');    % displacement thickness at the nozle throat.
    ddeltaStar_dx_0 = interp1(xInv, ddeltaStar_dx, 0,'linear','extrap');
    x0 = 0;
else                                                                    % there are no points before the throat
    deltaStar_0     = deltaStar(1);                                         % displacement thickness at the first point
    ddeltaStar_dx_0 = ddeltaStar_dx(1);
    x0 = xInv(1);
end

K               = deltaStar_0-d;
tau2            = -K/(ddeltaStar_dx_0-c);
tau1            = -x0^2/(log((deltaStar_0-a*x0^3-b*x0^2-c*x0-d)/(K*exp(-x0/tau2))));
deltaStar_out   = K*exp(-x.^2/tau1-x/tau2)+a*x.^3+b*x.^2+c*x+d;


end % fit_deltaStar3