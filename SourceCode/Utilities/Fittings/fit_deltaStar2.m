function [deltaStar_out] = fit_deltaStar2(xInv,deltaStar,ox)
% fit_deltaStar2 fits an analytical function to the displacement thickness
%
% The function is:
%           (-b+deltaStar_0)*exp(-x/tau)+(a*x+b)
% with:
%       tau = -b/(a-ddeltaStar_dx_0)
% *_0 quantities correspond to the nozzle throat
%
% Usage:
%   (1)
%       deltaStar_out = fit_deltaStar2(xInv,deltaStar,ox)
%
% Inputs and outputs:
%   xInv                [m]
%       position along the nozzle symmetry axis
%   deltaStar           [m]
%       displacement thickness
%   ox                  [-]
%       differentiation order
%   deltaStar_out       [m]
%       displacement thickness after fitting a function through it
%
% Author: Fernando Miro Miro based on the work of
%         Guillaume Grossir
% GNU Lesser General Public License 3.0

idx1    = find(xInv>=0,1,'first');                      % throat point
x       = xInv(idx1:end);                               % keeping x after the throat
Dx      = FDdif_nonEquis(x,['centered',num2str(ox)]);   % differentiation matrix in x
ddeltaStar_dx = Dx*deltaStar;                           % slope of the displacement thickness

[~,idx2] = max(deltaStar);
coef_fit = polyfit(xInv(idx1:idx2), deltaStar(idx1:idx2), 1);   % linear fitting
a        = coef_fit(1);
b        = coef_fit(2);

if min(xInv)<0                                                          % there are points before the throat
    deltaStar_0     = interp1(xInv, deltaStar,     0,'linear','extrap');    % displacement thickness at the nozle throat.
    ddeltaStar_dx_0 = interp1(xInv, ddeltaStar_dx, 0,'linear','extrap');
else                                                                    % there are no points before the throat
    deltaStar_0     = deltaStar(1);                                         % displacement thickness at the first point
    ddeltaStar_dx_0 = ddeltaStar_dx(1);
end

tau           = -b/(a-ddeltaStar_dx_0);              % condition to get a slope equal to 'slope_delta_throat' at the nozzle throat
deltaStar_out = (-b+deltaStar_0)*exp(-x/tau)+(a*x+b);% fit composed of an exponential decay and followed by a linear growth of the BL thickness


end % fit_deltaStar2