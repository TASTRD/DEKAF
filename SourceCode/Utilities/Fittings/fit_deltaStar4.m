function [deltaStar_out] = fit_deltaStar4(xInv,deltaStar)
% fit_deltaStar4 fits multiple third-order polynomials to smooth out the
% data.
%
% A window is made around each point of +/-5% of the total x stretch. Then
% a third-order polynomial is fit through the data contained in that
% window. The polynomial is then evaluated at the central point of the
% window, thus effectively smoothing out the data.
%
% Usage:
%   (1)
%       deltaStar_out = fit_deltaStar4(xInv,deltaStar)
%
% Inputs and outputs:
%   xInv                [m]
%       position along the nozzle symmetry axis
%   deltaStar           [m]
%       displacement thickness
%   deltaStar_out       [m]
%       displacement thickness after fitting a function through it
%
% Author: Fernando Miro Miro based on the work of
%         Guillaume Grossir
% GNU Lesser General Public License 3.0

% deltaStar_out = smooth(xInv,deltaStar,0.1,'rloess');

wdw = 0.05*(max(xInv) - min(xInv));                 % window size around each point (5% of the total x stretch
deltaStar_out = zeros(size(deltaStar));             % allocating 
for ix=length(deltaStar):-1:1                       % looping mesh positions
    idx = (xInv>xInv(ix)-wdw) & (xInv<xInv(ix)+wdw);    % points contained within the window
    P   = polyfit(xInv(idx),deltaStar(idx),2);          % polynomial fit in the window
    deltaStar_out(ix) = polyval(P,xInv(ix));            % evaluating fit at mesh point ix
end

end % fit_deltaStar4