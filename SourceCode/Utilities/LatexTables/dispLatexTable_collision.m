function [] = dispLatexTable_collision(mixCnst,col_ID,varargin)
% dispLatexTable_collision prints out to screen a table in latex format
% with the curve fit parameters for the collision integrals.
%
% Usage:
%   (1)
%   dispLatexTable_collision(mixCnst,'Omega11')
%      |--> prints the fits for the (1,1) collision
%
%   (2)
%   dispLatexTable_collision(mixCnst,{'Bstar','Cstar'})
%      |--> prints the fits for both the Bstar and Cstar collision ratios,
%      stashed from left to right in the table
%
%   (3)
%   dispLatexTable_collision(mixCnst,'Omega11',specs_1,specs_2)
%      |--> allows to pass two lists of species to output only the
%      collisions between them
%
%   (4)
%   dispLatexTable_collision(...,'order',N)
%      |--> displays only N+1 columns (necessary for a Nth order polynomial
%      fitting)
%
%   (5)
%   dispLatexTable_collision(...,'customNames',names)
%      |--> allows to pass a cell of strings containing customized names
%      for the different pairs. The names cell must have as many positions
%      as unique pairs can be made out of specs_1 and specs_2
%
%   (6)
%   dispLatexTable_collision(...,'write2file',fID)
%      |--> instead of displaying on the screen, it writes to the file
%      identified with fID
%
%   (7)
%       dispLatexTable_electronStates(...,'label',label)
%      |--> allows to pass a label string, with the label to be assigned to
%      the table
%
%   (8)
%       dispLatexTable_electronStates(...,'caption',caption)
%      |--> allows to pass a caption string, with the caption to be used
%      for the table's caption
%
%   (9)
%       dispLatexTable_electronStates(...,'open_str',open_str)
%      |--> allows to pass an opening string
%
%   (10)
%       dispLatexTable_electronStates(...,'stackColumns')
%      |--> it divide the table into two and stacks them one on top of the
%      other.
%
%   (11)
%       dispLatexTable_reactions(...,'autoSplitRows',N)
%      |--> automatically splits the table into subtables if it is longer
%      than N collisions per table. By default it is 30
%
% See also: get_specNameLatex
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% checking for the 'order' flag
[varargin,~,idx] = find_flagAndRemove('order',varargin);
if isempty(idx)
    order = 5; % default
else
    order = varargin{idx};
    varargin(idx) = [];
end
% checking for the 'customNames' flag
[varargin,bCustomNames,idx] = find_flagAndRemove('customNames',varargin);
if ~isempty(idx)
    customNames = varargin{idx};
    varargin(idx) = [];
end

% checking other flags
[NmaxRows,          varargin] = parse_optional_input(varargin,'autoSplitRows',30);
[label_str,         varargin] = parse_optional_input(varargin,'label','');
[caption_str,       varargin] = parse_optional_input(varargin,'caption','');
[open_str,          varargin] = parse_optional_input(varargin,'open_str','');
[varargin,bStackColumns]      = find_flagAndRemove('stackColumns',varargin);

% checking for the 'write2file' flag
[varargin,bWrite2file,idx] = find_flagAndRemove('write2file',varargin);
if ~isempty(idx)
    fID = varargin{idx};
    varargin(idx) = [];
else
    fID = '';
end
% checking for species lists
spec_list = mixCnst.spec_list;                                          % extracting species list from mixCnst
switch length(varargin)
    case 0
        specs_1 = spec_list;
        specs_2 = spec_list;
    case 2
        specs_1 = varargin{1};
        specs_2 = varargin{2};
    otherwise
        error('wrong number of inputs');
end

if ~iscell(col_ID);     col_ID = {col_ID};      end                     % making sure that col_ID is a cell
Ncoll = length(col_ID);                                                 % number of collisions to stash in columns
for icol = 1:Ncoll
    consts{icol} = mixCnst.(['consts_',col_ID{icol}]);                      % extracting collision constants
    if strcmp(col_ID{icol}(1:5),'Omega')                                    % renaming omegas
        col_name{icol} = regexprep(col_ID{icol},'Omega([0-9])([0-9])','\\Omega\^{($1,$2)}_{s\\ell}');
    elseif strcmp(col_ID{icol}(2:5),'star')                                 % renaming Astar and company
        col_name{icol} = regexprep(col_ID{icol},'([a-z]|[A-Z])star','$1\^{\*}_{s\\ell}');
    end

    % Preparing string cells
    ic=(icol-1)*(order+1) + 1;
    if order>-1;        str_header{ic} = [' $A^{',col_name{icol},'}$ '];     ic=ic+1;end
    if order>0;         str_header{ic} = [' $B^{',col_name{icol},'}$ '];     ic=ic+1;end
    if order>1;         str_header{ic} = [' $C^{',col_name{icol},'}$ '];     ic=ic+1;end
    if order>2;         str_header{ic} = [' $D^{',col_name{icol},'}$ '];     ic=ic+1;end
    if order>3;         str_header{ic} = [' $E^{',col_name{icol},'}$ '];     ic=ic+1;end
    if order>4;         str_header{ic} = [' $F^{',col_name{icol},'}$ '];     ic=ic+1;end

    passed_pairs = {};      it=0;
    for s=1:length(specs_1)                                                 % looping species
        spec_s = get_specNameLatex(specs_1{s});                                 % name of species s
        ss = find(ismember(spec_list,specs_1{s}));                              % index of species s in original spec_list
        for l=1:length(specs_2)                                                 % looping species
            spec_l = get_specNameLatex(specs_2{l});                                 % name of species l
            ll = find(ismember(spec_list,specs_2{l}));                              % index of species l in original spec_list
            pair_ID = num2str(sort([ss,ll]));                                       % obtaining string identifying the ss-ll pair
            if ~ismember(pair_ID,passed_pairs)                                      % checking if the pair has already been printed
                it=it+1; % increase counter
                passed_pairs = [passed_pairs;pair_ID];                                  % adding it to the list of checked pairs
                if bCustomNames                                                         % if the user asked for customized names
                    str_pairs{it} = customNames{it};                                        % we take those
                else                                                                    % otherwise
                    str_pairs{it} = ['$',spec_s,'-',spec_l,'$'];                            % we take the name string of the s-l pair
                end
                % populating
                ic=(icol-1)*(order+1) + 1;
                if order>-1;        str_consts{it,ic} = sprintf(' %0.4e ',consts{icol}.A(ss,ll));     ic=ic+1;end
                if order>0;         str_consts{it,ic} = sprintf(' %0.4e ',consts{icol}.B(ss,ll));     ic=ic+1;end
                if order>1;         str_consts{it,ic} = sprintf(' %0.4e ',consts{icol}.C(ss,ll));     ic=ic+1;end
                if order>2;         str_consts{it,ic} = sprintf(' %0.4e ',consts{icol}.D(ss,ll));     ic=ic+1;end
                if order>3;         str_consts{it,ic} = sprintf(' %0.4e ',consts{icol}.E(ss,ll));     ic=ic+1;end
                if order>4;         str_consts{it,ic} = sprintf(' %0.4e ',consts{icol}.F(ss,ll));     ic=ic+1;end
            end
        end
    end
end
str_consts = strrep(str_consts,'0.0000e+00','$ $');                     % removing zeros

Nrows = it;
Nsubtable = ceil(Nrows/NmaxRows);                                          % number of subtables

if Nsubtable==1                                                             % no need to split
    dispOrWrite(bWrite2file,fID,['\begin{table*}',open_str]);                   % table opening line
    dispOrWrite(bWrite2file,fID,'\centering');                                  % table centering
    writeTable(str_consts,str_pairs,str_header,fID,bStackColumns);              % writing table
    dispOrWrite(bWrite2file,fID,['\caption{',caption_str,'}']);                 % caption string
    dispOrWrite(bWrite2file,fID,['\label{',label_str,'}']);                     % label string
    dispOrWrite(bWrite2file,fID,'\end{table*}');                                % table closing line
else                                                                        % we need to split
    it=1;
    sbplts = divideAndRemainder(Nrows,NmaxRows);
    for isbtbl = 1:Nsubtable                                                    % looping subtables
        idx_sbplt = it:sbplts(isbtbl);     it=sbplts(isbtbl)+1;                     % inidices of the rows to include in this subplot
        dispOrWrite(bWrite2file,fID,['\begin{table*}',open_str]);                   % table opening line
        dispOrWrite(bWrite2file,fID,'\centering')                                   % table centering
        if isbtbl~=1
            dispOrWrite(bWrite2file,fID,'\ContinuedFloat');                         % continued float string
        end
        writeTable(str_consts(idx_sbplt,:),str_pairs(idx_sbplt),str_header,fID,bStackColumns); % writing table
        dispOrWrite(bWrite2file,fID,['\caption{',caption_str,'}']);                 % caption string
        if isbtbl==1
            caption_str = ['(cont) ',caption_str];                                      % adding (cont) to the caption string
            dispOrWrite(bWrite2file,fID,['\label{',label_str,'}']);                     % label string
        end
        dispOrWrite(bWrite2file,fID,'\end{table*}');                                % table closing line
    end
end

end % dispLatexTable_electronStates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = writeTable(str_consts,str_pairs,str_header,fID,bStackColumns)
% writeTable is a subfunction to dispLatexTable_reactions printing the
% subtable to the file or to the workspace.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

bWrite2file = ~isempty(fID);
Ncol = length(str_header);                                              % number of columns the table will have
dispOrWrite(bWrite2file,fID,['\begin{tabular}{',repmat('c',[1,Ncol/(bStackColumns+1)+1]),'}']); % table header
dispOrWrite(bWrite2file,fID,'\hline');                                  % first horizontal line
if bStackColumns                                                        % stacking half the columns on top of the other half
    idxMiddle = ceil(Ncol/2);                                               % locating the middle of the columns
    writeSubTable(str_consts,str_pairs,str_header,'Pair ',1:idxMiddle,bWrite2file,fID);
    dispOrWrite(bWrite2file,fID,'\hline');                                  % horizontal line separating two subtables
    writeSubTable(str_consts,str_pairs,str_header,'Pair ',idxMiddle+1:Ncol,bWrite2file,fID);
else                                                                    % putting all columns together
    writeSubTable(str_consts,str_pairs,str_header,'Pair ',1:Ncol,bWrite2file,fID);
end
dispOrWrite(bWrite2file,fID,'\hline');                                  % final horizontal line
dispOrWrite(bWrite2file,fID,'\end{tabular}');                           % close table header

end % writeTable
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = writeSubTable(str_consts,str_pairs,str_header,str_corner,idx,bWrite2file,fID)
% writeSubTable writes to file or to screen a set of column indices of the
% table formed by the cells passed.
%
% Usage:
%   (1)
%       writeSubTable(str_consts,str_pairs,str_header,str_corner,idx,bWrite2file,fID)
%
% Inputs and outputs
%   str_consts
%       cell (Npair x Ncol) with the strings to be written in each table
%       position.
%   strs_pairs
%       cell (1 x Npair) with the strings for the different the row labels
%   str_header
%       cell (1 x Ncol) with the strings for the different the column labels
%   str_corner
%       string for the top-left corner of the table
%   idx
%       index of the columns of the table to be printed
%   bWrite2file
%       boolean to say whether the table should be written to a file (true)
%       or printed to the screen
%   fID
%       file ID onto which to write (if necessary)
%
% See also: dispLatexTable_electronStates
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

dispOrWrite(bWrite2file,fID,[strjoin([{str_corner},str_header(idx)],' & '),' \\']);
dispOrWrite(bWrite2file,fID,'\hline');                                  % second horizontal line
for ii=1:length(str_pairs)
dispOrWrite(bWrite2file,fID,[strjoin([{str_pairs{ii}},str_consts(ii,idx)],' & '),' \\']);
end

end % writeSubTable