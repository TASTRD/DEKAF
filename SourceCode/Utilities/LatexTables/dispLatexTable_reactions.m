function [] = dispLatexTable_reactions(mixCnst,varargin)
% dispLatexTable_reactions prints out to screen a table in latex format for
% the reaction-rate constants of the different reactions.
%
% Usage:
%   (1)
%       dispLatexTable_reactions(mixCnst)
%       |--> displays a single table with all reactions
%
%   (2)
%       dispLatexTable_reactions(...,'reactions',vec_reactions)
%       |--> allows to pick a vector of reactions from those in
%       mixCnst.reac_list to include in the table
%
%   (3)
%       dispLatexTable_reactions(...,'write2file',fID)
%      |--> instead of displaying on the screen, it writes to the file
%      identified with fID
%
%   (4)
%       dispLatexTable_reactions(...,'label',label)
%      |--> allows to pass a label string, with the label to be assigned to
%      the table
%
%   (5)
%       dispLatexTable_reactions(...,'caption',caption)
%      |--> allows to pass a caption string, with the caption to be used
%      for the table's caption
%
%   (6)
%       dispLatexTable_reactions(...,'open_str',open_str)
%      |--> allows to pass an opening string
%
%   (7)
%       dispLatexTable_reactions(...,'autoSplit',N)
%      |--> automatically splits the table into subtables if it is longer
%      than N reactions per table
%
%   (8)
%       dispLatexTable_reactions(...,'eqReactions')
%      |--> creates a table for the arrhenius coefficients for the
%      equilibrium constants, not the forward reaction rates
%
% See also: get_specNameLatex
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

%% Checking and analyzing inputs
% checking for the 'write2file' flag
[varargin,bWrite2file,idx] = find_flagAndRemove('write2file',varargin);
if ~isempty(idx)
    fID = varargin{idx};
    varargin(idx) = [];
else
    fID = '';
end
% checking for the 'reactions' flag
[varargin,~,idx] = find_flagAndRemove('reactions',varargin);
if ~isempty(idx)                                                            % if the user wants to choose reactions
    i_reacs = varargin{idx};                                                    % we read them
    varargin(idx) = [];                                                         % and remove it from varargin
else                                                                        % otherwise
    i_reacs = 1:length(mixCnst.reac_list);                                      % we take them all
end
% checking for other flags
[label_str,   varargin] = parse_optional_input(varargin,'label','');
[caption_str, varargin] = parse_optional_input(varargin,'caption','');
[open_str,    varargin] = parse_optional_input(varargin,'open_str','');
[varargin,bEqReactions] = find_flagAndRemove('eqReactions',varargin);
% Analyzing inputs
Nr = length(i_reacs);
spec_list = mixCnst.spec_list;                                              % extracting species list
reac_list = mixCnst.reac_list;                                              % extracting reaction list
N_s = length(spec_list);                                                    % number of species
bidx_reacM = ~cellfun(@isempty,regexp(reac_list,'M','once'));               % indices for reactions with an M (any species)
reacPerReac = N_s*bidx_reacM + ~bidx_reacM;                                 % number of subreactions in each reaction (decomposing M)
NrTot = sum(reacPerReac(i_reacs));                                          % total number of reactions (decomposing M)
% checking for the 'caption' flag
[varargin,~,idx] = find_flagAndRemove('autoSplit',varargin);
if ~isempty(idx)                                                            % if the user wants to fix maximum number of reactions to split at
    Nsplit = varargin{idx};                                                     % we read it
    varargin(idx) = [];                                                         % and remove it from varargin
else                                                                        % otherwise
    Nsplit = NrTot;                                                             % we leave it empty
end

%% Splitting into subtables if necessary
eor = false;                                                                % end of reactions boolean
ist = 0;                                                                    % initializing subtable counter
ir = 0;                                                                     % initializing reaction counter (without expanding M)
while ~eor                                                                  % looping subtables
    ist=ist+1;                                                                  % increasing subtable counter
    isr = 0;                                                                    % initializing subreaction counter (expanding M)
    cll_reacs{ist} = [];                                                        % allocating cell of list of reactions
    while ~eor && ...                                                           % until we don't reach the end of the reactions
            isr + reacPerReac(i_reacs(ir+1)) <= Nsplit                          % or the maximum number of reactions per
        ir=ir+1;                                                                    % increasing reaction counter (without expanding M)
        isr = isr + reacPerReac(i_reacs(ir));                                       % increasing subreaction counter (expanding M)
        cll_reacs{ist} = [cll_reacs{ist},i_reacs(ir)];                              % extending cell of lists of reactions
        if ir==Nr;      eor=true;   end                                             % closing condition
    end
end
Nsubtable = length(cll_reacs);                                              % number of subtables

if Nsubtable==1                                                             % no need to split
    dispOrWrite(bWrite2file,fID,['\begin{table*}',open_str]);                   % table opening line
    writeTable(mixCnst,spec_list,reac_list,fID,cll_reacs{1},bidx_reacM,bEqReactions);        % writing table
    dispOrWrite(bWrite2file,fID,['\caption{',caption_str,'}']);                 % caption string
    dispOrWrite(bWrite2file,fID,['\label{',label_str,'}']);                     % label string
    dispOrWrite(bWrite2file,fID,'\end{table*}');                                % table closing line
else                                                                        % we need to split
    for isbtbl = 1:Nsubtable                                                    % looping subtables
        dispOrWrite(bWrite2file,fID,['\begin{table*}',open_str]);                   % table opening line
        if isbtbl~=1
            dispOrWrite(bWrite2file,fID,'\ContinuedFloat');                         % continued float string
        end
        writeTable(mixCnst,spec_list,reac_list,fID,cll_reacs{isbtbl},bidx_reacM,bEqReactions); % writing table
        dispOrWrite(bWrite2file,fID,['\caption{',caption_str,'}']);             % caption string
        if isbtbl==1
            caption_str = ['(cont) ',caption_str];                                      % adding (cont) to the caption string
            dispOrWrite(bWrite2file,fID,['\label{',label_str,'}']);                     % label string
        end
        dispOrWrite(bWrite2file,fID,'\end{table*}');                                % table closing line
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = writeTable(mixCnst,spec_list,reac_list,fID,i_reacs,bidx_reacM,bEqReactions)
% writeTable is a subfunction to dispLatexTable_reactions printing the
% subtable to the file or to the workspace.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_s = length(spec_list);
Nr = length(i_reacs);
bWrite2file = ~isempty(fID);

% preparing strings
if nnz(bidx_reacM(i_reacs))                                                 % the header needs an M column
    bMCol = true;                                                               % setting boolean to true
    str_open = '\begin{tabular}{ccccc}';
    if bEqReactions                                                             % equilibrium reaction constants
    str_header = 'Reaction & M & $A^\text{eq}_r$ $\left[\frac{\left(\text{m}^3/\text{mol}\right)^{\sum_s \nu''_{s r}-\nu''''_{s r}}}{\text{K}^{n^\text{eq}_{Tr}}}\right]$ & $n^\text{eq}_{Tr}$ [-] & $\theta^\text{eq}_r$ [K] \\';
    else                                                                        % forward reaction-rate constants
    str_header = 'Reaction & M & $A^f_r$ $\left[\frac{\left(\text{m}^3/\text{mol}\right)^{\sum_s \nu''_{s r}-1}}{\text{s}\,\text{K}^{n^f_{Tr}}}\right]$ & $n^f_{Tr}$ [-] & $\theta^f_r$ [K] \\';
    end
else                                                                        % it doesn't
    bMCol = false;                                                              % setting boolean to false
    str_open = '\begin{tabular}{cccc}';
    if bEqReactions                                                             % equilibrium reaction constants
    str_header = 'Reaction &     $A^\text{eq}_r$ $\left[\frac{\left(\text{m}^3/\text{mol}\right)^{\sum_s \nu''_{s r}-\nu''''_{s r}}}{\text{K}^{n^\text{eq}_{Tr}}}\right]$ & $n^\text{eq}_{Tr}$ [-] & $\theta^\text{eq}_r$ [K] \\';
    else                                                                        % forward reaction-rate constants
    str_header = 'Reaction &     $A^f_r$ $\left[\frac{\left(\text{m}^3/\text{mol}\right)^{\sum_s \nu''_{s r}-1}}{\text{s}\,\text{K}^{n^f_{Tr}}}\right]$ & $n^f_{Tr}$ [-] & $\theta^f_r$ [K] \\';
    end
end
strTab = sprintf('\t');                                                     % tab string
for s=length(spec_list):-1:1                                                % looping species
    specLatex_list{s} = get_specNameLatex(spec_list{s});                        % obtaining latex names for the species
end

if bEqReactions                                                             % we want the equilibrium Arrhenius coefficients (in Keq_struct
    A_r = mixCnst.Keq_struc.A_eqr;
    nT_r = mixCnst.Keq_struc.nT_eqr;
    theta_r = mixCnst.Keq_struc.theta_eqr;
else                                                                        % we want the forward reaction-rate constants
    A_r = mixCnst.A_r;
    nT_r = mixCnst.nT_r;
    theta_r = mixCnst.theta_r;
end

% writing to file
dispOrWrite(bWrite2file,fID,'\centering');                                  % table centering
dispOrWrite(bWrite2file,fID,str_open);                                      % table opening line
dispOrWrite(bWrite2file,fID,'\hline');                                      % first line
dispOrWrite(bWrite2file,fID,str_header);                                    % table header
dispOrWrite(bWrite2file,fID,'\hline');                                      % second line
ic = i_reacs(1) + sum(bidx_reacM(1:i_reacs(1)-1))*(N_s-1);                  % initializing reaction index counter (positions in mixCnst.A_r and company)
% we initialize it with the first reaction chosen by the user, plus all the
% M reactions that might have been skipped: the sum of bidx_reacM before
% that chosen reaction times the number of species minus one
for r=1:Nr                                                                  % looping reactions
    str_reac = get_reacNameLatex(reac_list{i_reacs(r)},spec_list);              % obtaining fancy name for the reaction
    if bidx_reacM(i_reacs(r))                                                   % we need to now loop over the species
        for s=1:N_s                                                                 % looping species
            if s==1                                                                     % first reaction info (with equation, n and theta)
                str2disp = ['$',str_reac,'$  & $',specLatex_list{1},'$',strTab,' & ',sprintf('%0.2e',A_r(ic)),strTab,' & ',num2str(nT_r(ic)),strTab,' & ',num2str(theta_r(ic)),strTab,' \\'];
            else                                                                        % rest of reaction info (no reaction, n or theta)
                str2disp = ['$ $',repmat(strTab,[1,7]),'  & $',specLatex_list{s},'$',strTab,' & ',sprintf('%0.2e',A_r(ic)),strTab,' & $ $ ',strTab,strTab,' & $ $ ',strTab,strTab,' \\'];
            end
            dispOrWrite(bWrite2file,fID,str2disp);                                      % displaying reaction line
            ic=ic+1;                                                                    % increasing counter
        end
    else                                                                        % we only have to print a single row
        if bMCol                                                                    % we must include the empty column for M
            str2disp = ['$',str_reac,'$  & $ $ ',strTab,strTab,' & ',sprintf('%0.2e',A_r(ic)),strTab,' & ',num2str(nT_r(ic)),strTab,' & ',num2str(theta_r(ic)),strTab,' \\'];
        else                                                                        % we don't
            str2disp = ['$',str_reac,'$  & ',sprintf('%0.2e',A_r(ic)),strTab,' & ',num2str(nT_r(ic)),strTab,' & ',num2str(theta_r(ic)),strTab,' \\'];
        end
        dispOrWrite(bWrite2file,fID,str2disp);                                      % displaying reaction line
        ic=ic+1;                                                                    % increasing counter
    end
end
dispOrWrite(bWrite2file,fID,'\hline');                                      % final line
dispOrWrite(bWrite2file,fID,'\end{tabular}');                               % table closing line
