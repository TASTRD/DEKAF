function [] = dispLatexTable_vibProp(mixCnst,varargin)
% dispLatexTable_vibProp prints out to screen a table in latex format for the properties of the different electronic species.
%
% Usage:
%   (1)
%       dispLatexTable_vibProp(mixCnst)
%       |--> displays a single table with both vibrational relaxation
%       constants (aVib_sl and bVib_sl)
%
%   (2)
%       dispLatexTable_vibProp(mixCnst,vibrators,partners)
%       |--> allows to specify which are the vibrator set of species, and
%       which are the partners
%
%   (3)
%       dispLatexTable_vibProp(...,'write2file',fID)
%       |--> instead of displaying on the screen, it writes to the file
%      identified with fID
%
%   (4)
%       dispLatexTable_vibProp(...,'label',label)
%      |--> allows to pass a label string, with the label to be assigned to
%      the table
%
%   (5)
%       dispLatexTable_vibProp(...,'caption',caption)
%      |--> allows to pass a caption string, with the caption to be used
%      for the table's caption
%
%   (6)
%       dispLatexTable_vibProp(...,'open_str',open_str)
%      |--> allows to pass an opening string
%
%   (7)
%       dispLatexTable_specProp(...,'lineSpacing',lineSpacing_str)
%      |--> allows to pass an string to identify the table's inter-line
%      spacing. Eg.: '5pt'
%
% See also: get_specNameLatex
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% checking for custom properties
spec_list = mixCnst.spec_list;          % extracting species list
N_s = length(spec_list);

% checking flags
[varargin,bWrite2file,idx] = find_flagAndRemove('write2file',varargin);
if ~isempty(idx);   fID = varargin{idx};
                    varargin(idx) = [];
else;               fID = '';
end
[label_str,         varargin] = parse_optional_input(varargin,'label','');
[caption_str,       varargin] = parse_optional_input(varargin,'caption','');
[open_str,          varargin] = parse_optional_input(varargin,'open_str','');
[lineSpacing_str,   varargin] = parse_optional_input(varargin,'lineSpacing','0pt');
if isempty(varargin);   spec_vibrators = spec_list;
                        spec_partners  = spec_list;
else;                   spec_vibrators = varargin{1};
                        spec_partners  = varargin{2};
end


% preparing header
str_header = 'Vibrator & Partner & $a^{Vib}_{s\ell}$ [K\ts{1/3}] & $b^{Vib}_{s\ell}$ [K\ts{-1/3}] \\';

% start displaying
dispOrWrite(bWrite2file,fID,['\begin{table*}',open_str]);                   % table opening line
dispOrWrite(bWrite2file,fID,'\centering');                                  % table centering
dispOrWrite(bWrite2file,fID,'\begin{tabular}{cccc}'); % table header
dispOrWrite(bWrite2file,fID,'\hline');                                      % first horizontal line
dispOrWrite(bWrite2file,fID,str_header);                                    % displaying header
dispOrWrite(bWrite2file,fID,'\hline');                                      % second horizontal line
for s=1:length(spec_vibrators)                                              % looping species
    ss = find(ismember(spec_list,spec_vibrators{s}));                           % identifying vibrator in spec_list
    if mixCnst.bMol(ss)                                                         % only molecular species vibrate
        for l=1:length(spec_partners)                                           % looping rows for one same species
            ll = find(ismember(spec_list,spec_partners{l}));                            % identifying partner in spec_list
            if ll==1;    str_s = ['$',get_specNameLatex(spec_list{ss}),'$'];            % fancy species name
            else;       str_s = '$ $';    end                                           % no name
            str_s = [str_s,' & $',get_specNameLatex(spec_list{ll}),'$'];                % second fancy species name
            str_s = [str_s,' & ',num2str(mixCnst.aVib_sl(ss,ll)),' & ',num2str(mixCnst.bVib_sl(ss,ll)),' \\'];
            if ll==N_s; str_s = [str_s,' [',lineSpacing_str,']' ]; end                  % adding line-spacing for the last row of each property
            dispOrWrite(bWrite2file,fID,str_s);
        end
    end
end
dispOrWrite(bWrite2file,fID,'\hline');                                  % final horizontal line
dispOrWrite(bWrite2file,fID,'\end{tabular}');                               % close table header
dispOrWrite(bWrite2file,fID,['\caption{',caption_str,'}']);                 % caption string
dispOrWrite(bWrite2file,fID,['\label{',label_str,'}']);                     % label string
dispOrWrite(bWrite2file,fID,'\end{table*}');                                % table closing line

end % dispLatexTable_vibProp