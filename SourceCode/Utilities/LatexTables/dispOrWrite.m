function [] = dispOrWrite(bWrite2file,fID,str)
% dispOrWrite displays to screen or writes to a file depending on a boolean
%
% Usage:
%   dispOrWrite(bWrite2file,fID,str)
%
% Inputs:
%   - bWrite2file       Boolean determining wether to write to file or not
%   - fID               File identifier
%   - str               string to write or display
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if bWrite2file
    dwnspc = sprintf('\n'); % defining downspace
    fwrite(fID,[dwnspc,str]);
else
    disp(str);
end