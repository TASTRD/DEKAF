function [] = loadPropertiesAndPrintTable(savepath,spec_lists,prop_lists,label_lists,caption_lists,spacing_list,opts_lists,extraInputs)
% loadPropertiesAndPrintTable loads the species properties and prints latex
% tables.
%
% Usage:
%   (1)
%       loadPropertiesAndPrintTable(savepath,spec_lists,prop_lists,label_lists,caption_lists,spacing_list,opts_lists,extraInputs)
%
% Inputs and outputs:
%   savepath
%       string containing the path where the tables are to be saved
%   spec_lists
%       cell of cells with the list tof species for each table
%   prop_lists
%       cell of cells with the properties to be included in each table
%   label_lists
%       cell of strings with the label to be put on each table
%   caption_lists
%       cell of strings with the caption to be put on each table
%   spacing_list
%       cell of strings with the identifier of the line-spacing to be used
%       in each table
%   opts_lists
%       cell of 2D cells with the pairs of field-value of the different
%       fields in options that have to be customized. E.g.:
%           opts_lists = {'opt1',25 ;
%           |             'opt2','whatever'};
%           |--> options.opt1 = 25;
%           |--> options.opt2 = 'whatever';
%   extraInputs
%       cell of cells with the extra inputs to be passed to
%       dispLatexTable_specProp for each case
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% openinig file to save
fID = fopen(savepath,'w');
% looping tables
vars2keep = who;
for jj=1:length(spec_lists)
    keepVars(vars2keep{:},'vars2keep','jj');
    spec_list = spec_lists{jj};
    prop_list = prop_lists{jj};
    label_list = label_lists{jj};
    caption_list = caption_lists{jj};

    % Obtaining properties
    options = struct;
    for ii=1:size(opts_lists{jj},1)
        options.(opts_lists{jj}{ii,1}) = opts_lists{jj}{ii,2};
    end
    options = setDefaults(options);
    [R0,hPlanck,cLight,kBoltz,nAvogadro,pAtm,qEl,epsilon0,sigmaStefBoltz] = getUniversal_constants();
    gasSurfReac_struct = struct; % dummies, needed for getSpecies_constants
    Keq_struc = struct;

    [Mm_s,Amu_s,Bmu_s,Cmu_s,Akappa_s,Bkappa_s,Ckappa_s,Dkappa_s,Ekappa_s,muRef_s,TmuRef_s,Smu_s,kappaRef_s,TkappaRef_s,Skappa_s,R_s,...
    nAtoms_s,thetaVib_s,pCrit_s,TCrit_s,Keq_struc,thetaRot_s,L_s,sigma_s,thetaElec_s,gDegen_s,bElectron,hForm_s,HForm298K_s,hIon_s,bPos,bMol,...
    gasSurfReac_struct,AThermFuncs_s] = ...
    getSpecies_constants(spec_list,R0,hPlanck,cLight,kBoltz,nAvogadro,Keq_struc,gasSurfReac_struct,options);

    if ismember('sigmaVib_s',prop_list)
    sigmaVib_s = getSpecies_sigmaVib(spec_list,bMol,options);
    end

    if ismember('alfaSubl_s',prop_list)
        alfaSubl_s = gasSurfReac_struct.alfaSubl_s;
        PVapP_s = gasSurfReac_struct.PVapP_s;
        QVapP_s = gasSurfReac_struct.QVapP_s;
    end

    [gDegenVib_s,thetaVib_s] = getSpecies_gDegenVib(thetaVib_s);

    for ii=1:length(prop_list)
        mixCnst.(prop_list{ii}) = eval(prop_list{ii});
    end
    mixCnst.spec_list = spec_list;
    mixCnst.nAtoms_s = nAtoms_s;

    % Writing to file
    dispLatexTable_specProp(mixCnst,prop_list,'write2file',fID,'open_str','[h]','label',label_list,'caption',caption_list,'lineSpacing',spacing_list{jj},extraInputs{jj}{:});
end
fclose(fID);
