function [listOut,idx] = order4subst(listIn)
% order4subst orders a certain list, such that when substituting it for
% another, it doesn't gives problems with names containing other names.
%
% For example, if we want to substitute:
%   {'N','NO','O'}      for     {'spec1','spec2','spec3'}
%
% We definitely want to substitute 'NO' for 'spec2' before the others,
% otherwise, if we substitute 'N' first, the list would be
%   {'spec1','spec1O','O'}
%
% Examples:
%   (1)
%       listIn = {'N', 'O', 'N2', 'O2', 'NO', 'N+', 'O+', 'N2+', 'O2+', 'NO+', 'e-'}
%       --> [listOut,idx] = order4subst(listIn)
%       listOut = {'N+', 'N2+', 'N2', 'O2+', 'O2', 'NO+', 'O+', 'NO', 'O', 'N', 'e-'}
%       idx = [6, 8, 3, 9, 4, 10, 7, 5, 2, 1, 11]
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

idx = 1:length(listIn); % index vector
ii=1; % initializing counter
while ii<=length(listIn)                                            % until we don't reach the end of the list
    b_contains = ~cellfun(@isempty,strfind(listIn,listIn{ii}));         % identifying locations containing ii
    jj = find(b_contains,1,'last');                                     % obtaining last location containing ii
    if ii==jj                                                               % we are at the only location which coincides
        ii=ii+1;                                                                % increase counter
    else                                                                    % we have to move ii to that jj location
        listIn = [listIn(1:ii-1),listIn(ii+1:jj),listIn(ii),listIn(jj+1:end)];  % updating list
        idx = [idx(1:ii-1),idx(ii+1:jj),idx(ii),idx(jj+1:end)];                 % updating index
        % we don't increase the counter now, because the new ii location
        % will be another, which will also need to be sorted
    end
end

listOut = listIn;