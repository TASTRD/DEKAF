function [] = loadCollisionsAndPrintTable(savepath,spec_lists,coll_lists,order_vec,caption_list,label_list,opts_lists,extraInputs,varargin)
% loadCollisionsAndPrintTable loads the species pair's collisions and
% prints latex tables.
%
% Usage:
%   (1)
%       loadCollisionsAndPrintTable(savepath,spec_lists,coll_lists,order_vec,caption_list,label_list,opts_lists,extraInputs)
%
%   (2)
%       loadCollisionsAndPrintTable(...,'plotFits')
%       |--> outputs plots for the various fits
%
% Inputs and outputs:
%   savepath
%       string containing the path where the tables are to be saved
%   spec_lists
%       cell of cells with the list of species whose collisions will be
%       used for each table
%   coll_lists
%       cell of cells of cells with the groups of collisions for each group
%       of species to be included in each table
%   caption_lists
%       cell of cells of strings with the caption to be put on each table
%       for each
%   label_lists
%       cell of strings with the main label to be put on each group of
%       species. The collision identifier will then be appended
%   spacing_list
%       cell of strings with the identifier of the line-spacing to be used
%       in each table
%   opts_lists
%       cell of 2D cells with the pairs of field-value of the different
%       fields in options that have to be customized. E.g.:
%           opts_lists = {'opt1',25 ;
%           |             'opt2','whatever'};
%           |--> options.opt1 = 25;
%           |--> options.opt2 = 'whatever';
%   extraInputs
%       cell of cells containing any additional inputs to be passed to
%       dispLatexTable_collision for the run of each group of species.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,bPlotFit] = find_flagAndRemove('plotFits',varargin);

fID = fopen(savepath,'w');
% looping tables
vars2keep = who;
for jj=1:length(spec_lists)
    keepVars(vars2keep{:},'vars2keep','jj');
    spec_list = spec_lists{jj};

    % Obtaining properties
    options = struct;
    for ii=1:size(opts_lists{jj},1)
        options.(opts_lists{jj}{ii,1}) = opts_lists{jj}{ii,2};
    end
    options.bCollisionRawData = true;
    options = setDefaults(options);

    % define all experimental constants associated with the collision cross-sectional areas

    [mixCnst.consts_Omega11,mixCnst.consts_Omega22,mixCnst.consts_Bstar,mixCnst.consts_Cstar,mixCnst.consts_Estar,mixCnst.consts_Omega14,...
     mixCnst.consts_Omega15,mixCnst.consts_Omega24,mixCnst.consts_Dbar_binary,mixCnst.stats_fits,~] = ...
                getPair_Collision_constants(spec_list,options);

    mixCnst.spec_list = spec_list;

    % Writing to file
    for ii=1:length(coll_lists{jj})
        dispLatexTable_collision(mixCnst,coll_lists{jj}{ii},'open_str','[h]','label',[label_list{jj},coll_lists{jj}{ii}{:}],'order',order_vec{jj}(ii),'caption',caption_list{jj}{ii},'write2file',fID,extraInputs{jj}{:});
        if bPlotFit && isfield(mixCnst.stats_fits,'raw_data')
            plotFits_collision(mixCnst,coll_lists{jj}{ii}{1},extraInputs{jj}{:},'savepath','./FIGURES/');
        end
    end
end
fclose(fID);