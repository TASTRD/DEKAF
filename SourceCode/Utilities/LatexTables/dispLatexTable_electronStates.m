function [] = dispLatexTable_electronStates(mixCnst,varargin)
% dispLatexTable_electronStates prints out to screen a table in latex
% format for the degenerecies and activation temperatures of the different
% electronic states.
%
% Usage:
%   (1)
%       dispLatexTable_electronStates(mixCnst)
%       |--> displays a single table with all species
%
%   (2)
%       dispLatexTable_electronStates(mixCnst,spec_listDisp)
%       |--> displays a table with the species specified in spec_listDisp
%
%   (3)
%       dispLatexTable_electronStates(...,'write2file',fID)
%      |--> instead of displaying on the screen, it writes to the file
%      identified with fID
%
%   (4)
%       dispLatexTable_electronStates(...,'levels',vec_levels)
%      |--> allows to choose only some levels to
%
%   (5)
%       dispLatexTable_electronStates(...,'label',label)
%      |--> allows to pass a label string, with the label to be assigned to
%      the table
%
%   (6)
%       dispLatexTable_electronStates(...,'caption',caption)
%      |--> allows to pass a caption string, with the caption to be used
%      for the table's caption
%
%   (7)
%       dispLatexTable_electronStates(...,'open_str',open_str)
%      |--> allows to pass an opening string
%
%   (8)
%       dispLatexTable_electronStates(...,'autoSplit',N)
%      |--> automatically splits the table into subtables if it is longer
%      than N levels per table
%
% See also: get_specNameLatex
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% checking for the 'write2file' flag
[varargin,bWrite2file,idx] = find_flagAndRemove('write2file',varargin);
if ~isempty(idx)
    fID = varargin{idx};
    varargin(idx) = [];
else
    fID = '';
end

% checking for the 'levels' flag
[varargin,bLevels,idx] = find_flagAndRemove('levels',varargin);
if ~isempty(idx)
    lvls = varargin{idx};
    varargin(idx) = [];
end

% checking other flags
[label_str,         varargin] = parse_optional_input(varargin,'label','');
[caption_str,       varargin] = parse_optional_input(varargin,'caption','');
[open_str,          varargin] = parse_optional_input(varargin,'open_str','');
[Nsplit,            varargin] = parse_optional_input(varargin,'autoSplit',NaN);

% checking for user-specified species lists
spec_list = mixCnst.spec_list;          % extracting species list
if isempty(varargin)                    % setting default (all species)
    spec_listDisp = mixCnst.spec_list;
else                                    % reading chosen species
    spec_listDisp = varargin{1};
end
idx_disp = cellfun(@(cll)find(ismember(spec_list,cll)),spec_listDisp);  % index of the species to be displayed

% number of species and levels to print out
N_s = length(idx_disp);                                                 % number of species to display
for ii=length(idx_disp):-1:1                                            % looping chosen species
    N_lvl_ii(ii) = find(mixCnst.thetaElec_s(idx_disp(ii),:)~=0,1,'last');   % finding the number of energy levels it has
end
N_lvl = max(N_lvl_ii);                                                  % maximum number of electronic levels
if ~bLevels                                                             % if the user didn't specify what levels to display
    lvls = 0:N_lvl-1;                                                       % we display them all
end
N_lvl = length(lvls);                                                   % updating number of levels
if isnan(Nsplit)                                                        % if the number of levels at which we should split is a NaN
    Nsplit = N_lvl;                                                         % we take them all
end
% splitting
ic = 0;                                                                 % allocating counter for the groups of levels
icvrd = 0;                                                              % covered levels
while icvrd<N_lvl                                                       % looping until we have all levels covered
    ic = ic+1;                                                              % increasing counter
    if icvrd+Nsplit>=N_lvl                                                  % we reached the end
        N_endGroup = N_lvl;                                                     % the last level in the group will be the last one
    else                                                                    % we didn't reach the end
        N_endGroup = icvrd+Nsplit;                                              % we take as many levels as there are in N_split
    end
    cll_lvls{ic} = lvls(icvrd+1:N_endGroup);                                % populating cell with the group of levels
    icvrd = N_endGroup;                                                     % updating the maximum covered level
end
Nsubtable = length(cll_lvls);                                           % number of tables needed

% preparing strings
str_c = repmat('c',[1,2*N_s+1]);                                            % header string determining the number of centered columns
for ic=1:Nsubtable                                                          % looping subtables
    str_spec{ic} = '$ $   ';                                                    % initializing
    str_gOmega{ic} = '$m$   ';
    N_lvlInCll = length(cll_lvls{ic});                                          % number of levels in this cell position
    matData = zeros(N_s*2,N_lvlInCll);                                          % allocating data matrix
    for ii=1:N_s                                                                % looping species to display
        spec_name = get_specNameLatex(spec_list{idx_disp(ii)});                     % obtaining string for LaTeX
        str_spec{ic} = [str_spec{ic},' & \multicolumn{2}{c}{$',spec_name,'$} '];    % building header string with all the species names
        str_gOmega{ic} = [str_gOmega{ic},' & $g^{Elec}_{',spec_name,',m}$ [-] & $\theta^{Elec}_{',spec_name,',m}$ [K] ']; % building header string with g and theta
        matData(2*(ii-1)+1:2*ii,:) = [mixCnst.gDegen_s(idx_disp(ii),cll_lvls{ic}+1) ; mixCnst.thetaElec_s(idx_disp(ii),cll_lvls{ic}+1)]; % populating data matrix
    end
    matData = [cll_lvls{ic} ; matData];                                         % adding level ID to the data matrix
    str4sprintf = ['%d ',repmat('\t& %d \t& %0.0f ',[1,N_s]),' \t\\\\ \n'];     % string to use as an argument to populate strData with matData using sprintf
    strData{ic} = sprintf(str4sprintf,matData);                                 % arranging matData into the way we wanted to be printed
    strData{ic} = strrep(strData{ic},'& 0 ','& $ $ ');                          % substituting zeros for blanks
    idx_dwnspc = strfind(strData{ic},'\\');                                     % location of the downspaces
    strData{ic}(1:idx_dwnspc(1)) = strrep(strData{ic}(1:idx_dwnspc(1)),'$ $',' 0 '); % making sure that in the first row there are zeros (ground state)
end

% start displaying
if Nsubtable==1                                                         % no need to split
    dispOrWrite(bWrite2file,fID,['\begin{table*}',open_str]);               % table opening line
    dispOrWrite(bWrite2file,fID,'\centering');                              % table centering
    writeTable(fID,bWrite2file,str_c,str_spec{1},str_gOmega{1},strData{1}); % writing table
    dispOrWrite(bWrite2file,fID,['\caption{',caption_str,'}']);             % caption string
    dispOrWrite(bWrite2file,fID,['\label{',label_str,'}']);                 % label string
    dispOrWrite(bWrite2file,fID,'\end{table*}');                            % table closing line
else                                                                    % we need to split
    for isbtbl = 1:Nsubtable                                                % looping subtables
        dispOrWrite(bWrite2file,fID,['\begin{table*}',open_str]);               % table opening line
        dispOrWrite(bWrite2file,fID,'\centering');                              % table centering
        if isbtbl~=1                                                            % for all subtables other than the first
            dispOrWrite(bWrite2file,fID,'\ContinuedFloat');                         % continued float string
        end
        writeTable(fID,bWrite2file,str_c,str_spec{isbtbl},str_gOmega{isbtbl},strData{isbtbl}); % writing table
        dispOrWrite(bWrite2file,fID,['\caption{',caption_str,'}']);             % caption string
        if isbtbl==1                                                            % for the first subtable
            caption_str = ['(cont) ',caption_str];                                  % adding (cont) to the caption string (for all following ones)
            dispOrWrite(bWrite2file,fID,['\label{',label_str,'}']);                 % label string
        end
        dispOrWrite(bWrite2file,fID,'\end{table*}');                            % table closing line
    end
end


end % dispLatexTable_electronStates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [] = writeTable(fID,bWrite2file,str_c,str_spec,str_gOmega,strData)
% writeTable is a subfunction to dispLatexTable_electronStates printing the
% subtable to the file or to the workspace.
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

dispOrWrite(bWrite2file,fID,['\begin{tabular}{',str_c,'}']);           % table header
dispOrWrite(bWrite2file,fID,'\hline');                                 % first horizontal line
dispOrWrite(bWrite2file,fID,[str_spec,' \\']);                         % string with the names of each species
dispOrWrite(bWrite2file,fID,[str_gOmega,' \\']);                       % string with the header of the degenerecies and activation temperatures
dispOrWrite(bWrite2file,fID,'\hline');                                 % second horizontal line
dispOrWrite(bWrite2file,fID,strData);                                  % main data chunk
dispOrWrite(bWrite2file,fID,'\hline');                                 % final horizontal line
dispOrWrite(bWrite2file,fID,'\end{tabular}');                          % close table header

end % writeTable