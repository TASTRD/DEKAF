function [] = dispLatexTable_specProp(mixCnst,varargin)
% dispLatexTable_specProp prints out to screen a table in latex format for the properties of the different electronic species.
%
% Usage:
%   (1)
%       dispLatexTable_specProp(mixCnst)
%       |--> displays a single table with all the default properties:
%               - Mm_s              - L_s
%               - sigma_s           - thetaRot_s
%               - thetaVib_s        - HForm298K_s
%
%   (2)
%       dispLatexTable_specProp(mixCnst,prop_list)
%       |--> displays a only the chosen properties
%
%   (3)
%       dispLatexTable_specProp(...,'write2file',fID)
%       |--> instead of displaying on the screen, it writes to the file
%      identified with fID
%
%   (4)
%       dispLatexTable_specProp(...,'label',label)
%      |--> allows to pass a label string, with the label to be assigned to
%      the table
%
%   (5)
%       dispLatexTable_specProp(...,'caption',caption)
%      |--> allows to pass a caption string, with the caption to be used
%      for the table's caption
%
%   (6)
%       dispLatexTable_specProp(...,'open_str',open_str)
%      |--> allows to pass an opening string
%
%   (7)
%       dispLatexTable_specProp(...,'lineSpacing',lineSpacing_str)
%      |--> allows to pass an string to identify the table's inter-line
%      spacing. Eg.: '5pt'
%
%   (8)
%       dispLatexTable_specProp(...,'disp0_list',disp0_list)
%      |--> allows to pass an cell of strings containing the names of the
%      properties for which 0s must be displayed
%
%   (9)
%       dispLatexTable_specProp(...,'disp0all')
%      |--> flag to display ALL properties that have a zero value. This
%      flag dominates over 'disp0_list'
%
%   (10)
%       dispLatexTable_specProp(...,'numberFormat',numberFormat)
%      |--> allows to pass a string with the format with which the numbers
%      are to be displayed (see num2str)
%
% See also: get_specNameLatex, num2str
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

% checking flags
[varargin,bWrite2file,idx] = find_flagAndRemove('write2file',varargin);
if ~isempty(idx);   fID = varargin{idx};
                    varargin(idx) = [];
else;               fID = '';
end
[label_str,         varargin] = parse_optional_input(varargin,'label','');
[caption_str,       varargin] = parse_optional_input(varargin,'caption','');
[open_str,          varargin] = parse_optional_input(varargin,'open_str','');
[disp0_list,        varargin] = parse_optional_input(varargin,'disp0_list',{'HForm298K_s'});
[lineSpacing_str,   varargin] = parse_optional_input(varargin,'lineSpacing','0pt');
[numberFormat_str,  varargin] = parse_optional_input(varargin,'numberFormat','');
[varargin,bDispAll0]          = find_flagAndRemove('disp0all',varargin);
if isempty(numberFormat_str);       numberFormat = {};
else;                               numberFormat = {numberFormat_str};
end

% checking for custom properties
if isempty(varargin)                    % setting default (all properties)
    prop_list = {'Mm_s','L_s','sigma_s','thetaRot_s','thetaVib_s','HForm298K_s'};
else                                    % reading chosen properties
    prop_list = varargin{1};
end
Nprop = length(prop_list);
spec_list = mixCnst.spec_list;          % extracting species list
N_s = length(spec_list);

if bDispAll0                            % in case we want to display all zero values
    disp0_list = prop_list;
end

% preparing header
str_header = 'Species';
for p=1:Nprop % looping properties
    switch prop_list{p}
        case 'Mm_s';            str_header = [str_header,' & $\mathscr{M}_s$ [kg/mol]'];
        case 'L_s';             str_header = [str_header,' & $\mathcal{L}_s$ [-]'];
        case 'sigma_s';         str_header = [str_header,' & $\sigma_s$ [-]'];
        case 'thetaRot_s';      str_header = [str_header,' & $\theta^{Rot}_s$ [K]'];
        case 'gDegenVib_s';     str_header = [str_header,' & $g^{Vib}_{sm}$ [-]'];
        case 'thetaVib_s';      str_header = [str_header,' & $\theta^{Vib}_{sm}$ [K]'];
        case 'HForm298K_s';     str_header = [str_header,' & $H_{f\,s}^{298\,\mathrm{K}}$ [J/mol]'];
        case 'hIon_s';          str_header = [str_header,' & $h_s^\mathrm{ion}$ [J/kg]'];
        case 'muRef_s';         str_header = [str_header,' & $\mu_{\text{ref}\,s}$ [kg/m-s]'];
        case 'TmuRef_s';        str_header = [str_header,' & $T_{\mu_{\text{ref}}\,s}$ [K]'];
        case 'Smu_s';           str_header = [str_header,' & $S_{\mu\,s}$ [K]'];
        case 'kappaRef_s';      str_header = [str_header,' & $\kappa_{\text{ref}\,s}$ [W/K-m]'];
        case 'TkappaRef_s';     str_header = [str_header,' & $T_{\kappa_{\text{ref}}\,s}$ [K]'];
        case 'Skappa_s';        str_header = [str_header,' & $S_{\kappa\,s}$ [K]'];
        case 'sigmaVib_s';      str_header = [str_header,' & $\sigma^{Vib}_s$ [m\ts{2}]'];
        case 'Amu_s';           str_header = [str_header,' & $A^\mu_s$ [-]'];
        case 'Bmu_s';           str_header = [str_header,' & $B^\mu_s$ [-]'];
        case 'Cmu_s';           str_header = [str_header,' & $C^\mu_s$ [-]'];
        case 'Akappa_s';        str_header = [str_header,' & $A^\kappa_s$ [-]'];
        case 'Bkappa_s';        str_header = [str_header,' & $B^\kappa_s$ [-]'];
        case 'Ckappa_s';        str_header = [str_header,' & $C^\kappa_s$ [-]'];
        case 'Dkappa_s';        str_header = [str_header,' & $D^\kappa_s$ [-]'];
        case 'Ekappa_s';        str_header = [str_header,' & $E^\kappa_s$ [-]'];
        case 'alfaSubl_s';      str_header = [str_header,' & $\alpha^\text{Subl}_s$ [-]'];
        case 'PVapP_s';         str_header = [str_header,' & $P^\text{Vap}_{s}$ [K]'];
        case 'QVapP_s';         str_header = [str_header,' & $Q^\text{Vap}_{s}$ [-]'];
        otherwise
            error(['the chosen property ''',prop_list{p},''' is not supported']);
    end
end
str_header = [str_header,' \\'];

% start displaying
dispOrWrite(bWrite2file,fID,['\begin{table*}',open_str]);                   % table opening line
dispOrWrite(bWrite2file,fID,'\centering');                                  % table centering
dispOrWrite(bWrite2file,fID,['\begin{tabular}{',repmat('c',[1,Nprop+1]),'}']); % table header
dispOrWrite(bWrite2file,fID,'\hline');                                      % first horizontal line
dispOrWrite(bWrite2file,fID,str_header);                                    % displaying header
dispOrWrite(bWrite2file,fID,'\hline');                                      % second horizontal line
for s=1:N_s                                                                 % looping species
    l_spec = cellfun(@(cll)length(mixCnst.(cll)(s,mixCnst.(cll)(s,:)~=0)),prop_list); % number of fields in each species property differrent from 0
    l_spec(ismember(prop_list,disp0_list) & l_spec==0) = 1;
    lMax = max(l_spec);                                                         % number of rows we will need (maximum of fields in any property)
    for l=1:lMax                                                                % looping rows for one same species
        if l==1;    str_s = ['$',get_specNameLatex(spec_list{s}),'$'];              % fancy species name
        else;       str_s = '$ $';    end                                           % no name
        for p=1:Nprop                                                               % looping properties
            if l_spec(p)<l || ...                                                       % if we have passed the maximum number of fields of this property, we don't want anything in this row
                    (mixCnst.nAtoms_s(s)==1 && ismember(prop_list{p},{'L_s','sigma_s','thetaVib_s'}))  % ... also atomic species shouldn't have L_s, sigma_s or thetaVib_s
                str_ps = ' $ $ ';                                                           % empty string
            else                                                                        % otherwise
                str_ps = [' ',num2str(mixCnst.(prop_list{p})(s,l),numberFormat{:}),' '];    % string with the value of the property in question
            end
            str_s = [str_s,sprintf('\t'),'&',str_ps];                                   % adding tab and ampersand
        end
        str_s = [str_s,' \\'];                                                      % adding downspace
        if l==lMax; str_s = [str_s,' [',lineSpacing_str,']' ]; end                  % adding line-spacing for the last row of each property
        dispOrWrite(bWrite2file,fID,str_s);
    end
end
dispOrWrite(bWrite2file,fID,'\hline');                                  % final horizontal line
dispOrWrite(bWrite2file,fID,'\end{tabular}');                               % close table header
dispOrWrite(bWrite2file,fID,['\caption{',caption_str,'}']);                 % caption string
dispOrWrite(bWrite2file,fID,['\label{',label_str,'}']);                     % label string
dispOrWrite(bWrite2file,fID,'\end{table*}');                                % table closing line