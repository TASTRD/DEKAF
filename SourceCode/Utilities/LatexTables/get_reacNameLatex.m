function strLatex = get_reacNameLatex(strDEKAF,spec_list)
% get_reacNameLatex transforms from DEKAF's reaction naming to Latex
%
% Examples
%   (1)
%       strDEKAF = 'N + N <-> N2+ + e-'
%       spec_list = {'N','N2+','e-'}
%       --> strLatex = get_reacNameLatex(strDEKAF,spec_list)
%       strLatex = '\mathrm{N} + \mathrm{N} \leftrightarrow \mathrm{N_{2}^\text{+}} + \mathrm{e^\text{-}}'
%
% See also: get_specNameLatex, order4subst
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

N_s = length(spec_list);
% ordering species to make sure we don't substitute them wrongly
spec_list = order4subst(spec_list);
% obtaining latex names for the species
specLatex_list = cell(size(spec_list));                                     % allocating
for s=1:N_s                                                                 % looping species
    specLatex_list{s} = get_specNameLatex(spec_list{s});                        % obtaining fancy names
end
% substituting in the expressions
for s=1:N_s                                                                 % looping species
    strDEKAF = strrep(strDEKAF,spec_list{s},['spec',num2str(s)]);               % substituting DEKAF names for dummies
end
for s=N_s:-1:1                                                              % looping species
    % we do it backwards to make sure that spec11 is substituted before spec1
    strDEKAF = strrep(strDEKAF,['spec',num2str(s)],specLatex_list{s});          % substituting dummies for Latex names
end
strLatex = strrep(strDEKAF,'<->','\leftrightarrow');