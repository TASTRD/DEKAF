function strLatex = get_specNameLatex(strDEKAF,varargin)
% get_specNameLatex returns the string corresponding to the LaTeX name of
% the different species.
%
% Examples:
%   get_specNameLatex('N')      --> '\mathrm{N}'
%   get_specNameLatex('N2')     --> '\mathrm{N_2}'
%   get_specNameLatex('N2+')    --> '\mathrm{N_2^\text{+}}'
%   get_specNameLatex('N2+','noText') --> '\mathrm{N_2^{+}}'
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

[~,bNoText] = find_flagAndRemove('noText',varargin);

strLatex = ['\mathrm{',strDEKAF,'}'];                           % E.g.: \mathrm{N}
if bNoText;     strLatex = regexprep(strLatex,'(.+)(\+|-)','$1\^{$2}');   % putting +/- signs in superscript form
else;           strLatex = regexprep(strLatex,'(.+)(\+|-)','$1\^\\text{$2}');
end
strLatex = regexprep(strLatex,'(.+)([0-9])','$1_{$2}');         % putting numbers in subscript form