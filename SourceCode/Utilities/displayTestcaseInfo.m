function [] = displayTestcaseInfo(intel,options)
% displayTestcaseInfo(intel,options) displays in the workspace the all the
% relevant test information and chosen options.
%
% Author(s):    Fernando Miro Miro
% GNU Lesser General Public License 3.0

Nstr = dispNstr();

for s=intel.mixCnst.N_spec:-1:1
    if length(intel.ys_e{s})>1
ys_cell(:,s) = [pad_str(intel.mixCnst.spec_list{s},6);{intel.ys_e{s}(1);intel.ys_e{s}(end)}];
    else
ys_cell(:,s) = [pad_str(intel.mixCnst.spec_list{s},6);{intel.ys_e{s}(1)}];
    end
end

for E=intel.mixCnst.N_elem:-1:1
    if length(intel.yE_e{E})>1
yE_cell(:,E) = [pad_str(intel.mixCnst.elem_list{E},6);{intel.yE_e{E}(1);intel.yE_e{E}(end)}];
    else
yE_cell(:,E) = [pad_str(intel.mixCnst.elem_list{E},6);{intel.yE_e{E}(1)}];
    end
end
% displaying message
if options.marchYesNo
disp('--------------------------- MARCHING SOLVER ------------------------------');
disp('--------------------------------------------------------------------------');
else
disp('------------------------- SELF-SIMILAR SOLVER ----------------------------');
disp('--------------------------------------------------------------------------');
end
disp(['--- Coordinate system: ',options.coordsys]);
if length(intel.M_e)>1
disp(['--- Edge Mach number: ',num2str(intel.M_e(1)),' - ',num2str(intel.M_e(end)),' [-]']);
disp(['--- Edge unit Reynolds number: ',num2str(intel.Re1_e(1)),' - ',num2str(intel.Re1_e(end)),' [1/m]']);
disp(['--- Edge temperature: ',num2str(intel.T_e(1)),' - ',num2str(intel.T_e(end)),' [K]']);
if strcmp(options.numberOfTemp,'2T')
disp(['--- Edge vib. temperature: ',num2str(intel.Tv_e(1)),' - ',num2str(intel.Tv_e(end)),' [K]']);
end
disp(['--- Edge pressure: ',num2str(intel.p_e(1)),' - ',num2str(intel.p_e(end)),' [Pa]']);
disp(['--- Edge mass fractions: ',sprintf('\n------ %s%d - %d [-]',ys_cell{:})]);
disp(['--- Edge elemental mass fractions: ',sprintf('\n------ %s%d - %d [-]',yE_cell{:})]);
if options.specify_cp
disp(['--- User-fixed specific heat capacity (cp): ',num2str(intel.cp_e(1)),' - ',num2str(intel.cp_e(end)),' [J/kg-K]']);
end
if options.specify_gam
disp(['--- User-fixed specific heat capacity ratio (gamma): ',num2str(intel.gam_e(1)),' - ',num2str(intel.gam_e(end)),' [-]']);
end
else
disp(['--- Edge Mach number: ',num2str(intel.M_e(1)),' [-]']);
disp(['--- Edge unit Reynolds number: ',num2str(intel.Re1_e),' [1/m]']);
disp(['--- Edge temperature: ',num2str(intel.T_e(1)),' [K]']);
if strcmp(options.numberOfTemp,'2T')
disp(['--- Edge vib. temperature: ',num2str(intel.Tv_e(1)),' [K]']);
end
disp(['--- Edge pressure: ',num2str(intel.p_e(1)),' [Pa]']);
disp(['--- Edge mass fractions: ',sprintf('\n------ %s%d [-]',ys_cell{:})]);
disp(['--- Edge elemental mass fractions: ',sprintf('\n------ %s%d [-]',yE_cell{:})]);
end
if options.shockJump
    switch options.shockType
        case 'wedge'
disp(['--- Compression ramp angle: ',num2str(intel.wedge_angle),' (deg)']);
        case 'cone'
disp(['--- Cone angle: ',num2str(intel.cone_angle),' (deg)']);
        otherwise
            error(['the chosen shock type ''',options.shockType,''' is not supported']);
    end
disp(['--- Shock wave angle: ',num2str(intel.shock_angle),' (deg)']);
end
disp(['--- Cross-flow velocity: ',num2str(intel.W_0),' [m/s]']);
disp(['--- Flow assumption: ',options.flow]);
disp(['--- Turbulence model: ',options.modelTurbulence]);
disp(['--- Thermal model: ',options.modelThermal]);
disp(['--- Number of temperatures: ',options.numberOfTemp]);
disp(['--- Equation of state: ',options.EoS]);
disp(['--- Turbulence modeling: ',options.modelTurbulence]);
disp(['--- Mixture: ',options.mixture]);
disp(['------ Species: {',strjoin(intel.mixCnst.spec_list,', '),'}']);
if strcmp(options.flow,'CNE')
disp(sprintf(['------ Reactions: \n------ ',strjoin(intel.mixCnst.reac_list,'\n------ ')])); %#ok<DSPS>
end
disp(['--- Transport model: ',options.modelTransport]);
if options.cstPr
disp(['--- Constant Prandtl number: ',num2str(intel.mixCnst.Pr)]);
end
switch options.modelTransport
    case 'Sutherland'
disp(['------- mu_ref: ',num2str(intel.mixCnst.mu_ref),' [kg/m-s]']);
disp(['------- Su_mu: ',num2str(intel.mixCnst.Su_mu),' [K]']);
disp(['------- T_ref: ',num2str(intel.mixCnst.T_ref),' [K]']);
    if ~options.cstPr
disp(['------- kappa_ref: ',num2str(intel.mixCnst.kappa_ref),' [W/m-K]']);
disp(['------- Su_kappa: ',num2str(intel.mixCnst.Su_kappa),' [K]']);
    end
    case {'BlottnerEucken','powerLaw','SutherlandWilke','SutherlandEuckenWilke'}
        % nothing to display
    otherwise % all other models use collision data
disp(['------- Collision model (Neut-Neut): ',options.modelCollisionNeutral]);
disp(['------- Collision model (Charge-charge): ',options.modelCollisionChargeCharge]);
end
if sum(strcmp(options.flow,{'TPGD','LTE','CNE','LTEED'})) % there will be diffusion
    if options.molarDiffusion
        strMolar = '(molar)';
    else
        strMolar = '(mass)';
    end
disp(['--- Diffusion model ',strMolar,': ',options.modelDiffusion]);
end
if sum(strcmp(options.flow,{'LTE','CNE','LTEED'})) % there will be chemistry
disp(['--- Chemistry equilibrium model: ',options.modelEquilibrium]);
end
if ~strcmp(options.flow,'CPG') % there will be vibrational and electronic energy
disp(['--- Species energy mode data coming from: ',options.modelEnergyModes]);
end
if strcmp(options.thermal_BC,'H_F') % condition on the wall heat flux
    if options.G_bc==0
    disp('--- Adiabatic wall boundary condition');
    else
    disp(['--- Energy boundary condition on the gradient of g (non-dimensional dynamic enthalpy): ',num2str(options.G_bc)]);
    end
elseif strcmp(options.thermal_BC,'gwall') % condition on the wall enthalpy
    disp(['--- Energy boundary condition on g (non-dimensional dynamic enthalpy): ',num2str(options.G_bc)]);
elseif strcmp(options.thermal_BC,'Twall') % condition on the wall enthalpy
    if options.marchYesNo
    disp(['--- Energy boundary condition on the temperature: ',num2str(options.trackerTwall(1)),'-',num2str(options.trackerTwall(end)),' [K]']);
    else
    disp(['--- Energy boundary condition on the temperature: ',num2str(options.trackerTwall(1)),' [K]']);
    end
elseif strcmp(options.thermal_BC,'ablation') % condition on the wall enthalpy
    disp('--- Ablation surface energy balance boundary condition on the temperature');
end
if strcmp(options.numberOfTemp,'2T') && strcmp(options.thermalVib_BC,'H_F') % condition on the wall heat flux
    if options.G_bc==0
    disp('--- Adiabatic wall boundary condition on Tv');
    else
    disp(['--- Energy boundary condition on the gradient of gv (non-dimensional vib-elec-el enthalpy): ',num2str(options.Gv_bc)]);
    end
elseif strcmp(options.numberOfTemp,'2T') && strcmp(options.thermalVib_BC,'gwall') % condition on the wall enthalpy
    disp(['--- Vibrational energy boundary condition on gv (non-dimensional vib-elec-el enthalpy): ',num2str(options.Gv_bc)]);
elseif strcmp(options.numberOfTemp,'2T') && ismember(options.thermalVib_BC,{'Twall','frozen'}) % condition on the wall enthalpy
    if options.marchYesNo
    disp(['--- Vibrational energy boundary condition on the vib. temperature: ',num2str(options.trackerTvwall(1)),'-',num2str(options.trackerTvwall(end)),' [K]']);
    else
    disp(['--- Vibrational energy boundary condition on the vib. temperature: ',num2str(options.trackerTvwall(1)),' [K]']);
    end
elseif strcmp(options.numberOfTemp,'2T') && strcmp(options.thermalVib_BC,'ablation') % condition on the wall enthalpy
    disp('--- Ablation surface energy balance boundary condition on the vibrational temperature');
end
switch options.wallBlowing_BC
    case 'none'
    disp('--- Zero-wall-blowing boundary condition');
    case 'SS'
    disp(['--- Self-similar-blowing boundary condition, with f_w = ',num2str(options.F_bc)]);
    case 'Vwall'
        if options.marchYesNo
    disp(['--- Blowing boundary condition on the velocity, with V_w = ',num2str(options.trackerVwall(1)),'-',num2str(options.trackerVwall(end)),' [m/s]']);
        else
    disp(['--- Blowing boundary condition on the velocity, with V_w = ',num2str(options.F_bc),' [m/s]']);
        end
    case 'mwall'
        if options.marchYesNo
    disp(['--- Blowing boundary condition on the mass flow, with m_w = ',num2str(options.trackerVwall(1)),'-',num2str(options.trackerVwall(end)),' [kg/s-m^2]']);
        else
    disp(['--- Blowing boundary condition on the mass flow, with m_w = ',num2str(options.trackerVwall(1)),' [kg/s-m^2]']);
        end
    case 'ablation'
    disp('--- Ablative boundary condition determining injected mass flow');
    otherwise
        error(['the chosen wallBlowing_BC ''',options.wallBlowing_BC,''' is not supported']);
end

if ismember(options.flow,{'TPGD','CNE'})
    switch options.wallYs_BC
        case 'nonCatalytic'
            disp('--- Non-catalytic wall boundary condition');
        case 'ysWall'
            ys_str0 = strjoin(cellfun(@(cll)num2str(cll(1)),  options.Ys_bc,'UniformOutput',false),' , ');
            ys_str1 = strjoin(cellfun(@(cll)num2str(cll(end)),options.Ys_bc,'UniformOutput',false),' , ');
            if strcmp(ys_str1,ys_str0)
                ys_str = ['{',ys_str1,'}'];
            else
                ys_str = ['{',ys_str0,'} - {',ys_str1,'}'];
            end
            disp(['--- Fixed wall mass fractions {',strjoin(intel.mixCnst.spec_list,','),'}: ',ys_str]);
        case 'catalyticGamma'
            disp('--- Surface mass fractions obtained from the catalytic gamma model, with reactions:');
            NstrCat = max(cellfun(@length,intel.mixCnst.gasSurfReac_struct.catReac_list));
            for r=1:length(intel.mixCnst.gasSurfReac_struct.catReac_list)
                disp(['------ ',pad(intel.mixCnst.gasSurfReac_struct.catReac_list{r},NstrCat+1),'gammaCat=',num2str(options.Ys_bc(r)),' [-]']);
            end
        case 'catalyticK'
            disp('--- Surface mass fractions obtained from the catalytic K model, with reactions:');
            NstrCat = max(cellfun(@length,intel.mixCnst.gasSurfReac_struct.catReac_list));
            for r=1:length(intel.mixCnst.gasSurfReac_struct.catReac_list)
                disp(['------ ',pad(intel.mixCnst.gasSurfReac_struct.catReac_list{r},NstrCat+1),'KCat=',num2str(options.Ys_bc(r)),' [m/s]']);
            end
        case 'ablation'
            disp('--- Surface mass fractions obtained from an ablative wall boundary condition, with reactions:');
            disp(['------ ',strjoin(intel.mixCnst.gasSurfReac_struct.gasSurfReac_list,[newline,'------ '])]);
        otherwise
            error(['the chosen wallYs_BC ''',options.wallYs_BC,''' is not supported']);
    end
elseif ismember(options.flow,{'LTEED'})
    switch options.wallYE_BC
        case 'zeroYEgradient'
            disp('--- Zero-elemental-mass-fraction-gradient boundary condition');
        case 'yEWall'
            yE_str0 = strjoin(cellfun(@(cll)num2str(cll(1)),  options.YE_bc,'UniformOutput',false),' , ');
            yE_str1 = strjoin(cellfun(@(cll)num2str(cll(end)),options.YE_bc,'UniformOutput',false),' , ');
            if strcmp(yE_str1,yE_str0)
                yE_str = ['{',yE_str1,'}'];
            else
                yE_str = ['{',yE_str0,'} - {',yE_str1,'}'];
            end
            disp(['--- Fixed wall elemental mass fractions {',strjoin(intel.mixCnst.elem_list,','),'}: ',yE_str]);
        otherwise
            error(['the chosen wallYE_BC ''',options.wallYE_BC,''' is not supported']);
    end
end

disp(['--- Wall-normal domain range eta [-]: (0 -> ',num2str(options.L),')']);
disp(['--- Number of points, eta: ',num2str(options.N)]);
disp(['--- Mapping parameter eta_i: ',num2str(options.eta_i)]);
if options.marchYesNo
disp(['--- Streamwise domain range x [m]: (',num2str(intel.xPhys(1,1)),' -> ',num2str(intel.xPhys(1,end)),')']);
disp(['--- Number of points, x: ',num2str(options.mapOpts.N_x)]);
disp(['--- Streamwise point distribution: ',num2str(options.xSpacing)]);
disp(['--- Marching finite-difference order: ',num2str(options.ox)]);
end
disp('--------------------------------------------------------------------------');
