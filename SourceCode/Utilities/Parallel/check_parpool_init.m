function started_parpool = check_parpool_init(N_thread)
%CHECK_PARPOOL_INIT Check if a parpool is currently running in MATLAB
% If so, do nothing. If not, start it up with a user-specified number of
% physical threads, N_thread.
%
% tip: to access your physical cores available, use the
% undocumented capability, feature('numcores').
% see https://stackoverflow.com/a/2705675/11141655 for details
%
% Author(s): Ethan Beyak

started_parpool = false;

if isempty(gcp('nocreate'))
    parpool(N_thread);
    started_parpool = true;
end

end % check_parpool_init
