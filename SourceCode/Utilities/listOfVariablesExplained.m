function [] = listOfVariablesExplained()
% LISTOFVARIABLESEXPLAINED() is a fake function used to have a
% documentation of what each of the terms in DEKAF is.
%
%  FLOW-FIELD VARIABLES
%    - u: streamwise velocity [m/s]
%    - w: spanwise velocity [m/s]
%    - T: static equilibrium (1T) or trans-rot (2T) temperature [K]
%    - Tv: vibrational-electronic-electron temperature [K]
%    - p: static pressure [Pa]
%    - rho: mixture density [kg/m^3]
%    - ys: species mass fraction [-]
%    - rhos: species partial density [kg/m^3]
%    - h: Static enthalpy [J/kg]
%    - htot: Total enthalpy [J/kg]
%
%  FLOW-FIELD GRADIENTS (of whatever variable Q)
%    - dQ_dy: first derivative wrt the wall-normal distance (y) [[Q]/m]
%    - dQ_dy2: second derivative wrt the wall-normal distance (y) [[Q]/m^2]
%    - dQ_dx: first derivative wrt the streamwise distance (x) [[Q]/m]
%    - dQ_dx2: second derivative wrt the streamwise distance (x) [[Q]/m^2]
%    - dQ_dxdy: second derivative wrt the streamwise distance (x) [[Q]/m^2]
%
%  EDGE QUANTITIES
%    - U_e: streamwise velocity [m/s]
%    - W_0: spanwise velocity [m/s]
%    - T_e: static temperature [m/s]
%    - p_e: static pressure [Pa]
%    - rho_e: mixture density [kg/m^3]
%    - ys_e: species mass fraction [-]
%    - mu_e: dynamic viscosity [kg/m-s]
%    - kappa_e: thermal conductivity [W/m-K]
%    - h_e: static enthalpy [J/kg]
%    - H_e: dynamic enthalpy [J/kg]
%    - gam_e: mixture heat capacity ratio [-]
%    - R_e: mixture gas constant [J/kg-K]
%    - cp_e: heat capacity at constant pressure [J/kg-K]
%    - a_e: speed of sound [m/s]
%    - Ecu: Eckert number based on U_e [-]
%    - Ecw: Eckert number based on W0 [-]
%    - beta: non-dimensional pressure gradient Hartree parameter [-]
%    - Theta: non-dimensional total enthalpy gradient Hartree parameter [-]
%    - theta: non-dimensional static enthalpy gradient Hartree parameter [-]
%    - Sc_e: Schmidt number [-]
%    - M_e: Mach number [-]
%    - Pr: Prandtl number [-]
%    - Re1_e: unit Reynolds number [-]
%    - U_e0, rho_e0, etc: quantities at the BL edge at the first xi location
%
%  NON-DIMENSIONAL or COMPUTATIONAL VARIABLES
%    - eta: wall-normal self-similar coordinate. Its differential relation
%    is    deta = U_e/sqrt(2*xi) * rho * dy
%    - xi: streamwise coordinate. Its differential relation is:
%           dxi = rho_e * mu_e * U_e * x
%    - f: non-dim. stream function. It is related to the streamwise
%    velocity through df_deta = u
%    - g: non-dim. TOTAL enthalpy H/H_e       ¡¡¡ ATTENTION !!!
%    - k: non-dim. spanwise velocity w/W_0
%    - ys: species mass fractions
%    - C: product of the non-dim. density and dynamic viscosity
%    - a1: C divided by the Prandtl number
%    - a2: C times (one minus one divided by the Prandtl number)
%    - gs: non-dim. species enthalpies hs/h_e
%    - zetas: ratio between the mixture and species molar weight Mm/Mm_s
%    - Scs: species schmidt number, representative of the inverse of the
%    non-dimensional diffusion coefficient mu/rho-D_s
%    - Scsl: same as Scs but for paired diffusion theories: mu/rho-D_sl
%    - j: inverse of the non-dim. mixture density rho_e/rho
%    - Omegas: scaled species production rate [weird units]
%
%  FLOW CHARACTERISTICS AND CONSTANTS
%    - R0:              Universal gas constant  8.3144598 J/(K-mol).
%    - hplanck:         Planck's constant       6.626070040e-34 J*s.
%    - cLight:          Speed of light          299792458 m/s
%    - kBoltz:          Boltzmann constant      1.38064852e-23 m^2-kg/K-s^2
%    - nAvogadro:       Avogadro constant       6.022140857e23 particles / mole
%    - pAtm:            STP Pressure constant   1.01325e5 Pa
%    - qEl              elementary charge       1.6021766208981e-19 C
%    - epsilon0         permittivity of vacuum  8.854187817620e-12 C^2/N-m^2
%    - sigmaStefBoltz   Stefan-Boltzmann cst.   5.67036713e-8 W/m^2-K^4
%    - nu_reac, nu_prod: stoichiometric coefficients of reactants and
%    products [-]
%    - R_s: species gas constant [J/kg-K]
%    - A_r: reaction rate preexponential constant [(m^3/mol)^sum(nu_reac) * K^(1/nT_r)]
%    - nT_r: reaction rate exponential constant [-]
%    - theta_r: reaction activation temperature [K]
%    - Mm_s: species molar weight [kg/mol]
%    - Amu_s, Bmu_s, Cmu_s: coefficients in Blottner's curve for the
%    species viscosity [-]
%    - nAtoms_s: number of atoms in the species' molecule [-]
%    - thetaRot_s: molecular species' rotational activation temperature [K]
%    - thetaVib_s: molecular species' vibrational activation temperature [K]
%    - thetaElec_s: species activation temperature of the different
%    electronic states [K]
%    - gDegen_s: degeneracies of the different electronic states [-]
%    - hForm_s: enthalpy of formation of each species [J/kg]
%    - I_s: molecule's moment of inertia about its center of mass [kg*m^2]
%    - L_s: linearity of a molecule (2 for linear like NO or N2, 3 for
%    non-linear like CH4) [-]
%    - sigma_s: steric factor of a molecule (1 for non-symmetric like NO, 2
%    for symmetric like N2) [-]
%    - mu_ref, Su_mu, T_ref, kappa_ref, Su_kappa: coefficients of
%    sutherland's law for viscosity and thermal conductivity [kg/m-s, K, K,
%    W/m-K, K].
%    - consts_Omega11: Gupta Yos curve fit coefficients to calculate
%    Omega11_sl, the average collision cross-sectional area of two-body
%    pair, species s & l, order (1,1). struct with fields .A, .B, .C, .D,
%    where each field is a matrix of size (N_s x N_l).
%    - consts_Omega22: Gupta Yos curve fit coefficients to calculate
%    Omega22_sl, the average collision cross-sectional area of two-body
%    pair, species s & l, order (2,2). struct with fields .A, .B, .C, .D,
%    where each field is a matrix of size (N_s x N_l).
%    - consts_Bstar: Gupta Yos curve fit coefficients to calculate
%    Bstar, for a given species pair (s,l). struct with fields .A, .B, .C,
%    .D, where each field is a matrix of size (N_s x N_l).
%
%  INTERMEDIATE VARIABLES OF INTEREST
%    - D_s, D_sl: diffusion coefficients for species-mixture diffusion
%    theories and species-species theories respectively. [m^2/s]
%    - cp: heat capacity at constant pressure [J/kg-K]
%    - cv: heat capacity at constant volume [J/kg-K]
%    - Q: partition functions [-]
%    - gibbs: species gibbs free energy [-]
%    - Mm: mixture molar mass [kg/mol]
%    - lnkf_r: natural logarithm of the forward reaction constant [weird]
%    - lnkb_r: natural logarithm of the backward reaction constant [weird]
%    - lnKeq_r: natural logarithm of the equilibrium reaction constant [weird]
%    - deltaG_r: jump in the gibbs free energy due to reaction r [J/mol]
%    - *Trans: relative to the translational energy mode
%    - *Rot: relative to the rotational energy mode
%    - *Vib: relative to the vibrational energy mode
%    - *Elec: relative to the electronic energy mode
%    - *el: relative to the translational energy mode of electrons
%    - lnOmega11_sl: natural logarithm of the collision cross-sectional
%    area of order (1,1) of pair (s,l). Defined by GuptaYos 1990, Eqn (22)
%    - lnOmega22_sl: natural logarithm of the collision cross-sectional
%    area of order (2,2) of pair (s,l). Defined by GuptaYos 1990, Eqn (22)
%    - lnDelta1_sl: natural logarithm of the collision cross-sectional
%    area weighted by the reduced-pair-mass of order 1 of pair (s,l).
%    Defined by GuptaYos 1990, Eqn (34)
%    - lnDelta2_sl: natural logarithm of the collision cross-sectional
%    area weighted by the reduced-pair-mass of order 2 of pair (s,l).
%    Defined by GuptaYos 1990, Eqn (35)
%    - lnBstar_sl: natural logarithm of the ratio of mixed-order, collision
%    cross-sectional areas for all pairs (s,l) of species.
%    Bstar is defined by (5*Omega12 - 4*Omega13)/Omega11
%    - Tstar: reduced temperature for charged collisions of each pair (s,l)
%    defined in Fertig 2000, Eqn (8) [K]
%    - lambdaD: Debye length. defined in Fertig 2000, Eqn (6) [m]
%
%  OTHERS
%    - N: number of points in the eta direction
%    - N_x: number of points in the xi direction
%    - D1, D2: derivation matrices with respect to eta
%    - D1_xi, D2_xi, D1_x, D1_y, etc: derivation matrices with respect to
%    other variables
%    - I1_eta, I1_xi, etc: derivation matrix over a variable
%    - dq_deta, dq_dx, etc: first derivative of a veriable q with a
%    variable eta, x, etc.
%    - dq_deta2, dq_dx2, etc: second derivative of a veriable q with a
%    variable eta, x, etc twice.
%    - dq_detadxi: crossed derivative of a variable with eta and xi.
%
% References:
%   () Fertig, M., Dohr, A., & Frühauf, H.-H. (2001). Transport
%       Coefficients for High Temperature Nonequilibrium Air Flows. Journal
%       of Thermophysics and Heat Transfer, 15(2), 148–156.
%       https://doi.org/doi:10.2514/6.1998-2937
%   () Gupta, R. N., Yos, J. M., Thompson, R. A., & Lee, K.-P. (1990). A
%       review of Reaction Rates and Thermodynamic and Transport Properties
%       for an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%       Calculations to 30000K.
%
% For detailed explanation on the options and their defaults see also
% setDefaults.m
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
% GNU Lesser General Public License 3.0