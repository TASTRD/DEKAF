function [] = dispif(string,condition)
% dispif displays a string only if a certain condition is satisfied
%
% Usage:
%   dispif(string,condition);
%
% INPUT
%  - string:    string to be displayed
%  - condition: boolean to determine if the string must be displayed or
%  not.
%
% Author: Fernando Miro Miro
% Date: September 2015

if condition
    disp(string);
end

end % dispif.m
