function displayEpilog(varargin)
% displayEpilog displays the epilog of DEKAF
%
% This is typically called either at initialization or upon completion of a
% major calculation
%
% Usage:
%   (1) displayEpilog();
%
% --------------------------------------------------------------------------
% ------------------------- Version DEKAF_2018Goat -------------------------
% ----------------- GNU Lesser General Public License 3.0 ------------------
% --------------------------------------------------------------------------
%
%   (2) displayEpilog('Computed with');
%
% --------------------------------------------------------------------------
% -------------------------- Computed with DEKAF ---------------------------
% ------------------------- Version DEKAF_2018Goat -------------------------
% ----------------- GNU Lesser General Public License 3.0 ------------------
% --------------------------------------------------------------------------
%
% Author: Ethan Beyak
% Date: September 2019
% GNU Lesser General Public License 3.0

[~,computed_with] = find_flagAndRemove('Computed with',varargin);

disp(repmat('-',[1,dispNstr()]));
if computed_with
    disp(centerStr('Computed with DEKAF',dispNstr()));
end
disp(centerStr(['Version ',DEKAFversion()],dispNstr()));
disp(centerStr('GNU Lesser General Public License 3.0',dispNstr()));
disp(repmat('-',[1,dispNstr()]));

end % displayEpilog
