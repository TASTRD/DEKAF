function [xi,dxi_dx] = eval_illingworth(x,U_e,rho_e,mu_e,Dx)
% eval_illingworth evaluates the illingworth transformation
%
% Usage:
%   (1)
%       [xi,dxi_dx] = eval_illingworth(x,U_e,rho_e,mu_e,Dx)
%
% Inputs and outputs:
%   x
%       streamwise position                 [m]             (size Nx x 1)
%   U_e
%       streamwise edge velocity            [m/s]           (size Nx x 1)
%   rho_e
%       streamwise density                  [kg/m^3]        (size Nx x 1)
%   mu_e
%       streamwise dynamic viscosity        [kg/m-s]        (size Nx x 1)
%   Dx
%       streamwise differentiation matrix   [1/m]           (size Nx x Nx)
%   xi
%       transformed marching variable       [kg^2/m^2-s^2]  (size Nx x 1)
%   dxi_dx
%       derivative of xi with x             [kg^2/m^3-s^2]  (size Nx x 1)
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

Ix = integral_mat(Dx,'ud');
% It is now time to scale the x vector to the computational grid (xi)
dxi_dx = U_e(:).*rho_e(:).*mu_e(:);                         % column vector
xi = Ix*dxi_dx + U_e(1).*rho_e(1).*mu_e(1)*x(1);            % column vector

end % eval_illingworth