function [intel,xi,xi_ref,xi_min,xi_max,x,xPhys,U_e,rho_e,p_e,T_e,H_e,h_e,mu_e] = ...
    getXi_QSS(intel,options,beta,Theta,U_e,W_0,rho_e,p_e,T_e,Tv_e0,H_e,h_e,mu_e,ys_e,varargin)
% getXi_QSS computes the marching variable xi in self-similar conditions.
%
% Usage (basic):
%   (1.1)
%       [intel,xi,xi_ref,xi_min,xi_max,x,U_e,rho_e,p_e,T_e,H_e,h_e,mu_e] = ...
%             getXi_QSS(intel,options,beta,Theta,U_e,W_0,rho_e,p_e,T_e,Tv_e0,H_e,h_e,mu_e,ys_e)
%
%   (1.2)   options.flow = 'LTEED'
%       [...] = getXi_QSS(intel,options,beta,Theta,U_e,W_0,rho_e,p_e,T_e,Tv_e0,H_e,h_e,mu_e,XE_e)
%
% Usage (for self-similar cases with a non-zero beta):
%   (2)
%       [...] = getXi_QSS(...,'x_ref',x_ref)
%       |--> one can fix the value of x for which the edge conditions
%       correspond to the provided *_e. Only supported if
%       options.dimXQSS=true or options.fixReQSS=true
%   (3)
%       [...] = getXi_QSS(...,'Re_ref',Re_ref)
%       |--> one can fix the value of the Reynolds number for which the
%       edge conditions correspond to the provided *_e. Only supported if
%       options.fixReQSS=true
%   (4)
%       [...] = getXi_QSS(...,'xi_ref',xi_ref)
%       |--> one can fix the value of xi for which the edge conditions
%       correspond to the provided *_e. Only supported if
%       options.dimXQSS=false or options.fixReQSS=false
%
% EXPLANATION for usages 2-4:
%   When working with self-similar solutions with a non-zero beta or Theta,
% it is necessary to specify a reference streamwise position, which
% corresponds to the location at which the edge conditions (U_e, T_e, etc.)
% are equal to the values passed by the user.
%   This is simply a consequence of the fact that, in such cases, the U_e
% profile, for instance, is equal to (see White1991):
%
%       U_e(xi) = U_ref * (xi/xi_ref)^(beta/2)
%
%   The user provides U_ref through intel.U_e, but it is also necessary to
% provide xi_ref. Alternatively one can also provide the streamwise
% position through the physical streamwise position (x_ref) or the Reynolds
% number (Re_ref). From them, one can then retrieve xi_ref.
%
% Inputs and outputs:
%   intel
%       structure containing flow fields which must contain:
%        .x
%           if options.dimXQSS=true. Streamwise positions at which the
%           profiles are to be evaluated
%        .Re
%           if options.fixReQSS=true. Reynolds numbers at which the
%           profiles are to be evaluated
%        .xi
%           if options.dimXQSS=false and options.fixReQSS=false. Marching
%           variable values at which the profiles are to be evaluated
%        .cone_angle
%           if options.coordsys='cone'. cone angle [deg]
%        .mixCnst
%           structure containing the mixture constants
%       Several fields are redefined in it, such as: xi, U_e, rho_e, p_e,
%       T_e, H_e, h_e, mu_e, ys_e, Re1_e, M_e, gam_e, R_e, Ecu, Ecw
%   options
%       structure containing options
%   xi
%       marching xi variable [kg^2/m^2-s^2]
%   xi0, xi_min, xi_max
%       reference, minimum and maximum marching xi variable [kg^2/m^2-s^2]
%   x
%       streamwise x variable (after Probstein-Elliot transformation if
%       needed) [m]
%   xPhys
%       streamwise physical x variable (without Probstein-Elliot
%       transformation) [m]
%   beta
%       velocity Hartree parameter [-]
%   Theta
%       total enthalpy Hartree parameter [-]
%   U_e
%       edge velocity [m/s]
%   rho_e
%       edge density [kg/m^3]
%   p_e
%       edge pressure [Pa]
%   T_e
%       edge temperature [K]
%   Tv_e0
%       edge vibrational-electronic-electron temperature at the reference
%       point x0 (vibrational temperature in QSS must be constant, so it'll
%       be the same everywhere [K]
%   H_e
%       edge dynamic enthalpy [J/kg]
%   h_e
%       edge static enthalpy [J/kg]
%   mu_e
%       edge dynamic viscosity [kg/m-s]
%   ys_e
%       edge mass fraction [-]
%   XE_e
%       edge elemental mole fraction [-]
%
% Author: Fernando Miro Miro
% Date: June 2019
% GNU Lesser General Public License 3.0

%---------------------------- DEVELOPERS NOTES ---------------------------- 
%-- 1. ys_e is actually XE_e for options.flow = 'LTEED'
%--------------------------------------------------------------------------

H_e0    = H_e(1);       % reference values
U_e0    = U_e(1);
p_e0    = p_e(1);
rho_e0  = rho_e(1);
mu_e0   = mu_e(1);
ys_e0   = cellfun(@(cll)cll(1),ys_e,'UniformOutput',false);
if ~isfield(intel,'cylCoordFuncs')
    error(['Error: there was no field in intel called cylCoordFuncs. ',newline,...
        'This is most-likely because the DEKAF solution was generated before modifying the coordinate-system transformations. ',newline,...
        'In order to generate it, one must simply run:',newline,newline,...
        '  intel.cylCoordFuncs = populateProbsteinElliotFunctions(intel,options);']);
end

if options.dimXQSS              % we must define xi from x
    xPhys = intel.x;
    xPhys_ref   = parse_optional_input(varargin,'x_ref',xPhys(1));
    x     = get_xyTransformed(xPhys,    [],options.coordsys,intel.cylCoordFuncs);
    x_ref = get_xyTransformed(xPhys_ref,[],options.coordsys,intel.cylCoordFuncs);
    if options.bVaryingEdgeQSS
        [xi,xi_ref,xi_max,xi_min,p_e,rho_e,T_e,U_e,h_e,H_e,mu_e,ys_e] = getFalknerSkan_xi(x,beta(1),Theta(1),x_ref,H_e0,U_e0,p_e0,rho_e0,mu_e0,ys_e0,T_e(1),Tv_e0,intel.mixCnst,intel,options);
    else
        xi = U_e .* mu_e .* rho_e .* x;
        xi_ref = xi;        xi_min = min(xi);       xi_max = max(xi);
    end
    intel.xi = xi;
elseif options.fixReQSS         % we must define x from Re, and then get xi
    Re = intel.Re;
    bConv = false;
    tol = options.tol; x_prev=Inf; it=1; itMax=options.T_itMax;
    while ~bConv && it<itMax
        it=it+1;
        Q_e = sqrt(U_e.^2 + W_0^2);
        xPhys = Re.^2.*mu_e./(rho_e.*Q_e);
        Re_ref  = parse_optional_input(varargin,'Re_ref',Re(1));
        xPhys_ref   = interp1_noInf(Re,xPhys,Re_ref,'spline');
        xPhys_ref   = parse_optional_input(varargin,'x_ref', xPhys_ref);
        x     = get_xyTransformed(xPhys,    [],options.coordsys,intel.cylCoordFuncs);
        x_ref = get_xyTransformed(xPhys_ref,[],options.coordsys,intel.cylCoordFuncs);
        if options.bVaryingEdgeQSS
            [xi,xi_ref,xi_max,xi_min,p_e,rho_e,T_e,U_e,h_e,H_e,mu_e,ys_e] = getFalknerSkan_xi(x,beta(1),Theta(1),x_ref,H_e0,U_e0,p_e0,rho_e0,mu_e0,ys_e0,T_e(1),Tv_e0,intel.mixCnst,intel,options);
        else
            xi = U_e .* mu_e .* rho_e .* x;
            xi_ref = xi;        xi_min = min(xi);       xi_max = max(xi);
        end
        bConv = ~any(abs(1-x_prev./x)>tol);
        disp(['Finding x for chosen Re. it=',num2str(it),' --> err = ',num2str(max(abs(1-x_prev./x)))]);
        x_prev = x;
    end
    intel.xi = xi;
elseif isfield(intel,'xi')      % xi is given directly
    xi      = intel.xi;
    xi_ref  = parse_optional_input(varargin,'xi_ref',xi(1));
    xi_max  = max(xi);
    xi_min  = min([xi(:);xi_max/100]);
    [~,U_e,rho_e,p_e,T_e,mu_e,y_se,h_e,H_e] = getEdge_FalknerSkan(xi,xi_ref,beta(1),Theta(1),U_e(1),p_e(1),H_e(1),ys_e0,p_e,T_e,Tv_e0,intel.mixCnst,intel,options);
    ys_e    = matrix2D2cell1D(y_se);
else
    error('neither options.dimXQSS was true with intel.x, nor options.fixReQSS was true with intel.Re, nor was there intel.xi. A streamwise reference is needed to dimensionalize the SS profile');
end

% throwing it back into intel so that it is also outputted
intel.U_e   = U_e;          intel.rho_e = rho_e;
intel.p_e   = p_e;          intel.T_e   = T_e;
intel.H_e   = H_e;          intel.h_e   = h_e;
intel.mu_e  = mu_e;         intel.ys_e  = ys_e;
intel.Tv_e  = repmat(Tv_e0,size(T_e));
Q_e         = sqrt(U_e.^2 + W_0^2);
intel.Re1_e = rho_e.*Q_e./mu_e;
[M_e,gam_e,R_e] = getEdge_Mach(T_e.',cell1D2matrix2D(ys_e),Q_e.',intel.mixCnst,options,intel,intel.Tv_e.');
intel.M_e   = M_e.';
intel.gam_e = gam_e.';
intel.R_e   = R_e.';
intel.Ecu   = U_e.^2./h_e;
intel.Ecw   = W_0.^2./h_e;

end % getXi_QSS