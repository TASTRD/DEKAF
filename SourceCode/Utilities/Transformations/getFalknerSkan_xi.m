function [xi,xi0,xi_max,xi_min,p_e,rho_e,T_e,U_e,h_e,H_e,mu_e,ys_e] = getFalknerSkan_xi(x,beta,Theta,x0,H_e0,U_e0,p_e0,rho_e0,mu_e0,ys_e,T_e0,Tv_e0,mixCnst,intel,options)
% getFalknerSkan_xi passes from the physical x to the computational xi
% variable for self-similar profiles with a pressure gradient (Falkner-Skan
% situation).
%
% Usage:
%   (1)
%       [xi,xi0,xi_max,xi_min,p_e,rho_e,T_e,U_e,h_e,H_e,mu_e,ys_e] = getFalknerSkan_xi(x,beta,Theta,...
%                               x0,H_e0,U_e0,p_e0,rho_e0,mu_e0,ys_e,T_e0,Tv_e0,mixCnst,intel,options)
%
%   (2)     options.flow = 'LTEED'
%       [...] = getFalknerSkan_xi(x,beta,Theta,x0,H_e0,U_e0,p_e0,rho_e0,mu_e0,XE_e,T_e0,Tv_e0,mixCnst,intel,options)
%
% Inputs and outputs:
%   xi
%       marching xi variable [kg^2/m^2-s^2]
%   x
%       physical dimensional x variable [m]
%   beta
%       velocity Hartree parameter [-]
%   Theta
%       total enthalpy Hartree parameter [-]
%   x0
%       physical x variable at the reference point [m]
%   xi0
%       marching xi variable at the reference point x0 [kg^2/m^2-s^2]
%   xi_max, xi_min
%       maximum and minimum value of xi used to build the xi vector [kg^2/m^2-s^2]
%   H_e0
%       edge dynamic enthalpy at the reference point xi0 [K]
%   U_e0
%       edge velocity at the reference point x0 [m/s]
%   p_e0
%       edge pressure at the reference point x0 [Pa]
%   rho_e0
%       edge density at the reference point x0 [kg/m^3]
%   mu_e0
%       edge viscosity at the reference point x0 [kg/m-s]
%   ys_e
%       edge mass fraction [-]
%   XE_e
%       edge elemental mole fraction [-]
%   T_e0
%       edge temperature at the reference point x0 [K]
%   Tv_e0
%       edge vibrational-electronic-electron temperature at the reference
%       point x0 (vibrational temperature in QSS must be constant, so it'll
%       be the same everywhere [K]
%   mixCnst
%       structure containing the mixture constants
%   intel
%       structure containing flow fields
%   options
%       structure containing options
%
% Author: Fernando Miro Miro
% Date: June 2019
% GNU Lesser General Public License 3.0

%---------------------------- DEVELOPERS NOTES ---------------------------- 
%-- 1. ys_e is actually XE_e for options.flow = 'LTEED'
%--------------------------------------------------------------------------

Nx      = 1001; % HARDCODE ALERT!!
tol     = options.tol;
itMax   = options.T_itMax;

m = beta/(2-beta); % used for the initial guess

xi_max  = U_e0 * mu_e0 * rho_e0 * max(x(:))/(m+1); % intial guess - incompressible value
xi0     = U_e0 * mu_e0 * rho_e0 * x0       /(m+1);
xi_min  = U_e0 * mu_e0 * rho_e0 * min(x(:))/(m+1);

xi_max  = max([xi_max,xi0]);                        % ensuring that all (initial) points are within the range
xi_min  = min([xi_max/100 , xi0 , xi_min]);

T_e     = repmat(T_e0, [Nx,1]);                     % initial guess of the temperature
Tv_e    = repmat(Tv_e0,[Nx,1]);                     % initial guess of the vibrational-electronic-electron temperature
p_e     = repmat(p_e0, [Nx,1]);                     % initial guess of the pressure

bOutOfRange = true;
err_xi = 1; it=0;
while (bOutOfRange || abs(err_xi) > tol) && it<itMax
    it=it+1;
    xi_vec  = linspace(xi_min,xi_max,Nx).';         % vector of xi positions
    [x_vec,U_e,rho_e,p_e,T_e,mu_e,y_se,h_e,H_e] = getEdge_FalknerSkan(xi_vec,xi0,beta,Theta,U_e0,p_e0,H_e0,ys_e,p_e,T_e,Tv_e,mixCnst,intel,options);
    bOutOfRange = any(x>max(x_vec));                % true if x is out range, false otherwise
    if bOutOfRange                                  % we are out of range
        xi_max = 2*xi_max;                              % increasing range
    end
    if x0<=max(x_vec)                               % if x0 is within the range
        xi0_prev = xi0;                                 % store previous value of xi0
        xi0 = interp1_noInf(x_vec,xi_vec,x0,'spline');  % interpolate to get corrected xi0
        err_xi = 1-xi0/xi0_prev;                        % difference between previous and current value
    elseif ~bOutOfRange
            xi_max = 2*xi_max;
    end
    if ismember(options.flow,{'LTE','LTEED'})
        disp(['Looping to obtain x in ',options.flow,'. it=',num2str(it),' --> err_xi = ',num2str(err_xi)]);
    end
end

if it==itMax
    warning(['Convergence of the edge conditions was not reached - update error ',num2str(err_xi),'. This means that the entire dimensional flowfield may be wrong']);
end

xi      = interp1_noInf(x_vec,xi_vec,x,'spline');
p_e     = interp1_noInf(x_vec,p_e,   x,'spline');
T_e     = interp1_noInf(x_vec,T_e,   x,'spline');
rho_e   = interp1_noInf(x_vec,rho_e, x,'spline');
U_e     = interp1_noInf(x_vec,U_e,   x,'spline');
h_e     = interp1_noInf(x_vec,h_e,   x,'spline');
H_e     = interp1_noInf(x_vec,H_e,   x,'spline');
mu_e    = interp1_noInf(x_vec,mu_e,  x,'spline');
y_se    = interp1_noInf(x_vec,y_se,  x,'spline');
ys_e    = cellfun(@(cll)cll.',matrix2D2cell1D(y_se),'UniformOutput',false);

end % getFalknerSkan_xi