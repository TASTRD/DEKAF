function [x_vec,U_e,rho_e,p_e,T_e,mu_e,y_se,h_e,H_e] = getEdge_FalknerSkan(xi_vec,xi0,beta,Theta,U_e0,p_e0,H_e0,ys_e,p_e,T_e,Tv_e,mixCnst,intel,options)
% getEdge_FalknerSkan returns the edge profiles corresponding to a
% Falkner-Skan solution defined by beta, Theta, and the values of the
% variables at a given xi0 position.
%
% Usage:
%   (1)
%       [x_vec,U_e,rho_e,p_e,T_e,mu_e,y_se,h_e,H_e] = getEdge_FalknerSkan(xi_vec,xi0,...
%                               beta,Theta,U_e0,p_e0,H_e0,ys_e,p_e,T_e,Tv_e,mixCnst,intel,options)
%
%   (2)     options.flow = 'LTEED'
%       [...] = getEdge_FalknerSkan(xi_vec,xi0,beta,Theta,U_e0,p_e0,H_e0,XE_e,p_e,T_e,Tv_e,mixCnst,intel,options)
%
% Inputs and outputs:
%   xi_vec
%       marching xi variable [kg^2/m^2-s^2]
%   x_vec
%       physical dimensional x variable [m]
%   xi0
%       marching xi variable at the reference point [kg^2/m^2-s^2]
%   beta
%       velocity Hartree parameter [-]
%   Theta
%       total enthalpy Hartree parameter [-]
%   U_e0
%       edge velocity at the reference point xi0 [m/s]
%   p_e0
%       edge pressure at the reference point xi0 [Pa]
%   H_e0
%       edge dynamic enthalpy at the reference point xi0 [J/kg]
%   T_e *1
%       edge temperature [K]
%   p_e *1
%       edge pressure [Pa]
%   Tv_e
%       edge vibrational-electronic-electron temperature [K]
%   ys_e
%       edge mass fraction [-]
%   XE_e
%       edge elemental fraction [-]
%   mixCnst
%       structure containing the mixture constants
%   intel
%       structure containing flow fields
%   options
%       structure containing options
%
% *1 are provided in the inputs as initial guesses
%
% Author: Fernando Miro Miro
% Date: June 2019
% GNU Lesser General Public License 3.0

%---------------------------- DEVELOPERS NOTES ---------------------------- 
%-- 1. ys_e is actually XE_e for options.flow = 'LTEED'
%--------------------------------------------------------------------------

Nx = length(xi_vec);        % number of streamwise positions
tol = options.tol;
itMax = options.T_itMax;

m = beta/(2-beta);          % used for the very first xi point (assumed incompresible before that

U_e     = U_e0 * (xi_vec / xi0).^(beta/2);                  % evaluating velocity
dUe_dxi = beta/2 * U_e0/xi0^(beta/2) * xi_vec.^(beta/2-1);  % evaluating velocity derivative
H_e     = H_e0 * (xi_vec / xi0).^(Theta/2);                 % evaluating total enthalpy
h_e     = H_e - 0.5*U_e.^2;                                 % evaluating static enthalpy
y_se0   = cell1D2matrix2D(ys_e);
y_se    = repmat(y_se0,[Nx,1]);                             % extending the mass fraction

err=1; it=0;
while err>tol && it<itMax
    it=it+1;
    [T_e,~,~,~,~,~,y_se] = getMixture_T(h_e,y_se,p_e,mixCnst,options,Tv_e,'Tguess',T_e(:));
    switch options.flow                                         % obtaining mixture molar mass and gas constant
        case 'CPG';                             Rstar_e             = intel.R_e(1);
                                                Mm_e                = mixCnst.R0/Rstar_e;
        case {'TPG','TPGD','LTE','LTEED','CNE'};[Mm_e,~,~,Rstar_e]  = getMixture_Mm_R(y_se,T_e,Tv_e,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron);
        otherwise;                          error(['the chosen flow assumption ''',options.flow,''' is not supported']);
    end

    if any(T_e<0)
        error('the chosen edge gradients conditions lead to a negative value of T_e');
    end
    pe_prev = p_e;
    rho_e = getMixture_rho([],p_e,T_e,[],[],[],[],'specify_R',Rstar_e,mixCnst.EoS_params,options);
    integrand = -U_e .* rho_e .* dUe_dxi;
    int     = cumtrapz(xi_vec,integrand);
    int     = int - interp1(xi_vec,int,xi0,'spline');
    p_e     = p_e0 + int;
    err     = norm(1 - p_e./pe_prev , Inf);        % computing error
end
mu_e    = getTransport_mu_kappa(y_se,T_e,Tv_e,rho_e,p_e,Mm_e,options,mixCnst);

x_vec = cumtrapz(xi_vec , 1./(rho_e.*mu_e.*U_e)) + xi_vec(1)/(rho_e(1).*mu_e(1).*U_e(1))*(1+m);


end % getEdge_FalknerSkan