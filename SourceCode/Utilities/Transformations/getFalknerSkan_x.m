function [x,p_e,rho_e,T_e,U_e,h_e,H_e,mu_e,ys_e] = getFalknerSkan_x(xi,beta,Theta,xi0,H_e0,U_e0,p_e0,ys_e,T_e0,Tv_e0,mixCnst,intel,options,varargin)
% getFalknerSkan_x passes from the computational xi to the physical x
% variable for self-similar profiles with a pressure gradient (Falkner-Skan
% situation).
%
% Usage:
%   (1.1)
%       [x,p_e,rho_e,T_e,U_e,h_e,H_e,mu_e,ys_e] = getFalknerSkan_x(xi,beta,Theta,...
%                                   xi0,H_e0,U_e0,p_e0,ys_e,T_e0,Tv_e0,mixCnst,intel,options)
%
%   (1.2)   options.flow = 'LTEED'
%       [...] = getFalknerSkan_x(xi,beta,Theta,xi0,H_e0,U_e0,p_e0,XE_e,T_e0,Tv_e0,mixCnst,intel,options)
%
%   (2)
%       [...] = getFalknerSkan_x(...,'xi_max',xi_max)
%       |--> allows to pass the value of xi_max to be used for the
%       interpolation.
%       By default - max(xi)
%
%   (3)
%       [...] = getFalknerSkan_x(...,'xi_min',xi_min)
%       |--> same but for the minimum xi.
%       By default - minimum between min(xi) and xi_max/100
%
%   (4)
%       [...] = getFalknerSkan_x(...,'check_EcVariation')
%       |--> performs a check to see how much the Eckert number varies, and
%       if there is a major difference, it issues a warning (not QSS)
%
% Inputs and outputs:
%   xi
%       marching xi variable [kg^2/m^2-s^2]
%   beta
%       velocity Hartree parameter [-]
%   Theta
%       total enthalpy Hartree parameter [-]
%   xi0
%       marching xi variable at the reference point [kg^2/m^2-s^2]
%   H_e0
%       edge dynamic enthalpy at the reference point xi0 [J/kg]
%   U_e0
%       edge velocity at the reference point xi0 [m/s]
%   p_e0
%       edge pressure at the reference point xi0 [Pa]
%   ys_e
%       edge mass fraction [-]
%   XE_e
%       edge elemental mole fraction [-]
%   T_e0
%       edge temperature at the reference point xi0 [K]
%   Tv_e0
%       edge vibrational-electronic-electron temperature at the reference
%       point x0 (vibrational temperature in QSS must be constant, so it'll
%       be the same everywhere [K]
%   mixCnst
%       structure containing the mixture constants
%   intel
%       structure containing flow fields
%   options
%       structure containing options
%
% See also: getFalknerSkan_xi
%
% Author: Fernando Miro Miro
% Date: June 2019
% GNU Lesser General Public License 3.0

%---------------------------- DEVELOPERS NOTES ---------------------------- 
%-- 1. ys_e is actually XE_e for options.flow = 'LTEED'
%--------------------------------------------------------------------------

xi_max          = parse_optional_input(varargin,'xi_max',max(xi(:)));
xi_min          = parse_optional_input(varargin,'xi_min',min(min(xi(:)),xi_max/100));
[~,bCheckEc]    = find_flagAndRemove('check_EcVariation',varargin);

Nx = 1001; % HARDCODE ALERT!!
xi_vec  = linspace(xi_min,xi_max,Nx).';                 % vector of xi positions
T_e     = repmat(T_e0, [Nx,1]);                         % initial guess of the temperature
Tv_e    = repmat(Tv_e0,[Nx,1]);                         % initial guess of the vibrational-electronic-electron temperature
p_e     = repmat(p_e0, [Nx,1]);                         % initial guess of the pressure

[x_vec,U_e,rho_e,p_e,T_e,mu_e,y_se,h_e,H_e] = getEdge_FalknerSkan(xi_vec,xi0,beta,Theta,U_e0,p_e0,H_e0,ys_e,p_e,T_e,Tv_e,mixCnst,intel,options);

x       = interp1_noInf(xi_vec,x_vec,xi,'spline');
p_e     = interp1_noInf(xi_vec,p_e,  xi,'spline');
T_e     = interp1_noInf(xi_vec,T_e,  xi,'spline');
rho_e   = interp1_noInf(xi_vec,rho_e,xi,'spline');
U_e     = interp1_noInf(xi_vec,U_e,  xi,'spline');
h_e     = interp1_noInf(xi_vec,h_e,  xi,'spline');
H_e     = interp1_noInf(xi_vec,H_e,  xi,'spline');
mu_e    = interp1_noInf(xi_vec,mu_e, xi,'spline');
y_se    = interp1_noInf(xi_vec,y_se, xi,'spline');
ys_e    = matrix2D2cell1D(y_se);

if bCheckEc                     % checking Eckert variation
    Ecu = U_e.^2 ./ h_e;
    Ecu0 = mean(Ecu(:));
    percent_Ecu = abs(1-Ecu/Ecu0);
    if percent_Ecu>1                % more than 1% variation
        warning(['The Eckert number varied a ',num2str(percent_Ecu),'% (Ecu=',num2str(min(Ecu)),'-',num2str(max(Ecu)),') between x=', ...
            num2str(x_vec(1)),'-',num2str(x_vec(end)),'m. QSS solutions are only valid for constant or zero Eckert number.']);
    end
end

end % getFalknerSkan_x