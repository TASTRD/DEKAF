function x = eval_htrowgnilli(xi,U_e,rho_e,mu_e,Dxi)
% eval_htrowgnilli evaluates the inverse illingworth transformation
%
% Usage:
%   (1)
%       x = eval_htrowgnilli(xi,U_e,rho_e,mu_e,Dxi)
%
% Inputs and outputs:
%   x
%       streamwise position                 [m]             (size Nx x 1)
%   U_e
%       streamwise edge velocity            [m/s]           (size Nx x 1)
%   rho_e
%       streamwise density                  [kg/m^3]        (size Nx x 1)
%   mu_e
%       streamwise dynamic viscosity        [kg/m-s]        (size Nx x 1)
%   Dxi
%       differentiation matrix wrt xi       [m^2-s^2/kg^2]  (size Nx x Nx)
%   xi
%       transformed marching variable       [kg^2/m^2-s^2]  (size Nx x 1)
%
% See also: eval_illingworth
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

Ixi = integral_mat(Dxi,'ud');
% It is now time to scale the x vector to the computational grid (xi)
dxi_dx = U_e(:).*rho_e(:).*mu_e(:);                         % column vector
dx_dxi = 1./dxi_dx;
% For stagnation flows, dx_dxi is 1/0. We do not want this artifact to
% propagate throughout the entire domain upon integration.
% To nullify this, we set dx_dxi(1) to a value of 0 to eliminate its
% influence upon integration.
if U_e(1) == 0
    dx_dxi(1) = 0;
    x0 = 0;
else
    x0 = xi(1)/dxi_dx(1);
end
x = Ixi*(dx_dxi) + x0;                      % column vector

end % eval_htrowgnilli