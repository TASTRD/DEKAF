function [xi,D1_xi,D2_xi,x,xPhys] = get_xi(xSpacing,mapOpts,ox,xPhys_e,U_e,rho_e,mu_e,coordsys,cylCoordFuncs)
%GET_XI Returns the streamwise point distribution in the transformed
% xi-space
%
% Usage:
%   (1)
%       [xi,D1_xi,D2_xi,dXI,x,xPhys] = get_xi(xiSpacing,mapOpts,ox,xPhys_e,...
%                                           U_e,rho_e,mu_e,coordsys,cylCoordFuncs);
%       |--> usage for 'linear', 'log', 'tanh', or 'custom_x' values of
%       xSpacing.
%
%   (2)
%       [xi,D1_xi,D2_xi,dXI,x,xPhys] = get_xi(xiSpacing,mapOpts,ox,xi_e,...
%                                           U_e,rho_e,mu_e,coordsys,cylCoordFuncs);
%       |--> usage for xSpacing='custom_xi'
%
% Inputs and outputs:
%   xSpacing
%       string identifying how the streamwise spacing is to be obtained.
%       Supported values are:
%           'linear', 'log', 'tanh'
%               see <a href="matlab:help xiMappings">xMappings</a>
%           'custom_xi'
%               allows the user to pass a custom xi-point distribution
%               through mapOpts.xi_custom
%           'custom_x'
%               allows the user to pass a custom x-point distribution (in
%               the physical space) through mapOpts.x_custom
%   mapOpts
%       structure with the necessary options for the streamwise mapping. It
%       must contain different fields depending on the choice of xiSpacing
%   ox
%       number identifying the streamwise differentiation order
%   xPhys_e             [m]
%       streamwise physical coordinate paired with the edge profiles
%       (size N_xe x 1)
%   xi_e                [m]
%       streamwise transformed coordinate paired with the edge profiles
%       (size N_xe x 1)
%   U_e, rho_e, mu_e    [m,kg,s]
%       edge velocity, density and dynamiic viscosity  (size N_xe x 1)
%   coordsys
%       string identifying the coordinate system
%   cylCoordFuncs
%       structure containing various functions to compute various
%       geometrical quantities related to cylindrical coordinates:
%           .xcAxis(xc)         [m]
%               position along the axis of revolution
%           .rc(xc)             [m]
%               spanwise radius of curvature
%           .alphac(xc)         [deg]
%               streamwise inclination angle
%           .Ixc_rc2(xc)        [m]
%               integral of (rc/L)^2 wrt xc
%           .Dxc_rc(xc)         [-]
%               derivative of rc wrt xc
%           .Dxc_alphac(xc)     [1/m]
%               derivative of alphac wrt xc
%           .IxBar_1rc2(xBar)   [m]
%               integral of (L/rc)^2 wrt xBar
%   xPhys               [m]
%       physical streamwise point distribution (size 1 x N_x)
%   x                   [m]
%       streamwise point distribution after applying (when needed) the
%       Probstein-Elliot transformations (size 1 x N_x)
%   xi                  [kg^2/m^2-s^2]
%       streamwise point distribution after the illingworth and (when
%       needed) the Probstein-Elliot transformation (size 1 x N_x)
%   D1_xi               [m^2-s^2/kg^s]
%       first-order differentiation matrix in the streamwise direction
%       (size N_x x N_x)
%   D2_xi               [m^2-s^2/kg^s]
%       second-order differentiation matrix in the streamwise direction
%       (size N_x x N_x)
%
% See also: eval_illingworth, eval_htrowgnilli, get_xyTransformed,
% get_xyInvTransformed
%
% Author: Fernando Miro Miro
% Date: July 2019
% GNU Lesser General Public License 3.0

switch xSpacing
    case 'custom_xi'                                                            % the user passed the actual values of xi
        xi = mapOpts.xi_custom(:);                                                  % extracting them
        xi_e = xPhys_e;                                                             % the 4th input is actually xi_e in this case
        [D1_xi,D2_xi] = FDdif_nonEquis(xi,['backward',num2str(ox)]);                % computing differentiation matrices
        U   = interp1OrRepmat(xi_e,U_e,  xi,'spline');                              % interpolating onto the new domain
        rho = interp1OrRepmat(xi_e,rho_e,xi,'spline');
        mu  = interp1OrRepmat(xi_e,mu_e, xi,'spline');
        x = eval_htrowgnilli(xi,U,rho,mu,D1_xi);                                    % evaluating inverse illingworth transformation to pass to the physical x space
        xPhys = get_xyInvTransformed(x,[],coordsys,cylCoordFuncs);         % passing them from the transformed (Probstein-Elliot) domain, to the physical
    case {'custom_x','linear','log','tanh'}                                     % the user specified dimensional x values
        switch xSpacing
            case 'custom_x'                                                         % the user passed the actual values of x
                xPhys = mapOpts.x_custom(:);                                            % extracting them
            case {'linear','log','tanh'}                                            % the values of x must be obtained from a mapping
                xPhys = xMappings(xSpacing,'get_x',mapOpts);                            % computing x-point distribution
                xPhys = xPhys(:);
            otherwise                                                               % break
                error(['Error: the chosen xSpacing ''',xSpacing,''' is not supported']);
        end
        x   = get_xyTransformed(xPhys,[],coordsys,cylCoordFuncs);                  % passing them to the transformed (Probstein-Elliot) domain if necessary
        U   = interp1OrRepmat(xPhys_e,U_e,  xPhys,'spline');                        % interpolating onto the new domain
        rho = interp1OrRepmat(xPhys_e,rho_e,xPhys,'spline');
        mu  = interp1OrRepmat(xPhys_e,mu_e, xPhys,'spline');
        Dx  = FDdif_nonEquis(x,['backward',num2str(ox)]);                           % obtaining differentiation matrix in the chosen x domain
        xi  = eval_illingworth(x,U,rho,mu,Dx);                                      % evaluating illingworth transformation to pass to the computational xi space
        [D1_xi,D2_xi] = FDdif_nonEquis(xi,['backward',num2str(ox)]);                % obtaining differentiation matrices
    otherwise
        error(['the chosen xSpacing ''',xSpacing,''' is not supported']);
end
xi      = xi(:).'; % making it into a row vector
x       = x(:).';
xPhys   = xPhys(:).';

end % get_xi
