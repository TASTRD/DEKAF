function [] = getImportedEdge_x(x_import, x_calc, ox, display)
%GETIMPORTEDEDGE_X Check if the calculated x is beyond the truncation error
% estimate with respect to the imported x domain.
%
% Usage:
%   (1)
%       x_import = getImportedEdge_x(x_import, x_calc, ox, display);
%
% Inputs:
%   x_import
%       imported x domain [m] that was used to initially build xi
%   x_calc
%       calculated x domain [m] from the xi used in DEKAF
%   ox
%       order of x discretization (options.ox)
%   display
%       if true, show a plot comparing x_import and x_calc if the
%       error exceed approximated truncation errors
%
% See also: eval_dimVars, convert_airfoil
%
% Author(s): Ethan Beyak
% Date: July 2019

% When importing the edge values, using the imported x values is
% more accurate than the calculated x based on xi, because the
% original xi appreciated some roundoff error from x by initial use
% of the integration matrix (see convert_airfoil as an example).
% Then the integration matrix is used *again* to compute this x.
% Hence there are two uses of integration matrices here.

errmsg_ID = 'DEKAF:getImportedEdge_x';
Ny = size(x_import,1);
if Ny>1
    error(errmsg_ID,['Size for x should be 1 x Nx, not ',num2str(Ny),' x Nx'])
end

% Difference between imported and calculated x's
deltax_err = x_import - x_calc(1,:);

% truncation error approximation of imported x grid
dx_import_te = abs(diff(x_import)).^(-ox);

% Multiply the truncation error by 2 because of the two applications of the
% integration matrices throughout DEKAF
num_x_large_err = nnz(abs(deltax_err(2:end)) > 2*dx_import_te);

if num_x_large_err
    % set up subject-verb agreement for the grammar-nazi in me ^^
    if num_x_large_err == 1
        svagree = {'is ', '', 's '};
    else
        svagree = {'are ', 's', ' '};
    end
    warning(['Comparing the imported x coordinates to the calculated x,',newline, ...
        'there ',svagree{1}, num2str(num_x_large_err), ' coordinate',svagree{2},' which ', ...
        'exceed',svagree{3},'the expected truncation error from integration.']);

    % Make a plot showing how bad the calculated x domain is w.r.t the imported x
    if display
        figID = figure;
        clf(figID);
        axes(figID);
        plot(x_import, 'k');
        hold on
        plot(x_calc, 'b');
        plot(x_calc + [0,dx_import_te], 'r--');
        plot(x_calc - [0,dx_import_te], 'r--');
        xlabel('index');
        legend({'imported x','calculated x' ...
            , 'truncation error upper bound' ...
            , 'truncation error lower bound'});
    end
end

end % getImportedEdge_x
