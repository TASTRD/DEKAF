function [x,y,r,r0,deta_dy] = getDimensional_xy(D_eta,rho,U_e,xi,xBar,coordsys,cylCoordFuncs)
% getDimensional_y returns the dimensional value of the wall-normal
% coordinate y at a single streamwise station.
%
% Usage:
%   (1)
%       [x,y,r,r0,deta_dy] = getDimensional_xy(D_eta,rho,U_e,xi,xBar,coordsys,cylCoordFuncs)
%
% Inputs and outputs:
%   D_eta           [-]                 (N_eta x N_eta)
%       differentiation matrix in the wall-normal direction.
%   rho             [kg/m^3]            (N_eta x 1)
%       mixture density
%   U_e             [m/s]               (1 x 1)
%       edge velocity
%   xi              [kg^2/m^2-s^2]      (1 x 1)
%       transformed streamwise position
%   xBar            [m]                 (1 x 1)
%       streamwise coordinate (Probstein-Elliot transformed)
%   coordsys
%       string identifying the coordinate system. Supported values are
%       found in <a href="matlab:help get_xyInvTransformed">get_xyInvTransformed</a>
%   cylCoordFuncs
%       structure containing various functions to compute various
%       geometrical quantities related to cylindrical coordinates:
%           .rc(xc)             [m]
%               spanwise radius of curvature
%           .alphac(xc)         [deg]
%               streamwise inclination angle
%           .Ixc_rc2(xc)        [m]
%               integral of (rc/L)^2 wrt xc
%           .Dxc_rc(xc)         [-]
%               derivative of rc wrt xc
%           .Dxc_alphac(xc)     [1/m]
%               derivative of alphac wrt xc
%           .IxBar_1rc2(xBar)   [m]
%               integral of (L/rc)^2 wrt xBar
%   x               [m]                 (1 x 1)
%       streamwise physical coordinate
%   y               [m]                 (N_eta x 1)
%       wall-normal physical coordinates
%   r               [m]                 (N_eta x 1)
%       radial distance from the axis to each x-y point
%   r0              [m]                 (N_eta x 1)
%       radial distance from the axis to the surface at each x-y point
%   deta_dy         [1/m]               (N_eta x 1)
%       derivative of the transformed variable eta wrt y
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

deta_dyBar  = rho *U_e/sqrt(2*xi);              % eta gradient wrt the transformed y
I1_eta      = integral_mat(D_eta,'du');         % eta = 0 in bottom row
yBar        = I1_eta * (1./deta_dyBar);         % transformed y
[x,y,r,r0,~,dyBar_dy]  = get_xyInvTransformed(xBar,yBar,coordsys,cylCoordFuncs);
deta_dy     = deta_dyBar .* dyBar_dy;           % chain rule

end % getDimensional_y