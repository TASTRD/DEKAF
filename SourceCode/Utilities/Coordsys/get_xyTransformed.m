function [xBar,yBar,r,r0,dyBar_dx,dyBar_dy,dyBar_dy2,dxBar_dx] = get_xyTransformed(x,y,coordsys,cylCoordFuncs)
% get_xyTransformed performs whichever transformation is necessary based on
% the options provided.
%
% Usage:
%   (1)
%       [xBar,yBar,r,r0,dyBar_dx,dyBar_dy,dyBar_dy2,dxBar_dx] = get_xyTransformed(x,y,coordsys,cylCoordFuncs)
%
%   (2)
%       xBar = get_xyTransformed(x,[],coordsys,cylCoordFuncs)
%
% Inputs and outputs:
%   xBar, yBar
%       streamwise and wall-normal transformed coordinates              [m]
%   x, y
%       streamwise and wall-normal physical coordinates                 [m]
%   r
%       radial distance from the axis of each x-y point                 [m]
%   r0
%       radial distance from the axis to the surface at each x-y point  [m]
%   dyBar_dx
%       variation of the transformed coordinate y with the physical
%       coordinate x                                                    [-]
%   dxBar_dx
%       variation of the transformed coordinate x with the physical
%       coordinate x                                                    [-]
%   dyBar_dy
%       variation of the transformed coordinate y with the physical
%       coordinate y                                                    [-]
%   dyBar_dy2
%       variation of the transformed coordinate y with the physical
%       coordinate y twice                                              [-]
%   coordsys
%       string identifying the coordinate system. Supported values are:
%           'cartesian2D'
%           'cone'
%           'cylindricalExternal'
%           'cylindricalInternal'
%   cylCoordFuncs
%       structure containing various functions to compute various
%       geometrical quantities related to cylindrical coordinates:
%           .rc(xc)             [m]
%               spanwise radius of curvature
%           .alphac(xc)         [deg]
%               streamwise inclination angle
%           .Ixc_rc2(xc)        [m]
%               integral of (rc/L)^2 wrt xc
%           .Dxc_rc(xc)         [-]
%               derivative of rc wrt xc
%           .Dxc_alphac(xc)     [1/m]
%               derivative of alphac wrt xc
%
% See also: get_xyInvTransformed, get_xyrProbsteinElliot_external,
% get_xyrProbsteinElliot_internal
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if isempty(y)
    y = NaN*ones(size(x));
end

switch coordsys                                                     % depending on the coordinate system
    case 'cartesian2D'                                                  % for cartesian
        xBar = x;                   yBar = y;                               % nothing to be done
        r = y;                      r0 = zeros(size(x));
        dyBar_dx = zeros(size(y));  dyBar_dy = ones(size(x));
        dyBar_dy2 = zeros(size(y)); dxBar_dx = ones(size(x));
    case {'cone','cylindricalExternal'}                                 % for conical and an exterior cylindrical system
        [xBar,yBar,r,r0,dyBar_dx,dyBar_dy,dyBar_dy2,dxBar_dx] = get_xyrProbsteinElliot_external(x,y,cylCoordFuncs);
    case 'cylindricalInternal'                                          % for conical and an interior cylindrical system
        [xBar,yBar,r,r0,dyBar_dx,dyBar_dy,dyBar_dy2,dxBar_dx] = get_xyrProbsteinElliot_internal(x,y,cylCoordFuncs);
    otherwise                                                           % for anything else break
        error(['the chosen coordsys ''',coordsys,''' is not supported']);
end

end % get_xyTransformed
