function varargout = deresolve_airfoil(xi, xi_interp, method_interp, varargin)
%DERESOLVE_AIRFOIL Interpolate/deresolve airfoil quantities using the input
% domain, xi, onto the query points, xi_interp, with a provided interpolation
% method.
%
% Usage:
%   varargout = deresolve_airfoil(xi, xi_interp, method_interp, varargin);
%
% Inputs:
%   xi              - xi domain calculated by integrating dxi_ds over the airfoil
%   xi_interp       - query domain of distinct xi points
%   method_interp   - interpolation method for interp1
%   varargin        - distinct actual arguments of input airfoil quantities
%                     over the input domain, xi
%
% Outputs:
%   varargout       - distinct actual arguments of input airfoil quantities
%                     interpolated onto the query domain, xi_interp, using
%                     method_interp
%
% See also: convert_airfoil
%
% Author(s): Ethan Beyak, Koen Groot
%
% GNU Lesser General Public License 3.0

varargout = cell(size(varargin));
for i = 1:length(varargin)
    varargout{i} = interp1(xi, varargin{i}, xi_interp, method_interp);
end

end % deresolve_airfoil
