function s_data = build_s_data(x_geom_data, s_geom_data, x_data, method_interp)
% BUILD_S_DATA From the x and s on the geometry's resolution, we
% extract out the s corresponding to the x/c vs. Cp_UV data.
%
% Usage:
%   s_data = build_s_data(x_geom_data, s_geom_data, x_data, method_interp);
%
% ASSUMPTION: The input data is oriented clockwise from the trailing edge
% of the LOWER surface to the trailing edge of the UPPER surface.
%
% Inputs:
%   x_geom_data     - x coordinates from the geometry input data (m)
%   s_geom_data     - s coordinates from the geometry input data (m)
%   x_data          - x coordinates from the Cp data input data (m)
%   method_interp   - interpolation method for interp1
%
% Outputs:
%   s_data          - s coordinates from the Cp data input data (m)
%
% See also: convert_airfoil
%
% Author(s): Ethan Beyak, Koen Groot
%
% GNU Lesser General Public License 3.0

% Find the leading edge index of the geom data set
% Split into the geometric top and bottom portions
[~,i_LE_geom] = min(x_geom_data);
s_geom_data_bottom = s_geom_data(1:i_LE_geom);
x_geom_data_bottom = x_geom_data(1:i_LE_geom);
s_geom_data_top = s_geom_data(i_LE_geom+1:end);
x_geom_data_top = x_geom_data(i_LE_geom+1:end);

% Find the leading edge index of the Cp_U data set
% And also split into top and bottom
[~,i_LE_data] = min(x_data);
x_data_bottom = x_data(1:i_LE_data);
x_data_top = x_data(i_LE_data+1:end);

% Now build s_data starting from the bottom to the top
% Note that x_data is multivalued vs. s - this requires us to perform
% the interpolation separately for the top and the bottom
%
% Note we only add sides to s_data if they exist in the input data (!)
% i.e. this allows one to pass in only one side of an airfoil
s_data = [];
if ~isempty(x_geom_data_bottom)
    s_data = [s_data; interp1(x_geom_data_bottom, s_geom_data_bottom, x_data_bottom, method_interp)];
end
if ~isempty(x_geom_data_top)
    s_data = [s_data; interp1(x_geom_data_top, s_geom_data_top, x_data_top, method_interp)];
end

end % build_s_data
