function [y_geom_data_M_inf, Cp_UV_data_M_inf, ...
    y_geom_data_M0, Cp_UV_data_M0] = apply_prandtl_glauert(...
    y_geom_data, Cp_UV_data, M_inf_UV_data, M_inf_UV)
%APPLY_PRANDTL_GLAUERT Use Prandtl-Glauert's scaling approximation to shift
% data (Cp and y-coordinates) from a supplied Mach number to a target Mach number.
%
% Usage:
%   [y_geom_data_M_inf, Cp_UV_data_M_inf, y_geom_data_M0, Cp_UV_data_M0] = ...
%       apply_prandtl_glauert(y_geom_data, Cp_UV_data, M_inf_UV_data, M_inf_UV);
%
% Inputs:
%   y_geom_data         - y coordinates of the geometry as specified by the
%                         input data file (dimensional)
%   Cp_UV_data          - pressure coefficient data nondimensionalized by
%                         the in-plane resultant velocity from input data
%   M_inf_UV_data       - Freestream Mach number based off the in-plane resultant
%                         velocity that was used to generate the Cp_UV_data
%   M_inf_UV            - The target Mach number for adjusting Cp_UV_data
%                         and y_geom_data by Prandtl-Glauert.
%
% Outputs:
%   y_geom_data_M_inf   - Modified y-coordinates post-scaling at M_inf_UV
%   Cp_UV_data_M_inf    - Modified Cp-distribution at M_inf_UV
%   y_geom_data_M0      - Modified y-coordinates post-scaling at M = 0
%   Cp_UV_data_M0       - Modified Cp-distribution at M = 0
%
% See also: convert_airfoil
%
% Author(s): Ethan Beyak, Koen Groot
%
% GNU Lesser General Public License 3.0

% Send to incompressible Mach 0
y_geom_data_M0 = y_geom_data*sqrt(1-M_inf_UV_data^2);
Cp_UV_data_M0 = Cp_UV_data*sqrt(1-M_inf_UV_data^2);

% Scale back up to the target Mach number
y_geom_data_M_inf = y_geom_data_M0/sqrt(1-M_inf_UV^2);
Cp_UV_data_M_inf = Cp_UV_data_M0/sqrt(1-M_inf_UV^2);

end % apply_prandtl_glauert
