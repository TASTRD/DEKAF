function [] = save_dat(filename, data, varargin)
%SAVE_DAT Save matrix of data to a file.
%
% Usage:
% (1) save_dat(filename, data)
% (2) save_dat(filename, data, file_description);
%
% Author(s): Ethan Beyak
% GNU Lesser General Public License 3.0

if ~isempty(varargin)
    file_description = varargin{1};
else
    file_description = 'datafile';
end

if exist(filename, 'file'), delete(filename); end

save(filename, 'data', '-ascii', '-double');
if exist(filename, 'file')
    fprintf(['The ', file_description, ' was properly written.\n']);
else
    error(['The ', file_description, ' wasn''t correctly written... Aborting!']);
end

end % save_dat