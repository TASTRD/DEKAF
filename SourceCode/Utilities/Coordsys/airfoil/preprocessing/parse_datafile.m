function [delimiters,row_offsets,col_offsets] = parse_datafile(filename)
%PARSE_DATAFILE
% Author(s):    Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

delimiters = [];
row_offsets = [];
col_offsets = [];

recording_vars = 0;
list_vars = {};
% The first row in the file is count zero (conform to MATLAB's dlmread convention)
row_cnt = -1;

if ~exist(filename, 'file')
    error([filename, ': No such file or directory']);
end
fid = fopen(filename);
this_line = fgetl(fid);
while true

    row_cnt = row_cnt + 1;

    if recording_vars == 0
        % Does this line contain 'VARIABLES = "FIRST_VAR"'? (typical Tecplot output)
        first_var_token = regexp(this_line,'^VARIABLES = "(\w+)"$','tokens');
        if ~isempty(first_var_token)
            list_vars{end+1} = first_var_token{1}{1}; %#ok<AGROW>
            recording_vars = 1;
        end
    elseif recording_vars == 1
        % Keep searching for the other variables' names
        other_var_token = regexp(this_line,'^"(\w+)"$','tokens');
        if ~isempty(other_var_token)
            list_vars{end+1} = other_var_token{1}{1}; %#ok<AGROW>
        else
            recording_vars = 2; % We've parsed all basic-state variables' names
        end
    end
    if recording_vars == 0 || recording_vars == 2
        % Either the .dat has no variables' names or ...
        % we're done appending variables now
        % Let's search for the first row of numeric data
        % This assumes the same delimiter is used between all of the columns of data
        whitespace_tokens = regexp(this_line,'^(\s*)-?\d\S+(,?\s*)','tokens');
        if ~isempty(whitespace_tokens)
            leading_whitespace = whitespace_tokens{1}{1};
            delimiter = whitespace_tokens{1}{2};
            col_offsets = length(leading_whitespace);
            row_offsets = row_cnt;
            delimiters = delimiter;
            break % We're done parsing the .dat for this zone -- exit the while
        end
    end

    this_line = fgetl(fid);

    if this_line == -1
        error('End of the file unexpectedly. Check the formatting of %s\n', filename);
    end
end
fclose(fid);
if ~isempty(list_vars)
    % Create a comma-separated string of the variables parsed
    vars = '';
    for j = 1:length(list_vars)
        vars = [vars, list_vars{j}, ', ']; %#ok<AGROW>
    end
    vars = vars(1:end-2); % Remove the final comma-space substring
    fprintf('Parsed datafile ''%s''\nOrder of variables: %s\n', filename, vars);
else
    fprintf('Parsed datafile ''%s''\n', filename);
end

end % parse_datafile