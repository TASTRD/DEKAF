function [Cp_data_out, geom_data_out] = get_Cp_geom_data(af, datafile)
%GET_CP_GEOM_DATA From a Tecplot file, extract the Cp vs x/c, as well as
% the x/c and y/c geometry of the airfoil.
%
% See caller_get_Cp_geom_data.m for an example usage
%
% Author(s): Ethan Beyak
% GNU Lesser General Public License 3.0

% ----------------------------- PREPROCESSING -----------------------------
% Set defaults
af = standardvalue(af,'save_dat',true);

% Build all the angles (!)
alpha_v_u = af.alpha_v_u;
Lambda_w_uv = af.Lambda_w_uv;
alpha_v_uw = asind(sind(af.alpha_v_u)*cosd(af.Lambda_w_uv));
Lambda_w_u = atan2d(tand(af.Lambda_w_uv)*secd(af.alpha_v_u),1);

if isfield(af,'M_inf_Q') && isfield(af,'M_inf_UV')
    error('Must specify only one of the fields {M_inf_Q, M_inf_UV}.');
elseif isfield(af,'M_inf_Q')
    M_inf_Q = af.M_inf_Q;
    M_inf_UV = M_inf_Q*cosd(Lambda_w_uv);
elseif isfield(af,'M_inf_UV')
    M_inf_UV = af.M_inf_UV;
    M_inf_Q = M_inf_UV/cosd(Lambda_w_uv);
else
    error('Must specify one of the fields {M_inf_Q, M_inf_UV}.');
end
M_inf_U = M_inf_UV*cosd(alpha_v_u);
M_inf_V = M_inf_UV*sind(alpha_v_u);
M_inf_W = M_inf_Q*sind(Lambda_w_uv);

% Get infinity values of static and dynamic pressure
a_inf = sqrt(af.gam*af.R*af.T_inf);
Q_inf = M_inf_Q*a_inf;
UV_inf = M_inf_UV*a_inf;
rho_inf = af.p_inf/(af.R*af.T_inf);
mu_inf = applySutherland(af.T_inf, af.mu_ref, af.T_ref, af.Su_mu); % kg/m-s

% --- ASSUMING Re IS BASED OFF OF THE RESULTANT INFINITY VELOCITY! ---
% If length scale is based on U direction ...
chord_U = af.Re_UV_cU*mu_inf/(rho_inf*UV_inf);
chord_UV = chord_U*secd(af.alpha_v_u);
chord_Q = chord_UV*secd(Lambda_w_uv);

% Re_Q_cU = rho_inf*Q_inf*chord_U/mu_inf;

% Write summary to the screen
fprintf('\n');
fprintf('Relevant properties:\n');
fprintf('Temperature: ........................ %.16g (K)\n', af.T_inf);
fprintf('Pressure: ........................... %.16g (Pa)\n', af.p_inf);
fprintf('Density: ............................ %.16g (kg/m^3)\n', rho_inf);
fprintf('Speed of sound: ..................... %.16g (m/s)\n', a_inf);
fprintf('Dynamic viscosity: .................. %.16g (kg/m-s)\n', mu_inf);
fprintf('Chord in Q dir: ..................... %.16g (m)\n', chord_Q);
fprintf('Chord in UV dir: .................... %.16g (m)\n', chord_UV);
fprintf('Chord in U dir: ..................... %.16g (m)\n', chord_U);
fprintf('Lambda_w_u (deg) : .................. %.16g\n', Lambda_w_u);
fprintf('Lambda_w_uv (deg) : ................. %.16g\n', Lambda_w_uv);
fprintf('alpha_v_u (deg) : ................... %.16g\n', alpha_v_u);
fprintf('alpha_v_uw (deg) : .................. %.16g\n', alpha_v_uw);
fprintf('Mach (U as velocity): ............... %.16g\n', M_inf_U);
fprintf('Mach (V as velocity): ............... %.16g\n', M_inf_V);
fprintf('Mach (W as velocity): ............... %.16g\n', M_inf_W);
fprintf('Mach (UV as velocity, perp. to LE): . %.16g\n', M_inf_UV);
fprintf('Mach (Q as velocity): ............... %.16g\n', M_inf_Q);
fprintf('Velocity (resultant): ............... %.16g (m/s)\n', Q_inf);
fprintf('\n');

% Parse the data for x,y,z,i,j,k,u,v,w,T,rho
[delimiters, row_offsets, col_offsets] = parse_datafile(datafile);
data = dlmread(datafile, delimiters, row_offsets, col_offsets);

% ------------------------------ GET CP DATA ------------------------------
% Extract out the logical indices for the wall
j = data(:,af.data_idx.j);
k = data(:,af.data_idx.k);
x_full = data(:,af.data_idx.x);
% Assume the wall is described by a) j = 1, b) picking only one spanwise
% station, and c) no-slip condition on u velocity components.
wall_idx = j == 1 & k == median(unique(k)) & x_full/chord_U <= 1;

% Grab wall quantities necessary for building Cp
x_wall = data(wall_idx,af.data_idx.x);
T_wall = data(wall_idx,af.data_idx.T);
rho_wall = data(wall_idx,af.data_idx.rho);

% Assume isentropic freestream & calorically perfect gas
p_wall = rho_wall*af.R.*T_wall;
Cp_UV = (p_wall/af.p_inf - 1)*(2/(af.gam*M_inf_UV^2));

% Nondimensionalize the x domain perpendicular to the leading edge
xoverc = x_wall/chord_U;

% Write out to a file
Cp_data_out = [xoverc, Cp_UV];

% Note that Tecplot, with the cases tested, writes data in a
% *counter-clockwise* fashion, even if the order of increasing i indices is
% clockwise. So we flip this orientation to be *clockwise* about the
% airfoil, agreeing with the convention of convert_airfoil.
Cp_data_out = flipud(Cp_data_out);

if af.save_dat
    save_dat(af.Cp_filename, Cp_data_out);
end

% ------------------ GET SURFACE COORDINATES OF GEOMETRY ------------------
y_wall = data(wall_idx,af.data_idx.y);
geom_data_out = [xoverc, y_wall/chord_U];

% Flip Tecplot's i indices to be clockwise (as described above)
geom_data_out = flipud(geom_data_out);

if af.save_dat
    save_dat(af.geom_filename, geom_data_out)
end

end % get_Cp_geom_data
