function [q_fit, q_wf, is_db] = fit_airfoil_array(q_unfit, s, i_maxq, smoothparam, domain_bounds)
%FIT_AIRFOIL_ARRAY(q_unfit, s, i_maxq, smoothparam, domain_bounds)
% Smooth a variable q = q(s) over s using MATLAB's fit() function with the
% 'smoothingspline' variable argument. This function has the capability to
% maintain the function's values near its maximum over s (since fitting
% notoriously destroys extrema easily) while blending into the fit
% as prescribed by smoothing spline. The blend is not an on/off switch, but
% rather a cosine, gradually turning on the fit as s moves away from the
% s location of max q.
%
% Inputs:
%   q_unfit         - unfit array of some quantity q (usually Cp or curvature)
%   s               - the surface coordinate corresponding to q (s domain)
%   i_maxq          - index corresponding to the maximum of q
%   smoothparam     - argument to fit()'s 'smoothingparam' option for
%                     'smoothingspline'. This can take one of two forms:
%                       1. a single value (double)
%                       2. a two-element array of doubles
%                        If option 2. is supplied, then the first entry is
%                     'smoothingparam' for the top side of the airfoil.
%                     Similarly, the second entry is the 'smoothingparam'
%                     for the bottom side of the airfoil.
%                         If option 1. is supplied, then the same smoothing
%                     constant is used for both sides of the airfoil.
%   domain_bounds   - s coordinates defining where the fit begins and ends.
%                     see the diagram below:
%
%  fit_bottom   cosine       original            cosine          fit_top
% -------------|------|-------------------|------------------|-------------
% s ---->
%              1      2                   3                  4
%
%               The numbers specify the index of the domain_bounds variable
%               whose entry specifies the s coordinate.
%
% Outputs:
%   q_fit   - fit array of the input quantity, q_unfit, including the cosine
%             blending
%   q_wf    - wholely fit array of q (that is, without the cosine blending)
%   is_db   - indices of the s array corresponding to the points nearest to
%             those s values indicated by domain_bounds. That is, is_db
%             is a four-element array of indices described by the diagram above.
%
% See also: convert_airfoil
%
% Author(s): Ethan Beyak, Koen Groot
%
% GNU Lesser General Public License 3.0

if length(smoothparam) == 1
    spf = [1 1]*smoothparam;
else
    spf = smoothparam;
end
% Build the fit for the bottom of the airfoil
idx_fit = [1:length(q_unfit)].'; %#ok<NBRAK>
q_FO = fit(idx_fit,q_unfit.','smoothingspline','smoothingparam',spf(1));
q_wf_b = feval(q_FO,idx_fit).';

% If the call specifies a distinct fit for the top, calculate that now
if length(smoothparam) == 2
    q_FO = fit(idx_fit,q_unfit.','smoothingspline','smoothingparam',spf(2));
    q_wf_t = feval(q_FO,idx_fit).'; % fit of q for the top
else % we use the same fit as for the bottom
    q_wf_t = q_wf_b;
end

% Specify the limits of the "near" attachment line bounds, points 2 and 3
[~,is_db(2)] = min(abs(s - s(i_maxq) - domain_bounds(2)));
[~,is_db(3)] = min(abs(s - s(i_maxq) - domain_bounds(3)));

% Specify the overlap limits of the fit, points 1 and 4
[~,is_db(1)] = min(abs(s - s(i_maxq) - domain_bounds(1)));
[~,is_db(4)] = min(abs(s - s(i_maxq) - domain_bounds(4)));

% Construct the weighting function over the s indices (cosine blending)
fw_b = [ones(1,is_db(1)) ...
    (1-cos(-pi:pi/(is_db(2)-is_db(1)-1):0))/2 ...
    zeros(1,length(s)-is_db(2))];
fw_t = [zeros(1,is_db(3)) ...
    (1+cos(-pi:pi/(is_db(4)-is_db(3)-1):0))/2 ...
    ones(1,length(s)-is_db(4))];

% Reconstruct the final fit
q_fit = q_wf_b.*fw_b + q_wf_t.*fw_t + q_unfit.*(1-fw_b-fw_t);

if length(smoothparam) == 1
    q_wf = q_wf_b;
else
    q_wf = [q_wf_b; q_wf_t];
end

end % fit_airfoil_array
