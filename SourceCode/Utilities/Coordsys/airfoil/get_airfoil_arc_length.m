function s = get_airfoil_arc_length(x, y)
%GET_AIRFOIL_ARC_LENGTH Calculate surface coordinates given Cartesian
%coordinates x,y assuming straight line distances between coordinates
%
% Usage:
%   s = get_airfoil_arc_length(x, y);
%
% Inputs:
%   x, y    - horizontal, vertical coordinates of airfoil surface
%
% Outputs:
%   s       - surface coordinate of airfoil surface
%
% See also: convert_airfoil
%
% Author(s): Ethan Beyak, Koen Groot
%
% GNU Lesser General Public License 3.0

s = [0; cumsum(sqrt(diff(x).^2 + diff(y).^2))];

end % get_airfoil_arc_length
