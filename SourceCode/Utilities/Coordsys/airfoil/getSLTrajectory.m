function [x_SL,y_SL,z_SL] = getSLTrajectory(s_SL,phi_s,xc_geom,yc_geom,xCp,Cp,flag,varargin)
% getSLTrajectory returns the trajectory of the inviscid streamline on the
% surface.
%
% Usage:
%   (1)
%       [x_SL,y_SL,z_SL] = getSLTrajectory(s_SL,phi_s,xc_geom,yc_geom,xCp,Cp,flag)
%
%   (2)
%       [...] = getSLTrajectory(...,'ccw_from_top_TE')
%       |--> the geometrical and Cp data is given by covering the airfoil
%       in the sense TE-top-LE-bottom-TE instead of TE-bottom-LE-top-TE
%       (default)
%
% Inputs and outputs:
%   s_SL        [m]
%       LE-normal positions of the inviscid streamline in the surface-LE
%       coordinate system
%   phi_s       [deg]
%       local sweep angle of the inviscid streamline
%   xc_geom     [m]
%       airfoil LE-normal geometry in the chord-LE coordinate system
%   yc_geom     [m]
%       airfoil chord-normal geometry in the chord-LE coordinate system
%   xCp         [m]
%       LE-normal position vector in the chord-LE coordinate system paired
%       with the Cp distribution
%   Cp          [-]
%       pressure coefficient distribution (paired with xCp)
%   flag
%       string identifying whether the streamline was obtained on the 'top'
%       or the 'bottom' of the airfoil
%   x_SL        [m]
%       LE-normal positions of the inviscid streamline in the chord-LE
%       coordinate system
%   y_SL        [m]
%       chord-normal positions of the inviscid streamline in the chord-LE
%       coordinate system
%   z_SL        [m]
%       LE-parallel positions of the inviscid streamline in both the
%       chord-LE and the surface-LE coordinate systems
%
% Author: Fernando Miro Miro
% Date: January 2020
% GNU Lesser General Public License 3.0

[~,bFlip] = find_flagAndRemove('ccw_from_top_TE',varargin);                 % checking for the flag with which the profiles must be flipped

if bFlip
    % we flip the vectors so that they cover the airfoil in the opposite
    % direction (TE-bottom-top instead of TE-top-bottom)
    xc_geom = flip(xc_geom);
    yc_geom = flip(yc_geom);
    xCp     = flip(xCp);
    Cp      = flip(Cp);
end

NGeom = length(xc_geom);                                                    % number of points in the geometry vector

[~,i_stag_data] = max(Cp);                                                  % finding stagnation point
[~,i_xCpMin]    = min(xCp);                                                 % minimum value of xCp, dividing the geometric top and bottom of the airfoil
[~,i_xGeomMin]  = min(xc_geom);                                             % minimum value of x_geom, dividing the geometric top and bottom of the airfoil

xStag   = xCp(i_stag_data);                                                 % x position of the stagnation point
s_geom  = get_airfoil_arc_length(xc_geom, yc_geom);                         % surface coordinate

if      i_stag_data> i_xCpMin;      [~,i_stag_geom] = min(abs(xc_geom(i_xGeomMin:NGeom) - xStag));  % stagnation point at the top    (negative AoA)
elseif  i_stag_data==i_xCpMin;      i_stag_geom = i_xGeomMin;                                       % stagnation point in the middle (zero AoA)
elseif  i_stag_data< i_xCpMin;      [~,i_stag_geom] = min(abs(xc_geom(1:i_xGeomMin)     - xStag));  % stagnation point at the bottom (positive AoA)
else;                               error('something very wrong - i_stag_data is neither >, < or = to i_xCpMin');
end

switch flag                                                                 % assigning the surface indices depending on whether the SL is on the top or the bottom surface
    case 'top';         idx_surf = i_stag_geom:NGeom;       s_geom0 = s_geom - s_geom(i_stag_geom);     zFact =  1;
    case 'bottom';      idx_surf = 1:i_stag_geom;           s_geom0 = s_geom(i_stag_geom) - s_geom;     zFact = -1;
    otherwise;          error(['the inputted value of the flag ''',flag,''' is not supported']);
end
% zFact accounts for the fact that the angle is based on surface
% coordinates, which, with the right-hand-rule assign different signs to
% the same physical z direction.

x_SL = interp1(s_geom0(idx_surf),xc_geom(idx_surf),s_SL,'spline');          % interpolating to obtain the chord-wise position of each surface point
y_SL = interp1(s_geom0(idx_surf),yc_geom(idx_surf),s_SL,'spline');          % same for the chord-normal position

phi_sAvg = (phi_s(1:end-1) + phi_s(2:end))/2;                               % average local sweep angle for each ds
z_SL     = zFact * cumsum(diff(s_SL).*tand(phi_sAvg));                      % integrating trajectory
z_SL     = [0,z_SL];                                                        % adding a leading zero

end % getSLTrajectory