function s_fine = build_s_fine(s_data, max_s, min_ds, i_stag_data)
% BUILD_S_FINE Build an ultra-fine s coordinate based on the data, the
% maximum s coordinate, the minimum ds, and an index for the stagnation point
%
% Usage:
%   s_fine = build_s_fine(s_data, max_s, min_ds, i_stag_data);
%
% Inputs:
%   s_data      - s coordinates from the Cp data input data (m)
%   max_s       - upper bound of final s coordinate of the s domain to be
%                 generated (i.e., near the trailing edge of upper surface)
%   min_ds      - equidistant difference between each s coordinate to be
%                 generated
%   i_stag_data - index of s coordinate corresponding to the stagnation/
%                 attachment line.
%
% Outputs:
%   s_fine      - s coordinates starting at the trailing edge of the lower
%                 surface (s \approx 0), containing exactly the stagnation
%                 point/attachment-line location, and progressing forward
%                 to the trailing edge of the upper surface.
%
% Note that the construction of s_fine does not guarantee s = 0 is in the
% domain any longer, since the step min_ds to the stagnation doesn't divide
% evenly. For example, 1:3:8 >> [1 4 7]; This is fine, because s = 0 is a
% trailing edge on the lower surface, and no one cares about the boundary
% layer at that point. Instead, we require knowing the boundary layer at
% exactly the stagnation point.
% Along the same vein, s = max_s may not be exactly contained in s_fine.
% Again, that's OK. That's the trailing edge on the upper surface.
%
% See also: build_s_data, convert_airfoil
%
% Author(s): Ethan Beyak, Koen Groot
%
% GNU Lesser General Public License 3.0

s_fine = [fliplr(s_data(i_stag_data):-min_ds:0) ...
    min_ds + s_data(i_stag_data):min_ds:max_s];

end % build_s_fine
