function varargout = select_airfoil_side(i_side, varargin)
%SELECT_AIRFOIL_SIDE Index all specified arguments, transforming the domain
%from the entire airfoil surface to the side specified from the input deck
%
% Usage:
%   varargout = select_airfoil_side(i_side, varargin);
%
% Inputs:
%   i_side      - logical indices, true on the side of interest, false else.
%   varargin    - distinct actual arguments of input airfoil quantities
%                 over the entire surface coordinate, s
%
% Outputs:
%   varargout   - distinct actual arguments of output airfoil quantities,
%                 now indexed on the side of interest.
%
% See also: convert_airfoil
%
% Author(s): Ethan Beyak, Koen Groot
%
% GNU Lesser General Public License 3.0

varargout = cell(size(varargin));
for i = 1:length(varargin)
    varargout{i} = varargin{i}(i_side);
end

end % select_airfoil_side
