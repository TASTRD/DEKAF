function [intel_out, options_out] = convert_airfoil(intel, options)
%CONVERT_AIRFOIL Main function to convert an airfoil's pressure coefficient,
% freestream conditions, and geometry parameters into an edge distribution
% suitable for DEKAF's non-self-similar boundary-layer equations.
%
% Usage:
%   [intel_out, options_out] = convert_airfoil(intel, options);
%
% Inputs:
%   intel       - struct with multiple fields: an airfoil struct with
%                 coordinate and Cp data, as well as other fields specifying
%                 the angle of attack, sweep, and freestream (infinity)
%                 conditions.
%   options     - struct whose fields contain the possible options to
%                 specify various models implemented in DEKAF. Of particular
%                 interest is the struct, options.airfoil. Its fields and
%                 a complete description of the other fields are given in
%                 setDefaults.m
%
% Outputs:
%   intel_out   - a struct whose fields are ready for the core functions of
%                 DEKAF. The self-similar marching coordinate xi and edge
%                 parameters are accurately established, as well as the
%                 special attachment line quantity, dUe0_dx: the derivative of
%                 U_e with respect to x at the attachment line. (Here, x is the
%                 surface coordinate.)
%   options_out - struct containing all options used throughout
%                 convert_airfoil and subsequently passed into the core of
%                 DEKAF.
%
% Overview of intel.airfoil usage:
%   A) Provide the geometry and pressure coefficient data.
%       1. xc_data
%       2. Cp_UV_data
%       3. xc_geom_data
%       4. yc_geom_data
%       5. chord_U
%
% Overview of options.airfoil usage:
%   A) Provide the airfoil side over which the boundary layer will be calculated.
%       1. side
%   B) Specify the orientation of the input data within intel.airfoil
%       1. data_orientation
%   C) Optionally provide information on the streamwise computational grid.
%       1. s_hyperinterp_fac
%   D) Optionally specify how the pressure coefficient will be *slightly*
%   manipulated to maintain isentropic edge quantities.
%       1. i_stag_shift_bounds
%       2. it_max_Cp
%       3. Cp_power
%   E) Optionally specify whether a curve fit should be produced for the
%   input pressure coefficient and calculated curvature of the airfoil.
%       1. fit_Cp
%       2. fit_kappa_curv
%   F) Optionally specify which plots to build during execution of convert_airfoil.
%       1. plot_Cp
%       2. plot_beta
%       3. plot_R_mrch
%       4. plot_xi
%       5. video_R_mrch_obj
%   G) Optionally debug the inner workings of convert_airfoil
%       1. debug
%
% Overview of intel usage:
%   A) Provide the angle of attack and sweep.
%       1a. alpha_v_u  and Lambda_w_uv OR
%       1b. alpha_v_uw and Lambda_w_u  OR
%       1c. alpha_v_u  and Lambda_w_u  OR
%       1d. alpha_v_uw and Lambda_w_uv
%   B) Provide freestream quantities.
%       1. T_inf
%       2. M_inf_Q or M_inf_UV
%       3. M_inf_UV_data
%       4. p_inf
%       5. ys_inf (optional)
%
% Details on each field within intel.airfoil:
%   A) Geometry and pressure coefficient data
%  - xc_data
%    |--->  column of x/c coordinates, size N_data x 1.
%           NO default values.
%  - Cp_UV_data
%    |--->  column of pressure coefficient values at x/c coordinates specified
%           by xc_data, size N_data x 1
%
%           The pressure coefficient is defined as
%
%                         p_wall - p_inf
%           Cp = ---------------------------------
%                  1
%                 --- rho_inf * (U_inf^2 + V_inf^2)
%                  2
%
%           i.e. nondimensionalized by the freestream velocity
%           in-plane resultant spanned by U_inf and V_inf.
%           NO default values.
%  - xc_geom_data
%    |--->  column of x/c coordinates of the geometry, size N_geom x 1.
%           Please note that N_geom does *not* have to equal N_data.
%           Interpolation will be performed appropriately if distinct.
%           NO default values.
%  - yc_geom_data
%    |--->  column of y/c coordinates of the geometry, size N_geom x 1.
%           NO default values.
%  - chord_U
%    |--->  chord c of the airfoil, dimensional. This is the reference length
%           of the inputs xc_data, xc_geom_data, and yc_geom_data.
%           NO default values.
%
% Details on each field within afopts:
%   A) side of interest
%  - side
%    |--->  string to specify the side of the airfoil over which the boundary
%           layer will be calculated. The possibilities are:
%       'top'       - the surface coordinates of the airfoil from the attachment line
%                     to the the trailing edge, progressing in a *clockwise* manner.
%       'bottom'    - the surface coordinates of the airfoil from the attachment line
%                     to the the trailing edge, progressing in a *counter-clockwise*
%                     manner.
%           NO default value.
%
%   B) Orientation of input data within intel.airfoil
%  - data_orientation
%    |--->  string to specify the directional orientation of the input data
%           of the airfoil. The possibilities are:
%       'cw_from_bottom_TE' - The first row in the column of input data
%                               corresponds to the trailing edge of the
%                               bottom of the airfoil, and the rows
%                               progress clockwise around the airfoil,
%                               with the final row as the trailing edge of the
%                               top of the airfoil.
%       'ccw_from_top_TE'   - The first row corresponds to the trailing edge
%                               of the top of the airfoil, and the rows
%                               progress counter-clockwise around the airfoil,
%                               with the final row as the trailing edge of the
%                               bottom of the airfoil.
%           default: 'cw_from_bottom_TE'
%   C) (Optional) Streamwise computational grid
%  - s_hyperinterp_fac
%    |--->  a positive number to hyperresolve the intermediate, overresolved s domain
%           used for convert_airfoil. The methodology incorporating this
%           input is as follows:
%
%           1. Out of the provided coordinates data (x/yc_geom_data) and xc_data,
%           generate the surface tangential coordinate for both sets:
%           s_geom_data and s_data. These are not necessarily equally-spaced.
%           Spline interpolation is performed when generating the interpolant
%           s_data. The method 'spline' is protected against user modification
%           because other methods are not continuous in the second derivative.
%           This is crucial for the proper subsequent manipulations, especially
%           near the attachment line.
%
%           2. Build an equally-spaced s domain (call it s_fine) whose
%           delta step ds is min([diff(s_data); diff(s_geom_data)])/s_hyperinterp_fac,
%           covers the entire airfoil surface (both sides), and contains the
%           Cp_UV_data's attachment line. In that sense, s_hyperinterp_fac
%           causes overinterpolation. We choose to overinterpolate because
%           the quadrature oscillations are reduced massively, and are
%           confined to a much smaller s extent. Note that large values of
%           this factor may cause memory issues for your workstation.
%           default: 10.0
%
%   Notes on options.mapOpts: All of the usual fields of options.mapOpts
%         are still applicable for the airfoil preprocessing function.
%         The defaults of mapOpts are identical except for one field:
%         'xSpacing'. It's default is 'tanh' for airfoil problems instead
%         of the usual value of 'linear' in order to help combat the natural
%         square-like deresolving of the attachment-line region due to the
%         Illingworth transformation.
%         Additionally, the user may not specify options.mapOpts.deltaxi_start
%         for the 'tanh' mapping due to programming architecture errors.
%         See Issue #61 on Gitlab for a related feature request.
%
%   D) (Optional) Cp manipulation to enforce isentropic flow.
%  - i_stag_shift_bounds:
%    |--->  two-element array of how many points away from the attachment line
%           for which Cp data points will be shifted until the Cp at the
%           attachment line is not anisentropic (which would lead to complex edge
%           velocities). The methodology incorporating this input is as follows:
%
%           1. Since the edge values of the boundary layer are populated
%           assuming isentropic relations, the input Cp itself must also
%           be isentropic. For some input Cp's, the attachment line value
%           can be anisentropic due to a coarse base flow grid resolution,
%           or the byproduct of approximations in an inviscid code, for
%           example. We tweak one (or more) data points at the attachment line
%           region, carefully re-interpolating with splines until the
%           attachment line Cp does not overshoot its isentropic value.
%
%           2. So what does this input specify? Consider this zoomed-in
%           schematic of the Cp near the attachment line in Figure 1.
%           If we use i_stag_shift_bounds = [1 0], it will shift the -Cp value
%           upward for the attachment line and one data point immediately
%           to the left (on the bottom side of the airfoil). Then the Cp is
%           reinterpolated with spline. This process completes when adjusted
%           Cp at the attachment line no longer overshoots its isentropic value.
%           Another example would be i_stag_shift_bounds = [1 1]; in total,
%           3 data points would be shifted in this process: the attachment-line
%           point Cp, one adjacent on the bottom side, and one adjacent on top.
%           In general, we recommend manipulating as few data points as possible.
%           But sometimes, if only adjusting the Cp at the attachment line,
%           the resulting spline fit could add inappropriate oscillations.
%           To ameloriate this effect, the user can "spread" out the affected
%           region out.
%
%
%    -Cp  |               \                 o            o - data points
%         |                o               /           \ / - spline interpolated Cp
%         |                 \             /
%         |                  \           /
%         |                   \         o
%         |                    o       /
%         |                     \     /
%         |----------------------\---/-------------------- Cp isentropic
%         |                       \ /                      attachment line value
%         |                        v
%         |________________________o_______________________ s axis
%                (bottom)   attachment line     (top)
%
%       Figure 1: Schematic of Cp vs s zoomed in near the attachment line region.
%       Note that Cp is actually quadratic near the attachment line, but the limitation
%       of a constant font size does not allow the ASCII schematic to show this.
%
%           Note that even if the input Cp data does not overshoot the attachment
%           line value, the resulting spline fit on the overresolved s_fine can
%           produce Cp values beyond their isentropic limit.
%           default: [0 0]
%  - it_max_Cp
%    |--->  Specify the maximum number of iterations used in the Cp
%           isentropic-attachment-line shifting algorithm.
%           default: ceil(-5*log10(options.tol)) (i.e. five steps per order of magnitude).
%  - Cp_power
%    |--->  Specify the power to scale the Cp data points on each iteration
%           in the Cp isentropic-attachment-line shifting algorithm. The
%           methodology is outlined as follows:
%
%           1. Get the average of the anchored Cp data points (the ones which
%           are held constant throughout the algorithm). Let this be called
%           Cp_anchor_av.
%
%           2. For each iteration, scale the shifting Cp data points as follows:
%
%   Cp_UV_crct(i_stag_shifted) = Cp_fac*(Cp_UV_crct(i_stag_shifted)-Cp_anchor_av) + Cp_anchor_av;
%
%           where Cp_UV_crct is the array of corrected Cp data (changing per iteration);
%           i_stag_shifted are the indices of that array which will be shifted per
%           assignment of i_stag_shift_bounds; and where Cp_fac is defined as...
%
%   Cp_fac = abs((Cp_UV_stag-Cp_anchor_av)/(Cp_UV_fine(i_stag)-Cp_anchor_av+options.tol))^Cp_power;
%
%           The scaling factor, Cp_fac, is slightly imperfect, i.e. with
%           options.tol in the denominator, *all* Cp values will undershoot
%           the attachment line value, Cp_UV_stag, by at most the tolerance.
%           Note that this Cp_fac equation is where the input Cp_power comes
%           into play. A value of 1 is rather straightforward and thus the default.
%           But for some cases, the algorithm may be too accelerated with a value
%           of Cp_power = 1, so a value less than unity may be useful for deceleration.
%           default: 1
%
%   E) (Optional) Apply curve fits to Cp and/or the streamwise radius of curvature
%  - fit_Cp
%    |--->  Specify if the Cp input data should have a curve fit applied
%           to it using MATLAB's Curve Fitting Toolbox. The methodology
%           incorporating this input is as follows:
%
%           1. Some pressure coefficients received from inviscid solvers
%           or CFD solutions can have a coarse resolution, and one may be
%           curious the effect of a smoother Cp distribution has on the
%           resulting boundary layer. We approach this by maintaining
%           the input data near the attachment line, fitting the curves further
%           downstream on both sides of the airfoil, and joining the distinct
%           regions together with a cosine curve. See Figure 2 below.
%
%  fit(bottom)  cosine       original            cosine         fit(top)
% -------------|------|-------------------|------------------|-------------
% s axis ---->
%              1      2                   3                  4
%
%       Figure 2: Schematic of curve fitting vs s methodology in convert_airfoil
%
%           2. The fit uses a "SmoothingSpline" over the entire domain
%           with a given smoothing parameter. This number may be specified
%           to be different between the top and bottom sides of the airfoil.
%           See the input fit_Cp_smoothingparam for more details.
%           3. The distinct regions of Figure 2 are specified by surface
%           distance s away from the attachment line. See the input
%           fit_Cp_domain_bounds_perc for more details.
%           Implementation details can be found in fit_airfoil_array.m
%           default: false
%  - fit_Cp_smoothingparam
%    |--->  Specify the Cp smoothing parameter for the curve fit's smoothing
%           spline applied to both sides of the airfoil.
%           MATLAB's documentation for the smoothing parameter can be found at
%
%           https://www.mathworks.com/help/curvefit/fit.html#bto2vuv-1-fitOptions
%
%           If fit_Cp_smoothingparam input is a scalar, the same parameter
%           is applied to both sides of the airfoil. If it is given as a
%           two-element array, the first entry applies to the bottom side,
%           the second entry applies to the top side.
%           Determining the right value for this smoothing parameter takes
%           experimentation. To ascertain the right value, the authors
%           recommend iterating with options.airfoil.plot_Cp = 1 and
%           options.airfoil.debug = 1 until satisfied.
%           default: 8e-8 (the magic value)
%  - fit_Cp_domain_bounds_perc
%    |--->  A four-element array to specify the divisions of the Cp fit scheme
%           outlined in Figure 2. The units of this array are defined in the
%           following manner -- fractions of s away from the attachment line:
%
%           (s - s_attachmentline)/s_final
%
%           so a value of s=0 corresponds to the attachment line. Negative values
%           specify the bottom side of the airfoil, positive values specify
%           the top side.
%           Entry #1: s location where the curve fit and the cosine on the
%             bottom meet.
%           Entry #2: s location where the cosine on the bottom side and
%             the original data meet.
%           Entry #3: s location where the original data and the cosine on
%             the top side meet.
%           Entry #4: s location where the cosine and the curve fit on the
%             top side meet.
%           Determining the right values for this s locations takes
%           experimentation. To ascertain the right values, the authors
%           recommend iterating with options.airfoil.plot_Cp = 1 and
%           options.airfoil.debug = 1 until satisfied.
%           default: [-0.1 -0.03 0.03 0.1] in units of (s - s_attachmentline)/s_final
%  - fit_kappa_curv
%    |--->  Specify whether the calculated streamwise curvature should have
%           a curve fit applied to it or not. The methodology for the curve
%           fitting process is exactly the same as outlined above for fitting
%           the pressure coefficient, Cp. That is, see input fit_Cp for more
%           details.
%           Note that the curvature values modified here are *not* used at
%           all in the core of DEKAF because curvature terms are neglected
%           in the first-order boundary-layer approximation. This fitting
%           would only be useful for additional analysis applied on the boundary
%           layer that involves curvature (e.g. stability analysis).
%           default: false
%  - fit_kappa_smoothingparam
%    |--->  Specify the streamwise curvature smoothing parameter for the
%           curve fit's smoothing spline applied to both sides of the airfoil.
%           The implementation is exactly the same as fit_Cp_smoothingparam.
%           Determining the right value for this smoothing parameter takes
%           experimentation. To ascertain the right value, the authors
%           recommend iterating with options.airfoil.plot_R_mrch = 1 and
%           options.airfoil.debug = 1 until satisfied.
%           default: 8e-8 (the magic value)
%  - fit_kappa_domain_bounds_perc
%    |--->  A four-element array to specify the divisions of the streamwise
%           curvature fit scheme outlined in Figure 2. The implementation is
%           exactly the same as fit_Cp_domain_bounds_perc.
%           Determining the right value for this smoothing parameter takes
%           experimentation. To ascertain the right value, the authors
%           recommend iterating with options.airfoil.plot_R_mrch = 1 and
%           options.airfoil.debug = 1 until satisfied.
%           default: [-0.1 -0.03 0.03 0.1] in units of (s - s_attachmentline)/s_final
%
%   F) (Optional) Plots for debugging and presentations
%  - plot_Cp
%    |--->  If plot_Cp = 1, plot 3 figures related to the pressure coefficient.
%           1. If fit_Cp is true, then compare the curve fitted Cp to the original Cp data.
%           2. Show the manipulation of the near-attachment-line Cp data points
%              to maintain isentropic flow.
%           3. Show the final Cp of both sides of the airfoil vs. x/c.
%           default: false (0)
%  - plot_beta
%    |--->  If plot_beta = 1, plot 2 figures related to the beta-Hartree
%           parameter (or beta, for short).
%           1. Compare the directly calculated beta, the beta whose points
%              near the attachment line have been adjusted artifically to be near
%              unity, and the resulting beta output into DEKAF's
%              non-self-similar boundary-layer solver, all vs. s.
%           2. Show the output beta vs. (s - s_attachmentline)/max(s), over
%              both sides of the airfoil, and zoomed in near the attachment line.
%           default: false (0)
%  - plot_R_mrch
%    |--->  If plot_R_mrch = 1 and streamwise curvature has been fitted (i.e.
%           fit_kappa_curv is true), plot 2 figures related to the streamwise
%           curvature.
%           1. Streamwise curvature (or kappa, for short) vs. the surface
%              coordinate (s - s_max-curvature)/max(s), demonstrating the
%              effect of the curve fit against the unmanipulated data.
%              The legend in this figure is omitted; see the next figure
%              for the associated legend.
%           2. Radius of streamwise curvature (or R_mrch, for short) vs.
%              the surface coordinate (s - s_max-curvature)/max(s).
%           If plot_R_mrch = 1 and fit_kappa_curv = false, then only plot #2
%           is created showing the raw streamwise radius of curvature.
%           If plot_R_mrch = 2 and fit_kappa_curv = true, not only are the above
%           two figures plotted, but also a sequence of plots are made, showing
%           the effect of fitting curvature on the osculating circles of the
%           airfoil and the evolute. For more information, see input video_R_mrch_obj.
%           default: 0
%  - plot_xi
%    |--->  If plot_xi = 1, then two plots are made detailing the streamwise
%           marching coordinate xi, the dimensional surface coordinate s,
%           both against their respective indices.
%           1. The horizontal axis is the index percentage of the xi domain,
%              e.g. i_xi of 100% is the end of the xi domain. The xi domain input
%              into DEKAF is shown in blue as xi(xi_{interp}), and a xi
%              based on a linear s domain is shown in black for your
%              mapping identity reference. A dash-dot red line indicates the
%              xi value and index percentage where clustering in xi ends.
%              Dashed lines emanating from the axes correspond to the nodes'
%              nearest index percentages of [2:2:10 20:20:80]% of s.
%           2. The horizontal axis again is the index percentage, but of
%              the s domain now. The s domain input into DEKAF is shown in
%              blue as s(xi_{interp}), and a linear s distribution is plotted
%              for your mapping identity reference in black. A dash-dot
%              red line indicates the s value and index percentage where
%              clustering in s ends. Dashed lines emanating from the axes
%              correspond to the nodes' nearest index percentages of
%              [2:2:10 20:20:80]% of s.
%           The take-away of plot 2 is, the lower the blue curve, the *stronger*
%           the clustering in that s domain. And beyond the dash-dot red index,
%           the domain is underresolved with respect to the corresponding
%           equally-spaced domain.
%           default: false (0)
%  - video_R_mrch_obj
%    |--->  If plot_R_mrch = 2 and fit_kappa_curv = true, then this struct
%           provides the facilities to save the resulting sequence of
%           figures of osculating circles as a video -- an AVI file. These
%           fields of this struct are the following:
%      - video_R_mrch_obj.WriteAVI
%    |------->  If true, write the sequence of figures to an AVI file.
%               default: false
%      - video_R_mrch_obj.FrameRate
%    |------->  Rate of video playback in frames per second, specified as a
%               positive number.
%               default: 40
%      - video_R_mrch_obj.Quality
%    |------->  Video quality, specified as an integer in the range,
%               [0,100]. Higher quality numbers result in higher video
%               quality and larger file sizes. Lower quality numbers result
%               in lower video quality and smaller file sizes.
%               default: 25
%           See MATLAB's documentation on videowriter for more details:
%           https://www.mathworks.com/help/matlab/ref/videowriter.html
%      - video_R_mrch_obj.Filename
%    |------->  Output file name of the AVI file.
%               default: ['osculating-circles-evolute-' ...
%                   ,num2str(video_R_mrch_obj.FrameRate), 'fps-' ...
%                   ,num2str(video_R_mrch_obj.Quality), 'q'];
%
%   G) (Optional) Debugging
%  - debug
%    |--->  If true, after several key functions of convert_airfoil, interactive
%           prompts ask the user if they'd like to quit, enter keyboard mode
%           directly in the source code, or continue onward. This is essential
%           to figure out the proper values of fitting, or tweaking how the
%           Cp is adjusted to be isentropic at the attachment line, or
%           specifying the clustering of xi. These prompts occur after:
%           1. the Cp has been fitted (if fit_Cp is true);
%           2. the streamwise curvature kappa has been fitted (if
%              fit_kappa_curv is true); and
%           3. the end of the function, convert_airfoil, before the hand-off
%              to the quasi-self-similar solver in DEKAF.
%           default: false (0)
%
% Details on each field within intel:
%   A) Angle of attack and sweep    [deg]
%  - alpha_v_u and Lambda_w_uv
%    |--->  alpha_v_u is defined such that
%               tan(alpha_v_u) = V_inf/U_inf
%           and Lambda_w_uv is defined such that ...
%               tan(Lambda_w_uv) = W_inf/sqrt(U_inf^2 + V_inf^2)
%           NO default values.
%  - alpha_v_uw and Lambda_w_u
%    |--->  alpha_v_uw is defined such that
%               tan(alpha_v_uw) = V_inf/sqrt(U_inf^2 + W_inf^2)
%           and Lambda_w_u is defined such that ...
%               tan(Lambda_w_u) = W_inf/U_inf
%           NO default values.
%  - alpha_v_u and Lambda_w_u
%    |--->  alpha_v_u is defined such that
%               tan(alpha_v_u) = V_inf/U_inf
%           and Lambda_w_u is defined such that ...
%               tan(Lambda_w_u) = W_inf/U_inf
%           NO default values.
%  - alpha_v_uw and Lambda_w_uv
%    |--->  alpha_v_uw is defined such that
%               tan(alpha_v_uw) = V_inf/sqrt(U_inf^2 + W_inf^2)
%           and Lambda_w_u is defined such that ...
%               tan(Lambda_w_uv) = W_inf/sqrt(U_inf^2 + V_inf^2)
%           NO default values.
%
%   B) Freestream quantities
%  - T_inf
%    |--->  freestream temperature (absolute)
%           NO default value.
%  - M_inf_Q or M_inf_UV
%    |--->  freestream Mach number. A user may specify the Mach number
%           with either reference velocity Q_inf or UV_inf, defined as:
%
%           Q_inf = sqrt(U_inf^2 + V_inf^2 + W_inf^2)
%           UV_inf = sqrt(U_inf^2 + V_inf^2)
%
%           M_inf_Q = Q_inf/a_inf
%           M_inf_UV = UV_inf/a_inf
%
%           where a is the freestream speed of sound, sqrt(gamma*R*T_inf).
%           NO default value.
%   E) (Optional) Scaling Cp and y coordinates with Prandtl-Glauert
%  - M_inf_UV_data
%    |--->  the Mach number (with reference velocity UV_inf) at which the
%           Cp is determined. If not supplied, it is assumed that the Cp
%           data corresponds to M_inf_UV, i.e. Prandtl-Glauert (PG) is the
%           identity transformation. Otherwise, PG will transform the Cp
%           data set to M_inf_UV from M_inf_UV_data.
%           default: M_inf_UV
%  - p_inf
%    |--->  freestream pressure (absolute)
%           NO default value.
%  - ys_inf
%    |--->  freestream mass fractions of specified options.mixture. Order
%           of the mass fractions in the cell array are given in the comments
%           of the function, getDefault_composition_ys_e.
%           default: getDefault_composition_ys_e(options.mixture)
%
%           Note: it is recommended for most airfoil cases to use
%           options.mixture = 'Air' instead of the default established
%           inside setDefaults.
%
% Installation:
%   Standard usage of convert_airfoil doesn't require any additional
%     packages than the standard usage of DEKAF itself. However, the
%     curve-fitting features of fit_Cp and fit_kappa_curv require
%     MATLAB's Curve Fitting Toolbox. More information about the toolbox
%     can be found at the following URL:
%
%     https://www.mathworks.com/products/curvefitting.html
%     (Accessed 2020-02-13)
%
% Author(s): Koen Groot & Ethan Beyak
%
% GNU Lesser General Public License 3.0

%% 0. Establish defaults and perform preliminary checks
errmsgID = 'DEKAF:convert_airfoil';
if ~isfield(intel,'airfoil')
    error('To use convert_airfoil, the field ''airfoil'' must be provided to intel');
end
af = intel.airfoil;

% Establish protected values
% airfoil solutions will always be marching downstream of the attachment line
options.marchYesNo = protectedvalue(options,'marchYesNo',1);
% converting from infinity conditions to edge conditions use relations
options.flow = protectedvalue(options,'flow','CPG',...
    ['The relations used in convert_airfoil rely on a constant value of gamma.\n',...
    'Any flow assumption that isn''t options.flow = ''CPG'' is not permitted.']);
options.EoS         = protectedvalue(options,'EoS',         'idealGas');
options.coordsys    = protectedvalue(options,'coordsys',    'cartesian2D');
options.specify_cp  = protectedvalue(options,'specify_cp',  true);
options.specify_gam = protectedvalue(options,'specify_gam', true);

% Retrieving mixture constants
% Note: defaults of options have already been established in the scope
% above, so we do not need to output options from getAll_constants
mixCnst = getAll_constants(options.mixture,options);
afopts = options.airfoil;

% Note for convert_airfoil, we can set the defaults of gamma and R to be more
% general than the typical values for air (1.4 and 287.058 J/kg-K),
% if in the freestream, we assume the default composition of the mixture.
ys_inf_default = getDefault_composition_ys_e(options.mixture);
intel = standardvalue(intel,'ys_inf',ys_inf_default);
y_s_inf = cell2mat(intel.ys_inf);
gam_inf_default = getMixture_gam(intel.T_inf,intel.T_inf,intel.T_inf,intel.T_inf,intel.T_inf,y_s_inf,...
    mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,...
    mixCnst.gDegen_s,mixCnst.bElectron,options,mixCnst.AThermFuncs_s);
[~,R_inf_default,~,~] = getMixture_Mm_R(y_s_inf,intel.T_inf,intel.T_inf,mixCnst.Mm_s,...
    mixCnst.R0,mixCnst.bElectron);
cp_inf_default = gam_inf_default*R_inf_default/(gam_inf_default - 1); % Assume a calorically perfect gas

intel = standardvalue(intel,'cp',cp_inf_default,['The specific heat at constant pressure was not specified.\n', ...
    'The default of ',num2str(cp_inf_default,'%.16e'),' J/kg-K was used!']);
intel = standardvalue(intel,'gam',gam_inf_default,['The ratio of specific heats was not specified.\n', ...
    'The default of ',num2str(gam_inf_default,'%.16e'),' was used!']);
intel.R_e = intel.cp*(1 - 1/intel.gam); % Assume a calorically perfect gas
mixCnst.cp_CPG = intel.cp;
intel.mixCnst = mixCnst;

% Geometry
afopts = standardvalue(afopts,'fit_Cp',false);
afopts = standardvalue(afopts,'fit_Cp_smoothingparam',8e-8);
afopts = standardvalue(afopts,'fit_Cp_domain_bounds_perc',[-0.1 -0.03 0.03 0.1]);
afopts = standardvalue(afopts,'data_orientation','cw_from_bottom_TE');
afopts = standardvalue(afopts,'s_hyperinterp_fac',10);
afopts = standardvalue(afopts,'i_stag_shift_bounds',[0 0]);
afopts = standardvalue(afopts,'it_max_Cp',ceil(-5*log10(options.tol)));
afopts = standardvalue(afopts,'Cp_power',1);
s_hyperinterp_fac = afopts.s_hyperinterp_fac;
afopts = standardvalue(afopts,'fit_kappa_curv',false);
afopts = standardvalue(afopts,'fit_kappa_smoothingparam',8e-8);
afopts = standardvalue(afopts,'fit_kappa_domain_bounds_perc',[-0.1 -0.03 0.03 0.1]);

% Interactive
afopts = standardvalue(afopts,'debug',0);
debugstr = '>>> airfoil.debug enabled: q to quit, k to debug, any other button to continue\n';
abortstr = 'DEKAF terminated by user input.';

% Plotting + Videos
afopts = standardvalue(afopts,'plot_Cp',false);
afopts = standardvalue(afopts,'plot_beta',false);
afopts = standardvalue(afopts,'plot_xi',false);
afopts = standardvalue(afopts,'plot_R_mrch',afopts.fit_kappa_curv);
afopts = standardvalue(afopts,'video_R_mrch_obj',struct);
afopts.video_R_mrch_obj = standardvalue(afopts.video_R_mrch_obj,'WriteAVI',false);
afopts.video_R_mrch_obj = standardvalue(afopts.video_R_mrch_obj,'FrameRate',40);
afopts.video_R_mrch_obj = standardvalue(afopts.video_R_mrch_obj,'Quality',25);
afopts.video_R_mrch_obj = standardvalue(afopts.video_R_mrch_obj,'Filename'...
    ,['osculating-circles-evolute-', num2str(afopts.video_R_mrch_obj.FrameRate), 'fps-' ...
    , num2str(afopts.video_R_mrch_obj.Quality), 'q']);

% Number of strings for messages
Nstr = dispNstr();

%% 1. Infinity value specification
% Compute the four angles describing the input freestream velocity vector
% (only two are required to uniquely define the angle of attack and sweep)
%
% For the uninitiated, we use this subscript notation for these angles:
%
% ** tan(alpha)  = v/u corresponds to alpha_v_u
% ** tan(alpha)  = v/(u^2 + w^2)^(1/2) corresponds to alpha_v_uw
% ** tan(Lambda) = w/u corresponds to Lambda_w_u
% ** tan(Lambda) = w/(u^2 + v^2)^(1/2) corresponds to Lambda_w_uv
% ... and (u^2 + v^2)^(1/2), we often refer to as the "in-plane resultant".

% See equations 33-36 in AIAA 2020-1024 by Groot, Beyak, Heston, Reed
if isfield(intel,'alpha_v_u') && isfield(intel,'Lambda_w_uv') % sweep, then pitch
    alpha_v_u = intel.alpha_v_u;
    Lambda_w_uv = intel.Lambda_w_uv;
    alpha_v_uw = asind(sind(alpha_v_u)*cosd(Lambda_w_uv));
    Lambda_w_u = atan2d(tand(Lambda_w_uv)*secd(alpha_v_u),1);
    intel.alpha_v_uw = alpha_v_uw;
    intel.Lambda_w_u = Lambda_w_u;
elseif isfield(intel,'alpha_v_uw') && isfield(intel,'Lambda_w_u') % pitch, then sweep
    alpha_v_uw = intel.alpha_v_uw;
    Lambda_w_u = intel.Lambda_w_u;
    Lambda_w_uv = asind(sind(Lambda_w_u)*cosd(alpha_v_uw));
    alpha_v_u = atan2d(tand(alpha_v_uw)*secd(Lambda_w_u),1);
    intel.alpha_v_u = alpha_v_u;
    intel.Lambda_w_uv = Lambda_w_uv;
elseif isfield(intel,'alpha_v_u') && isfield(intel,'Lambda_w_u')
    alpha_v_u = intel.alpha_v_u;
    Lambda_w_u = intel.Lambda_w_u;
    % Note equations 36a and 36b have typos in the original AIAA 2020-1024
    % Those 2 formulae ought to have their angles flipped as the following:
    Lambda_w_uv = atan2d(tand(Lambda_w_u)*cosd(alpha_v_u),1);
    alpha_v_uw = atan2d(tand(alpha_v_u)*cosd(Lambda_w_u),1);
    intel.alpha_v_uw = alpha_v_uw;
    intel.Lambda_w_uv = Lambda_w_uv;
elseif isfield(intel,'alpha_v_uw') && isfield(intel,'Lambda_w_uv')
    alpha_v_uw = intel.alpha_v_uw;
    Lambda_w_uv = intel.Lambda_w_uv;
    Lambda_w_u = asind(sind(Lambda_w_uv)/cosd(alpha_v_uw));
    alpha_v_u = asind(sind(alpha_v_uw)/cosd(Lambda_w_uv));
    intel.alpha_v_u = alpha_v_u;
    intel.Lambda_w_u = Lambda_w_u;
else
    error(['Invalid inputs for the geometric angles. Must provide one of the four sets:\n', ...
        '{alpha_v_u & Lambda_w_uv, alpha_v_uw & Lambda_w_u,\n'...
        ' alpha_v_u & Lambda_w_u,  alpha_v_uw & Lambda_w_uv}.'],errmsgID)
end

% Decompose input Mach into its components
% To get a solid three-dimensional view of these angles and their relations,
% see Fig. 16 in Appendix B of AIAA 2020-1024 by Groot et al.
if isfield(intel,'M_inf_Q') && isfield(intel,'M_inf_UV')
    error('Must specify only one of the fields {M_inf_Q, M_inf_UV}.');
elseif isfield(intel,'M_inf_Q')
    M_inf_Q = intel.M_inf_Q;
    M_inf_UV = M_inf_Q*cosd(Lambda_w_uv);
elseif isfield(intel,'M_inf_UV')
    M_inf_UV = intel.M_inf_UV;
    M_inf_Q = M_inf_UV/cosd(Lambda_w_uv);
else
    error('Must specify one of the fields {M_inf_Q, M_inf_UV}.');
end
M_inf_U = M_inf_UV*cosd(alpha_v_u);
M_inf_V = M_inf_UV*sind(alpha_v_u);
M_inf_W = M_inf_Q*sind(Lambda_w_uv);
a_inf = sqrt(intel.gam*intel.R_e*intel.T_inf);
U_inf = M_inf_U*a_inf;
V_inf = M_inf_V*a_inf;
UV_inf = sqrt(U_inf^2 + V_inf^2);
W_0 = M_inf_W*a_inf;
Q_inf = M_inf_Q*a_inf;
rho_inf = intel.p_inf/(intel.R_e*intel.T_inf);
h_inf = intel.cp*intel.T_inf; % Assume calorically perfect gas
q_inf_UV = 1/2*rho_inf*UV_inf^2;

% M_inf_UV_data is the Mach number at which the Cp is determined.
% If not supplied, it is assumed that the Cp data corresponds to the above M_inf_UV,
% i.e. Prandtl-Glauert (PG) is the identity transformation. Otherwise, PG will
% transform the Cp data set to M_inf_UV.
af = standardvalue(af,'M_inf_UV_data',M_inf_UV);
M_inf_UV_data = af.M_inf_UV_data;

%% 2. Data reorientation and dimensionalization
% Reorient Cp data as a row.
% Locally we call this the unfit Cp, to accomodate for the fit Cp if desired.
Cp_UV_data_unfit = af.Cp_UV_data(:).';

% Enforce input data assumption to be oriented clockwise (CW) from bottom
% trailing edge (TE) to top TE
switch afopts.data_orientation
    case 'cw_from_bottom_TE'
        % do nothing: already the correct format
    case 'ccw_from_top_TE'
        af.xc_data = flip(af.xc_data);
        Cp_UV_data_unfit = flip(Cp_UV_data_unfit);
        af.xc_geom_data = flip(af.xc_geom_data);
        af.yc_geom_data = flip(af.yc_geom_data);
    otherwise
        error(['Must specify one of the values for airfoil.data_orientation:\n' ...
            '{cw_from_bottom_TE, ccw_from_top_TE}'],errmsgID);
end

% Dimensionalize x/c of the Cp data
x_data = af.xc_data*af.chord_U;
% ... and the geometry data's x/c and y/c to be x and y (in meters)
x_geom_data = af.xc_geom_data*af.chord_U;
y_geom_data = af.yc_geom_data*af.chord_U;

%% 3. Build s coordinates for geometric and Cp data
method_interp = 'spline';
s_geom_data = get_airfoil_arc_length(x_geom_data, y_geom_data);
s_data = build_s_data(x_geom_data, s_geom_data, x_data, method_interp);

%% 4. Create a fitted version of Cp data, if desired.
axfs = 15;
fs = 1.2*axfs;
nmrk = 30;
lwCp = 1.5;
lopts = {'interpreter','latex','edgecolor','none'};
axopts = {'interpreter','latex','fontsize',fs};
axgcaopts = {'fontsize',axfs,'ticklabelinterpreter','latex'};
fitmrk = {'v','<','>','^'};

if afopts.fit_Cp
    dispif('--- Fitting the pressure coefficient...',options.display);
    [~,i_stag_data_tmp] = max(Cp_UV_data_unfit); % index of max Cp (signed)
    [Cp_UV_data, Cp_UV_data_wf, is_Cp_db] = fit_airfoil_array(Cp_UV_data_unfit, s_data, i_stag_data_tmp, ...
        afopts.fit_Cp_smoothingparam, afopts.fit_Cp_domain_bounds_perc*s_data(end));
    if afopts.plot_Cp
        fitCplgnd = {'raw','whole fit (B)','whole fit (T)','fit','$q=0$','$\max q$',...
                    'fit data start (B)','raw data end (B)','raw data end (T)','fit data start (T)'};
        fnumCpv(1) = figure;
        clf(fnumCpv(1));
        sCprawdataendv = s_data(is_Cp_db(2:3)); % Raw data end bounds: [bottom, top]
        sCpfitstartv = s_data(is_Cp_db([1 4])); % Fit start bounds: [bottom, top]
        s_bottom = (s_data(1:max(is_Cp_db))-s_data(i_stag_data_tmp))/s_data(end);
        s_top = (s_data(min(is_Cp_db):end)-s_data(i_stag_data_tmp))/s_data(end);
        mrkindsbottom = round(linspace(1,length(s_bottom),nmrk));
        mrkindsbottom([end+1 end+2]) = is_Cp_db(3:4);
        mrkindstop = round(linspace((mrkindsbottom(2) - mrkindsbottom(1))/2,length(s_top),nmrk));
        mrkindstop([end+1 end+2]) = is_Cp_db(1:2);
        npl = 10;
        hCpv = gobjects(1,npl);
        for ipl = 1:2
            subplot(2,1,ipl)
            % Raw Cp from input
            hCpv(1) = plot((s_data-s_data(i_stag_data_tmp))/s_data(end), Cp_UV_data_unfit ...
                ,'color', 'k' ...
                ,'linestyle', '-' ...
                ,'linewidth', lwCp ...
                );
            hold on
            % Whole fit on the left side
            hCpv(2) = plot(s_bottom, Cp_UV_data_wf(1,  1:max(is_Cp_db)) ...
                ,'color', [0.2 0.85 0.5] ...
                ,'linestyle', '-' ...
                ,'linewidth', lwCp ...
                ,'marker', '<' ...
                ,'markerindices', mrkindsbottom ...
                );
            % Whole fit on the right side
            hCpv(3) = plot(s_top, Cp_UV_data_wf(2,min(is_Cp_db):end) ...
                ,'color', [0.2 0.85 0.5] ...
                ,'linestyle', '-' ...
                ,'linewidth', lwCp ...
                ,'marker', '>' ...
                ,'markerindices', mrkindstop ...
                );
            % Resulting distribution blending the whole fits and the raw data
            hCpv(4) = plot((s_data-s_data(i_stag_data_tmp))/s_data(end), Cp_UV_data ...
                ,'color', [0.8 0.2 0]...
                ,'linestyle', '--' ...
                ,'linewidth', lwCp ...
                );
            % Cp = 0 line
            hCpv(5) = plot(([0 max(s_data)]-s_data(i_stag_data_tmp))/s_data(end), [0 0], 'k-');
            mxCp = max([Cp_UV_data Cp_UV_data_unfit]);
            mnCp = min([Cp_UV_data Cp_UV_data_unfit]);
            % s = attachment line line
            hCpv(6) = plot(0*[1 1],1.1*[mnCp mxCp],'-.k');

            % bounding lines showing when the fit starts and when the raw data ends
            bopt = {'linestyle','-.','color','k','markerfacecolor',0.7*[1 1 1],'marker'};
            hCpv(7) = plot((sCpfitstartv(1) - s_data(i_stag_data_tmp))/s_data(end)*[1 1 1],...
                1.1*[mnCp (mnCp+mxCp)/2 mxCp],bopt{:},fitmrk{1}); % bottom
            hCpv(8) = plot((sCprawdataendv(1) - s_data(i_stag_data_tmp))/s_data(end)*[1 1 1],...
                1.1*[mnCp (mnCp+mxCp)/2 mxCp],bopt{:},fitmrk{2}); % left
            hCpv(9) = plot((sCprawdataendv(2) - s_data(i_stag_data_tmp))/s_data(end)*[1 1 1],...
                1.1*[mnCp (mnCp+mxCp)/2 mxCp],bopt{:},fitmrk{3}); % right
            hCpv(10) = plot((sCpfitstartv(2) - s_data(i_stag_data_tmp))/s_data(end)*[1 1 1],...
                1.1*[mnCp (mnCp+mxCp)/2 mxCp],bopt{:},fitmrk{4}); % top
            grid on

            s_Cpxtr = ([sCpfitstartv(1) sCpfitstartv(2)] - s_data(i_stag_data_tmp))/s_data(end);
            xlabel('$(s-s_{\mathrm{stag}})/\max s$',axopts{:})
            ylabel('$C_p^{\overline{u}_\infty \overline{v}_\infty}$',axopts{:})
            set(gca,'ydir','reverse');
            set(gca,'ytick',-1:0.5:1);
            set(gca,'yticklabel',flipud({'-1','-$\frac{1}{2}$','0','$\frac{1}{2}$','1'}));
            set(gca,axgcaopts{:});
        end
        lhCp = legend(hCpv, fitCplgnd,'interpreter','latex','edgecolor','none',axopts{:});
        subplot(2,1,1)
        xlim([-inf inf]) % general
        ylim([mnCp mxCp])
        set(gca,'position',[0.05093 0.59380 0.73726 0.39078])
        text((0.25*max(s_data)-s_data(i_stag_data_tmp))/s_data(end),1,'bottom',axopts{:},'verticalalignment','bottom')
        text((0.75*max(s_data)-s_data(i_stag_data_tmp))/s_data(end),1,'top',axopts{:},'verticalalignment','bottom')
        subplot(2,1,2)
        axis([s_Cpxtr+1*[-1 1]*max(abs(s_Cpxtr)) 1.1*[min(Cp_UV_data_unfit(is_Cp_db(:))) mxCp]])
        set(gca,'position',[0.05094 0.09625 0.73727 0.401305]);
        set(lhCp,'position', [0.79441 0.57976 0.19504 0.41973]);
        set(lhCp,'fontsize',axfs,'color','none')
        set(fnumCpv(1),'color','w')
        drawnow
        if afopts.debug
            userkeyboard = input(debugstr,'s');
            if strcmpi(userkeyboard,'q')
                error(abortstr);
            elseif strcmpi(userkeyboard,'k')
                keyboard
            end
        end
    end
else
    Cp_UV_data = Cp_UV_data_unfit;
end

%% 5. Create ultra-fine s coordinates and interpolate x, y, and Cp_UV
[~,i_stag_data] = max(Cp_UV_data);
s_fine = build_s_fine(s_data, max([s_data; s_geom_data]), ...
    min([diff(s_data); diff(s_geom_data)])/s_hyperinterp_fac, i_stag_data);
% Note that s_fine is equally-spaced. There is current no streamwise
% mapping performed, thus implicitly hard-coding 'linear' s spacing.
N_s = length(s_fine);
if ~N_s
    error(['N_s is equal to zero. Double check the orientation of the input data\n', ...
        'Please contact the developers for support!'],errmsgID);
end

% Construct the isentropic Cp for the attachment line
if M_inf_UV_data == 0 % exactly incompressible
    Cp_UV_stag = 1;
else
    Cp_UV_stag = 2/(intel.gam*M_inf_UV_data^2) * ((1+(intel.gam-1)/2*M_inf_UV_data^2)^(intel.gam/(intel.gam-1)) - 1);
end

% Interpolate out global x, y, and Cp_UV onto the ultra-fine s
x_fine = interp1(s_geom_data, x_geom_data, s_fine, method_interp);
y_fine = interp1(s_geom_data, y_geom_data, s_fine, method_interp);

% Shift the x coordinates to never be negative (for our sanity under
% square root operations and setting the leading edge to be zero)
x_fine = x_fine - min(x_fine);

% Interpolate from data resolution to the fine grid
Cp_UV_fine = Cp_UV_stag - interp1(s_data, Cp_UV_stag - Cp_UV_data, s_fine, method_interp);
[~,i_stag] = max(Cp_UV_fine);

hCpvary = gobjects(1,4);
lgndCpvary = cell(1,4);
mrksz = 10;
if afopts.plot_Cp
    % Start making the plot for the Cp correction
    fnumCpv(2) = figure;
    clf(fnumCpv(2));
    set(fnumCpv(2),'color','w')
    hCpvary(1) = plot(s_data, Cp_UV_data, 'k-o');
    lgndCpvary{1} = 'Input data';
    if afopts.fit_Cp
        lgndCpvary{1} = [lgndCpvary{1}, ', fitted'];
    end
    hold on
    hCpvary(2) = plot(s_fine, Cp_UV_fine, 'b-p');
    lgndCpvary{2} = ['Overresolved via ',method_interp];
    hCpvary(3) = plot(s_data(i_stag_data), Cp_UV_data(i_stag_data), ...
        'bp', 'markerfacecolor', [0.7 0.7 1], 'markersize', mrksz);
    lgndCpvary{3} = 'Stagnation point of input data';
    hCpvary(4) = plot([min(s_geom_data) max(s_geom_data)], Cp_UV_stag*[1 1], 'r--');
    lgndCpvary{4} = ['Maximum isentropic pressure, ',...
        '$C_{p,\mathrm{stag}}^{\overline{u}_\infty \overline{v}_\infty}$'];
    xlabel('$s$ (m)',axopts{:})
    ylabel('$C_p^{\overline{u}_\infty \overline{v}_\infty}$',axopts{:})
    grid on
    grid minor
    set(gca,axgcaopts{:},'ydir','reverse');

    % Plot the unaltered data
    fnumCpv(3) = figure;
    clf(fnumCpv(3));
    set(fnumCpv(3),'color','w')
    hCp = gobjects(1,2);
    hCp(1) = plot(x_data(i_stag_data:end)/af.chord_U, Cp_UV_data(i_stag_data:end),'.r-');
    hold on
    hCp(2) = plot(x_data(1:i_stag_data)/af.chord_U, Cp_UV_data(1:i_stag_data),'.b-');
    xlabel('$x/c$',axopts{:})
    ylabel('$C_p^{\overline{u}_\infty \overline{v}_\infty}$',axopts{:})
    grid on
    grid minor
    set(gca,axgcaopts{:},'ydir','reverse');
    legend(hCp, {'top', 'bottom'}, 'location','southeast',lopts{:});
    xlim([0 inf])
    drawnow
end

% Note that Cp_UV_data may have some thermodynamically unphysical data
% due to an unconverged basic state. For example, at the attachment line,
% Cp_UV_data may exceed Cp_UV_stag. If this occurs, we choose to shift the
% two Cp data points surrounding the overshoot, until the resplined data
% undershoots the isentropic attachment line Cp-value.
dispif(['--- Relative error of interpolated over isentropic attachment line Cp_UV is: ', ...
    num2str(max(Cp_UV_fine)/Cp_UV_stag-1,'%.3e'),' (neg.: undershoot, pos.: overshoot)'],1)
it = -1;
need_to_correct_Cp = abs(max(Cp_UV_fine)/Cp_UV_stag-1) > options.tol || max(Cp_UV_fine) > Cp_UV_stag;
if need_to_correct_Cp
    warning('Adjusting Cp data so that splined maximum attains the attachment line value')

    % Extend the arrays for the line handles and legend
    hCpvary(5:9) = gobjects(1,5);
    lgndCpvary(5:9) = cell(1,5);

    % Initialize corrected Cp_UV_data
    Cp_UV_crct = Cp_UV_data;

    % Find the number of Cp data points that exceed the attachment line isentropic Cp
    % If none exceed it, then it was an undershoot, and we only shift the
    % neighboring 2 data points.
    % If only 1 point exceeds, we still would like to shift 2 points
    % because the following algorithm depends on this Cp_fac which is
    % an average of two data points -- if only shifting one point, then
    % it will likely be near-symmetric and the average will not be able to
    % "pull" on the shifting data points.
    i_stag_shifted = find(Cp_UV_crct > Cp_UV_stag);
    if isempty(i_stag_shifted)
        dispif('--- No data points are non-isentropic! Ensuring spline does not overshoot',1);
        i_stag_shifted = i_stag_data;
    end
    lshift = afopts.i_stag_shift_bounds(1);
    rshift = afopts.i_stag_shift_bounds(2);
    i_stag_anchors = [min(i_stag_shifted)-lshift-1, max(i_stag_shifted)+rshift+1];
    i_stag_shifted = min(i_stag_shifted)-lshift:max(i_stag_shifted)+rshift;

    % Calculate the average of the anchored Cp values that drives the
    % shifting of the Cp values that directly neighbor the attachment-line
    % point: aka Cp_anchor_av
    Cp_anchor_av = mean(Cp_UV_crct(i_stag_anchors));

    for it = 1:afopts.it_max_Cp
        % We expect that Cp near attachment line varies quadratically with s.
        % Therefore, we define a factor to correct the Cp_anchor_av to the
        % anchoring Cp value according to the quadratic dependence. We are
        % doing this imperfectly by adding a tolerance to the denominator,
        % such that all Cp values will undershoot the attachment line value by
        % at most the tolerance
        Cp_fac = abs((Cp_UV_stag-Cp_anchor_av)/(Cp_UV_fine(i_stag)-Cp_anchor_av+options.tol))^afopts.Cp_power;

        % Apply factor to Cp_anchor_av
        Cp_UV_crct(i_stag_shifted) = Cp_fac*(Cp_UV_crct(i_stag_shifted)-Cp_anchor_av) + Cp_anchor_av;

        % Reinterpolate the Cp-values on the entire fine resolution domain
        Cp_UV_fine = Cp_UV_stag - interp1(s_data, Cp_UV_stag-Cp_UV_crct, s_fine, method_interp);

        % Correct the attachment line index
        [~,i_stag] = max(Cp_UV_fine);

        % Assign the exit condition: the residual of the Cp with respect to its isentropic value
        Cp_ratio_residual = max(Cp_UV_fine)/Cp_UV_stag - 1;

        if afopts.plot_Cp
            figure(fnumCpv(2))
            hCpvary(5) = plot(s_fine, Cp_UV_fine, '-', 'color', magma(it,afopts.it_max_Cp));
            hCpvary(6) = plot(s_data(i_stag_anchors), Cp_UV_crct(i_stag_anchors), 'og');
            hCpvary(7) = plot(s_data(i_stag_shifted), Cp_UV_crct(i_stag_shifted), 'om');
            anchmin = min(s_data(i_stag_anchors));
            anchmax = max(s_data(i_stag_anchors));
            Cpcrctmax = max([Cp_UV_stag, max(Cp_UV_crct(i_stag_anchors))]);
            Cpcrctmin = min(Cp_UV_data(i_stag_anchors));
            xlim([-(anchmax-anchmin)/4 + anchmin, (anchmax-anchmin)/4 + anchmax])
            ylim([(Cpcrctmin - Cpcrctmax)/4 + Cpcrctmin, -(Cpcrctmin - Cpcrctmax)/2 + Cpcrctmax])
        end

        % And write out iteration information to the screen
        litm = num2str(length(num2str(afopts.it_max_Cp))); % length of itmax string
        itstr{1} = ['--- iteration: ', sprintf(['%0',litm,'d'],it) ,'/', num2str(afopts.it_max_Cp), ' '];
        itstr{3} = [' ',num2str(Cp_ratio_residual,'%+.3e')];
        itstr{2} = repmat('.',1,Nstr - length(itstr{1}) - length(itstr{3}));
        dispif([itstr{:}],1);
        % Check if the Cp correction converged
        if abs(Cp_ratio_residual) <= options.tol
            break
        end
    end
end
if afopts.plot_Cp
    % Finalize the plot for the Cp correction
    figure(fnumCpv(2))
    if need_to_correct_Cp
        utkorange = [255 130 0]/255;
        utkorangelite = [255 190 0]/255;
        lgndCpvary{5} = 'Intermediate $C_p^{\overline{u}_\infty \overline{v}_\infty}$-distributions';
        lgndCpvary{6} = 'Adjustment anchors of input data';
        lgndCpvary{7} = 'Shifting points of input data';
        hCpvary(8) = plot(s_fine, Cp_UV_fine, '-s','color',utkorange,...
            'markerfacecolor', utkorangelite);
        lgndCpvary{8} = 'Final distribution';
        plot(s_data(i_stag_anchors), Cp_UV_crct(i_stag_anchors), 'og');
        plot(s_data(i_stag_shifted), Cp_UV_crct(i_stag_shifted), 'om');

        hCpvary(9) = plot(s_fine(i_stag), Cp_UV_fine(i_stag), ...
            's', 'color', utkorange, 'markerfacecolor', utkorangelite, 'markersize', mrksz);
        lgndCpvary{9} = 'Final attachment line';
        anchmin = min(s_data(i_stag_anchors));
        anchmax = max(s_data(i_stag_anchors));
        Cpcrctmax = max([Cp_UV_stag, max(Cp_UV_crct(i_stag_anchors))]);
        Cpcrctmin = min(Cp_UV_data(i_stag_anchors));
        xlim([-(anchmax-anchmin)/4 + anchmin, (anchmax-anchmin)/4 + anchmax])
        ylim([(Cpcrctmin - Cpcrctmax)/4 + Cpcrctmin, -(Cpcrctmin - Cpcrctmax)/2 + Cpcrctmax])
    end
    legend(hCpvary,lgndCpvary,'location','best',lopts{:})
    drawnow
end
if it == afopts.it_max_Cp
    error(['The Cp correction did not work within a reasonable number of iterations!\n', ...
        'Increase your ''airfoil.it_max_Cp'' if it appears it''s converging but hasn''t saturated.\n'...
        'If it saturated before the convergence level of options.tol, then\n'...
        'double-check that your data corresponds to the input ''airfoil.M_inf_UV_data value''.\n'...
        'If the Cp is unable to become isentropic, you may need to alter Cp_power,\n'...
        'or as a last resort, increase options.tol to the level of saturation of the Cp correction.\n'...
        'If that still doesn''t work, contact the developers for support!'],errmsgID)
end

%% 6. Perform Prandtl-Glauert scaling for Cp_UV up to the specified M_inf_UV
[y_M_inf_fine, Cp_UV_M_inf_fine] = apply_prandtl_glauert(...
    y_fine, Cp_UV_fine, M_inf_UV_data, M_inf_UV);

%% 7. Calculate radii of curvature and wall-normal angles w.r.t. global x
% Calculate s derivatives
ds_fine = s_fine(2) - s_fine(1);
% The only reason for us to use this stencil (centered, of constant 4th order
% accuracy) is for the sparse behavior of the differentiation matrices formed in FDdif.
[D1_s, D2_s] = FDdif(N_s, ds_fine, 'centered_cst4','sparse');
dx_ds = (D1_s*x_fine.').';
dy_ds = (D1_s*y_M_inf_fine.').';
dx_ds2 = (D2_s*x_fine.').';
dy_ds2 = (D2_s*y_M_inf_fine.').';

% Formula is from wiki: https://en.wikipedia.org/wiki/Radius_of_curvature#Definition
% Last accessed: 2020-02-11
% The leading negative sign assures positive R values for convex curvature.
kappa_cuf = -(dx_ds.*dy_ds2 - dy_ds.*dx_ds2)./(dx_ds.^2 + dy_ds.^2).^(3/2);
[~,i_maxc] = max(kappa_cuf); % index of max curvature (signed)
lwk = 1.5;
if afopts.fit_kappa_curv
    dispif('--- Fitting the curvature for the airfoil''s surface...',options.display);
    [kappa_curvature, kappa_cf, is_kappa_db] = fit_airfoil_array(kappa_cuf, s_fine, i_maxc, ...
        afopts.fit_kappa_smoothingparam, afopts.fit_kappa_domain_bounds_perc*s_fine(end));
    if afopts.plot_R_mrch
        skapparawdataendv = s_fine(is_kappa_db(2:3)); % Raw data end bounds: [bottom, top]
        skappafitstartv = s_fine(is_kappa_db([1 4])); % Fit start bounds: [bottom, top]
        fnumkappav(1) = figure;
        clf(fnumkappav(1))
        set(fnumkappav(1),'color','w')
        % Raw kappa from input (calculated with finite differences)
        plot((s_fine-s_fine(i_maxc))/s_fine(end), kappa_cuf ...
            ,'color', 'k' ...
            ,'linestyle', '-' ...
            ,'linewidth', lwk ...
            );
        hold on
        % Whole fit across the entire domain
        plot((s_fine-s_fine(i_maxc))/s_fine(end), kappa_cf ...
            ,'color', [0.2 0.85 0.5] ...
            ,'linestyle', '--' ...
            ,'linewidth', lwk ...
            );
        % Resulting distribution blending the whole fit and the raw data
        plot((s_fine-s_fine(i_maxc))/s_fine(end), kappa_curvature ...
            ,'color', [0.8 0.2 0]...
            ,'linestyle', '--' ...
            ,'linewidth', lwk ...
            );
        % kappa = 0 line
        plot(([0 max(s_fine)]-s_fine(i_maxc))/s_fine(end), [0 0], 'k-');
        mxkp = max([kappa_curvature kappa_cuf]);
        mnkp = min([kappa_curvature kappa_cuf]);
        % s = s_maxcurv line
        plot(0*[1 1],1.1*[mnkp mxkp],'-.k');

        % bounding lines showing when the fit starts and when the raw data ends
        bopt = {'linestyle','-.','color','k','markerfacecolor',0.7*[1 1 1],'marker'};
        plot((skappafitstartv(1) - s_fine(i_maxc))/s_fine(end)*[1 1 1],1.1*[mnkp 0 mxkp],bopt{:},fitmrk{1}); % bottom
        plot((skapparawdataendv(1) - s_fine(i_maxc))/s_fine(end)*[1 1 1],1.1*[mnkp 0 mxkp],bopt{:},fitmrk{2}); % left
        plot((skapparawdataendv(2) - s_fine(i_maxc))/s_fine(end)*[1 1 1],1.1*[mnkp 0 mxkp],bopt{:},fitmrk{3}); % right
        plot((skappafitstartv(2) - s_fine(i_maxc))/s_fine(end)*[1 1 1],1.1*[mnkp 0 mxkp],bopt{:},fitmrk{4}); % top
        grid on
        axis([([0 max(s_fine)] - s_fine(i_maxc))/s_fine(end) 1.1*[mnkp mxkp]])
        xlabel('$(s-s_{\mathrm{max\, curv}})/\max s$',axopts{:})
        ylabel('$\kappa_c$ [1/m]',axopts{:})
        set(gca,axgcaopts{:});
        set(gca,'position',[0.060515 0.10017 0.928750 0.89073]);
    end
else
    kappa_curvature = kappa_cuf;
end
R_mrch_fine = 1./kappa_curvature;
R_mrch_uf = 1./kappa_cuf; % Output into final intel.airfoil struct
if afopts.fit_kappa_curv
    R_mrch_f = 1./kappa_cf;
else
    R_mrch_f = NaN;
end
if afopts.plot_R_mrch
    xlblkappa = '$(s-s_{\mathrm{max\, curv}})/\max s$';
    ylblkappa = '$R_c$ [m]';
    medianR = abs(median(R_mrch_uf));
    Raxlims = [([0 max(s_fine)]-s_fine(i_maxc))/s_fine(end) -2.5*medianR 2.5*medianR];
    fnumkappav(2) = figure;
    clf(fnumkappav(2))
    set(fnumkappav(2),'color','w')
    if afopts.fit_kappa_curv
        npl = 9;
        hkv = gobjects(1,npl);
        for ipl = 1:2
            subplot(2,1,ipl)
            % Raw R_mrch from input (calculated with finite differences for kappa)
            hkv(1) = plot((s_fine-s_fine(i_maxc))/s_fine(end), R_mrch_uf ...
                ,'color', 'k' ...
                ,'linestyle', '-' ...
                ,'linewidth', lwk ...
                );
            hold on
            % Whole fit across the entire domain
            hkv(2) = plot((s_fine-s_fine(i_maxc))/s_fine(end), R_mrch_f ...
                ,'color', [0.2 0.85 0.5] ...
                ,'linestyle', '-' ...
                ,'linewidth', lwk ...
                );
            % Resulting distribution blending the whole fit and the raw data
            hkv(3) = plot((s_fine-s_fine(i_maxc))/s_fine(end), R_mrch_fine ...
                ,'color', [0.8 0.2 0] ...
                ,'linestyle', '--' ...
                ,'linewidth', lwk ...
                );

            mxRp = max([R_mrch_fine R_mrch_uf]);
            mnRp = min([R_mrch_fine R_mrch_uf]);
            % R = 0 line
            hkv(4) = plot(([0 max(s_fine)]-s_fine(i_maxc))/s_fine(end), [0 0], 'k-');
            % s = s_maxcurv line
            hkv(5) = plot(0*[1 1],1.1*[mnRp mxRp],'-.k');

            % bounding lines showing when the fit starts and when the raw data ends
            hkv(6) = plot((skappafitstartv(1)-s_fine(i_maxc))/s_fine(end)*[1 1 1],1.1*[mnRp 0 mxRp],bopt{:},fitmrk{1}); % bottom
            hkv(7) = plot((skapparawdataendv(1)-s_fine(i_maxc))/s_fine(end)*[1 1 1],1.1*[mnRp 0 mxRp],bopt{:},fitmrk{2}); % left
            hkv(8) = plot((skapparawdataendv(2)-s_fine(i_maxc))/s_fine(end)*[1 1 1],1.1*[mnRp 0 mxRp],bopt{:},fitmrk{3}); % right
            hkv(9) = plot((skappafitstartv(2)-s_fine(i_maxc))/s_fine(end)*[1 1 1],1.1*[mnRp 0 mxRp],bopt{:},fitmrk{4}); % top
            ylabel(ylblkappa,axopts{:})
            grid on
            axis(Raxlims)
            xlabel(xlblkappa,axopts{:})
            set(gca,axgcaopts{:});
        end
        fitRlgnd = {'raw','whole fit','fit','$q=0$','$\max q$','fit data start (B)',...
            'raw data end (B)','raw data end (T)','fit data start (T)'};
        lhRmrch = legend(hkv,fitRlgnd,lopts{:},axopts{:});
        subplot(2,1,1)
        axis([-0.35781 0.50462 -0.47151 11.20085])
        set(gca,'position',[0.05093 0.59380 0.73726 0.39078])
        subplot(2,1,2)
        xlim([-0.08249 0.1126])
        ylim([-0.5 10])
        set(gca,'position',[0.05094 0.09625 0.73727 0.401305]);
        set(lhRmrch,'position', [0.79289 0.73098 0.19810 0.25256]);
        set(lhRmrch,'fontsize',axfs,'color','none');
    else
        % Raw R_mrch from input (calculated with finite differences for kappa)
        plot((s_fine-s_fine(i_maxc))/s_fine(end), R_mrch_uf ...
            ,'color', 'k' ...
            ,'linestyle', '-' ...
            ,'linewidth', lwk ...
            );
        lhRmrch = legend({'raw'},lopts{:},axopts{:});
        set(lhRmrch,'fontsize',axfs,'color','none','location','best');
        xlabel(xlblkappa,axopts{:})
        ylabel(ylblkappa,axopts{:})
        axis(Raxlims)
        set(gca,axgcaopts{:});
    end
    drawnow
    if afopts.debug
        userkeyboard = input(debugstr,'s');
        if strcmpi(userkeyboard,'q')
            error(abortstr);
        elseif strcmpi(userkeyboard,'k')
            keyboard
        end
    end
end
% The wall-normal theta w.r.t global x must be rotated a positive 90
% degrees CCW given the dtheta of the surface inclination
%          /|
%         / |
%   ds   /  |
%       /   | dy
%      /    |
%     /_____|
%       dx
dtheta_surface = atan2d(dy_ds,dx_ds);
theta_wn_fine = dtheta_surface + 90;

if afopts.fit_kappa_curv && afopts.plot_R_mrch == 2
    videoon = afopts.video_R_mrch_obj.WriteAVI;
    if videoon
        videoobject = VideoWriter(afopts.video_R_mrch_obj.Filename,'Motion JPEG AVI');
        videoobject.FrameRate = afopts.video_R_mrch_obj.FrameRate;
        videoobject.Quality = afopts.video_R_mrch_obj.Quality;
        dispif('--- Opening video object',options.display);
        open(videoobject)
    end
    dispif('--- Generating the locus of osculating circles (i.e. the evolute)',options.display);
    % Build the osculating circles for both the fitted and unfitted data set
    % Note that R_mrch is signed here: R < 0, concave; R > 0, convex.
    centers_oscu = [x_fine - R_mrch_fine.*cosd(theta_wn_fine); y_fine - R_mrch_fine.*sind(theta_wn_fine)];
    centers_oscu_uf = [x_fine - R_mrch_uf.*cosd(theta_wn_fine); y_fine - R_mrch_uf.*sind(theta_wn_fine)];

    fnumevolute = figure;
    clf(fnumevolute)
    set(fnumevolute,'color','w')
    plot(x_fine, y_fine, 'r-', 'linewidth', 4)
    hold on
    set(gca,axgcaopts{:});
    xlabel('$x$ (m)',axopts{:})
    ylabel('$y$ (m)',axopts{:})

    oscu_opts = {'-k'};
    oscu_uf_opts = {'--r'};

    hcenter(1)=plot(centers_oscu(1,:), centers_oscu(2,:), oscu_opts{:}, 'linewidth', 2);
    hcenter(2)=plot(centers_oscu_uf(1,:), centers_oscu_uf(2,:), oscu_uf_opts{:}, 'linewidth', 2);

    axis([-medianR 1+medianR -medianR-0.5 medianR+0.5]*af.chord_U)
    axis equal

    ncirc = round(length(R_mrch_fine)/10);
    icirc = 1:length(R_mrch_fine);
    icirc = round(linspace(icirc(1), icirc(end), ncirc));
    icirc = [icirc, i_maxc]; % include the smallest radius at the end

    jinterval = 1;
    for j = 1:length(icirc)
        Rj = abs(R_mrch_fine(icirc(j)));
        hcirc(1,j) = circle2p0(centers_oscu(1,icirc(j)), centers_oscu(2,icirc(j)), Rj ...
            , 'linewidth', 2, 'color', 'k'); %#ok<AGROW>
        hcirc(2,j) = circle2p0(centers_oscu_uf(1,icirc(j)), centers_oscu_uf(2,icirc(j)), abs(R_mrch_uf(icirc(j))) ...
            , 'linewidth', 2.5, 'color', 'r', 'linestyle', '--'); %#ok<AGROW>
        hcirc(3,j) = plot(centers_oscu(1,icirc(j)), centers_oscu(2,icirc(j)),oscu_opts{:},'marker','x','markersize',10); %#ok<AGROW>
        hcirc(4,j) = plot(centers_oscu_uf(1,icirc(j)), centers_oscu_uf(2,icirc(j)),oscu_uf_opts{:},'marker','o','markersize',10); %#ok<AGROW>
        hcirc(5,j) = plot(x_fine(icirc(j)), y_fine(icirc(j)),'xk','markersize',10); %#ok<AGROW>

        if Rj < medianR
            axis([-1.1*2*Rj af.chord_U+1.1*2*Rj -1.1*2*Rj-0.5*af.chord_U 1.1*2*Rj+0.5*af.chord_U])
        else
            axis([-1.1*2*medianR af.chord_U+1.1*2*medianR -1.1*2*medianR-0.5*af.chord_U 1.1*2*medianR+0.5*af.chord_U])
        end
        if j > jinterval
            delete(hcirc(:,j-jinterval))
        end
        drawnow
        if videoon
            writeVideo(videoobject,getframe(gcf));
        end
    end
    legend(hcenter,{'Fit','Unfit'},'location','southeast')
    if videoon
        dispif('--- Closing video object!',options.display);
        close(videoobject);
    end
end

%% 8. Get edge values from infinity values & Cp
dCp_UV_ds_M_inf_fine = (D1_s*Cp_UV_M_inf_fine.').';

% Extract out the side of interest for DEKAF marching
switch afopts.side
    case 'top'
        i_side = i_stag:N_s;
    case 'bottom'
        i_side = i_stag:-1:1;
    otherwise
        error(['airfoil.side = ''', afopts.side,'''is invalid!'])
end

% Use isentropic relations to construct the edge quantities
T_e = intel.T_inf*(1 + Cp_UV_M_inf_fine*intel.gam*(M_inf_UV^2)/2).^((intel.gam - 1)/intel.gam);
if M_inf_UV > 0 % compressible
    p_e = intel.p_inf*(Cp_UV_M_inf_fine*intel.gam*M_inf_UV^2/2 + 1);
    dp_e_ds = intel.p_inf*dCp_UV_ds_M_inf_fine*intel.gam*M_inf_UV^2/2;
elseif M_inf_UV < 0
    error('M_inf_UV < 0 is unphysical, since the reference velocity is a square-root-sum-of-squares!');
else
    p_e = q_inf_UV*Cp_UV_M_inf_fine + intel.p_inf;
    dp_e_ds = q_inf_UV*dCp_UV_ds_M_inf_fine;
end

% Stagnation flow has zero streamwise velocity at the edge of the BL
U_e0 = 0;

% Build edge density from perfect gas equation of state
rho_e = p_e./(intel.R_e*T_e);

% On integration through the singularity, there is a drastic numerical
% oscillation. To remedy this, we send that oscillation to the side that is
% not of interest.
switch afopts.side
    case 'top'
        integration_dir = 'ud';
    case 'bottom'
        integration_dir = 'du';
end
dispif('--- Calculating the integration matrix for the airfoil''s surface...',options.display);
try
    I1_s = integral_mat(D1_s,integration_dir);
    % desparsify the integration matrix from the sparse diff. matrix on output
    % see MATLAB tips on page: https://www.mathworks.com/help/matlab/ref/full.html
    % "In 64-bit MATLAB, however, double matrices with fewer than half of
    % their elements nonzero are more efficient to store as sparse matrices."
    if (nnz(I1_s)/numel(I1_s)) >= 1/2
        I1_s = full(I1_s);
    end
catch
    error(['It appears constructing the integration matrix has failed.\n', ...
        'This is likely due to too large of a matrix size demanded for your workstation.\n', ...
        'Consider reducing the surface-distance resolving factor, ', ...
        's_hyperinterp_fac, down from ', num2str(s_hyperinterp_fac), '.'],errmsgID);
end
dispif('--- Complete!',options.display);

% Note that the typical compressible Bernoulli's formula allows one to
% specify a U_e0. We define this quantity such that the U_e at the
% attachment line (max Cp) is zero, i.e. U_e(stag)^2 + bernoulli_int(stag) is zero.
bernoulli_int = -2*(I1_s*(dp_e_ds./rho_e).').';
Ushift2 = -bernoulli_int(i_stag);
U_e = sqrt(Ushift2 + bernoulli_int);

% The lines below are performed as a precaution, they were necessary before
% the automatic adjustment of the Cp_UV-distribution (section 5.) was implemented.
% In the current version, we ensure that the Cp_UV-distribution always undershoots
% the maximum possible isentropic attachment line Cp_UV-value.
%
% Note that in previous versions of convert_airfoil, the ultra-fine s grid
% didn't guarantee to *exactly* contain the attachment line.
% As a result, U_e at the attachment line by compressible Bernoulli's may
% be mathematically complex (very slightly). In those cases, we took the
% magnitude of the complex number to assign the proper m/s at those stations!
% However, with the new behavior of build_s_fine exactly including
% the attachment line of s_data, this complex behavior likely will not happen
% again, regardless of whether the Cp correction was an undershoot or an overshoot.
% It is kept here for posterity, and at some point in the future, a bold developer
% may decide to remove it cold.
near_stagnation_pts = imag(U_e)~=0;
U_e(near_stagnation_pts) = abs(U_e(near_stagnation_pts));

% Send all velocities upstream of the attachment line (the side we're not
% interested in) to be negative for more accurate derivatives.
U_e_unflipped = U_e(i_side);
U_e = -U_e;
U_e(i_side) = U_e_unflipped;

% To build derivatives of U_e, we invert Euler's equation along a streamline,
% avoiding numerical differentiation. Note that the attachment line value of
% dU_e_ds is NaN by this relationship.
dU_e_ds = -dp_e_ds./rho_e./U_e;
% Since for eval_dimVars, we need a derivative of U_e at s = 0, we can
% numerically calculate it with the differentiation matrix, avoiding the
% NaN attachment line value from the analytical inviscid Euler's equation.
dU_e_ds(i_stag) = D1_s(i_stag,:)*U_e.';
% The second derivative is no longer needed at this time, but is kept for
% potential future use.
%dU_e_ds2 = ((-dp_e_ds2 + dp_e_ds./rho_e.*drho_e_ds)./rho_e -dU_e_ds.^2)./U_e;%(D2_s*U_e.').';% UNUSED

% Get dynamic viscosity
y_s_e = repmat(y_s_inf,[N_s,1]);
Mm_e = mixCnst.R0/intel.R_e*ones(1,N_s);
cp_e = intel.cp*ones(1,N_s);
% We need to transpose the arguments to abide by the function's expectation
% of size N_eta x 1, so N_s is acting as N_eta.
[mu_e,~] = getTransport_mu_kappa(y_s_e,T_e.',T_e.',rho_e.',p_e.',Mm_e.',options,mixCnst,T_e.',T_e.',T_e.',cp_e.');
mu_e = mu_e.';

%% 9. Calculate the beta-Hartree parameter
dxi_ds = rho_e.*U_e.*mu_e;
xi = (I1_s*(dxi_ds.')).';
% Shift our definition of xi = 0 to be at the attachment line directly
xi = xi - xi(i_stag);
beta = 2*xi./U_e.*dU_e_ds./dxi_ds;
% Correct beta at attachment line
beta(i_stag) = 1;

hbeta = gobjects(1,4);
if afopts.plot_beta
    fnumbetav(1) = figure;
    clf(fnumbetav(1))
    set(fnumbetav(1),'color','w')
    hbeta(1) = plot(s_fine, beta, 'k');
    ylim([-1 2])
    grid on
    hold on
    plot([s_fine(1) s_fine(end)], [0 0], 'k', 'color', 0.7*[1 1 1], 'linewidth', 0.3);
    xlabel('$s$ (m)',axopts{:})
    ylabel('$\beta_{H}$',axopts{:})
    set(gca,axgcaopts{:});
end
% Neighboring beta values are corrected to 1 as well because the spline
% interpolation performs poorly near the attachment line. NOTE: This is
% inconsistent to assert, as beta is dependent on xi and velocity gradients
% at the edge, which would in turn affect the pressure and the thermodynamics.
beta(i_stag + [-1, 1]) = 1;

% Determine which side of the airfoil the attachment line is on (top/bottom)
[~,i_LE] = min(x_fine);
if i_stag < i_LE % Stagnation is on the bottom.
    % We expect that the |U_e| for s > s_stag will increase faster than |U_e|
    % for s < s_stag. Therefore, we extend beta = 1 until this expected
    % variation is observed, overwriting the interpolated values.
    i_beta_top = i_stag + 1 + find(beta(i_stag + 2:end) > 1,1);
    i_beta_bottom = i_stag - 1 - find(beta(i_stag - 2:-1:1) < 1,1);
elseif i_stag > i_LE % Stagnation is on the top.
    % We expect that the |U_e| for s < s_stag will increase faster than |U_e|
    % for s > s_stag.
    i_beta_top = i_stag + 1 + find(beta(i_stag + 2:end) < 1,1);
    i_beta_bottom = i_stag - 1 - find(beta(i_stag - 2:-1:1) > 1,1);
else % Stagnation is directly at the leading edge (e.g. symmetric airfoil)
    % The best guess we have for the changes in U_e is that the beta
    % distribution should be symmetric.
    i_beta_top = i_stag + 1;
    i_beta_bottom = i_stag - 1;
end
% It is possible that beta does not exceed 1, then the correction is limited
% to the number of added points for the interpolation. Note that we round the
% s_hyperinterp_fac allowing non-integer factors for its value.
i_beta_top = min([i_beta_top, i_stag + round(s_hyperinterp_fac) - 1]);
i_beta_bottom = min([i_beta_bottom, i_stag - round(s_hyperinterp_fac) + 1]);
beta(i_stag + 2:i_beta_top - 1) = 1;
beta(i_beta_bottom + 1:i_stag - 2) = 1;

if afopts.plot_beta
    figure(fnumbetav(1))
    hbeta(2) = plot(s_fine, beta, 'b--');
    hold on
    hbeta(3) = plot(s_fine(i_stag), beta(i_stag), 'bo');
end

if afopts.plot_beta
    fnumbetav(2) = figure;
    clf(fnumbetav(2))
    set(fnumbetav(2),'color','w')
    sreldom = (s_fine-s_fine(i_stag))/s_fine(end); % relative s domain
    for ipl = 1:2
        subplot(2,1,ipl)
        plot(sreldom, beta ...
            ,'color', 'k' ...
            ,'linestyle', '-' ...
            ,'linewidth', 2 ...
            );
        hold on
        grid on
        xlabel('$(s-s_{\mathrm{max\, C_p}})/\max s$',axopts{:})
        ylabel('$\beta_H$',axopts{:})
        set(gca,axgcaopts{:});
    end
    subplot(2,1,1)
    xlim([min(sreldom) max(sreldom)])
    bymax = 1.3;
    ylim([-0.3 bymax])
    %set(gca,'position',[0.077945 0.58481 0.74146 0.39977])
    subplot(2,1,2)
    xlim([-0.06 0.06])
    ylim([0 bymax])
    %set(gca,'position',[0.077945 0.08656 0.74146 0.39977]);
    drawnow
end

%% 10. Extract the relevant edge quantities on the desired side of the airfoil
switch afopts.side
    case 'top'
        s_1side = s_fine(i_side) - s_fine(i_stag);
    case 'bottom'
        % the direction of s, xi and U_e is flipped here (!)
        s_1side = s_fine(i_stag) - s_fine(i_side);
        xi = -xi;
        %U_e = -U_e; % flipped already above
        % Since s is flipped, we also need to flip the gradients.
        dU_e_ds = -dU_e_ds;
        % W_0 has to be flipped for the bottom side, because the z-axis
        % flips to keep a right-handed coordinate system.
        W_0 = -W_0;
end
%%%%%%%%% CLIP OUT SIDE NOT OF INTEREST. USAGE IS NOT IDEMPOTENT.
[x_fine, y_M_inf_fine, R_mrch_fine, R_mrch_uf, theta_wn_fine, U_e, T_e, rho_e, p_e, mu_e, beta, xi, Cp_UV_M_inf_fine] = ...
    select_airfoil_side(i_side, ...
    x_fine, y_M_inf_fine, R_mrch_fine, R_mrch_uf, theta_wn_fine, U_e, T_e, rho_e, p_e, mu_e, beta, xi, Cp_UV_M_inf_fine);

% Now having chosen a side of the airfoil, we deresolve our xi
% and map it to the user-specified clustering scheme simultaneously.
switch options.xSpacing
    case {'linear','log','tanh'}
        % Set mapOpts needed
        % IMPORTANT! the inputs in mapOpts are fixed into x_start, x_end,
        % etc. even though it is actually xi. The reason is that the
        % xMappings function expects inputs related to x to apply the
        % different spacings.
        options.mapOpts.x_start = xi(1);
        options.mapOpts.x_end = xi(end);
        deltaxi_default = 0.01*(options.mapOpts.x_end - options.mapOpts.x_start)/(options.mapOpts.N_x - 1);
        % See issue #61: the user cannot specify deltaxi_default
        options.mapOpts.deltax_start = deltaxi_default;

        % Build a hyperbolic-tangent xi that will be the interpolation domain
        xi_interp = xMappings(options.xSpacing, 'get_x', options.mapOpts);
    otherwise
        error(['options.xSpacing = ''', options.xSpacing,'''is not ready yet!'])
end
hp = gobjects(1,6);
if afopts.plot_xi
    fnumxi = figure;
    clf(fnumxi)
    set(fnumxi,'color','w')
    subplot(2,1,2)
    hp(1) = plot(100*([1:length(s_1side)]-1)/(length(s_1side)-1), s_1side, 'k-'); %#ok<NBRAK>
    hold on

    ipsel = [0.02:0.02:0.1 0.2:0.2:0.8];
    isel = round(ipsel*length(s_1side));
    plot(100*repmat(isel'-1,1,2)'/(length(s_1side)-1),[s_1side(isel)' zeros(length(isel),1)]','--k')
    plot(100*[isel'-1 zeros(length(isel),1) ]'/(length(s_1side)-1),repmat(s_1side(isel),2,1),'--k')

    subplot(2,1,1)
    hp(2)=plot(100*([1:length(xi)]-1)/(length(xi)-1), xi, 'k-'); %#ok<NBRAK>
    hold on

    plot(100*repmat(isel'-1,1,2)'/(length(xi)-1),[xi(isel)' zeros(length(isel),1) ]','--k')
    plot(100*[isel'-1 zeros(length(isel),1) ]'/(length(xi)-1),repmat(xi(isel),2,1),'--k')
end

% We force usage of linear interpolation here to ensure there is no
% overshooting of any variables (e.g. non-monotonic s array).
%%%%%%%%% USAGE IS NOT IDEMPOTENT.
[U_e, T_e, rho_e, p_e, mu_e, beta, s, x, y, R_mrch, R_mrch_uf, theta_wn, Cp_UV_M_inf] = ...
    deresolve_airfoil(xi, xi_interp, 'linear', ...
    U_e, T_e, rho_e, p_e, mu_e, beta, s_1side, x_fine, y_M_inf_fine, ...
    R_mrch_fine, R_mrch_uf, theta_wn_fine, Cp_UV_M_inf_fine);
xi = xi_interp;

if afopts.plot_xi
    figure(fnumxi)
    subplot(2,1,2)
    hp(3) = plot(100*([1:length(s)]-1)/(length(s)-1), s, 'b-'); %#ok<NBRAK>
    hold on
    xlabel('$i_s$ [\%]', axopts{:})
    ylabel('$s\, \mathrm{[m]}$', axopts{:})
    set(gca,axgcaopts{:});

    isel = round(ipsel*length(s));
    plot(100*repmat(isel'-1,1,2)'/(length(s)-1),[s(isel)' zeros(length(isel),1)]','--b')
    plot(100*[isel'-1 zeros(length(isel),1) ]'/(length(s)-1),repmat(s(isel),2,1),'--b')

    ds_interp = diff(s)*(length(s)-1);
    % Clustering occurs when the mapped s changes more slowly than the
    % corresponding equally-spaced s, i.e. the derivative is less than
    % that of the equally-spaced s. The slope of the equally-spaced s is
    % given by max(s), since it's "rise/run" for triangles.
    %
    % For schemes without clustering, e.g. 'linear', the find ought to return
    % the first index, hence the max(blah, 1) wrapper to prevent out-of-bounds access.
    is_cluster_end = max(find(ds_interp >= max(s), 1, 'first') - 1, 1);
    hp(5) = plot(100*(is_cluster_end-1)/(length(s)-1)*[0 1], s(is_cluster_end)*[1 1], 'r-.');
    plot(100*(is_cluster_end-1)/(length(s)-1)*[1 1], s(is_cluster_end)*[0 1], 'r-.');
    axis([-inf inf -inf inf])

    legend(hp([1 3 5]),{'$s(s_{\mathrm{linear}})$','$s(\xi_{\mathrm{interp.}})$',...
        '$s_\mathrm{cluster\,end}$'},...
        lopts{:},'location','southeast')

    subplot(2,1,1)
    hp(4)=plot(100*([1:length(xi)]-1)/(length(xi)-1), xi, 'b-'); %#ok<NBRAK>
    xlabel('$i_{\xi}$ [\%]', axopts{:})
    ylabel('$\xi\, \mathrm{[kg^2/m^2/s^2]}$', axopts{:})
    hold on
    set(gca,axgcaopts{:});

    plot(100*repmat(isel'-1,1,2)'/(length(xi)-1),[xi(isel)' zeros(length(isel),1) ]','--b')
    plot(100*[isel'-1 zeros(length(isel),1) ]'/(length(xi)-1),repmat(xi(isel),2,1),'--b')

    dxi_interp = diff(xi)*(length(xi)-1);
    ixi_cluster_end = max(find(dxi_interp >= max(xi), 1, 'first') - 1, 1);
    hp(6) = plot(100*(ixi_cluster_end-1)/(length(xi)-1)*[0 1], xi(ixi_cluster_end)*[1 1], 'r-.');
    plot(100*(ixi_cluster_end-1)/(length(xi)-1)*[1 1], xi(ixi_cluster_end)*[0 1], 'r-.');
    axis([-inf inf -inf inf])

    legend(hp([2 4 6]),{'$\xi(s_{\mathrm{linear}})$','$\xi(\xi_{\mathrm{interp.}})$',...
        '$\xi_\mathrm{cluster\,end}$'},...
        lopts{:},'location','southeast')
    drawnow
end

% Now having constructed the xi for DEKAF, we obtain its differentiation
% matrices by using 'custom_xi' functionality.
options.mapOpts.xi_custom = xi;
[D1_xi,D2_xi] = FDdif_nonEquis(options.mapOpts.xi_custom,['backward',num2str(options.ox)]);

% For clarity, we reset the unused relevant fields of mapOpts for 'custom_xi'
options.xSpacing = 'custom_xi'; % OVERRIDING USER-INPUT FIELD
options.mapOpts.XI_i            = NaN;
options.mapOpts.frac_XIi        = NaN;
options.mapOpts.x_start         = NaN;
options.mapOpts.x_end           = NaN;
options.mapOpts.deltax_start    = NaN;

if afopts.plot_beta
    switch afopts.side
        case 'top'
            domain_plot_beta = s + s_fine(i_stag);
        case 'bottom'
            domain_plot_beta = -s + s_fine(i_stag);
    end
    figure(fnumbetav(1))
    hbeta(4) = plot(domain_plot_beta, beta, '.r-');
    legend(hbeta,{'Direct','Stagnation adjusted','Stagnation',...
        'Result for DEKAF'},'location','northeast',lopts{:})
    drawnow
end

% Complete all edge value calculations that should've been done in build_edge_values
M_e = U_e./sqrt(intel.gam*intel.R_e*T_e);
y_s_e = repmat(y_s_inf,[options.mapOpts.N_x,1]);
Mm_e = mixCnst.R0/intel.R_e*ones(1,options.mapOpts.N_x);
cp_e = intel.cp*ones(1,options.mapOpts.N_x);
[~,kappa_e] = getTransport_mu_kappa(y_s_e,T_e.',T_e.',rho_e.',p_e.',Mm_e.',options,mixCnst,T_e.',T_e.',T_e.',cp_e.');
kappa_e = kappa_e.';
h_e = intel.cp*T_e;
H_e = getEnthalpy_semitotal(h_e, U_e);
Theta = zeros(1,options.mapOpts.N_x); % override any nondim. gradients of semi-total enthalpy for CPG flows
[Ecu, Ecw, Ecp] = getEckert_numbers(U_e, W_0, h_e, p_e, rho_e);
theta = NaN*ones(1,options.mapOpts.N_x); % unused for CPG! only in vib.-energy equation
fi = getStreamlineFi(U_e, W_0, true);
Re1_e = rho_e.*U_e./mu_e;
g_lim = 0.5; % limit enthalpy to 50% of the minimum

%% 11. Build the options and intel structs to interface with DEKAF
options.airfoil = afopts; % end the local association
options_out = options;
intel_out = intel;

% Import all edge values into DEKAF, replacing the need for build_edge_values
% (CPG-relevant quantities only)
intel_out.xi = xi;
intel_out.M_e = M_e;
intel_out.U_e = U_e;
intel_out.T_e = T_e;
intel_out.rho_e = rho_e;
intel_out.p_e = p_e;
intel_out.mu_e = mu_e;
intel_out.kappa_e = kappa_e;
intel_out.h_e = h_e;
intel_out.H_e = H_e;
intel_out.beta = beta;
intel_out.theta = theta;
intel_out.Theta = Theta;
intel_out.Ecu = Ecu;
intel_out.Ecw = Ecw;
intel_out.Ecp = Ecp;
intel_out.fi = fi;
intel_out.R_e = intel.R_e*ones(1,options.mapOpts.N_x);
intel_out.Re1_e = Re1_e;
intel_out.gam_e = intel.gam*ones(1,options.mapOpts.N_x);
intel_out.g_lim = g_lim;
intel_out.x = s;
intel_out.D1_xi = D1_xi;
intel_out.D2_xi = D2_xi;
intel_out.cylCoordFuncs = populateProbsteinElliotFunctions(intel,options);
intel_out.xPhys = get_xyInvTransformed(intel_out.x,[],options.coordsys,intel_out.cylCoordFuncs);
intel_out.Sc_e = NaN; % Unused for CPG
intel_out.Le_e = NaN; % Unused
intel_out.cp_e = cp_e;
intel_out.U_e0 = U_e0;
intel_out.U_inf = U_inf;
intel_out.V_inf = V_inf;
intel_out.W_0 = W_0;
intel_out.Q_inf = Q_inf;
intel_out.h_inf = h_inf;
intel_out.rho_inf = rho_inf;

% And extra airfoil-specific components
intel_out.airfoil.x = x;                    % 1. Airfoil coords
intel_out.airfoil.y = y;
intel_out.airfoil.R_mrch = R_mrch;          % 2. Surface parameters
if afopts.fit_kappa_curv
    intel_out.airfoil.R_mrch_uf = R_mrch_uf;
end
intel_out.airfoil.theta_wn = theta_wn;
intel_out.airfoil.Cp_UV = Cp_UV_M_inf;      % 3. Pressure coefficient

% intel.ys_inf is a cell array length N_spec of scalars. We need to multiply
% each element by a vector size N_x to appropriately skip build_edge_values.
intel_out.ys_e = cellfun(@(ys_e_entry) ys_e_entry*ones(1,options.mapOpts.N_x), intel.ys_inf, ...
    'UniformOutput', false);
X_E = getElement_XE_from_ys(cell1D2matrix2D(intel_out.ys_e),mixCnst.Mm_s,mixCnst.elemStoich_mat);
intel_out.yE_e = matrix2D2cell1D(getEquilibrium_ys(X_E,mixCnst.Mm_E));

% Load additional quantities needed for eval_dimVars that are unique for
% attachment-line flows.
intel_out.dUe0_dx = dU_e_ds(i_stag);

% Note that further in DEKAF, getQSS_dataStructures transforms many of these
% 1 x N_x edge-quantity vectors to just their first entry for QSS.
% However, it does this based on regular expressions of the fields' names.
% And some of intel's fields (e.g. Ecu, Ecw) (that are 1 x N_x)
% need their first value for QSS, despite the regex not matching!
% We supply a list of field names for those that are required for QSS's
% first-element extraction.
options_out.import_addtl_edge_flds_4_QSS = {'beta', 'Theta', 'theta', ...
    'Ecu', 'Ecw', 'xi'};

% Enforce that indeed the first dimensional s coordinate used in
% eval_dimVars() is in fact equal to zero
options_out.xDim0 = true;

options_out.marchYesNo = true;
intel_out.x_e = intel_out.x;

% Crucial bit here: it is assumed that the sweep within DEKAF is applied
% on the resultant in-plane freestream flow, i.e. Lambda_w_uv, *not* the
% contrasting sweep dependent on U_inf, Lambda_w_u.
intel_out.Lambda_sweep = intel.Lambda_w_uv;

if afopts.debug
    userkeyboard = input(debugstr,'s');
    if strcmpi(userkeyboard,'q')
        error(abortstr);
    elseif strcmpi(userkeyboard,'k')
        keyboard
    end
end

end % convert_airfoil
