function [x,y,r,r0,dyBar_dx,dyBar_dy,dyBar_dy2,dxBar_dx] = get_xyInvTransformed(xBar,yBar,coordsys,cylCoordFuncs)
% get_xyInvTransformed performs whichever inverse transformation is
% necessary based on the options provided.
%
% Usage:
%   (1)
%       [x,y,r,r0,dyBar_dx,dyBar_dy,dyBar_dy2,dxBar_dx] = get_xyInvTransformed(xBar,yBar,coordsys,cylCoordFuncs)
%
%   (2)
%       x = get_xyInvTransformed(xBar,[],coordsys,cylCoordFuncs)
%
% Inputs and outputs:
%   xBar, yBar
%       streamwise and wall-normal transformed coordinates              [m]
%   x, y
%       streamwise and wall-normal physical coordinates                 [m]
%   r
%       radial distance from the axis to each x-y point                 [m]
%   r0
%       radial distance from the axis to the surface at each x-y point  [m]
%   dyBar_dx
%       variation of the transformed coordinate y with the physical
%       coordinate x                                                    [-]
%   dxBar_dx
%       variation of the transformed coordinate x with the physical
%       coordinate x                                                    [-]
%   dyBar_dy
%       variation of the transformed coordinate y with the physical
%       coordinate y                                                    [-]
%   dyBar_dy2
%       variation of the transformed coordinate y with the physical
%       coordinate y twice                                              [-]
%   coordsys
%       string identifying the coordinate system. Supported values are:
%           'cartesian2D'
%           'cone'
%           'cylindricalExternal'
%           'cylindricalInternal'
%   cylCoordFuncs
%       structure containing various functions to compute various
%       geometrical quantities related to cylindrical coordinates:
%           .rc(xc)             [m]
%               spanwise radius of curvature
%           .alphac(xc)         [deg]
%               streamwise inclination angle
%           .Ixc_rc2(xc)        [m]
%               integral of (rc/L)^2 wrt xc
%           .Dxc_rc(xc)         [-]
%               derivative of rc wrt xc
%           .Dxc_alphac(xc)     [1/m]
%               derivative of alphac wrt xc
%           .IxBar_1rc2(xBar)   [m]
%               integral of (L/rc)^2 wrt xBar
%
% See also: get_xyTransformed, get_xyrInvProbsteinElliot_external,
% get_xyrInvProbsteinElliot_internal
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if isempty(yBar)
    yBar = NaN*ones(size(xBar));
end

switch coordsys                                                     % depending on the coordinate system
    case 'cartesian2D'                                                  % for cartesian
        x = xBar;                   y = yBar;                               % nothing to be done
        r = y;                      r0 = zeros(size(x));
        dyBar_dx = zeros(size(y));  dyBar_dy = ones(size(x));
        dyBar_dy2 = zeros(size(y)); dxBar_dx = ones(size(x));
    case {'cone','cylindricalExternal'}                                 % for conical and an exterior cylindrical system
        [x,y,r,r0,dyBar_dx,dyBar_dy,dyBar_dy2,dxBar_dx] = get_xyrInvProbsteinElliot_external(xBar,yBar,cylCoordFuncs);
    case 'cylindricalInternal'                                          % for conical and an interior cylindrical system
        [x,y,r,r0,dyBar_dx,dyBar_dy,dyBar_dy2,dxBar_dx] = get_xyrInvProbsteinElliot_internal(xBar,yBar,cylCoordFuncs);
    otherwise                                                           % for anything else break
        error(['the chosen coordsys ''',coordsys,''' is not supported']);
end

if any(abs(imag(y))>eps*max(real(y)))
    warning('Warning: non-negligible imaginary part of y');
end
y=real(y);

end % get_xyInvTransformed
