function cylCoordFuncs = populateProbsteinElliotFunctions(intel,options)
% populateProbsteinElliotFunctions prepares the necessary functions to
% apply Probstein-Elliot coordinate-system changes.
%
% Usage:
%   (1)
%       cylCoordFuncs = populateProbsteinElliotFunctions(intel,options)
%
% Inputs and outputs:
%   intel
%       DEKAF structure that must contain:
%           a) For options.coordsys='cone':
%               .cone_angle     [deg]
%                   value of the cone's half angle
%           b) For options.coordsys='cylindricalExternal' or
%           'cylindricalInternal':
%               .xcAxis         [m]
%                   vector of locations along the streamwise axis of
%                   revolution, paired with intel.rc
%               .rc             [m]
%                   vector with the values of the transverse radius of
%                   curvature at each axial location in intel.xcAxis
%   options
%       DEKAF strucure which must contain:
%           .coordsys
%               string identifying the coordinate system
%           .xSpacing
%               string identifying the streamwise mapping
%           .ox
%               finite-difference order
%           .marchYesNo
%               boolean determining if the solution is being marched or not
%   cylCoordFuncs
%       structure containing various functions to compute various
%       geometrical quantities related to cylindrical coordinates:
%           .xcAxis(xc)         [m]
%               position along the axis of revolution
%           .rc(xc)             [m]
%               spanwise radius of curvature
%           .alphac(xc)         [deg]
%               streamwise inclination angle
%           .Ixc_rc2(xc)        [m]
%               integral of (rc/L)^2 wrt xc
%           .Dxc_rc(xc)         [-]
%               derivative of rc wrt xc
%           .Dxc_alphac(xc)     [1/m]
%               derivative of alphac wrt xc
%           .IxBar_1rc2(xBar)   [m]
%               integral of (L/rc)^2 wrt xBar
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

supportedMappings = {'linear','log','tanh','custom_x'};                     % list of mappings for which the conical coordys are supported

if isfield(options,'marchYesNo') && options.marchYesNo;     str_FD = ['backward',num2str(options.ox)];
else;                                                       str_FD = 'backward2';
end

switch options.coordsys
    case 'cartesian2D'                                                          % cartesian coordinate-system - no additional treatment needed
        cylCoordFuncs = struct;                                                     % dummy place holder
    case 'cone'                                                                 % straight cone
        if options.marchYesNo && ~ismember(options.xSpacing,supportedMappings)      % sanity check
            error(['Error: the chosen coordsys ''',options.coordsys,''' is not supported for the chosen xSpacing ''',options.xSpacing,'''.']);
        end
        neededvalue(intel,'cone_angle',['Error: the ''',options.coordsys,''' coordinate system was selected, but intel.cone_angle was not provided']);
        cone_angle  = intel.cone_angle;
        cylCoordFuncs.xcAxis      = @(xc)xc*cosd(cone_angle);                       % axis coordinates
        cylCoordFuncs.rc          = @(xc)xc*sind(cone_angle);                       % spanwise radius of curvature
        cylCoordFuncs.alphac      = @(xc)cone_angle*ones(size(xc));                 % surface streamwise inclination angle
        cylCoordFuncs.Ixc_rc2     = @(xc)1/3 * xc.^3*sind(cone_angle)^2;            % integral of rc^2 wrt xc
        cylCoordFuncs.Dxc_rc      = @(xc)sind(cone_angle)*ones(size(xc));           % derivative of rc wrt xc
        cylCoordFuncs.Dxc_alphac  = @(xc)zeros(size(xc));                           % derivative of alphac wrt xc
        cylCoordFuncs.IxBar_1rc2  = @(xBar)(3*xBar/sind(cone_angle)^2).^(1/3);      % integral of 1/rc^2 wrt xBar
    case {'cylindricalExternal','cylindricalInternal'}                          % BL in the outter or inner face of a body of revolution
        if options.marchYesNo && ~ismember(options.xSpacing,supportedMappings)      % sanity check
            error(['Error: the chosen coordsys ''',options.coordsys,''' is not supported for the chosen xSpacing ''',options.xSpacing,'''.']);
        end
        neededvalue(intel,'xcAxis',['Error: the ''',options.coordsys,''' coordinate system was selected, but intel.xcAxis was not provided']);
        neededvalue(intel,'rc',    ['Error: the ''',options.coordsys,''' coordinate system was selected, but intel.rc was not provided']);
        xcAxis      = intel.xcAxis(:);                                              % making inputs into column vectors
        rc          = intel.rc(:);
        DxcAxis     = FDdif_nonEquis(xcAxis,str_FD);                                % differentiation matrix in the x-axis direction
        IxcAxis     = integral_mat(DxcAxis,'ud');                                   % integration matrix in the x-axis direction
        alphac      = atand(DxcAxis*rc);                                            % surface inclination angle [deg]
        dxc_dxcAxis = 1./cosd(alphac);                                              % derivative of the surface coordinate wrt the axis coordinate
        xc          = IxcAxis*dxc_dxcAxis;                                          % integrating to obtain surface coordinate (starting from 0!)
        Dxc         = FDdif_nonEquis(xc,str_FD);                                    % differentiation matrix in the x-surface direction
        Ixc         = integral_mat(Dxc,'ud');                                       % integration matrix in the x-surface direction
        Ixc_rc2     = Ixc*(rc.^2);                                                  % integral of rc^2 wrt xc
        Dxc_rc      = Dxc*rc;                                                       % derivative of rc wrt xc
        Dxc_alphac  = pi/180 * Dxc*alphac;                                          % derivative of alphac wrt xc
        cylCoordFuncs.xcAxis     = @(x)interp1(xc,xcAxis,    real(x),   options.meth_edge_interp);
        cylCoordFuncs.rc         = @(x)interp1(xc,rc,        real(x),   options.meth_edge_interp);
        cylCoordFuncs.alphac     = @(x)interp1(xc,alphac,    real(x),   options.meth_edge_interp);
        cylCoordFuncs.Ixc_rc2    = @(x)interp1(xc,Ixc_rc2,   real(x),   options.meth_edge_interp);
        cylCoordFuncs.Dxc_rc     = @(x)interp1(xc,Dxc_rc,    real(x),   options.meth_edge_interp);
        cylCoordFuncs.Dxc_alphac = @(x)interp1(xc,Dxc_alphac,real(x),   options.meth_edge_interp);
        cylCoordFuncs.IxBar_1rc2 = @(xBar)interp1(Ixc_rc2,xc,real(xBar),options.meth_edge_interp); % NOTE: the real() is included to remove eventual residual imaginary parts <eps arising from the spectral integration
    otherwise                                                                   % break
        error(['Error: the chosen coordsys ''',options.coordsys,''' is not supported.']);
end

end % populateProbsteinElliotFunctions