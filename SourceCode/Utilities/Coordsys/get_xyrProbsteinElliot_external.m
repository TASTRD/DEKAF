function [xBar,yBar,r,r0,dyBar_dx,dyBar_dy,dyBar_dy2,dxBar_dx] = get_xyrProbsteinElliot_external(x,y,cylCoordFuncs)
% get_xyrProbsteinElliot_external performs the Probstein-Elliot
% transformation on a boundary layer developing on the external side of a
% cylindrically-symmetric surface.
%
% Usage:
%   (1)
%       [xBar,yBar,r,r0,dyBar_dx,dyBar_dy,dyBar_dy2,dxBar_dx] = get_xyrProbsteinElliot_external(x,y,cylCoordFuncs)
%
% Inputs and outputs:
%   xBar, yBar
%       streamwise and wall-normal transformed coordinates              [m]
%   x, y
%       streamwise and wall-normal physical coordinates                 [m]
%   r
%       radial distance from the axis of each x-y point                 [m]
%   r0
%       radial distance from the axis to the surface at each x-y point  [m]
%   dyBar_dx
%       variation of the transformed coordinate y with the physical
%       coordinate x                                                    [-]
%   dxBar_dx
%       variation of the transformed coordinate x with the physical
%       coordinate x                                                    [-]
%   dyBar_dy
%       variation of the transformed coordinate y with the physical
%       coordinate y                                                    [-]
%   dyBar_dy2
%       variation of the transformed coordinate y with the physical
%       coordinate y twice                                              [-]
%   cylCoordFuncs
%       structure containing various functions to compute various
%       geometrical quantities related to cylindrical coordinates:
%           .rc(xc)             [m]
%               spanwise radius of curvature
%           .alphac(xc)         [deg]
%               streamwise inclination angle
%           .Ixc_rc2(xc)        [m]
%               integral of (rc/L)^2 wrt xc
%           .Dxc_rc(xc)         [-]
%               derivative of rc wrt xc
%           .Dxc_alphac(xc)     [1/m]
%               derivative of alphac wrt xc
%
% NOTE: the arbitrary scaling distance L, needed to maintain dimensional
% coherence is omitted, since it is assumed equal to 1m.
%
% See also: get_xyrInvProbsteinElliot_external,
% get_xyrProbsteinElliot_internal
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

alpha       = cylCoordFuncs.alphac(x);                          % streamwise inclination angle  [deg]
r0          = cylCoordFuncs.rc(x);                              % spanwise radius of curvature  [m]
dr0_dx      = cylCoordFuncs.Dxc_rc(x);                          % derivative of r0 wrt x        [-]
dalpha_dx   = cylCoordFuncs.Dxc_alphac(x);                      % derivative of alpha wrt x     [1/m]

r           = r0 + y.*cosd(alpha);                              % radial distance
xBar        = cylCoordFuncs.Ixc_rc2(x);                         % transformed x - integral of (rc/L)^2 wrt x
yBar        = r0.*y + 1/2 * y.^2.*cosd(alpha);                  % transforming y

dyBar_dx    = dr0_dx.*y - 1/2*y.^2 .* sind(alpha).*dalpha_dx;   % variation of the transformed coordinate y with the physical coordinate x
dxBar_dx    = r0.^2;                                            % variation of the transformed coordinate x with the physical coordinate x
dyBar_dy    = r;                                                % variation of the transformed coordinate y with the physical coordinate y
dyBar_dy2   = cosd(alpha);                                      % variation of the transformed coordinate y with the physical coordinate y twice

end % get_xyrProbsteinElliot_external