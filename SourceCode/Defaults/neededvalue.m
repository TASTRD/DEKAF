function [] = neededvalue(options,varstr,msg)
% NEEDEDVALUE(options,variable,msg) will display an error message
% specified by the user, if the field varstr is not included in the options
% structure
%
% see also: standardvalue
%
% GNU Lesser General Public License 3.0

if (~isfield(options,varstr) || isempty(options.(varstr)))
    error(msg);
end

end