function [intel,varargout] = getDefault_composition(intel,mixture,varargin)
% getDefault_composition(intel,mixture) checks if the user has specified a
% mixture edge composition, and if not, it sets it to the default
% composition of the chosen mixture.
%
% Usage:
% 1.    intel = getDefault_composition(intel,mixture);
%       |--> populate the struct intel with new .ys_e value
%
% 2.    [intel,ys_e] = getDefault_composition(intel,mixture,flag_ys_mat)
%       |--> perform usage (1) and if flag_ys_mat=true, return ys_e as a
%       matrix (N_eta x N_spec)
%
% 3.    intel = getDefault_composition(intel,mixture,flag_ys_mat,ys_name)
%       |--> perform usage (2) using a different name for the variable
%       storing the mass fractions in intel (other than the default 'ys_e')
%
% These defaults are established in <a href="matlab:help getDefault_composition_ys_e">getDefault_composition_ys_e</a>
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

switch length(varargin)
    case 0
    flag_ys_mat = false; % default
    ys_name = 'ys_e'; % default
    case 1
    ic = 1;
    flag_ys_mat = varargin{ic};
    ys_name = 'ys_e'; % default
    case 2
    ic = 1;
    flag_ys_mat = varargin{ic}; ic = ic + 1;
    ys_name = varargin{ic}; %ic = ic + 1;
end

mixCnst = intel.mixCnst;
ys_e = getDefault_composition_ys_e(mixture);

% ensuring that charge neutrality and the concentration condition are
% satisfied
if any(mixCnst.bElectron)
    ys_e = enforce_chargeNeutralityCond(ys_e,mixCnst.bElectron,mixCnst.bPos,mixCnst.Mm_s,mixCnst.R0,'massFrac');
end
ys_e = enforce_concCond(ys_e,mixCnst.idx_bath);

% setting default
spec_ys = [intel.mixCnst.spec_list(:),ys_e(:)].';
warning_msg = ['No edge mass fractions were input, so defaults for the ',mixture,' mixture were taken: ',sprintf('\n %s --> %d ',spec_ys{:})];
if length(ys_e)>1;  intel = standardvalue(intel,ys_name,ys_e,warning_msg);
else;               intel = standardvalue(intel,ys_name,ys_e);
end

if flag_ys_mat
    varargout{1} = cell1D2matrix2D(intel.(ys_name)); % making cell into matrix form
end

end
