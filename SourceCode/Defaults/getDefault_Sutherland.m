function varargout = getDefault_Sutherland(options,varargin)
% getDefault_Sutherland(options,mixture) fixes in options the defaults for
% sutherland's law, for a given mixture.
%
% getDefault_Sutherland(options,mixture,cstPr) allows to set a flag
% determining wether a constant prandtl number is desired. In this case,
% the coefficients for kappa are not imposed.
%
% Usage:
% (1)
%   [mu_ref,T_ref,Su_mu] = getDefault_Sutherland(options);
%
% (2)
%   [mu_ref,T_ref,Su_mu,kappa_ref,Su_kappa] = getDefault_Sutherland(options);
%
% (3)
%   [...] = getDefault_Sutherland(...,muRef_s,TmuRef_s,Smu_s,kappaRef_s,TkappaRef_s,Skappa_s)
%   |--> allows to pass species sutherland quantities in case it is a
%   mono-species mixture
%
% Inputs and Out
%
% Available mixtures are:
%   - air   Source: White, Frank, Viscous Fluid Flow, ed. 2, p. 32, Table 1-3
%           Recommended temperature range: 160 - 2000 K, ±2% error
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
%
% GNU Lesser General Public License 3.0

if isempty(varargin)
    muRef_s = [];    TmuRef_s = [];    Smu_s = [];    kappaRef_s = [];    TkappaRef_s = [];    Skappa_s = [];
else
    ic=1;
    muRef_s    = varargin{ic};ic=ic+1;  TmuRef_s    = varargin{ic};ic=ic+1;     Smu_s    = varargin{ic};ic=ic+1;
    kappaRef_s = varargin{ic};ic=ic+1;  TkappaRef_s = varargin{ic};ic=ic+1;     Skappa_s = varargin{ic};%ic=ic+1;
end

mixture = options.mixture;
cstPr = options.cstPr;

if strcmp(options.modelTransport,'Sutherland') || strcmp(options.modelTransport,'powerLaw')
    % defining defaults for each mixture (ex: 'air2', 'Air')
    [tokens,~] = regexp(mixture,'(^[a-zA-Z_]+)\d*','tokens','once');
    mixture_abbrev = tokens{1}; % abbreviated mixture string
    switch mixture_abbrev
        case {'air','Air'}
            mu_ref      = 1.716e-5;
            T_ref       = 273.15;
            Su_mu       = 110.6;
            kappa_ref   = 0.0241;
            Su_kappa    = 194;
            % Source: White, Frank, Viscous Fluid Flow, ed. 2, p. 32, Table 1-3
            % Recommended temperature range: 160 - 2000 K, ±2% error
        otherwise
            if length(muRef_s)==1 % mono-species mixture
                if TmuRef_s~=TkappaRef_s
                    error(['the reference temperature for mu and kappa in Sutherland''s law for ''',mixture_abbrev,''' are not the same. Please introduce the parameters manually, or change them in getSpecies_constants.']);
                else
                    mu_ref      = muRef_s;
                    T_ref       = TmuRef_s;
                    Su_mu       = Smu_s;
                    kappa_ref   = kappaRef_s;
                    Su_kappa    = Skappa_s;
                end
            else
                error(['the chosen mixture ''',mixture,''' is not a valid option.']);
            end
    end

    % establishing defaults
    options = standardvalue(options,'mu_ref',mu_ref);               % Reference viscosity used in Sutherland's law                  [kg/(m*s)]
    options = standardvalue(options,'T_ref',T_ref);                 % Reference temperature used in Sutherland's law                [K]
    options = standardvalue(options,'Su_mu',Su_mu);                 % Sutherland's constant corresponding to viscosity              [K]
    % define outputs
    oc = 1;
    varargout{oc} = options.mu_ref; oc = oc + 1;
    varargout{oc} = options.T_ref;  oc = oc + 1;
    varargout{oc} = options.Su_mu;  oc = oc + 1;

    if ~cstPr
        options = standardvalue(options,'kappa_ref',kappa_ref);     % Reference thermal conductivity used in Sutherland's law       [W/(m*K)]
        options = standardvalue(options,'Su_kappa',Su_kappa);       % Sutherland's constant corresponding to thermal conductivity   [W/(m*K)]

        varargout{oc} = options.kappa_ref;  oc = oc + 1;
        varargout{oc} = options.Su_kappa;  %oc = oc + 1;
    else
        varargout{oc} = nan;  oc = oc + 1;
        varargout{oc} = nan; %oc = oc + 1;
    end
else
    oc = 1;
    varargout{oc} = nan;  oc = oc + 1;
    varargout{oc} = nan;  oc = oc + 1;
    varargout{oc} = nan;  oc = oc + 1;
    varargout{oc} = nan;  oc = oc + 1;
    varargout{oc} = nan; %oc = oc + 1;
end
