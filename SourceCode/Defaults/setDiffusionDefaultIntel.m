function intel = setDiffusionDefaultIntel(intel,options)
% setDiffusionDefaultIntel sets the necessary defaults in intel for the
% diffusion theory.
%
% Usage:
%   (1)
%       intel = setDiffusionDefaultIntel(intel,options)
%
%   intel must contain: nothing
%   options must contain: bcstSc and bcstLe
%
% See also: setDefaults, readTransportOpts
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

if options.bcstLe
    warnMsgSc = '';
    warnMsgLe = 'Warning: constant-Lewis diffusion was chosen but Le_e was not fixed in intel -> the default value of 1.4 will be used.';
elseif options.bcstSc
    warnMsgSc = 'Warning: constant-Schmidt diffusion was chosen but Sc_e was not fixed in intel -> the default value of 0.7 will be used.';
    warnMsgLe = '';
else
    warnMsgSc = '';
    warnMsgLe = '';
end
intel = standardvalue(intel,'Sc_e',0.7,warnMsgSc); % Schmidt number
intel = standardvalue(intel,'Le_e',1.4,warnMsgLe); % Lewis number