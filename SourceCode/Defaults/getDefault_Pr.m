function Pr = getDefault_Pr(options)
% getDefault_Pr(options) fixes the default value of the Prandtl
% number that should be used (if a constant Prandtl option is chosen)
% depending on the gas mixture.
%
% Available mixtures:
%   - air   0.7
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

mixture = options.mixture;

% grab any non-digit characters before the digit
[tokens,~] = regexp(mixture,'(^[a-zA-Z_]+)\d*','tokens','once');
mixture_abbrev = tokens{1}; % abbreviated mixture string

switch mixture_abbrev
    case {'air','airC','oxygen','Air','Air-Ar','Air-He','Air-Ne','Air-CO2','Ar','N'}
        Pr_default  = 0.7;
    case 'mars'
        Pr_default  = 0.71;
    otherwise
        error(['the chosen mixture ''',mixture,''' is not a valid option for a constant Prandtl']);
end

if options.cstPr
    warning_msg = ['A constant Pr was chosen, but none was specified, so it was fixed to the default value for an ',mixture_abbrev,' mixture - ',num2str(Pr_default)];
    options = standardvalue(options,'Pr',Pr_default,warning_msg);
else % placeholder - it doesn't really matter what Pr is, if we don't do cstPr
    options = standardvalue(options,'Pr',Pr_default);
end

Pr = options.Pr;

end