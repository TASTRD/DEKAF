function myval_out = protectedvalue(mystruct,varstr,myval,varargin)
%PROTECTEDVALUE Prevent the assignment of a struct's field
%
%   Examples:
%       (1)     myval = protectedvalue(mystruct,varstr,myval);
%
%       (2)     myval = protectedvalue(mystruct,varstr,myval,errmsg);
%           |--> pass a custom error message
%
% myval can either be a:
%   - single boolean/number/string
%   - cell with multiple strings
%   - vector with multiple boolean/numbers
%
% Author(s): Ethan Beyak
%            Fernando Miro Miro
% GNU Lesser General Public License 3.0

% Let's be clear about argument intent.
myval_out = myval;

% If the user hasn't provided any protected value, then we can leave
% with no error thrown.
if ~isfield(mystruct,varstr)
    return
end

% unifying inputs
if isnumeric(myval) || islogical(myval)
    str_myval = strrep(num2str(myval),' ',',');
elseif ischar(myval)
    str_myval = ['''',myval,''''];
    myval = {myval};
elseif iscell(myval) && ischar(myval{1})
    str_myval = ['''',strjoin(myval,''','''),''''];
else
    error('Unexpected case logic. Contact a developer');
end

% Ok, so the user did provide a value to the protected field.
% We need to need to see if the provided value disagrees with the protected value
if ~isempty(varargin)
    errmsg = varargin{1};
else
    errmsg = ['This function cannot work with a value of field ''', varstr, ''' other than ',str_myval, ...
        newline, 'This is a protected value!'];
end

if ~ismember(mystruct.(varstr),myval)
    error(errmsg,'DEKAF:protectedvalue');
end

end % protectedvalue
