function options = standardvalue(options,varstr,value,varargin)
% STANDARDVALUE(options,field,value) Set a default value for a chosen field
% in the struct 'options'.
%
% STANDARDVALUE(options,field,value,warnMess) allows to pass also a warning
% message to be displayed while imposing the default.
%
% Author(s): Koen Groot
%            Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

%  Returns the struct itself.
if (~isfield(options,varstr) || isempty(options.(varstr)))
    options.(varstr) = value;
    if nargin==4 % we print a warning
        % Adding an unused MSGID for the second argument allows formatted
        % warning messages, e.g. using the newline (\n) and tab (\t) chars.
        warning(varargin{1},'DEKAF:standardvalue');
    end
end

end
