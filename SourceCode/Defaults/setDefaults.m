function opts = setDefaults(opts)
% setDefaults(opts) fixes all the defaults on the different options
% needed to run DEKAF, both in self-similar and marching mode. The
% different options available and the different defaults fixed are:
%
%  A) Domain and numerics options
%  - N
%    |--->  number of points in the eta direction
%           default: 100
%  - L
%    |--->  non-dimensional height of the computational domain
%           default: 100
%  - eta_i
%    |--->  non-dimensional parameter fixing the mapping. It determines
%           what eta location will have half of the points over and under
%           it.
%           default: 6
%  - N_thread
%    |--->  specify the integer number of threads used in parfor's
%           throughout DEKAF. If 1, then a parallel pool is never initiated
%           tip: to access your physical cores available, use the
%           undocumented capability, feature('numcores').
%           see https://stackoverflow.com/a/2705675/11141655 for details
%           (Accessed last 2020-02-19)
%           default: 1
%  - chebydist
%    |--->  boolean identifying if the chebyshev distribution should be
%           used (true) or finite-differences
%           default: true
%  - expandBaseFlow
%    |--->  boolean identifying if the variable ghat will be expanded about
%           the base flow with respect to variables T and dfdeta.
%           see eval_thermo.m for more details.
%           OLD OPTION, NOT SUPPORTED
%           default: false
%
%  B) Boundary conditions
%  - thermal_BC
%    |--->  string identifying the type of thermal wall boundary condition
%           to be used. The possibilities are:
%       'adiab'         - adiabatic wall
%       'H_F'           - allows to fix the heat-flux at the wall
%       'Twall'         - allows to fix a dimensional value for the wall T
%       'TwallProfile'  - same as Twall, only that one can input a profile
%           *P          rather than a lone value. This option requires G_bc
%                       to have the same length as the edge profiles (U_e,
%                       T_e, etc.).
%                       DEVELOPERS NOTE: after build_edge_values, where the
%                       appropriate modifications are made, this option
%                       will be changed to Twall, and all following
%                       arrangements are unified for both of them.
%       'gwall'         - allows to fix a non-dimensional value for the
%                       wall g (non-dimensional total enthalpy)
%       'ablation'      - enforces the surface energy balance existing in
%                       an ablating solid wall (see Mortensen 2013)
%           *P - requires an x coordinate (in m) paired with the profile to
%           be passed through options.x_bc
%           no default
%  - G_bc
%    |--->  value to be used for the temperature boundary condition.
%           Depending on what was inputted through thermal_BC, this could
%           be a heat flux ('H_F'), a dimensional temperature ('Twall') or
%           a non-dimensional total enthalpy ('gwall'). For the 'adiab'
%           boundary condition, G_bc is not required and will be ignored.
%           no default
%  - thermalVib_BC
%    |--->  string identifying the type of vibrational thermal wall
%           boundary condition to be used. The possibilities are:
%       'adiab'         - adiabatic wall
%       'H_F'           - allows to fix the vibrational heat-flux at the wall
%       'Twall'         - allows to fix a dimensional value for the wall T
%       'TwallProfile'  - same as Twall, only that one can input a profile
%           *P          rather than a lone value. This option requires G_bc
%                       to have the same length as the edge profiles (U_e,
%                       T_e, etc.).
%                       DEVELOPERS NOTE: after build_edge_values, where the
%                       appropriate modifications are made, this option
%                       will be changed to Twall, and all posterior
%                       arangements are unified for both of them.
%       'gwall'         - allows to fix a non-dimensional value for the
%                       wall g (non-dimensional total enthalpy)
%       'ablation'      - enforces the surface energy balance existing in
%                       an ablating solid wall (see Mortensen 2013)
%       'frozen'        - vibrationally frozen wall. The vibrational wall
%                       temperature is taken equal to the vibrational edge
%                       temperature
%       'thermalEquil'  - wall in thermal equilibrium
%           *P - requires an x coordinate (in m) paired with the profile to
%           be passed through options.x_bc
%           default: the same as thermal_BC
%  - tauv_bc
%    |--->  value to be used for the vibrational temperature boundary
%           condition. Depending on what was inputted through
%           thermalVib_BC, this could be a heat flux ('H_F'), a dimensional
%           temperature ('Twall') or a non-dimensional total enthalpy
%           ('gwall'). For the 'adiab' boundary condition, G_bc is not
%           required and will be ignored.
%           default: the same as G_bc
%  - wallBlowing_BC
%    |--->  string identifying what wall-blowing condition is to be used.
%           Supported inputs:
%       'none'              no blowing
%       'SS'        *F      Self-similar: blows with a constant fw
%       'Vwall'     *Fx     blows with a constant Vw
%       'VwProfile' *FxP    allows specification of a wall-blowing profile
%                           DEVELOPERS NOTE: after build_edge_values, where
%                           the appropriate modifications are made, this
%                           option will be changed to Vwall, and all
%                           posterior arangements are unified for both of
%                           them.
%       'mwall'     *Fx     blows with a constant surface massflow
%       'mwProfile' *FxP    allows specification of a surface massflow profile
%       'ablation'          enforces the surface mixture mass conservation
%                           of an ablating solid wall (see Mortensen 2013)
%           *F - requires the complementary options.F_bc
%           *x - requires a streamwise location (x, xi, or Re) to be
%           specified in intel, as well as their associated booleans
%           (options.dimXQSS=true for x, and options.fixReQSS=true for Re)
%           *P - requires an x coordinate (in m) paired with the profile to
%           be passed through options.x_bc
%           default: 'none'
%  - F_bc
%    |--->  complementary numerical value for the wall-blowing boundary
%           condition. This could be different things depending on the
%           entry given in options.wallBlowing_BC:
%       'SS'        the self-similar blowing constant fw [-]
%       'Vwall'     the blowing velocity [m/s]
%       'VwProfile' the velocity profile paired with intel.x_e [m/s]
%       'mwall'     the blowing surface mass-flow [kg/s-m^2]
%       'mwProfile' the blowing surface mass-flow profile paired with
%                   intel.x_e [kg/s-m^2]
%           default: 0
%  - wallYs_BC
%     |--> string identifying what wall-mass-fraction boundary condition is
%     to be used. Supported inputs:
%       'nonCatalytic'      (default)
%           non-catalytic condition             (dys_dy = 0)
%       'ysWall'         *Y
%           constant-mass-fraction condition    (y_s = cst)
%       'yswProfile'     *YP
%           wall mass-fraction profile          (y_s = y_s(x))
%       'fullCatalytic'
%           fully-catalytic condition           (y_s = y_sEq)
%                           (NOT SUPPORTED YET)
%       'catalyticGamma' *Y
%           employs the gamma catalytic model.
%       'catalyticK'     *Y
%           employs the a simplified version of the gamma catalytic model
%           with a constant catalytic velocity.
%       'ablation'
%           enforces the surface species concentration conservation of an
%           ablating solid wall (see Mortensen 2013)
%       *Y requires the complementary options.Ys_bc
%       *P - requires an x coordinate (in m) paired with the profile to
%       be passed through options.x_bc
%  - Ys_bc
%    |--> complementary cell of numerical values for the wall mass fraction
%         boundary condition. This could be different things and will have
%         different defaults depending on the entry given in
%         options.wallYs_BC:
%       'ysWall' or 'yswProfile'
%           the value of each mass fraction at the wall [-]
%       'catalyticGamma'
%           the value of the catalytic recombination probability [-]
%       'catalyticK'
%           the value of the catalytic recombination velocity [m/s]
%  - wallYE_BC
%     |--> string identifying what wall elemental-mass-fraction boundary
%          condition is to be used. Supported inputs:
%       'zeroYEgradient'      (default)
%           imposes a null wall-normal gradient             (dyE_dy = 0)
%       'yEWall'         *Y
%           constant-elemental-mass-fraction condition      (y_E = cst)
%       'yEwProfile'     *YP 
%           wall elemental-mass-fraction profile            (y_E = y_E(x))
%       *Y requires the complementary options.YE_bc
%       *P - requires an x coordinate (in m) paired with the profile to
%       be passed through options.x_bc
%  - YE_bc
%     |--> complementary cell of numerical values for the wall elemental
%          mass fraction boundary condition. This could be different things
%          and will have different defaults depending on the entry given in
%          options.wallYE_BC:
%       'yEWall' or 'yEwProfile'
%           the value of each elemental mass fraction at the wall [-]
%  - x_bc
%    |--->  complementary numerical value for the dimensional streamwise
%           coordinate (x in meters), necessary when a wall profile of
%           velocities, mass fractions or temperatures is specified
%           (wallBlowing_BC = 'VwProfile' or 'mwProfile', wallYs_BC =
%           'yswProfile', and thermal_BC = 'TwallProfile')
%           default: 0
%  - inviscidFlowfieldTreatment
%    |--->  string identifying the treatment chosen for the inviscid
%           flowfield region. Supported values are:
%               'constant'
%                   the inviscid flow quantities are considered constant
%                   along the streamwise stretch.
%               '1DNonEquilibriumEuler'
%                   employs a one-dimensional non-equilibrium Euler solver
%                   along the inviscid streamline (see group of options G).
%               'airfoil'
%                   the airfoil preprocessing routine is run, to convert a
%                   pressure coefficient of an airfoil into an edge
%                   distribution. If true, please populate the fields of
%                   intel.airfoil appropriately. See convert_airfoil for
%                   more information.
%               'custom'
%                   allows the user to provide a series of custom profiles
%                   along the boundary-layer edge.
%           default: 'constant'
%
%  C) Flow options
%  - flow
%    |--->  string identifying the flow assumption. Supported values are: 
%               'CPG'
%                   calorically perfect gas
%               'TPG'
%                   thermally perfect gas
%               'TPGD'
%                   Thermally Perfect with Species Diffusion but frozen
%                   chemistry
%               'LTE'
%                   Local Thermodynamic Equilibrium with constant elemental
%                   mole fraction.
%               'LTEED'
%                   Local Thermodynamic Equilibrium with constant elemental
%                   diffusion.
%               'CNE'
%                   chemical non-equilibrium
%           default: CPG
%  - mixture
%    |--->  string identifying the mixture to be used. Available options
%           can be found in see <a href="matlab:help getMixture_constants">getMixture_constants</a>.
%           default: 'air2'
%  - EoS
%    |--->  string identifying the equation of state to be used. Supported
%           values are:
%               'idealGas'
%                   ideal-gas equation of state
%               'vanDerWaals'
%                   van-der-Waals equation of state
%               'vanDerWaals0'
%                   simpler version of the van-der-Waals equation of state,
%                   that does not require a Newton-Raphson method to
%                   retrieve the density from the pressure.
%           van-der-Waals equations are only available for CPG and TPG flow
%           assumptions with mono-species mixtures and 1T.
%           default: 'idealGas'
%  - modelTurbulence
%    |--->  string identifying the turbulence model to employ. Supported
%           values are:
%               'none'
%                   laminar computation with no modelling of turbulence
%               'SmithCebeci1967'
%                   uses the approximations proposed by Smith and Cebeci in
%                   their 1967 paper, consisting in a two-layer eddy
%                   viscosity based on Prandtl's mixing-length theory for
%                   the inner layer, and a nearly-constant value in the
%                   outer layer. The eddy Prandtl number is taken =1.
%               'SSTkomega'
%                   uses the SST k-omega two-equation turbulence model
%                   proposed by Menter.
%           the SmithCebeci1967, and the SSTkomega models are only
%           available for CPG and TPG flow assumptions with mono-species
%           mixtures and 1T.
%           default: 'none'
%  - shockJump
%    |--->  boolean to compute shock-jump relations from the given initial
%           conditions. In other words, if true, it considers U_e0, p_e0,
%           etc. to be pre-shock, and if false, it considers them
%           post-shock conditions
%           default: false
%  - CPGconeShock
%    |--->  boolean that, when true, will compute the conical shock-jump
%           relations using the CPG Taylor-Maccoll, instead of the
%           appropriate ones.
%           default: false
%  - coordsys
%    |--->  string identifying the coordinate system to be used.
%           Supported inputs:
%               'cartesian2D'
%                   2-D cartesian coordinates
%               'cone'
%                   cylindrical coordinate system for cones. Requires
%                   intel.cone_angle to be passed.
%               'cylindricalExternal'
%                   cylindrical coordinate system for an arbitrary boody of
%                   revolution, with flow external to it - the BL develops
%                   in its outer face. Requires intel.xcAxis and intel.rc
%                   to be passed.
%               'cylindricalInternal'
%                   cylindrical coordinate system for an arbitrary boody of
%                   revolution, with flow internal to it - the BL develops
%                   in its inner face. Requires intel.xcAxis and intel.rc
%                   to be passed.
%           default: 'cartesian2D'
%  - transverseCurvature
%    |--->  boolean determining if the transverse curvature terms should be
%           included in conical BLs (marching with coordsys='cone')
%           default: false for coordsys='cone' or 'cylindricalExternal'
%                    true  for coordsys='cylindricalInternal'
%           NOTE: the reason for having different defaults, is that the
%           transverse curvature term results in a diverging effective
%           viscosity for external cylindrical surfaces: mu*(1+y/r0)^2, but
%           to a converging one for internal ones: mu*(1-y/r0)^2.
%  - cstPr
%    |--->  boolean determining if the Prandtl number should be fixed and
%           to the value passed through options.Pr.
%           default: true for flow=CPG, false for all others
%  - specify_cp^1
%    |--->  boolean determining if the heat capacity at constant pressure
%           is manually fixed by the user. The user must pass it in
%           intel.cp.
%           default: false
%  - specify_gam^1
%    |--->  boolean determining if the heat capacity ratio is manually
%           fixed by the user.  The user must pass it in intel.gam.
%           default: false
%  - bChargeNeutrality
%    |--->  boolean determining if the charge-neutrality assumption must be
%           enforced - zero net sum of charges in the whole domain. (only
%           important for ionized mixtures)
%           default: true
%  - numberOfTemp
%    |--->  string identifying the number of temperatures to be considered.
%           default: '1T'
%           Supported inputs:
%       '1T'        -   one-temperature model (thermal equilibrium)
%       '2T'        -   two-temperature model: T for translational &
%                       rotational energies, and Tv for vibrational,
%                       electronic and electron energies.
%  - modelThermal
%    |--->  string identifying the thermal model to be used.
%           default: 'RRHO'
%           Supported inputs:
%       'RRHO'
%           rigid rotor and harmonic oscillator
%       'RRHO-1LA'
%           first linear approximation to the rigid rotor and harmonic
%           oscillator. h_s = cp_s(T)*T
%       'NASA7'
%           uses the polynomial fittings in the NASA-7 database. They are
%           the same coefficients used in CFD++ and collected in Thompson,
%           R. A., Lee, K.-P., & Gupta, R. N. (1990). Computer Codes For
%           The Evaluation Of Thermodynamic Properties, Transport
%           Properties, And Equilibrium Constants Of An 11-Species Air Model.   
%       'customPoly7'
%           uses a polynomial fitting like the one in the NASA-7 database,
%           but with customized coefficients. The coefficients must be
%           passed through options.customPoly
%  - neglect_hRot
%    |--->  boolean. if true, neglect rotational energy contribution to
%           specific enthalpy calculation for each species.
%           default: false
%  - neglect_hElec
%    |--->  boolean. if true, neglect electronic energy contribution to
%           specific enthalpy calculation for each species.
%           default: false
%  - neglect_hVib
%    |--->  boolean. if true, neglect vibrational energy contribution to
%           specific enthalpy calculation for each species.
%           default: false
%  - neglect_kappaEl
%    |--->  boolean. if true, neglect contribution of electrons to the
%           thermal conductivity (if the transport model allows for it).
%           default: false
%  - customPoly
%    |--->  structure containing the customized curve-fit coefficients used
%           for options.modelThermal='customPoly7'. It must contain:
%               .ATherm_s
%                   cell of size N_spec x 1, with matrices of size 7 x Nrange
%               .Tlims_s
%                   cell of size N_spec x 1, with vectors of size 1 x Nrange+1
%           further details on these fields can be found in 
%           <a href="matlab:help populate_thermalPolyCoefs">populate_thermalPolyCoefs</a>.
%           default: struct
%  - bLinearInterpThermalPoly
%    |--->  boolean to determine whether the thermal polynomial
%           coefficients should be interpolated around the limits of their
%           range of applicability (true) or not (false). This ensures
%           continuous thermal functions over the entire temperature range.
%           This option only applies to polynomial thermal models (NASA7,
%           or customPoly7).
%           default: true
%  - modelEnergyModes*1
%    |--->  string identifying the source of the species properties related
%           to their energy modes (rotational temperatures, activation
%           temperature and degenerecies of the electronic states and
%           vibrational activation temperatures).
%           ('Park90', 'mutation' or 'Bitter2015' for some species)
%           default: 'mutation'
%  - referenceVibRelax*4
%    |--->  string identifying the reference from which the vibrational
%           relaxation constants are to be obtained. Available references
%           are detailed in getPair_Vibrational_constants.
%           default: 'Park93'
%  - bParkCorrectionTauVib*4
%    |--->  boolean determining if Park's high-temperature correction for
%           the vibrational relaxation times should be applied or not
%           default: true
%  - modelTransport
%    |--->  string identifying the model used to calculate the
%           transport properties, dynamic viscosity and thermal
%           conductivity.
%           default: 'Sutherland' if flow = 'CPG', 'CE_122' otherwise
%           Supported inputs:
%      'Sutherland'
%               Sutherland's law. only available for CPG and TPG. It
%               requires options to have mu_ref, T_ref, Su_mu, and, if Pr is
%               not constant, also kappa_ref and Su_kappa. If these are not
%               provided, defaults will be taken. (see
%               getDefault_Sutherland)
%      'powerLaw'
%               Power law. only available for CPG and TPG. It  takes the
%               same T_ref and mu_ref same as Sutherland's to apply the
%               power law. It requires the user to input the exponent of
%               the power law through intel.mPowerLaw (default 1)
%      'SutherlandWilke'
%               uses Sutherland's law for each species viscosity and
%               thermal conductivity, and then uses Wilke's mixing rule to
%               obtain the mixture properties
%      'SutherlandEuckenWilke'
%               uses Sutherland's law for each species viscosity, Eucken's
%               correction to obtain the species thermal conductivities,
%               and then Wilke's mixing rule to obtain mixture properties.
%      'BlottnerEucken'
%               Blottner curve fit for species viscosity; Eucken's rule for
%               thermal conductivity, and Wilke's mixing rule
%      'KineticEuckenWilke'
%               species shear viscosity coefficients from kinetic theory
%               for the species viscosity, Eucken's rule for thermal
%               conductivity, and Wilke's mixing rule
%      'CE_###'
%               Use Chapman-Enskog with different approximation orders
%               (0,1,2,3) for viscosity mu (1), heavy-particle conductivity
%               kappaHeavy (1 or 2) and electron conductivity kappaEl (0, 2
%               or 3). The digit corresponding to the electron conductivity
%               can be left empty. Examples of combinations would be:
%                   'CE_11'   1st on mu and kappaHeavy, neglect kappaEl
%                   'CE_12'   1st on mu, 2nd on kappaHeavy, neglect kappaEl
%                   'CE_120'  1st on mu, 2nd on kappaHeavy, neglect kappaEl
%                   'CE_123'  1st on mu, 2nd on kappaHeavy, 3rd on kappaEl
%      'Yos67'
%               Yos' approximation to the Chapman-Enskog theory. Yos 1967,
%               Approximate Equations for the Viscosity and Translational
%               Thermal Conductivity of Gas Mixtures. AVSSD-0112-67-RM.
%               Also presented by Gupta et al. in their 1990 NASA paper
%      'Brokaw58'
%               Brokaw's 1958 paper; model of first approximation of the
%               Chapman-Enskog theory
%  - modelCollisionNeutral
%    |--->  string representing the primary reference for
%           collision cross-sectional area curve fit constants for neutral-
%           neutral interactions, as well as neutral-electron interactions.
%           See getPair_Collision_constants.m for supported values.
%           default: 'Wright2005Bellemans2015'
%  - modelCollisionChargeCharge
%    |--->  string representing the primary reference for
%           collision cross-sectional area curve fit constants for charge-
%           charge interactions.
%           See getPair_Collision_constants.m for supported values.
%           default: 'Mason67Devoto73'
% -  collisionRefExceptions
%    |--->  struct to specify any auxilliary sources of
%           collisional data, to replace the primary source of data
%           specified by options.modelCollison. Note
%           options.collisionRefExceptions has the following fields:
%       .references -   cell array of strings whose entries are the
%                       exceptions to options.modelCollisionNeutral and
%                       options.modelCollisionChargeCharge references.
%                       Possibilities are identical to that of the union of
%                       options.modelCollisionNeutral and
%                       options.modelCollisionChargeCharge.
%       .pairs      -   cell of cells where the rth cell corresponds to the
%                       rth reference. the rth cell is a cell array of
%                       strings whose entries correspond to the collision
%                       pairs whose references will be defined by the rth
%                       reference...
%                       i.e., options.collisionRefExceptions.references{r}
%                       ...as opposed to the primary reference...
%                       i.e., options.modelCollisionNeutral
%           See getPair_Collision_constants.m *456
%           default: .references = []; .pairs = [];
% -  bCollisionExtrap
%    |--->  boolean. if true, add artificial data points to improve
%           numerical behavior of polynomial curve fits. if false, use
%           original data unaltered.
%           default: true
% -  bCollisionRawData
%    |--->  boolean. if true, output the raw collision data to the
%           stats_fits struct in mixCnst. if false, then do not create this
%           substruct in stats_fits.
%           default: false
% -  modelDiffusion*12
%    |--->  string identifying the diffusion model to be used.
%           default: FOCE_RamshawAmbipolar
%           Supported inputs:
%       'cstSc'     - assumes the same diffusion coefficient for all
%                     species, such that the Schmidt number
%                     (Sc=mu/(rho*D_s)) remains constant. It requires
%                     intel.Sc_e as an input, otherwise defaults are taken
%                     (see setDiffusionDefaultIntel.m)
%       'cstLe'     - assumes the same diffusion coefficient for all
%                     species, such that the Lewis number
%                     (Le=Pr/Sc=rho*D_s*cp/kappa) remains constant. It
%                     requires intel.Le_e as an input, otherwise defaults
%                     are taken (see setDiffusionDefaultIntel.m)
%       [modelBinary]_[modelMulti] - Allows to choose different models for
%                                    the binary diffusion, and multicomponent.
%                     Supported inputs for modelBinary are:
%           'GuptaYos90'    - uses the curve fits in the Gupta Yos 1990
%                             NASA paper.
%           'FOCE'          - uses Chapman and Enskog's expression coming
%                             from molecular dynamics for dilute mixtures
%                             (see Magin 2004 Transport algorithms for
%                             partially ionized and unmagnetized plasmas)
%                      Supported inputs for modelMulti are:
%           'Exact'         - computes the exact multicomponent diffusion
%            (not supported)  coefficients following Hirschfelder 1964.
%                             Molecular Theory of Gases and Liquids. Pag. 541
%           'StefanMaxwell' - Inverts the Stefan-Maxwell system applying
%            (not supported)  imposing mass conservation as proposed by
%                             Magin 2004, Transport algorithms for
%                             partially ionized and unmagnetized plasmas.
%           'Ramshaw'       - uses the Ramshaw correction. Ramshaw 1993
%                             Hydrodynamic Theory of Multicomponent
%                             Diffusion and Thermal Diffusion in
%                             Multitemperature Gas Mixtures.
%           'RamshawAmbipolar' - same as Ramshaw, but enforcing the
%                             ambipolar condition.
%           'Effective'     - uses the effective diffusion coefficient as
%                             the one for the diffusion of each species
%                             within the mixture. See: Sutton 1998,
%                             Multi-Component Diffusion with Application to
%                             Computational Aerothermodynamics.
%           'EffectiveAmbipolar' - same as Effective but enforcing the
%                             ambipolar condition
%           'Binary'        - takes the binary diffusion coefficients as
%           (not recommended) the multi-component ones
%           'Klentzman'     - Uses the binary diffusion coefficients as the
%           (not recommended) effective diffusion coefficients. Note that
%                             this only works for mixtures of two species.
%                             See Klentzman's and Tumin's 2013 AIAA paper.
% -  molarDiffusion*12
%    |--->  boolean to determine if the given diffusion coefficients
%           correspond to molar or to mass diffusion.
%           default: false (cstSc) true (others)
% -  modelEquilibrium*123
%    |--->  string identifying the model for the reaction equilibrium
%           constant Keq_r.
%           default: 'RRHO'
%           Supported inputs:
%      'RRHO'      - using the partition functions assuming a Rigid-Rotor
%                    Harmonic-Oscillator behavior of molecules.
%      'Park85'    - polynomial fit proposed in Park 1985, "On Convergence
%                    of Computation of Chemically Reacting Flows"
%                    AIAA-85-0247 (only available for mixture 'air5Park85')
%      'Arrhenius' - Arrhenius-type expression resulting from dividing the
%                    forward and the backward reaction-rates
% -  Yos67_sumHeavy
%    |--->  boolean to determine if the user wants to sum over the heavy
%           species only for Yos67 transport model. See applyYos67
%           default: false
% -  inputBlottner
%    |--->  boolean to determine if the user wants to input the blottner
%           curve fits personally.
%           default: false
% -  inputCustomConstants
%    |--->  structure containing the constants customly specified by the
%           user. For example, if inputBlottner is true, then
%           inputCustomConstants should have .Amu_s, .Bmu_s and .Cmu_s.
%           default: empty structure
% -  ys_lim*12
%    |--->  double to specify the limit for the smallest allowable species
%           mass fraction. this numerical cutoff is used in order to avoid
%           transient negative mass fractions during the eta-iterative
%           process.
%           See update_solution.m
%           default: 1e-30
%  - idx_bath
%    |--->  index of the bath species (the one with the higher mass
%           fraction in general). 
%           default obtained from <a href="matlab:help getMixture_constants">getMixture_constants</a>
%  - idx_bathElement
%    |--->  index of the bath element (the one with the higher mass
%           fraction in general). 
%           default obtained from <a href="matlab:help getMixture_constants">getMixture_constants</a>
%  - bVariableIdxBath
%    |--->  employs a variable index for the bath species based on the one
%           with the highest mass fraction.
%           default: false
%  - passElementalFractions
%    |--->  boolean determining if, for an LTEED flow assumption, one
%           provides the elemental (true) or the species (false) mass
%           fractions at the boundary-layer edge
%           default: false
%  - enhanceChemistry
%    |--->  factor by which the species reaction rates will be multiplied.
%           this effectively enhances/reduces chemical activity
%           artificially.
%           default: 1
%  - highOrderTerms
%    |--->  boolean to add the high-order fluctuation terms to the forcing
%           vector.
%           default: false
%  - dimoutput
%    |--->  boolean determining if the code should output dimensional
%           quantities or not
%           default: true if marching, false otherwise
%  - dimXQSS
%    |--->  boolean to set a dimensional value for the streamwise (intel.x)
%           location at which the self-similar profiles must be evaluated
%           default: false
%  - fixReQSS
%    |--->  boolean to set a Reynolds number (intel.Re) corresponding to
%           the x location at which the self-similar profiles must be
%           evaluated.
%           default: false
%  - bVaryingEdgeQSS
%    |--->  boolean to determine whether varying edge conditions should be
%           used for QSS cases (coming from constant beta or Theta)
%           default: true
%  - BLproperties
%    |--->  boolean to determine if, in the computation of the
%           boundary layer's dimensional quantities, we should also compute
%           integral properties such as the BL height, displacement
%           thickness and so on.
%           default: false
%  - bGICM4delta99
%    |--->  boolean to GICM-interpolate the solution onto a finer grid
%           before interpolating to get the value of delta99. Only relevant
%           if options.BLproperties=true
%           default: true
%  - rotateVelocityVector
%    |--->  string determining the angle phi by which to rotate the
%           velocity vector in the post-processing within eval_dimVars.
%           That is, this only applies if .dimoutput = true
%           default: 'zero'
%           Supported inputs:
%      'streamline'- rotate to the angle of the inviscid streamline,
%                    phi = atan2(W_e,U_e)
%      'cst_sweep' - rotate to the angle of the swept geometry,
%                    phi = intel.Lambda_sweep
%                    CURRENTLY UNSUPPORTED
%      'zero'      - do not rotate. stay aligned with global coords,
%                    phi = 0
%      'rhow_IP'  - rotate to angle specified by rho*w inflection point,
%                    CURRENTLY UNSUPPORTED
%      'w_IP'      - rotate to angle specified by w inflection point,
%                    CURRENTLY UNSUPPORTED
%           See also eval_dimVars
%  - enthalpyOffset
%    |--->  boolean to determine if the origin of enthalpies should be
%           moved, in order to avoid negative enthalpies. In general it is
%           recommendable to make this offset if the species with a
%           negative formation enthalpy reaches major mass fractions in
%           some region of the domain.
%           default: true
%  - GICMresmult
%    |--->  multiplier on GICM's output resolution w.r.t. options.N.
%           default: 5
%
%  D) Iteration and convergence options
%  - it_max
%    |--->  maximum number of iterations to be performed in each eta loop
%           default: 60
%  - tol
%    |--->  tolerance of the residuals, after which the method will be
%           considered converged.
%           default: eps
%  - max_residual
%    |--->  maximum value of converged residuals at the end of each xi station.
%           if the residuals exceed this value, then xi marching will halt,
%           and DEKAF should exit gracefully.
%           default: 1e-3
%  - T_itMax*1
%    |--->  maximum number of iterations in the Newton-Raphson iterative
%           method used in eval_thermo to obtain the temperature from the
%           static enthalpy.
%           default: 50
%  - T_tol*1
%    |--->  tolerance for the Newton-Raphson iterative method used in
%           eval_thermo to obtain the temperature from the static enthalpy.
%           default: eps
%  - shock_itMax
%    |--->  maximum number of iterations to be performed when determining
%           the position of a shock (only necessary if options.shock = true)
%           default: 30
%  - shock_tol
%    |--->  tolerance of the residuals, after which the method will be
%           considered converged, when determining the position of a shock
%           (only necessary if options.shock = true)
%           default: 100*eps
%  - Fields4conv
%    |--->  list of strings identifying the different fields
%           (variables or equations) to be tracked to assess the
%           convergence of the solver.
%           default: {'df_deta3', 'dg_deta2', 'dk_deta2', 'ys', 'df_dxi',
%           'dg_dxi', 'dk_dxi', 'dys_dxi','momXeq','momZeq','enereq'} (some
%           of them are only for Cooke/ multispec/ or marching)
%  - variableNormMinimum
%    |--->  threshold for the minimum value a field may have
%           in order to be taken into consideration for the evaluation of
%           the convergence of the solver. This is introduced such that
%           there isn't a field that is identically zero, like the mass
%           fraction gradient for frozen flow (TPGD), which "fakes" our
%           convergence to be worse than it is.
%           default: 1e-9
%  - perturbRelaxation
%    |--->  relaxation factor for perturbation terms, before
%           added onto the mean flow quantity. this option is helpful in
%           extremely stiff systems where overshoot may cause a quantity to
%           reach a nonphysical value (e.g., negative enthalpy). suggested
%           usage in stiff systems is underrelaxation, say a value of 0.85.
%           this will increase required number of eta iterations, but may
%           create more well-behaved numerics. overrelaxation may decrease
%           eta iterations, but create more poorly-behaved numerics.
%           If it is 0, the user can input an annonymous function to define
%           the relaxation factor dynamically through options.relaxFactFunction
%           default: 1.0
%  - relaxFactFunction
%           annonymous function used to define the relaxation factor
%           dynamically. It is only used if options.perturbRelaxation=0. It
%           must be a function depending on:
%               conv
%                   |--> the level of convergence reached at the last
%                   iteration counter.
%               it
%                   |--> iteration counter.
%               i_xi
%                   |--> marching station counter.
%           the function may obviously be independent of some of these if
%           the user deems it necessary, but it will always be passed all
%           three as inputs.
%           For some examples of the annonymous function see 
%           <a href="matlab:help relaxFactFunction_conv">relaxFactFunction_conv</a> or <a href="matlab:help relaxFactFunction_table">relaxFactFunction_table</a>
%           default: 1
%  - it_res_sat
%    |--->  positive integer to determine how many previous marching
%           iterations should be checked for residual saturation. If
%           saturation is reached, the xi station is considered to be
%           converged, overriding the tolerance set by options.tol.
%           This option is closely related to it_res_sat_diff_val.
%           it_res_sat can be both a single value (used for all streamwise
%           locations) or a vector with different values for each xi
%           station.
%           default: 20
%               Note: By numerical saturation, we mean that the residuals
%               have leveled off at a certain order of magnitude -- similar
%               to the idea of convergence.
%  - it_res_sat_diff_val
%    |--->  positive double. this number represents the limiting value for
%           the average rate of change of the maximum residual before the
%           iterations are stopped due to numerical saturation. This option
%           is closely related to it_res_sat
%           it_res_sat_diff_val can be both a single value (used for all
%           streamwise locations) or a vector with different values for
%           each xi station.
%           See also check_res_saturation
%           default: 0.01
%  - LTE_NR_relax
%    |--->  integer. this number represents the the strength of the
%           relaxation scheme for the Newton-Raphson convergence for the
%           LTE solver. 0 for no relaxation, 1 for light relaxation, 2 for
%           moderate relaxation, and 3 for the strictest relaxation.
%           See getEquilibrium_X.m for details.
%           default: if electrons, 2. else, 0.
%  - LTETableInterpMethod
%    |--->  string identifying the method to be used when interpolating the
%           initial guess for the equilibrium mole fractions.
%           Supported values:
%               'cheb-interp1'  uses a spline interpolation on the
%                               pressure, to then use chebyshev on the
%                               temperature it is the most accurate, but it
%                               requires all points to be interpolated
%                               separately, so it takes a lot of time.
%               'interp2'       uses matlab's 2D built-in spline
%                               interpolation. It is significantly faster,
%                               but less accurate
%           default: 'interp2'
%  - ODEtauvMethod
%    |--->  string identifying the method to be used in the resolution of
%           the ODE of the vibrational energy equation for the initial
%           guess on tauv. Only necessary if options.model_tauv = ODE
%           Supported values: detailed in eval_ODEtauv
%           default: 'ChebyNR'
%  - bReturnNonDimFieldsIfDimFail
%    |--->  boolean determining whether, in the instance of an error in
%           eval_dimVars, the code should break (false) or return the
%           non-dimensional profiles (true).
%           default: true
%  E) Marching options @1
%  - noseFlow
%    |--->  for marching computations, this string is identifying the flow
%           assumption to be employed for the quasi-self-similar
%           computation performed at the first streamwise station. The
%           flow-field is assumed quasi-self-similar in the preceeding
%           streamwise range.
%           default: 'TPGD' for options.flow='CNE'
%                    options.flow otherwise
%  - xSpacing
%    |--->  string identifying the spacing to be used in the streamwhise
%           physical x direction.
%           Supported inputs:
%       'custom_x'
%               the user must pass the x vector on which the boundary layer
%               is to be solved through options.mapOpts.x_custom
%       'custom_xi'
%               the user must pass the transformed xi vector on which the
%               boundary layer is to be solved through
%               options.mapOpts.xi_custom.
%       'linear'    (default)
%               uses a linear spacing based on options.mapOpts.N_x,
%               x_start, and x_end
%       'log'
%               uses a logarithmic spacing based on options.mapOpts.N_x,
%               x_start, and x_end
%       'tanh'
%               uses a hyperbolic tangent spacing based on
%               options.mapOpts.N_x, x_start, x_end, XI_i, and frac_XIi
%           default: 'tanh' if opts.runAirfoil is true, else 'linear'
%  - meth_edge_interp
%    |--->  string identifying the interpolation scheme to be used in the
%           xi direction when building the edge values of the boundary.
%           default: 'spline'
%           Supported inputs:
%       all of MATLAB's supported input for METHOD in interp1, that is,
%         'linear'   - (default) linear interpolation
%         'nearest'  - nearest neighbor interpolation
%         'next'     - next neighbor interpolation
%         'previous' - previous neighbor interpolation
%         'spline'   - piecewise cubic spline interpolation (SPLINE)
%         'pchip'    - shape-preserving piecewise cubic interpolation
%         'cubic'    - same as 'pchip'
%         'v5cubic'  - the cubic interpolation from MATLAB 5, which doesn't
%                      extrapolate and uses 'spline' if X is not equally
%                      spaced.
%
%  - mapOpts
%    |--->  structure containing all the necessary options for the xi
%           mapping. It contains:
%       %1      x_custom        [m]
%               |--->   vector of customly-chosen streamwise positions on
%                       which to run the marching for options.xSpacing =
%                       'custom_x'
%       %2      xi_custom       [kg^2/m^2-s^2]
%               |--->   vector of customly-chosen streamwise positions on
%                       which to run the marching for options.xSpacing =
%                       'custom_xi'
%       %345    N_x
%               |--->   number of points in the xi discretization
%                       default: 500
%       %345    x_start         [m]
%               |--->   initial position for the marching in the x direction.
%                       default: none (needed)
%       %345    x_end           [m]
%               |--->   final position during the marching in the x direction.
%                       default: none (needed)
%       %5      deltax_start    [m]
%               |--->   first value of the marching step
%                       default: 0.01 * (x_end-x_start)/(N_x-1)
%       %5      XI_i            [-]
%               |--->   intermediate value of XI, where the step will have
%                       reached a certain fraction of the final asymptotic
%                       value
%                       default: 0.6
%                       (the last 40% of the x points will have a minimum
%                       step of frac_XIi times the final step)
%       %5      frac_XIi        [-]
%               |--->   fraction of the final asymptotic value to be
%                       reached at XI_i
%                       default: 0.8
%           %1 only for 'custom_x' spacing
%           %2 only for 'custom_xi' spacing
%           %3 only for 'linear' spacing
%           %4 only for 'log' spacing
%           %5 only for 'tanh' spacing
%  - ox
%    |--->  order of accuracy of the marching discretization (1-6)
%           default: 2
%  - restartFromMarching
%    |--->  boolean to determine if the marching is being restarted from a
%           previous marching. Requires the intel structure from the
%           previous run to be passed through options.intelQSS.
%           default: false
%  - restartFromMarching_ixi
%    |--->  integer identifying at what streamwise position the solution
%           must be restarted from if options.restartFromMarching=true. 
%           default: NaN (fixed in <a href="matlab:help update_intel4restart">update_intel4restart</a>)
%  - intelQSS
%    |--->  intel structure with the flowfield to be used for the
%           restarting of the solution if options.restartFromMarching = true.
%           default: struct
%  - xDim0
%    |--->  boolen which will determine if, when obtaining dimensional
%           values, x must be corrected to account for the initial xi
%           position (false) or if it should start from 0 (true).
%           default: false
%  - iXi_stop
%    |--->  allows to specify the value of the streamwise station at which
%           the marching must be halted. This is useful to perform faster
%           regression tests.
%           default: options.mapOpts.N_x
%
%  F) Inviscid solver   (only if options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler')
%  - inviscidMethod
%    |--->  string identifying the method to be used for the inviscid 1D
%           solver. Supported inputs:
%       'ode45'     uses matlab's built-in ode45 function
%       'FD'        uses backward diferences of order options.inviscid_ox
%           default: 'FD'
%  - inviscid_ox
%    |--->  order of the marching in the invisicid solver.
%           default: options.ox
%  - xInviscidSpacing
%    |--->  string identifying the spacing to be used in the inviscid
%    solver in the x direction. 
%           default: 'linear'
%           Supported inputs:
%       'linear'        uses a linear spacing
%       'log'           uses a logarithmic spacing
%       'tanh'          uses a hyperbolic tangent spacing
%  - inviscidMap
%    |--->  structure containing all the necessary options for the xi
%           mapping. It contains:
%               N_x
%               |--->   number of points in the x discretization
%                       default: 2000
%               x_start
%               |--->   initial position for the inviscid marching in the x
%                       direction.  [m]
%                       default: 0.9*options.mapOpts.x_start
%               x_end
%               |--->   final position durig the marching in the x
%                       direction.  [m]
%                       default: 1.1*options.mapOpts.x_end
%       %1      deltax_start
%               |--->   first value of the marching step
%                       default: 0.01 * (x_end-x_start)/(N_x-1)
%       %1      XI_i
%               |--->   intermediate value of X, where the step will have
%                       reached a certain fraction of the final asymptotic
%                       value
%                       default: 0.5
%                       (the last 40% of the x points will have a minimum
%                       step of frac_XIi times the final step)
%       %1      frac_XIi
%               |--->   fraction of the final asymptotic value to be
%                       reached at XI_i
%                       default: 0.8
%
%  G) Airfoil       (only if options.inviscidFlowfieldTreatment = 'airfoil')
%    |--->  details provided in the <a href="matlab:help convert_airfoil">convert_airfoil</a> function.
%
%  H) Importing solution options
%  - importSolution
%    |--->  determines if a solution has to be imported for the
%           inital guess or not. It's options are false, 'struc' or 'file',
%           depending on whether the guess is to be passed through a
%           structure or a presaved file.
%           default: false
%  - FileName
%    |--->  path to the file with the solution with which the
%           initialization must be done
%           no default
%  - importedIntel
%    |--->  structure to contain the fields of the imported solution
%           no default
%
%  I) Plotting and display options
%  - display
%    |--->  boolean to determine if messages should be outputted to the
%           user or not.
%           default: true
%  - quiet
%    |--->  boolean to reduce the number of messages printed by the solver
%           default: false
%  - silenceWarnings
%    |--->  boolean to stop any warning message from popping up
%           default: false
%  - displayHeader
%    |--->  boolean to choose whether to print out the function
%           header (the fancy DEKAF mug).
%           default: true
%  - plotRes
%    |--->  boolean to determine if the residuals should be plotted.
%           default: true
%  - plot_transient
%    |--->  boolean to determine if the profiles should be plotted at
%           several loop counts throughout the iteration
%           default: false
%  - plot_marching
%    |--->  boolean to determine if the profiles should be plotted at
%           several marching locations
%           default: false
%  - plot_marchPos
%    |--->  vector identifying the values of i_xi for which the
%           profiles will be plotted during the marching
%           default: [1,2,5,10,100,200,500,1000,2000,5000,10000]
%  - fields2plot
%    |--->  list of fields to be plotted
%           default: all system unknowns, plus j and C (non-dimensional
%           density and viscosity).
%  - plot_massFractions
%    |--->  boolean to determine if (in multi-species flow
%           assumptions) there should be a separate plot with the mass fractions.
%           default: true
%  - subplot_vertical
%    |--->  boolean to determine if mass-fraction subplot should
%           be arranged vertically (true) or horizontally (false)
%           default: false
%  - it_plot
%    |--->  list of loop counts at which the profiles are to be plotted
%           default: [1:5,10,50,100,150]
%  - plot_options
%    |--->  cell with any of MATLAB's plotting variable options
%           to the plotting function. For example, if we want to specify a
%           plot style 'x--', make the line width of 2, and make the color
%           equal to RGB 110, we should introduce options.plot_options =
%           {'x--','linewidth',2,'color',[1,1,0]}
%           default: ''
%  - MarchingGlobalConv
%    |--->  boolean to plot several 3D graphs with the residual in every
%           point of the domain after the marching.
%           default: true
%  - xlim
%    |--->  horizontal limits for the profile plots (on the non-dim. variab)
%           default: [-10,max(j)]
%  - ylim
%    |--->  vertical limits for the profile plots (on eta)
%           default: [0,20]
%  - logPlots
%    |--->  boolean to make the scales in the plots logarithmic
%           default: false
%  - gifConv
%    |--->  boolean to create an animated gif with the profile's
%           convergence
%           default: false
%  - savePath_gif
%    |--->  path in which to save the gif created if gifConv=true
%           default: 'gifConv_DEKAF'
%  - gifLapse
%    |--->  time in seconds to be spent on every gif frame
%           default: 1
%  - gifStep
%    |--->  iteration step to store frames
%           default: 1
%  - fields4gif
%    |--->  list of fields to plot in the gif created if gifConv=true
%           default: {'fh','dfh_deta','dfh_deta2','dfh_deta3','gh','dgh_deta','dgh_deta2'}
%  - gifxlim & gifylim
%    |--->  limits for the profile plots in the gif (on the non-dimensional
%           quantities).
%           default: same as xlim & ylim
%  - gifLogPlot
%    |--->  boolean to make the scales in the gif plots logarithmic
%           default: false
%  - gifMarch
%    |--->  boolean to create a gif of the evolution of the BL profiles as
%           the solution is marched
%           default: false
%  - savePath_gifMarch
%    |--->  path in which to save the marching gif created if gifMarch=true
%           default: 'gifMarch_DEKAF'
%  - gifMarchStep
%    |--->  marching step to store frames
%           default: 1
%  - gifMarchLapse
%    |--->  time in seconds to be spent on every marching gif frame
%           default: 0.1
%  - gifMarchxlim & gifMarchylim
%    |--->  limits for the profile plots in the marching gif (on the
%           non-dimensional quantities).
%           default: same as xlim & ylim
%  - gifMarchLogPlot
%    |--->  boolean to make the scales in the gif plots logarithmic
%           default: false
%  - fields4gifMarch
%    |--->  list of fields to plot in the marching gif created if gifMarch=true
%           default: {'f','df_deta','df_deta2','df_deta3','g','dg_deta','dg_deta2','C','dC_deta','j'}
%  - plotInviscid
%    |--->  boolean to plot the solution of the inviscid solver
%           default: true
%
%  J) Mathematical models for initial condition guesses
%  - model_g
%    |--->  string to specify different mathematical model for g profile
%           Supported inputs:
%       'const'     -   constant
%       'polyexp'   -   cubic polynomial times exponential of a quadratic
%                       polynomial  (default)
%       'tanh'      -   hyperbolic tangent
%       'poly'      -   2nd order polynomials
%           default: fixed in build_IC
%  - model_f
%    |--->  string to specify different mathematical model for f profile
%           Supported inputs:
%       'polyexp'   -   cubic polynomial times exponential of a quadratic
%                       polynomial  (default)
%       'quad'      -   quadratic polynomial
%       'tanh'      -   hyperbolic tangent
%       'poly'      -   2nd order polynomials
%           default: fixed in build_IC
%  - model_tauv
%    |--->  string to specify different mathematical model for tauv profile
%           Supported inputs:
%       'polyexp'   -   cubic polynomial times exponential of a quadratic
%                       polynomial
%       'exp'       -   exponential function tauv_e+deltatauv*exp(-2*eta)
%           default: fixed in build_IC
%  - dn
%    |--->  value representing 99% boundary-layer thickness
%           default: 5
%           See build_IC.m
%
%  X) Internal options to DEKAF (not fixed by the user). No defaults
%  - marchYesNo
%    |--->  boolean keeping track of wether the solution is being marched
%           or if we are in the self-similar case.
%  - bPairDiff
%    |--->  boolean keeping track of wether the chosen diffusion assumption
%           has paired diffusion or not (see modelDiffusion in block B)
%  - bcstSc
%    |--->  boolean keeping track of wether we are using a constant Schmidt
%           number diffusion model or not
%  - bcstLe
%    |--->  boolean keeping track of wether we are using a constant Lewis
%           number diffusion model or not
%  - modelDiffusionBinary
%    |--->  string identifying the model to be used for the binary
%           diffusion coefficients.
%  - modelDiffusionMulti
%    |--->  string identifying the model to be used for the multicomponent
%           diffusion coefficients.
%  - CEOrderMu
%    |--->  integer keeping track of the chosen order for the
%           Chapman-Enskog approximation on the viscosity
%  - CEOrderKappaHeavy
%    |--->  integer keeping track of the chosen order for the
%           Chapman-Enskog approximation on the heavy-particle thermal
%           conductivity
%  - CEOrderKappaEl
%    |--->  integer keeping track of the chosen order for the
%           Chapman-Enskog approximation on the electron thermal
%           conductivity
%  - trackerTwall
%    |--->  tracker where the isothermal wall-temperature is stored when a
%           'Twall' or 'TwallProfile' thermal BC is chosen. This is done so
%           that the enforcement of the non-dimensional total enthalpy at
%           the wall (g) can be correctly updated for a varying cp.
%  - trackerTvwall
%    |--->  tracker where the isothermal wall vibrational-temperature is
%           stored when a 'Twall', 'TwallProfile' or 'frozen' thermalVib BC
%           is chosen. This is done so that the enforcement of the
%           non-dimensional total enthalpy at the wall (gv) can be
%           correctly updated for a varying cpv.
%  - trackerVwall
%    |--->  tracker where the wall-blowing velocity or mass flow are stored
%           when a 'Vwall', 'VwProfile', 'mwall' or 'mwProfile'
%           wall-blowing BC is chosen. This is done so that the enforcement
%           of the wall BC on the self-similar velocity variable (f) can be
%           correctly updated for a varying rho_w and edge conditions.
%  - trackerYsWall
%    |--->  tracker where the wall species mass fractions are stored when a
%           'ysWall', or 'yswProfile' wallYs_BC is chosen. This is done to
%           reinforce the concentration condition at the wall.
%  - trackerYEWall
%    |--->  tracker where the wall elemental species mass fractions are
%           stored when a 'yEWall', or 'yEwProfile' wallYE_BC is chosen.
%           This is done to reinforce the concentration condition at the
%           wall.
%  - NindFields4conv
%    |--->  vector with the number of subindices the fields chosen for
%           convergence (Fields4conv) have
%  - FancyFields4conv
%    |--->  cell with the fancy names of the fields chosen for convergence
%           (Fields4conv)
%  - NindFields4conv
%    |--->  vector with the number of subindices the fields chosen for
%           convergence (Fields4conv) have
%  - N_subplots
%    |--->  vector with the number of subindices the fields to plot
%           (fields2plot) have
%  - fields2plotNames
%    |--->  cell with the fancy names of the fields to plot (fields2plot)
%  - gif_Nsubplots
%    |--->  vector with the number of subindices the fields to plot in a
%           gif (fields4gif) have
%  - fields4gifNames
%    |--->  cell with the fancy names of the fields to plot in a gif
%           (fields4gif)
%  - gifMarch_Nsubplots
%    |--->  vector with the number of subindices the fields to plot in the
%           marching gif (fields4gifMarch) have
%  - fields4gifMarchNames
%    |--->  cell with the fancy names of the fields to plot in the marching
%           gif (fields4gifMarch)
%  - pTEqVecs
%    |--->  boolean to tell getEquilibrium_X that we are providing vectors
%    with pairs of p-T points, rather than a meshed p-T domain. Always true
%    for the DEKAF solvers, only false when generating LTE tables (in which
%    case it is desirable to have a mesh)
%  - mapOpts.separation_i_xi
%    |--->  integer specifying the xi-station index where
%           separation/divergence of the DEKAF solution occurs. It is
%           initially fixed to options.mapOpts.N_x, and then overwritten if
%           the solution stalls prematurely.
%  - import_addtl_edge_flds_4_QSS
%    |--->  cell array of strings where each entry is another edge-value field
%           to add to the QSS intel structure (see getQSS_intel.m for details).
%           Note to developers: this is true when running airfoil cases from
%           stagnation (intel.Ecu and Ecw need to be transferred between
%           intels, but the *_e regex does not hit them).
%           default: {}
%
% Foot-notes:
%  *1 - only available/necessary for flow~=CPG
%  *2 - not available/necessary for flow=TPG
%  *3 - not available/necessary for flow=TPGD
%  *4 - only available/necessary for numberOfTemp='2T'
%  ^1 - only affects flow=CPG
%  @1 - only for marching
%
% References:
% Gupta,  R. N.,  Yos,  J. M.,  Thompson,  R. A.,  and Lee,  K.-P.,  "A
%   Review of Reaction Rates and Thermodynamic and Transport Properties for
%   an 11-Species Air Model for Chemical and Thermal Nonequilibrium
%   Calculations to 30000 K," Nasa-rp-1232, National Aeronautics and Space
%   Administration, 1990.
%
% Brokaw, R. S. (1958). Approximate formulas for the viscosity and thermal
%   conductivity of gas mixtures. The Journal of Chemical Physics, 29(2),
%   391–397.
%
% Michael J. Wright, Deepak Bose, Grant E. Palmer, and Eugene Levin.
%   "Recommended Collision Integrals for Transport Property Computations
%    Part 1: Air Species", AIAA Journal, Vol. 43, No. 12 (2005), pp.
%    2558-2564. https://doi.org/10.2514/1.16713
%
% Mason, E. A. (1967). Transport Coefficients of Ionized Gases. Physics of
%   Fluids, 10(8), 1827. https://doi.org/10.1063/1.1762365
%
% Devoto, R. (1973). Transport coefficients of ionized argon. Physics of
%   Fluids, 16(5)
%
% G. Stuckert 1991, PhD. Thesis. Linear Stability Theory of Hypersonic
%   Chemically Reacting Viscous Flows. NOTE: this reference proposes
%   curve-fits for the neutral collisions based on many different authors.
%
% Magin, T. E. and Degrez, G., "Transport of partially ionized and
%   unmagnetized plasmas," Physical Review E, Vol. 70, 2004.
%
% Scoggins, J. (2017). Development of numerical methods and study of
%   coupled flow, radiation, and ablation phenomena for atmospheric entry.
%   von Karman Institute.
%
% Smith, A. M. O., & Cebeci, T. (1967). Numerical Solution of the Turbulent
%   Boundary Layer Equations. Douglas Aircraft Division Technical report
%   DAC 33735. 
%
% Mortensen, C. (2013). Effects of Thermochemical Nonequilibrium on
%   Hypersonic Boundary-Layer Instability in the Presence of Surface
%   Ablation or Isolated Two-Dimensional Roughness. UCLA.
%
% Millikan, R. C., & White, D. R. (1963). Systematics of vibrational
%   relaxation. The Journal of Chemical Physics, 39(12), 3209–3213.
%   https://doi.org/10.1063/1.1734182
%
% Park, C. (1993). Review of chemical-kinetic problems of future NASA
%   missions. I - Earth entries. Journal of Thermophysics and Heat
%   Transfer, 7(3), 385--398. https://doi.org/10.2514/3.496
%
% Park, C., Howe, J. T., Jaffe, R. L., & Candler, G. V. (1994). Review of
%   chemical-kinetic problems of future NASA missions. II - Mars entries.
%   Journal of Thermophysics and Heat Transfer, 8(1), 9–23.
%   https://doi.org/10.2514/3.496
%
% W. G. Vincenti and C. H. Kruger, "Introduction to Physical Gas Dynamics",
%   Wiley, New York, 1965. See Chapter 7, page 225.
%
% Menter, F. R. (1994). Two-equation eddy-viscosity turbulence models for
%   engineering applications. AIAA Journal, 32(8), 1598–1605.
%   https://doi.org/10.2514/3.12149
%
% Default values for the edge quantities and properties are detailed in
% build_edge_values.
%
% See also: build_edge_values, listOfVariablesExplained, build_IC
% getMixture_constants, setDiffusionDefaultIntel
%
% Author(s):    Fernando Miro Miro
%               Ethan Beyak
%               Koen Groot
% GNU Lesser General Public License 3.0

% A) Domain and numerics options
opts = standardvalue(opts,'N',     100);            % number of points
opts = standardvalue(opts,'L',     100);            % domain size
opts = standardvalue(opts,'eta_i', 6);              % mapping parameter
opts = standardvalue(opts,'N_thread',1);            % number of threads used in parallel pools/parfor's
opts = standardvalue(opts,'chebydist', true);       % chebyshev or FD distribution
opts = standardvalue(opts,'expandBaseFlow',false);  % expand g about base flow for T and dfdeta
opts = standardvalue(opts,'marchYesNo',false,'the field marchYesNo defining whether the BL is being marched or not was not defined. It was set to false (no march)'); % marching/nonomarching (internal option)

% B) Boundary conditions
opts = standardvalue(opts,'flow',  'CPG');                          % flow assumption (needed sooner)
%      --> Thermal (trans-rot temperature)
opts = standardvalue(opts,'thermal_BC','adiab','the field thermal_BC, which has to choose which boundary condition is used, is empty. Taken as adiabatic'); % type of wall boundary condition
if strcmp(opts.thermal_BC,'adiab') % if the user chose adiabatic, we set it to H_F, and make G_bc=0
    opts.thermal_BC = 'H_F';
    opts.G_bc = 0;
elseif ~strcmp(opts.thermal_BC,'ablation') % otherwise we need to be inputted a G_bc value
    neededvalue(opts,'G_bc','the field G_bc, which has to choose the value of the g boundary condition'); % value at the wall boundary
else
    opts = standardvalue(opts,'G_bc',NaN,'the field G_bc, which fixes the wall value of the thermal BC was set to a placeholder NaN.'); % value of thermal BC at the wall
end
if sum(strcmp(opts.thermal_BC,{'Twall','TwallProfile'}))            % if the user specifies a dimensional temperature, we will store the value
    opts.trackerTwall = opts.G_bc;                                      % in a tracker, so that the g to be imposed at the boundary can be correctly
else                                                                % otherwise
    opts.trackerTwall = NaN;                                            % we NaN it to make sure it doesn't bother us elsewhere
end                                                                 % updated for a varying wall cp.
%      --> Thermal (vib.-elec.-el temperature)
opts = standardvalue(opts,'numberOfTemp','1T');                     % number of temperatures
opts = standardvalue(opts,'thermalVib_BC',opts.thermal_BC);         % vibrational thermal boundary condition
if strcmp(opts.thermalVib_BC,'adiab') % if the user chose adiabatic, we set it to H_F, and make G_bc=0
    opts.thermalVib_BC = 'H_F';
    opts.tauv_bc = 0;
end
opts = standardvalue(opts,'tauv_bc',opts.G_bc);                     % vibrational thermal boundary condition
if sum(strcmp(opts.thermalVib_BC,{'Twall','TwallProfile','frozen'}))% if the user specifies a dimensional temperature, we will store the value
    opts.trackerTvwall = opts.tauv_bc;                                  % in a tracker, so that the g to be imposed at the boundary can be correctly
else                                                                % otherwise
    opts.trackerTvwall = NaN;                                           % we NaN it to make sure it doesn't bother us elsewhere
end                                                                 % updated for a varying wall cp.
%      --> wall blowing
opts = standardvalue(opts,'wallBlowing_BC','none');                 % wall-blowing boundary condition
if ismember(opts.wallBlowing_BC,{'Vwall','VwProfile','mwall','mwProfile'}) % if the user specifies dimensional Vw or mw, we will store the value in a tracker
    neededvalue(opts,'F_bc',['the ''',opts.wallBlowing_BC,''' wallBlowing_BC option needs options.F_bc to be provided also']);
    opts.trackerVwall = opts.F_bc;
else                                                        % otherwise
    opts.trackerVwall = NaN;                                    % we NaN it to make sure it doesn't bother us elsewhere
end
opts = standardvalue(opts,'F_bc',0);                                % value of the BC at the wall
%      --> species concentration
opts = standardvalue(opts,'wallYs_BC','nonCatalytic');              % wall-mass-fraction boundary condition
if (ismember(opts.wallYs_BC,{'ysWall','yswProfile','catalyticGamma','catalyticK'}) && ismember(opts.flow,{'CPG','TPG','LTE'})) || ...
   (ismember(opts.wallYs_BC,{'fullCatalytic'}) && ismember(opts.flow,{'CPG','TPG'}))
    error(['the chosen wallYs_BC ''',opts.wallYs_BC,''' cannot be satisfied for the chosen flow ''',opts.flow]);
end
if nnz(ismember({opts.wallYs_BC,opts.thermal_BC,opts.wallBlowing_BC},{'ablation'}))>0 && ~ismember(opts.flow,{'TPGD','CNE'})
    error(['one of the wall boundary conditions (wallYs_BC, thermal_BC, or wallBlowing_BC was chosen to ''ablation''.',newline,....
        'This is not compatible with the chosen flow assumption (''',opts.flow,'''), only ''TPGD'' and ''CNE'' are supported']);
end
opts = standardvalue(opts,'Ys_bc',NaN);                             % we fix it with NaNs for now (build_edge_values will take care of it)
%      --> elemental concentration
opts = standardvalue(opts,'wallYE_BC','zeroYEgradient');            % wall-elemental-mass-fraction boundary condition
opts = standardvalue(opts,'YE_bc',NaN);                             % we fix it with NaNs for now (build_edge_values will take care of it)
%      --> streamwise position vector paired with the BCs
if ismember(opts.wallBlowing_BC,{'VwProfile','mwProfile'})
    neededvalue(opts,'x_bc',['the chosen wallBlowing_BC ''',opts.wallBlowing_BC,''' requires a dimensional options.x_bc paired with it.']);
elseif ismember(opts.thermal_BC,{'TwallProfile'})
    neededvalue(opts,'x_bc',['the chosen thermal_BC ''',opts.thermal_BC,''' requires a dimensional options.x_bc paired with it.']);
elseif ismember(opts.wallYs_BC,{'yswProfile'})
    neededvalue(opts,'x_bc',['the chosen wallYs_BC ''',opts.wallYs_BC,''' requires a dimensional options.x_bc paired with it.']);
end
opts = standardvalue(opts,'x_bc',0);                                % position vector paired with the BCs

% C) Flow options
opts = standardvalue(opts,'Cooke', false);                          % cooke equation
opts = standardvalue(opts,'mixture','air2');                        % mixture
opts = standardvalue(opts,'EoS','idealGas');                        % equation of state
opts = standardvalue(opts,'modelTurbulence','none');                % turbulence model
opts = standardvalue(opts,'shockJump',false);                       % boolean to use shock-jump relations
opts = standardvalue(opts,'CPGconeShock',false);                    % boolean to compute the conical jump-shock using CPG Taylor-Maccoll
opts = standardvalue(opts,'coordsys','cartesian2D');                % string identifying the coordinate system to use
dflt_transverseCurvature = ~ismember(opts.coordsys,{'cone','cylindricalExternal'});
opts = standardvalue(opts,'transverseCurvature',dflt_transverseCurvature); % boolean switching on the transverse curvature in BL and cone cases
if opts.shockJump
    switch opts.coordsys
        case 'cartesian2D';     opts.shockType = 'wedge';
        case 'cone';            opts.shockType = 'cone';
        otherwise;              error(['post-shock conditions for the chosen coordinate system ''',opts.coordsys,''' are not supported.']);
    end
end
opts = standardvalue(opts,'cstPr',strcmp(opts.flow,'CPG'));         % constant Prandtl option
opts = standardvalue(opts,'specify_cp',false);                      % user-fixed heat capacity
opts = standardvalue(opts,'specify_gam',false);                     % user-fixed heat capacity ratio
opts = standardvalue(opts,'bChargeNeutrality',true);                % charge-neutrality assumption
opts = standardvalue(opts,'modelThermal','RRHO');                   % thermal model
opts = standardvalue(opts,'modelEnergyModes','mutation');           % source of the energies and degenerecies of the electronic states
opts = standardvalue(opts,'referenceVibRelax','Park93');            % source of the vibrational relaxation constants
opts = standardvalue(opts,'bParkCorrectionTauVib',true);            % high-temperature correction of the vibrational relaxation times
opts = standardvalue(opts,'neglect_hRot',false);                    % neglect rotational energy contribution to species enthalpy
opts = standardvalue(opts,'neglect_hElec',false);                   % neglect electronic energy contribution to species enthalpy
opts = standardvalue(opts,'neglect_hVib',false);                    % neglect vibrational energy contribution to species enthalpy
opts = standardvalue(opts,'neglect_kappaEl',false);                 % neglect electron contribution to thermal conductivity
opts = standardvalue(opts,'customPoly',struct);                     % customized polynomial curve-fit coefficients
opts = standardvalue(opts,'bLinearInterpThermalPoly',true);         % linear interpolation for polynomial curve-fit data
if strcmp(opts.flow, 'CPG')
    opts = standardvalue(opts,'modelTransport','Sutherland');       % mathematical model for transport properties (dynamic viscosity and thermal conductivity)
else
    opts = standardvalue(opts,'modelTransport','CE_122');
end
opts = standardvalue(opts,'modelCollisionNeutral','Wright2005Bellemans2015');    % mathematical model for collision cross-sectional areas for pairs of species (neutral-neutral + electron-neutral)
opts = standardvalue(opts,'modelCollisionChargeCharge','Mason67Devoto73');  % mathematical model for collision cross-sectional areas for pairs of species (charge-charge)
opts = standardvalue(opts,'modelDiffusion','FOCE_RamshawAmbipolar'); % model for the diffusion
opts = readTransportOpts(opts);                                     % creating all internal options for the transport properties (mu, kappa D_sl)
dflt = ~strcmp(opts.modelDiffusion,'cstSc');                        % we will set different defaults depending on the diffusion theory
opts = standardvalue(opts,'molarDiffusion',dflt);                   % quantity the diffusion coefficients refer to (true - molar fractions, false - mass fractions)
opts = standardvalue(opts,'modelEquilibrium','RRHO');                     % model for the chemical equilibrium constant
exceptions.references = []; exceptions.pairs = [];
opts = standardvalue(opts,'collisionRefExceptions',exceptions);     % exceptions to the primary reference of collision cross-sectional areas
opts = standardvalue(opts,'bCollisionExtrap',true);
opts = standardvalue(opts,'bCollisionRawData',false);
if strcmp(opts.modelEquilibrium,'Park85') && ~strcmp(opts.mixture,'air5Park85') % Checking that options.modelEquilibrium is compatible with the chosen mixture
    error(['the option ''Park85'' was chosen for modelEquilibrium, but the mixture ''',opts.mixture,''' did not correspond.']);
end
opts = standardvalue(opts,'Yos67_sumHeavy',false);                  % boolean to sum only over the heavies for the Yos67 tranport model
opts = standardvalue(opts,'inputBlottner',false);                   % boolean to manually input blottner curve fit parameters
opts = standardvalue(opts,'inputCustomConstants',struct);           % structure containing the customly inputted constants
opts = standardvalue(opts,'ys_lim',1e-30);                          % limit for smallest species mass fraction. in order to avoid spurious negative mass fractions.
opts = standardvalue(opts,'bVariableIdxBath',false);                % boolean for the variable bath index
opts = standardvalue(opts,'passElementalFractions',false);          % boolean for the elemental-fraction inputs at the edge
opts = standardvalue(opts,'enhanceChemistry',1);                    % enhancement factor for the reaction rates
opts = standardvalue(opts,'highOrderTerms',false);                  % high-order fluctuation terms in the forcing
opts = standardvalue(opts,'dimoutput',opts.marchYesNo);             % produce a dimensional output
opts = standardvalue(opts,'dimXQSS',false);                         % set a dimensional value for the x location where the QSS profiles must be evaluated
opts = standardvalue(opts,'fixReQSS',false);                        % set a Reynolds number corresponding to the x location where the QSS profiles must be evaluated
opts = standardvalue(opts,'bVaryingEdgeQSS',true);                  % vary the edge conditions for the QSS dimensionalization
opts = standardvalue(opts,'BLproperties',false);                    % compute boundary-layer integral properties
opts = standardvalue(opts,'bGICM4delta99',true);                    % use GICM to refine the grid before interpolating the value of delta99
opts = standardvalue(opts,'rotateVelocityVector','zero');           % rotate dimensional velocity vector by a given angle, phi, wrt the global coordinates
opts = standardvalue(opts,'enthalpyOffset',true);                   % move the origin of enthalpies to have always positive enthalpies
opts = standardvalue(opts,'GICMresmult',5);                         % multiplier on options.N to obtain a reasonable delta_99 (only used if BLproperties is true)
    % we want the number of species also in options, so that defaults can be fixed correctly
[~,~,~,~,~,~,opts.N_spec,spec_list] = getMixture_constants(opts.mixture,1,1,opts);
opts.N_elem = length(getElement_info(spec_list));
opts = standardvalue(opts,'inviscidFlowfieldTreatment','constant'); % inviscid flowfield treatment
if strcmp(opts.inviscidFlowfieldTreatment,'airfoil')                % airfoil treatment
    opts = standardvalue(opts,'mapOpts',struct);                        % Fixing options that are bypassed by the airfoil option
    opts.mapOpts.x_start = NaN;
    opts.mapOpts.x_end   = NaN;
    xSpacing = 'tanh';                                                  % default spacing
else                                                                % not airfoil treatment
    xSpacing = 'linear';                                                % default linear spacing
end

% D) Iteration and convergence options
opts = standardvalue(opts,'it_max',60);                 % maximum iteration of the eta loop
opts = standardvalue(opts,'tol',   eps);                % accepted tolerance
opts = standardvalue(opts,'max_residual',1e-3);         % maximum residual tolerance while xi marching
opts = standardvalue(opts,'T_itMax',50);                % maximum number of iterations for the Newton-Raphson algorithm to get T from static h
opts = standardvalue(opts,'T_tol',opts.tol);            % tolerance to be used in the Newton-Raphson algorithm to get T from static h
opts = standardvalue(opts,'shock_itMax',30);            % maximum iteration when determining a shock position
opts = standardvalue(opts,'shock_tol',   100*eps);      % accepted shock tolerance
Fields4conv = {'df_deta3','dg_deta2','momXeq','enereq'};
if opts.Cooke
    Fields4conv = [Fields4conv,{'dk_deta2','momZeq'}];
end
if strcmp(opts.numberOfTemp,'2T')
    Fields4conv = [Fields4conv,{'dtauv_deta2'}]; %,'vibenereq'}];
end
switch opts.modelTurbulence
    case {'none','SmithCebeci1967'}     % no changes
    case 'SSTkomega';                   Fields4conv = [Fields4conv , {'dKT_deta2','dWT_deta2'}];
    otherwise;                          error(['the chosen modelTurbulence ''',opts.modelTurbulence,''' is not supported']);
end
opts = standardvalue(opts,'Fields4conv',   Fields4conv);        % variables to be considered for the evaluation of the norm
[NindFields4conv,FancyFields4conv] = variableDatabase(opts.Fields4conv,opts);
opts.NindFields4conv =  NindFields4conv;                        % number of subindices the fields chosen for convergence have
opts.FancyFields4conv = FancyFields4conv;                       % fancy names for the convergence variables
opts = standardvalue(opts,'variableNormMinimum', 1e-9);         % value under which a variable will not be considered for the evaluation of the norm
opts = standardvalue(opts,'perturbRelaxation',1.0);             % perturbation relaxation factor
opts = standardvalue(opts,'relaxFactFunction',@(conv,it,i_xi)1);% annonymous function for the perturbation relaxation factor
opts = standardvalue(opts,'it_res_sat',20);                     % number of iterations required to prematurely leave marching loop due to residual saturation
opts = standardvalue(opts,'it_res_sat_diff_val',0.01);
if nnz(ismember(spec_list,'e-'))
    opts = standardvalue(opts,'LTE_NR_relax',2);                    % moderate relaxation on Newton-Raphson LTE solver
else
    opts = standardvalue(opts,'LTE_NR_relax',0);                    % no relaxation on Newton-Raphson LTE solver
end
opts = standardvalue(opts,'LTETableInterpMethod','interp2');        % interpolation method for the LTE tables
opts = standardvalue(opts,'ODEtauvMethod','ChebyNR');               % resolution method for the tauv initial guess
opts = standardvalue(opts,'bReturnNonDimFieldsIfDimFail',true);     % return non-dimensional fields if dimensionalization fails
opts.pTEqVecs = true;                         % INTERNAL OPTION - providing the getEquilibrium_X function a vector of p-T points, not a grid

% E) Marching options
if ismember(opts.flow,{'CNE'});  dflt_noseFlow = 'TPGD';            % for CNE, we start from a frozen flow
else;                            dflt_noseFlow = opts.flow;         % otherwise we start from whatever the flow assumption was
end
opts = standardvalue(opts,'noseFlow',dflt_noseFlow);                % flow assumption to be used at the nose
opts = standardvalue(opts,'xSpacing',xSpacing);                                 % spacing in the x direction
opts = standardvalue(opts,'meth_edge_interp','spline');                         % define the interpolation scheme for building the edge values of the boundary layer
if opts.marchYesNo
    opts = standardvalue(opts,'mapOpts',    struct);                                % structure with the mapping options
    opts.mapOpts = standardvalue(opts.mapOpts,'N_x',    500);                       % Number of xi locations
    switch opts.xSpacing
        case {'linear','log','tanh'}
        neededvalue(opts.mapOpts,'x_start',['xSpacing=''',opts.xSpacing,''' with dimXBL=true was chosen, but no value was input in mapOpts.x_start']);
        neededvalue(opts.mapOpts,'x_end',['xSpacing=''',opts.xSpacing,''' with dimXBL=true was chosen, but no value was input in mapOpts.x_end']);
        case 'custom_x'
        neededvalue(opts.mapOpts,'x_custom','xSpacing=''custom_x'' was chosen, but no value was input in mapOpts.x_custom');
        opts.mapOpts.N_x = length(opts.mapOpts.x_custom);
        case 'custom_xi'
        neededvalue(opts.mapOpts,'xi_custom','xSpacing=''custom_xi'' was chosen, but no value was input in mapOpts.xi_custom');
        opts.mapOpts.N_x = length(opts.mapOpts.xi_custom);
        otherwise
            error(['the chosen xSpacing ''',opts.xSpacing,''' is not supported']);
    end
    opts.mapOpts = standardvalue(opts.mapOpts,'x_start',NaN);                       % placeholder
    opts.mapOpts = standardvalue(opts.mapOpts,'x_end',NaN);                         % placeholder
    deltax_default = 0.01*(opts.mapOpts.x_end - opts.mapOpts.x_start)/(opts.mapOpts.N_x-1);
    opts.mapOpts = standardvalue(opts.mapOpts,'deltax_start',deltax_default);       % initial x step
    opts.mapOpts = standardvalue(opts.mapOpts,'XI_i',  0.5);                        % tanh mapping parameter
    opts.mapOpts = standardvalue(opts.mapOpts,'frac_XIi',  0.8);                    % tanh mapping parameter
    opts.mapOpts = standardvalue(opts.mapOpts,'separation_i_xi',opts.mapOpts.N_x);  % if separation occurs, this index denotes the final xi station
    opts = standardvalue(opts,'ox',      2);                                        % order of the scheme
else
    opts.mapOpts.N_x = 1;                                                           % placeholder for non-marching cases
end
opts = standardvalue(opts,'restartFromMarching',false);                         % if the marching is restarted from a previous marching
opts = standardvalue(opts,'restartFromMarching_ixi',NaN);                       % streamwise position to restart from (fixed in update_intel4restart)
opts = standardvalue(opts,'xDim0',false);                                       % if the dimensional x should start in zero or not
if opts.marchYesNo && length(opts.it_res_sat)==1                            % repmatting saturation residual count if necessary
    opts.it_res_sat = repmat(opts.it_res_sat,[1,opts.mapOpts.N_x]);
end
if opts.marchYesNo && length(opts.it_res_sat_diff_val)==1                   % repmatting saturation residual difference if necessary
    opts.it_res_sat_diff_val = repmat(opts.it_res_sat_diff_val,[1,opts.mapOpts.N_x]);
end
opts = standardvalue(opts,'iXi_stop',opts.mapOpts.N_x+1);                       % streamwise station where we should stop

% F) Inviscid solver
opts = standardvalue(opts,'inviscidMethod','FD');                               % string identifying the method to solve the inviscid flowfield
if strcmp(opts.inviscidFlowfieldTreatment,'1DNonEquilibriumEuler')
    warningMsg1 = {'No minimum x was found for the inviscid solver, so it was set to 0.9*mapOpts.x_start'};
    warningMsg2 = {'No maximum x was found for the inviscid solver, so it was set to 1.1*mapOpts.x_end'};
else
    warningMsg1 = {}; warningMsg2 = {};
end
opts = standardvalue(opts,'inviscidMap',    struct);                            % structure with the mapping options
if strcmp(opts.inviscidFlowfieldTreatment,'1DNonEquilibriumEuler')
opts.inviscidMap = standardvalue(opts.inviscidMap,'N_x',    2000);              % Number of x locations
opts.inviscidMap = standardvalue(opts.inviscidMap,'x_start',0.9*opts.mapOpts.x_start,warningMsg1{:}); % Initial x location
opts.inviscidMap = standardvalue(opts.inviscidMap,'x_end',  1.1*opts.mapOpts.x_end,warningMsg2{:});   % Final x location
deltax_default = 0.01*(opts.inviscidMap.x_end - opts.inviscidMap.x_start)/(opts.inviscidMap.N_x-1);
opts.inviscidMap = standardvalue(opts.inviscidMap,'deltax_start',  deltax_default); % initial value of the step
opts.inviscidMap = standardvalue(opts.inviscidMap,'XI_i',  0.5);                % tanh mapping parameter
opts.inviscidMap = standardvalue(opts.inviscidMap,'frac_XIi',  0.8);            % tanh mapping parameter
end
if isfield(opts,'ox');      default_inviscid_ox = opts.ox;
else;                       default_inviscid_ox = 0;                        % placeholder
end
opts = standardvalue(opts,'inviscid_ox',default_inviscid_ox);               % order of the scheme
opts = standardvalue(opts,'xInviscidSpacing','linear');                     % spacing in the xi direction

% G) Airfoil options
%%%% not specified here

% H) Importing solution options
opts = standardvalue(opts,'importSolution',false);      % import a precomputed solution
if opts.importSolution
    neededvalue(opts,'FileName','the options importSolution was chosen, but no path was specified in FileName');
end
opts = standardvalue(opts,'import_addtl_edge_flds_4_QSS',{});   % transfer additional edge-quantity fields to QSS intel not matched by regex (see getQSS_intel)

% I) Plotting and display options
opts = standardvalue(opts,'display', true);                                 % display error messages
opts = standardvalue(opts,'quiet', false);                                  % silence certain messages
opts = standardvalue(opts,'silenceWarnings', false);                        % silence warning messages
opts = standardvalue(opts,'displayHeader', true);                           % display header message
opts = standardvalue(opts,'plotRes',  true);                                % plot the final residuals
opts = standardvalue(opts,'plot_transient',false);                          % intermediate iteration plots
opts = standardvalue(opts,'plot_marching',false);                           % intermediate marching plots
opts = standardvalue(opts,'plot_marchPos',[1,2,5,10,50,100,200,500,1000,2000,5000,10000]); % intermediate marching plot locations
opts = standardvalue(opts,'plot_massFractions',true);                       % for multi-species flow assumptions, it chooses to make a plot with the species mass fractions
opts = standardvalue(opts,'subplot_vertical',false);                        % whether subplots should be on top of each other or side by side
opts = standardvalue(opts,'it_plot',[1:5,10,50,100,150]);
default_fields2plot = {'f','df_deta','df_deta2','df_deta3','g','dg_deta','dg_deta2','a1','da1_deta','C','dC_deta','j'};
switch opts.numberOfTemp
    case '1T'
        % nothing to add
    case '2T'
        default_fields2plot = [default_fields2plot,{'tauv','dtauv_deta','dtauv_deta2'}];
    otherwise
        error(['the chosen number of temperatures ''',opts.numberOfTemp,''' is not supported']);
end
opts = standardvalue(opts,'fields2plot',default_fields2plot);               % default fields to plot
[N_subplots,fields2plotNames] = variableDatabase(opts.fields2plot,opts);
opts.fields2plotNames = fields2plotNames;                                   % dependent on fields2plot
opts.N_subplots = N_subplots;                                               % dependent on fields2plot
opts = standardvalue(opts,'plot_options',{});                               % additional plotting options
opts = standardvalue(opts,'MarchingGlobalConv',true);                       % to make several 3D plots at the end of the marching, to show the residuals in each domain point
opts = standardvalue(opts,'xlim',[-Inf,Inf]);                               % x limit for the plots
opts = standardvalue(opts,'ylim',[0,min([opts.L,20])]);                     % y limit for the plots
opts = standardvalue(opts,'logPlots',false);                                % logarithmic scale on the plots
opts = standardvalue(opts,'gifConv',false);                                 % to plot a convergence gif
opts = standardvalue(opts,'savePath_gif','gifConv_DEKAF');                  % save path for the convergence gif
opts = standardvalue(opts,'gifLapse',1);                                    % time lapse for each gif frame
opts = standardvalue(opts,'gifStep',1);                                     % iteration step for each gif frame
fields4gif = {'fh','dfh_deta','gh'};
if sum(strcmp(opts.flow,{'CNE','TPGD'}))
    fields4gif = [fields4gif,{'ysh'}];
end
opts = standardvalue(opts,'fields4gif',fields4gif);                         % fields to plot in the gif
[N_subplots,fields2plotNames] = variableDatabase(opts.fields4gif,opts);
opts.fields4gifNames = fields2plotNames;                                    % dependent on fields4gif
opts.gif_Nsubplots = N_subplots;                                            % dependent on fields4gif
opts = standardvalue(opts,'gifxlim',opts.xlim);                             % x limit for the gif plots
opts = standardvalue(opts,'gifylim',opts.ylim);                             % y limit for the gif plots
opts = standardvalue(opts,'gifLogPlot',false);                              % make plots in the gif with a logarithmic scale
opts = standardvalue(opts,'gifMarch',false);                                % to plot a marching gif
opts = standardvalue(opts,'savePath_gifMarch','gifMarch_DEKAF');            % save path for the marching gif
opts = standardvalue(opts,'gifMarchLapse',0.1);                             % time lapse for each marching gif frame
opts = standardvalue(opts,'gifMarchStep',1);                                % iteration step for each marching gif frame
opts = standardvalue(opts,'gifMarchxlim',opts.xlim);                        % x limit for the marching gif plots
opts = standardvalue(opts,'gifMarchylim',opts.ylim);                        % y limit for the marching gif plots
opts = standardvalue(opts,'gifMarchLogPlot',false);                         % make plots in the marching gif with a logarithmic scale
opts = standardvalue(opts,'fields4gifMarch',{'f','df_deta','df_deta2','df_deta3','g','dg_deta','dg_deta2','C','dC_deta','j'}); % fields to plot in the marching gif
[N_subplots,fields2plotNames] = variableDatabase(opts.fields4gifMarch,opts);
opts.fields4gifMarchNames = fields2plotNames;                               % dependent on fields4gifMarch
opts.gifMarch_Nsubplots = N_subplots;                                       % dependent on fields4gifMarch
opts = standardvalue(opts,'plotInviscid',true);                             % analytical guess for g profile

% J) Mathematical models for initial condition guesses
opts = standardvalue(opts,'model_g','');                                    % analytical guess for g profile
opts = standardvalue(opts,'model_f','');                                    % analytical guess for f profile
opts = standardvalue(opts,'model_tauv','');                                 % analytical guess for tauv profile
opts = standardvalue(opts,'dn',5);                                          % approximate value of 99% boundary-layer thickness

end % setDefaults.m
