function opts = readTransportOpts(opts)
% readTransportOpts reads and interprets the modelDiffusion and
% modelTransport strings to create all internal options related with the
% transport models.
%
% The options it defines are: bcstSc, bPairDiff, modelDiffusionBinary,
% modelDiffusionMulti, CEOrderMu, CEOrderKappaHeavy, CEOrderKappaEl
%
%   Examples:
%       (1)     opts = readTransportOpts(opts)
%
% See also: setDefaults
%
% Author: Fernando Miro Miro
% GNU Lesser General Public License 3.0

modelDiffusion = opts.modelDiffusion;
modelTransport = opts.modelTransport;

switch modelDiffusion
    case 'cstSc'
        opts.bcstSc = true;
        opts.bcstLe = false;
        opts.bPairDiff = false;
        opts.modelDiffusionBinary = '';
        opts.modelDiffusionMulti = '';
    case 'cstLe'
        opts.bcstSc = false;
        opts.bcstLe = true;
        opts.bPairDiff = false;
        opts.modelDiffusionBinary = '';
        opts.modelDiffusionMulti = '';
    otherwise
        opts.bcstSc = false;
        opts.bcstLe = false;
        parts = regexp(modelDiffusion,'_','split');                     % dividing different information
        if ~isempty(regexp(parts{2},'Effective|Klentzman','once'))      % Effective or Klentzman diffusion model is a spec-mix model
            opts.bPairDiff = false;
        else                                                            % all others are spec-spec
            opts.bPairDiff = true;
        end
        opts.modelDiffusionBinary = parts{1};
        opts.modelDiffusionMulti = parts{2};
end

if strcmp(modelTransport(1:3),'CE_')                            % we need to generate the CEOrder inner options
    parts = regexp(modelTransport,'_','split');                     % dividing different information
    digits = parts{2};                                              % digits part of the string
    if length(digits)==2                                            % the electron input was omitted
        opts.CEOrderKappaEl = 0;                                        % we set it to zero
    elseif length(digits)==3                                        % the electron was given
        opts.CEOrderKappaEl = int16(str2double(digits(3)));             % it is in the third position
    else                                                            % wrong number of inputs - break
        error(['the input modelTransport ''',modelTransport,'''is not supported.']);
    end
    opts.CEOrderMu = int16(str2double(digits(1)));
    opts.CEOrderKappaHeavy = int16(str2double(digits(2)));
else                                                            % otherwise we just set them to zeros
    opts.CEOrderMu = 0;
    opts.CEOrderKappaHeavy = 0;
    opts.CEOrderKappaEl = 0;
end
