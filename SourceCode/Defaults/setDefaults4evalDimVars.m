function [intel,options] = setDefaults4evalDimVars(intel,options)
% setDefaults4evalDimVars sets the default values necessary for
% eval_dimVars.
%
% Usage:
%   (1)
%       [intel,options] = setDefaults4evalDimVars(intel,options)
%
% Author: Fernando Miro Miro
% Date: September 2019
% GNU Lesser General Public License 3.0

% Check input structures for valid and possibly deprecated options
options = assess_inputs(intel, options,'onlyRenamedVars');

%% Defining new options
options2 = setDefaults(options);
optflds  = fieldnames(options);
optflds2 = fieldnames(options2);
idxMissing = find(~ismember(optflds2,optflds));
for ii=1:length(idxMissing)
    warning(['Option ''',optflds2{idxMissing(ii)},''' was not defined, probably the file corresponds to an older DEKAF version. A default value was assigned to it based on those in setDefaults.']);
    options.(optflds2{idxMissing(ii)}) = options2.(optflds2{idxMissing(ii)});
end

%% Checking for needed options
neededIntelFlds = {'Theta','Lambda_sweep'};
for ii=1:length(neededIntelFlds)
    if ~isfield(intel,neededIntelFlds{ii})
        error(['intel_dimVars needs intel.',neededIntelFlds{ii},' to be defined.']);
    end
end

%% Modifying outdated options
optNames  = {'modelThermal' };
oldValues = {'1T'           };
newValues = {'RRHO'         };
for ii=length(optNames):-1:1                            % looping possible outdated options
    if isequal(options.(optNames{ii}),oldValues{ii})        % if the value is equal to the old one
        options.(optNames{ii}) = newValues{ii};                 % we redefine it to its new value
        if ischar(oldValues{ii})
            strOld = oldValues{ii};
            strNew = newValues{ii};
        elseif isnumeric(oldValues{ii})
            strOld = num2str(oldValues{ii});
            strNew = num2str(newValues{ii});
        else
            error(['the file type of ''',optNames{ii},''' is not supported']);
        end
        warning(['Option field ''',optNames{ii},''' was renamed from ',strOld,' to ',strNew]);
    end
end

%% Checking mixCnst
mixCnst_dflt = getAll_constants(options.mixture,options);
if strcmp(options.flow,'CPG')
mixCnst_dflt.cp_CPG = intel.cp_e(1);
end
intel.mixCnst = compareAndPopulate_mixCnst(intel.mixCnst,mixCnst_dflt);

%% Reobtaining table paths for LTE
if ismember(options.flow,{'LTE','LTEED'})
    [LTEtablePath,XEq] = checkCompositionLTE(intel.ys_e,intel.mixCnst.Mm_s,intel.mixCnst.R0,options.mixture,options);
    if ~isequal(intel.mixCnst.LTEtablePath,LTEtablePath)
        warning(['The LTEtablePath (used for LTE) will be changed from ''',intel.mixCnst.LTEtablePath,''' to ''',LTEtablePath,'''']);
        intel.mixCnst.LTEtablePath = LTEtablePath;
    end
    if ~isequal(intel.mixCnst.XEq,XEq)
        warning(['The XEq (used for LTE) will be changed from ',num2str(intel.mixCnst.XEq),' to ',num2str(XEq)]);
        intel.mixCnst.XEq = XEq;
    end
    intel.mixCnst.LTEtableData = load(intel.mixCnst.LTEtablePath);
end

end % setDefaults4evalDimVars