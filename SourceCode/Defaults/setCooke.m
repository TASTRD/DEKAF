function Cooke = setCooke(intel)
%SETCOOKE Assign the value of Cooke based on whether there is sweep or not.
% Cooke is on if sweep is nonzero.

if isfield(intel, 'Lambda_sweep')
    Cooke = intel.Lambda_sweep ~= 0;
elseif isfield(intel, 'airfoil')
    if isfield(intel,'Lambda_w_uv');        Cooke = intel.Lambda_w_uv ~= 0;
    elseif isfield(intel, 'Lambda_w_u');    Cooke = intel.Lambda_w_u  ~= 0;
    else;                                   error('unexpected logic!');
    end
else
    Cooke = false;
end

end % setCooke
