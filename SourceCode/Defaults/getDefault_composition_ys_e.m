function ys_e = getDefault_composition_ys_e(mixture)
%getDefault_composition_ys_e Assigns the default composition at the edge of
% the domain.
%
% These defaults are established in 
%   - Air, Air-He, Air-Ne, Air-Ar, Air-CO2
%       100% Air
%   - air2, air5Park85, air5Park90, air5Park01, air5Bortner,
%   air5Stuckert91, air11Park93, air11Park01, airC6Mortensen,
%   airC11Mortensen, air2-CO2, air2-Ar
%       76.71% N2, and 23.29% O2
%   - oxygen2Park01, oxygen2Bortner
%       100% O2
%   - mars5Park94
%       100% CO2
%   - mars5Mortensen
%       97% CO2 and 3% O2
%   - mars8Mortensen
%       96.85% CO2, 3% N2 and 0.15% O2
%
% All other species have a trace value of 1e-10
%
% Author(s): Fernando Miro Miro
%            Ethan Beyak
% GNU Lesser General Public License 3.0

trc = 1e-10;
trcEl = 1e-10;

switch mixture
    case {'Air','Ar','N2'} % Air or other mono-species mixtures
        ys_e = {1};
    case {'Air-He','Air-Ne','Air-Ar','Air-CO2'} % Air & whatever
        ys_e = {1-trc,trc};
    case {'air2-He','air2-Ne','air2-Ar','air2-CO2'} % air2 (O2 & N2) & whatever
        ys_e = {0.2329,0.7671-trc,trc};
    case 'air2' % O2 & N2
        ys_e = {0.2329,0.7671};
    case {'air5Park85','air5Park90','air5Park01','air5Bortner','air5Stuckert91'} % N, O, NO, N2 & O2
        ys_e = {trc,trc,trc,0.7671-3*trc,0.2329};
    case {'air11Park93','air11Park93Bose'} % N, O, N2, O2, NO, N+, O+, N2+, O2+, NO+, e-
        ys_e = {trc,trc,0.7671-8*trc-1e-30,0.2329,trc,trc,trc,trc,trc,trc,trcEl};
    case 'air11Park01' % e-, N, N+, O, O+, NO, N2, N2+, O2, O2+, NO+
        ys_e = {1e-30, trc, trc, trc, trc, trc, 0.7671-8*trc-trcEl, trc, 0.2329, trc, trc};
    case {'oxygen2Park01','oxygen2Bortner'} % O & O2
        ys_e = {trc,1-trc};
    case {'mars5Park94'} % O, C, CO, O2 & CO2
        ys_e = {trc,trc,trc,trc,1-4*trc};
%         ys_e = {trc,trc,trc,0.03,0.97-3*trc};
    case {'mars5Mortensen'} % CO2,CO,C,O,O2
        ys_e = {0.97-3*trc,trc,trc,trc,0.03};
    case {'mars8Mortensen'} % CO2,CO,C,N,O,NO,N2,O2
        ys_e = {0.9685-5*trc,trc,trc,trc,trc,trc,0.03,0.0015};
    case 'airC6Mortensen' % CO2,N,O,NO,N2,O2
        ys_e = {trc,trc,trc,trc,0.7671-4*trc,0.2329};
    case 'airC11Mortensen' % C3,CO2,C2,CO,CN,C,N,O,NO,N2,O2
        ys_e = {trc,trc,trc,trc,trc,trc,trc,trc,trc,0.7671-9*trc,0.2329};
    otherwise
        error(['the chosen mixture ''',mixture,''' is not a valid option']);
end

end % getDefault_composition_ys_e
