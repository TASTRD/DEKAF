% Test_kappaEl_3rdApprox

clear;
addpath(genpath([getenv('DEKAF_DIRECTORY')]));

%%% INPUTS
p0 = 1e4; % Pa
T = (8e3:10:20e3).';
y_s0 = [6.7e-10,0.023,1.6e-7,0.07,2e-7,0.016,0.731,1.75e-5,0.16,9.9e-11,1.75e-5];
mixture = 'air11mutation';
opts = struct;
%%% END OF INPUTS

% preparing inputs
[mixCnst,opts] = getAll_constants(mixture,opts);
fldnms = fieldnames(mixCnst);
for ii=1:length(fldnms)                                     % extracting everything from mixCnst
    eval([fldnms{ii},' = mixCnst.',fldnms{ii},';']);
end

N_eta = length(T);
y_s0 = enforce_chargeNeutralityCond(y_s0,bElectron,bPos,Mm_s,R0,'massFrac','ys_lim',opts.ys_lim);  % charge-neutrality condition
y_s0 = enforce_concCond(y_s0,idx_bath);                                                     % concentration condition
y_s = repmat(y_s0,[N_eta,1]);
p = repmat(p0,[N_eta,1]);

rho = getMixture_rho(y_s,p,T,T,R0,Mm_s,bElectron);

kappaEl_2 = getElectron_kappa(2,T,T,y_s,rho,p,Mm_s,bElectron,bPos,kBoltz,R0,nAvogadro,qEl,...
    epsilon0,consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,consts_Estar,...
    consts_Omega14,consts_Omega15,consts_Omega24);

kappaEl_3 = getElectron_kappa(3,T,T,y_s,rho,p,Mm_s,bElectron,bPos,kBoltz,R0,nAvogadro,qEl,...
    epsilon0,consts_Omega11,consts_Omega22,consts_Bstar,consts_Cstar,consts_Estar,...
    consts_Omega14,consts_Omega15,consts_Omega24);

%% Computing collision integrals
[lnOmega11_sl,Tstar] = getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega11,bPos);
lnOmega22_sl         = getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega22,bPos);
lnBstar_sl = getPair_ABCstar(Tstar,consts_Bstar);
lnCstar_sl = getPair_ABCstar(Tstar,consts_Cstar);

Omega11_sl  = exp(lnOmega11_sl);
Omega22_sl  = exp(lnOmega22_sl);
Bstar_sl    = exp(lnBstar_sl);
Cstar_sl    = exp(lnCstar_sl);

[Omega12_sl,Omega13_sl] = getPair_Omega12_Omega13(Omega11_sl,Bstar_sl,Cstar_sl);% obtaining 12, and 13 collision integrals from Bstar and Cstar
lnOmega14_sl = getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega14,bPos);
lnOmega15_sl = getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega15,bPos);
lnOmega24_sl = getPair_lnOmegaij(T,T,y_s,rho,Mm_s,bElectron,kBoltz,nAvogadro,qEl,epsilon0,consts_Omega24,bPos);
lnEstar_sl = getPair_ABCstar(Tstar,consts_Estar);
lnOmega23_sl = lnEstar_sl + lnOmega22_sl;

Omega14_sl = exp(lnOmega14_sl);
Omega15_sl = exp(lnOmega15_sl);
Omega23_sl = exp(lnOmega23_sl);
Omega24_sl = exp(lnOmega24_sl);

% Enforcing that for the electron-Neutral collisions, Omega14 = Omega15 = Omega13
bCharged = getPair_bCharged(bPos,bElectron);
bCharged3D = repmat(permute(bCharged,[3,1,2]),[N_eta,1,1]); % (s,l) --> (eta,s,l)
Omega14_sl(~bCharged3D) = Omega13_sl(~bCharged3D); % we apply it on all non-charged, but we only use the electron-neutral afterwards
Omega15_sl(~bCharged3D) = Omega13_sl(~bCharged3D);

allOm.Omega11_sl = Omega11_sl;
allOm.Omega22_sl = Omega22_sl;
allOm.Omega12_sl = Omega12_sl;
allOm.Omega13_sl = Omega13_sl;
allOm.Omega14_sl = Omega14_sl;
allOm.Omega15_sl = Omega15_sl;
allOm.Omega23_sl = Omega23_sl;
allOm.Omega24_sl = Omega24_sl;

% computing Lambda functions and Debye length
[Mm,~] = getMixture_Mm_R(y_s,T,T,Mm_s,R0,bElectron);
X_s = getSpecies_X(y_s,Mm,Mm_s);
[Lambda11_ee,Lambda12_ee,Lambda22_ee] = getElectron_Lambdaij(X_s,T,bElectron,...
        Mm_s,kBoltz,nAvogadro,Omega11_sl,Omega12_sl,Omega13_sl,Omega14_sl,...
        Omega15_sl,Omega22_sl,Omega23_sl,Omega24_sl);
kappa_denom = Lambda11_ee.*Lambda22_ee - Lambda12_ee.^2;
lambda_Debye = getMixture_lambdaDebye(y_s,T,T,rho,Mm_s,nAvogadro,bPos,bElectron,epsilon0,kBoltz,qEl);
%% plotting
figure
subplot(2,2,1)
plot(T,kappaEl_2,'k'); hold on; grid minor;
plot(T,kappaEl_3,'r');
legend('2nd approx.','3rd approx.');
xlabel('T [K]'); ylabel('\kappa^{el} [W/m-K]');
subplot(2,2,2)
plot(T,lambda_Debye,'k'); hold on; grid minor;
xlabel('T [K]'); ylabel('\lambda_D [m]');

subplot(2,2,3);
plot(T,Lambda11_ee,T,Lambda12_ee,T,Lambda22_ee); grid minor;
xlabel('T [K]'); ylabel('\Lambda_{ee}^{(i,j)} [??]');
legend('\Lambda_{ee}^{(1,1)}','\Lambda_{ee}^{(1,2)}','\Lambda_{ee}^{(2,2)}')
subplot(2,2,4);
plot(T,kappa_denom); grid minor;
xlabel('T [K]'); ylabel('\kappa_{denom} [??]');
%%
figure
plot(T,Lambda11_ee,T,(Lambda12_ee.^2)./Lambda22_ee);
legend('\Lambda_{ee}^{(1,1)}','(\Lambda_{ee}^{(1,2)})^2 / \Lambda_{ee}^{(2,2)}')
xlabel('T [K]'); ylabel('\Lambda_{ee}^{(i,j)} [??]');
%%

list_Osl = {'Omega11_sl','Omega22_sl'};
list_Osel = {'Omega12_sl','Omega13_sl','Omega14_sl','Omega15_sl'};
list_Oelel = {'Omega23_sl','Omega24_sl'};

% spec-spec collisions
for ii=1:length(list_Osl)
    figure
    for s=1:N_spec
        for l=s:N_spec
            subplot(N_spec,N_spec,(s-1)*N_spec+l)
            semilogy(T,allOm.(list_Osl{ii})(:,s,l));
            title([spec_list{s},' <-> ',spec_list{l}]);
        end
    end
    suptitle(list_Osl{ii});
end

% spec-el collisions
[N_i,N_j] = optimalPltSize(N_spec);
for ii=1:length(list_Osel)
    figure
    for s=1:N_spec
        subplot(N_i,N_j,s)
        semilogy(T,allOm.(list_Osel{ii})(:,s,bElectron));
        title([spec_list{s},' <-> e-']);
    end
    suptitle(list_Osel{ii});
end

% el-el
figure
[N_i,N_j] = optimalPltSize(length(list_Oelel));
for ii=1:length(list_Oelel)
    subplot(N_i,N_j,ii)
    semilogy(T,allOm.(list_Oelel{ii})(:,bElectron,bElectron));
    title(list_Oelel{ii});
end