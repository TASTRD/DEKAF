% Test_fdcoeff is a script to test that the derivatives obtained by
% fdcoeff_1d are indeed correct.
%
% Author: Fernando Miro Miro
% Date: July 2019

clear;close all;
addpath(genpath([getenv('VESTA_DIRECTORY'),'/VESTA_code']));
addpath(genpath(getenv('DEKAF_DIRECTORY')))

% creating a randomly spaced vector
Nx = 100;
x_vec = zeros(Nx+1,1);
dx_vec = linspace(0,4*pi/Nx,Nx+1); dx_vec(1) = [];
for ii=1:Nx
%     dx = 2*pi/Nx;
%     dx = rand * 4*pi/Nx;
    dx = dx_vec(ii);
    x_vec(ii+1) = x_vec(ii) + dx;
end

sin_1 = sin(x_vec);
cos_1 = cos(x_vec);

[Dx1F1,Dx2F1] = FDdif_nonEquis(x_vec,'forward1');
[Dx1B1,Dx2B1] = FDdif_nonEquis(x_vec,'backward1');
[Dx1C1,Dx2C1] = FDdif_nonEquis(x_vec,'centered1');

[Dx1F2,Dx2F2] = FDdif_nonEquis(x_vec,'forward2');
[Dx1B2,Dx2B2] = FDdif_nonEquis(x_vec,'backward2');
[Dx1C2,Dx2C2] = FDdif_nonEquis(x_vec,'centered2');

[Dx1F3,Dx2F3] = FDdif_nonEquis(x_vec,'forward3');
[Dx1B3,Dx2B3] = FDdif_nonEquis(x_vec,'backward3');
[Dx1C3,Dx2C3] = FDdif_nonEquis(x_vec,'centered3');

[Dx1F4,Dx2F4] = FDdif_nonEquis(x_vec,'forward4');
[Dx1B4,Dx2B4] = FDdif_nonEquis(x_vec,'backward4');
[Dx1C4,Dx2C4] = FDdif_nonEquis(x_vec,'centered4');

[Dx1F5,Dx2F5] = FDdif_nonEquis(x_vec,'forward5');
[Dx1B5,Dx2B5] = FDdif_nonEquis(x_vec,'backward5');
[Dx1C5,Dx2C5] = FDdif_nonEquis(x_vec,'centered5');

cos_F1 = Dx1F1*sin_1;
cos_B1 = Dx1B1*sin_1;
cos_C1 = Dx1C1*sin_1;

cos_F2 = Dx1F2*sin_1;
cos_B2 = Dx1B2*sin_1;
cos_C2 = Dx1C2*sin_1;

cos_F3 = Dx1F3*sin_1;
cos_B3 = Dx1B3*sin_1;
cos_C3 = Dx1C3*sin_1;

cos_F4 = Dx1F4*sin_1;
cos_B4 = Dx1B4*sin_1;
cos_C4 = Dx1C4*sin_1;

cos_F5 = Dx1F5*sin_1;
cos_B5 = Dx1B5*sin_1;
cos_C5 = Dx1C5*sin_1;

sin_F1 = -Dx2F1*sin_1;
sin_B1 = -Dx2B1*sin_1;
sin_C1 = -Dx2C1*sin_1;

sin_F2 = -Dx2F2*sin_1;
sin_B2 = -Dx2B2*sin_1;
sin_C2 = -Dx2C2*sin_1;

sin_F3 = -Dx2F3*sin_1;
sin_B3 = -Dx2B3*sin_1;
sin_C3 = -Dx2C3*sin_1;

sin_F4 = -Dx2F4*sin_1;
sin_B4 = -Dx2B4*sin_1;
sin_C4 = -Dx2C4*sin_1;

sin_F5 = -Dx2F5*sin_1;
sin_B5 = -Dx2B5*sin_1;
sin_C5 = -Dx2C5*sin_1;

%% plot
figure;
subplot(2,1,1);     plot(x_vec,                          sin_1,      'k.');     xlabel('x'); ylabel('F(x)');
subplot(2,1,2);     plot((x_vec(1:end-1)+x_vec(2:end))/2,diff(x_vec),'k-');     xlabel('x'); ylabel('\Delta x');

figure; Ni=2; Nj=6; ic=0;
%%%%%%%%% FORWARD
ic=ic+1; % column index
subplot(Ni,Nj,ic);
it=1; Nclr = 5; pltFlag = 'Rbw2';
plot(x_vec,cos_F1,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
plot(x_vec,cos_F2,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,cos_F3,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,cos_F4,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,cos_F5,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,cos_1,'k--','displayname','original'); hold on;
xlabel('x');ylabel('\partial_x F');legend(); title('forward (1st)'); ylim([-1,1]);

subplot(Ni,Nj,ic+Nj);
it=1; Nclr = 5; pltFlag = 'Rbw2';
semilogy(x_vec,abs(cos_1-cos_F1),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
semilogy(x_vec,abs(cos_1-cos_F2),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
semilogy(x_vec,abs(cos_1-cos_F3),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
semilogy(x_vec,abs(cos_1-cos_F4),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
semilogy(x_vec,abs(cos_1-cos_F5),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
xlabel('x');ylabel('err(\partial_x F)');legend(); title('forward (1st) error'); ylim([1e-8,1]);

ic=ic+1; % column index
subplot(Ni,Nj,ic);
it=1; Nclr = 5; pltFlag = 'Rbw2';
plot(x_vec,sin_F1,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
plot(x_vec,sin_F2,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
plot(x_vec,sin_F3,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,sin_F4,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,sin_F5,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,sin_1,'k--','displayname','original'); hold on;
xlabel('x');ylabel('\partial_x^2 F');legend(); title('forward (2nd)'); ylim([-1,1]);

subplot(Ni,Nj,ic+Nj);
it=1; Nclr = 5; pltFlag = 'Rbw2';
semilogy(x_vec,abs(sin_1-sin_F1),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
semilogy(x_vec,abs(sin_1-sin_F2),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
semilogy(x_vec,abs(sin_1-sin_F3),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
semilogy(x_vec,abs(sin_1-sin_F4),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
semilogy(x_vec,abs(sin_1-sin_F5),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
xlabel('x');ylabel('err(\partial_x^2 F)');legend(); title('forward (2nd) error'); ylim([1e-8,1]);

%%%%%%%%% CENTRAL
ic=ic+1;
subplot(Ni,Nj,ic);
it=1; Nclr = 5; pltFlag = 'Rbw2';
plot(x_vec,cos_C1,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
plot(x_vec,cos_C2,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
plot(x_vec,cos_C3,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,cos_C4,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,cos_C5,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,cos_1,'k--','displayname','original'); hold on;
xlabel('x');ylabel('\partial_x F');legend(); title('central (1st)'); ylim([-1,1]);

subplot(Ni,Nj,ic+Nj);
it=1; Nclr = 5; pltFlag = 'Rbw2';
semilogy(x_vec,abs(cos_1-cos_C1),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
semilogy(x_vec,abs(cos_1-cos_C2),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
semilogy(x_vec,abs(cos_1-cos_C3),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
semilogy(x_vec,abs(cos_1-cos_C4),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
semilogy(x_vec,abs(cos_1-cos_C5),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
xlabel('x');ylabel('err(\partial_x F)');legend(); title('central (1st) error'); ylim([1e-8,1]);

ic=ic+1; % column index
subplot(Ni,Nj,ic);
it=1; Nclr = 5; pltFlag = 'Rbw2';
plot(x_vec,sin_C1,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
plot(x_vec,sin_C2,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
plot(x_vec,sin_C3,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,sin_C4,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,sin_C5,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,sin_1,'k--','displayname','original'); hold on;
xlabel('x');ylabel('\partial_x^2 F');legend(); title('central (2nd)'); ylim([-1,1]);

subplot(Ni,Nj,ic+Nj);
it=1; Nclr = 5; pltFlag = 'Rbw2';
semilogy(x_vec,abs(sin_1-sin_C1),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
semilogy(x_vec,abs(sin_1-sin_C2),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
semilogy(x_vec,abs(sin_1-sin_C3),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
semilogy(x_vec,abs(sin_1-sin_C4),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
semilogy(x_vec,abs(sin_1-sin_C5),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
xlabel('x');ylabel('err(\partial_x^2 F)');legend(); title('central (2nd) error'); ylim([1e-8,1]);

%%%%%%%%% BACKWARD
ic=ic+1;
subplot(Ni,Nj,ic);
it=1; Nclr = 5; pltFlag = 'Rbw2';
plot(x_vec,cos_B1,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
plot(x_vec,cos_B2,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
plot(x_vec,cos_B3,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,cos_B4,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,cos_B5,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,cos_1,'k--','displayname','original'); hold on;
xlabel('x');ylabel('\partial_x F');legend(); title('backward (1st)'); ylim([-1,1]);

subplot(Ni,Nj,ic+Nj);
it=1; Nclr = 5; pltFlag = 'Rbw2';
semilogy(x_vec,abs(cos_1-cos_B1),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
semilogy(x_vec,abs(cos_1-cos_B2),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
semilogy(x_vec,abs(cos_1-cos_B3),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
semilogy(x_vec,abs(cos_1-cos_B4),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
semilogy(x_vec,abs(cos_1-cos_B5),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
xlabel('x');ylabel('err(\partial_x F)');legend(); title('backward (1st) error'); ylim([1e-8,1]);

ic=ic+1; % column index
subplot(Ni,Nj,ic);
it=1; Nclr = 5; pltFlag = 'Rbw2';
plot(x_vec,sin_B1,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
plot(x_vec,sin_B2,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
plot(x_vec,sin_B3,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,sin_B4,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,sin_B5,'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
plot(x_vec,sin_1,'k--','displayname','original'); hold on;
xlabel('x');ylabel('\partial_x^2 F');legend(); title('backward (2nd)'); ylim([-1,1]);

subplot(Ni,Nj,ic+Nj);
it=1; Nclr = 5; pltFlag = 'Rbw2';
semilogy(x_vec,abs(sin_1-sin_B1),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
semilogy(x_vec,abs(sin_1-sin_B2),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1; hold on;
semilogy(x_vec,abs(sin_1-sin_B3),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
semilogy(x_vec,abs(sin_1-sin_B4),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
semilogy(x_vec,abs(sin_1-sin_B5),'color',two_colors(it,Nclr,pltFlag),'displayname',['Order ',num2str(it)]); it=it+1;
xlabel('x');ylabel('err(\partial_x^2 F)');legend(); title('backward (2nd) error'); ylim([1e-8,1]);
