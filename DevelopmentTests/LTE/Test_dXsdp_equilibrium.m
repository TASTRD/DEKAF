% Test_dXsdp_equilibrium is a script to compare the thermodynamic
% derivatives of the equilibrium composition computed with the general
% method, and those computed with analytical functions (dXs_dT and dXs_dp)
% or with a FD method (dXs_dXF).
%
% Author: Fernando Miro Miro
% Date: April 2020

clear;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']));

%%% INPUTS
T = (2000:100:10000).';                        % temperatures [K]
method           = 'mtimesx';               % resolution method for the equilibrium system
% mixture          = 'air5mutation';          % mixture
% yE_e             = {0.77, 0.23};            % edge concentrations
mixture          = 'air11mutation';         % mixture
yE_e             = {0,0.77, 0.23};          % edge concentrations
modelEquilibrium = 'RRHO';                  % equilibrium model
modelDiffusion   = 'FOCE_StefanMaxwell';    % diffusion model
flow             = 'LTEED';                 % flow assumption
% p_vec   = logspace(3,6,4);                  % pressures [Pa]
p_vec   = 1e5;                              % pressures [Pa]
dX      = 1e-4;                             % jump in elemental fraction
%%% END OF INPUTS

% Computing auxiliaries
NT = length(T);                             % number of temperatures
Np = length(p_vec);                         % number of pressures

options.mixture             = mixture;                  % populating options
options.modelEquilibrium    = modelEquilibrium;
options.modelDiffusion      = modelDiffusion;
options.flow                = flow;
options.T_tol               = 1e-11;
[mixCnst,options] = getAll_constants(mixture,options);  % populating mixture constants
for s=mixCnst.N_spec:-1:1
    idxEl = find(strcmp(mixCnst.elem_list,mixCnst.spec_list{s}));
    if isempty(idxEl);      ys_e{s} = 0;
    else;                   ys_e{s} = yE_e{idxEl};
    end
end
[mixCnst.LTEtablePath,mixCnst.XEq] = checkCompositionLTE(ys_e,mixCnst.Mm_s,mixCnst.R0,options.mixture,options);
mixCnst.LTEtableData = load(mixCnst.LTEtablePath);
N_elem = mixCnst.N_elem;

%% Running
for ip=Np:-1:1
    p = p_vec(ip)*ones(NT,1);
    % Numerical derivatives
    [X_s_num{ip},dXs_dT_num{ip},dXs_dp_num{ip}] = getEquilibrium_X_complete(p,T,method,mixture,mixCnst,modelEquilibrium,1,options,'computeXFgrads');
    % Analytical derivatives
    [X_s_anl{ip},dXs_dT_anl{ip},dXs_dp_anl{ip}] = getEquilibrium_X_complete(p,T,method,mixture,mixCnst,modelEquilibrium,2,options);
    % Approximate numerical derivatives (1st-order FD)
    for E=1:N_elem
        dX_vec = -dX * mixCnst.XEq/sum(mixCnst.XEq(~ismember(1:N_elem,E))); dX_vec(E) = dX;
%         dX_vec = zeros(1,N_elem); dX_vec(E) = dX;
        X_E = mixCnst.XEq + dX_vec;
        X_s_num_XE1 = getEquilibrium_X_complete(p,T,method,mixture,mixCnst,modelEquilibrium,0,options,'variable_XE',X_E,'idxE',E);
        X_E = mixCnst.XEq - dX_vec;
        X_s_num_XE2 = getEquilibrium_X_complete(p,T,method,mixture,mixCnst,modelEquilibrium,0,options,'variable_XE',X_E,'idxE',E);
        dXs_dXF_apx{ip}(:,:,E) = (X_s_num_XE1 - X_s_num_XE2)/(2*dX);
    end
    % Derivatives wrt the elemental mass fraction
    Mm_E = mixCnst.Mm_E;
    yEq = getEquilibrium_ys(mixCnst.XEq,Mm_E);
    for E=1:N_elem
        dy_vec = zeros(1,N_elem); dy_vec(E) = dX*Mm_E(E)/0.03;
        X_E = getSpecies_X(yEq + dy_vec,[],Mm_E);
        X_s_num_XE1 = getEquilibrium_X_complete(p,T,method,mixture,mixCnst,modelEquilibrium,0,options,'variable_XE',X_E,'idxE',E);
        X_E = getSpecies_X(yEq - dy_vec,[],Mm_E);
        X_s_num_XE2 = getEquilibrium_X_complete(p,T,method,mixture,mixCnst,modelEquilibrium,0,options,'variable_XE',X_E,'idxE',E);
        dXs_dyF_apx{ip}(:,:,E) = (X_s_num_XE1 - X_s_num_XE2)/(2*dX*Mm_E(E)/0.03);
    end
    % thermal diffusion coefficient
    X_E = repmat(mixCnst.XEq,[NT,1]);
    [~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,~,A_E{ip},C_EF{ip},~,dXs_dyF{ip},dXs_dXF{ip}] = getAll_properties(mixCnst,[],p,X_E,T,[],options,'guessXsLTE',X_s_num{ip});
end

%% Plotting
%{%
clear plots legends
N_spec = mixCnst.N_spec;
figure; 
[Ni,Nj] = optimalPltSize(2 + 2*N_elem);
pltFlag1 = 'mgm';
pltFlag2 = 'vrdsInv';
for ip=1:Np
    for s=1:N_spec
        clr1 = two_colors(s,N_spec+1,pltFlag1);
        clr2 = two_colors(s,N_spec+1,pltFlag2);
        name1 = ['s = ',mixCnst.spec_list{s},', numerical '];
        name2 = ['s = ',mixCnst.spec_list{s},', FD '];
        ir=0;
        ir=ir+1;subplot(Ni,Nj,ir);
                          plot(T,dXs_dT_num{ip}(:,s),'-' ,'Color',clr1); hold on;
                          plot(T,dXs_dT_anl{ip}(:,s),'--','Color',clr2);
        ir=ir+1;subplot(Ni,Nj,ir);
        plots(s)        = plot(T,dXs_dp_num{ip}(:,s),'-' ,'Color',clr1); hold on;
        plots(N_spec+s) = plot(T,dXs_dp_anl{ip}(:,s),'--','Color',clr2);
        legends{s}        = name1;
        legends{N_spec+s} = name2;
        for ie=1:N_elem
        ir=ir+1;subplot(Ni,Nj,ir);
                          plot(T,dXs_dXF{ip}(:,s,ie),    '-' ,'Color',clr1,'displayname',name1); hold on;
                          plot(T,dXs_dXF_apx{ip}(:,s,ie),'--','Color',clr2,'displayname',name2); hold on;
        end
        %{%
        for ie=1:N_elem
        ir=ir+1;subplot(Ni,Nj,ir);
                          plot(T,dXs_dyF{ip}(:,s,ie),    '-' ,'Color',clr1,'displayname',name1); hold on;
                          plot(T,dXs_dyF_apx{ip}(:,s,ie),'--','Color',clr2,'displayname',name2); hold on;
        end
        %}
    end
    ir=0;
    ir=ir+1;subplot(Ni,Nj,ir);
    title(['p = 10^',num2str(log10(p_vec(ip))),' Pa']);
    xlabel('T [K]'); ylabel('\partialX_s/\partialT [1/K]');
    ir=ir+1;subplot(Ni,Nj,ir);
    xlabel('T [K]'); ylabel('\partialX_s/\partialp [1/Pa]');
    for ie=1:N_elem
    ir=ir+1;subplot(Ni,Nj,ir);
    xlabel('T [K]'); ylabel(['\partialX_s/\partialX_{E=',num2str(ie),'} [-]']);
    end
    for ie=1:N_elem
    ir=ir+1;subplot(Ni,Nj,ir);
    xlabel('T [K]'); ylabel(['\partialX_s/\partialy_{E=',num2str(ie),'} [-]']);
    end
end
legend(plots,legends,'NumColumns',2);