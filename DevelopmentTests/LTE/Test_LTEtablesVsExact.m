% Test_LTEtablesVsExact is a script to make a simple check of the
% equilibrium mole fraction profiles obtained from interpolating on tables
% and from solving the exact equilibrium system using Newton-Raphson
%
% Author: Fernando Miro Miro
% Date: December 2017

%%%% START OF INPUTS

clear;
dekafpath = [getenv('DEKAF_DIRECTORY'),'_stable'];
addpath(genpath([dekafpath,'/SourceCode']));

% Define temperature, pressure, and elemental fractions
T = linspace(2e2,15e3,400)';
% T = 5000;
N_T = length(T);
p = 4000*ones(N_T,1);

% Air 11
options.mixture = 'air11mutation';
tableName = 'LTETables_air11_N79O21_1000T10-30000_57p1-10000000.mat';
X_e = [0.21,0.79]; % First entry, Oxygen mole fraction; Second entry, nitrogen mole fraction.
XEq = [X_e(1)/X_e(2),1]; % Air 11 (second entry is ratio of ions to electrons)

% Air 5
% options.mixture = 'air5mutation';
% tableName = 'LTETables_air5_N79O21_1000T10-30000_57p1-10000000.mat';
% X_e = [0.21,0.79]; % First entry, Oxygen mole fraction; Second entry, nitrogen mole fraction.
% XEq = X_e(1)/X_e(2);

% Air 5 Park85
% options.mixture = 'air5Park85';
% tableName = 'LTETables_air5_N80O20_1000T5-30000_57p1-10000000_Park85.mat';
% X_e = [0.2,0.8]; % First entry, Oxygen mole fraction; Second entry, nitrogen mole fraction.
% XEq = X_e(1)/X_e(2);
% options.modelEquilibrium = 'Park85';
% extraName = '_Park85';

% Oxygen 2
% options.mixture = 'oxygen2Bortner';
% tableName = 'LTETables_oxygen2_O100_1000T10-30000_57p1-10000000.mat';
% XEq = [];

%%%% END OF INPUTS

relpath2table = '/SourceCode/Utilities/LTETablesEtc/';
tablePathAbs = [dekafpath,relpath2table,tableName];
options.flow = 'LTE';
options.display = true;
[mixCnst,options] = getAll_constants(options.mixture,options);
tic
X_s = getEquilibrium_X_tables(p,T,tablePathAbs,options);
toc

% Exact solution
options.X_mat0 = X_s;
N_spec = length(mixCnst.spec_list);
mixCnst.XEq = XEq;

T_r = getReaction_T(ones(size(mixCnst.Keq_struc.nT_r)),T);
lnKeq_r = getReaction_lnKeq(T_r,T,T,T,T,T,mixCnst.delta_nuEq,mixCnst.Keq_struc,options);

R0 = mixCnst.R0;
delta_nuEq = mixCnst.delta_nuEq;                                            % difference between products and reactants in eq. cond.
lnKpeq = getEquilibrium_lnKpeq(lnKeq_r,R0,T,sum(delta_nuEq,1)');

fprintf('Running the ''mtimesx'' method...\n');
X_exact = getEquilibrium_X(p,T,lnKpeq,mixCnst.XEq,mixCnst,'mtimesx',options);

% Exact solution from enthalpy
[R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,T_mat] = ...
                      reshape_inputs4enthalpy(mixCnst.R_s,mixCnst.nAtoms_s,mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,T);
y_s = getEquilibrium_ys(reshape(X_s,[N_T,N_spec]),mixCnst.Mm_s);
[h,~,~,~]  = getMixture_h_cp(y_s,T_mat,T_mat,T_mat,T_mat,T_mat,R_s_mat,nAtoms_s_mat,thetaVib_s_mat,thetaElec_s_mat,gDegen_s_mat,bElectron_mat,hForm_s_mat,options);
[~,options.T0] = getEquilibrium_X_tables(p,h,tablePathAbs,[],'hInput',options); % initial guess on temperature (LTE tables)
%options.T0 = h/h(end) * T(end);                                             % initial guess on temperature (CPG)

fprintf('Running the ''mtimesxh'' method...\n');
[X_exacth,Th] = getEquilibrium_X(p,h,mixCnst.XEq,mixCnst,'mtimesxh',options.mixture,options);

%% Exact solution from entropy
rho = getMixture_rho(y_s,p,T,T,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron);
s = getMixture_s(T,T,T,T,T,rho,y_s,mixCnst.R_s,mixCnst.nAtoms_s,...
                      mixCnst.thetaVib_s,mixCnst.thetaElec_s,mixCnst.gDegen_s,mixCnst.bElectron,mixCnst.hForm_s,mixCnst.Mm_s,...
                      mixCnst.nAvogadro,mixCnst.hPlanck,mixCnst.kBoltz,mixCnst.L_s,mixCnst.sigma_s,mixCnst.thetaRot_s,options);

fprintf('Running the ''mtimesxs'' method...\n');
[X_exacts,Ts] = getEquilibrium_X(p,s,mixCnst.XEq,mixCnst,'mtimesxs',options.mixture,options);

%% PLOTTING TO VERIFY BOTH ITERATIVE SCHEMES FOR MOLE FRACTIONS AGAINST MUTATION
X_mtimesx   = squeeze(X_exact);
X_mtimesxh  = squeeze(X_exacth);
X_mtimesxs  = squeeze(X_exacts);
X_tables    = squeeze(X_s);

figure
clear plots legends
lnwdth = 1;
% subplot(1,2,1)
for s=1:size(X_tables,2)
    ic = 1;
plots(ic) = semilogy(T,X_tables(:,s),'g-','LineWidth',lnwdth); legends{ic} = 'tables'; ic=ic+1; hold on;
plots(ic) = semilogy(T,X_mtimesx(:,s),'b-.','LineWidth',lnwdth); legends{ic} = 'mtimesx'; ic=ic+1;
plots(ic) = semilogy(Th,X_mtimesxh(:,s),'k--','LineWidth',lnwdth); legends{ic} = 'mtimesxh'; ic=ic+1;
plots(ic) = semilogy(Ts,X_mtimesxs(:,s),'r:','LineWidth',lnwdth*1.5); legends{ic} = 'mtimesxs'; ic=ic+1;
end
legend(plots,legends);
ylim([1e-50,1]);
grid minor
ylabel('X_s')
xlabel('T [K]'); legend();
% subplot(1,2,2)
% plot(T,X_tables-X_mtimesx,'b-','LineWidth',2); hold on;
% plot(T,X_tables-X_mtimesxh,'k--','LineWidth',2); hold off;
% % ylim([min(real(X_tables(:)-X_mtimesx(:))),max(real(X_tables(:)-X_mtimesx(:)))]);
% grid minor
% ylabel('X_{s,table} - X_{s,calc}')
% xlabel('T [K]')