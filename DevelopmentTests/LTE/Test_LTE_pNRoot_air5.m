% Test_LTE_pNRoot_air5 is a script to test, for a wide range of
% temperatures, which root of the 4th-order polynomial on pN defining the
% equilibrium condition for an air-5 mixture is the correct one.

addpath(genpath('/home/miromiro/workspace/DEKAF/SourceCode/'));
close all;
clear;

%%
T_vec = (50:1:10000)';
N = length(T_vec);
p_e = 4000;
p_vec = p_e*ones(N,1);
mixture = 'air5mutation';

opts.elecStates = 'mutation';
opts.modelCollisionNeutral = 'GuptaYos90';
opts.collisionRefExceptions.references = [];
opts.collisionRefExceptions.pairs = [];
mixCnst = getAll_constants(mixture,opts);

y_s = [0.05,0.1,0.05,0.6,0.2]; % used only to check if gibbs is actually varying with p_s
y_s = repmat(y_s,[N,1]);

[Mm,~] = getMixture_Mm_R(y_s,T_vec,T_vec,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron);
X_s = getSpecies_X(y_s,Mm,mixCnst.Mm_s);

rho = getMixture_rho(y_s,p_vec,T_vec,T_vec,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron);

N_spec = mixCnst.N_spec;

opts.modelEquilibrium = 'RRHO';
p_s = X_s*p_e; % just to test
mixCnst.Keq_struc.p_s = p_s;

p_e_mat = repmat(p_vec,[1,N_spec]);

R0 = mixCnst.R0;
R_s = mixCnst.R_s;
Mm_s = mixCnst.Mm_s;
I_s = mixCnst.I_s;
L_s = mixCnst.L_s;
sigma_s = mixCnst.sigma_s;
thetav_s = mixCnst.thetav_s;
theta_s = mixCnst.theta_s;
gDegen_s = mixCnst.gDegen_s;
kBoltz = mixCnst.kBoltz;
hPlanck = mixCnst.Keq_struc.hPlanck;
nAvogadro = mixCnst.nAvogadro;
pAtm = mixCnst.Keq_struc.pAtm;
pAtm_vec = repmat(pAtm,[N,1]);

% computing equilibrium constants
T_r = getReaction_T(ones(size(mixCnst.Keq_struc.nT_r)),T_vec);
lnKeq_r = getReaction_lnKeq(T_r,T_vec,T_vec,T_vec,T_vec,T_vec,pAtm_vec,mixCnst.Keq_struc,opts);

%% loading mutation equilibrium tables
% data = load('mutation_table_78N22O_1481T_200_15000_396p_500_40000_withYs.dat');
data = load('mutationTables_T200-10-15000_P400-100-20000_air5.dat');
%%
T_tab = unique(data(:,1));
% T_tab = T_tab(1:981);
y_seq = data(:,11:15);
y_seq = y_seq(data(:,2)==p_e,:);
% y_seq = y_seq(1:981,:);

[Mmeq,~] = getMixture_Mm_R(y_seq,T_tab,T_tab,mixCnst.Mm_s,mixCnst.R0,mixCnst.bElectron);
X_seq = getSpecies_X(y_seq,Mmeq,mixCnst.Mm_s);
p_seq = X_seq*p_e;

KNtab = p_seq(:,1).^2 ./ p_seq(:,4); % pressure of N squared divided by pressure of N2
KOtab = p_seq(:,2).^2 ./ p_seq(:,5); % pressure of O squared divided by pressure of O2
KNOtab = p_seq(:,1).*p_seq(:,2) ./ p_seq(:,3); % pressure of N and O divided by pressure of NO

X = 0.21/0.79; % ratio of atomic fractions

lnKN = lnKeq_r(:,1);
lnKO = lnKeq_r(:,6);
lnKNO = lnKeq_r(:,11);

lnKpN = lnKN + log(R0*T_vec);
lnKpO = lnKO + log(R0*T_vec);
lnKpNO = lnKNO + log(R0*T_vec);

ln1e300 = log(1e300); % limit at which we will cut off the K_p

lnKpN(lnKpN<-ln1e300) = -ln1e300;
lnKpO(lnKpO<-ln1e300) = -ln1e300;
lnKpNO(lnKpNO<-ln1e300) = -ln1e300;

KN = R0*T_vec.*exp(lnKN);
KO = R0*T_vec.*exp(lnKO);
KNO = R0*T_vec.*exp(lnKNO);

figure
plot(T_vec,lnKpN,'k-',T_vec,lnKpO,'k-',T_vec,lnKpNO,'k-',...
    T_tab,log(KNtab),'r--',T_tab,log(KOtab),'r--',T_tab,log(KNOtab),'r--');
legend('KN','KO','KNO',...
    'KN mut','KO mut','KNO mut')
%ylim([1e-10,1e20]);

%% Analytical solution
%{
a = (KO.*X.^2./(4.*KN.*KNO.^2)-X.^2./KN.^2+KO.*X./(2.*KN.*KNO.^2)-2.*X./KN.^2+KO./(4.*KN.*KNO.^2)-1./KN.^2);
b = (KO.*X.^2./(2.*KN.*KNO)-X.^2./KN+KO.*X./(KN.*KNO)+KO.*X./(4.*KNO.^2)-3.*X./KN+KO./(2.*KN.*KNO)+KO./(4.*KNO.^2)-2./KN);
c = (KO.*X.^2./(4.*KNO)+p_e.*KO.*X.^2./(4.*KNO.^2)-X.^2./4+KO.*X./(2.*KNO)+KO.*X./(2.*KN)+2.*p_e.*X./KN-X+KO./(2.*KNO)-p_e.*KO./(4.*KNO.^2)+KO./(4.*KN)+2.*p_e./KN-1);
d = (KO.*X./4+p_e.*X-p_e.*KO./(2.*KNO)+KO./4+2.*p_e);
e = -p_e.*KO./4-p_e.^2;

[x1,x2,x3,x4] = roots4thOrderPoly(a,b,c,d,e);

pN      = x1;
f1      = KO/4.*(1+(1-X)./KNO.*pN);
f2      = f1.^2 + KO.*X/2 .* (2./KN.*pN.^2 + pN);
pO      = -f1+sqrt(f2);
pO2     = pO.^2./KO;
pN2     = pN.^2./KN;
pNO     = pO.*pN./KNO;
%}

%% Analytical solution using logarithms
%{
% a = multiOrderSum(exp(1).^(lnKO-2.*lnKNO-lnKN).*X.^2./4 , - exp(1).^-(2.*lnKN).*X.^2 , + exp(1).^(lnKO-2.*lnKNO-lnKN).*X./2 , - 2.*exp(1).^-(2.*lnKN).*X , + exp(1).^(lnKO-2.*lnKNO-lnKN)./4 , - exp(1).^-(2.*lnKN));
% b = multiOrderSum(exp(1).^(lnKO-lnKNO-lnKN).*X.^2./2 , - exp(1).^-lnKN.*X.^2 , + exp(1).^(lnKO-lnKNO-lnKN).*X , + exp(1).^(lnKO-2.*lnKNO).*X./4 , - 3.*exp(1).^-lnKN.*X , + exp(1).^(lnKO-lnKNO-lnKN)./2 , + exp(1).^(lnKO-2.*lnKNO)./4 , - 2.*exp(1).^-lnKN);
% c = multiOrderSum(exp(1).^(lnKO-lnKNO).*X.^2./4 , + p_e.*exp(1).^(lnKO-2.*lnKNO).*X.^2./4 , - X.^2./4+exp(1).^(lnKO-lnKNO).*X./2 , + exp(1).^(lnKO-lnKN).*X./2 , + 2.*p_e.*exp(1).^-lnKN.*X , - X*ones(size(a)) , + exp(1).^(lnKO-lnKNO)./2 , - p_e.*exp(1).^(lnKO-2.*lnKNO)./4 , + exp(1).^(lnKO-lnKN)./4 , + 2.*p_e.*exp(1).^-lnKN , - ones(size(a)));
% d = multiOrderSum(exp(1).^lnKO.*X./4 , + p_e.*X.*ones(size(a)) , - p_e.*exp(1).^(lnKO-lnKNO)./2 , + exp(1).^lnKO./4 , + 2.*p_e.*ones(size(a)));
a = exp(1).^(lnKpO-2.*lnKpNO-lnKpN).*X.^2./4 - exp(1).^-(2.*lnKpN).*X.^2 + exp(1).^(lnKpO-2.*lnKpNO-lnKpN).*X./2 - 2.*exp(1).^-(2.*lnKpN).*X + exp(1).^(lnKpO-2.*lnKpNO-lnKpN)./4 - exp(1).^-(2.*lnKpN);
b = exp(1).^(lnKpO-lnKpNO-lnKpN).*X.^2./2 - exp(1).^-lnKpN.*X.^2 + exp(1).^(lnKpO-lnKpNO-lnKpN).*X + exp(1).^(lnKpO-2.*lnKpNO).*X./4 - 3.*exp(1).^-lnKpN.*X + exp(1).^(lnKpO-lnKpNO-lnKpN)./2 + exp(1).^(lnKpO-2.*lnKpNO)./4 - 2.*exp(1).^-lnKpN;
c = exp(1).^(lnKpO-lnKpNO).*X.^2./4 + p_e.*exp(1).^(lnKpO-2.*lnKpNO).*X.^2./4 - X.^2./4+exp(1).^(lnKpO-lnKpNO).*X./2 + exp(1).^(lnKpO-lnKpN).*X./2 + 2.*p_e.*exp(1).^-lnKpN.*X - X*ones(size(a)) + exp(1).^(lnKpO-lnKpNO)./2 - p_e.*exp(1).^(lnKpO-2.*lnKpNO)./4 + exp(1).^(lnKpO-lnKpN)./4 + 2.*p_e.*exp(1).^-lnKpN - ones(size(a));
d = exp(1).^lnKpO.*X./4 + p_e.*X.*ones(size(a)) - p_e.*exp(1).^(lnKpO-lnKpNO)./2 + exp(1).^lnKpO./4 + 2.*p_e.*ones(size(a));
e = -p_e.*exp(1).^lnKpO./4-p_e.^2;

% [x1,x2,x3,x4] = roots4thOrderPoly(a,b,c,d,e,'log','multiOrder');
x1 = roots4thOrderPoly(a,b,c,d,e,'log');
% figure;
% plot(T_vec,x1,T_vec,x2,T_vec,x3,T_vec,x4);
% legend('x1','x2','x3','x4');

lnpN    = log(x1);

f1      = 1/4*exp(lnKpO) + (1-X)/4*exp(lnKpO-lnKpNO+lnpN);
%f2      = multiOrderSum(f1.^2 , + X*exp(lnKO-lnKN+2*lnpN) , + X/2*exp(lnKO+lnpN));
f2      = f1.^2 + X*exp(lnKpO-lnKpN+2*lnpN) + X/2*exp(lnKpO+lnpN);
pO      = -f1+sqrt(f2);
lnpO    = log(pO);
lnpO2   = 2*lnpO - lnKpO;
lnpN2   = 2*lnpN - lnKpN;
lnpNO   = lnpN + lnpO - lnKpNO;
pO      = single(abs(pO));
pN      = single(abs(x1));
pO2     = abs(exp(single(lnpO2)));
pN2     = abs(exp(single(lnpN2)));
pNO     = abs(exp(single(lnpNO)));
%}
%% Analytical solution solving for pO
% a = (KN/(2*KNO^2*KO*X)-2/(KO^2*X)+KN/(4*KNO^2*KO*X^2)-1/(KO^2*X^2)+KN/(4*KNO^2*KO)-1/KO^2);
% b = (KN/(KNO*KO*X)-3/(KO*X)+KN/(4*KNO^2*X)+KN/(2*KNO*KO*X^2)-1/(KO*X^2)+KN/(2*KNO*KO)-2/KO+KN/(4*KNO^2));
% c = (KN/(2*KO*X)+2*p_e/(KO*X)+KN/(2*KNO*X)-1/X+KN/(4*KNO*X^2)+p_e*KN/(4*KNO^2*X^2)-1/(4*X^2)+KN/(4*KO)+2*p_e/KO+KN/(2*KNO)-p_e*KN/(4*KNO^2)-1);
% d = (KN/(4*X)+p_e/X-p_e*KN/(2*KNO)+KN/4+2*p_e);
% e = -p_e*KN/4-p_e^2;
a = -2.*exp(1).^-(2.*lnKpO)./X+exp(1).^(-lnKpO-2.*lnKpNO+lnKpN)./(2.*X)-exp(1).^-(2.*lnKpO)./X.^2+exp(1).^(-lnKpO-2.*lnKpNO+lnKpN)./(4.*X.^2)-exp(1).^-(2.*lnKpO)+exp(1).^(-lnKpO-2.*lnKpNO+lnKpN)./4;
b = -3.*exp(1).^-lnKpO./X+exp(1).^(-lnKpO-lnKpNO+lnKpN)./X+exp(1).^(lnKpN-2.*lnKpNO)./(4.*X)-exp(1).^-lnKpO./X.^2+exp(1).^(-lnKpO-lnKpNO+lnKpN)./(2.*X.^2)-2.*exp(1).^-lnKpO+exp(1).^(-lnKpO-lnKpNO+lnKpN)./2+exp(1).^(lnKpN-2.*lnKpNO)./4;
c = 2.*p_e.*exp(1).^-lnKpO./X+exp(1).^(lnKpN-lnKpO)./(2.*X)+exp(1).^(lnKpN-lnKpNO)./(2.*X)-1./X+exp(1).^(lnKpN-lnKpNO)./(4.*X.^2)+p_e.*exp(1).^(lnKpN-2.*lnKpNO)./(4.*X.^2)-1./(4.*X.^2)+2.*p_e.*exp(1).^-lnKpO+exp(1).^(lnKpN-lnKpO)./4+exp(1).^(lnKpN-lnKpNO)./2-p_e.*exp(1).^(lnKpN-2.*lnKpNO)./4-1;
d = exp(1).^lnKpN./(4.*X)+p_e./X-p_e.*exp(1).^(lnKpN-lnKpNO)./2+exp(1).^lnKpN./4+2.*p_e;
e = -p_e.*exp(1).^lnKpN./4-p_e.^2;

% [x1,x2,x3,x4] = roots4thOrderPoly(a,b,c,d,e,'log','multiOrder');
[x1,x2,x3,x4] = roots4thOrderPoly(a,b,c,d,e,'log');
figure;
plot(T_vec,x1,T_vec,x2,T_vec,x3,T_vec,x4);
legend('x1','x2','x3','x4');
%{%
lnpO = log(x3);
pO = x3;

f1 = 1/4*exp(lnKpN) - (1-X)/(4*X)*exp(lnKpN+lnpO-lnKpNO);
f1p2 = 1/16*exp(2*lnKpN) - (1-X)/(8*X)*exp(2*lnKpN+lnpO-lnKpNO) + ((1-X)/(4*X))^2*exp(2*(lnKpN+lnpO-lnKpNO));
f2 = f1p2 + 1/(2*X)*exp(lnKpN+lnpO) + 1/X*exp(lnKpN-lnKpO+2*lnpO);

pN = -f1+sqrt(f2);
lnpN = log(-f1+sqrt(f2));
lnpO2   = 2*lnpO - single(lnKpO);
lnpN2   = 2*lnpN - single(lnKpN);
lnpNO   = lnpN + lnpO - single(lnKpNO);
pO      = abs(pO);
pO2     = abs(exp(lnpO2));
pN2     = abs(exp(lnpN2));
pNO     = abs(exp(lnpNO));
%}

%% Newton-Raphson iterative solution
%{
% limK = 1e-16;
% KO(KO<limK) = limK;
% KN(KN<limK) = limK;
% KNO(KNO<limK) = limK;

% we first make an initial guess of the mass fractions:
pN = 1e-10*ones(size(T_vec)) * p_e;
pO = 1e-10*ones(size(T_vec)) * p_e;
pNO = 1e-10*ones(size(T_vec)) * p_e;
pN2 = (0.8-1e-10)*ones(size(T_vec)) * p_e;
pO2 = 0.2*ones(size(T_vec)) * p_e;

p_n = [pN,pO,pNO,pN2,pO2];
p_n = permute(p_n,[2,3,1]);

pN2_it = 0.79*p_e;
pO2_it = 0.21*p_e;
for ii=1:4000;%length(T_vec)
    lnKO_it = lnKO(ii);
    lnKN_it = lnKN(ii);
    lnKNO_it = lnKNO(ii);
    conv = 1;
    tol = eps;
    it = 0;
    it_max = 100;
    while conv>eps && it<it_max
        it = it+1;
        % we first compute what pN and pO should be for that pN2 and pO2
        lnpO_it = lnKO_it+log(pO2_it);
        lnpN_it = lnKN_it+log(pN2_it);
        % we then compute pNO accordingly
        lnpNO_it = lnpO_it + lnpN_it - lnKNO_it;
        % and impose the two global conditions by changing O2 and N2
        pN2_it = (2*p_e - exp(lnpO_it) - (1+X)*exp(lnpNO_it) - (2+X)*exp(lnpN_it))./(2*(X+1));
        pO2_it = p_e - pN2_it - exp(lnpO_it) - exp(lnpN_it) - exp(lnpNO_it);
        p_n = [exp(lnpN_it),exp(lnpO_it),exp(lnpNO_it),pN2_it,pO2_it];
        if it>1
            conv = norm(p_n-pn_prev,inf);
        end
        pn_prev = p_n;
        %disp(['It. ',num2str(it),' err ',num2str(conv)]);
    end
    pN(ii) = exp(lnpN_it);
    pO(ii) = exp(lnpO_it);
    pNO(ii) = exp(lnpNO_it);
    pO2(ii) = pO2_it;
    pN2(ii) = pN2_it;
    disp(['For T = ',num2str(T_vec(ii)),' K, X_O is ',num2str(pO(ii)/p_e),' after ',num2str(it),' iterations']);
end
%}

%% Newton-Raphson using logarithms

p_e_vec = logspace(1,6,18);
N_p = length(p_e_vec);
oN = log(0.2*p_e); oO = log(0.2*p_e); oNO = log(0.2*p_e); oN2 = log(0.2*p_e); oO2 = log(0.2*p_e);
o = [oN;oO;oNO;oN2;oO2];
ii_vec = N:-10:1;
N_ii = length(ii_vec);
o_out = zeros(N_ii,N_p,length(o));
for jj = 1:N_p
    iit = 0;
for ii=ii_vec
    iit = iit+1;
    tol = eps;
    conv = 1;
    it = 0;
    it_max = 200;
    while conv>tol && it<it_max
        it = it+1;
    J = [[0,            2,              0,              0,              -1];
         [2,            0,              0,              -1,              0];
         [1,            1,              -1,             0,               0];
         [-X*exp(o(1)),   exp(o(2)),    (1-X)*exp(o(3)), -2*X*exp(o(4)),  2*exp(o(5))];
         [exp(o(1)),      exp(o(2)),        exp(o(3)),       exp(o(4)),     exp(o(5))]];
     F = [2*o(2)-o(5)-lnKpO(ii) ; 2*o(1)-o(4)-lnKpN(ii) ; o(1)+o(2)-o(3)-lnKpNO(ii) ;
         -X*exp(o(1)) + exp(o(2)) + (1-X)*exp(o(3)) - 2*X*exp(o(4)) + 2*exp(o(5));
          exp(o(1)) + exp(o(2)) + exp(o(3)) + exp(o(4)) + exp(o(5)) - p_e_vec(jj)];
      do = -J\F;
      o = o+do;
      if it~=1
          conv = norm(o-o_prev,inf);
      end
      o_prev = o;
%       disp(['It ',num2str(it),' err ',num2str(conv)]);
    end
    o_out(iit,jj,:) = reshape(o,[1,1,length(o)]);
%     disp(['T = ',num2str(T_vec(ii)),' K with err. ',num2str(conv),' after ',num2str(it),' it.']);
    disp(['Completed ',num2str(((jj-1)*N + N-ii+1)/(N_p*N) * 100),'%']);
end
o = reshape(o_out(N_ii,jj,:),[length(o),1,1]);
end

pN = exp(o_out(:,:,1));
pO = exp(o_out(:,:,2));
pNO = exp(o_out(:,:,3));
pN2 = exp(o_out(:,:,4));
pO2 = exp(o_out(:,:,5));

%% Plotting
p_e_mat = repmat(p_e_vec,[N_ii,1]);
XN = abs(pN./p_e_mat);
XO = abs(pO./p_e_mat);
XN2 = abs(pN2./p_e_mat);
XO2 = abs(pO2./p_e_mat);
XNO = abs(pNO./p_e_mat);

clear plots legends
figure
for jj = 1:N_p
    plot(T_vec(ii_vec),XN(:,jj),'Color',color_rainbow(jj,N_p))
    hold on;
    plot(T_vec(ii_vec),XO(:,jj),'Color',color_rainbow(jj,N_p))
    plot(T_vec(ii_vec),XNO(:,jj),'Color',color_rainbow(jj,N_p))
    plot(T_vec(ii_vec),XN2(:,jj),'Color',color_rainbow(jj,N_p))
    plots(jj) = plot(T_vec(ii_vec),XO2(:,jj),'Color',color_rainbow(jj,N_p));
    legends{jj} = ['p = ',num2str(round(p_e_vec(jj))),' Pa'];
end
legend(plots,legends);
ylabel('X_s [-]');
xlabel('T [K]');

%{
figure
subplot(2,1,1)
plot(T_vec,2*lnpN,T_vec,lnKpN,T_vec,lnpN2);
xlim([0,10000]); ylim([-50,30]);
subplot(2,1,2)
plot(T_vec,XN,'k',T_vec,XO,'k',T_vec,XN2,'k',T_vec,XO2,'k',T_vec,XNO,'k',...
    T_tab,X_seq(:,1),'r--',T_tab,X_seq(:,2),'r--',T_tab,X_seq(:,3),'r--',T_tab,X_seq(:,4),'r--',T_tab,X_seq(:,5),'r--');
xlim([0,10000]);
%legend('x1','x2','x3','x4');
%}

%% Plotting separate
figure
subplot(5,1,1)
plot(T_vec(1:2000),XN2(1:2000));
subplot(5,1,2)
plot(T_vec(1:2000),XO2(1:2000));
subplot(5,1,3)
plot(T_vec(1:2000),XO(1:2000));
subplot(5,1,4)
plot(T_vec(1:2000),XN(1:2000));
subplot(5,1,5)
plot(T_vec(1:2000),XNO(1:2000));