% Test_enthalpyFormation is a script to test if working with the species
% enthalpy (including the enthalpy of formation) would also converge to the
% cp*T limit for low temperatures.

addpath(genpath('/home/miromiro/workspace/DEKAF/SourceCode/'));

% We will work in equilibrium conditions, so we first retrieve the
% equilibrium composition
clear
data = load('mutation_table_78N22O_1481T_200_15000_396p_500_40000_withYs.dat');
y_s = data(:,11:15);
p_e = 4000;
y_s = y_s(data(:,2)==p_e,:);
T_vec = data(:,1);
T_vec = T_vec(data(:,2)==p_e);
N = length(T_vec);
p_vec = p_e*ones(N,1);

h_tab1 = data(:,5);
h_tab1 = h_tab1(data(:,2)==p_e);

data2 = load('Mutation_LTE_T50_15000_P2000_15000.dat');
T_vec2 = data2(:,1);
T_vec2 = T_vec2(data2(:,2)==p_e);
h_tab2 = data2(:,5);
h_tab2 = h_tab2(data2(:,2)==p_e);
h_tab2_int = interp1(T_vec2,h_tab2,T_vec);

% getting constants and so on
mixture = 'air5mutation';
opts.elecStates = 'mutation';
opts.modelCollisionNeutral = 'GuptaYos90';
opts.collisionRefExceptions.references = [];
opts.collisionRefExceptions.pairs = [];
mixCnst = getAll_constants(mixture,opts);

rho = getMixture_rho(y_s,p_vec,T_vec,T_vec,mixCnst.R0,mixCnst.Mm_s,mixCnst.bElectron);
% CPG enthalpy
cp = 1004.5;
h_CPG = cp*T_vec;

%% CNE enthalpy
[N_spec,N_stat] = size(mixCnst.theta_s);
R_s = repmat(mixCnst.R_s',[N,1]);
nAtoms_s = repmat(mixCnst.nAtoms_s',[N,1]);
thetav_s = repmat(mixCnst.thetav_s',[N,1]);
hForm_s = repmat(mixCnst.hForm_s',[N,1]);
%hForm_s = zeros(size(y_s));
bElectron = repmat(mixCnst.bElectron',[N,1]);
theta_s = repmat(reshape(mixCnst.theta_s,[1,N_spec,N_stat]),[N,1,1]);
gDegen_s = repmat(reshape(mixCnst.gDegen_s,[1,N_spec,N_stat]),[N,1,1]);
T_mat = repmat(T_vec,[1,N_spec]);
[h_CNE,~,~,h_s] = getMixture_h_cp(y_s,T_mat,T_mat,T_mat,T_mat,T_mat,R_s,nAtoms_s,thetav_s,theta_s,gDegen_s,bElectron,hForm_s);
g_s = getSpecies_gibbs(T_vec,p_vec,mixCnst.R_s,mixCnst.Mm_s,mixCnst.I_s,mixCnst.L_s,mixCnst.sigma_s,mixCnst.thetav_s,mixCnst.theta_s,mixCnst.gDegen_s,mixCnst.kBoltz,mixCnst.Keq_struc.hPlanck,mixCnst.nAvogadro,mixCnst.hForm_s);

%% Enthalpies constants with mutation
state_var.T = T_vec;
for s=1:N_spec
state_var.rho_s{s} = y_s(:,s).*rho;
end
prop_list = {'h','h298','hForm_s','hs','gibbs_s'};
mutOut = eval_prop_mutation(state_var,'CNE',prop_list);
h_mut = mutOut.h;
h298_mut = mutOut.h298;

%% plotting
T2plot = {T_vec,T_vec,T_vec,T_vec,T_vec};
enthalpies2plot = {h_CNE,h_CPG,h_tab2_int,h298_mut,h_mut};
legends = {'CNE DEKAF','CPG','tables 2','mutation 298','mutation'};
N_ent = length(enthalpies2plot);

styleS = {'o','p','*','s','d','^','+'};
LineS = {'-','-.','--',':','-','-.','--',':','-','-.','--',':'};

% plotting
clear plotss
figure
subplot(3,4,1);
plot(T_vec,h_s(:,1),T_vec,mutOut.hs{1},'--');
ylabel('h_1');
subplot(3,4,2);
plot(T_vec,h_s(:,2),T_vec,mutOut.hs{2},'--');
ylabel('h_1');
subplot(3,4,3);
plot(T_vec,h_s(:,3),T_vec,mutOut.hs{3},'--');
ylabel('h_3');
subplot(3,4,4);
plot(T_vec,h_s(:,4),T_vec,mutOut.hs{4},'--');
ylabel('h_4');
subplot(3,4,5);
plot(T_vec,h_s(:,5),T_vec,mutOut.hs{5},'--');
ylabel('h_5');
subplot(3,4,6);
plot(T_vec,g_s(:,1),T_vec,mutOut.gibbs_s{1},'--');
ylabel('g_1');
subplot(3,4,7);
plot(T_vec,g_s(:,2),T_vec,mutOut.gibbs_s{2},'--');
ylabel('g_1');
subplot(3,4,8);
plot(T_vec,g_s(:,3),T_vec,mutOut.gibbs_s{3},'--');
ylabel('g_3');
subplot(3,4,9);
plot(T_vec,g_s(:,4),T_vec,mutOut.gibbs_s{4},'--');
ylabel('g_4');
subplot(3,4,10);
plot(T_vec,g_s(:,5),T_vec,mutOut.gibbs_s{5},'--');
ylabel('g_5');
subplot(3,4,11)
for ii=1:N_ent
plotss(ii) = plotLessMarkers_VESTA(T2plot{ii},enthalpies2plot{ii},10,styleS{ii},LineS{ii},'Color',0.8*color_rainbow(ii,N_ent),'MarkerSize',10);
hold on;
end
legend(plotss,legends);
subplot(3,4,12);
plot(T_vec,y_s);