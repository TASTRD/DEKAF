Installation guide for the DEKAF flow solver
============================================

This file is intended as a guide for the installation of DEKAF. Before using the code, it is important to carefully read the [readme file](README.md), and similarly, before developing code one must read the [developers manual](DevMANUAL.md).

Linux
-----

Steps 2 and 3 are obtained from [here](https://docs.gitlab.com/ce/ssh/README.html).

**1) Install git**

Very often it is already included in the OS's function suit. Just to check, run `git --help` on your terminal. If it does not find it (`Command 'git' not found`), then install it.

For ubuntu, for instance, run:
```
sudo apt-get install git
```

**2) Create an ssh key**

In a terminal go to `~/.ssh/` and run:
```
ssh-keygen -t rsa -b 4096 -C "email@example.com"
```
substituting `email@example.com` with the email with which you registered in gitlab. You will be asked to give a name for your ssh key (`DEKAFkey` for example), and to add a password for it if desired.

Two files will be generated with the private (`DEKAFkey` in the mentioned example) and the public key (`DEKAFkey.pub`).

**3) Adding an ssh key to your gitlab account**

Open the newly-generated public key (`DEKAFkey.pub` in the previous example), and copy the text contained in it. It should look something like this:
```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDDgw6meEaPjv58ZV/s3UrYxUQyDz/e1n31ySk1KYv8r67OEGQMQ1H3a81oAEyCdx0RmtR5W/opKqY7ETRHRf78RaGAv3GlUDEk0Xv9t3n1AnwTtxV8AHQQ3aOH3FK+e+ZW0+0u1Z7LmPJ1sn5qMbUAa6yg3bqDalhdiEo9CgPTm2LB0TsLo0Ft7gdvGwkaRDGl/Ac51Aj8kiY5q06zLsjmdp35lGjsCg8gBWnIITCHDdEAnCBST3QhcwfZ7yrzyv10gawWBxLAyhYZHTNyDQpx9seQpENt6XXOEBCCWAE+GNsBfdgd8I5lNU02GqJcLRpzHMRcBqGwHeXBgjse6dLeZ1+twOtxN6dHWs/slDHUoKAYi2GKIKvhD9Mz/fhN/UrKCIXp0dM0bztWgFZqz82I8O2Ew1v5vXFIzdwkIflDZ7SMVSVeVYgi7jgngc2ywgWttY5t2Cu2PR8dDsMc9V3MwaDIK2QTUDyiT/IgcB65gePz2/kY4Rp+srNn09deXdg7j/aZdHedlrI2RR6PJzCAslVHXgqwDyEq3GiPvCz4ClUymYah484JWqV6/fFjOqTshnKIeF06lKHEKg2fQsViAcJilGD06CHXSqDyFSU9Ir5PDCF3Zt9yoZb5s1EJ6UlNhCsBP7eGHMFhtjMLKfbjJFMFqpdDh86MX86SJSV+Nw== email@example.com
```
Then, in your gitlab account, go to **Settings** > **SSH Keys**, and paste the public key onto the **Key** field.

Give it a recognizable title (identifying the computer in question), and then **Add key**.

**4) Clone the repository**

In a terminal navigate to the folder where you want to have DEKAF installed and run:
```
git clone git@gitlab.com:TASTRD/DEKAF.git /folder/
```
substituting `/folder/` for the desired installation folder path.

**5) Define environmental variable**

In order to make the execution of examples easier, it is desirable to define an environmental variable directing to DEKAF's installation directory in your local machine. This can be done very easily in linux by editting the `~/.bashrc` file, and including an additional line with:
```
export DEKAF_DIRECTORY=/folder
```
substituting `/folder` for the **full** path to the DEKAF installation folder. For instance: `/home/miromiro/workspace/DEKAF`.

Having defined the `DEKAF_DIRECTORY` environmental variable, the full code will be found by matlab when executing scripts from any location, as long as the following command is run at the beginning of the matlab session:
```
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']));
```
The scripts in the `Examples` folder already start with this command.

Note that the `bashrc` file is automatically executed upon opening a new terminal, so any matlab sessions launched before modifying it will not find the environmental variable definition. The simplest is to just close the session and the terminal and open it again.

Windows 10
----------

Steps 2 and 3 are obtained from [here](https://help.cloudforge.com/hc/en-us/articles/215243143-Configure-TortoiseGIT-client-to-work-with-SSH-keys-on-Windows).

**1) Install git**

First, install [git for windows](https://gitforwindows.org/).

For windows users, it is often convenient to install a git manager. There exist [quite a few](https://git-scm.com/download/gui/windows), out of which we recommend [tortoise git](https://tortoisegit.org/). Follow all the steps in the installation wizard, and when prompted, make sure to introduce the same email you are registered with in gitlab.

**2) Create an ssh key**

Download and install [PUTTYgen](https://www.puttygen.com/download.php?val=4). Execute PUTTYgen, click on **Generate**.

After the generation has concluded, save both the public (named for example `DEKAFkey.pub`) and private key (named for example `DEKAFkey.ppk`) by clicking on **Save public key** and **Save private key**.

Also copy the public key - the text under "*Public key for pasting into OpenSSH authorized_keys file*", which should look something like this:
```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDDgw6meEaPjv58ZV/s3UrYxUQyDz/e1n31ySk1KYv8r67OEGQMQ1H3a81oAEyCdx0RmtR5W/opKqY7ETRHRf78RaGAv3GlUDEk0Xv9t3n1AnwTtxV8AHQQ3aOH3FK+e+ZW0+0u1Z7LmPJ1sn5qMbUAa6yg3bqDalhdiEo9CgPTm2LB0TsLo0Ft7gdvGwkaRDGl/Ac51Aj8kiY5q06zLsjmdp35lGjsCg8gBWnIITCHDdEAnCBST3QhcwfZ7yrzyv10gawWBxLAyhYZHTNyDQpx9seQpENt6XXOEBCCWAE+GNsBfdgd8I5lNU02GqJcLRpzHMRcBqGwHeXBgjse6dLeZ1+twOtxN6dHWs/slDHUoKAYi2GKIKvhD9Mz/fhN/UrKCIXp0dM0bztWgFZqz82I8O2Ew1v5vXFIzdwkIflDZ7SMVSVeVYgi7jgngc2ywgWttY5t2Cu2PR8dDsMc9V3MwaDIK2QTUDyiT/IgcB65gePz2/kY4Rp+srNn09deXdg7j/aZdHedlrI2RR6PJzCAslVHXgqwDyEq3GiPvCz4ClUymYah484JWqV6/fFjOqTshnKIeF06lKHEKg2fQsViAcJilGD06CHXSqDyFSU9Ir5PDCF3Zt9yoZb5s1EJ6UlNhCsBP7eGHMFhtjMLKfbjJFMFqpdDh86MX86SJSV+Nw== email@example.com
```

**3) Adding an ssh key to your gitlab account**

In your gitlab account, go to **Settings** > **SSH Keys**, and paste the public key (what you just copied from PUTTYgen) onto the **Key** field.

Give it a recognizable title (identifying the computer in question), and then **Add key**.

**4) Clone the repository**

Open an explorer window, and go to the folder where you want DEKAF to be installed. Right click > **git clone**, in the **URL** field introduce the path to the remote repository:
```
git@gitlab.com:TASTRD/DEKAF.git
```
in the **Load Putty Key** field find the recently created putty key (named `DEKAFkey.ppk` in the example) and execute.

It will prompt you asking if you want to add the host server key to your registry, say yes.

**5) Define environmental variable**

Click the lower-left windows icon and search "environment". It should find something called **Edit the system environment variables**. Open it and go to **New**. A dialogue will prompt asking for the variable name (`DEKAF_DIRECTORY`) and the value, which must be the **full** path to the DEKAF installation folder (for instance `C:\workspace\DEKAF`).

Having defined the `DEKAF_DIRECTORY` environmental variable, the full code will be found by matlab when executing scripts from any location, as long as the following command is run at the beginning of the matlab session:
```
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']));
```
The scripts in the `Examples` folder already start with this command.
