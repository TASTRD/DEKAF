% This script reads in a basic state (exported from Tecplot) and extracts
% the coefficient of pressure, Cp, as a function of x/c, as well as the
% x/c, y/c geometry coordinates.
%
% The rows of the data file are ordered from the lower surface TE to the
% upper surface TE upon completion, in accordance with DEKAF's standard
% (and XFOIL's standard, for what it's worth).
%
% Reference:
%  Altitude data: https://www.digitaldutch.com/atmoscalc/
%
% Author(s): Ethan Beyak
% GNU Lesser General Public License 3.0
restoredefaultpath;
addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']));

%%
clear;
clc;
close all;

% Simulation variables
lLambda_w_uv = 30:-5:0; % Sweep angle, arctan(W_inf,sqrt(U_inf^2 + V_inf^2)) (deg)
Re_UV_cU = 13.1e6;      % Reynolds number, ref. velocity: sqrt(U_inf^2 + V_inf^2),
                        % ref. length: chord
M_inf_UV = 0.65;        % Mach number, ref. velocity: sqrt(U_inf^2 + V_inf^2)
alpha_v_u = -1.6434;    % Angle of attack, arctan(V_inf,U_inf) (deg)

% Plotting variables
Nlam = length(lLambda_w_uv);
hCp = gobjects(Nlam,1);
axfs = 15;
fs = 1.2*axfs;
fnum = 1;
lopts = {'interpreter','latex','edgecolor','none'};
axopts = {'interpreter','latex','fontsize',fs};
axgcaopts = {'fontsize',axfs,'ticklabelinterpreter','latex'};
popts = {'markersize',5};
if ishandle(fnum)
    close(fnum)
end
figure(fnum)

for iL = 1:Nlam
    airfoil.alpha_v_u = alpha_v_u;
    airfoil.Lambda_w_uv = lLambda_w_uv(iL);
    Lambdastr = num2str(airfoil.Lambda_w_uv);
    datapath = '/home/ebeyak/scratch/projects/uli_ee/data/X207o_v5/basic_states/';
    datafile = [datapath, 'q.cl065.',Lambdastr,'deg.wall.dat'];

    airfoil.Cp_filename = [datapath, 'X207o.Cp.cl065.',Lambdastr,'deg.dat'];
    airfoil.geom_filename = [datapath, 'X207o.geom.cl065.',Lambdastr,'deg.dat'];
    airfoil.Re_UV_cU = Re_UV_cU;
    airfoil.M_inf_UV = M_inf_UV;

    airfoil.p_inf = 15473.8; % Static pressure at 44k ft (Pa)
    airfoil.T_inf = 216.650; % Temperature at 44k ft (K)

    % Save data?
    airfoil.save_dat = false;%true;%

    % Thermodynamics
    airfoil.gam = 1.4; % Ratio of specific heats, cp/cv
    airfoil.R = 287.058; % Specific gas constant (J/kg-K)

    % Sutherland's empirical constants for air
    airfoil.mu_ref = 1.716e-5; % kg/m-s
    airfoil.T_ref = 273.15; % K
    airfoil.Su_mu = 110.6;

    % Column indices from Tecplot for DIMENSIONAL PRIMITIVE variables
    % (corresponding to datafile)
    data_idx.x = 9;    % Global horizontal coord.
    data_idx.y = 10;   % Global vertical coord.
    data_idx.z = 11;   % Global into-the-page coord.
    data_idx.i = 1;    % Structured index of x
    data_idx.j = 7;    % Structured index of y
    data_idx.k = 8;    % Structured index of z
    data_idx.u = 2;    % Velocity comp. in x
    data_idx.v = 3;    % Velocity comp. in y
    data_idx.w = 4;    % Velocity comp. in z
    data_idx.T = 5;    % Temperature
    data_idx.rho = 6;  % Mass density of mixture
    airfoil.data_idx = data_idx;

    % Get the data for x/c & Cp as well as x/c & y/c
    [Cp_UV_data, geom_data_out] = get_Cp_geom_data(airfoil, datafile);

    % Plot Cp
    subplot(2,1,1);
    hCp(iL) = plot(Cp_UV_data(:,1), Cp_UV_data(:,2), '.-', popts{:});
    hold on
end
legstr = strcat(cellstr(num2str(lLambda_w_uv.')),'^\circ');
legstr{1} = [legstr{1}, ' = \Lambda'];
legstr = strcat('$',legstr,'$');
legend(hCp,legstr,lopts{:})
set(gca,axgcaopts{:},'ydir','reverse');
grid on
xlim([0 1]);
ylabel('$C_p$',axopts{:});

% Plot airfoil coordinates
subplot(2,1,2);
hold on
plot(geom_data_out(:,1), geom_data_out(:,2), '.k-', popts{:});
xlabel('$x/c$',axopts{:});
ylabel('$y/c$',axopts{:});
xlim([0 1]);
axis equal
grid on
set(gca,axgcaopts{:});
