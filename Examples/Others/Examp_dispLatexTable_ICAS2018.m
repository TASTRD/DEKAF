% Examp_dispLatexTable_ICAS2018 is a script to print out to screen all the
% latex tables needed for the ICAS paper.
%
% It uses all the dispLatexTable_* functions in Utilities/LatexTables/
%
% Author: Fernando Miro Miro
% Date: June 2018

%%%%% INPUTS
mixture11 = 'air11Park91';
mixture5 = 'air5mutation';
opts.modelEnergyModes = 'mutation';
%%%%% END OF INPUTS

%% Obtaining mixture data
addpath(genpath([getenv('DEKAF_DIRECTORY'),'_stable']));
mixCnst11 = getAll_constants(mixture11,opts);
mixCnst5 = getAll_constants(mixture5,opts);
dwnspc = sprintf('\n'); % defining downspace

%% Species data
fID = fopen('speciesTables_ICAS.tex','w');
% main species properties
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_specProp(mixCnst11,'write2file',fID);
fwrite(fID,[dwnspc,'\caption{Species properties taken from the mutation$^\text{++}$ database~\cite{MaginThesis,ScogginsThesis}, gathered and adapted from Ref.~\cite{Gurvich1989}.}']);
fwrite(fID,[dwnspc,'\label{tb_specProp}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% electronic energy levels of atomic species
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\begin{subtable}{1\textwidth}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_electronStates(mixCnst11,{'N','O','N+','O+'},'write2file',fID,'levels',0:8);
fwrite(fID,[dwnspc,'\end{subtable}']);
fwrite(fID,[dwnspc,'\caption{Atomic species'' electronic levels taken from the mutation$^\text{++}$ database~\cite{MaginThesis,ScogginsThesis}, gathered and adapted from Ref.~\cite{Gurvich1989}.}']);
fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% electronic energy levels of atomic species (continuation)
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\ContinuedFloat']); fwrite(fID,[dwnspc,'\begin{subtable}{1\textwidth}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_electronStates(mixCnst11,{'N','O'},'write2file',fID,'levels',9:45);
fwrite(fID,[dwnspc,'\end{subtable}']);
fwrite(fID,[dwnspc,'\caption{(cont.) Atomic species'' electronic levels taken from the mutation$^\text{++}$ database~\cite{MaginThesis,ScogginsThesis}, gathered and adapted from Ref.~\cite{Gurvich1989}.}']);
fwrite(fID,[dwnspc,'\label{tb_AtomSpecElecLev}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% electronic energy levels of molecular neutral species
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_electronStates(mixCnst11,{'N2','O2','NO'},'write2file',fID);
fwrite(fID,[dwnspc,'\caption{Molecular neutral species'' electronic levels taken from the mutation$^\text{++}$ database~\cite{MaginThesis,ScogginsThesis}, gathered and adapted from Ref.~\cite{Gurvich1989}.}']);
fwrite(fID,[dwnspc,'\label{tb_MolNeutSpecElecLev}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% electronic energy levels of molecular charged species
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_electronStates(mixCnst11,{'N2+','O2+','NO+'},'write2file',fID);
fwrite(fID,[dwnspc,'\caption{Molecular neutral species'' electronic levels taken from the mutation$^\text{++}$ database~\cite{MaginThesis,ScogginsThesis}, gathered and adapted from Ref.~\cite{Gurvich1989}.}']);
fwrite(fID,[dwnspc,'\label{tb_MolCharSpecElecLev}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);

fclose(fID);

%% reaction data
fID = fopen('reactionTables_ICAS.tex','w');
% air5 reactions
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_reactions(mixCnst5,'write2file',fID);
fwrite(fID,[dwnspc,'\caption{Air-5 reaction rate constants. Dissociation reactions taken from Park~\cite{Park1990} and exchange from Bose \& Candler~\cite{Bose1997}.}']);
fwrite(fID,[dwnspc,'\label{tb_reacPropAir5}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% air11 reactions
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\begin{subtable}{1\textwidth}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_reactions(mixCnst11,'write2file',fID,'reactions',1:3);
fwrite(fID,[dwnspc,'\end{subtable}']);
fwrite(fID,[dwnspc,'\caption{Air-11 reaction rate constants taken from Park~\cite{Park1993}.}']);
fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% air11 reactions (continuation)
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\ContinuedFloat']); fwrite(fID,[dwnspc,'\begin{subtable}{1\textwidth}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_reactions(mixCnst11,'write2file',fID,'reactions',4:21);
fwrite(fID,[dwnspc,'\end{subtable}']);
fwrite(fID,[dwnspc,'\caption{(cont.) Air-11 reaction rate constants taken from Park~\cite{Park1993}.}']);
fwrite(fID,[dwnspc,'\label{tb_reacPropAir11}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);

fclose(fID);

%% collision data
fID = fopen('collisionTables_ICAS.tex','w');
% Omega_11 neutral-neutral
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Omega11',{'N','O','NO','N2','O2'},{'N','O','NO','N2','O2'},'order',3,'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $\Omega^{(1,1)}_{s \ell}(T)$ of neutral-neutral collisions. Original data from Ref.~\cite{Wright2005}.}']);
fwrite(fID,[dwnspc,'\label{tb_colOmega11NeutNeut}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% Omega_11 charged-charged
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Omega11',{'N+','e-'},{'N+'},'order',5,'customNames',{'Rep.','Att.'},'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $(T^{*})^2 \, \Omega^{(1,1)}_{s \ell}(T^{*})$ of charged-charged collisions (attractive and repulsive). Original data from Ref.~\cite{Mason1967}}.']);
fwrite(fID,[dwnspc,'\label{tb_colOmega11CharChar}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% Omega_11 electron-neutral
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Omega11',{'N','O','NO','N2','O2'},{'e-'},'order',5,'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $\Omega^{(1,1)}_{s \ell}(T)$ of electron-neutral collisions. Original data from Ref.~\cite{Wright2005}.}']);
fwrite(fID,[dwnspc,'\label{tb_colOmega11elNeut}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% Omega_11 ion-neutral
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Omega11',{'N','O','NO','N2','O2'},{'N+','O+','NO+','N2+','O2+'},'order',3,'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $\Omega^{(1,1)}_{s \ell}(T)$ of ion-neutral collisions. Original data from Ref.~\cite{Levin2004}.}']);
fwrite(fID,[dwnspc,'\label{tb_colOmega11IonNeut}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);

% Omega_22 neutral-neutral
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Omega22',{'N','O','NO','N2','O2'},{'N','O','NO','N2','O2'},'order',3,'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $\Omega^{(2,2)}_{s \ell}(T)$ of neutral-neutral collisions. Original data from Ref.~\cite{Wright2005}.}']);
fwrite(fID,[dwnspc,'\label{tb_colOmega22NeutNeut}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% Omega_22 charged-charged
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Omega22',{'N+','e-'},{'N+'},'order',5,'customNames',{'Rep.','Att.'},'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $(T^{*})^2 \, \Omega^{(2,2)}_{s \ell}(T^{*})$ of charged-charged collisions (attractive and repulsive). Original data from Ref.~\cite{Mason1967}}.']);
fwrite(fID,[dwnspc,'\label{tb_colOmega22CharChar}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% Omega_22 electron-neutral
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Omega22',{'N','O','NO','N2','O2'},{'e-'},'order',5,'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $\Omega^{(2,2)}_{s \ell}(T)$ of electron-neutral collisions. Original data from Ref.~\cite{Wright2005}.}']);
fwrite(fID,[dwnspc,'\label{tb_colOmega22elNeut}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% Omega_22 ion-neutral
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Omega22',{'N','O','NO','N2','O2'},{'N+','O+','NO+','N2+','O2+'},'order',3,'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $\Omega^{(2,2)}_{s \ell}(T)$ of ion-neutral collisions. Original data from Ref.~\cite{Levin2004}.}']);
fwrite(fID,[dwnspc,'\label{tb_colOmega22IonNeut}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);

% Bstar neutral-neutral and ion-neutral
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Bstar',{'N'},{'N','N+'},'order',0,'customNames',{'Neutral-neutral','Ion-neutral'},'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $B^{*}_{s \ell}(T)$ of neutral-neutral and ion-neutral collisions. Original data from Refs.~\cite{Wright2005,Levin2004}.}']);
fwrite(fID,[dwnspc,'\label{tb_colBstarNeutNeut}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% Bstar charged-charged
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Bstar',{'N+','e-'},{'N+'},'order',5,'customNames',{'Rep.','Att.'},'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $B^{*}_{s \ell}(T^{*})$ of charged-charged collisions (attractive and repulsive). Original data from Ref.~\cite{Mason1967}}.']);
fwrite(fID,[dwnspc,'\label{tb_colBstarCharChar}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% Bstar electron-neutral
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Bstar',{'N','O','NO','N2','O2'},{'e-'},'order',5,'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $B^{*}_{s \ell}(T)$ of electron-neutral collisions. Original data from Ref.~\cite{Wright2005}.}']);
fwrite(fID,[dwnspc,'\label{tb_colBstarelNeut}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);


% Cstar neutral-neutral and ion-neutral
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Cstar',{'N'},{'N','N+'},'order',0,'customNames',{'Neutral-neutral','Ion-neutral'},'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $C^{*}_{s \ell}(T)$ of neutral-neutral and ion-neutral collisions. Original data from Refs.~\cite{Wright2005,Levin2004}.}']);
fwrite(fID,[dwnspc,'\label{tb_colCstarNeutNeut}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% Cstar charged-charged
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Cstar',{'N+','e-'},{'N+'},'order',5,'customNames',{'Rep.','Att.'},'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $C^{*}_{s \ell}(T^{*})$ of charged-charged collisions (attractive and repulsive). Original data from Ref.~\cite{Mason1967}}.']);
fwrite(fID,[dwnspc,'\label{tb_colCstarCharChar}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);
% Cstar electron-neutral
fwrite(fID,[dwnspc,'\begin{table*}']); fwrite(fID,[dwnspc,'\centering']);
dispLatexTable_collision(mixCnst11,'Cstar',{'N','O','NO','N2','O2'},{'e-'},'order',5,'write2file',fID); %
fwrite(fID,[dwnspc,'\caption{Polynomial-logarithmic curve-fit coefficients for $C^{*}_{s \ell}(T)$ of electron-neutral collisions. Original data from Ref.~\cite{Wright2005}.}']);
fwrite(fID,[dwnspc,'\label{tb_colCstarelNeut}']); fwrite(fID,[dwnspc,'\end{table*}']); fwrite(fID,[dwnspc,' ']);

fclose(fID);
