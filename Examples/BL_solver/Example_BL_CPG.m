% Example_BL_CPG is an example of how to run a typical marching
% boundary-layer case in perfect-gas conditions.
%
% The fixed edge profiles are the temperature and pressure profiles and the
% initial Mach number, but numerous other combinations are possible. Type
% "help DEKAF" to see the supported inputs. For a full breakdown of the
% options available, together with their default values, type "help
% setDefaults"
%
% Author: Fernando Miro Miro
% May 2019


clear;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions (edge profiles)
x_e     = linspace(0,2,1001);                   % [m]
p_e     = 101325 - 5000*sin(pi/2*x_e/x_e(end)); % [Pa]
M_e0    = 0.05;                                 % [-]
T_e     = 300*ones(size(x_e));                  % [K]
Tw      = 350;                                  % [K]
Lambda_sweep = 45;                              % [deg]

Nx = 100; % number of streamwise points

% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size (in non-dimensional eta length)
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 5e-13;                % Convergence tolerance
options.it_max                  = 40;                   % maximum number of iterations

% flow options
options.thermal_BC              = 'Twall';              % Wall bc (isothermal)
options.G_bc                    = Tw;                   % value of the wall temperature
% options.flow                    = 'CPG';                % flow assumption
% options.mixture                 = 'air2';               % mixture
% options.modelTransport          = 'Sutherland';         % transport model
options.flow                    = 'LTE';                % flow assumption
options.mixture                 = 'air5mutation';       % mixture
options.modelTransport          = 'CE_122';             % transport model
options.Cooke                   = true;                 % we want to solve the Cooke equation too (quasi-3D)
options.rotateVelocityVector    = 'streamline';         % we want to rotate the velocity vector to be aligned with the inviscid streamline

% display options
options.plotRes = false;                         % we don't want the residual plot

% flow conditions
intel.M_e0              = M_e0;                 % Mach number at the first position of the boundary-layer edge  [-]
intel.T_e               = T_e;                  % Temperature profile at boundary-layer edge                    [K]
intel.p_e               = p_e;                  % Pressure profile at boundary-layer edge                       [Pa]
intel.x_e               = x_e;                  % Streamwise coordinate paired with T_e and p_e                 [m]
intel.Lambda_sweep      = Lambda_sweep;         % Sweep angle                                                   [deg]

% marching mapping options
options.xSpacing            = 'tanh';       % mapping in the marching direction
options.mapOpts.N_x         = Nx;           % number of streamwise points
options.mapOpts.x_start     = 1e-8;         % intial x location [m]
options.mapOpts.x_end       = 2;            % final x location [m]

% Executing DEKAF
[intel,options] = DEKAF(intel,options,'marching');

% Saving
saveLight('DKF_CPG_march_M0p05_CF.mat',intel,options);

%% plotting
figure;
for ix=1:Nx
subplot(1,2,1); plot(intel.u(:,ix),intel.eta,'Color',two_colors(ix,Nx,'Rbw2'),'displayname',['x=',num2str(intel.x(1,ix)),'m']); hold on;
                xlabel('u [m/s]'); ylabel('\eta [-]'); grid minor; ylim([0,options.eta_i]);
subplot(1,2,2); plot(intel.w(:,ix),intel.eta,'Color',two_colors(ix,Nx,'Rbw2'),'displayname',['x=',num2str(intel.x(1,ix)),'m']); hold on;
                xlabel('w [m/s]'); ylabel('\eta [-]'); grid minor; ylim([0,options.eta_i]);
end
legend();
