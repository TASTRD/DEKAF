% Example_BL_CNE is an example of how to run a typical boundary layer in
% chemical non-equilibrium around a wedge. The oblique shock jump is
% solved, followed by an iniviscid 1-D simulation of the conditions at the
% boundary-layer edge.
%
% The fixed preshock conditions are temperature, prerssure and Mach number,
% but numerous other combinations are possible. Type "help DEKAF" to see
% the supported inputs. For a full breakdown of the options available,
% together with their default values, type "help setDefaults"
%
% Author: Fernando Miro Miro
% May 2019


clear;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
M_e = 10;
T_e = 300;          % [K]
p_e = 4000;         % [Pa]
wedge_angle = 5;    % [deg]

% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size (in non-dimensional eta length)
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 5e-13;                % Convergence tolerance
options.it_max                  = 40;                   % maximum number of iterations
options.T_tol                   = 5e-13;                % convergence tolerance for the NR to get T from the enthalpy
options.T_itMax                 = 200;                  % maximum number of iterations for the NR to get T from the enthalpy

% flow options
options.thermal_BC              = 'adiab';              % Wall bc (adiabatic)
options.flow                    = 'CNE';                % flow assumption
options.mixture                 = 'air5mutation';       % mixture
options.modelTransport          = 'CE_123';             % transport model

% shock options
options.shockJump       = true;                 % we do want to use shock relations
intel.wedge_angle       = wedge_angle;          % wedge angle in degrees

% display options
options.plotRes = false;                        % we don't want the residual plot

% flow conditions
intel.M_e0               = M_e;                  % Mach number at boundary-layer edge       [-]
intel.T_e0               = T_e;                  % Temperature at boundary-layer edge       [K]
intel.p_e0               = p_e;                  % Static pressure at boundary-layer edge   [Pa]

% inviscid solver
options.inviscidFlowfieldTreatment = '1DNonEquilibriumEuler';
options.inviscidMap.N_x      = 5000;

% marching mapping options
options.xSpacing            = 'tanh';       % mapping in the marching direction
options.mapOpts.N_x         = 50;          % number of streamwise points
options.mapOpts.x_start     = 1e-8;         % intial x location [m]
options.mapOpts.x_end       = 1;            % final x location [m]

% Executing DEKAF
[intel,options] = DEKAF(intel,options,'marching');

% Saving
saveLight('DKF_CNE_M10.mat',intel,options);