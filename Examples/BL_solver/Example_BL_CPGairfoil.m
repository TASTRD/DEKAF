% Example_BL_CPGairfoil is an example of how to run a typical marching
% boundary-layer case in perfect-gas conditions, with the edge conditions
% being provided by the inviscid computation around an airfoil.
%
% Type "help DEKAF" to see the supported inputs, as well as
% "help convert_airfoil". For a full breakdown of the options available,
% together with their default values, type "help setDefaults".
%
% Author(s): Fernando Miro Miro, Ethan Beyak
% Date: March 2020
clear;
close all;
clc;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'_stable/SourceCode']));

% Pick the angle-of-attack example case of your choice
% where AoA is defined as arctan(V_inf/U_inf)
%alpha_v_u = 0; % 0-deg AoA
alpha_v_u = 3; % 3-deg AoA
switch alpha_v_u
    case 0; filenameCp = 'ExternalFiles/naca0012_Cp_@0.txt';
    case 3; filenameCp = 'ExternalFiles/naca0012_Cp_@3.txt';
end

lLambda = [0 45]; % Sweep in degrees, arctan(W_inf/sqrt(U_inf^2 + V_inf^2))
lsides = {'bottom'};%{'top','bottom'}; % which side of the airfoil we want to cover
NL = length(lLambda);
Nsides = length(lsides);

for iL=1:NL
    for iside=1:Nsides
        %
        % flow variables
        chord   = 1;                % chord (ref. length of coordinates)    [m]
        T_inf   = 298;              % freestream temperature                [K]
        p_inf   = 101325;           % freestream pressure                   [Pa]
        % Note we specify the resultant Mach number, *not* based off of
        % sqrt(U_inf^2 + V_inf^2), but rather sqrt(U_inf^2 + V_inf^2 + W_inf^2).
        M_inf_Q = 0.5;              % freestream Mach based on resultant Q  [-]
        Pr      = 0.72;             % constant Prandtl number               [-]
        R       = 287.058;          % gas constant                          [J/kg-K]
        gam     = 1.4;              % constant gamma                        [-]
        cp      = gam*R/(gam-1);    % heat capacity at constant pressure    [J/kg-K]
        %
        % import airfoil data
        start_row   = 4;
        formatSpec  = '%10f%9f%f%[^\n\r]';
        fileID      = fopen(filenameCp,'r');
        dataArray   = textscan(fileID, formatSpec, ...
            'Delimiter', '', 'WhiteSpace', '', 'TextType', 'string', ...
            'HeaderLines' ,start_row-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');
        fclose(fileID);
        % end import
        %
        % Populating airfoil structure
        airfoil.xc_data         = dataArray{1};
        airfoil.Cp_UV_data      = dataArray{3};
        airfoil.xc_geom_data    = dataArray{1};
        airfoil.yc_geom_data    = dataArray{2};
        airfoil.chord_U         = chord;            % [m]
        airfoil.M_inf_UV_data   = 0;                % And the Cp data was obtained by an incompressible code
        %
        % The coordinates are stored in a counter-clockwise manner from the top's trailing edge.
        options.airfoil.data_orientation    = 'ccw_from_top_TE';
        % For accurate derivatives near the attachment line, we typically
        % need to overresolve our data set by assigning s_hyperinterp_fac > 1.
        options.airfoil.s_hyperinterp_fac   = 3;
        options.airfoil.debug               = 0;                % interactive debugging: off = 0, on = 1
        options.airfoil.side                = lsides{iside};
        %
        intel.M_inf_Q     = M_inf_Q;          % -
        intel.Lambda_w_uv = lLambda(iL);      % [deg]
        intel.alpha_v_u   = alpha_v_u;        % [deg]
        intel.T_inf       = T_inf;            % [K]
        intel.p_inf       = p_inf;            % [Pa]
        %
        % Plotting for convert_airfoil
        options.airfoil.plot_Cp     = 1;
        options.airfoil.plot_R_mrch = 1;
        options.airfoil.plot_beta   = 1;
        options.airfoil.plot_xi     = 1;
        %
        % Let intel and options know we're running an airfoil case.
        intel.airfoil                       = airfoil;
        options.inviscidFlowfieldTreatment  = 'airfoil';
        %
        % Marching mapping options
        options.xiSpacing       = 'tanh';   % mapping in the marching direction
        options.mapOpts.N_x     = 500;      % number of streamwise points
        %
        % essential DEKAF inputs
        options.N                       = 90;                   % number of wall-normal points
        options.L                       = 100;                  % wall-normal domain size, in nondimensional eta units
        options.eta_i                   = 6;                    % mapping parameter, eta_crit
        options.thermal_BC              = 'adiab';              % wall bc
        options.flow                    = 'CPG';                % flow assumption
        options.mixture                 = 'Air';                % mixture
        options.modelTransport          = 'Sutherland';         % transport model
        options.mu_ref                  = 1.716e-05;            % Sutherland's reference mu             [kg/m-s]
        options.T_ref                   = 273.15;               % Sutherland's reference temperature    [K]
        options.Su_mu                   = 110.6;                % Sutherland's constant for mu          [K]
        options.cstPr                   = true;                 % constant Prandtl number
        options.Pr                      = Pr;
        options.specify_cp              = true;                 % constant cp
        intel.cp                        = cp;
        options.specify_gam             = true;                 % constant gamma
        intel.gam                       = gam;
        %
        options.tol                     = eps;                  % convergence tolerance
        options.it_max                  = 100;                  % maximum number of iterations
        options.it_res_sat              = 8;                    % set number of iterations to determine convergence saturation
        options.N_thread                = 1;                    % parfor access to # of physical cores
        options.plot_transient          = false;                % to plot the inter-iteration profiles
        options.plotRes                 = false;                % to plot the residual convergence
        options.MarchingGlobalConv      = false;                % global convergence plot
        options.dimoutput               = true;                 % to output fields in dimensional form
        options.display                 = 0;                    % throttle display of messages
        options.BLproperties            = true;                 % calculate boundary-layer properties, like delta99 and shape-factor
        options.rotateVelocityVector    = 'streamline';         % output streamline-rotated velocities as u_s and w_s in intel
        %
        % --- Run DEKAF --- %
        [intel_all{iL,iside},options_all{iL,iside}] = DEKAF(intel,options,'marching'); %#ok<SAGROW>
        %
        % Example saving per loop iteration.
        %saveLight(['DKF_NACA0012_',num2str(alpha_v_u),'deg_L',num2str(lLambda(iL)),'_',lsides{iside},'.mat'],...
        %    intel_all{iL,iside},options_all{iL,iside});
    end
end

% Save all sweeps and sides into one large .mat
saveDEKAF(['DKF_NACA0012_',num2str(alpha_v_u),'deg.mat'],...
    'intel_all','options_all','lLambda','lsides');

%% Obtaining inviscid streamline etc.
s_SL    = cell(NL,Nsides);
phi_SL  = cell(NL,Nsides);
x_SL    = cell(NL,Nsides);
y_SL    = cell(NL,Nsides);
z_SL    = cell(NL,Nsides);
for iL=1:NL
    for iside=1:Nsides
        s_SL{iL,iside} = intel_all{iL,iside}.x(1,1:options_all{iL,iside}.mapOpts.separation_i_xi);
        phi_SL{iL,iside} = rad2deg(intel_all{iL,iside}.phi_s(1:options_all{iL,iside}.mapOpts.separation_i_xi));
        xc_geom = intel_all{iL,iside}.airfoil.xc_geom_data*airfoil.chord_U;
        yc_geom = intel_all{iL,iside}.airfoil.yc_geom_data*airfoil.chord_U;
        xCp = xc_geom;
        Cp = intel_all{iL,iside}.airfoil.Cp_UV_data;
        [x_SL{iL,iside},y_SL{iL,iside},z_SL{iL,iside}] = getSLTrajectory(s_SL{iL,iside},phi_SL{iL,iside},...
            xc_geom,yc_geom,xCp,Cp,lsides{iside},options_all{iL,iside}.airfoil.data_orientation);
    end
end

%% Plotting streamlines
legends = strcat('\Lambda = ', cellstr(num2str(lLambda.')), ' deg');
streamlinevargin = {};
if all(ismember({'top','bottom'},lsides))
    streamlinevargin = {'topAndBottom'};
end
if strcmp(options_all{1,1}.airfoil.data_orientation, 'ccw_from_top_TE')
    streamlineargs = {flip(xc_geom),flip(yc_geom),...
        cellfun(@flip, x_SL, 'UniformOutput', 0),...
        cellfun(@flip, y_SL, 'UniformOutput', 0),...
        cellfun(@flip, z_SL, 'UniformOutput', 0)};
else
    streamlineargs = {xc_geom, yc_geom, x_SL, y_SL, z_SL};
end
plotStreamlinesAirfoil(streamlineargs{:},legends,streamlinevargin{:});

%% Plotting flow variables
axgcafs = 15;
axfs = 1.2*axgcafs;
lfs = axgcafs;
cbfs = 0.8*axgcafs;
n_levels = 40;
plot_etai = true;
Nvprof = 13;
lopts = {'interpreter','latex','edgecolor','none'};
% For 2016b, MATLAB can't directly set 'fontsize' of a legend with lopts{:}
lsetopts = {'fontsize',lfs};
axopts = {'interpreter','latex','fontsize',axfs};
axgcaopts = {'fontsize',axgcafs,'ticklabelinterpreter','latex'};
cfopts = {n_levels,'edgecolor','none'};
cbopts = {'location','northoutside','ticklabelinterpreter','latex','fontsize',cbfs};
cblopts = axopts;

for iL=1:NL
    for iside=1:Nsides
        plotlabel = ['$\Lambda = ', num2str(lLambda(iL)), '^\circ,$ side = ', lsides{iside}];
        if options_all{iL,iside}.mapOpts.separation_i_xi ~= options_all{iL,iside}.mapOpts.N_x
            % we separated at some point
            N_s = options_all{iL,iside}.mapOpts.separation_i_xi-1;
        else
            % no separation! we marched the entire extent
            N_s = options_all{iL,iside}.mapOpts.separation_i_xi;
        end
        %
        % Plot v profiles for various s stations
        fnumvprof = figure;
        clf(fnumvprof)
        set(fnumvprof,'color','w');
        p = 0.7; % cluster indices near attachment line
        vinds = round(linspace(1,N_s^p,Nvprof).^(1/p));%[1 10:10:N_s];
        hvprof = gobjects(1,Nvprof);
        for inds = 1:Nvprof
            isi = vinds(inds);
            hvprof(inds) = plot(intel_all{iL,iside}.v(:,isi),intel_all{iL,iside}.y(:,isi),...
                'color',magma(inds,round(1.1*length(vinds))));
            hold on;
        end
        xlabel('$\overline{v}$ (m/s)',axopts{:})
        ylabel('$y$ (m)',axopts{:})
        set(gca,axgcaopts{:})
        htb = TextLocation(plotlabel,lopts{:});
        set(htb, lsetopts{:});
        legvprof = cellstr(num2str(intel_all{iL,iside}.airfoil.x(vinds).'/intel_all{iL,iside}.airfoil.chord_U,'%.5f'));
        legvprof{2} = [legvprof{2}, '=\;x/c'];
        legvprof{1} = '\mathrm{attachment\;line}';
        legvprof = strcat('$',legvprof,'$');
        legend(hvprof, legvprof, lopts{:},'location','best');
        %
        % Plot a contour of u against s and y with grid lines
        fnumusycont = figure;
        clf(fnumusycont)
        set(fnumusycont,'color','w');
        isep = options_all{iL,iside}.mapOpts.separation_i_xi;
        contourf(intel_all{iL,iside}.x(:,1:isep), intel_all{iL,iside}.y(:,1:isep), intel_all{iL,iside}.u(:,1:isep), cfopts{:});
        xlabel('$s$ (m)',axopts{:})
        ylabel('$y$ (m)',axopts{:})
        colormap(magma(n_levels))
        cbh = colorbar(cbopts{:});
        set(cbh.Label,cblopts{:},'string','$\overline{u}$ (m/s)');
        set(gca,axgcaopts{:})
        htb = TextLocation(plotlabel,lopts{:});
        set(htb, lsetopts{:});
        %
        hold on
        gl = 10^-5; % grid contour level
        % Plot horizontal and vertical grid lines
        contour(intel_all{iL,iside}.x(:,1:isep), intel_all{iL,iside}.y(:,1:isep), ...
            gl*repmat([1:options_all{iL,iside}.N]',1,isep), ...
            gl*options_all{iL,iside}.N/2*(0:2/(options_all{iL,iside}.N-1):2),'-k') %#ok<NBRAK>
        contour(intel_all{iL,iside}.x(:,1:isep), intel_all{iL,iside}.y(:,1:isep), ...
            gl*repmat(1:isep,options_all{iL,iside}.N,1), ...
            gl*isep/2*(0:2/(isep-1):2),'-k')
        %
        if plot_etai
            % If Malik-mapping, this index is N/2, but we do a min(abs()) for generality
            [~,eta_i_dem] = min(abs(intel_all{iL,iside}.eta - options_all{iL,iside}.eta_i));
            contour(intel_all{iL,iside}.x(:,1:isep), intel_all{iL,iside}.y(:,1:isep), ...
                gl*repmat([1:options_all{iL,iside}.N]',1,isep), ...
                gl*eta_i_dem*[1 1],'-r') %#ok<NBRAK>
        end
        %
        % Create the airfoil's global coordinates
        Ny = options_all{iL,iside}.N;
        theta_wn = repmat(intel_all{iL,iside}.airfoil.theta_wn,[Ny,1]);
        x_af_1D = intel_all{iL,iside}.airfoil.x;
        y_af_1D = intel_all{iL,iside}.airfoil.y;
        xglobal = repmat(x_af_1D/airfoil.chord_U,[Ny,1]) + intel_all{iL,iside}.y/airfoil.chord_U.*cosd(theta_wn);
        yglobal = repmat(y_af_1D/airfoil.chord_U,[Ny,1]) + intel_all{iL,iside}.y/airfoil.chord_U.*sind(theta_wn);
        %
        % Plot u contours (without grid lines)
        fnumucont = figure;
        clf(fnumucont)
        set(fnumucont,'color','w');
        % on the airfoil's x_g and y_g
        subplot(2,1,1)
        contourf(xglobal(:,1:isep), yglobal(:,1:isep), intel_all{iL,iside}.u(:,1:isep), cfopts{:});
        xlabel('$x_{\mathrm{g}}/c$', axopts{:})
        ylabel('$y_{\mathrm{g}}/c$', axopts{:})
        caxis([min(min(intel_all{iL,iside}.u(:,1:isep))) max(max(intel_all{iL,iside}.u(:,1:isep)))])
        colormap(magma(n_levels))
        cbh = colorbar(cbopts{:});
        set(cbh.Label,cblopts{:},'string','$\overline{u}$ (m/s)');
        axis equal
        hold on
        plot((intel_all{iL,iside}.airfoil.x)/airfoil.chord_U,intel_all{iL,iside}.airfoil.y/airfoil.chord_U,'-k')
        set(gca,axgcaopts{:})
        %
        % or against s and y
        subplot(2,1,2)
        contourf(intel_all{iL,iside}.x(:,1:isep), intel_all{iL,iside}.y(:,1:isep), intel_all{iL,iside}.u(:,1:isep), cfopts{:});
        xlabel('$s$ (m)',axopts{:})
        ylabel('$y$ (m)',axopts{:})
        caxis([min(min(intel_all{iL,iside}.u(:,1:isep))) max(max(intel_all{iL,iside}.u(:,1:isep)))])
        colormap(magma(n_levels))
        %cbh = colorbar(cbopts{:});
        %set(cbh.Label,cblopts{:});
        set(gca,axgcaopts{:})
        htb = TextLocation(plotlabel,lopts{:});
        set(htb, lsetopts{:});
        %
        % Plot w_s contours
        fnumwscont = figure;
        clf(fnumwscont)
        set(fnumwscont,'color','w');
        % on the airfoil's x_g and y_g
        subplot(2,1,1)
        contourf(xglobal(:,1:isep), yglobal(:,1:isep), intel_all{iL,iside}.w_s(:,1:isep), cfopts{:});
        xlabel('$x_{\mathrm{g}}/c$', axopts{:})
        ylabel('$y_{\mathrm{g}}/c$', axopts{:})
        caxis([min(min(intel_all{iL,iside}.w_s(:,1:isep))) max(max(intel_all{iL,iside}.w_s(:,1:isep)))])
        colormap(magma(n_levels))
        cbh = colorbar(cbopts{:});
        set(cbh.Label,cblopts{:},'string','$\overline{w}_s$ (m/s)');
        axis equal
        hold on
        plot((intel_all{iL,iside}.airfoil.x)/airfoil.chord_U,intel_all{iL,iside}.airfoil.y/airfoil.chord_U,'-k')
        set(gca,axgcaopts{:})
        %
        % or against s and y
        subplot(2,1,2)
        contourf(intel_all{iL,iside}.x(:,1:isep), intel_all{iL,iside}.y(:,1:isep), intel_all{iL,iside}.w_s(:,1:isep), cfopts{:})
        xlabel('$s$ (m)',axopts{:})
        ylabel('$y$ (m)',axopts{:})
        caxis([min(min(intel_all{iL,iside}.w_s(:,1:isep))) max(max(intel_all{iL,iside}.w_s(:,1:isep)))])
        colormap(magma(n_levels))
        %cbh = colorbar(cbopts{:});
        set(gca,axgcaopts{:})
        htb = TextLocation(plotlabel,lopts{:});
        set(htb, lsetopts{:});
    end
end

fnumaf = figure;
clf(fnumaf)
set(fnumaf,'color','w');
Ni=2;Nj=1;ic=0;
% Pressure coefficient, Cp
ic=ic+1;
subplot(Ni,Nj,ic);
plot(intel_all{1,1}.airfoil.xc_geom_data,intel_all{1,1}.airfoil.Cp_UV_data);
set(gca,'ydir','reverse');
xlabel('$x/c$',axopts{:})
ylabel('$C_p^{\overline{u}_\infty \overline{v}_\infty}$',axopts{:})
set(gca,axgcaopts{:})
% airfoil coordinates
ic=ic+1;
subplot(Ni,Nj,ic);
plot(intel_all{1,1}.airfoil.xc_geom_data,intel_all{1,1}.airfoil.yc_geom_data);
axis equal
xlabel('$x/c$',axopts{:})
ylabel('$y/c$',axopts{:})
set(gca,axgcaopts{:})
