% Example_QSS_CPG is an example of how to run a typical quasi-self-similar
% case in perfect-gas conditions.
%
% The present models saves the self-similar quantities, without evaluating
% them at any streamwise position. See how to do so in the documentation
% of eval_dimVars, under "Examples for self-similar solutions".
%
% The fixed edge conditions are temperature, unit Reynolds number and Mach
% number, but numerous other combinations are possible. Type "help DEKAF"
% to see the supported inputs. For a full breakdown of the options
% available, together with their default values, type "help setDefaults"
%
% Author: Fernando Miro Miro
% May 2019


clear;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
M_e = 5;
T_e = 5000;         % [K]
Tw  = T_e;          % [K]
Re1_e = 10e6;       % [1/m]
wedge_angle = 5;    % [deg]
beta = -0.05;       % [-]
Lambda_sweep = 45;  % [deg]

% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size (in non-dimensional eta length)
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 5e-13;                % Convergence tolerance
options.it_max                  = 40;                   % maximum number of iterations

% flow options
options.thermal_BC              = 'Twall';              % Wall bc (isothermal)
options.G_bc                    = Tw;                   % value of the wall temperature
options.flow                    = 'CPG';                % flow assumption
options.mixture                 = 'air2';               % mixture
% options.numberOfTemp            = '2T';
% options.flow                    = 'LTE';                % flow assumption
% options.mixture                 = 'air5mutation';       % mixture
options.modelTransport          = 'CE_122';             % transport model
options.Cooke                   = true;                 % we want to solve the Cooke equation too (quasi-3D)

options.rotateVelocityVector    = 'streamline';         % we want to rotate the velocity field to align with the invisicid streamline

options.BLproperties = true;

% display options
options.plotRes = false;                         % we don't want the residual plot

% flow conditions
intel.M_e               = M_e;                  % Mach number at boundary-layer edge            [-]
intel.T_e               = T_e;                  % Temperature at boundary-layer edge            [K]
intel.Tv_e              = T_e;
intel.Re1_e             = Re1_e;                % Unit Reynolds number at boundary-layer edge   [1/m]
intel.beta              = beta;                 % Pressure-gradient Hartree parameter           [-]
intel.Lambda_sweep      = Lambda_sweep;         % Sweep angle                                   [deg]

% Executing DEKAF
[intel,options] = DEKAF(intel,options);

% Saving
saveLight('DKF_CPG_M5_CF.mat',intel,options);

% dimensionalizing
Nx = 101;
x_vec = linspace(1e-2,1,Nx);
options.dimXQSS = true;
intel.x = x_vec;
[intel,options] = eval_dimVars(intel,options,'x_ref',0.1);
% Re_vec = linspace(100,3000,Nx);
% options.fixReQSS = true;
% intel.Re = Re_vec;
% intel = eval_dimVars(intel,options,'Re_ref',1000);


saveLight('DKF_CPG_M5_CF_dim.mat',intel,options);

%% plotting
figure; Nplt = 20;
idx_plt = round(linspace(1,Nx,Nplt+1)); idx_plt(1) = [];
for ix=1:Nplt
subplot(1,3,1); plot(intel.u_s(:,idx_plt(ix)),intel.eta,'Color',two_colors(ix,Nplt,'Rbw2'),'displayname',['x=',num2str(intel.x(1,idx_plt(ix))),'m']); hold on;
                xlabel('u_s [m/s]'); ylabel('\eta [-]'); grid minor; ylim([0,options.eta_i]);
subplot(1,3,2); plot(intel.w_s(:,idx_plt(ix)),intel.eta,'Color',two_colors(ix,Nplt,'Rbw2'),'displayname',['x=',num2str(intel.x(1,idx_plt(ix))),'m']); hold on;
                xlabel('w_s [m/s]'); ylabel('\eta [-]'); grid minor; ylim([0,options.eta_i]);
subplot(1,3,3); plot(intel.T(:,idx_plt(ix)),intel.eta,'Color',two_colors(ix,Nplt,'Rbw2'),'displayname',['x=',num2str(intel.x(1,idx_plt(ix))),'m']); hold on;
                xlabel('T [K]');     ylabel('\eta [-]'); grid minor; ylim([0,options.eta_i]);
end
legend();

figure; [Ni,Nj] = optimalPltSize(4); ic=0;
ic=ic+1;subplot(Ni,Nj,ic);  plot(intel.x(1,:),intel.St);            xlabel('x [m]'); ylabel('St [-]');          title('Stanton');
ic=ic+1;subplot(Ni,Nj,ic);  plot(intel.x(1,:),intel.delta99);       xlabel('x [m]'); ylabel('\delta [m]');      title('BL height');
% ic=ic+1;subplot(Ni,Nj,ic);  plot(intel.x(1,:),intel.eta99);         xlabel('x [m]'); ylabel('\delta_\eta [-]'); title('BL height (on \eta)');
ic=ic+1;subplot(Ni,Nj,ic);  plot(intel.x(1,:),intel.deltastar);     xlabel('x [m]'); ylabel('\delta^* [m]');    title('Displacement thickness');
ic=ic+1;subplot(Ni,Nj,ic);  plot(intel.x(1,:),intel.thetastar);     xlabel('x [m]'); ylabel('\theta^* [m]');    title('Momentum thickness');
% ic=ic+1;subplot(Ni,Nj,ic);  plot(intel.x(1,:),intel.shapefactor);   xlabel('x [m]'); ylabel('H^* [m]'); title('Shape factor');
