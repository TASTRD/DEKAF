% Example_BL_CNE_2 is an example of how to run a typical boundary layer in
% chemical non-equilibrium around a wedge. The conical shock jump is
% solved, then assuming frozen conditions at the boundary-layer edge.
%
% The fixed preshock conditions are temperature, prerssure and Mach number,
% but numerous other combinations are possible. Type "help DEKAF" to see
% the supported inputs. For a full breakdown of the options available,
% together with their default values, type "help setDefaults"
%
% Author: Fernando Miro Miro
% September 2019


clear;

addpath(genpath([getenv('DEKAF_DIRECTORY'),'/SourceCode']))

% Flow conditions
M_e = 10;
T_e = 1000; % K
p_e = 4000; % Pa
cone_angle = 5; % deg
Tw = 2000; % K

% Solver inputs
options.N                       = 100;                  % Number of points
options.L                       = 40;                   % Domain size
options.eta_i                   = 6;                    % Mapping parameter, eta_crit
options.tol                     = 5e-13;                % Convergence tolerance
options.it_max                  = 40;                   % maximum number of iterations
options.T_tol                   = 5e-13;                % convergence tolerance for the NR to get T from the enthalpy
options.T_itMax                 = 200;                  % maximum number of iterations for the NR to get T from the enthalpy

% flow options
options.thermal_BC              = 'Twall';              % Wall bc
options.G_bc                    = Tw;                   % wall temperature [K]
options.flow                    = 'CNE';                % flow assumption
options.mixture                 = 'air5mutation';       % mixture
options.modelTransport          = 'CE_123';             % transport model
options.modelDiffusion          = 'FOCE_Ramshaw';       % transport model

% shock options
options.coordsys        = 'cone';               % conical coordinate system
options.shockJump       = true;                 % we do want to use shock relations
intel.cone_angle        = cone_angle;           % cone angle in degrees

% display options
options.plotRes = false;                        % we don't want the residual plot

% flow conditions
intel.M_e0               = M_e;                  % Mach number at boundary-layer edge                        [-]
intel.T_e0               = T_e;                  % Static temperature at boundary-layer edge                 [K]
intel.p_e0               = p_e;                  % Absolute static pressure at boundary-layer edge           [Pa]

% inviscid solver
options.inviscidFlowfieldTreatment = 'constant';

% marching mapping options
options.xSpacing            = 'custom_x';               % mapping in the marching direction
options.mapOpts.x_custom    = linspace(1e-8,1,300);     % streamwise position vector [m]

[intel,options] = DEKAF(intel,options,'marching');

% Saving
saveLight('DKF_CNE2_M10.mat',intel,options);
