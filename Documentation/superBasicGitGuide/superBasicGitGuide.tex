
\documentclass[10pt, onecolumn, a4paper]{article}
\usepackage{placeins}
\usepackage{bm}% bold math

\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{placeins}
\usepackage{breqn}
%\usepackage{amsmath}
\usepackage{mathrsfs}
\usepackage{amsfonts}
\usepackage[cm]{fullpage}
\usepackage[superscript,biblabel]{cite}
\usepackage{multirow}
\usepackage{hyperref}

\usepackage{pifont}% http://ctan.org/pkg/pifont
\newcommand{\cmark}{\ding{51}}%
\newcommand{\xmark}{\ding{55}}%

\newcommand{\me}{\mathrm{e}}
%\newcommand{\ln}{\mathrm{ln}}
\newcommand{\code}[1]{\texttt{#1}}

\title{Super basic guide to git}
\author{Fernando Mir\'o Mir\'o and Ethan Beyak}

\begin{document}

\maketitle

\section{The bible}

If there is anything you need to know, or if you have any question on git it's probably explained in \href{<https://github.com/progit/progit2/releases/download/2.1.16/progit.pdf>}{the bible}. The small section on the history of git is also a cool read.

\section{First steps - install everything}

Install git:\\

\code{sudo apt-get install git}\\

\noindent go to \href{<https://gitlab.com/users/sign-in>}{sign in}, fill in the register next next next...\\

\noindent once you have made an account, you need to join the DEKAF group. Send a request to the group administrators, which as of 05/28/2019 are:
\begin{itemize}
\item Ethan Beyak: \href{ethanbeyak@gmail.com}{ethanbeyak@gmail.com}
\item Fernando Mir\'o Mir\'o: \href{fernando.miro.miro@vki.ac.be}{fernando.miro.miro@vki.ac.be}
\item Alexander Moyes: \href{alexandermoyes7@gmail.com}{alexandermoyes7@gmail.com}
\item Andrew Riha: \href{andrewriha@tamu.edu}{andrewriha@tamu.edu}
\end{itemize}

\noindent Once you have been granted access go to \href{https://docs.gitlab.com/ce/ssh/README.html}{the readme} and follow the steps to ``generating a new ssh key pair''. Avoid the sixth step, because it gives problems and it doesn't really matter.\\

\noindent \textbf{Important}: if you plan on using several computers with one same gitlab account, make sure to give an identifiable name when you copy your key to your gitlab profile.

\section{svn vs git}

There's a lot of documentation out there of the idea behind git and so on. We recommend watching the following two videos before doing anything, to get an idea of how it works, what's the philosophy behind, etc: \href{<https://www.youtube.com/watch?v=OqmSzXDrJBk>}{4min video}, \href{<https://www.youtube.com/watch?v=Y9XZQO1n_7c&t=1s>}{20min video}.\\

Unlike svn or other centralized version control, git is a \textit{distributed version control} -- everyone has the complete history of the project (trunks, branches, whatever) in his local machine. Therefore most of the work is done locally. It enables and encourages branching often, not only to create an area where you can work freely, but for small changes you may make whilst working. For more info on the differences between svn and git see \href{<https://www.quora.com/What-are-the-differences-between-Subversion-and-Git-Id-like-to-also-version-control-design-files>}{this link}.

\begin{figure}
\center
\includegraphics[width=0.70\textwidth]{git_sketch.png}
\caption{Git architecture}
\label{git_sketch}
\end{figure}

\section{Basic commands}

\subsection{Cloning remote repository (very first step)}
\begin{itemize}
 \item \code{git clone git@gitlab.com:TASTRD/DEKAF.git /folder/} will clone the remote repository onto the folder in your local machine.
\end{itemize}

\subsection{Local single-branch commands}
\begin{itemize}
 \item \code{git add file} will add a new file to version control
 \item \code{git reset HEAD} would revert any adds that you've done, brining your current HEAD to a the previously saved state. It also has a bunch of options, like doing \code{HEAD\~{}2} will go back to two commitments ago. Again, refer to the bible for more info.
 \item \code{git commit -m \"{}FMM: description\"} commits your changes to the LOCAL repository leaving an explanatory message. It is good practice to start the message with your initials
 \item \code{git rm file} removes a file from version control. It has to have been committed before, otherwise it doesn't work.
 \item \code{git log} shows you the commitment history. It has a lot of options so that you can see only a certain period of time, or so that it prints out only basic information, or so that it tells you what branch you're on, etc. Refer to the bible for specifics.
 \item \code{git status} shows you what is the status of your current working branch: what you've added, what hasn't been version controlled, what hasn't been committed, etc.
 \item \code{git show <commit_ID>} shows you what exactly was changed by a certain commitment. Where \code{<commit_ID>} is the SHA1, the hexadecimal 40-digit code identifying the commitment.
 \item \code{git stash} allows you to save whatever changes you have made in a branch in a ''dirty'' floder, so that you can freely checkout to other branches. If you want to recover the stashed changes, you should call \code{git stash apply}.
\end{itemize}

\subsection{Local multi-branch commands}
\begin{itemize}
 \item \code{git branch NewBranch} creates a new branch named NewBranch from your current working branch
 \item \code{git branch -d NewBranch} deletes the NewBranch
 \item \code{git checkout NewBranch} changes your workspace to the NewBranch
 \item \code{git checkout master} will bring you back to the master (the trunk or main branch)
 \item \code{git merge NewBranch} will merge the contents of the NewBranch onto your current branch. If there are any conflicts, it will document them, you'll have to solve them and then commit.
\end{itemize}

\noindent \textbf{Important}: in order to resolve conflicts in a branch rather than the master, it is recommended to merge the master \textit{into} the branch, and not the other way around. You can then solve your conflicts in the branch without disrupting the master, and when you're done you can merge everything into the master.

\noindent An \textbf{even better} alternative merging workflow is to:
\begin{enumerate}
\item \code{git checkout NewBranch} to go into the branch you want to merge.
\item \code{git rebase master} to rewind back to the first common commit shared by master and NewBranch, apply the master commits, and then apply the NewBranch commits after them. Any possible conflicts can be solved on the fly during the rebase, thus \textbf{avoiding hateful merge commits}. The conflicts are solved in the branch, keeping the master clean.
\item \code{git checkout master} to go back to the master.
\item \code{git merge NewBranch} to merge all the changes made in the branch to the master. Having already rebased in the branch, this will be done cleanly and without troubles.
\end{enumerate}

\subsection{Remote single-branch commands}

\begin{wrapfigure}{R}{0.3\textwidth}
\centering
\includegraphics[width=0.25\textwidth]{rebase_conflict.eps}
\caption{\label{fig_rebase_conflict}Conflict when combining local and online repositories.}
\end{wrapfigure}

\begin{itemize}
 \item \code{git push origin master} pushes the changes you've done on the master to the url where your online repository is (where you cloned things from). This is called the origin. If instead of \code{master} you do \code{NewBranch} it will push another branch.
 \item \code{git pull origin master} retrieves the changes in the master branch done in the online repository. Same story, using \code{NewBranch} instead, you pull another branch.
 \item \code{git fetch --dry-run} will tell you if there are any differences between the online and your local repository. To then list the actual differences you should do \code{git fetch origin} and then \code{git log origin/master \^{}master} to list the changes on the master branch
\end{itemize}

\noindent \textbf{Important}: in order to deal with conflicts arising from pulling from the online repository, the recommended procedure is:
\begin{enumerate}
\item \code{git fetch origin master} will fetch the differences between the remote and the local repositories, it will however not overwrite your local repository with updates from the remote one.
\item \code{git log origin/master ^master} will show the differences it just fetched
\item \code{git status} will tell you if the changes can be fast-forwarded or if the remote and your local repositories have diverged (like in Fig. \ref{fig_rebase_conflict}).
\item If they are "fast-forwardable" you can simply \code{git pull}
\item If they've diverged (like in Fig. \ref{fig_rebase_conflict}), you can \code{git rebase origin master}, which will first rewind your HEAD (local repository) to the point where they started diverging (point 4). Then it will apply on it the changes of the remote repository (points 5-6), and try to make the changes in your HEAD (points 7-8). If there were any conflicts, the new changes would remain unstaged. You would have to solve these conflicts (listed in \code{.git/rebase-apply/patch}), \code{add} them, and finally do \code{git rebase --continue}. The final history of the branch would read in the order 1-2-3-4-5-6-7-8.
\end{enumerate}

\subsection{Remote multi-branch commands}
\begin{itemize}
 \item \code{git fetch origin NewBranch} fetches a new branch created by someone else, stored in the online repository
 \item \code{git push -u origin NewBranch} pushes a new branch created in your local repository to the remote one
 \item \code{git remote show origin} it shows you all the different links between your local braches and the remote branches (master included)
 \item \code{git push --delete origin NewBranch} will delete the branch on the remote repository \textbf{but not on the local one}
 \item \code{git remote prune origin} cut links from your local repository to any dead branches in the remote repository. This does not eliminate the branches from your local repository, only the connection with the remote.
 \item \code{git fetch -p \&\& git branch -vv | awk '/: gone]/{print\$1}' | xargs git branch -D} removes from your repository branches that have been eliminated from the remote repository. The command is pretty fancy, the idea is: \code{git fetch -p} prunes the branches (same as \code{git remote prune origin}), and then checks what is the status of the branches with \code{git branch -vv} (\code{-vv} for verbose).  Then comes the fancy part: it reads which branches have "gone" at the end, and then uses their names as arguments to \code{git branch -D}.
\end{itemize}

\noindent To see a bunch of examples go to \href{<https://mislav.net/2010/07/git-tips/>}{this link}.

\section{Random lessons we've been learning on the fly}

You may get to a situation where one of your local branches is no longer linked to the online repository. This means that when you want to update your local repository with the remote, after doing \code{git fetch origin MyBranch}, if you then \code{git status}, it won't show you that your branch is behind. You could still \textit{force} the \code{pull}, but then you may have annoying merge conflicts (you want to stay away from that - ``\textit{every merge should be a controlled endeavour}'', Ethan Beyak, December 2\textsuperscript{nd} 2017, 5:02 a.m.).\\

The way to solve this, is relinking your local branch, to the remote. This can be done through:\\

\code{git branch --set-upstream-to origin/MyBranch}\\


\end{document}
